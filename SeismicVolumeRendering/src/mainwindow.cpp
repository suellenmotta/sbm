#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    unsigned int firstInline,
                 lastInline,
                 stepInline,
                 firstCrossline,
                 lastCrossline,
                 stepCrossline,
                 firstDepthSlice,
                 lastDepthSlice,
                 stepDepthSlice;

    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    renderWidget->getVolumeBounds(firstInline,
                                  lastInline,
                                  stepInline,
                                  firstCrossline,
                                  lastCrossline,
                                  stepCrossline,
                                  firstDepthSlice,
                                  lastDepthSlice,
                                  stepDepthSlice);

    ui->inlineSpinBox->setRange(firstInline,lastInline);
    ui->inlineSpinBox->setSingleStep(stepInline);
    ui->crosslineSpinBox->setRange(firstCrossline,lastCrossline);
    ui->crosslineSpinBox->setSingleStep(stepCrossline);
    ui->depthSpinBox->setRange(firstDepthSlice,lastDepthSlice);
    ui->depthSpinBox->setSingleStep(stepDepthSlice);
    ui->inlineStepSpinBox->setRange(stepInline,lastInline);
    ui->inlineStepSpinBox->setSingleStep(stepInline);
    ui->crosslineStepSpinBox->setRange(stepCrossline,lastInline);
    ui->crosslineStepSpinBox->setSingleStep(stepCrossline);
    ui->depthStepSpinBox->setRange(stepDepthSlice,lastDepthSlice);
    ui->depthStepSpinBox->setSingleStep(stepDepthSlice);

    ui->inlineSpinBox->setValue(firstInline);
    ui->crosslineSpinBox->setValue(firstCrossline);
    ui->depthSpinBox->setValue(lastDepthSlice);
    ui->inlineStepSpinBox->setValue(stepInline);
    ui->crosslineStepSpinBox->setValue(stepCrossline);
    ui->depthStepSpinBox->setValue(stepDepthSlice);

    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa1");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa2");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa3");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa4");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa5");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa6");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa7");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa8");
    item->setCheckState(Qt::CheckState::Checked);

    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("mapa9");
    item->setCheckState(Qt::CheckState::Checked);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::enableInlineSliceOptions(bool enable)
{
    ui->inlineLabel->setEnabled(enable);
    ui->inlineSpinBox->setEnabled(enable);
    ui->inlineStepLabel->setEnabled(enable);
    ui->inlineStepSpinBox->setEnabled(enable);
}

void MainWindow::enableCrosslineSliceOptions(bool enable)
{
    ui->crosslineLabel->setEnabled(enable);
    ui->crosslineSpinBox->setEnabled(enable);
    ui->crosslineStepLabel->setEnabled(enable);
    ui->crosslineStepSpinBox->setEnabled(enable);
}

void MainWindow::enableDepthSliceOptions(bool enable)
{
    ui->depthLabel->setEnabled(enable);
    ui->depthSpinBox->setEnabled(enable);
    ui->depthStepLabel->setEnabled(enable);
    ui->depthStepSpinBox->setEnabled(enable);
}

void MainWindow::on_inlineSpinBox_valueChanged(int value)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->updateInline(value);
        renderWidget->update();
    }
}

void MainWindow::on_crosslineSpinBox_valueChanged(int value)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->updateCrossline(value);
        renderWidget->update();
    }
}

void MainWindow::on_depthSpinBox_valueChanged(int value)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->updateDepthSlice(value);
        renderWidget->update();
    }
}

void MainWindow::on_showInlineCheckbox_clicked(bool checked)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->showInline(checked);
        renderWidget->update();
    }

    enableInlineSliceOptions(checked);
}

void MainWindow::on_showCrosslineCheckbox_clicked(bool checked)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->showCrossline(checked);
        renderWidget->update();
    }

    enableCrosslineSliceOptions(checked);
}

void MainWindow::on_showDepthSliceCheckbox_clicked(bool checked)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        renderWidget->showDepthSlice(checked);
        renderWidget->update();
    }

    enableDepthSliceOptions(checked);
}

void MainWindow::on_horizonsListWidget_itemChanged(QListWidgetItem *item)
{
    auto renderWidget = findChild<RenderWidget*>
            ("openGLWidget");

    if(renderWidget->isValid())
    {
        auto index = ui->horizonsListWidget->row(item);

        renderWidget->showSurface(index,item->checkState() == Qt::Checked);

        renderWidget->update();
    }
}

void MainWindow::on_inlineStepSpinBox_valueChanged(int value)
{
    ui->inlineSpinBox->setSingleStep(value);
}

void MainWindow::on_crosslineStepSpinBox_valueChanged(int value)
{
    ui->crosslineSpinBox->setSingleStep(value);
}

void MainWindow::on_depthStepSpinBox_valueChanged(int value)
{
    ui->depthSpinBox->setSingleStep(value);
}
