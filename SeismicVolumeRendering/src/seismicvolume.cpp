#include "seismicvolume.h"

#include <fstream>
#include <iostream>
#include <fstream>
#include <QDebug>

#include <glm/ext.hpp>

namespace seismic
{

Volume::Volume()
    : NewVolume()
{

}

Volume::Volume(const seismic::Info &info)
    : NewVolume(toVolumeInfo(info))
{
}

Volume::Volume(const seismic::Info &info, float* buffer)
    : NewVolume(toVolumeInfo(info), buffer)
{
}

Volume::~Volume()
{
}

glm::mat4 Volume::getStartModelMatrix()
{
    float seismicTransf[] =
    {
        0,  0,  1, 0,
        1,  0,  0, 0,
        0,  -1, 0, 0,
        0,  0,  0, 1
    };

    return glm::make_mat4(seismicTransf);
}

NewVolume::Info Volume::toVolumeInfo(const seismic::Info& info)
{
    NewVolume::Info newInfo;
//    newInfo.p1 = glm::vec3(info.p1,info.firstDepth);
//    newInfo.p2 = glm::vec3(info.p2,info.firstDepth);
//    newInfo.p3 = glm::vec3(info.p3,info.firstDepth);
//    newInfo.p4 = glm::vec3(info.p4,info.firstDepth);
//    newInfo.p5 = glm::vec3(info.p1,info.lastDepth);
//    newInfo.p6 = glm::vec3(info.p2,info.lastDepth);
//    newInfo.p7 = glm::vec3(info.p3,info.lastDepth);
//    newInfo.p8 = glm::vec3(info.p4,info.lastDepth);
    newInfo.p1 = glm::vec3(info.p2,info.firstDepth);
    newInfo.p2 = glm::vec3(info.p2,info.lastDepth);
    newInfo.p3 = glm::vec3(info.p3,info.lastDepth);
    newInfo.p4 = glm::vec3(info.p3,info.firstDepth);
    newInfo.p5 = glm::vec3(info.p1,info.firstDepth);
    newInfo.p6 = glm::vec3(info.p1,info.lastDepth);
    newInfo.p7 = glm::vec3(info.p4,info.lastDepth);
    newInfo.p8 = glm::vec3(info.p4,info.firstDepth);
//    newInfo.p1 = glm::vec3(info.p2,info.lastDepth);
//    newInfo.p2 = glm::vec3(info.p2,info.firstDepth);
//    newInfo.p3 = glm::vec3(info.p3,info.firstDepth);
//    newInfo.p4 = glm::vec3(info.p3,info.lastDepth);
//    newInfo.p5 = glm::vec3(info.p1,info.lastDepth);
//    newInfo.p6 = glm::vec3(info.p1,info.firstDepth);
//    newInfo.p7 = glm::vec3(info.p4,info.firstDepth);
//    newInfo.p8 = glm::vec3(info.p4,info.lastDepth);

    auto numInlines = (info.lastInline-info.firstInline)/info.stepInline+1;
    auto numCrosslines = (info.lastCrossline-info.firstCrossline)/info.stepCrossline+1;
    auto numDepthSlices = (info.lastDepth-info.firstDepth)/info.stepDepth+1;
    newInfo.numXSlices = numDepthSlices;
    newInfo.numYSlices = numCrosslines;
    newInfo.numZSlices = numInlines;

    return newInfo;
}

unsigned int Volume::inlineIdx(unsigned int inlineNum)
{
    return
        (inlineNum-seismicInfo.firstInline)/seismicInfo.stepInline;
}

unsigned int Volume::crosslineIdx(unsigned int crosslineNum)
{
    return
        (crosslineNum-seismicInfo.firstCrossline)/seismicInfo.stepCrossline;
}

unsigned int Volume::depthIdx(unsigned int depthSliceNum)
{
    return
        (depthSliceNum-seismicInfo.firstDepth)/seismicInfo.stepDepth;
}



NewVolume* readSeismicVolume(const std::string& filePath, const seismic::Info &info)
{
    std::ifstream input;
    input.open(filePath,std::ios::binary);

    if(input.is_open())
    {
        auto numInlines = (info.lastInline-info.firstInline)/info.stepInline+1;
        auto numCrosslines = (info.lastCrossline-info.firstCrossline)/info.stepCrossline+1;
        auto numDepthSlices = (info.lastDepth-info.firstDepth)/info.stepDepth+1;

        auto size = numInlines*numCrosslines*numDepthSlices;

        float* buffer = nullptr;
        try
        {
            buffer = new float[size];
        }
        catch (const std::bad_alloc& e)
        {
            std::cout << "Allocation failed: " << e.what() << '\n';
            return nullptr;
        }

        input.read(reinterpret_cast<char*>(&buffer[0]),size*sizeof(float));

        if (input)
            std::cout << "all characters read successfully.";
        else
            std::cout << "error: only " << input.gcount() << " could be read";

        input.close();

        return new seismic::Volume(info,buffer);
    }

    return nullptr;
}

}
