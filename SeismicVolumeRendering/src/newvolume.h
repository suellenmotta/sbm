#ifndef NEWVOLUME_H
#define NEWVOLUME_H

#include <glm/glm.hpp>
#include <vector>

class NewVolume
{
public:
    class Info
    {
    public:
        /*
         * Volume bounding box global coordinates
         *
         *       p4------------p3
         *      / |           / |
         *     /  |          /  |
         *    /   p8--------/--p7
         *   p1-----------p2  /
         *    | /          | /
         *    |/           |/
         *   p5-----------p6
         *
         */

        glm::vec3 p1, p2, p3, p4;
        glm::vec3 p5, p6, p7, p8;

        /*
         * Local system slices orientation
         *
         *        Y
         *        ^     Z
         *        |    /
         *        |  /
         *        |/
         *        ------ > X
         */
        unsigned int numXSlices;
        unsigned int numYSlices;
        unsigned int numZSlices;

        void computeWorldPoints(const glm::vec3 &spacing);
    };

    NewVolume();
    NewVolume(const Info& info);
    NewVolume(const Info& info, float* buffer);
    virtual ~NewVolume();

    float* getXSliceBuffer(unsigned int index, unsigned int &width, unsigned int &height);
    float* getYSliceBuffer(unsigned int index, unsigned int &width, unsigned int &height);
    float* getZSliceBuffer(unsigned int index, unsigned int &width, unsigned int &height);

    /*
     * Slice points
     *
     *    p2 ------------- p3
     *    |                |
     *    |                |
     *    p0 ------------- p1
     *
     */
    void getXSlicePoints(unsigned int index, std::vector<glm::vec3>& points);
    void getYSlicePoints(unsigned int index, std::vector<glm::vec3>& points);
    void getZSlicePoints(unsigned int index, std::vector<glm::vec3>& points);

    glm::vec3 getCenter();
    float getCircumscribedSphereRadius();
    virtual glm::mat4 getStartModelMatrix();

protected:

    virtual unsigned int getIndex(unsigned int x,
      unsigned int y, unsigned int z);

    Info info;
    float* buffer;
};

NewVolume* readNRRD(const std::string& filePath);

#endif // NEWVOLUME_H
