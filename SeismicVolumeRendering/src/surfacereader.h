#ifndef SURFACEREADER_H
#define SURFACEREADER_H

#include <string>
#include <vector>
#include <glm/glm.hpp>

bool readTSurfFile(const std::string& filePath, std::vector<glm::vec3>& vertices,
                   std::vector<unsigned int> &indices);

bool readOffFile(const std::string& filePath, std::vector<glm::vec3>& vertices,
                   std::vector<unsigned int> &indices);


#endif // SURFACEREADER_H
