#include "surfacereader.h"

#include <cstdlib>
#include <iostream>
#include <fstream>


bool readTSurfFile(const std::string& filePath, std::vector<glm::vec3> &vertices, std::vector<unsigned int>& indices)
{
    std::ifstream in( filePath );
    if (!in)
    {
        printf( "Error opening the file %s\n", filePath.c_str() );
        return false;
    }

    int cont = 1;
    while (in)
    {
        //Search the header.
        std::string line;
        while (in >> line && line != "HEADER");
        if (!in)
            return false;;

        //Read the line.
        getline( in, line );

        while (in >> line && line != "GEOLOGICAL_FEATURE");

        //Surface name.
        std::string surfaceName, surfaceType;

        //Get the surface name.
        in >> surfaceName;
        while (in >> line && line != "GEOLOGICAL_TYPE");

        in >> surfaceType;

        printf( "%d:\nReading %s %s...\n", cont, surfaceType == "top" ? "horizon" : "fault", surfaceName.c_str( ) );
        cont++;

        while (in >> line && line != "TFACE");

        while (in >> line && line == "VRTX")
        {
            unsigned int index;
            glm::vec3 p;
            in >> index >> p.x >> p.z >> p.y;
            vertices.push_back( p );
        }

        if (line != "TRGL")
        {
            while (in >> line && line != "TRGL");
        }

        unsigned int t[3];
        in >> t[0] >> t[1] >> t[2];
        t[0]--; t[1]--; t[2]--;
        indices.insert(indices.end(),t,t+3);
        while (in >> line && line == "TRGL")
        {
            in >> t[0] >> t[1] >> t[2];
            t[0]--; t[1]--; t[2]--;
            indices.insert(indices.end(),t,t+3);
        }

        while (in >> line && line != "END");
    }

    return true;
}



bool readOffFile(const std::string& filePath, std::vector<glm::vec3>& vertices,
                   std::vector<unsigned int> &indices)
{
    std::ifstream in( filePath );
    if (!in)
    {
        printf( "Error opening the file %s\n", filePath.c_str() );
        return false;
    }

    std::string off;
    in >> off;

    int numVertices, numTriangles, numEdges;
    in >> numVertices >> numTriangles >> numEdges;

    int verticesStart = vertices.size();
    for(auto i = 0; i < numVertices; ++i)
    {
        float x,y,z;
        in >> x >> y >> z;

        vertices.push_back(glm::vec3(x,y,z));
    }

    for(auto i = 0; i < numTriangles; ++i)
    {
        unsigned int n, i0, i1, i2;
        in >> n >> i0 >> i1 >> i2;

        indices.push_back(i0+verticesStart);
        indices.push_back(i1+verticesStart);
        indices.push_back(i2+verticesStart);
    }

    return true;
}
