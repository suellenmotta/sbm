#ifndef ARCBALL_H
#define ARCBALL_H

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

class Arcball
{
public:
    Arcball(){}
    Arcball(const glm::ivec2& center, float radius)
        : center(center)
        , radius(radius) {}

    //Center and radius in screen coordinates
    glm::ivec2 center;
    float radius;

    glm::quat getRotation(glm::ivec2 p0, glm::ivec2 p1)
    {
        auto q0 = screenToArcsphere(p0);
        auto q1 = screenToArcsphere(p1);

        return q1*glm::conjugate(q0);
    }

private:

    glm::quat screenToArcsphere(const glm::ivec2 & screenPos)
    {
        if(radius > 0)
        {
            glm::vec3 v(glm::vec2(screenPos-center)/radius, 0.0f);
            auto r = glm::length2(v);
            if(r > 1)
                v *= (1.0f/glm::sqrt(r));
            else
                v.z = glm::sqrt(1-r);

            return glm::quat(0.0f, v);
        }

        return glm::quat();
    }
};


#endif

