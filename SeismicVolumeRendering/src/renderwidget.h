#ifndef RENDERWIDGET_H
#define RENDERWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>

#include <vector>

#include "glm/glm.hpp"
#include "seismicvolume.h"
#include "volumerenderer.h"

class RenderWidget
        : public QOpenGLWidget
        , protected QOpenGLExtraFunctions
{
public:
    RenderWidget(QWidget* parent);
    virtual ~RenderWidget();

    void setupSeismicScene();
    void setupBabyGEScene();

    VolumeRenderer volumeRenderer;

    void getVolumeBounds(unsigned int& firstInline,
                         unsigned int& lastInline, unsigned int &stepInline,
                         unsigned int& firstCrossline,
                         unsigned int& lastCrossline, unsigned int &stepCrossline,
                         unsigned int& firstDepthSlice,
                         unsigned int& lastDepthSlice, unsigned int &stepDepthSlice);




    void addSurface(const std::string& filePath);

    void updateInline(unsigned int inlineNum);
    void updateCrossline(unsigned int crosslineNum);
    void updateDepthSlice(unsigned int depthSliceNum);

    void showInline(bool show = true);
    void showCrossline(bool show = true);
    void showDepthSlice(bool show = true);

    void showSurface(unsigned int index, bool show = true);

private:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int w, int h);

    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void wheelEvent(QWheelEvent *event);

    seismic::Info info;

    QOpenGLShaderProgram* surfaceProgram;

    unsigned int surfaceVAO;
    std::vector<glm::vec3> colors;
    std::vector<GLuint> surfaceVBOs;
    std::vector<GLuint> surfaceEBOs;
    std::vector<glm::vec3> surfaceColors;
    std::vector<GLsizei> numElements;
    std::vector<bool> surfaceShow;

    glm::ivec2 oldPos;
    bool moving;

};

#endif // RENDERWIDGET_H
