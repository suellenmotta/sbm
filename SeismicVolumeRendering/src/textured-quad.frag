#version 400 core

in vec2 uv;
uniform sampler2D valueSampler;
uniform sampler1D lutSampler;
out vec3 color;

uniform uint numColors;
uniform float colorMarkers[6];

uniform float minValue;
uniform float maxValue;

void main()
{
    float value = texture( valueSampler, uv ).r;
    float u = clamp((value-minValue)*(1/(maxValue-minValue)),0,1);
//    color = vec3(u,u,u); return;
    if( u > colorMarkers[5] ) u = colorMarkers[5];

    uint maxIdx=0;
    while(u>colorMarkers[maxIdx]) maxIdx++;

    float p = 1.0f / 5.0f;
    if(maxIdx < 1) u = 0.0f;
    else
    {
        uint minIdx = maxIdx-1;

        float f = p / (colorMarkers[maxIdx]-colorMarkers[minIdx]);

        u = ((u-colorMarkers[minIdx])*f) + (minIdx*p);
    }

    color = texture(lutSampler,u).rgb;

}
