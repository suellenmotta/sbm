#include "volumerenderer.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>

#include <QMatrix>

struct vertex
{
    glm::vec3 pos;
    glm::vec2 texCoord;
};

#define MIN_VAL -93.0f;
#define MAX_VAL 88.0f;

VolumeRenderer::VolumeRenderer()
    : xSliceShow(true)
    , ySliceShow(true)
    , zSliceShow(true)
    , volume(nullptr)
{

}

VolumeRenderer::~VolumeRenderer()
{
    delete volume;

    glDeleteVertexArrays(1, &VAO);

    glDeleteBuffers(1, &xSliceVBO);
    glDeleteBuffers(1, &ySliceVBO);
    glDeleteBuffers(1, &zSliceVBO);

    glDeleteTextures(1, &xSliceTexture);
    glDeleteTextures(1, &ySliceTexture);
    glDeleteTextures(1, &zSliceTexture);
}

void VolumeRenderer::init()
{
    initializeOpenGLFunctions();

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/textured-quad.vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/textured-quad.frag");
    program.link();

    if(program.isLinked())
    {
        glGenBuffers(1, &zSliceVBO);
        glGenBuffers(1, &ySliceVBO);
        glGenBuffers(1, &xSliceVBO);

        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);
        glEnableVertexAttribArray( 0 );
        glEnableVertexAttribArray( 1 );
        glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
        glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
        glVertexAttribBinding(0,0);
        glVertexAttribBinding(1,0);

        glGenTextures(1, &zSliceTexture);
        glGenTextures(1, &ySliceTexture);
        glGenTextures(1, &xSliceTexture);

        float colorTable[] =
        {
            170/255.0f,0.0f,0.0f,
            1.0f,28/255.0f,0.0f,
            1.0f,200/255.0f,0.0f,
            243/255.0f,243/255.0f,243/255.0f,
            56/255.0f,70/255.0f,127/255.0f,
            0.0f,0.0f,0.0f
        };
        lutMarkers = {0.0f,0.070352f,0.25f,0.5f,0.883249f,1.0f};

        glGenTextures(1, &lutTexture);
        updateColorTableTexture(colorTable,6);
    }
}

void VolumeRenderer::setVolume(NewVolume* volume)
{
    if(this->volume)
        delete volume;

    this->volume = volume;

    focus();

    updateXSlice(0);
    updateYSlice(0);
    updateZSlice(0);
}

void VolumeRenderer::setWindowSize(int width, int height)
{
    cam.width = width;
    cam.height = height;

    arcball.center = glm::ivec2(cam.width/2,cam.height/2);
    arcball.radius = glm::max(cam.width,cam.height);
}

void VolumeRenderer::focus()
{
    cam.model = volume->getStartModelMatrix();
    auto center = cam.model*glm::vec4(volume->getCenter(),1);

    cam.model = glm::translate(glm::mat4(),glm::vec3(-center))*cam.model;

    cam.fovy  = 60.f;

    auto radius = volume->getCircumscribedSphereRadius();
    auto aspect = (float)cam.width/cam.height;
    auto fovx = cam.fovy*aspect;

    auto distance = glm::max(
                radius / glm::tan(glm::radians(fovx*0.5f)),
                radius / glm::tan(glm::radians(cam.fovy*0.5f)));

    cam.at = glm::vec3();
    cam.eye = cam.at;
    cam.eye.z += distance;
    cam.up = glm::vec3(0.f,1.f,0.f);
    cam.zNear = 0.005f*distance;
    cam.zFar  = 100*distance;
}

void VolumeRenderer::rotate(const glm::ivec2& oldScreenPos, const glm::ivec2& newScreenPos)
{
    glm::quat q = arcball.getRotation(oldScreenPos, newScreenPos);
    auto Rot = glm::mat4_cast(q);
    cam.model = Rot * cam.model;
}

void VolumeRenderer::zoom(float delta)
{
    cam.eye += (cam.at - cam.eye) * 0.001f * delta;
}

void VolumeRenderer::updateXSlice(unsigned int index)
{
    std::vector<glm::vec3> vertices;
    volume->getXSlicePoints(index, vertices);
    updateVBO(vertices,xSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getXSliceBuffer(index,width,height);
    updateTexture(slice,width,height,xSliceTexture);

    xSliceMinVal = MIN_VAL;
    xSliceMaxVal = MAX_VAL;
//    xSliceMinVal = *(std::min_element(slice,slice+width*height));
//    xSliceMaxVal = *(std::max_element(slice,slice+width*height));

    delete[] slice;
}

void VolumeRenderer::updateYSlice(unsigned int index)
{
    std::vector<glm::vec3> vertices;
    volume->getYSlicePoints(index, vertices);
    updateVBO(vertices,ySliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getYSliceBuffer(index,width,height);
    updateTexture(slice,width,height,ySliceTexture);

    ySliceMinVal = MIN_VAL;
    ySliceMaxVal = MAX_VAL;
//    ySliceMinVal = *(std::min_element(slice,slice+width*height));
//    ySliceMaxVal = *(std::max_element(slice,slice+width*height));

    delete[] slice;
}

void VolumeRenderer::updateZSlice(unsigned int index)
{
    std::vector<glm::vec3> vertices;
    volume->getZSlicePoints(index, vertices);
    updateVBO(vertices,zSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getZSliceBuffer(index,width,height);
    updateTexture(slice,width,height,zSliceTexture);

    zSliceMinVal = MIN_VAL;
    zSliceMaxVal = MAX_VAL;
//    zSliceMinVal = *(std::min_element(slice,slice+width*height));
//    zSliceMaxVal = *(std::max_element(slice,slice+width*height));

    delete[] slice;
}

void VolumeRenderer::showXSlice(bool show)
{
    xSliceShow = show;
}

void VolumeRenderer::showYSlice(bool show)
{
    ySliceShow = show;
}

void VolumeRenderer::showZSlice(bool show)
{
    zSliceShow = show;
}

void VolumeRenderer::draw()
{
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    program.bind();

    cam.view = glm::lookAt(cam.eye, cam.at, cam.up);
    cam.proj = glm::perspective(glm::radians(cam.fovy), (float)cam.width/cam.height, cam.zNear, cam.zFar);
    auto mvp = cam.proj * cam.view * cam.model;

    program.setUniformValue("mvp", QMatrix4x4(glm::value_ptr(glm::transpose(mvp))));

    program.setUniformValue("valueSampler",0);
    program.setUniformValue("lutSampler", 1);
    program.setUniformValueArray("colorMarkers",&lutMarkers[0],6,1);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_1D, lutTexture);

    glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(VAO);

    if(xSliceShow)
    {
        glBindVertexBuffer(0,xSliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, xSliceTexture);
        program.setUniformValue("minValue",xSliceMinVal);
        program.setUniformValue("maxValue",xSliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(ySliceShow)
    {
        glBindVertexBuffer(0,ySliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, ySliceTexture);
        program.setUniformValue("minValue",ySliceMinVal);
        program.setUniformValue("maxValue",ySliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(zSliceShow)
    {
        glBindVertexBuffer(0,zSliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, zSliceTexture);
        program.setUniformValue("minValue",zSliceMinVal);
        program.setUniformValue("maxValue",zSliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    glBindVertexArray(0);
}

void VolumeRenderer::updateVBO(const std::vector<glm::vec3>& vertices, GLuint vboID)
{
    std::vector< vertex > vbo;
    vbo.reserve( 4 );

    vbo.push_back({vertices[0], {0.0f,0.0f}});
    vbo.push_back({vertices[1], {1.0f,0.0f}});
    vbo.push_back({vertices[2], {0.0f,1.0f}});
    vbo.push_back({vertices[3], {1.0f,1.0f}});

    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, vbo.size()*sizeof(vertex), &vbo[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VolumeRenderer::updateTexture(float* slice, unsigned int width,
    unsigned int height, GLuint textureID)
{
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, width, height,
                  0, GL_RED, GL_FLOAT, slice );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D,0);
}

void VolumeRenderer::updateColorTableTexture(float* colorTable, unsigned int numColors)
{
    glActiveTexture(GL_TEXTURE1);

    glBindTexture(GL_TEXTURE_1D, lutTexture);

    glTexImage1D( GL_TEXTURE_1D, 0, GL_RGB, numColors,
                  0, GL_RGB, GL_FLOAT, colorTable );

    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_1D,0);
}
