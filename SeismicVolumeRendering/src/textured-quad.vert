#version 400 core

layout( location = 0 ) in vec3 vertexPos;
layout( location = 1 ) in vec2 vertexTex;

uniform mat4 mvp;

out vec2 uv;

void main()
{
    gl_Position = mvp * vec4( vertexPos, 1 );

    uv = vertexTex;
}
