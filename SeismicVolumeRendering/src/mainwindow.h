#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void enableInlineSliceOptions(bool enable=true);
    void enableCrosslineSliceOptions(bool enable=true);
    void enableDepthSliceOptions(bool enable=true);

private slots:
    void on_inlineSpinBox_valueChanged(int value);
    void on_crosslineSpinBox_valueChanged(int value);
    void on_depthSpinBox_valueChanged(int value);

    void on_showInlineCheckbox_clicked(bool checked);

    void on_showCrosslineCheckbox_clicked(bool checked);

    void on_showDepthSliceCheckbox_clicked(bool checked);

    void on_horizonsListWidget_itemChanged(QListWidgetItem *item);

    void on_inlineStepSpinBox_valueChanged(int value);

    void on_crosslineStepSpinBox_valueChanged(int value);

    void on_depthStepSpinBox_valueChanged(int value);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
