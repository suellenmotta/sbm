#include "renderwidget.h"

#include <QMouseEvent>

#include "surfacereader.h"

#include <glm/ext.hpp>

RenderWidget::RenderWidget(QWidget *parent)
    : QOpenGLWidget(parent)
    , moving(false)
{
    info.firstInline    = 1499;
    info.firstCrossline = 1499;
    info.firstDepth     = 0;
    info.stepInline     = 6;
    info.stepCrossline  = 6;
    info.stepDepth      = 20;
    info.lastInline     = 8507;
    info.lastCrossline  = 7505;
    info.lastDepth      = 15000;
    info.p1 = {2490,2490};
    info.p2 = {32520,2490};
    info.p3 = {32520,37530};
    info.p4 = {2490,37530};

    colors.push_back({0.5019607843, 0, 0});
    colors.push_back({0, 0.5098039216, 0.7843137255});
    colors.push_back({0.2352941176, 0.7058823529, 0.2941176471});
    colors.push_back({1, 0.8823529412, 0.09803921569});
    colors.push_back({0.9607843137, 0.5098039216, 0.1882352941});
    colors.push_back({0.568627451, 0.1176470588, 0.7058823529});
    colors.push_back({0.2745098039, 0.9411764706, 0.9411764706});
    colors.push_back({0.9411764706, 0.1960784314, 0.9019607843});
    colors.push_back({0.8235294118, 0.9607843137, 0.2352941176});
    colors.push_back({0.9803921569, 0.7450980392, 0.7450980392});
    colors.push_back({0, 0.5019607843, 0.5019607843});
    colors.push_back({0.9019607843, 0.7450980392, 1});
    colors.push_back({0.9019607843, 0.09803921569, 0.2941176471});
    colors.push_back({0.6666666667, 0.431372549, 0.1568627451});
    colors.push_back({1, 0.9803921569, 0.7843137255});
    colors.push_back({0.6666666667, 1, 0.7647058824});
    colors.push_back({0.5019607843, 0.5019607843, 0});
    colors.push_back({1, 0.8431372549, 0.7058823529});
    colors.push_back({0, 0, 0.5019607843});
    colors.push_back({0.5019607843, 0.5019607843, 0.5019607843});
    colors.push_back({1, 1, 1});
    colors.push_back({0, 0, 0});
}


RenderWidget::~RenderWidget()
{
    delete surfaceProgram;

    for(auto& surfaceVBO : surfaceVBOs)
        glDeleteBuffers(1, &surfaceVBO);

    for(auto& surfaceEBO : surfaceEBOs)
        glDeleteBuffers(1, &surfaceEBO);
}

void RenderWidget::setupSeismicScene()
{
#ifdef __linux__
    NewVolume* volume = seismic::readSeismicVolume("/local/suellen/seismicdata/SEAM-full.dat",info);
#elif _WIN32
    NewVolume* volume = seismic::readSeismicVolume("C:/Users/suellen/Google Drive/SEAM-full.dat",info);
#endif

    volumeRenderer.init();
    volumeRenderer.setWindowSize(width(),height());
    volumeRenderer.setVolume(volume);

    updateDepthSlice(info.lastDepth);

    surfaceProgram = new QOpenGLShaderProgram();
    surfaceProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/blinn-phong.vert");
    surfaceProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/blinn-phong.frag");
    surfaceProgram->link();

    glGenVertexArrays(1, &surfaceVAO);
    glBindVertexArray(surfaceVAO);
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
    glVertexAttribBinding(0,0);
    glVertexAttribBinding(1,0);
    glBindVertexArray(0);

#ifdef __linux__
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa1.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa2.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa3.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa4.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa5.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa6.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa7.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa8.off");
    addSurface("/local/suellen/seismicdata/SEAMhorizons/mapa9.off");
#elif _WIN32
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa1.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa2.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa3.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa4.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa5.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa6.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa7.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa8.off");
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/mapa9.off");
#endif

    surfaceColors = std::vector<glm::vec3>(colors.begin(),colors.begin()+9);
}

void RenderWidget::setupBabyGEScene()
{
#ifdef __linux__
    NewVolume* volume = readNRRD("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.nrrd");
#elif _WIN32
    NewVolume* volume = readNRRD("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.nrrd");
#endif
    volumeRenderer.init();
    volumeRenderer.setWindowSize(width(),height());
    volumeRenderer.setVolume(volume);

    surfaceProgram = new QOpenGLShaderProgram();
    surfaceProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/blinn-phong.vert");
    surfaceProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/blinn-phong.frag");
    surfaceProgram->link();

    glGenVertexArrays(1, &surfaceVAO);
    glBindVertexArray(surfaceVAO);
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
    glVertexAttribBinding(0,0);
    glVertexAttribBinding(1,0);
    glBindVertexArray(0);

#ifdef __linux__
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.off");
#elif _WIN32
    addSurface("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.off");
#endif

    surfaceColors.push_back(glm::vec3(0.45f,0.45f,0.45f));
}

void RenderWidget::getVolumeBounds(
        unsigned int& firstInline,
        unsigned int& lastInline,
        unsigned int& stepInline,
        unsigned int& firstCrossline,
        unsigned int& lastCrossline,
        unsigned int& stepCrossline,
        unsigned int& firstDepthSlice,
        unsigned int& lastDepthSlice,
        unsigned int& stepDepthSlice)
{
    firstInline = info.firstInline;
    lastInline = info.lastInline;
    stepInline = info.stepInline;
    firstCrossline = info.firstCrossline;
    lastCrossline = info.lastCrossline;
    stepCrossline = info.stepCrossline;
    firstDepthSlice = info.firstDepth;
    lastDepthSlice = info.lastDepth;
    stepDepthSlice = info.stepDepth;
}



void RenderWidget::addSurface(const std::string& filePath)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<unsigned int> indices;

    if(readOffFile(filePath,vertices,indices))
    {
        normals.resize(vertices.size(),glm::vec3(0,0,0));
        for(unsigned int i = 0; i < indices.size()-2; i+=3)
        {
            auto i0 = indices[i+0];
            auto i1 = indices[i+1];
            auto i2 = indices[i+2];

            auto u = vertices[i1]-vertices[i0];
            auto v = vertices[i2]-vertices[i0];

            auto n = glm::cross(u,v);

            normals[i0] += n;
            normals[i1] += n;
            normals[i2] += n;
        }

        struct VertexType
        {
            glm::vec3 pos;
            glm::vec3 normal;
        };

        std::vector<VertexType> vertexBuffer;
        vertexBuffer.reserve(vertices.size());
        for(auto i = 0; i < vertices.size(); ++i)
        {
            vertexBuffer.push_back({vertices[i],glm::normalize(normals[i])});
        }

        GLuint surfaceVBO, surfaceEBO;

        glGenBuffers(1, &surfaceVBO);
        glBindBuffer(GL_ARRAY_BUFFER, surfaceVBO);
        glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size()*sizeof(VertexType), &vertexBuffer[0], GL_DYNAMIC_DRAW);

        glGenBuffers(1, &surfaceEBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surfaceEBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

        surfaceVBOs.push_back(surfaceVBO);
        surfaceEBOs.push_back(surfaceEBO);
        numElements.push_back((GLsizei)indices.size());
        surfaceShow.push_back(true);
    }
}

void RenderWidget::updateInline(unsigned int inlineNum)
{
    auto index = (inlineNum-info.firstInline)/info.stepInline;
    volumeRenderer.updateZSlice(index);
}

void RenderWidget::updateCrossline(unsigned int crosslineNum)
{
    auto index = (crosslineNum-info.firstCrossline)/info.stepCrossline;
    volumeRenderer.updateYSlice(index);
}

void RenderWidget::updateDepthSlice(unsigned int depthSliceNum)
{
    auto index = (depthSliceNum-info.firstDepth)/info.stepDepth;
    volumeRenderer.updateXSlice(index);
}

void RenderWidget::showInline(bool show)
{
    volumeRenderer.showZSlice(show);
}

void RenderWidget::showCrossline(bool show)
{
    volumeRenderer.showYSlice(show);
}

void RenderWidget::showDepthSlice(bool show)
{
    volumeRenderer.showXSlice(show);
}

void RenderWidget::showSurface(unsigned int index, bool show)
{
    if(index<surfaceShow.size())
        surfaceShow[index]=show;
}

void RenderWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    glClearColor(0,0,0,1);
    glViewport(0,0,width(),height());

    setupSeismicScene();
//    setupBabyGEScene();

}


void RenderWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    volumeRenderer.draw();

    auto cam = volumeRenderer.getCamera();

    QMatrix4x4 mv(glm::value_ptr(glm::transpose(cam.view * cam.model)));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(cam.proj)));

    surfaceProgram->bind();
    surfaceProgram->setUniformValue("mv", mv);
    surfaceProgram->setUniformValue("mv_ti", mv.inverted().transposed());
    surfaceProgram->setUniformValue("mvp", p*mv);

    surfaceProgram->setUniformValue("light.position", QVector3D(0.0f,0.0f,0.0f) );
    surfaceProgram->setUniformValue("material.ambient", QVector3D(0.1f, 0.1f, 0.1f));
    surfaceProgram->setUniformValue("material.specular", QVector3D(0.25f, 0.25f, 0.25f));
    surfaceProgram->setUniformValue("material.shininess", 32.0f);

    glBindVertexArray(surfaceVAO);

    for(auto i=0; i < surfaceVBOs.size(); i++)
    {
        if(surfaceShow[i])
        {
            glBindVertexBuffer(0,surfaceVBOs[i],0,2*sizeof(glm::vec3));

            auto color = surfaceColors[i%colors.size()];
            surfaceProgram->setUniformValue("material.diffuse", color.r, color.g, color.b);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, surfaceEBOs[i]);
            glDrawElements(GL_TRIANGLES, numElements[i], GL_UNSIGNED_INT,0);
        }
    }

    glBindVertexArray(0);
}


void RenderWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);

    volumeRenderer.setWindowSize(w,h);
}


void RenderWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        oldPos = glm::ivec2(event->x(), height()-event->y());
        moving = true;
    }
}


void RenderWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        moving = false;
    }
}


void RenderWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(moving && event->buttons() & Qt::LeftButton)
    {
        glm::ivec2 newPos(event->x(), height()-event->y());

        volumeRenderer.rotate(oldPos,newPos);
        update();
        oldPos = newPos;
    }
}


void RenderWidget::wheelEvent(QWheelEvent *event)
{
    volumeRenderer.zoom(event->delta());
    update();
}

