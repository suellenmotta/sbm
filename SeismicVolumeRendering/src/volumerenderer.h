#ifndef VOLUMERENDERER_H
#define VOLUMERENDERER_H


#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>

#include "newvolume.h"
#include "arcball.h"

class VolumeRenderer
    : public QOpenGLExtraFunctions
{
public:
    VolumeRenderer();
    virtual ~VolumeRenderer();

    void init();
    void setVolume(NewVolume* volume);
    void setWindowSize(int width, int height);

    void focus();
    void rotate(const glm::ivec2 &oldScreenPos, const glm::ivec2 &newScreenPos);
    void zoom(float delta);

    void updateXSlice(unsigned int index);
    void updateYSlice(unsigned int index);
    void updateZSlice(unsigned int index);

    void showXSlice(bool show = true);
    void showYSlice(bool show = true);
    void showZSlice(bool show = true);

    void draw();

    struct Camera {
       glm::vec3 eye;      /* posicao do olho ou centro de projecao conica */
       glm::vec3 at;       /* ponto para onde a camera aponta              */
       glm::vec3 up;       /* orientacao da direcao "para cima" da camera  */
       float fovy;         /* angulo de abertura da camera                 */
       float zNear,zFar;   /* distancia do plano proximo e distante        */
       float width,height; /* largura e altura da janela em pixels         */

       glm::mat4x4 model;
       glm::mat4x4 view;
       glm::mat4x4 proj;
    };

    const Camera& getCamera() { return cam; }

protected:
    void updateVBO(const std::vector<glm::vec3>& vertices, GLuint vboID);
    void updateTexture(float* slice, unsigned int width,
                       unsigned int height, GLuint textureID);
    void updateColorTableTexture(float* colorTable, unsigned int numColors);

    QOpenGLShaderProgram program;

    GLuint VAO;

    GLuint xSliceVBO;
    GLuint ySliceVBO;
    GLuint zSliceVBO;

    GLuint xSliceTexture;
    GLuint ySliceTexture;
    GLuint zSliceTexture;

    bool xSliceShow;
    bool ySliceShow;
    bool zSliceShow;

    float xSliceMinVal, xSliceMaxVal;
    float ySliceMinVal, ySliceMaxVal;
    float zSliceMinVal, zSliceMaxVal;

    NewVolume* volume;

    Camera cam;

    Arcball arcball;

    std::vector<float> lutMarkers;
    GLuint lutTexture;



};

#endif // VOLUMERENDERER_H
