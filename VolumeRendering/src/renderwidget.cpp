#include "renderwidget.h"

#include <QImage>
#include <QGLWidget>
#include <QMouseEvent>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "arcball.h"

#include <cmath>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


RenderWidget::RenderWidget(QWidget *parent)
    : QOpenGLWidget(parent)
    , program(nullptr)
    , arcball(nullptr)
    , moving(false)
{
    cam.at = glm::vec3(0.f,0.f,0.f);
    cam.eye = cam.at;
    cam.eye.z = cam.at.z + 20.f;
    cam.up = glm::vec3(0.f,1.f,0.f);
    cam.zNear = 0.1f;
    cam.zFar  = 100.f;
    cam.fovy  = 60.f;
    cam.width = width();
    cam.height = height();

    auto ze = glm::normalize(cam.eye-cam.at);
    auto xe = glm::normalize(cam.up);
    auto ye = glm::normalize(glm::cross(ze,xe));

    arcball = arcballInit(cam.width/2,cam.height/2,600,xe,ye,ze);
}


RenderWidget::~RenderWidget()
{
    delete program;

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    arcballDestroi(arcball);
}


void RenderWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glEnable(GL_DEPTH_TEST);

    glClearColor(0,0,0,1);
    glPointSize(3.0f);
    glViewport(0,0,width(),height());

    //Compilar os shaders
    program = new QOpenGLShaderProgram();
    program->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/vertexshader.glsl");
    program->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/fragmentshader.glsl");
    program->link();

    createSphere();
    createTexture(":/textures/sphere_texture.jpg");

    //Criar VBO e VAO
    createVBO();
}


void RenderWidget::paintGL()
{
    //Limpar a tela
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    //Linkar o VAO
    glBindVertexArray(VAO);

    //Linkar o programa e passar as uniformes
    program->bind();

    //Definir matriz view e projection
    view = glm::lookAt(cam.eye, cam.at, cam.up);
    proj = glm::perspective(glm::radians(cam.fovy), (float)cam.width/cam.height, cam.zNear, cam.zFar);

    QMatrix4x4 m(glm::value_ptr(glm::transpose(model)));
    QMatrix4x4 v(glm::value_ptr(glm::transpose(view)));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(proj)));

    //Passar as uniformes da luz e do material
    program->setUniformValue("light.position", v*QVector3D(5,9,-5) );
    program->setUniformValue("material.ambient", QVector3D(0.1f,0.1f,0.1f));
    program->setUniformValue("material.diffuse", QVector3D(1.0f,1.0f,1.0f));
    program->setUniformValue("material.specular", QVector3D(1.0f,1.0f,1.0f));
    program->setUniformValue("material.shininess", 24.0f);

    //Ativar e linkar a textura
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    program->setUniformValue("sampler", 0);

    //Passar as matrizes de transformação
    for( int x = -3; x <= 3; x+=3 )
    {
        for( int z = -3; z <= 3; z+=3)
        {
            QMatrix4x4 mObj;
            mObj.translate(x,0,z);

            QMatrix4x4 mv = v * (m * mObj);
            QMatrix4x4 mvp = p * mv;
            program->setUniformValue("mv", mv);
            program->setUniformValue("mv_ti", mv.inverted().transposed());
            program->setUniformValue("mvp", mvp);

            //Desenhar
            glDrawArrays(GL_POINTS,0,vertices.size());
        }
    }
}


void RenderWidget::resizeGL(int w, int h)
{
    //Atualizar a viewport
    glViewport(0,0,w,h);

    //Atualizar a câmera
    cam.width = w;
    cam.height = h;

    arcball->center_x = cam.width/2;
    arcball->center_y = cam.height/2;
}


void RenderWidget::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        oldPos = QPoint(event->x(), cam.height-event->y());
        moving = true;
    }
}


void RenderWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        moving = false;
    }
}


void RenderWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(moving && event->buttons() & Qt::LeftButton)
    {
        QPoint newPos(event->x(), cam.height-event->y());

        if(newPos.x()!=oldPos.x() || newPos.y()!=oldPos.y())
        {
            glm::quat q = arcballRotQuat(arcball,
                                         glm::ivec2(oldPos.x(),oldPos.y()),
                                         glm::ivec2(newPos.x(),newPos.y()));
            auto Rot = glm::mat4_cast(q);
            model = Rot * model;

            update();

            oldPos = newPos;
        }
    }
}


void RenderWidget::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0)
        cam.eye.z *= 0.9f;
    else
        cam.eye.z *= 1.1f;

    update();
}


int getIndex( int i, int j, int n )
{
    return j + i * ( n + 1 );
}
void RenderWidget::createSphere()
{
    const int n = 100;
    const int m = 100;

    const int numTriangles = 2 * n * m;
    const int numVertices = ( n + 1 ) * ( m + 1 );

    for( unsigned int i = 0; i <= n; i++ )
    {
        for( unsigned int j = 0; j <= m; j++ )
        {
            //Atualizar as coordenadas de textura
            float s = (float) i / n;
            float t = (float) j / m;
            texCoords.push_back(glm::vec2(s,t));

            //Calcula os parâmetros
            double theta = 2 * s * M_PI;
            double phi = t * M_PI;
            double sinTheta = sin( theta );
            double cosTheta = cos( theta );
            double sinPhi = sin( phi );
            double cosPhi = cos( phi );

            //Calcula os vértices == equacao da esfera
            vertices.push_back( glm::vec3(cosTheta * sinPhi,
                                          cosPhi,
                                          sinTheta * sinPhi) );
        }
    }

    normals = vertices;

    indices.resize(numTriangles*3);

    //Preenche o vetor com a triangulação
    unsigned int k = 0;
    for( unsigned int i = 0; i < n; i++ )
    {
        for( unsigned int j = 0; j < m; j++ )
        {
            indices[ k++ ] = getIndex( i + 1, j, n );
            indices[ k++ ] = getIndex( i + 1, j + 1, n );
            indices[ k++ ] = getIndex( i, j, n );


            indices[ k++ ] = getIndex( i + 1, j + 1, n );
            indices[ k++ ] = getIndex( i, j + 1, n );
            indices[ k++ ] = getIndex( i, j, n );
        }
    }
}


void RenderWidget::createVBO()
{
    //Construir vetor do vbo
    //OBS: Os dados já poderiam estar sendo armazenados assim na classe.
    struct vertex
    {
        glm::vec3 pos;
        glm::vec3 normal;
        glm::vec2 texCoord;
    };

    std::vector< vertex > vbo;
    vbo.reserve( vertices.size() );
    for( unsigned int i = 0; i < vertices.size(); i++ )
    {
        vbo.push_back({vertices[i], normals[i], texCoords[i]});
    }

    //Criar VBO, linkar e copiar os dados
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vbo.size()*sizeof(vertex), &vbo[0], GL_STATIC_DRAW);

    //Criar EBO, linkar e copiar os dados
//    glGenBuffers(1, &EBO);
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    //Criar VAO, linkar e definir layouts
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    //Habilitar, linkar e definir o layout dos buffers
    glBindBuffer( GL_ARRAY_BUFFER, VBO );

    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE,
                           sizeof(vertex),
                           (void*)0 );

    glEnableVertexAttribArray( 1 );
    glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE,
                           sizeof(vertex),
                           (void*)sizeof(glm::vec3) );

    glEnableVertexAttribArray( 2 );
    glVertexAttribPointer( 2, 2, GL_FLOAT, GL_FALSE,
                           sizeof(vertex),
                           (void*)(2*sizeof(glm::vec3)) );

    //Linkar o EBO
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
}


void RenderWidget::createTexture(const std::string& imagePath)
{
    //Criar a textura
    glGenTextures(1, &textureID);
    
    //Linkar (bind) a textura criada
    glBindTexture(GL_TEXTURE_2D, textureID);
    
    //Abrir arquivo de imagem com o Qt
    QImage texImage = QGLWidget::convertToGLFormat(QImage(imagePath.c_str()));

    //Enviar a imagem para o OpenGL
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA,
                 texImage.width(), texImage.height(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, texImage.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);
}
