/* Implementa��es do modulo ARCBALL
 *
 * Autor: Marcelo Gattass
 * Data: 15 de novembro de 2005
 *
 * O modulo contem duas implementacoes quase equivalentes:
 * (a) uma com base na matriz de rotacao em torno de um eixo qualquer;
 * (b) outra com base de quaternios.
 *
 * A unica diferenca e' qua a de quaternios roda o dobro para o mesmo
 * movimento de mouse.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glm/gtc/quaternion.hpp>

#include "arcball.h"


Arcball* arcballInit(int xc, int yc, int radius, glm::vec3 ex, glm::vec3 ey, glm::vec3 ez )
{
   Arcball* arc=(Arcball*) malloc(sizeof(Arcball));
   arc->center_x = (float)xc;
   arc->center_y = (float)yc;
   arc->radius = (float)radius;
   arc->ex=ex;
   arc->ey=ey;
   arc->ez=ez;
   return arc;
}



void arcballDestroi(Arcball* arc) {
   if (arc) free(arc);
}


static glm::quat screenToArcsphere(Arcball *arc, int x, int y)
{
    float vx, vy, vz, r;
    float s;

    vx = ( x - arc->center_x) / arc->radius;
    vy = ( y - arc->center_y) / arc->radius;

    r = vx * vx + vy * vy;

    if (r > 1)
    {
        s = 1/sqrt(r);
        vx *= s;
        vy *= s;
        vz = 0.0;
    }
    else
        vz = sqrt(1 - r);

    return glm::quat(0.0f, vx, vy, vz);
}


glm::quat arcballRotQuat(Arcball *arc, glm::ivec2 p0, glm::ivec2 p1)
{
    auto q0 = screenToArcsphere(arc, p0.x, p0.y);
    auto q1 = screenToArcsphere(arc, p1.x, p1.y);

    glm::quat q = q1*glm::conjugate(q0);
    glm::mat3 Rt = glm::mat3_cast(q);
    arc->ex=Rt*arc->ex;
    arc->ey=Rt*arc->ey;
    arc->ez=Rt*arc->ez;

    return q;
}
