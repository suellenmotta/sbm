/* Declaracoes do modulo ARCBALL
 *
 * Autor: Marcelo Gattass
 * Data: 20 de novembro de 2005
 * O modulo contem duas implementacoes quase equivalentes: 
 * (a) uma com base na matriz de rotacao em torno de um eixo qualquer;
 * (b) outra com base de quaternios.
 *
 * A unica diferenca entre os resultados duas e' qua a de quaternios 
 * roda o dobro para o mesmo movimento de mouse.
 * 
 */
#ifndef ARCBALL_H
#define ARCBALL_H

#include <glm/glm.hpp>

struct Arcball {
  /* Centro do arcball (em coordenadas do canvas) */
  float center_x, center_y;

  /* Raio do arcball  (em coordenadas do canvas) */
  float radius;

  /* screen x y and z glm::vec3s in object space */
  glm::vec3 ex,ey,ez;
};

/* 
 * Cria uma instancia do arcball centrado na posicao (xc,yc), de raio radius num canvas
 * e ajusta os eixos de rotacao de forma a acomodar possiveis diferencas entre os eixos
 * da tela e dos objetos.
 * Os eixos {ex},{ey} e {ez} sao os vetores x,y e z da camera escritos no sistema de 
 * coordenadas dos objetos 
 */
Arcball* arcballInit(int xc, int yc, int radius, glm::vec3 ex, glm::vec3 ey, glm::vec3 ez );

/*
 * Rota��o livre, as coordenadas do canvas (x0,y0) e (x1,y1) tem origem no canto inferior esquerdo
 * Implementa as rotacoes como uso de quaternios.
 * Semelhante a funcao "arcballRotMatrix" com a diferenca que o angulo de rotacao e' o dobro.
 */
glm::quat arcballRotQuat(Arcball *arc, glm::ivec2 p0, glm::ivec2 p1);

/* 
 * Rota��o livre, as coordenadas do canvas (x0,y0) e (x1,y1) tem origem no canto inferior esquerdo 
 * Implementa as rotacoes como uso da matriz de rotacao em torno de um eixo qualquer.
 * Semelhante a funcao "arcballRotMatrix" com a diferenca que o angulo de rotacao e' a metade.
 */
//glm::mat4 arcballRotMatrix(Arcball* arc, int x0, int y0, int x1, int y1);


/* Deleta um arcball */
void arcballDestroi(Arcball* arc);


#endif

