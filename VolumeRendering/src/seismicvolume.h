#ifndef SEISMICVOLUME_H
#define SEISMICVOLUME_H

namespace seismic
{

struct VolumeInfo
{
    unsigned int firstInline;
    unsigned int firstCrossline;
    unsigned int firstDepth;
    unsigned int stepInline;
    unsigned int stepCrossline;
    unsigned int stepDepth;
    unsigned int numInlines;
    unsigned int numCrosslines;
    unsigned int numDepthSlices;
};


class Volume
{
public:
    Volume(const VolumeInfo& info);
    Volume(const VolumeInfo& info, float* buffer);
    virtual ~Volume();

    float* inlineSlice(unsigned int inlineNum);
    float* crosslineSlice(unsigned int crosslineNum);
    float* depthSlice(unsigned int depthSliceNum);

private:
    unsigned int inlineIdx(unsigned int inlineNum);
    unsigned int crosslineIdx(unsigned int crosslineNum);
    unsigned int depthIdx(unsigned int depthSliceNum);

    VolumeInfo info;
    float* buffer;
};

/**
 * @brief Reads seismic volume written in OpendTect simple (flat) 3d file format.
 * @param filePath
 * @param volume Seismic volume read from file
 * @return true if successfully read
 */
bool read(const std::string& filePath, Volume& volume);

/**
 * @brief Writes seismic volume file in the same format described in read function.
 * @param filePath
 * @param volume
 * @return true if successfully written
 */
bool write(const std::string& filePath, const Volume& volume){return false;}

}
#endif // SEISMICVOLUME_H
