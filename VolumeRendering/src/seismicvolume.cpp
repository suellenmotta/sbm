#include "seismicvolume.h"

namespace seismic
{


Volume::Volume(const VolumeInfo& info)
    : info(info)
    , buffer(nullptr)
{
    auto size = info.numInlines*info.numCrosslines*info.numDepthSlices;
    if(size>0) buffer = new float[size];
}

Volume::Volume(const VolumeInfo& info, float* buffer)
    : info(info)
    , buffer(buffer)
{

}

Volume::~Volume()
{
    if(buffer)
        delete[] buffer;
    buffer=nullptr;
}

float* Volume::inlineSlice(unsigned int inlineNum)
{
    auto ilIdx = inlineIdx(inlineNum);
    auto sliceSize = info.numCrosslines*info.numDepthSlices;

    auto firstPos=ilIdx*sliceSize;
    auto lastPos=firstPos+sliceSize;

    float* slice = new float[sliceSize];
    for(auto xlIdx=firs; xlIdx<info.numCrosslines; ++xlIdx)
    {

    }

    return slice;
}

float* Volume::crosslineSlice(unsigned int crosslineNum)
{

}

float* Volume::depthSlice(unsigned int depthSliceNum)
{

}

unsigned int Volume::inlineIdx(unsigned int inlineNum)
{
    return
        (inlineNum-info.firstInline)/info.stepInline;
}

unsigned int Volume::crosslineIdx(unsigned int crosslineNum)
{
    return
        (crosslineNum-info.firstCrossline)/info.stepCrossline;
}

unsigned int Volume::depthIdx(unsigned int depthSliceNum)
{
    return
        (depthSliceNum-info.firstDepth)/info.stepDepth;
}

}
