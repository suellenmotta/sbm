#include "renderareawidget.h"

#include <QMouseEvent>
#include <QMatrix4x4>
#include <QtMath>
#include <QMessageBox>

#include <iostream>

const char* vertexShaderSource = R"(
    #version 400 core

    layout( location = 0 ) in vec3 vertexPos;
    layout( location = 1 ) in vec3 vertexTexS;
    uniform mat4 transformMatrix;

    void main()
    {
        gl_Position = transformMatrix * vec4( vertexPos, 1 );
    }
)";


const char* geometryShaderSource = R"(
    #version 400 core
    layout (triangles) in;
    layout (triangle_strip, max_vertices = 3) out;

    out vec3 S;

    void main()
    {
        S.x = 1.0;
        S.y = 1.0;
        S.z = 0.0;

        gl_Position = gl_in[0].gl_Position;
        EmitVertex();

        S.x = 0.0;
        S.y = 1.0;
        S.z = 1.0;

        gl_Position = gl_in[1].gl_Position;
        EmitVertex();

        S.x = 1.0;
        S.y = 0.0;
        S.z = 1.0;

        gl_Position = gl_in[2].gl_Position;
        EmitVertex();

        EndPrimitive();
    }
)";


const char* fragmentShaderSource = R"(
    #version 400 core

    uniform vec4 surfaceColor;
    uniform vec4 wireframeColor;
    uniform sampler1D wireframeTexture;

    in vec3 S;
    out vec4 finalColor;

    void main()
    {
        float s1 = texture(wireframeTexture, S.x).r;
        float s2 = texture(wireframeTexture, S.y).r;
        float s3 = texture(wireframeTexture, S.z).r;

        finalColor = surfaceColor;
//        finalColor = mix( finalColor, wireframeColor, s1 );
//        finalColor = mix( finalColor, wireframeColor, s2 );
//        finalColor = mix( finalColor, wireframeColor, s3 );
    }
)";


RenderAreaWidget::RenderAreaWidget(QWidget* parent)
    : QOpenGLWidget(parent),
      program(nullptr),
      ibo(QOpenGLBuffer::IndexBuffer),
      wireframeTexture(0)
{

}


RenderAreaWidget::~RenderAreaWidget()
{
    makeCurrent();
    vbo.destroy();
    ibo.destroy();
    delete program;
    doneCurrent();
}


void RenderAreaWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0.5f, 0.5f, 0.5f, 1.f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);

    glViewport(0,0,width(),height());

    program = new QOpenGLShaderProgram;
    program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
//    program->addShaderFromSourceCode(QOpenGLShader::Geometry, geometryShaderSource);
    program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    program->link();
    if (!program->isLinked())
    {
        QMessageBox::critical(nullptr,"Erro","Não foi possível compilar/linkar os shaders.");
        exit(0);
    }

    program->bind();

    view.lookAt(QVector3D(0,0,120),QVector3D(0,0,0),QVector3D(0,1,0));
    proj.perspective(qDegreesToRadians(60.0f),(float)width()/height(),0.1f,500.0f);

    std::vector< glm::vec3 > vertices =
    {
        {-1,1,0}, {0,1,0},       {1,1,0},      {-0.5,0.5,0}, {0.5,0.5,0}, {-1,0,0},
        { 1,0,0}, {-0.5,-0.5,0}, {0.5,-0.5,0}, {-1,-1,0},    {0,-1,0},    {1,-1,0}
    };

    std::vector< unsigned int > indices =
    {
        0,5,3,   0,3,1,
        1,3,4,   1,4,2,
        4,6,2,   5,7,3,
        4,8,6,   5,9,7,
        7,9,10,  7,10,8,
        8,10,11, 6,8,11
    };

    CornerTable* mesh = createCornerTable( vertices, indices );

    vbo.create();
    ibo.create();

    vbo.bind();
    vbo.allocate( &vertices[0], (int)vertices.size()*sizeof(glm::vec3) );

    ibo.bind();
    ibo.allocate( &indices[0], (int)indices.size()*sizeof(unsigned int));

    createWireframeTexture(3);

    computeBorders();
}


void RenderAreaWidget::paintGL()
{
    glDisable(GL_DEPTH_TEST);

    program->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(vbo.isCreated())
    {
        program->setUniformValue("transformMatrix", proj*view);

        program->setUniformValue("surfaceColor", QVector4D(0.6f,0,0,1));
        program->setUniformValue("wireframeColor", QVector4D(1,1,1,1));

        vbo.bind();
        program->enableAttributeArray(0);
        program->setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(QVector3D));

        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_1D, wireframeTexture );
        program->setUniformValue("wireframeTexture",0);

        ibo.bind();
        glDrawElements( GL_TRIANGLES,36,GL_UNSIGNED_INT,0 );

        glPointSize(5.0f);
        for(unsigned int i = 0; i < borders.size(); ++i)
        {
            borders[i].bind();
            program->setUniformValue("surfaceColor", QVector4D(0.0f,0.0f,0.6f,1));
            glDrawElements( GL_POINTS, numBorderPoints[i],GL_UNSIGNED_INT,0 );
        }
    }
}


void RenderAreaWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}


void RenderAreaWidget::mousePressEvent(QMouseEvent *)
{


}


void RenderAreaWidget::mouseMoveEvent(QMouseEvent *event)
{
    QVector3D point( event->x(), height()-event->y(), 0 );

    point = point.unproject( view, proj, QRect(0,0,width(),height()));
    point.setZ(0.f);

    QString posText = tr("X = %1, Y = %2").arg(point.x()).arg(point.y());
    emit updateMousePositionText(posText);
}


void RenderAreaWidget::mouseReleaseEvent(QMouseEvent *)
{
}


void RenderAreaWidget::createWireframeTexture(int thickness)
{
    glGenTextures(1,&wireframeTexture);
    glBindTexture(GL_TEXTURE_1D, wireframeTexture);

    int borderTexels = thickness / 2;
    int levels = 8;

    int size = pow(2,levels);
    std::vector< float > data( size, 0.0f );

    for(int i = size-1; i >= size-borderTexels && i>=0; i--)
        data[i] = 1.0f;

    if(size-borderTexels > 1)
        data[size-borderTexels-1] = 0.5f;

    for(int level=0, curSize=size; level<=levels; level++, curSize/=2)
    {
        glTexImage1D( GL_TEXTURE_1D, level, GL_RED, curSize,
                      0, GL_RED, GL_FLOAT, &data[size-curSize] );
    }

    //TODO: Avoid saturation

    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
}


void RenderAreaWidget::computeBorders()
{
    std::vector< unsigned int > indices =
    {
        3,8,4
    };

    borders.push_back(QOpenGLBuffer(QOpenGLBuffer::IndexBuffer));
    numBorderPoints.push_back(indices.size());

    auto border = borders.back();
    border.create();
    border.bind();
    border.allocate( &indices[0], (int)indices.size()*sizeof(unsigned int) );
}


CornerTable* RenderAreaWidget::createCornerTable(
        const std::vector< glm::vec3 >& vertices,
        const std::vector< unsigned int >& indices )
{
    double* dVertices = new double[vertices.size()*3];
    for( unsigned int i = 0; i < vertices.size(); i++ )
    {
        dVertices[i*3+0] = vertices[i].x;
        dVertices[i*3+1] = vertices[i].y;
        dVertices[i*3+2] = vertices[i].z;
    }

    CornerTable* mesh = new CornerTable( &indices[0],
            dVertices, indices.size()/3, vertices.size(), 3 );

    delete[] dVertices;



    return mesh;
}

