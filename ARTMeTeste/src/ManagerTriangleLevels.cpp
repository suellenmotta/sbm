/* 
 * File:   main.cpp
 * Author: jefersonlocal
 *
 * Created on January 26, 2015, 10:17 AM
 */

#include <stdlib.h>
#include <stdio.h>
#include "ManagerTriangleLevels.h"



void setTriangleLevels( LevelType *triangle, LevelType level, LevelType sublevel )
{
    (*triangle) = (level << 2) | sublevel;
}



void setTriangleSubLevel( LevelType *triangle, LevelType sublevel )
{
    (*triangle) = (((*triangle) & ~0x3) | (sublevel));
}



LevelType getLevel( const LevelType *triangle )
{
    return (*triangle) >> 2;
}



LevelType getSubLevel( const LevelType *triangle )
{
    return (*triangle)& 0x3;
}



void decrementSubLevel( LevelType *triangle )
{
    LevelType sublevel = getSubLevel( triangle );

    LevelType level = getLevel( triangle );

    level += (sublevel + 3) / 4 - 1;
    sublevel = (sublevel + 3) % 4;

    setTriangleLevels( triangle, level, sublevel );
}



void incrementSubLevel( LevelType *triangle, LevelType increment )
{
    LevelType sublevel = getSubLevel( triangle );

    LevelType level = getLevel( triangle );

    level += (sublevel + increment) / 4;
    sublevel = (sublevel + increment) % 4;

    setTriangleLevels( triangle, level, sublevel );
}



