#ifndef RENDERAREAWIDGET_H
#define RENDERAREAWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>

#include "glm/glm.hpp"
#include "CornerTable.h"

class RenderAreaWidget
    : public QOpenGLWidget
    , protected QOpenGLFunctions
{
Q_OBJECT

public:
    explicit RenderAreaWidget(QWidget* parent = 0);
    ~RenderAreaWidget();

    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

    void mousePressEvent(QMouseEvent *) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    void createWireframeTexture(int thickness);
    void computeBorders();

    CornerTable* createCornerTable( const std::vector< glm::vec3 >& vertices,
                                    const std::vector< unsigned int >& indices );

signals:
    void updateMousePositionText(const QString& message);

private:
    QOpenGLShaderProgram* program;
    QOpenGLBuffer vbo;
    QOpenGLBuffer ibo;

    QMatrix4x4 view;
    QMatrix4x4 proj;

    GLuint wireframeTexture;

    CornerTable* mesh;

    std::vector< QOpenGLBuffer > borders;
    std::vector< int > numBorderPoints;
};

#endif // RENDERAREAWIDGET_H
