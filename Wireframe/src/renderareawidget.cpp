#include "renderareawidget.h"

#include <QMouseEvent>
#include <QMatrix4x4>
#include <QtMath>
#include <QMessageBox>

#include <iostream>

const char* vertexShaderSource = R"(
    #version 400 core

    layout( location = 0 ) in vec3 vertexPos;
    layout( location = 1 ) in vec3 vertexTexS;
    uniform mat4 transformMatrix;

    void main()
    {
        gl_Position = transformMatrix * vec4( vertexPos, 1 );
    }
)";


const char* geometryShaderSource = R"(
    #version 400 core
    layout (triangles) in;
    layout (triangle_strip, max_vertices = 3) out;

    out vec3 S;

    void main()
    {
        S.x = 1.0;
        S.y = 1.0;
        S.z = 0.0;

        gl_Position = gl_in[0].gl_Position;
        EmitVertex();

        S.x = 0.0;
        S.y = 1.0;
        S.z = 1.0;

        gl_Position = gl_in[1].gl_Position;
        EmitVertex();

        S.x = 1.0;
        S.y = 0.0;
        S.z = 1.0;

        gl_Position = gl_in[2].gl_Position;
        EmitVertex();

        EndPrimitive();
    }
)";


const char* fragmentShaderSource = R"(
    #version 400 core

    uniform vec4 surfaceColor;
    uniform vec4 wireframeColor;
    uniform sampler1D wireframeTexture;

    in vec3 S;
    out vec4 finalColor;

    void main()
    {
        float s1 = texture(wireframeTexture, S.x).r;
        float s2 = texture(wireframeTexture, S.y).r;
        float s3 = texture(wireframeTexture, S.z).r;

        finalColor = surfaceColor;
        finalColor = mix( finalColor, wireframeColor, s1 );
        finalColor = mix( finalColor, wireframeColor, s2 );
        finalColor = mix( finalColor, wireframeColor, s3 );
    }
)";


RenderAreaWidget::RenderAreaWidget(QWidget* parent)
    : QOpenGLWidget(parent),
      program(nullptr),
      ibo(QOpenGLBuffer::IndexBuffer),
      wireframeTexture(0)
{

}


RenderAreaWidget::~RenderAreaWidget()
{
    makeCurrent();
    vbo.destroy();
    ibo.destroy();
    delete program;
    doneCurrent();
}


void RenderAreaWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0.5f, 0.5f, 0.5f, 1.f);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);

    glViewport(0,0,width(),height());

    program = new QOpenGLShaderProgram;
    program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    program->addShaderFromSourceCode(QOpenGLShader::Geometry, geometryShaderSource);
    program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    program->link();
    if (!program->isLinked())
    {
        QMessageBox::critical(nullptr,"Erro","Não foi possível compilar/linkar os shaders.");
        exit(0);
    }

    program->bind();

    view.lookAt(QVector3D(120.f, 90.f, 120.f),QVector3D(0,0,0),QVector3D(0,1,0));
    proj.perspective(qDegreesToRadians(60.0f),(float)width()/height(),0.001f,500.0f);

    program->setUniformValue("transformMatrix", proj*view);

    std::vector< QVector3D > vertices =
    {
        {-1,1,-1}, {1,1,-1}, {1,-1,-1}, {-1,-1,-1},
        {-1,1, 1}, {1,1, 1}, {1,-1, 1}, {-1,-1, 1}
    };

    std::vector< unsigned int > indices =
    {
        1,3,0,  2,3,1,
        7,5,4,  7,6,5,
        4,1,0,  4,5,1,
        2,7,3,  6,7,2,
        3,4,0,  7,4,3,
        5,2,1,  5,6,2
    };

    vbo.create();
    ibo.create();

    vbo.bind();
    vbo.allocate( &vertices[0], (int)vertices.size()*sizeof(QVector3D) );

    ibo.bind();
    ibo.allocate( &indices[0], (int)indices.size()*sizeof(unsigned int));

    createWireframeTexture(3);
}


void RenderAreaWidget::paintGL()
{
    program->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(vbo.isCreated())
    {
        program->setUniformValue("transformMatrix", proj*view);

        program->setUniformValue("surfaceColor", QVector4D(0.6f,0,0,1));
        program->setUniformValue("wireframeColor", QVector4D(1,1,1,1));

        vbo.bind();
        program->enableAttributeArray(0);
        program->setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(QVector3D));

        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_1D, wireframeTexture );
        program->setUniformValue("wireframeTexture",0);

        ibo.bind();
        glDrawElements( GL_TRIANGLES,36,GL_UNSIGNED_INT,0 );
    }
}


void RenderAreaWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}


void RenderAreaWidget::mousePressEvent(QMouseEvent *)
{

}


void RenderAreaWidget::mouseMoveEvent(QMouseEvent *event)
{
    QVector3D point( event->x(), height()-event->y(), 0 );

    point = point.unproject( view, proj, QRect(0,0,width(),height()));
    point.setZ(0.f);

    QString posText = tr("X = %1, Y = %2").arg(point.x()).arg(point.y());
    emit updateMousePositionText(posText);
}


void RenderAreaWidget::mouseReleaseEvent(QMouseEvent *)
{
}


void RenderAreaWidget::createWireframeTexture(int thickness)
{
    glGenTextures(1,&wireframeTexture);
    glBindTexture(GL_TEXTURE_1D, wireframeTexture);

    int borderTexels = thickness / 2;
    int levels = 8;

    int size = pow(2,levels);
    std::vector< float > data( size, 0.0f );

    for(int i = size-1; i >= size-borderTexels && i>=0; i--)
        data[i] = 1.0f;

    if(size-borderTexels > 1)
        data[size-borderTexels-1] = 0.5f;

    for(int level=0, curSize=size; level<=levels; level++, curSize/=2)
    {
        glTexImage1D( GL_TEXTURE_1D, level, GL_RED, curSize,
                      0, GL_RED, GL_FLOAT, &data[size-curSize] );
    }

    //TODO: Avoid saturation

    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
}

