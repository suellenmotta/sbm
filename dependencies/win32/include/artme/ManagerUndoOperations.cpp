/* 
 * File:   ManagerFaceLevels.h
 * Author: jefersonlocal
 *
 * Created on November 06, 2015, 12:47 PM
 */

#include <stdlib.h>
#include <stdio.h>
#include "ManagerUndoOperations.h"



void setOperation( CornerType *operation, CornerType corner, CornerType operationType )
{
    ( *operation ) = ( corner << 2 ) | operationType;
}



void setOperationType( CornerType *operation, CornerType operationType )
{
    ( *operation ) = ( ( *operation ) & ~0x3 ) | ( operationType );
}



void setCornerOperation( CornerType *operation, CornerType corner )
{
    ( *operation ) = ( corner << 2 ) | ( ( *operation )& 0x3 );
}



CornerType getCornerOperation( const CornerType *operation )
{
    return (*operation ) >> 2;
}



unsigned char getOperationType( const CornerType *operation )
{
    return (*operation )& 0x3;
}


