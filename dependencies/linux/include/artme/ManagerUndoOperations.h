/* 
 * File:   ManagerFaceLevels.h
 * Author: jefersonlocal
 *
 * Created on November 06, 2015, 12:47 PM
 */

#ifndef MANAGERUNDOOPERATIONS_H
#define	MANAGERUNDOOPERATIONS_H
#include  "DefinitionTypes.h"
#ifdef __cplusplus
extern "C"
{
#endif


    /**
     * Define the corner and the operation type.
     * @param operation - variable to store the informations of undo/redo
     * mechanism.
     * @param corner - corner where the operation must be executed.
     * @param operationType - operation type that must be executed.
     */
    void setOperation( CornerType *operation, CornerType corner, CornerType operationType );

    /**
     * Define the operation type.
     * @param operation - variable to store the informations of undo/redo
     * mechanism.
     * @param operationType - operation type that must be executed.
     */
    void setOperationType( CornerType *operation, CornerType operationType );

    /**
     * Define the corner to apply the operation.
     * @param operation - variable to store the informations of undo/redo
     * mechanism.
     * @param corner - corner where the operation must be executed.
     */
    void setCornerOperation( CornerType *operation, CornerType corner );

    /**
     * Get the corner to apply the operation.
     * @param operation - variable to store the informations of undo/redo
     * mechanism.
     * @return - corner to apply the operation.
     */
    CornerType getCornerOperation( const CornerType *operation );

    /**
     * Get the operation type.
     * @param operation operation - variable to store the informations of undo/redo
     * mechanism.
     * @return - operation type.
     */
    unsigned char getOperationType( const CornerType *operation );

#ifdef __cplusplus
}
#endif


#endif	/* MANAGERUNDOOPERATIONS_H */

