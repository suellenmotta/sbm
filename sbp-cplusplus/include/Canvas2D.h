#ifndef CANVAS2D_H
#define CANVAS2D_H

#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <glm/glm.hpp>
#include <vector>

#include "Image.h"

class Canvas2D
        : public QOpenGLWidget
        , protected QOpenGLExtraFunctions
{
public:
    Canvas2D(QWidget* parent);
    ~Canvas2D();

    void setImage(Image* image);
    void startPicking();
    void stopPicking();

    void setMinValue(float value);
    void setMaxValue(float value);

private:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

    void mouseDoubleClickEvent(QMouseEvent *) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    Image* image;

    std::vector< QVector3D > points;
    std::vector< QVector3D > curvePoints;
    std::vector< QVector3D > slicePoints;
    std::vector< unsigned int > indices;
    QVector3D previewPoint;

    QOpenGLShaderProgram* program;
    QOpenGLBuffer pointsBuffer;
    QOpenGLBuffer curveBuffer;

    QOpenGLShaderProgram* sliceProgram;
    QOpenGLBuffer slicePointsBuffer;

    QMatrix4x4 view;
    QMatrix4x4 proj;

    bool previewing;
    int dragIdx;

    float minValue;
    float maxValue;


    GLuint VAO;
    GLuint sliceVBO;
    GLuint sliceTexture;

    void createVBOs();
    void updateVBO();

};

#endif
