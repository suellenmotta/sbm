#ifndef IMAGE_H
#define IMAGE_H

#include <string>

class Image
{
public:
    int width, height;
    float* data;
};


Image* readImage(const std::string& filePath, int width, int height);

#endif
