#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Canvas2D.h"

class MainWindow : public QMainWindow
{
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void createToolBar();

    QToolBar* toolBar;
    Canvas2D* canvas;

    Image* seismicImage;
    Image* attributeImage;

private slots:
    void onLoadAmplitudeImageTriggered();
    void onLoadAttributeImageTriggered();
    void onViewAttributeImageToggled(bool viewAttr);
    void onPickPointsToggled(bool pickPts);
    void onExecuteTriggered();
    void onRestartTriggered();
};

#endif //MAINWINDOW_H
