#include "MainWindow.h"

#include <QToolBar>
#include <QAction>
#include <QHBoxLayout>
#include <QFileDialog>

#define MIN_AMP_VALUE -97.5357666f
#define MAX_AMP_VALUE 90.11730957f

#define MIN_ATT_VALUE -2.35197687f
#define MAX_ATT_VALUE 2.33468342f

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , seismicImage(nullptr)
    , attributeImage(nullptr)
{
    setWindowTitle("Salt boundary picking");
    createToolBar();

    QWidget* centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);

    canvas = new Canvas2D(centralWidget);

    QHBoxLayout* horizontalLayout = new QHBoxLayout(centralWidget);
    horizontalLayout->setSpacing(6);
    horizontalLayout->setContentsMargins(11, 11, 11, 11);
    horizontalLayout->addWidget(canvas);
}

MainWindow::~MainWindow()
{
}

void MainWindow::createToolBar()
{
    toolBar = new QToolBar(this);
    toolBar->setStyleSheet("QToolBar{spacing:6px;}");
    addToolBar(toolBar);

    QAction* loadAmpAct = new QAction("Load amplitude image",this);
    QAction* loadAttAct = new QAction("Load attribute image",this);
    QAction* viewAttAct = new QAction("View attribute",this);
    viewAttAct->setCheckable(true);
    QAction* pickPtsAct = new QAction("Pick",this);
    pickPtsAct->setCheckable(true);
    QAction* executeAct = new QAction("Run",this);
    QAction* restartAct = new QAction("Restart",this);

    toolBar->addAction(loadAmpAct);
    toolBar->addAction(loadAttAct);
    toolBar->addAction(viewAttAct);
    toolBar->addSeparator();
    toolBar->addAction(pickPtsAct);
    toolBar->addAction(executeAct);
    toolBar->addAction(restartAct);

    connect(loadAmpAct,&QAction::triggered,this,&MainWindow::onLoadAmplitudeImageTriggered);
    connect(loadAttAct,&QAction::triggered,this,&MainWindow::onLoadAttributeImageTriggered);
    connect(viewAttAct,&QAction::toggled,this,&MainWindow::onViewAttributeImageToggled);
    connect(pickPtsAct,&QAction::toggled,this,&MainWindow::onPickPointsToggled);
    connect(executeAct,&QAction::triggered,this,&MainWindow::onExecuteTriggered);
    connect(restartAct,&QAction::triggered,this,&MainWindow::onRestartTriggered);
}

void MainWindow::onLoadAmplitudeImageTriggered()
{
    std::string path = "C:/Users/suellen/Projects/sbm/sbp-cplusplus/data/seam-challenge-il2867.dat";

    int w = 1002;
    int h = 751;
    seismicImage = readImage(path,w,h);
    canvas->setImage(seismicImage);
    canvas->setMinValue(MIN_AMP_VALUE);
    canvas->setMaxValue(MAX_AMP_VALUE);
    canvas->update();
    return;

    QString fileName = QFileDialog::getOpenFileName(this,
        "Load volume", "", "Todos os arquivos (*.*);;DAT Files (*.dat)");

    QFileInfo check(fileName);

    if( check.exists() && check.isFile() )
    {
        seismicImage = readImage(fileName.toStdString(),w,h);
        canvas->setImage(seismicImage);
        canvas->setMinValue(MIN_AMP_VALUE);
        canvas->setMaxValue(MAX_AMP_VALUE);
        canvas->update();
    }
}

void MainWindow::onLoadAttributeImageTriggered()
{
    std::string path = "C:/Users/suellen/Projects/sbm/sbp-cplusplus/data/seam-challenge-envelope-il2867.dat";

    int w = 1002;
    int h = 751;
    attributeImage = readImage(path,w,h);
    canvas->setImage(attributeImage);
    canvas->setMinValue(MIN_ATT_VALUE);
    canvas->setMaxValue(MAX_ATT_VALUE);
    canvas->update();
    return;

    QString fileName = QFileDialog::getOpenFileName(this,
        "Load volume", "", "Todos os arquivos (*.*);;DAT Files (*.dat)");

    QFileInfo check(fileName);

    if( check.exists() && check.isFile() )
    {
        attributeImage = readImage(fileName.toStdString(),w,h);
        canvas->setImage(attributeImage);
        canvas->setMinValue(MIN_ATT_VALUE);
        canvas->setMaxValue(MAX_ATT_VALUE);
        canvas->update();
    }
}

void MainWindow::onViewAttributeImageToggled(bool viewAttr)
{
    if(viewAttr)
    {
        canvas->setImage(attributeImage);
        canvas->setMinValue(MIN_ATT_VALUE);
        canvas->setMaxValue(MAX_ATT_VALUE);
        canvas->update();
    }
    else
    {
        canvas->setImage(seismicImage);
        canvas->setMinValue(MIN_AMP_VALUE);
        canvas->setMaxValue(MAX_AMP_VALUE);
        canvas->update();
    }
}

void MainWindow::onPickPointsToggled(bool pickPts)
{
    if(pickPts)
        canvas->startPicking();
    else
        canvas->stopPicking();
}

void MainWindow::onExecuteTriggered()
{

}

void MainWindow::onRestartTriggered()
{

}
