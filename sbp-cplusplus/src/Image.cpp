#include "Image.h"

#include <fstream>
#include <iostream>

Image* readImage(const std::string &filePath, int width, int height)
{
    std::ifstream input;
    input.open(filePath,std::ios::binary);

    if(input.is_open())
    {
        auto size = width*height;

        float* buffer = nullptr;
        try
        {
            buffer = new float[size];
        }
        catch (const std::bad_alloc& e)
        {
            std::cerr << "Allocation failed: " << e.what() << std::endl;
            return nullptr;
        }

        try
        {
            input.read(reinterpret_cast<char*>(&buffer[0]),size*sizeof(float));
            input.close();
            std::cerr << "Image read successfully.";
        }
        catch( const std::ios_base::failure& e)
        {
            std::cerr << "Read file failed: " << e.what() << std::endl;
            delete[] buffer;
            input.close();
            return nullptr;
        }

        Image* img = new Image;
        img->width = width;
        img->height = height;
        img->data = buffer;

        return img;
    }

    return nullptr;
}
