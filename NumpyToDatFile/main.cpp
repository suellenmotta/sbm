#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
#include "npy.hpp"

template< typename Scalar = unsigned char >
struct NumpyData
{
    std::vector< unsigned long > shape;
    std::vector< Scalar > data;
};

NumpyData<> train, test1, test2;

template< typename Scalar >
void loadNumpyData(const std::string& path, NumpyData<Scalar>& np)
{
    npy::LoadArrayFromNumpy(path, np.shape, np.data);
}

void assembleAndWriteVolume()
{
    /**
      * Info based in the article
      *
      * Train: IL 300-701 | XL 300-1000  | Z 1005-1877
      * Test1: IL 100-299 | XL 300-1000  | Z 1005-1877
      * Test2: IL 100-701 | XL 1001-1200 | Z 1005-1877
      *
      **/
    unsigned long nIls = 601;
    unsigned long nXls = 901;
    unsigned long nZss = 255;

    unsigned long fullSize = nIls*nXls*nZss;
    std::vector<unsigned long> vShape = {nIls,nXls,nZss};

    std::vector<float> volume;
    volume.resize(fullSize,0.0f);

    auto fill = [&volume,&vShape](NumpyData<>& npy, unsigned int ilInc, unsigned int xlInc)
    {
        auto index = [](std::vector<unsigned long> shape,
                      unsigned int i, unsigned int j, unsigned int k)
        {
            return shape[1]*shape[2]*i + shape[2]*j + k;
        };

        for(unsigned int i = 0; i < npy.shape[0]; ++i)
        {
            for(unsigned int j = 0; j < npy.shape[1]; ++j)
            {
                for(unsigned int k = 0; k < npy.shape[2]; ++k)
                {
                    auto idx = index(npy.shape,i,j,k);
                    auto vIdx = index(vShape,ilInc+i,xlInc+j,k);

                    volume[vIdx] = (float) npy.data[idx];
                }
            }
        }
    };

    fill(train,200,0);
    fill(test1,0,0);
    fill(test2,0,701);

    std::ofstream output("f3-benchmark.dat");
    output.write(reinterpret_cast<char*>(volume.data()),volume.size()*sizeof(float));
    output.close();
}

void loadAllFiles(const std::string& rootDir)
{
    std::string trainPath = rootDir + "train/train_labels.npy";
    std::string test1Path = rootDir + "test_once/test1_labels.npy";
    std::string test2Path = rootDir + "test_once/test2_labels.npy";

    loadNumpyData(trainPath,train);
    loadNumpyData(test1Path,test1);
    loadNumpyData(test2Path,test2);

    std::cout << "\ntrain data shape: ";
    for(auto d : train.shape)
        std::cout << d << " ";
    std::cout << std::endl;

    std::cout << "\ntest1 data shape: ";
    for(auto d : test1.shape)
        std::cout << d << " ";
    std::cout << std::endl;

    std::cout << "\ntest2 data shape: ";
    for(auto d : test2.shape)
        std::cout << d << " ";
    std::cout << std::endl;
}


void processEntireVolumes(const std::string& rootDir)
{
    std::string labelVolumePath = rootDir + "labels_entire_volume.npy";
    std::string seismicVolumePath = rootDir + "seismic_entire_volume.npy";

    NumpyData< unsigned char > labelNpy;
    NumpyData< double > seismicNpy;

    loadNumpyData(labelVolumePath,labelNpy);
    loadNumpyData(seismicVolumePath,seismicNpy);

    auto min = *(std::min_element(seismicNpy.data.begin(),seismicNpy.data.end()));
    auto max = *(std::max_element(seismicNpy.data.begin(),seismicNpy.data.end()));

    std::cout << "\nmin: " << min << " max: " << max << std::endl;

    std::cout << "\nlabel data shape: ";
    for(auto d : labelNpy.shape)
        std::cout << d << " ";
    std::cout << std::endl;

    std::cout << "\nseismic data shape: ";
    for(auto d : seismicNpy.shape)
        std::cout << d << " ";
    std::cout << std::endl;

    std::cout << "\n\nClasses: ";
    std::set<unsigned char> classes;
    for(auto v : labelNpy.data)
    {
        classes.insert(v);
    }
    for(auto v : classes)
    {
        std::cout << (int)v << " ";
    }
    std::cout << std::endl;



    //        info2.numXSlices = 546; //crosslines
    //        info2.numYSlices = 330; //depth slices
    //        info2.numZSlices = 581; //inlines
    //        info2.computeWorldPoints({1.f,4.f,1.f});

    //        info.firstInline    = 0;
    //        info.firstCrossline = 0;
    //        info.firstDepth     = 0;
    //        info.stepInline     = 1;
    //        info.stepCrossline  = 1;
    //        info.stepDepth      = 1;
    //        info.lastInline     = 580;
    //        info.lastCrossline  = 545;
    //        info.lastDepth      = 329;
    //        info.p1 = {info2.p1.z,info2.p1.x};
    //        info.p2 = {info2.p2.z,info2.p2.x};
    //        info.p3 = {info2.p3.z,info2.p3.x};
    //        info.p4 = {info2.p4.z,info2.p4.x};



    if(labelNpy.shape[0]!=seismicNpy.shape[0] || labelNpy.shape[1]!=seismicNpy.shape[1] || labelNpy.shape[2]!=seismicNpy.shape[2])
    {
        std::cout << "Label volume shape and seismic volume shape do not match.\n";
        return;
    }

    //For subvolume
    unsigned long nIls = 445;
    unsigned long nXls = 475;
    unsigned long nZss = 253;
    unsigned int firstIl = 0;
    unsigned int firstXl = 380;
    unsigned int firstZl = 1;

    //For entire volume
//    unsigned long nIls = seismicNpy.shape[0];
//    unsigned long nXls = seismicNpy.shape[1];
//    unsigned long nZss = seismicNpy.shape[2];
//    unsigned int firstIl = 0;
//    unsigned int firstXl = 0;
//    unsigned int firstZl = 0;


    std::vector<unsigned long> vShape = {nIls,nXls,nZss};

    unsigned long fullSize = nIls*nXls*nZss;
    std::vector<float> labelVolume,seismicVolume;
    labelVolume.resize(fullSize);
    seismicVolume.resize(fullSize);

    auto index = [](std::vector<unsigned long> shape,
                  unsigned int i, unsigned int j, unsigned int k)
    {
        return shape[1]*shape[2]*i + shape[2]*j + k;
    };

    auto lastIl = firstIl+vShape[0]-1;
    auto lastXl = firstXl+vShape[1]-1;
    auto lastZl = firstZl+vShape[2]-1;

    for(unsigned int i = firstIl, iv=0; i <= lastIl; ++i,++iv)
    {
        for(unsigned int j = firstXl, jv=0; j <= lastXl; ++j,++jv)
        {
            for(unsigned int k = firstZl, kv=0; k <= lastZl; ++k, ++kv)
            {
                auto idx = index(seismicNpy.shape,i,j,k);
                auto vIdx = index(vShape,iv,jv,kv);

                seismicVolume[vIdx] = (float) seismicNpy.data[idx];
                labelVolume[vIdx] = (float) labelNpy.data[idx];
            }
        }
    }

    std::ofstream outputLabel("f3-benchmark-label.dat",std::ios::binary);
    outputLabel.write(reinterpret_cast<char*>(labelVolume.data()),labelVolume.size()*sizeof(float));
    outputLabel.close();

    std::ofstream outputSeismic("f3-benchmark-seismic.dat",std::ios::binary);
    outputSeismic.write(reinterpret_cast<char*>(seismicVolume.data()),seismicVolume.size()*sizeof(float));
    outputSeismic.close();
}

//int test_save(void) {
//  const long unsigned leshape [] = {2,3};
//  vector<double> data {1, 2, 3, 4, 5, 6};
//  npy::SaveArrayAsNumpy("data/out.npy", false, 2, leshape, data);

//  const long unsigned leshape2 [] = {6};
//  npy::SaveArrayAsNumpy("data/out2.npy", false, 1, leshape2, data);

//  return 0;
//}

int main(int argc, char **argv)
{
//    std::string rootDir = "C:/Users/suellen/Google Drive/Doutorado/Tese/Dados/F3/benchmark/data/";
//    loadAllFiles(rootDir);
//    assembleAndWriteVolume();

    std::string rootDir = "C:/Users/suellen/Google Drive/Doutorado/Tese/Dados/F3/benchmark/";
    processEntireVolumes(rootDir);

    return 0;
}
