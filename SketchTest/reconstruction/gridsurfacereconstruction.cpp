#include "gridsurfacereconstruction.h"

#include "SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "SurfaceModeling/Solver/OptimizationSolvers/PCGJacobiOptimizationSolver.h"
#include "SurfaceModeling/ModelManager/QuadraticExpression.h"
#include "SurfaceModeling/ModelManager/OptimizationModel.h"

#include "MarchingCubes/MarchingCubes.h"

#include "io.h"

#include <sstream>

GridSurfaceReconstructor::GridSurfaceReconstructor(const std::vector<glm::vec3> &pointCloud, bool closed)
    : SurfaceReconstructor3D(pointCloud,closed)
{

}

void GridSurfaceReconstructor::setGridResolution(unsigned int width, unsigned int height, unsigned int depth)
{
    if(width%2 > 0)  width++;
    if(height%2 > 0) height++;
    if(depth%2 > 0)  depth++;

    grid.nx = 2*width+1;
    grid.ny = 2*height+1;
    grid.nz = 2*depth+1;

    grid.sx = 4.0f / (grid.nx-1);
    grid.sy = 4.0f / (grid.ny-1);
    grid.sz = 4.0f / (grid.nz-1);

    writeOFF("grid.off",grid);
}

void GridSurfaceReconstructor::exportNeutralFile(const std::string &path)
{
    //writeNeutralFile(path,grid,meshVertices,meshIndices);
}

void GridSurfaceReconstructor::optimize()
{
    Solver::PCGJacobiOptimizationSolver solver;
    ModelManager::OptimizationModel model(&solver);

    std::vector<SamplePoint> sampledPoints;
    samplePoints(sampledPoints);

    writeXYZ("sampled_points.xyz", sampledPoints);

    setupVariables(model);
    setupObjectiveFunction(model);
    setupInputPointsConstraints(model,sampledPoints);
    setupBoundaryConstraints(model);

    model.optimize();

    auto numVariables = model.getNumberVariables();
    grid.functionValue.resize(numVariables,0.0f);

    auto variables = model.getVariables();

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        double x = variables[i].getValue();
        grid.functionValue[i] = x;
    }
}


void GridSurfaceReconstructor::reconstruct()
{
    RegularGrid objGrid;
    objGrid.nx = grid.nx/2+1;
    objGrid.ny = grid.ny/2+1;
    objGrid.nz = grid.nz/2+1;

    objGrid.sx = grid.sx;
    objGrid.sy = grid.sy;
    objGrid.sz = grid.sz;

    objGrid.functionValue.resize(objGrid.nx*objGrid.ny*objGrid.nz);

    for(unsigned int z0 = objGrid.nz/2, z1 = 0;
        z1 < objGrid.nz; ++z0, ++z1)
    {
        for(unsigned int y0 = objGrid.ny/2, y1 = 0;
            y1 < objGrid.ny; ++y0, ++y1)
        {
            for(unsigned int x0 = objGrid.nx/2, x1 = 0;
                x1 < objGrid.nx; ++x0, ++x1)
            {
                auto id0 = grid.index(x0,y0,z0);
                auto id1 = objGrid.index(x1,y1,z1);

                objGrid.functionValue[id1]=grid.functionValue[id0];
            }
        }
    }

    MarchingCubes mc(objGrid.nx,objGrid.ny,objGrid.nz);
        mc.init_all();
        mc.set_ext_data(const_cast<float*>(objGrid.functionValue.data()));
        mc.set_method(false);
        mc.run();

    //    mc.writePLY("surface.ply");

        //Transform the points to world space
        glm::vec3 first = {-1.0f,-1.0f,-1.0f};
        glm::vec3 last = {1.0f,1.0f,1.0f};

        auto f = (last - first) /
                glm::vec3(objGrid.nx-1.0f, objGrid.ny-1.0f, objGrid.nz-1.0f);
        for(unsigned int i = 0; i < mc.nverts(); ++i)
        {
            auto v = mc.vert(i);
            glm::vec3 p = {v->x,v->y,v->z};
            meshVertices.push_back(p * f + first);
        }

        //unormalize
        for (auto& p : meshVertices)
        {
            p = ( p / factor) + med;
//            std::swap(p.y,p.z);
        }

        for(int i = 0; i < mc.ntrigs(); ++i)
        {
            auto triang = mc.trig(i);
            meshIndices.push_back(triang->v1);
            meshIndices.push_back(triang->v2);
            meshIndices.push_back(triang->v3);
        }


}

void GridSurfaceReconstructor::setupVariables(OptimizationModel& model)
{
    auto numVariables = grid.nx*grid.ny*grid.nz;

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        std::stringstream name;
        name << "x" << i;
        model.addVariable( name.str(), -1000, 1000 );
    }

    model.update();
}

void GridSurfaceReconstructor::setupObjectiveFunction(OptimizationModel& model)
{
    auto variables = model.getVariables();
    ModelManager::QuadraticExpression& obj = model.getObjectiveFunction();

    int nx = (int)grid.nx;
    int ny = (int)grid.ny;
    int nz = (int)grid.nz;

    const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );
    omp_lock_t writelock;
    omp_init_lock( &writelock );

    for(int z = 0; z < nz; ++z)
    {
        printf( "\r%6.2lf%% complete...", 100.0 * ( double ) z / grid.nz );
        fflush( stdout );

        #pragma omp parallel for num_threads(numThreads)
        for(int y = 0; y < ny; ++y)
        {
            ModelManager::QuadraticExpression aux;

            for(int x = 0; x < nx; ++x)
            {
                ModelManager::QuadraticExpression aux2;
                ModelManager::LinearExpression exp;

                for(int k = -1; k < 2; ++k)
                {
                    auto zk = z+k;

                    if(zk >= 0 && zk < nz)
                    {
                        for(int j = -1; j < 2; ++j)
                        {
                            auto yj = y+j;

                            if(yj >= 0 && yj < ny)
                            {
                                for(int i = -1; i < 2; ++i)
                                {
                                    auto xi = x+i;

                                    if(xi >= 0 && xi < nx)
                                    {
                                        auto idx = grid.index(xi,yj,zk);
                                        exp += variables[idx];
                                    }
                                }
                            }
                        }
                    }
                }

                auto idx = grid.index(x,y,z);
                exp -= exp.size()*variables[idx];

                aux2 = exp * exp;
                aux2.update( );
                aux += aux2;
            }

            aux.update( );

            omp_set_lock( &writelock );
            obj += aux;
            omp_unset_lock( &writelock );
        }
    }
}

void GridSurfaceReconstructor::setupInputPointsConstraints(OptimizationModel& model,
                                                       const std::vector<SamplePoint> &sampledPoints)
{
    auto sense = isSoft ? SOFT : EQUAL;

    auto variables = model.getVariables();

    for (unsigned int i = 0; i < sampledPoints.size( ); i++)
    {
        const SamplePoint& p = sampledPoints[i];

        auto gPos = p.gridPos;

        ModelManager::LinearExpression exp;

        unsigned int idx = 0;
        for(unsigned int k = 0; k < 2; k++)
        {
            for(unsigned int j = 0; j < 2; j++)
            {
                for(unsigned int i = 0; i < 2; i++)
                {
                    exp +=
                        p.bCoords[idx] * variables[grid.index(gPos.x+i, gPos.y+j, gPos.z+k)];

                    ++idx;
                }
            }
        }

        model.addConstraint(exp,sense,0);
    }

    if(isSoft)
        model.setSoftConstraintsWeight(1000.0);
}

void GridSurfaceReconstructor::setupBoundaryConstraints(OptimizationModel& model)
{
    auto nx = grid.nx;
    auto ny = grid.ny;
    auto nz = grid.nz;

    auto variables = model.getVariables();

    float value = 10.0f;

    if(isClosed)
    {
        for(unsigned int z = 0; z < nz; ++z)
        {
            for(unsigned int y = 0; y < ny; ++y)
            {
                for(unsigned int x = 0; x < nx; ++x)
                {
                    if( (nx > 1 && (x==0 || x==nx-1)) ||
                        (ny > 1 && (y==0 || y==ny-1)) ||
                        (nz > 1 && (z==0 || z==nz-1)) )
                    {
                        model.addConstraint(variables[grid.index(x,y,z)], EQUAL, value);
                    }
                }
            }
        }

        //Assuming the center index is inside the object
        auto centerIdx = grid.index(nx*0.5,ny*0.5,nz*0.5);
        model.addConstraint(variables[centerIdx],LESS_EQUAL,-value);
    }

    else
    {
        if(nz==1)
        {
            for(unsigned int x = 0; x < nx; ++x)
            {
                auto bottomIdx = grid.index(x,0,0);
                auto topIdx    = grid.index(x,ny-1,0);

                model.addConstraint(variables[bottomIdx], EQUAL, value);
                model.addConstraint(variables[topIdx],    EQUAL, -value);
            }
        }

        else
        {
            for(unsigned int z = 0; z < nz; ++z)
            {
                for(unsigned int x = 0; x < nx; ++x)
                {
                    auto bottomIdx = grid.index(x,0,z);
                    auto topIdx    = grid.index(x,ny-1,z);

                    model.addConstraint(variables[bottomIdx], EQUAL, value);
                    model.addConstraint(variables[topIdx],    EQUAL, -value);
                }
            }
        }
    }
}

void GridSurfaceReconstructor::samplePoints(std::vector<SamplePoint>& sampledPoints)
{
    auto volume = grid.sx*grid.sy*grid.sz;

    auto calcVolume = [](const glm::vec3& p0, const glm::vec3& p1)
    {
        auto dist = glm::abs(p1-p0);
        return dist.x*dist.y*dist.z;
    };

    auto computeCoords3d = [&](SamplePoint& p,
            const glm::vec3& pMin)
    {
        p.bCoords.resize(8);
        p.bCoords[0] = (calcVolume(p.coords,pMin+glm::vec3(grid.sx,grid.sy,grid.sz))) / volume;
        p.bCoords[1] = (calcVolume(p.coords,pMin+glm::vec3(0,grid.sy,grid.sz))) / volume;
        p.bCoords[2] = (calcVolume(p.coords,pMin+glm::vec3(grid.sx,0,grid.sz))) / volume;
        p.bCoords[3] = (calcVolume(p.coords,pMin+glm::vec3(0,0,grid.sz))) / volume;
        p.bCoords[4] = (calcVolume(p.coords,pMin+glm::vec3(grid.sx,grid.sy,0))) / volume;
        p.bCoords[5] = (calcVolume(p.coords,pMin+glm::vec3(0,grid.sy,0))) / volume;
        p.bCoords[6] = (calcVolume(p.coords,pMin+glm::vec3(grid.sx,0,0))) / volume;
        p.bCoords[7] = (calcVolume(p.coords,pMin)) / volume;
    };

    auto calcArea = [](const glm::vec3& p0, const glm::vec3& p1)
    {
        auto dist = glm::abs(p1-p0);
        return dist.x*dist.y;
    };

    auto computeCoords2d = [&](SamplePoint& p,
            const glm::vec3& pMin)
    {
        p.bCoords.resize(4);
        p.bCoords[0] = (volume-calcArea(p.coords,pMin)) / volume;
        p.bCoords[1] = (volume-calcArea(p.coords,pMin+glm::vec3(grid.sx,0,0))) / volume;
        p.bCoords[2] = (volume-calcArea(p.coords,pMin+glm::vec3(0,grid.sy,0))) / volume;
        p.bCoords[3] = (volume-calcArea(p.coords,pMin+glm::vec3(grid.sx,grid.sy,0))) / volume;
        p.bCoords[4] = (volume-calcArea(p.coords,pMin+glm::vec3(0,0,grid.sz))) / volume;
        p.bCoords[5] = (volume-calcArea(p.coords,pMin+glm::vec3(grid.sx,0,grid.sz))) / volume;
        p.bCoords[6] = (volume-calcArea(p.coords,pMin+glm::vec3(0,grid.sy,grid.sz))) / volume;
        p.bCoords[7] = (volume-calcArea(p.coords,pMin+glm::vec3(grid.sx,grid.sy,grid.sz))) / volume;
    };



    glm::vec3 first = {-2.0f,-2.0f,-2.0f};

    for(unsigned int k = 0; k < grid.nz-1; ++k)
    {
        auto zMin = first.z + k*grid.sz;
        auto zMax = zMin+grid.sz;

        for(unsigned int j = 0; j < grid.ny-1; ++j)
        {
            auto yMin = first.y + j*grid.sy;
            auto yMax = yMin+grid.sy;

            for(unsigned int i = 0; i < grid.nx-1; ++i)
            {
                auto xMin = first.x + i*grid.sx;
                auto xMax = xMin+grid.sx;

                for(const auto& p : pts)
                {
                    auto eps = 0.01f;

                    if(p.x >= xMin+eps && p.x <= xMax-eps &&
                       p.y >= yMin+eps && p.y <= yMax-eps &&
                       p.z >= zMin+eps && p.z <= zMax-eps)
                    {
                        SamplePoint point;
                        point.coords = p;
                        point.gridPos = {i,j,k};

                        computeCoords3d(point,{xMin,yMin,zMin});

                        sampledPoints.push_back(point);
                        break;
                    }
                }
            }
        }
    }
}
