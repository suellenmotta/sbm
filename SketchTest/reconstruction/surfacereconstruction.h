#ifndef SURFACERECONSTRUCTOR_H
#define SURFACERECONSTRUCTOR_H


#include <glm/glm.hpp>
#include <vector>

#include "io.h"


#include <iomanip>
#include <algorithm>
#include "defs.h"


/** Reconstructs a surface given a point cloud as input **/
template< typename VertexType >
class SurfaceReconstructor
{
public:
    SurfaceReconstructor(const std::vector< VertexType >& pointCloud, bool closed = false)
        : pts(pointCloud)
        , isClosed(closed)
        , isSoft(true) {}

    void run()
    {
        normalizePoints();
        optimize();
        reconstruct();
    }

    void getSurfaceMesh(std::vector<VertexType>& vertices, std::vector< unsigned int>& indices)
    {
        vertices = meshVertices;
        indices = meshIndices;
    }


    void setSmooth(bool isSmooth)
    {
        isSoft = isSmooth;
    }

protected:
    virtual void normalizePoints()
    {
//        writeXYZ("input_points.xyz", pts);

        auto min = pts[0], max = pts[0];

        for(const auto& p : pts)
        {
            for(int i = 0; i < p.length(); ++i)
                if(p[i]<min[i]) min[i]=p[i];

            for(int i = 0; i < p.length(); ++i)
                if(p[i]>max[i]) max[i]=p[i];
        }

        med = (min + max)*0.5f;
        auto size = max-min;


        float offset = 0.0f;//2.f*0.01f;
        auto largest = *std::max_element(&size.x,&size.x+size.length());

        factor = (2.0f-offset) / largest;
        for (auto& p : pts)
        {
            p = ( p - med ) * factor;
        }

        writeXYZ("normalized_points.xyz", pts);
    }

    virtual void optimize() = 0;
    virtual void reconstruct() {}

    std::vector<VertexType> pts;
    bool isClosed;
    bool isSoft;

    std::vector<VertexType> meshVertices;
    std::vector< unsigned int> meshIndices;

    float factor;
    VertexType med;
};

using SurfaceReconstructor3D = SurfaceReconstructor<glm::vec3>;
using SurfaceReconstructor2D = SurfaceReconstructor<glm::vec2>;



//// To be implemented
//void reconstruct(const Octree<float>& oct, std::vector<glm::vec3>& vertices, std::vector< unsigned int>& indices)
//{
//}

//// To be implemented
//void reconstruct(const RegularGrid& grid, std::vector<glm::vec3>& vertices, std::vector< unsigned int>& indices)
//{
//}

//// To be implemented
//void reconstruct(const TetrahedralMesh& tetraMesh, std::vector<glm::vec3>& vertices, std::vector< unsigned int>& indices)
//{

//}





#endif // SURFACERECONSTRUCTOR_H
