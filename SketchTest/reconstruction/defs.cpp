#include "defs.h"

std::ostream& operator << (std::ostream& output, const glm::vec3& point)
{
    output << std::fixed << "("
           << point.x << ", "
           << point.y << ", "
           << point.z <<
              ")";
    return output;
}

std::ostream& operator << (std::ostream& output, const glm::vec2& point)
{
    output << std::fixed << "("
           << ((point.x >= 0) ? "+" : "")
           << point.x << ", "
           << ((point.y >= 0) ? "+" : "")
           << point.y <<
              ")";
    return output;
}
