/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeshWindow.h
 * Author: ncortez
 *
 * Created on September 20, 2016, 5:52 PM
 */

#ifndef MESHWINDOW_H
#define MESHWINDOW_H

#include "Geometry.h"
#include <iup/iup.h>
#include <cstdlib>
#include <cstdio>
#include <mutex> 
#include <future>
#include <fstream>
#include <vector>
#include "../../ModelManager/Variable.h"


using namespace std;

namespace Solver
{
    class OptimizationSolver;
}

namespace ModelManager
{
    class OptimizationModel;
}

class MeshWindow
{
public:
    
    MeshWindow( );
    
    virtual ~MeshWindow( );
    
    /**
     * Exibe a janela.
     */
    void show( );

    /**
     * Oculta a janela.
     */
    void hide( );
    
private:
    /**
     * Structure to represent a point by your variables.
     */
    struct VarPoint
    {
        ModelManager::Variable x, y;
    };
    
    /**
     * Dialog pointer.
     */
    Ihandle *_dialog;  
    
    /**
     * Variable used to scale to data.
     */
    double _scale = 1;
    
    
    /**
     * true if a fixed point is selected
     * false if it is released or no fixedPoint is selected
     */
    bool _pointClicked;
    
    
    /**
     * Holds the index for the last selected fixed point
     */
    int _choosenPointIndex;
    
    Point _pMin, _pMax;
    
    std::vector<Point> _input;
    std::vector< std::vector< int > > _neighbors;
    std::vector< std::vector< int > > _faces;
    std::vector< int > _distances; 
    
    Solver::OptimizationSolver *_solver;
    
    ModelManager::OptimizationModel *_m;
    
    std::vector<VarPoint> _variables;       

    /**
     * Store wich mouse button are pressed.
     */
    int _mousePressed;

    /**
     * Store the first clicked point in pixel coordinates. This data is used to
     * move the slice.
     */
    int _firstPixelX, _firstPixelY;
    
    /**
     * Mutex used to protect the current solution copy.
     */
    std::mutex _mtx;
    

    /**
     * Future variable used to check if the thread is finished.
     */
    std::future<void> _fut;

    /**
     * Flag used to cancel the thread.
     */
    bool _continueSolver;
    
private:
    
    
    /**
     * Cria janela da IUP e define suas configuracoes e callbacks.
     */
    void createWindow( );
        
    /**
     * Create a set of menus.
     * @return - pointer to main menu. That is a menu's composition.
     */
    Ihandle* createMenus( );
    
    /**
     * Incializa algumas propriedades do canvas OpenGL.
     */
    void initialize( );
    
    void readFile( const char* fileName );
    
    void renderFirst( Ihandle* canvas );
    
    void zoom( int delta );
    
    void run( );
    
    void initializeSolution( unsigned int initIndex );
    
    void allocateSolver( int index );
    
    int findNewIndex(int oldIndex, std::vector< int > indexesVector);

    
    bool updateSolution( const std::vector<double>& v, double gnorm );

    static bool displayFunction( const std::vector<double>& v, double gnorm );

    void buildModel( );
    
    void optimize( double eps, unsigned numIterations );

    
    void activateComponents( Ihandle* component );
    
    void convertPixelToWorld( int px, int py, double& x, double& y );
    
    void resizeCanvas( int width, int height );
    
    
    ModelManager::OptimizationModel *getUniformLaplacianModel( const std::vector<Point>& input,
                                             const std::vector< std::vector< int > >& neighbors,
                                             std::vector< std::vector< int > >& faces );

    ModelManager::OptimizationModel *getUniformLaplacianAverageModel( const std::vector<Point>& input,
                                             const std::vector< std::vector< int > >& neighbors,
                                             std::vector< std::vector< int > >& faces );
        
    ModelManager::OptimizationModel *getDiagonalModel( const std::vector<Point>& input,
                                     const std::vector< std::vector< int > >& neighbors,
                                     std::vector< std::vector< int > >& faces );
    
    
    ModelManager::OptimizationModel *getJmetricModel( const std::vector<Point>& input,
                                    const std::vector< std::vector< int > >& neighbors,
                                    std::vector< std::vector< int > >& faces,
                                    std::vector<int>& distances );
    
    
    static void getJmetricItModel( );
    
    
    
    
    void openData( const std::string& path, unsigned int initIndex );
    
    static int newButtonCallback( Ihandle *button );
    
    static int openDataMenuCallback( Ihandle* button );
    
    static int actionCanvasCallback( Ihandle* canvas );
    
    static int wheelCanvasCallback( Ihandle *canvas, float delta, int x,
                                    int y, char *status );
    
    static int motionCanvasCallback( Ihandle* canvas, int x, int y, char* status );

    static int buttonCanvasCallback( Ihandle* canvas, int button, int pressed,
                                        int x, int y, char* status );
    
    static int runButtonCallback( Ihandle *button );
    
    static void optimizeCurveCallback( );
    
    static int checkFunction( );
    
    static int resizeCanvasCallback( Ihandle *canvas, int width, int height );
    
    static int stopButtonCallback( Ihandle *button );

    static int exitButtonCallback( Ihandle* button );

    
    



};

#endif /* MESHWINDOW_H */

