#ifndef MODELMANAGER_LINEAR_EXPRESSION_H
#define MODELMANAGER_LINEAR_EXPRESSION_H

#include <vector>
#include <iostream>

#include "Variable.h"



namespace ModelManager
{
    class LinearExpression;

    std::ostream& operator<<( std::ostream &stream, const LinearExpression &exp );
    LinearExpression operator+( const LinearExpression &x, const LinearExpression &y );
    LinearExpression operator+( const LinearExpression &x );
    LinearExpression operator+( const Variable x, const Variable y );
    LinearExpression operator+( double a, const Variable x );
    LinearExpression operator-( const LinearExpression &x, const LinearExpression &y );
    LinearExpression operator-( const LinearExpression &x );
    LinearExpression operator-( const Variable x, const Variable y );
    LinearExpression operator-( double a, const Variable x );
    LinearExpression operator*( double a, Variable x );
    LinearExpression operator*( const Variable x, double a );
    LinearExpression operator*( double a, const LinearExpression &x );
    LinearExpression operator*( const LinearExpression &x, double a );

    class LinearExpression
    {
    private:
        /**
         * The constant of the linear expression.
         */
        double _constant;

        /**
         * The coefficients of the variable on linear expression.
         */
        std::vector<double> _coefficients;

        /**
         * The variables on linear expression.
         */
        std::vector<Variable> _variables;


    public:
        /**
         * The linear expression constructor that receives the constant value.
         * @param constant - the constant value.
         */
        LinearExpression( double constant = 0.0 );

        /**
         * The linear expression constructor that receives a variable and its
         * coefficient.
         * @param v - first variable on linear expression.
         * @param coefficient - coefficient of the variable.
         */
        LinearExpression( const Variable v, double coefficient = 1.0 );

        /**
         * Clear the linear expression.
         */
        void clear( );

        /**
         * Get the variable of index v.
         * @param i - index of the variable that must be returned.
         * @return - the variable of index v.
         */
        Variable getVariable( unsigned int v ) const;

        /**
         * Get the coefficient of the variable of index c. THIS FUNCTION UPDATE
         * FUNCTION MUST BE CALLED TO ENSURE TO CORRECT OPERATION OF THIS
         * FUNCTION.
         * @param v - variable index that must be returned the coefficient.
         * @return - the coefficient of the variable i.
         */
        double getCoefficient( unsigned int v ) const;

        /**
         * Get the coefficient of the variable v.
         * @return -  variable that must be returned the coefficient.
         */
        double getCoefficient( const Variable v ) const;

        /**
         * Get the constant of the linear expression.
         * @return - the constant of the linear expression.
         */
        double getConstant( ) const;

        /**
         * Get the linear expression value.
         * @return - the linear expression value.
         */
        double getValue( ) const;

        /**
         * Overload of the operator << to print the linear expression.
         * @param stream - the stream that will be used to print the linear
         * expression.
         * @param exp - linear expression that needs to be printed.
         * @return - the strem with the linear expression printed.
         */
        friend std::ostream& operator<<( std::ostream &stream, const LinearExpression &exp );

        /**
         * Overload of the operator + to sum two linear expressions.
         * @param x - the left hand side of the linear expression.
         * @param y - the right hand side of the linear expression.
         * @return - the new linear expression.
         */
        friend LinearExpression operator+( const LinearExpression &x, const LinearExpression &y );

        /**
         * Overload of the operator + to set the sign of the linear expression.
         * @param x - the right hand side of the linear expression.
         * @return - the new linear expression.
         */
        friend LinearExpression operator+( const LinearExpression &x );

        /**
         * The overload of the operator + to sum two variables and return a
         * linear expression.
         * @param x - the left hand side variable.
         * @param y - the right hand side variable.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator+( const Variable x, const Variable y );

        /**
         * The overload of the operator + to sum a variable and a constant.
         * @param a - the constant that must be summed to variable.
         * @param x - the variable that must be summed to constant.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator+( double a, const Variable x );

        /**
         * The overload of the operator - to subtract two linear expressions.
         * @param x - the left hand side of the linear expression.
         * @param y - the right hand side of the linear expression.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator-( const LinearExpression &x, const LinearExpression &y );

        /**
         * The overload of the operator - to invert the sign of the linear
         * expression.
         * @param x - the linear expression that will have your sign inverted.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator-( const LinearExpression &x );

        /**
         * The overload of the operator - to subtract two variables and return
         * @param x - the left hand side variable.
         * @param y - the right hand side variable.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator-( const Variable x, const Variable y );

        /**
         * The overload of the operator - to subtract a constant and a variable.
         * @param a - the constant to subtract a variable.
         * @param x - the variable that must be subtract of the constant a.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator-( double a, const Variable x );

        /**
         * The overload of the operator * to multiply a constant and a variable.
         * @param a - the constant to be multiplied for a variable.
         * @param x - the variable that must be multiplied for a constant a.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator*( double a, const Variable x );

        /**
         * The overload of the operator * to multiply a constant and a variable.
         * @param x - the variable that must be multiplied for a constant a.
         * @param a - the constant to be multiplied for a variable.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator*( const Variable x, double a );

        /**
         * The overload of the operator * to multiply a constant and a linear
         * expression.
         * @param a - the constant that must be multiplied by a linear expression.
         * @param x - the variable that must be multiplied by a constant.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator*( double a, const LinearExpression &x );

        /**
         * The overload of the operator * to multiply a constant and a linear
         * expression.
         * @param x - the variable that must be multiplied by a constant.
         * @param a - the constant that must be multiplied by a linear expression.
         * @return - the resultant linear expression.
         */
        friend LinearExpression operator*( const LinearExpression &x, double a );

        /**
         * The overload of the operator = to copy a linear expression;
         * @param rhs - the linear expression that must be copied.
         * @return - the copied linear expression.
         */
        LinearExpression operator=( const LinearExpression &rhs );

        /**
         * The overload of the operator += to sum the a linear expression in the
         * current linear expression.
         * @param exp - linear expression that must be summed with the current
         * linear expression.
         */
        void operator+=( const LinearExpression &exp );

        /**
         * The overload of the operator -= to subtract the a linear expression
         * in the current linear expression.
         * @param exp - linear expression that must be subtract to the current
         * linear expression.
         */
        void operator-=( const LinearExpression &exp );

        /**
         * The overload of the operator *= to multiply a constant by the current
         * linear expression.
         * @param mult - constant that must be multiplied by the current linear
         * expression.
         */
        void operator*=( double mult );

        /**
         * The overload of the operator /= to divide a constant by the current
         * linear expression.
         * @param a - constant that must be multiplied by the current linear
         * expression. If a = 0.0, nothing will be made.
         */
        void operator/=( double a );

        /**
         * The overload of the operator + sum a linear expression with to the
         * current expression. 
         * @param rhs - the linear expression that must be summed with the
         * current linear expression.
         * @return - the resultant linear expression.
         */
        LinearExpression operator+( const LinearExpression &rhs );

        /**
         * The overload of the operator - subtract a linear expression with to the
         * current expression. 
         * @param rhs - the linear expression that must be subtracted with the
         * current linear expression.
         * @return - the resultant linear expression.
         */
        LinearExpression operator-( const LinearExpression &rhs );

        /**
         * Remove the variable with index i in the linear expression.
         * @param i - index of the linear expression that must be removed.
         */
        void remove( unsigned int i );

        /**
         * Remove the variable v.
         * @param v - variable that must be removed.
         * @return - true if the variable v was found and removed and false
         * otherwise.
         */
        bool remove( const Variable v );

        /**
         * Define a new coefficient to i-th term.
         * @param u - term index.
         * @param c - new coefficient.
         */
        void setCoefficient( unsigned int i, double c );

        /**
         * Get the number of variable in the linear expression.
         * @return - the number of variable in the linear expression.
         */
        unsigned int size( ) const;

        /**
         * Group the expression and sort it.
         */
        void update( );
    };

} // namespace ModelManager
#endif
