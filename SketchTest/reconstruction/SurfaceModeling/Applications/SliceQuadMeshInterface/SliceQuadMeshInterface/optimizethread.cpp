#include "optimizethread.h"

OptimizeThread::OptimizeThread( ModelManager::OptimizationModel *m)
{
    _m = m;
    _stop = false;
}

void OptimizeThread::stopThread()
{
   _stop = true;
}

bool OptimizeThread::currentSolution( const std::vector<double>& x, double gnorm, void *object )
{
    OptimizeThread *t = (OptimizeThread*) object ;
    if (t)
    {
        return t->emitInformation( x, gnorm );
    }
    return true;
}



bool OptimizeThread::emitInformation( const std::vector<double>& x, double gnorm )
{
    _mutex.lock();
    emit currentSolutionUpdate( x, gnorm );
    _mutex.unlock();
    return !_stop;
}



void OptimizeThread::run()
{
    if (_m)
    {
        _m->setDisplayInformationCallback( currentSolution, this );
        _m->optimize();
    }
    //finish
}
