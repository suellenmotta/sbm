#ifndef OPTIMIZETHREAD_H
#define OPTIMIZETHREAD_H
#include <QThread>
#include <QMutex>
#include "ModelManager/OptimizationModel.h"
class OptimizeThread : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief OptimizeThread Constructor.
     * @param m - model.
     */
    explicit OptimizeThread( ModelManager::OptimizationModel *m );

    /**
     * @brief stopThread - stop optimizing process.
     */
    void stopThread();

    /**
     * @brief run - execute thread.
     */
    virtual void run();
signals:
    /**
     * @brief currentSolution - signal to update about the current solution.
     * @param x - current solution.
     * @param gnorm - gradient norm.
     */
    void currentSolutionUpdate( const std::vector<double>& x, double gnorm );
public slots:

private:
    /**
     * @brief currenSolution - update the current solution.
     * @param x - current solution values.
     * @param gnorm - gradient norm.
     * @param object - object point.
     * @return - true to continue and false otherwise.
     */
    static bool currentSolution( const std::vector<double>& x, double gnorm, void *object );

    /**
     * @brief emitInformation - emit information about the current solution.
     * @param x - current solution.
     * @param gnorm - gradient norm.
     */
    bool emitInformation( const std::vector<double>& x, double gnorm );
private:
    ModelManager::OptimizationModel *_m;

    QMutex _mutex;

    bool _stop;
};

#endif // OPTIMIZETHREAD_H
