#ifndef MESHELEMENTS_H
#define MESHELEMENTS_H
#include "mesh.h"
#include "slicemeshoptimization.h"
class MeshElements : virtual public Mesh
{
public:
    MeshElements();
    MeshElements(std::string File);
    void  setVectorPoints();
    void setTriangles();
    void setindborda();
    void setIndknown();
};

#endif // MESHELEMENTS_H
