#-------------------------------------------------
#
# Project created by QtCreator 2018-03-02T13:52:11
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SliceQuadMeshInterface
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    renderopengl.cpp \
    slicemeshoptimization.cpp \
    ../../../ModelManager/Constraint.cpp \
    ../../../ModelManager/ConstraintRepresentation.cpp \
    ../../../ModelManager/DSIModel.cpp \
    ../../../ModelManager/LinearExpression.cpp \
    ../../../ModelManager/Model.cpp \
    ../../../ModelManager/OptimizationModel.cpp \
    ../../../ModelManager/PreprocessingEqualityConstraints.cpp \
    ../../../ModelManager/PreprocessingSimpleConstraints.cpp \
    ../../../ModelManager/QuadraticExpression.cpp \
    ../../../ModelManager/Variable.cpp \
    ../../../ModelManager/VariableRepresentation.cpp \
    ../../../Graph/DSIGraph.cpp \
    ../../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.cpp \
    ../../../Solver/OptimizationSolvers/OptimizationSolver.cpp \
    ../../../Solver/Solver.cpp \
    optimizethread.cpp \
    mesh.cpp \
    meshgrid.cpp \
    meshelements.cpp

HEADERS += \
        mainwindow.h \
    renderopengl.h \
    slicemeshoptimization.h \
    ../../../ModelManager/Constraint.h \
    ../../../ModelManager/ConstraintRepresentation.h \
    ../../../ModelManager/DSIModel.h \
    ../../../ModelManager/Enums.h \
    ../../../ModelManager/LinearExpression.h \
    ../../../ModelManager/Model.h \
    ../../../ModelManager/OptimizationModel.h \
    ../../../ModelManager/PreprocessingEqualityConstraints.h \
    ../../../ModelManager/PreprocessingSimpleConstraints.h \
    ../../../ModelManager/QuadraticExpression.h \
    ../../../ModelManager/Variable.h \
    ../../../ModelManager/VariableRepresentation.h \
    ../../../Graph/DSIGraph.h \
    ../../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h \
    ../../../Solver/OptimizationSolvers/OptimizationSolver.h \
    ../../../Solver/Solver.h \
    optimizethread.h \
    mesh.h \
    meshgrid.h \
    meshelements.h

FORMS += \
        mainwindow.ui

DISTFILES += \
    vertexShader.glsl \
    fragmentShader.glsl \
    geometryShader.glsl \
    fragmentShaderPoints.glsl \
    vertexShaderPoints.glsl
#Para utilizar glPointSize:
win32{
LIBS += -lopengl32 -lglu32 -lpthread
QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp
}

linux{
LIBS += -lGL
QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp
}

INCLUDEPATH +="../../../"
