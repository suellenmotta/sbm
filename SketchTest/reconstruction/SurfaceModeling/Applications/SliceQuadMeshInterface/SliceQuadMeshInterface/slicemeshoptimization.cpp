#include "slicemeshoptimization.h"
#include <fstream>
SliceMeshOptimization::SliceMeshOptimization(std::string fileName )
{
    readFile(fileName);

}



void SliceMeshOptimization:: readFile( std::string fileName )
{
    std::ifstream in( fileName );
    if ( in.fail( ) )
    {
        return;
    }
    double trash;

    int columns, lines;

    //Read the number of lines and columns.
    in >> columns >> lines >> trash;

    matrix.resize( lines );
    //v.resize( lines );
    for ( unsigned int i = 0; i < matrix.size( ); i++ )
    {
        matrix[i].resize( columns );
        //v[i].resize( columns );
    }

    //Read the mesh.
    for ( int i = 0; i < lines; i++ )
    {
        for ( int j = 0; j < columns; j++ )
        {
            in >> matrix[i][j].x >> matrix[i][j].y >> trash >> matrix[i][j].known;
        }
    }
    _lines = lines;
    _columns = columns;
}



std::vector<std::vector< Point >> SliceMeshOptimization:: GetMatrix()
{
    return matrix;
}



void SliceMeshOptimization:: GetSize(int &lines, int &columns)
{
    lines=_lines;
    columns=_columns;
}
