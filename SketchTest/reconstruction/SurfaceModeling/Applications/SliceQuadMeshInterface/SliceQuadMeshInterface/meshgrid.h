#ifndef MESHGRID_H
#define MESHGRID_H
#include "mesh.h"
class MeshGrid: public Mesh
{
public:
    MeshGrid();
    MeshGrid(std::string File);
    void  setVectorPoints();
    void setTriangles();
    void setindborda();
    void setIndknown();
};

#endif // MESHGRID_H
