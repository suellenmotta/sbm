#ifndef MESH_H
#define MESH_H
#include "slicemeshoptimization.h"
#include<iostream>
class Mesh
{
protected :
    std::vector< int > _triangles;
    std::vector< int > _indborda;
    std::vector< int > _indknown;
    std::vector<Point> _points;
public:
    virtual void  setVectorPoints()=0;
    std::vector<Point> getVectorPoints();
    virtual void setTriangles()=0;
    std::vector< int > getTriangles();
    virtual void setindborda()=0;
    std::vector< int > getIndborda();
    virtual void setIndknown()=0;
    std::vector< int > getIndknown();
};

#endif // MESH_H
