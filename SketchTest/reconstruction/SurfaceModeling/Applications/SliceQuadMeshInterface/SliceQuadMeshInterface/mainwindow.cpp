#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QStatusBar>
#include <QLineEdit>
#include <iostream>
#include <QImage>
#include <fstream>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    fill_initial_solution();
    fill_solver();
    ui->actionSalve_Mesh->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_actionOpen_Mesh_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Mesh"), "", tr("Arquivos de malha (*.coords)"));
    if(fileName.size())
    {
        SliceMeshOptimization Aux(fileName.toStdString());
        Aux.GetSize( lines, col );
        ui->openGLWidget->setMatrixInfo( lines, col, Aux.GetMatrix() );
        ui->pushButtonRun->setEnabled(true);
        ui->actionSalve_Mesh->setEnabled(true);
    }
}
void MainWindow::fill_initial_solution()
{
    //Preenchendo initial solution com nomes
    ui->Initial->addItem("Corrente");
    ui->Initial->addItem("Zero");
    ui->Initial->addItem("Entrada");

}

void MainWindow::fill_solver()
{
    //Preenchendo solver com nomes
    ui->Solver->addItem("LBFGS");
    ui->Solver->addItem("LIS");
    ui->Solver->addItem("SLU");

}


void MainWindow::on_pushButton_clicked()
{
    this->close();
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

//Botão de Run Clicado
void MainWindow::on_pushButtonRun_clicked()
{
    //Quando aperta Botão RUN

    /* Conectando o sinal de Update do tempo que está no RenderOpenGl
    com o slot para exibir lá embaixo no
    status bar a string recebida */

    connect(ui->openGLWidget,
    SIGNAL( updateTime(const QString&)),
    statusBar(),
    SLOT(showMessage(const QString&)));
    QDoubleValidator aux();

    ui->pushButtonStop->setEnabled(true);

    int maxIterations = ui->MaxIterations->value();
    ui->openGLWidget->setMaxIterations( maxIterations );

    double eps = ui->EPS->text().toDouble();
    ui->openGLWidget->setEPS( eps );

    InitialSolutionId ini;
    QString initialSolution = ui->Initial->currentText();
    //Quando troca de opção no ComboBox de solução inicial
    if (initialSolution == "Corrente")
    {
        ini = CURRENTCOORD;
    }
    else if (initialSolution == "Entrada")
    {
        ini = INPUTCOORD;
    }
    else
    {
        ini = ZEROCOORD;
    }
    ui->openGLWidget->updateInitialSolution( ini );

    QString solverString = ui->Solver->currentText();
    CurrentSolver solver;
    if (solverString == "LBFGS")
    {
        solver = LBFGS;
    }
    else if (solverString == "LIS")
    {
        solver = LIS;
    }
    else
    {
        solver = SLU;
    }
    ui->openGLWidget->updateCurrentSolver( solver );

    bool dimensions = ui->dimensions->isChecked();
    ui->openGLWidget->setSeparatedDimensions(dimensions);

    //Executa processo.
    ui->openGLWidget->runOptimization();
}

void MainWindow::on_pushButtonStop_clicked()
{
    //Quando aperta Botão RUN
    disconnect(ui->openGLWidget,
    SIGNAL( updateTime(const QString&)),
    statusBar(),
    SLOT(showMessage(const QString&)));
    QDoubleValidator aux();

    ui->pushButtonStop->setEnabled(false);
    ui->openGLWidget->stopThread();
}

void MainWindow::on_Initial_currentIndexChanged(const QString &arg1)
{
    InitialSolutionId ini;
    //Quando troca de opção no ComboBox de solução inicial
    if (arg1 == "Corrente")
    {
        ini = CURRENTCOORD;
    }
    else if (arg1 == "Entrada")
    {
        ini = INPUTCOORD;
    }
    else
    {
        ini = ZEROCOORD;
    }
    ui->openGLWidget->updateInitialSolution( ini );

}

void MainWindow::on_Solver_currentIndexChanged(const QString &arg1)
{
    //Quando troca de opção no ComboBox de Solver
    CurrentSolver solver;
    if (arg1 == "LBFGS")
    {
        solver = LBFGS;
    }
    else if (arg1 == "LIS")
    {
        solver = LIS;
    }
    else
    {
        solver = SLU;
    }
    ui->openGLWidget->updateCurrentSolver( solver );
}


void MainWindow::on_actionSalvar_Print_triggered()
{
    //Pegando a imagem
    QImage image = ui->openGLWidget->grabFramebuffer();

    //Salvando a Imagem
    QString fileName = QFileDialog::getSaveFileName(this,
    tr("Salvar imagem"), "", tr("Arquivos de imagem (*.png)"));
    if (!fileName.isEmpty() )
    {
        image.save(fileName);
    }
}

void MainWindow::on_actionSalve_Mesh_triggered()
{
    std::vector<Point> points=ui->openGLWidget->Get_Matrix();
    QString filename = QFileDialog::getSaveFileName(this,
    tr("Salvar Arquivo"), "", tr("Arquivos de malha (*.coords)"));
    if(!filename.endsWith(".coords"))
    {
        filename.append(".coords");

    }
    std::ofstream resultfile(filename.toStdString());
    if ( resultfile.is_open() )
    {
        resultfile<<col<<" "<<lines<< " 1\n";
        for(int i= 0;i<points.size();i++)
        {
            resultfile<<std::scientific<<points[i].x<<" "<<points[i].y<< " "<<0<<" "<<points[i].known<<"\n";
        }
        resultfile.close();
    }
}
