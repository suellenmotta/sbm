/* 
 * File:   main.cpp
 * Author: jcoelho
 *
 * Created on October 20, 2016, 12:09 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

struct Point
{
    double x, y, z;


    friend std::ostream& operator<<( std::ostream& o, const Point& p );
    friend std::istream& operator>>( std::istream& is, Point& p );
};

struct Triangle
{
    unsigned int v1, v2, v3;


    friend std::ostream& operator<<( std::ostream& o, const Triangle& p );
    friend std::istream& operator>>( std::istream& is, Triangle& t );
};



std::istream& operator>>( std::istream& is, Point& p )
{
    is >> p.x >> p.y >> p.z;
    return is;
}



std::ostream& operator<<( std::ostream& o, const Point& p )
{
    return o << p.x << " " << p.y << " " << p.z;
}



std::istream& operator>>( std::istream& is, Triangle& t )
{
    is >> t.v1 >> t.v2 >> t.v3;
    t.v1--;
    t.v2--;
    t.v3--;
    return is;
}



std::ostream& operator<<( std::ostream& o, const Triangle& t )
{
    return o << t.v1 << " " << t.v2 << " " << t.v3;
}



void writeSurface( std::string& name, std::string& surfaceType,
                   std::vector<Point>& coords,
                   std::vector<Triangle>& triangleMesh )
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Fail to write the surface %s!\n", name.c_str( ) );
        return;
    }

    out.precision( 14 );
    out << std::scientific;

    out << coords.size( ) << " " << triangleMesh.size( ) << " ";
    out << (surfaceType == "top" ? "horizon" : "fault") << std::endl;

    for (unsigned int i = 0; i < coords.size( ); i++)
    {
        out << coords[i] << std::endl;
    }

    for (unsigned int i = 0; i < triangleMesh.size( ); i++)
    {
        out << triangleMesh[i] << std::endl;
    }
}



void readSurface( std::ifstream& in, std::string& name, std::string& surfacetType )
{
    //Triangle mesh information.
    std::vector<Point> coords;
    std::vector<Triangle> triangleMesh;

    Point p;

    std::string line;
    while (in >> line && line != "TFACE");

    while (in >> line && line == "VRTX")
    {
        unsigned int index;
        in >> index >> p;
        coords.push_back( p );
    }

    if (line != "TRGL")
    {
        while (in >> line && line != "TRGL");
    }

    Triangle t;
    in >> t;
    triangleMesh.push_back( t );
    while (in >> line && line == "TRGL")
    {
        in >> t;
        triangleMesh.push_back( t );
    }

    while (in >> line && line != "END");

    printf( "Writing the surface %s with %d points and %d triangles\n\n",
            name.c_str( ), ( int ) coords.size( ), ( int ) triangleMesh.size( ) );

    //Write surface file
    writeSurface( name, surfacetType, coords, triangleMesh );
}



/*
 * 
 */
int main( int argc, char** argv )
{
    if (argc != 2)
    {
        printf( "Usage: prog file.ts\n" );
        return 0;
    }

    std::ifstream in( argv[1] );
    if (!in)
    {
        printf( "Error opening the file %s\n", argv[1] );
        return 0;
    }

    int cont = 1;
    while (in)
    {
        //Search the header.
        std::string line;
        while (in >> line && line != "HEADER");
        if (!in)
            break;

        //Read the line.
        getline( in, line );

        while (in >> line && line != "GEOLOGICAL_FEATURE");

        //Surface name.
        std::string surfaceName, surfaceType;

        //Get the surface name.
        in >> surfaceName;
        while (in >> line && line != "GEOLOGICAL_TYPE");

        in >> surfaceType;

        printf( "%d:\nReading %s %s...\n", cont, surfaceType == "top" ? "horizon" : "fault", surfaceName.c_str( ) );
        cont++;

        //Le a superficie.
        readSurface( in, surfaceName, surfaceType );
    }
    return 0;
}

