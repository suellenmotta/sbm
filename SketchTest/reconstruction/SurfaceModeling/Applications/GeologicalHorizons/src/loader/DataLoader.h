/* 
 * File:   DataLoader.h
 * Author: ncortez
 *
 * Created on October 13, 2016, 1:55 PM
 */

#ifndef DATALOADER_H
#define DATALOADER_H

#include "../modeler/GeologicalSurfaceModeling.h"
#include <vector>

class DataLoader
{
private:

    enum SurfaceType
    {
        HORIZON = 0,
        FAULT = 1,
        SECTION_LIMIT = 2
    };
private:

    /**
     * File's path to all curves.
     */
    std::string _curvesPath;

    /**
     * File's path to all initial meshes.
     */
    std::string _meshesPath;
private:

    /**
     * Allocate auxiliary vectors.
     * @param hSurfaces - vector with one surface for each time for each horizon.
     * @param fSurfaces - vector with one surface for each time for each fault.
     * @param horizons - vector with all horizons.
     * @param faults - vector with all faults.
     * @param geoTimes - vector with all geological times.
     * @param hTimes - a n-vector, where n is the number of horizons, that stores
     * the max time for each horizon.
     * @param fTimes - a n-vector, where n is the number of faults, that stores
     * the max time for each fault.
     * @param maxTime - the max time that any surface appears.
     */
    void allocateAuxilliarVectors( std::vector< std::vector< HorizonSurface* > >& hSurfaces,
                                   std::vector< std::vector< FaultSurface* > >& fSurfaces,
                                   std::vector< Horizon* >& horizons,
                                   std::vector< Fault* >& faults,
                                   std::vector< GeologicalTime* >& geoTimes,
                                   const std::vector<unsigned int>& hTimes,
                                   const std::vector<unsigned int>& fTimes,
                                   const unsigned int& maxTime );

    /**
     * Get information about the number of horizons and faults and your times.
     * @param hTimes - a n-vector, where n is the number of horizons, that stores
     * the max time for each horizon.
     * @param fTimes - a n-vector, where n is the number of faults, that stores
     * the max time for each fault.
     * @param maxTime - the max time that any surface appears.
     */
    void getCountInformation( std::vector< unsigned int >& hTimes,
                              std::vector< unsigned int >& fTimes,
                              unsigned int& maxTime );


    /**
     * Get Horizon or Fault name from its file path
     * @param path
     */
    std::string getNameFromPath( std::string path );

    /**
     * create a CornerTable from a file with points and triangle mesh
     * @param path
     * @return 
     */
    CornerTable* getCornerTableFromFile( std::string path, std::string &type );

    /**
     * Build a GeologicalSurfaceModeling that contains all necessary information.
     * @param hTimes - a n-vector, where n is the number of horizons, that stores
     * the max time for each horizon.
     * @param fTimes - a n-vector, where n is the number of faults, that stores
     * the max time for each fault.
     * @param maxTime - the max time that any surface appears.
     * @return - GeologicalSurfaceModeling that contains all necessary information.
     */
    GeologicalSurfaceModeling* generateGeologicalModel( const std::vector< unsigned int >& hTimes,
                                                        const std::vector< unsigned int >& fTimes,
                                                        const unsigned int& maxTime );
    /**
     * Read a curve from a file in path.
     * @param path - file path to read the curve.
     * @return - the read curve.
     */
    Curve* readCurve( const char* path );

    /**
     * Read the surface meshes.
     * @param gSurfModel - a geological surface modeling object with all 
     * structure already allocated.
     */
    void readSurfaceMeshes( GeologicalSurfaceModeling* gSurfModel );
public:

    /**
     * Default constructor.
     */
    DataLoader( );

    /**
     * Load the file and build a GeologicalSurfaceModeling modeling object.
     * @param path - file's path to all curves.
     * @param meshPath - file's path to all meshes.
     * @return - a GeologicalSurfaceModeling modeling object.
     */
    GeologicalSurfaceModeling* load( const std::string& path, const std::string& meshPath );

    /**
     * Destructor.
     */
    virtual ~DataLoader( );
};

#endif /* DATALOADER_H */

