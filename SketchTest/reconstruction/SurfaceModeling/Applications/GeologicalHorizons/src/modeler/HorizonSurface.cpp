#include "HorizonSurface.h"



HorizonSurface::HorizonSurface( ) : Surface( )
{
}



HorizonSurface::HorizonSurface( std::vector<Curve*>& h, std::vector< std::vector<CurveIntersection> >& i ) : Surface( )
{
    _curves = h;
    _intersections = i;
}



HorizonSurface::HorizonSurface( std::vector<Curve*>& h, CornerTable* tmesh, unsigned int time ) : Surface( )
{
    _curves = h;
    _triangleMesh = tmesh;
    _time = time;
}



HorizonSurface::HorizonSurface( std::vector<Curve*>& h, CornerTable* tmesh, std::vector< std::vector<CurveIntersection> >& i, unsigned int time ) : Surface( )
{
    _curves = h;
    _triangleMesh = tmesh;
    _intersections = i;
    _time = time;
}



void HorizonSurface::addHorizonCurve( Curve* c )
{
    _curves.push_back( c );
    _intersections.resize( _curves.size( ) );
}



Curve* HorizonSurface::getHorizonCurve( unsigned int i )
{
    return _curves[i];
}



const Curve* HorizonSurface::getHorizonCurve( unsigned int i ) const
{
    return _curves[i];
}



std::vector<Curve*>& HorizonSurface::getHorizonCurves( )
{
    return _curves;
}



const std::vector<Curve*>& HorizonSurface::getHorizonCurves( ) const
{
    return _curves;
}



void HorizonSurface::setHorizonCurves( std::vector<Curve*>& horizonCurves )
{
    _curves = horizonCurves;
    _intersections.resize( _curves.size( ) );
}



CornerTable* HorizonSurface::generateMesh( )
{
    return 0;
}
