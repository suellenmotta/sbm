#include "Horizon.h"
#include "HorizonSurface.h"



Horizon::Horizon( )
{
}



Horizon::~Horizon( )
{
    for (int i = 0; i < _horizonSurfaces.size( ); i++)
    {
        delete _horizonSurfaces[i];
    }
}



Horizon::Horizon( std::vector<HorizonSurface*>& surfaces )
{
    _horizonSurfaces = surfaces;
}



void Horizon::addHorizonSurface( HorizonSurface* surface )
{
    _horizonSurfaces.push_back( surface );
}



int Horizon::getLastTime( ) const
{
    return ( int ) _horizonSurfaces.size( ) - 1;
}



HorizonSurface* Horizon::getHorizonSurface( unsigned int t )
{
    if (t < _horizonSurfaces.size( ))
        return _horizonSurfaces[t];
    return 0;
}



const HorizonSurface* Horizon::getHorizonSurface( unsigned int t ) const
{
    if (t < _horizonSurfaces.size( ))
        return _horizonSurfaces[t];
    return 0;
}



std::vector<HorizonSurface*>& Horizon::getHorizonSurfaces( )
{
    return _horizonSurfaces;
}



const std::vector<HorizonSurface*>& Horizon::getHorizonSurfaces( ) const
{
    return _horizonSurfaces;
}



void Horizon::setHorizonSurfaces( const std::vector<HorizonSurface*>& surfaces )
{
    _horizonSurfaces = surfaces;
}



std::string Horizon::getName( )
{
    return _name;
}



void Horizon::setName( std::string name )
{
    _name = name;
}