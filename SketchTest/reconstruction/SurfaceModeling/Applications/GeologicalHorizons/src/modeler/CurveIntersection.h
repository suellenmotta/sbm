#ifndef CURVE_INTERSECTION_H
#define CURVE_INTERSECTION_H

class Curve;

/**
 * Define an intersection between a current curve and another curve.
 */
class CurveIntersection
{
private:
    /**
     * Curve that the current curve intersects.
     */
    Curve* _curve;

    /**
     * Polyline index that the intersections happens.
     */
    unsigned int _polylineIndex;

    /**
     * Intersection point on polyline.
     */
    unsigned int _pointIndex;


public:
    /**
     * Default constructor.
     */
    CurveIntersection( );
    
    /*
     *Destructor
     */
    ~CurveIntersection( );

    /**
     * Constructor that receives a curve, a polyline index, and a point index (inside the polyline) where the intersection happens.
     */
    CurveIntersection( Curve* curve, unsigned int polyline, unsigned int pointIndex );

    /**
     * Get the curve that the current curve intersects.
     */
    Curve* getCurve( );

    /**
     * Get the curve that the current curve intersects.
     */
    const Curve* getCurve( ) const;

    /**
     * Get the polyline index, where the intersection happens.
     */
    unsigned int getPolyline( ) const;

    /**
     * Get the point index, where the intersection happens.
     */
    unsigned int getPointIndex( ) const;

    /**
     * Set the intersection data.
     */
    void set( Curve* curve, unsigned int polyline, unsigned int pointIndex );

    /**
     * Set a curve that intersects with the current curve.
     */
    void setCurve( Curve* curve );

    /**
     * Set the polyline that intersects with the another curve.
     */
    void setPolyline( unsigned int polyline );

    /**
     * Set the point that intersects with the another curve.
     */
    void setPointIndex( unsigned int p );

};
#endif
