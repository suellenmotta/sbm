#ifndef HORIZON_H
#define HORIZON_H

#include <vector>

#include "Fault.h"
#include "HorizonSurface.h"

/**
 * Store information about one horizon, i. e., all horizon versions. 
 * 
 * One version for each time.
 */
class Horizon
{
private:
    /**
     * Store one horizon surface for each time. The horizon on position i is relative to the time i. This means that the horizon list is contiguos.
     */
    std::vector<HorizonSurface*> _horizonSurfaces;
    
    /**
     * Name of the horizon
     */
    std::string _name;


public:
    /**
     * Default constructor.
     */
    Horizon( );
    
    /**
     * Destructor
     */
    ~Horizon( );

    /**
     * Construtor that receives a list of horizons.
     */
    Horizon( std::vector<HorizonSurface*>& surfaces );

    /**
     * Add an horizon surface to the vector.The surfaces must be added following the time order.
     */
    void addHorizonSurface( HorizonSurface* surface );

    /**
     * Get the last time that has an horizon.
     */
    int getLastTime( ) const;

    /**
     * Return the horizon surface of the time t.
     */
    HorizonSurface* getHorizonSurface( unsigned int t );

    /**
     * Return the horizon surface of the time t.
     */
    const HorizonSurface* getHorizonSurface( unsigned int t ) const;

    /**
     * Return the horizon surface vector.
     */
    std::vector<HorizonSurface*>& getHorizonSurfaces( );

    /**
     * Return the horizon surface vector.
     */
    const std::vector<HorizonSurface*>& getHorizonSurfaces( ) const;

    /**
     * Define a vector of horizon surfaces. One for each time, in the time order.
     */
    void setHorizonSurfaces( const std::vector<HorizonSurface*>& surfaces );
    
    /**
     * Get the name of the horizon
     */
    std::string getName( );
    
    /**
     * Set the name of the horizon
     */
    void setName( std::string name );

};
#endif
