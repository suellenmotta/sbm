#include "CurveIntersection.h"
#include "Curve.h"



CurveIntersection::CurveIntersection( )
{
}


CurveIntersection::~CurveIntersection( )
{
}


CurveIntersection::CurveIntersection( Curve* curve, unsigned int polyline, unsigned int pointIndex )
{
    _curve = curve;
    _polylineIndex = polyline;
    _pointIndex = pointIndex;
}



Curve* CurveIntersection::getCurve( )
{
    return _curve;
}



const Curve* CurveIntersection::getCurve( ) const
{
    return _curve;
}



unsigned int CurveIntersection::getPolyline( ) const
{
    return _polylineIndex;
}



unsigned int CurveIntersection::getPointIndex( ) const
{
    return _pointIndex;
}



void CurveIntersection::set( Curve* curve, unsigned int polyline, unsigned int pointIndex )
{
    _curve = curve;
    _polylineIndex = polyline;
    _pointIndex = pointIndex;
}



void CurveIntersection::setCurve( Curve* curve )
{
    _curve = curve;
}



void CurveIntersection::setPolyline( unsigned int polyline )
{
    _polylineIndex = polyline;
}



void CurveIntersection::setPointIndex( unsigned int p )
{
    _pointIndex = p;
}
