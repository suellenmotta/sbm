#ifndef FAULT_H
#define FAULT_H

#include <vector>

#include "FaultSurface.h"

/**
 * Store information about one fault for each time.
 */
class Fault
{
private:
    /**
     * List of faults for each time. The fault on position i is relative to the time i.
     */
    std::vector<FaultSurface*> _faultSurfaces;
    
    /**
     * Name of the fault
     */
    std::string _name;


public:
    /**
     * Default construtor.
     */
    Fault( );

    /**
     * Construtor that receives a list of fault surfaces.
     */
    Fault( std::vector<FaultSurface*>& faults );
    
    /*
     * Destructor
     */
    ~Fault( );

    /**
     * Add a fault surface to the vector.The faults must be added following the time order.
     */
    void addFaultSurface( FaultSurface* fault );

    /**
     * Get the last time that has a fault surface.
     */
    int getLastTime( ) const;

    /**
     * Return the fault surface of the time t.
     */
    FaultSurface* getFaultSurface( unsigned int t );

    /**
     * Return the fault surface of the time t.
     */
    const FaultSurface* getFaultSurface( unsigned int t ) const;

    /**
     * Get the fault surfaces vector.
     */
    std::vector<FaultSurface*>& getFaultSurfaces( );

    /**
     * Get the fault surfaces vector.
     */
    const std::vector<FaultSurface*>& getFaultSurfaces( ) const;

    /**
     * Define a vector of fault surfaces. One for each time, in the time order.
     */
    void setFaultSurfaces( std::vector<FaultSurface*>& surfaces );

    /**
     * Get the name of the fault
     */
    std::string getName( );
    
    /**
     * Set the name of the fault
     */
    void setName( std::string name );
};
#endif
