#ifndef CURVE_H
#define CURVE_H

#include <vector>
#include "Polyline.h"

/**
 * Represent a curve that defines, in part, a fault or horizon.
 */
class Curve
{
private:
    /**
     * The set of polylines that defines a curve.
     */
    std::vector<Polyline> _polylines;


public:
    /**
     * Default constructor.
     */
    Curve( );
    
    /*
     * Destructor
     */
    ~Curve( );

    /**
     * Construtor that receives the polylines
     * .
     */
    Curve( std::vector<Polyline>& p );

    /**
     * Add a new polyline to the current curve.
     */
    void addPolyline( Polyline& p );

    /**
     * Get the number of polylines.
     */
    unsigned int getNumberPolylines( ) const;

    /**
     * Get the polyline of index p.
     */
    Polyline& getPolyline( unsigned int p );

    /**
     * Get the polyline of index p.
     */
    const Polyline& getPolyline( unsigned int p ) const;

    /**
     * Get all polylines.
     */
    std::vector<Polyline>& getPolylines( );

    /**
     * Get all polylines.
     */
    const std::vector<Polyline>& getPolylines( ) const;

    /**
     * Define the set of polylines.
     */
    void setPolylines( std::vector<Polyline>& polylines );

};
#endif
