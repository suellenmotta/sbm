#include "Fault.h"
#include "FaultSurface.h"



Fault::Fault( )
{
}



Fault::Fault( std::vector<FaultSurface*>& faults )
{
    _faultSurfaces = faults;
}



Fault::~Fault( )
{
    for (int i = 0; i < _faultSurfaces.size( ); i++)
    {
        delete _faultSurfaces[i];
    }
}



void Fault::addFaultSurface( FaultSurface* fault )
{
    _faultSurfaces.push_back( fault );
}



int Fault::getLastTime( ) const
{
    return ( int ) _faultSurfaces.size( ) - 1;
}



FaultSurface* Fault::getFaultSurface( unsigned int t )
{
    if (t < _faultSurfaces.size( ))
        return _faultSurfaces[t];
    return 0;
}



const FaultSurface* Fault::getFaultSurface( unsigned int t ) const
{
    if (t < _faultSurfaces.size( ))
        return _faultSurfaces[t];
    return 0;
}



std::vector<FaultSurface*>& Fault::getFaultSurfaces( )
{
    return _faultSurfaces;
}



const std::vector<FaultSurface*>& Fault::getFaultSurfaces( ) const
{
    return _faultSurfaces;
}



void Fault::setFaultSurfaces( std::vector<FaultSurface*>& surfaces )
{
    _faultSurfaces = surfaces;
}



std::string Fault::getName( )
{
    return _name;
}



void Fault::setName( std::string name )
{
    _name = name;
}


