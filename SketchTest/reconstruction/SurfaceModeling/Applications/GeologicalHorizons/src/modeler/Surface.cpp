#include "Surface.h"



Surface::Surface( )
{
    _time = 0;
    _triangleMesh = 0;
}


Surface::~Surface( )
{
    delete _triangleMesh;
    for(int i = 0; i < _curves.size(); i++)
    {
        delete _curves[i];
    }
}



Surface::Surface( CornerTable* tmesh, unsigned int time )
{
    _triangleMesh = tmesh;
    _time = time;
}



void Surface::addIntersection( unsigned int c, CurveIntersection i )
{  
    _intersections[c].push_back( i );
}



CurveIntersection& Surface::getIntersection( unsigned int c, unsigned int i )
{
    return _intersections[c][i];
}



const CurveIntersection& Surface::getIntersection( unsigned int c, unsigned int i ) const
{
    return _intersections[c][i];
}



unsigned int Surface::getNumberIntersections( unsigned int c ) const
{
    return _intersections[c].size( );
}



std::vector<CurveIntersection>& Surface::getIntersection( unsigned int c )
{
    return _intersections[c];
}



const std::vector<CurveIntersection>& Surface::getIntersection( unsigned int c ) const
{
    return _intersections[c];
}



CornerTable* Surface::getTriangleMesh( )
{
    return _triangleMesh;
}



const CornerTable* Surface::getTriangleMesh( ) const
{
    return _triangleMesh;
}



void Surface::setTriangleMesh( CornerTable* triangleMesh )
{
    _triangleMesh = triangleMesh;
}



unsigned int Surface::getTime( ) const
{
    return _time;
}



void Surface::setIntersection( unsigned int c, std::vector<CurveIntersection>& intersection )
{
    _intersections[c] = intersection;
}



void Surface::setTime( unsigned int time )
{
    _time = time;
}

