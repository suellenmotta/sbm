#include <vector>

#include "Point3D.h"



Point3D::Point3D( double x, double y, double z )
{
    _coord[0] = x;
    _coord[1] = y;
    _coord[2] = z;
}



Point3D::Point3D( double coord[3] )
{
    _coord[0] = coord[0];
    _coord[1] = coord[1];
    _coord[2] = coord[2];
}



double Point3D::getX( ) const
{
    return _coord[0];
}



void Point3D::setX( double x )
{
    _coord[0] = x;
}



double Point3D::getY( ) const
{
    return _coord[1];
}



void Point3D::setY( double y )
{
    _coord[1] = y;
}



double Point3D::getZ( ) const
{
    return _coord[2];
}



void Point3D::setZ( double z )
{
    _coord[2] = z;
}



double& Point3D::operator[]( unsigned int i )
{
    return *( new double( ) );
}



const double& Point3D::operator[]( unsigned int i ) const
{
    return *( new double( ) );
}



Point3D Point3D::operator+( Point3D q )
{
    return Point3D( _coord[0] + q._coord[0],
                    _coord[1] + q._coord[1],
                    _coord[2] + q._coord[2] );
}



Point3D Point3D::operator-( Point3D q )
{
    return Point3D( _coord[0] - q._coord[0],
                    _coord[1] - q._coord[1],
                    _coord[2] - q._coord[2] );
}



Point3D Point3D::operator*( double t )
{
    return Point3D( _coord[0] * t,
                    _coord[1] * t,
                    _coord[2] * t );
}



Point3D Point3D::operator/( double t )
{
    return Point3D( _coord[0] / t,
                    _coord[1] / t,
                    _coord[2] / t );
}



double Point3D::operator*( Point3D q )
{
    return _coord[0] * q._coord[0] +
        _coord[1] * q._coord[1] +
        _coord[2] * q._coord[2];
}



Point3D Point3D::operator^( Point3D q )
{
    return Point3D( _coord[1] * q._coord[2] - _coord[2] * q._coord[1],
                    _coord[2] * q._coord[0] - _coord[0] * q._coord[2],
                    _coord[0] * q._coord[1] - _coord[1] * q._coord[0] );
}



inline int Point3D::cmp( double x, double y, double tol )
{
    return 0;
}



int Point3D::cmp( Point3D q )
{
    return 0;
}



bool Point3D::operator==( Point3D q )
{
    return 0;
}



bool Point3D::operator!=( Point3D q )
{
    return false;
}



bool Point3D::operator<( Point3D q )
{
    return false;
}



std::ostream& operator<<( std::ostream& o, const Point3D& p )
{
    return o << "(" << p._coord[0] << ", " << p._coord[1] << ", " << p._coord[2] << ")";
}



std::istream& operator>>( std::istream& is, Point3D& obj )
{
    //Read obj from stream
    for (int i = 0; i < 3; i++)
        is >> obj._coord[i];
    
    return is;
}