#ifndef POINT3_D_H
#define POINT3_D_H

#include <fstream>

class Point3D
{
private:
    double _coord[3];


private:
    /**
     * Compare x and y with a tolerance. 
     */
    inline int cmp( double x, double y, double tol );

public:
    /**
     * Default constructor.
     */
    Point3D( double x = 0, double y = 0, double z = 0 );

    /**
     * Construtor that receives a (x,y,z) as a double vector.
     */
    Point3D( double coord[3] );


    /**
     * Get the x coordinate value.
     */
    double getX( ) const;

    /**
     * Set the x coordinate value.
     */
    void setX( double x );

    /**
     * Get the y coordinate value.
     */
    double getY( ) const;

    /**
     * Set the y coordinate value.
     */
    void setY( double y );

    /**
     * Get the z coordinate value.
     */
    double getZ( ) const;

    /**
     * Set the z coordinate value.
     */
    void setZ( double z );

    /**
     * Get the i coordinate value pointer.
     */
    double& operator[]( unsigned int i );

    /**
     * Get the i coordinate value pointer.
     */
    const double& operator[]( unsigned int i ) const;

    /**
     * Add two points.
     */
    Point3D operator+( Point3D q );

    /**
     * Subtract two points.
     */
    Point3D operator-( Point3D q );

    /**
     * Multiply each coordinate by a double.
     */
    Point3D operator*( double t );

    /**
     * Divide each coordinate by a double
     */
    Point3D operator/( double t );

    /**
     *  Scalar multiplication between points.
     */
    double operator*( Point3D q );

    /**
     * Crossproduct between two points.
     */
    Point3D operator^( Point3D q );

    /**
     * Compare two points.
     */
    int cmp( Point3D q );

    /**
     * Compare if two points are equal.
     */
    bool operator==( Point3D q );

    /**
     * Compare if two points are differents. 
     */
    bool operator!=( Point3D q );

    /**
     * Compare if a point is smaller than other.
     */
    bool operator<( Point3D q );

    /**
     * Return a ostream to output the point information  (x, y, z).
     */
    friend std::ostream& operator<<( std::ostream& o, const Point3D& p );

    /**
     * Read a point from stream.
     * @param is - the current stream.
     * @param obj - the point to be read.
     * @return - the stream.
     */
    friend std::istream& operator>>( std::istream& is, Point3D& obj );

};
#endif
