#ifndef GEOLOGICAL_SURFACE_MODELING_H
#define GEOLOGICAL_SURFACE_MODELING_H

#include <vector>

#include "Horizon.h"
#include "Fault.h"
#include "GeologicalTime.h"

/**
 * All necessary information to surface modeling. All horizons, all faults, and all geological times.
 */
class GeologicalSurfaceModeling
{
private:

    /**
     * Error that defines the limit for a point intersection
     */
    double _epsIntersections;

    /**
     * List of all horizons on model.
     */
    std::vector<Horizon*> _horizons;

    /**
     * List of all faults on model.
     */
    std::vector<Fault*> _faults;

    /**
     * List of all geological times on model.
     */
    std::vector<GeologicalTime*> _geologicalTimes;


public:
    /**
     * Defaut construtor.
     */
    GeologicalSurfaceModeling( );

    /**
     * Destrutor.
     */
    ~GeologicalSurfaceModeling( );

    /**
     * Contructor that receives all horizons, faults and geological times.
     */
    GeologicalSurfaceModeling( std::vector<Horizon*>& horizons, std::vector<Fault*>& faults, std::vector<GeologicalTime*>& times );

    /**
     * Compute all the curves intersections between each horizon and each fault 
     */
    void computeIntersections( );

    /**
     * Compute all curves intersections between a given horizon curve point and each fault 
     * @param hor - horizonIndex
     * @param timeH - horizonSurfaceIndex
     * @param curvH - curveIndex
     * @param polH - polylineIndex
     * @param pH - pointIndex
     * @param pointH - point that will be computed intersection of.
     */
    void computeHorizonPointIntersections( const unsigned int hor,
                                           const unsigned int timeH,
                                           const unsigned int curvH,
                                           const unsigned int polH,
                                           const unsigned int pH,
                                           const Point3D& pointH );

    /**
     * Add a new horizon.
     */
    void addHorizon( Horizon* h );

    /**
     * Add a new fault.
     */
    void addFault( Fault* f );

    /**
     * Add a new geological time.
     */
    void addGeologicalTime( GeologicalTime* g );

    /**
     * Get the number of faults.
     */
    unsigned int getNumberFaults( ) const;

    /**
     * Get the number of geological times.
     */
    unsigned int getNumberGeologicalTimes( ) const;

    /**
     * Get the number of horizons.
     */
    unsigned int getNumberHorizons( ) const;

    /**
     * Get the fault of index f.
     */
    Fault* getFault( unsigned int f );

    /**
     * Get the fault of index f.
     */
    const Fault* getFault( unsigned int f ) const;

    /**
     * Get the horizon of index h.
     */
    Horizon* getHorizon( unsigned int h );

    /**
     * Get the horizon of index h.
     */
    const Horizon* getHorizon( unsigned int h ) const;

    /**
     * Get the geological time of index g.
     */
    GeologicalTime* getGeologicalTime( unsigned int g );

    /**
     * Get the geological time of index g.
     */
    const GeologicalTime* getGeologicalTime( unsigned int g ) const;

    /**
     * Perform the optimization method to build the surfaces. It will call the run method for all geological times. This is the most expensive method.
     */
    void run( );

};
#endif
