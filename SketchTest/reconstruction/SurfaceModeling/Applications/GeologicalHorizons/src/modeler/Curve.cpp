#include "Curve.h"



Curve::Curve( )
{
}



Curve::~Curve( )
{
}



Curve::Curve( std::vector<Polyline>& p )
{
    _polylines = p;
}



void Curve::addPolyline( Polyline& p )
{
    _polylines.push_back( p );
}



unsigned int Curve::getNumberPolylines( ) const
{
    return _polylines.size( );
}



Polyline& Curve::getPolyline( unsigned int p )
{
    return _polylines[p];
}



const Polyline& Curve::getPolyline( unsigned int p ) const
{
    return _polylines[p];
}



std::vector<Polyline>& Curve::getPolylines( )
{
    return _polylines;
}



const std::vector<Polyline>& Curve::getPolylines( ) const
{
    return _polylines;
}



void Curve::setPolylines( std::vector<Polyline>& polylines )
{
    _polylines = polylines;
}
