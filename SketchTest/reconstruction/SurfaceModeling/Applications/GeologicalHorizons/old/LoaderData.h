/* 
 * File:   LoaderData.h
 * Author: jcoelho
 *
 * Created on April 9, 2016, 8:59 PM
 */

#ifndef LOADERDATA_H
#define	LOADERDATA_H

#include <string>


namespace SurfaceModeling
{
    class ModelingData;
    class SurfaceData;

    enum SurfaceType
    {
        HORIZON = 0,
        FAULT = 1,
        SECTION = 2
    };

    struct PreLoaderData
    {
        unsigned int _section;
        unsigned int _time;
        unsigned int _surfaceIndex;
        unsigned int _type;
        std::string _path;

        /**
         * Compare two data in order to put them in a better order to allocate
         * the surfaces
         * @param d - the pre-loader to be compared.
         * @return - true if the order is corrected and false otherwise.
         */
        bool operator<( const PreLoaderData d ) const
        {
            if ( _time < d._time )
                return true;
            if ( _time > d._time )
                return false;

            if ( _section < d._section )
                return true;
            if ( _section > d._section )
                return false;

            if ( _surfaceIndex < d._surfaceIndex )
                return true;
            if ( _surfaceIndex > d._surfaceIndex )
                return false;

            return _type < d._type;
        }
    };

    class LoaderData
    {
    public:
        /**
         * Default Constructor.
         */
        LoaderData( );

        /**
         * Load the surfaces at sections.
         * @param path - path of file that contains a list of surfaces.
         * @return - loaded data.
         */
        ModelingData* loadData( const std::string& path );

        /**
         * 
         */
        virtual ~LoaderData( );
    private:

        /**
         * Read a surface file.
         * @param path - surface path.
         * @param surfaceId - surface id.
         * @param sectionId - section id.
         * @return  - surface data of the read surface.
         */
        SurfaceData* loadSurface( const std::string& path,
                                  const unsigned int surfaceId,
                                  const unsigned int sectionId );
    };
};

#endif	/* LOADERDATA_H */

