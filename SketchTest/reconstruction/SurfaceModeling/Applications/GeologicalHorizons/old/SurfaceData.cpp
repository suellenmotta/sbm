/* 
 * File:   Surface.cpp
 * Author: jcoelho
 * 
 * Created on April 6, 2016, 8:23 PM
 */

#include "SurfaceData.h"

namespace SurfaceModeling
{



    SurfaceData::SurfaceData( const unsigned int surfaceId, const unsigned int sectionId )
    {
        _surfaceId = surfaceId;
        _sectionId = sectionId;
    }



    void SurfaceData::addPolyline( const Polyline& poly )
    {
        _surface.push_back( poly );
    }



    SurfaceData::~SurfaceData( )
    {
    }

}