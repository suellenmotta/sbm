/* 
 * File:   ModelingData.h
 * Author: jcoelho
 *
 * Created on April 8, 2016, 2:45 PM
 */

#ifndef MODELINGDATA_H
#define	MODELINGDATA_H
#include <vector>


namespace SurfaceModeling
{
    class TimeData;

    class ModelingData
    {
    public:
        /**
         * Default constructor.
         */
        ModelingData( );

        /**
         * Destructor.
         */
        virtual ~ModelingData( );

        /**
         * Add a new time data information.
         * @param data - time data information.
         */
        void addTimeData( TimeData* data );

        /**
         * Get all section on a time t.
         * @param t - index of time to return the information. This is the sorted
         * index and not the time value.
         * @return - a constant data pointer to requested information.
         */
        const TimeData* getTimeData( const unsigned int t ) const;
    private:

        /**
         * A set of time data information. Each time data contains all sections
         * at that time.
         */
        std::vector<TimeData*> _timeData;
    };
};
#endif	/* MODELINGDATA_H */

