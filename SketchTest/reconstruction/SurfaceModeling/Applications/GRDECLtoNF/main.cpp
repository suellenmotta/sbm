/* 
 * File:   main.cpp
 * Author: jcoelho
 *
 * Created on August 22, 2017, 9:47 AM
 */


#include <cstdlib>
#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <cassert>
#include <map>

#include "ert/ecl/ecl_box.h"
#include "ert/ecl/ecl_kw.h"

struct KeywordAndFilePos
{
    std::string keyword;
    long long filePos;
};

struct Point
{
    double x, y, z;
};

struct Cell
{



    Cell( )
    {
        active = false;
    }
    Point p[8];
    bool active;

};

struct Property
{
    std::vector<double> data;
    std::string name;
};

struct Model
{
    std::vector<Cell> cellsMesh;
    std::vector<Property> properties;

    int numActiveCells;
    int nx, ny, nz;
};



void findKeywordsOnFile( const std::string &fileName, std::vector< KeywordAndFilePos >& keywords )
{
    std::ifstream data( fileName.c_str( ) );

    std::string line;

    //Read all line.
    while (std::getline( data, line ))
    {
        //Verify if the line starts with a letter.
        if (line.size( ) && std::isalpha( line[0] ))
        {
            KeywordAndFilePos keyPos;

            //Get the position on file.
            long long l = data.tellg( );

            //Get the position of the first letter.
            keyPos.filePos = l - line.size( );

            size_t first = line.find_first_not_of( " \r\n\t" );
            size_t last = line.find_last_not_of( " \r\n\t" );

            //Extract the keyword.
            keyPos.keyword = line.substr( first, ( last - first + 1 ) );

            //Save the keyword and position.
            keywords.push_back( keyPos );
        }
    }

    for (unsigned int i = 0; i < keywords.size( ); i++)
    {
        std::cout << keywords[i].keyword << " - " << keywords[i].filePos << std::endl;
    }
}



void findGridKeywordPositions( const std::vector< KeywordAndFilePos >& keywordsAndFilePos,
                               long long* coordPos, long long* zcornPos, long long* specgridPos,
                               long long* actnumPos, long long* mapaxesPos )
{
    assert( coordPos && zcornPos && specgridPos && actnumPos && mapaxesPos );

    for (unsigned int i = 0; i < keywordsAndFilePos.size( ); i++)
    {
        if (keywordsAndFilePos[i].keyword == "COORD")
        {
            *coordPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "ZCORN")
        {
            *zcornPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "SPECGRID")
        {
            *specgridPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "ACTNUM")
        {
            *actnumPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "MAPAXES")
        {
            *mapaxesPos = keywordsAndFilePos[i].filePos;
        }
    }
}



void writeNF( const std::string& name, const Model& m )
{

    unsigned int numberNodes = m.cellsMesh.size( ) * 8;
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << name << std::endl;
        return;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Aug/17'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numberNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numberNodes << std::endl << std::endl;
    int idx = 1;
    for (unsigned int i = 0; i < m.cellsMesh.size( ); i++)
    {
        for (unsigned int v = 0; v < 8; v++)
        {
            out << idx << " " << m.cellsMesh[i].p[v].x << " " << m.cellsMesh[i].p[v].y << " " << m.cellsMesh[i].p[v].z << std::endl;
            idx++;
        }
    }

    out << "%ELEMENT" << std::endl;
    out << m.cellsMesh.size( ) << std::endl << std::endl;

    idx = 1;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << m.cellsMesh.size( ) << std::endl << std::endl;

    for (int i = 0; i < m.cellsMesh.size( ); i++)
    {
        out << idx << " 0 1 ";
        for (unsigned int v = 0; v < 8; v++)
        {
            out << 8 * i + v + 1 << " ";
        }
        out << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR" << std::endl;
    out << m.properties.size( ) << std::endl;
    if (m.properties.size( ))
    {
        std::string propertiesString;

        for (unsigned int i = 0; i < m.properties.size( ); i++)
        {
            propertiesString += "'" + m.properties[i].name + "' ";
        }
        out << propertiesString << std::endl << std::endl;
        
        out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR.DATA" << std::endl;

        out << m.cellsMesh.size( ) << std::endl;

        idx = 1;
        for (unsigned int i = 0; i < m.cellsMesh.size( ); i++)
        {
            out << idx << " 1" << std::endl;
            for (unsigned int p = 0; p < m.properties.size( ); p++)
            {
                out << m.properties[p].data[i] << " ";
            }
            out << std::endl;
            idx++;
        }
    }
    out << "%END" << std::endl;
}



const std::vector<std::string>& invalidPropertyDataKeywords( )
{

    static std::vector<std::string> keywords;
    static bool isInitialized = false;
    if (!isInitialized)
    {
        // Related to geometry
        keywords.push_back( "COORD" );
        keywords.push_back( "ZCORN" );
        keywords.push_back( "SPECGRID" );
        keywords.push_back( "MAPAXES" );
        keywords.push_back( "FAULTS" );
        keywords.push_back( "MAPUNITS" );
        keywords.push_back( "GRIDUNIT" );
        keywords.push_back( "COORDSYS" );

        isInitialized = true;
    }
    return keywords;
}



bool isValidDataKeyword( const std::string& keyword )
{
    const std::vector<std::string>& keywordsToSkip = invalidPropertyDataKeywords( );
    for (const std::string keywordToSkip : keywordsToSkip)
    {
        if (keywordToSkip == keyword)
        {
            return false;
        }
    }

    return true;
}



int findOrCreateResult( const std::string& newResultName, const Model& m )
{
    int resultIndex = -1;
    for (int i = 0; i < m.properties.size( ); i++)
    {
        if (newResultName == m.properties[i].name)
        {
            return i;
        }
    }

    return resultIndex;
}



bool readDataFromKeyword( ecl_kw_type* eclipseKeywordData, const std::string& resultName, Model& m )
{
    int numCells = m.cellsMesh.size( );
    int numActiveCells = m.numActiveCells;

    bool mathingItemCount = false;
    {
        size_t itemCount = static_cast < size_t > ( ecl_kw_get_size( eclipseKeywordData ) );
        if (itemCount == numCells)
        {
            mathingItemCount = true;
        }
        if (itemCount == numActiveCells)
        {
            mathingItemCount = true;
        }
    }

    if (!mathingItemCount)
        return false;

    int resultIndex = findOrCreateResult( resultName, m );
    if (resultIndex < 0)
        return false;

    std::vector< double >& newPropertyData = m.properties[resultIndex].data;

    newPropertyData.resize( ecl_kw_get_size( eclipseKeywordData ), 1e100 );
    ecl_kw_get_data_as_double( eclipseKeywordData, newPropertyData.data( ) );

    return true;
}



void allocateProperties( const std::vector< KeywordAndFilePos >& fileKeywords, Model& m )
{
    for (unsigned int i = 0; i < fileKeywords.size( ); ++i)
    {
        if (isValidDataKeyword( fileKeywords[i].keyword ))
        {
            Property p;
            p.name = fileKeywords[i].keyword;
            m.properties.push_back( p );
        }
    }
}



bool readProperties( const std::vector< KeywordAndFilePos >& fileKeywords, const std::string& fileName, Model& m )
{
    //Allocate properties on model.
    allocateProperties( fileKeywords, m );

    //Open file.
    FILE* gridFilePointer = fopen( fileName.c_str( ), "r" );

    if (!gridFilePointer || !fileKeywords.size( ))
    {
        return false;
    }

    for (unsigned int i = 0; i < fileKeywords.size( ); ++i)
    {
        //Check if the world represents a property.
        if (!isValidDataKeyword( fileKeywords[i].keyword ))
            continue;

        //Move to correct position on file.
        fseek( gridFilePointer, fileKeywords[i].filePos, SEEK_SET );

        //Read the data.
        ecl_kw_type* eclipseKeywordData = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) );

        //Check if the data is correct.
        if (eclipseKeywordData)
        {
            readDataFromKeyword( eclipseKeywordData, fileKeywords[i].keyword, m );
            ecl_kw_free( eclipseKeywordData );
        }
    }

    fclose( gridFilePointer );
    return true;
}



bool readGeometry( const std::vector< KeywordAndFilePos >& keywordsAndFilePos, const std::string& fileName, Model& m )
{
    long long coordPos = -1;
    long long zcornPos = -1;
    long long specgridPos = -1;
    long long actnumPos = -1;
    long long mapaxesPos = -1;

    findGridKeywordPositions( keywordsAndFilePos, &coordPos, &zcornPos, &specgridPos, &actnumPos, &mapaxesPos );

    if (coordPos < 0 || zcornPos < 0 || specgridPos < 0)
    {
        printf( "Error! Invalid grid\n" );
        return false;
    }

    FILE* gridFilePointer = fopen( fileName.c_str( ), "r" );
    if (!gridFilePointer)
        return false;


    ecl_kw_type* specGridKw = NULL;
    ecl_kw_type* zCornKw = NULL;
    ecl_kw_type* coordKw = NULL;
    ecl_kw_type* actNumKw = NULL;
    ecl_kw_type* mapAxesKw = NULL;

    // Try to read all the needed keywords. Early exit if some are not found

    bool allKwReadOk = true;

    fseek( gridFilePointer, specgridPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( specGridKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_INT_TYPE ) ) );

    fseek( gridFilePointer, zcornPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( zCornKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) ) );

    fseek( gridFilePointer, coordPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( coordKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) ) );

    // If ACTNUM is not defined, this pointer will be NULL, which is a valid condition
    if (actnumPos >= 0)
    {
        fseek( gridFilePointer, actnumPos, SEEK_SET );
        allKwReadOk = allKwReadOk && NULL != ( actNumKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_INT_TYPE ) ) );
    }

    // If MAPAXES is not defined, this pointer will be NULL, which is a valid condition
    if (mapaxesPos >= 0)
    {
        fseek( gridFilePointer, mapaxesPos, SEEK_SET );
        mapAxesKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) );
    }

    if (!allKwReadOk)
    {
        if (specGridKw) ecl_kw_free( specGridKw );
        if (zCornKw) ecl_kw_free( zCornKw );
        if (coordKw) ecl_kw_free( coordKw );
        if (actNumKw) ecl_kw_free( actNumKw );
        if (mapAxesKw) ecl_kw_free( mapAxesKw );

        fclose( gridFilePointer );

        return false;
    }

    int nx = ecl_kw_iget_int( specGridKw, 0 );
    int ny = ecl_kw_iget_int( specGridKw, 1 );
    int nz = ecl_kw_iget_int( specGridKw, 2 );

    m.nx = nx;
    m.ny = ny;
    m.nz = nz;

    ecl_grid_type* inputGrid = ecl_grid_alloc_GRDECL_kw( nx, ny, nz, zCornKw, coordKw, actNumKw, mapAxesKw );

    int cellCount = ecl_grid_get_global_size( inputGrid );


    // The indexing conventions for vertices in ECLIPSE
    //
    //      2-------------3              
    //     /|            /|                  
    //    / |           / |               /j   
    //   /  |          /  |              /     
    //  0-------------1   |             *---i  
    //  |   |         |   |             | 
    //  |   6---------|---7             |
    //  |  /          |  /              |k
    //  | /           | /
    //  |/            |/
    //  4-------------5
    //  vertex indices

    std::vector<Cell>& cellsMesh = m.cellsMesh;
    cellsMesh.resize( cellCount );

    int grdeclToNF[8] = { 4, 0, 5, 1, 7, 3, 6, 2 };

    double minZ = 1e100, maxZ = -1e100;

    int activeCells = 0;
    for (int c = 0; c < cellCount; ++c)
    {
        int matrixActiveIndex = ecl_grid_get_active_index1( inputGrid, c );
        if (matrixActiveIndex != -1)
        {
            cellsMesh[c].active = matrixActiveIndex;
            activeCells++;
        }

        // Corner coordinates
        for (int v = 0; v < 8; ++v)
        {
            Point pt;
            ecl_grid_get_cell_corner_xyz1( inputGrid, c, v, &pt.x, &pt.y, &pt.z );
            //pt.z *= -1;
            cellsMesh[c].p[grdeclToNF[v]] = pt;
            if (cellsMesh[c].active)
            {
                minZ = std::min( minZ, pt.z );
                maxZ = std::max( maxZ, pt.z );
            }
        }
    }
    printf( "%.3lf %.3lf => %.3lf    %d\n", minZ, maxZ, maxZ - minZ, activeCells );

    fclose( gridFilePointer );

    return true;
}



int main( )
{
    Model m;
    std::vector< KeywordAndFilePos > keywordsAndFilePos;
        std::string fileName( "FN-SS-KP-10-101.grdecl" );
//    std::string fileName( "IRAP_1005.GRDECL" );

    findKeywordsOnFile( fileName, keywordsAndFilePos );

    readGeometry( keywordsAndFilePos, fileName, m );
    readProperties( keywordsAndFilePos, fileName, m );

    writeNF( "teste.pos", m );

    return 0;
}
