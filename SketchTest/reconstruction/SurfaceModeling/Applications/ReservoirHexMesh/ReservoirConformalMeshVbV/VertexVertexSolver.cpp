/* 
 * File:   VertexVertexSolver.cpp
 * Author: jcoelho
 * 
 * Created on April 26, 2017, 1:02 PM
 */


#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <omp.h>

#include "VertexVertexSolver.h"
#include "../grid3d.h"
#include "../Point3D.h"
#include "../../../../../libs/Timer/Timer.h"

//Vector to access the neighborhood to set matrix elements.
const int neighbouhood[25][4] = {
    {+0, +0, -2, +02 },
    {+0, -1, -1, +04 },
    {-1, +0, -1, +04 },
    {+0, +0, -1, -24 },
    {+1, +0, -1, +04 },
    {+0, +1, -1, +04 },
    {+0, -2, +0, +02 },
    {-1, -1, +0, +04 },
    {+0, -1, +0, -24 },
    {+1, -1, +0, +04 },
    {-2, +0, +0, +02 },
    {-1, +0, +0, -24 },
    {+0, +0, +0, +84 },
    {+1, +0, +0, -24 },
    {+2, +0, +0, +02 },
    {-1, +1, +0, +04 },
    {+0, +1, +0, -24 },
    {+1, +1, +0, +04 },
    {+0, +2, +0, +02 },
    {+0, -1, +1, +04 },
    {-1, +0, +1, +04 },
    {+0, +0, +1, -24 },
    {+1, +0, +1, +04 },
    {+0, +1, +1, +04 },
    {+0, +0, +2, +02 }
};



/***********************PUBLIC METHODS*****************************************/

VertexVertexSolver::VertexVertexSolver( )
{
    _eps = 1e-3;
    _iterations = 0;
    _maxSubIterations = 5000;
    _maxIterations = 4;
    _pen = 5.0;
    _showLog = true;
    _stepsNumber = 1;
    _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
}



void VertexVertexSolver::computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                                   const std::vector<bool>& activePoints,
                                                   ReservoirMesh* reservoir,
                                                   int ri, int rj, int rk,
                                                   const unsigned int fixedBoundaryPoints )
{
    if (fixedBoundaryPoints < 1)
    {
        if (_showLog)
        {
            printf( "\n    Error: the number of fixed boundary cells must be greater then 0!\n" );
        }
        return;
    }

    if (_showLog)
    {
        printf( "\n    Pre-processing active cells and fixed boundary points...\n" );
        printf( "\n    Computing re-index vector...\n" );
    }

    Timer t;

    //Save the number of fixed boundary cells.
    _fixedBoundaryPoints = fixedBoundaryPoints;
    _roi = ri;
    _roj = rj;
    _rok = rk;
    _reservoir = reservoir;

    //Get the grid size.
    const unsigned int gridSize = grid.Size( );

    //Vector to map between point on grid to variable space.
    std::vector<int> reindexPoints;

    //Compute the fixed points.
    const unsigned int fixedPoints = preprocessFixedPoints( activePoints, reindexPoints );

    //Compute the number of unknown points.
    const unsigned int numberUnknowPoints = gridSize - fixedPoints;

    //Vector to store the number of active points on a vertex.
    std::vector<char> numActivePoints( numberUnknowPoints, 0 );

    if (_showLog)
    {
        t.printTime( "        Total time" );
        printf( "\n    %u points was fixed on preprocessing step...\n", fixedPoints );
        printf( "    %u points have unknown values...\n", numberUnknowPoints );
        printf( "    %u points have unknown values...\n", numberUnknowPoints );
    }

    t.restart( );

    //Allocate auxiliary vectors.
    _r.resize( numberUnknowPoints, 0 );
    _p.resize( numberUnknowPoints, 0 );
    _q.resize( numberUnknowPoints, 0 );
    _sum.resize( _numberOfThreads );

    if (_showLog)
    {
        t.printTime( "        Total time" );
        double space = sizeof (unsigned int ) * gridSize + sizeof (FPType ) * ( gridSize + 3 * numberUnknowPoints ) + numberUnknowPoints;
        space /= 1048576.0;

        printf( "\n   Necessary GPU space: %.2lf MB\n", space );
        printf( "\n    **************************OPTIMIZING**************************\n\n" );
        printf( "\n    Optimizing the model with %u variables...\n\n", 3 * numberUnknowPoints );
    }

    //Optimize dimension by dimension.
    for (unsigned int dimension = 0; dimension < 3; dimension++)
    {
        t.restart( );
        if (_showLog)
        {
            printf( "        DIMENSION %u:", dimension + 1 );
            printf( "\n\n            ITERATION           GRADIENT NORM           TIME\n" );
        }

        //Compute the vectors values for and specific dimension.
        computeBVector( dimension, grid, reindexPoints, numActivePoints );

        //Compute gradient. This is equivalent to compute the initial residual.
        FPType gnorm = computeInitialResidual( grid, dimension, reindexPoints, _r, numActivePoints );

        _iterations = 0;
        while (_iterations++ < _maxIterations && gnorm > _eps * _eps)
        {
            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                t.printTime( "" );
            }

            //Solve linear system.
            gnorm = PCG( grid, dimension, reindexPoints, gnorm, numActivePoints );

            //Compute the vectors values for and specific dimension.
            computeBVector( dimension, grid, reindexPoints, numActivePoints );
        }

        if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
        {
            printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
            t.printTime( "" );
        }
    }

    //Free memory.
    _r.clear( );
    _p.clear( );
    _q.clear( );
    _sum.clear( );
}



FPType VertexVertexSolver::getEPS( ) const
{
    return _eps;
}



unsigned int VertexVertexSolver::getMaxSubIterations( ) const
{
    return _maxSubIterations;
}



unsigned int VertexVertexSolver::getMaxIterations( ) const
{
    return _maxIterations;
}



unsigned int VertexVertexSolver::getNumberIterations( ) const
{
    return _iterations;
}



unsigned int VertexVertexSolver::getNumberfOfThreads( ) const
{
    return _numberOfThreads;
}



unsigned int VertexVertexSolver::getStepsNumber( ) const
{
    return _stepsNumber;
}



VertexVertexSolver::~VertexVertexSolver( )
{

}



void VertexVertexSolver::setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) )
{
    _displayInformation = displayInformation;
}



void VertexVertexSolver::setEPS( const FPType eps )
{
    _eps = eps;
}



void VertexVertexSolver::setMaxSubIterations( const unsigned int maxSubIter )
{
    _maxSubIterations = maxSubIter;
}



void VertexVertexSolver::setMaxIterations( const unsigned int maxIter )
{
    _maxIterations = maxIter;
}



void VertexVertexSolver::setNumberOfThreads( const unsigned int numThreads )
{
    _numberOfThreads = numThreads;
}



void VertexVertexSolver::setPenalization( const FPType pen )
{
    _pen = pen;
}



void VertexVertexSolver::showLog( const bool log )
{
    _showLog = log;
}



void VertexVertexSolver::setStepsNumber( const unsigned int sn )
{
    _stepsNumber = sn;
}



/***********************PRIVATE METHODS****************************************/
void VertexVertexSolver::computeReindexPointsVector( const std::vector<bool> &activePoints,
                                                     std::vector<int>& reindexPoints ) const
{
    //Allocate storage.
    reindexPoints.resize( activePoints.size( ), -1 );

    //Compute new indexes.
    for (unsigned int i = 0, index = 0; i < activePoints.size( ); i++)
    {
        if (!activePoints[i])
        {
            reindexPoints[i] = index;
            index++;
        }
        else
        {
            reindexPoints[i] = -1;
        }
    }
}



void VertexVertexSolver::computeBVector( const unsigned int dimension,
                                         const DsGrid3D<Point3D>& grid,
                                         const std::vector<int>& reindexPoints,
                                         std::vector<char>& numActivePoints )
{
    //Get the number of variables to be computed.
    unsigned int unknownVariables = _p.size( );

    //Initialize vectors.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownVariables; i++)
    {
        _p[i] = 0.0;
        numActivePoints[i] = 0;
    }

    //Get the reservoir dimensions.
    int ri = _reservoir->getGrid( ).Ni( );
    int rj = _reservoir->getGrid( ).Nj( );
    int rk = _reservoir->getGrid( ).Nk( );

    int dx[8][3] = {
        {0, 0, 0 },
        {1, 0, 0 },
        {0, 1, 0 },
        {1, 1, 0 },
        {0, 0, 1 },
        {1, 0, 1 },
        {0, 1, 1 },
        {1, 1, 1 }
    };

    //Check orientations.
    for (int i = 0; i < 8; i++)
    {
        if (_reservoir->invertedX( ))
        {
            dx[i][0] = !dx[i][0];
        }

        if (_reservoir->invertedY( ))
        {
            dx[i][0] = !dx[i][1];
        }

        if (_reservoir->invertedZ( ))
        {
            dx[i][2] = !dx[i][2];
        }
    }

    //Get the reservoir nodes.
    const std::vector<Point3D>& nodes = _reservoir->getReservoirNodes( );

    //Add constraints about active cells and to conform the mesh.
    for (int k = 0; k < rk; k++)
    {
        for (int j = 0; j < rj; j++)
        {
            for (int i = 0; i < ri; i++)
            {
                //Get the cells information.
                const CellMesh& c = _reservoir->getGrid( ).GetRef( i, j, k );

                //Penalize the move on active cell vertices.
                if (c.active( ) && !c.fixed( ))
                {
                    //Compute the cell index.
                    int oi = i + _roi;
                    int oj = j + _roj;
                    int ok = k + _rok;

                    if (_reservoir->invertedX( ))
                        oi = ( ri - 1 ) - i + _roi;

                    if (_reservoir->invertedY( ))
                        oj = ( rj - 1 ) - j + _roj;

                    if (_reservoir->invertedZ( ))
                        ok = ( rk - 1 ) - k + _rok;

                    for (int p = 0; p < 8; p++)
                    {
                        //Get the vertex index.
                        int vi = oi + dx[p][0];
                        int vj = oj + dx[p][1];
                        int vk = ok + dx[p][2];

                        //Get the vertex grid index.
                        int gidx = grid.GetIndex( vi, vj, vk );

                        //Verify if it is a variable.
                        if (reindexPoints[gidx] != -1)
                        {
                            //Get the vertex index on nodes vector.
                            int v = c.point( p );

                            //Get the variable index.
                            int vidx = reindexPoints[gidx];

                            //Count the number penalizations.
                            numActivePoints[vidx]++;

                            //Add to b vector.
                            _p[vidx] += 2 * _pen * nodes[v][dimension];
                        }
                    }
                }
            }
        }
    }

    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    //Compute the b vector.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int gidx = grid.GetIndex( i, j, k );

                if (reindexPoints[gidx] != -1)
                {
                    //Get the variable index.
                    int vidx = reindexPoints[gidx];

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx2 = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx2] == -1)
                            {
                                //Get the variable index.
                                _p[vidx] -= neighbouhood[n][3] * grid.GetRef( gidx2 ).x[dimension];
                            }
                        }
                    }
                }
            }
        }
    }
}



unsigned int VertexVertexSolver::preprocessFixedPoints( const std::vector<bool>& activePoints,
                                                        std::vector<int>& reindexPoints ) const
{
    //Get the number of fixed points.
    std::vector<unsigned int> sum( _numberOfThreads, 0 );
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < activePoints.size( ); i++)
    {
        if (activePoints[i])
        {
            int thread = omp_get_thread_num( );
            sum[thread]++;
        }
    }

    unsigned totalFixedPoints = 0;
    for (unsigned int i = 0; i < sum.size( ); i++)
    {
        totalFixedPoints += sum[i];
    }

    //From active points, compute the re-index vector.
    computeReindexPointsVector( activePoints, reindexPoints );

    return totalFixedPoints;
}



/***********************PCG ALG. METHODS***************************************/
FPType VertexVertexSolver::computeInitialResidual( const DsGrid3D<Point3D>& grid,
                                                   const unsigned int dimension,
                                                   const std::vector<int>& reindexPoints,
                                                   std::vector<FPType>& r,
                                                   std::vector<char>& numActivePoints )
{
    //Get the number of variables.
    unsigned int unknownPoints = r.size( );

    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        r[i] = 0.0;
    }

    //Multiply matrix.
    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;


    //Multiply Ax
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int currentPoint = grid.GetIndex( i, j, k );
                if (reindexPoints[currentPoint] != -1)
                {
                    //Get variable index.
                    currentPoint = reindexPoints[currentPoint];

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx] != -1)
                            {
                                FPType aij = neighbouhood[n][3] +
                                    ( ( vi == 0 && vj == 0 && vk == 0 ) ? ( 2.0 * _pen * numActivePoints[currentPoint] ) : 0.0 );

                                r[currentPoint] += aij * grid.GetRef( gidx ).x[dimension];
                            }
                        }
                    }
                }
            }
        }
    }

    //Compute residual vector: b - Ax.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        r[i] = _p[i] - r[i];

        int thread = omp_get_thread_num( );
        _sum[thread] += r[i] * r[i];
    }

    FPType rnorm2 = 0.0;
    for (unsigned int i = 0; i < _numberOfThreads; i++)
    {
        rnorm2 += _sum[i];
        _sum[i] = 0.0;
    }

    return rnorm2;
}



void VertexVertexSolver::matrixVec( const DsGrid3D<Point3D>& grid,
                                    const unsigned int dimension,
                                    const std::vector<int>& reindexPoints,
                                    const std::vector<FPType>& p,
                                    std::vector<FPType>& q,
                                    std::vector<char>& numActivePoints )
{
    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    //Get the number of unknown points.
    const unsigned int unknownPoints = q.size( );

    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        q[i] = 0.0;
    }

    //Multiply Ap.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int currentPoint = grid.GetIndex( i, j, k );
                if (reindexPoints[currentPoint] != -1)
                {
                    //Get variable index.
                    currentPoint = reindexPoints[currentPoint];

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx] != -1)
                            {
                                const int vidx = reindexPoints[gidx];
                                FPType aij = neighbouhood[n][3] +
                                    ( ( vi == 0 && vj == 0 && vk == 0 ) ? ( 2.0 * _pen * numActivePoints[currentPoint] ) : 0.0 );

                                q[currentPoint] += aij * p[vidx];
                            }
                        }
                    }
                }
            }
        }
    }
}



FPType VertexVertexSolver::PCG( const DsGrid3D<Point3D>& grid,
                                const unsigned int dimension,
                                const std::vector<int>& reindexPoints,
                                FPType rnorm2,
                                std::vector<char>& numActivePoints )
{
    //Get the number of variables.
    unsigned int n = _r.size( );

    //        FPType rnorm2 = computeInitialResidual( grid, dimension, reindexPoints, _r );

    //Allocate auxiliary vectors.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < n; i++)
    {
        _p[i] = 0.0;
        _q[i] = 0.0;
    }

    //Compute initial residual.
    FPType rho = 0.0, rhoOld = 1.0;

    //Iterate.
    for (unsigned int iter = 0; iter < _maxSubIterations; iter++)
    {
        //For this algorithm rho is equal to <r, r>/84. Not more :(
        rho = 0;

        //Compute rho.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            int thread = omp_get_thread_num( );
            _sum[thread] += _r[i] * ( _r[i] / ( 84.0 + 2.0 * _pen * numActivePoints[i] ) );
        }

        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            rho += _sum[i];
            _sum[i] = 0.0;
        }

        //Compute p.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            _p[i] = _r[i] / ( 84.0 + 2.0 * _pen * numActivePoints[i] ) + rho * _p[i] / rhoOld;
        }

        //q = A*p and gradient = ||Ax - b||
        matrixVec( grid, dimension, reindexPoints, _p, _q, numActivePoints );

        //<p, q>
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            int thread = omp_get_thread_num( );
            _sum[thread] += _p[i] * _q[i];
        }

        FPType dotPQ = 0;
        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            dotPQ += _sum[i];
            _sum[i] = 0.0;
        }

        //Compute x(k) = x(k-1) + alpha * p(k)
        updateSolution( grid, dimension, reindexPoints, _p, rho, dotPQ, numActivePoints );

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            _r[i] = _r[i] - rho * _q[i] / dotPQ;

            int thread = omp_get_thread_num( );
            _sum[thread] += _r[i] * _r[i];
        }

        rnorm2 = 0;
        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            rnorm2 += _sum[i];
            _sum[i] = 0.0;
        }

        rhoOld = rho;
        if (rnorm2 <= _eps * _eps)
        {
            return rnorm2;
        }
    }
    return rnorm2;
}



void VertexVertexSolver::updateSolution( const DsGrid3D<Point3D>& grid,
                                         const unsigned int dimension,
                                         const std::vector<int>& reindexPoints,
                                         const std::vector<FPType>& p,
                                         const FPType rho,
                                         const FPType dotPQ,
                                         std::vector<char>& numActivePoints ) const
{
    //Get limits.
    const int lk = grid.Nk( ) - _fixedBoundaryPoints;
    const int lj = grid.Nj( ) - _fixedBoundaryPoints;
    const int li = grid.Ni( ) - _fixedBoundaryPoints;

    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int gidx = grid.GetIndex( i, j, k );
                if (reindexPoints[gidx] != -1)
                {
                    //Get variable index.
                    const int vidx = reindexPoints[gidx];

                    Point3D &point = grid.GetRef( gidx );
                    FPType &x = point.x[dimension];

                    x = x + rho * p[vidx] / dotPQ;
                }
            }
        }
    }
}
