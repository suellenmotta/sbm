#include "../FromReservoirToGeomecanic/ReservoirMesh.h"
#include "../FromReservoirToGeomecanic/ReservoirHorizon.h"
#include "ReservoirConformalMeshEbE.h"
#include "../FromReservoirToGeomecanic/WriteOutputFiles.h"
#include <fstream>
#include <map>
#include "../../../../../libs/Timer/Timer.h"


const double tx = 354720.50000000000;
const double ty = 7516884.9999999991;
using namespace std;



void rotatePoint( double& x, double& y, double angle )
{
    double xx = x * cos( angle ) - y * sin( angle );
    double yy = x * sin( angle ) + y * cos( angle );

    x = xx;
    y = yy;
}



void generateTopBaseHorizons( const ReservoirMesh& r )
{
    Point3D pMin( r.getXMin( ), r.getYMin( ), 0 );
    Point3D pMax( r.getXMax( ), r.getYMax( ), 0 );

    std::ofstream top( "Brugge/top.dat" ), bas( "Brugge/base.dat" );

    const int n = 10;

    for (int j = 0; j < n; j++)
    {
        double ly = ( double ) j / ( n - 1 );
        double y = ( 1 - ly ) * pMin[1] + ly * pMax[1];

        for (int i = 0; i < n; i++)
        {
            double lx = ( double ) i / ( n - 1 );

            double x = ( 1 - lx ) * pMin[0] + lx * pMax[0];

            top << x << " " << y << " " << pMax[2] << endl;
            bas << x << " " << y << " " << pMin[2] << endl;
        }
    }
}



bool buildHorizon( std::string hIn, std::string hout,
                   double rotAngle, ReservoirMesh& r,
                   double zoffset )
{
    std::ifstream in( hIn.c_str( ) );
    if (!in)
    {
        cout << "Falha ao abrir o arquivo " << hIn << endl;
        return false;
    }

    std::set<Point3D> input;

    Point3D p;
    while (in >> p)
    {
        input.insert( p );
    }

    std::vector<Point3D> coord( input.begin( ), input.end( ) );

    //Translate data
    Point3D t( -tx, -ty, 0 );
    for (unsigned int i = 0; i < coord.size( ); i++)
    {
        coord[i] += t;
    }

    Point3D hMin( +1e100, +1e100, +1e100 ), hMax( -1e100, -1e100, -1e100 );

    //Rotate points.
    for (unsigned int i = 0; i < coord.size( ); i++)
    {
        rotatePoint( coord[i][0], coord[i][1], rotAngle );

        hMin[0] = std::min( hMin[0], coord[i][0] );
        hMin[1] = std::min( hMin[1], coord[i][1] );
        hMin[2] = std::min( hMin[2], coord[i][2] );

        hMax[0] = std::max( hMax[0], coord[i][0] );
        hMax[1] = std::max( hMax[1], coord[i][1] );
        hMax[2] = std::max( hMax[2], coord[i][2] );
    }

    //horizon dimensions.
    double hDimen[2] = { hMax[0] - hMin[0], hMax[1] - hMin[1] };

    Point3D rMin( r.getXMin( ), r.getYMin( ), r.getZMin( ) ),
        rMax( r.getXMax( ), r.getYMax( ), r.getZMax( ) );


    //Scale the horizon.
    for (unsigned int i = 0; i < coord.size( ); i++)
    {
        Point3D &p = coord[i];

        for (int d = 0; d <= 1; d++)
        {
            double t = ( coord[i][d] - hMin[d] ) / hDimen[d];
            p[d] = ( 1 - t ) * rMin[d] + t * rMax[d];
        }
        p[2] += zoffset;
    }

    std::ofstream out( hout.c_str( ) );
    if (!out)
    {
        cout << "Falha ao abrir o arquivo " << hout << endl;
        return false;
    }

    for (unsigned int i = 0; i < coord.size( ); i++)
    {
        out << coord[i] << endl;
    }
}



void buildBruggeModel( const std::string& path, double penalization, double eps, int numThreads )
{
    Timer t;
    const unsigned int numHori = 7;
    std::string horizonNames[numHori] = { "Brugge/top.dat", "Brugge/base.dat",
                                         "Brugge/Horizonte1.dat", "Brugge/Horizonte2.dat",
                                         "Brugge/Horizonte3.dat", "Brugge/Horizonte3_Base.dat",
                                         "Brugge/Horizonte4.dat" };

    std::vector<ReservoirHorizon> horizons( numHori );

    ReservoirConformalMeshEbE m;

    ReservoirMesh r;
    r.readReservoirEclipseGridFile( path + "Brugge/FN-SS-KP-10-101.grdecl" );

    m.setReservoirMesh( &r );

    for (unsigned int i = 0; i < numHori; i++)
    {
        horizons[i].readFile( path + horizonNames[i] );

        horizons[i].preprocess( );

        m.addHorizon( &horizons[i] );
    }

    m.writeNeutralFileForInput( "brugge.pos" );
    return;
    double fact = ( double ) r.getGrid( ).Nj( ) / r.getGrid( ).Ni( );

    m.setNumCellsI( 12 );
    m.setNumCellsJ( 12 * fact + 0.5 );

    double w = r.getWidth( ) / 2;
    double l = r.getLengh( ) / 2;

    m.setIIncrement( w );
    m.setJIncrement( l );
    m.setNumThreads( numThreads );
    m.setPenalization( penalization );
    m.setEPS( eps );

    int s[8] = { 4, 4, 14, 9, 16, 3, 4, 4 };
    std::vector<int> slices( s, s + 8 );
    m.setNumCellsAmongSurfaces( slices );

    m.generateInitialMesh( );
    //    m.writeNeutralFileForOuput( "/home/v/jcoelho/FN-SS-KP-10-101Full.pos" );
    m.simplifyMesh( );

    m.writeNeutralFileForOuput( "/home/v/jcoelho/FN-SS-KP-10-101Simply.pos" );
    //    m.writeNeutralFileReservoirOuput( "/home/v/jcoelho/FN-SS-KP-10-101-Reservoir.pos" );
    t.printTime( "Total time" );
}



void buildBruggeModelReservoirOnly( const std::string& path, double penalization, double eps, int numThreads, std::string name, double facscal )
{
    Timer t;
    const unsigned int numHori = 2;
    std::string horizonNames[numHori] = { "Brugge/baseRot.dat", "Brugge/topRot.dat" };

    std::vector<ReservoirHorizon> horizons( numHori );

    ReservoirConformalMeshEbE m;

    ReservoirMesh r;
    r.readReservoirEclipseGridFile( path + "Brugge/2017/BRUGGE2017.GRDECL" );

    m.setReservoirMesh( &r );

    for (unsigned int i = 0; i < numHori; i++)
    {
        horizons[i].readFile( path + horizonNames[i] );

        horizons[i].preprocess( );

        m.addHorizon( &horizons[i] );
    }

    m.setFacScal( facscal );
    m.setNumCellsI( 45 );
    m.setNumCellsJ( 30 );

    double l = std::max( r.getWidth( ), r.getLengh( ) );

    m.setIIncrement( 1.5 * l );
    m.setJIncrement( 1.5 * l );
    m.setNumThreads( numThreads );
    m.setPenalization( penalization );
    m.setEPS( eps );

    int s[3] = { 50, 9, 25 };
    std::vector<int> slices( s, s + 3 );
    m.setNumCellsAmongSurfaces( slices );

    m.generateInitialMesh( );
    //    m.writeNeutralFileForOuput( "/home/v/jcoelho/FN-SS-KP-10-101Full.pos" );

    //    m.simplifyMesh( );

    m.optimizeMesh( );

    //    m.writeNeutralFileSimplifiedOuput( "/home/v/jcoelho/Brugge2017.pos" );

//    m.writeNeutralFileForOuput( "/home/v/jcoelho/" + name );
    //    m.writeNeutralFileSimplifiedOuput( "/home/v/jcoelho/" + name );

    t.printTime( "Total time" );

    //WriteOutputFiles wri( &m, "PorepBrugge.txt" );
    //wri.writeFiles( "brugge" );
}



void buildNorneModel( const std::string& path, double penalization, double eps, int numThreads, std::string name )
{
    Timer t;
    const unsigned int numHori = 2;
    std::string horizonNames[numHori] = { "Norne/top.dat", "Norne/base.dat" };

    //    std::string horizonNames[numHori] = { "Norne/top.dat", "Norne/base.dat",
    //                                         "Norne/Horizonte1.dat", "Norne/Horizonte2.dat",
    //                                         "Norne/Horizonte3.dat", "Norne/Horizonte3_Base.dat",
    //                                         "Norne/Horizonte4.dat" };

    std::vector<ReservoirHorizon> horizons( numHori );

    ReservoirConformalMeshEbE m;

    ReservoirMesh r;
    r.readReservoirEclipseGridFile( path + "Norne/IRAP_1005.GRDECL" );

    m.setReservoirMesh( &r );

    for (unsigned int i = 0; i < numHori; i++)
    {
        horizons[i].readFile( path + horizonNames[i] );

        horizons[i].preprocess( );

        m.addHorizon( &horizons[i] );
    }

    int ni = r.getGrid( ).Ni( ) / 2;
    int nj = r.getGrid( ).Nj( ) / 2;

    m.setNumCellsI( ni );
    m.setNumCellsJ( nj );

    double w = r.getWidth( ) / 2;
    double l = r.getLengh( ) / 2;

    m.setIIncrement( w );
    m.setJIncrement( l );
    m.setNumThreads( numThreads );
    m.setPenalization( penalization );
    m.setEPS( eps );


    int s[8] = { 16, 22, 16 };
    //    int s[8] = { 4, 4, 14, 22, 16, 3, 4, 4 };
    //    std::vector<int> slices( s, s + 8 );
    std::vector<int> slices( s, s + 2 );
    m.setNumCellsAmongSurfaces( slices );

    m.generateInitialMesh( );

    //    m.writeNeutralFileForOuput( "/home/v/jcoelho/FN-SS-KP-10-101Full.pos" );
    //    m.simplifyMesh( );

    m.optimizeMesh( );
    m.simplifyMesh( );

    //    m.writeNeutralFileSimplifiedOuput( "/home/v/jcoelho/IRAP_1005.pos" );
    //    m.writeNeutralFileForOuput( "/home/v/jcoelho/IRAP_1005-Reservoir.pos" );
    m.writeNeutralFileSimplifiedOuput( "/home/v/jcoelho/" + name );
    t.printTime( "Total time" );
}



void singleReservoir( const std::string& path, double penalization, int numThreads )
{
    const unsigned int numHori = 2;
    std::vector<ReservoirHorizon> horizons( numHori );

    ReservoirConformalMeshEbE m;

    ReservoirMesh r;
    r.readReservoirEclipseGridFile( path );

    std::string horizonNames[numHori] = { "top.dat", "base.dat" };

    generateTopBaseHorizons( r );
    for (unsigned int i = 0; i < numHori; i++)
    {
        horizons[i].readFile( path + horizonNames[i] );
        horizons[i].preprocess( );
        m.addHorizon( &horizons[i] );
    }

    int ni = r.getGrid( ).Ni( ) / 2;
    int nj = r.getGrid( ).Nj( ) / 2;

    m.setNumCellsI( ni );
    m.setNumCellsJ( nj );

    double w = r.getWidth( ) / 2;
    double l = r.getLengh( ) / 2;

    m.setIIncrement( w );
    m.setJIncrement( l );
    m.setNumThreads( numThreads );
    m.setPenalization( penalization );
    m.generateInitialMesh( );

}



int main( int argc, char** argv )
{
    if (argc < 5)
    {
        printf( "Please, give the horizons path...\n" );
        return 0;
    }
    Timer t;


    const unsigned int numThreads = atoi( argv[1] );
    const FPType penalization = atof( argv[2] );
    const FPType eps = atof( argv[3] );
    const std::string path = argv[4];


    ReservoirMesh r;
    r.readReservoirEclipseGridFile( path + "Brugge/2017/BRUGGE2017.GRDECL" );

    double offset = r.getActiveZMax( ) - r.getActiveZMin( );
    double hover = 0;
    double hunder = ( r.getActiveZMin( ) - 2 * offset );

    std::string name = "mesh" + std::string( argv[2] ) + ".pos";

    generateTopBaseHorizons( r );
    buildHorizon( path + "Brugge/top.dat", path + "Brugge/topRot.dat", 0.0, r, hover );
    buildHorizon( path + "Brugge/base.dat", path + "Brugge/baseRot.dat", 0.0, r, hunder );

    //double facScal = 0.030;
    //double facScal = 0.100;
    //double facScal = 0.150;
    //double facScal = 0.200;
    //double facScal = 0.238;
    //double facScal = 0.3;
    //double facScal = 0.300;
    //double facScal = 0.7;
    double facScal[6] = { 0.030, 0.100, 0.150, 0.200, 0.238, 0.3 };
//    double facScal[2] = { 0.238, 0.3 };
    for (int i = 0; i < 6; i++)
    {
        buildBruggeModelReservoirOnly( path, penalization, eps, numThreads, name, facScal[i] );
    }

    //ECLIPSE
    //    buildBruggeModel( path, penalization, eps, numThreads );
    //    std::string name = "mesh" + std::string( argv[2] ) + ".pos";
    //    buildNorneModel( path, penalization, eps, numThreads, name );

    //
    //    ReservoirMesh r2;
    //    r2.readReservoirEclipseGridFile( path + "Brugge/2017/BRUGGE2017.GRDECL" );
    //
    //    offset = -r.getActiveZMax( );
    //    hover = 0;
    //    hunder = ( r.getActiveZMin( ) - 2 * offset );
    //
    //    generateTopBaseHorizons( r );
    //    buildHorizon( path + "Norne/top.dat", path + "Norne/topRot.dat", 0.0, r, hover );
    //    buildHorizon( path + "Norne/base.dat", path + "Norne/baseRot.dat", 0.0, r, hunder );

    //ECLIPSE
    //    buildNorneModel( path, penalization, eps, numThreads );
    //    buildNorneModel( path, penalization,eps,  numThreads );



    //Geresim
    //    const unsigned int numHori = 8;
    //    std::string horizonNames[numHori] = { "Base.dat", "FundoMar.dat",
    //                                         "Horizonte1.dat", "Horizonte2.dat",
    //                                         "Horizonte3.dat", "Horizonte3_Base.dat", "Horizonte4.dat",
    //                                         "Horizonte5.dat" };
    //
    //    std::vector<ReservoirHorizon> horizons( numHori );
    //
    //    ReservoirConformalMeshEbE m;
    //
    //    ReservoirMesh r;
    //    r.readReservoirGeresimFile( path + "../Pituba.txt" );
    //    
    //    m.setReservoirMesh( &r );
    //
    //    for (unsigned int i = 0; i < numHori; i++)
    //    {
    //        horizons[i].readFile( path + horizonNames[i] );
    //
    //        double angle = r.getRotationAngle( );
    //        horizons[i].preprocess( tx, ty, -angle );
    //
    //        m.addHorizon( &horizons[i] );
    //    }
    //
    //    int ni = r.getGrid( ).Ni( ) / 2;
    //    int nj = r.getGrid( ).Nj( ) / 2;
    //
    //    m.setNumCellsI( ni );
    //    m.setNumCellsJ( nj );
    //
    //    double w = r.getWidth( ) / 2;
    //    double l = r.getLengh( ) / 2;
    //
    //    m.setIIncrement( w );
    //    m.setJIncrement( l );
    //    m.setNumThreads( numThreads );
    //    m.setPenalization( penalization );
    //    m.generateMesh( );
    //
    //    //    m.writeNeutralFileForInput( "/local/jcoelho/Dropbox/trans/Pituba/ModelInput.pos" );
    //    m.writeNeutralFileForOuput( "Model.pos" );
    //    //    m.writeNeutralFileRodrigo( "Pituba.pos" );
    //    //    m.writeNeutralFileForOuput( path + "Model.pos" );
    //
    t.printTime( "Total time" );

    return 0;
}
