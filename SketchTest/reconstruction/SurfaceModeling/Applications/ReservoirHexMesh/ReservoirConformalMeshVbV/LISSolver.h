/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReservoirConformalMeshLIS.h
 * Author: jcoelho
 *
 * Created on 10 de Abril de 2018, 22:31
 */

#ifndef LIS_SOLVER_H
#define LIS_SOLVER_H
#include "lis.h"
#include "../Point3D.h"
#include "../FromReservoirToGeomecanic/ReservoirMesh.h"
#include <vector>

//Forward declaration.
template <typename TYPE> class DsGrid3D;

class LISSolver
{
public:
    /**
     * Default constructor.
     */
    LISSolver( );

    /**
     * Compute the mesh geometry by an optimization process that minimize the
     * discrete Laplacian norm operator. This is equivalent to maximize the
     * elements regularity or/and Jacobian. ATTENTION: the grid geometry will
     * be changed by the optimization method.
     * @param grid - the cell grid with the geometry and topology.
     * @param activeCells - the active cells on grid.
     * @param fixedBoundaryCells - the number of cell slices must be fixed. The
     * minimum is 1.
     */
    void computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                   const std::vector<bool>& activeCells,
                                   ReservoirMesh* reservoir,
                                   int ri, int rj, int rk,
                                   const unsigned int fixedBoundaryCells = 1 );

    /**
     * Get the current tolerance.
     * @return - the current tolerance.
     */
    FPType getEPS( ) const;

    /**
     * Get the to the number of sub-iterations. It was used as an upper bound to
     * solve the linear system.
     * @param maxSubIter - new upper bound to the number of sub-iterations.
     */
    unsigned int getMaxSubIterations( ) const;

    /**
     * Get the current maximum iterations.
     * @return - the current maximum iterations.
     */
    unsigned int getMaxIterations( ) const;

    /**
     * Get the number of iterations used to solve the last model.
     * @return - number of iterations used to solve the last model.
     */
    unsigned int getNumberIterations( ) const;

    /**
     * Get the number of threads that must be used in the computations.
     * @return - number of threads that must be used in the computations.
     */
    unsigned int getNumberfOfThreads( ) const;

    /**
     * Get the number of iterations that must be performed before to display
     * the log information and call (if set) the display callback information.
     * @return - steps number.
     */
    unsigned int getStepsNumber( ) const;

    /**
     * Destructor.
     */
    virtual ~LISSolver( );

    /**
     * Set a callback function to be called after 'stepsNumber' iterations, i.e,
     * after each 'stepsNumber' iterations this function is called with gradient
     * norm of the current solution. If stepsNumber have no set, this function
     * will be called after each iteration. The current solution can be accessed
     * in Grid3D, once the current solution replace the the grid geometry.
     * @param displayInformation - pointer to a function that must be called
     * after a new solution is found. This function must return false to
     * interrupt the solve or true to continue.
     */
    void setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) );

    /**
     * Define a new tolerance to be used by solver.
     * @param eps - new tolerance to be used by solver.
     */
    void setEPS( const FPType eps );

    /**
     * Define a new upper bound to the number of sub-iterations. It will be used
     * as an upper bound to solve the linear system.
     * @param maxSubIter - new upper bound to the number of sub-iterations.
     */
    void setMaxSubIterations( const unsigned int maxSubIter );

    /**
     * Define a new upper bound to the number of iterations.
     * @param maxIter - new upper bound to the number of iterations.
     */
    void setMaxIterations( const unsigned int maxIter );

    /**
     * Define the number of threads that must be used in the computations.
     * @param numThreads - number of threads that must be used in the computations.
     */
    void setNumberOfThreads( const unsigned int numThreads );

    /**
     * Define the penalization to move the active vertices.
     * @param pen - penalization to move the active vertices.
     */
    void setPenalization( const FPType pen );

    /**
     * Set if the solver must show the log or not.
     * @param log - true if the solve need to show the log and false otherwise.
     */
    void showLog( const bool log );

    /**
     * Set the number of iterations that must be performed before to display
     * the log information and call (if set) the display callback information.
     * @param sn - steps number.
     */
    void setStepsNumber( const unsigned int sn );
private:
    /**
     * Compute the new index for points. This is equivalent to consider
     * the grid without the fixed points.
     * @param activePoints - vector that stores a boolean to know if each
     * points is fixed or not.
     * @param reindexPoints - new points index.
     */
    void computeReindexPointsVector( const std::vector<bool> &activePoints,
                                     std::vector<int>& reindexPoints ) const;

    /**
     * Compute b vector.
     * @param dimension - dimension to compute the values: 0-x, 1-y, 2-z.
     * @param grid - grid representation.
     * @param reindexPoints - map the grid index to variables index.
     */
    void computeBVector( const unsigned int dimension,
                         const DsGrid3D<Point3D>& grid,
                         const std::vector<int>& reindexPoints,
                         const unsigned int unknownPoints,
                         LIS_VECTOR x,
                         LIS_VECTOR b );

    void computeActivePointsVector( const unsigned int unknownPoints,
                                    const DsGrid3D<Point3D>& grid,
                                    const std::vector<int>& reindexPoints,
                                    std::vector<char>& numActivePoints );

    /**
     * Define which points will have fixed positions by computing a re-index
     * vector. This vector maps from grid points to variables space.
     * @param grid - grid.
     * @param activeCells - vector that defines if a cell is active or not.
     * @param fixedBoundaryCells - the number of cell slices must be fixed.
     * @param reindexPoints - maps from grid points to variables space. If the
     * re-index is equal to -1, it means that the point is known.
     * @return - the number of active points.
     */
    unsigned int preprocessFixedPoints( const std::vector<bool>& activePoints,
                                        std::vector<int>& reindexPoints )const;
private:
    /**
     * Compute the squared gradient norm.
     * @param x - current solution.
     * @param b - b vector.
     * @param ptr - vector that stores how many nonzero values are in each
     * line.
     * @param index - vector that stores the column of each element.
     * @param value - matrix values.
     * @param unknownPoints - number of unknown points on grid.
     * @return - the squared gradient norm.
     */
    double computeResidual( LIS_VECTOR x,
                                   LIS_VECTOR b,
                                   const LIS_INT *ptr,
                                   const LIS_INT *index,
                                   const LIS_SCALAR* value,
                                   const unsigned int unknownPoints ) const;

    /**
     * Copy the current solution to grid.
     * @param grid - grid.
     * @param x - current solution.
     * @param activePoints - vector to store if a point is fixed or not.
     * @param reindexPoints - map the grid index to variables index.
     * @param dimension - solution dimension.
     * @param unknownPoints - number of unknown points on grid.
     * @param fixedBoundaryCells - the number of cell slices must be fixed.
     */
    void updateSolution( DsGrid3D<Point3D>& grid,
                         LIS_VECTOR x,
                         const std::vector<int>& reindexPoints,
                         const unsigned int dimension )const;

    /**
     * Compute the number of nonzero elements.
     * @param grid - grid representation.
     * @param reindexPoints - maps from grid points to variables space. If the
     * re-index is equal to -1, it means that the point is known.
     * @return - the number of nonzero elements..
     */
    unsigned int computeNumberNonZeroEelements( const DsGrid3D<Point3D>& grid,
                                                const std::vector<int>& reindexPoints );


    /**
     * Compute how many elements there is in each matrix line.
     * @param grid - grid representation.
     * @param activePoints - points that is already known.
     * @param reindexPoints - map the grid index to variables index.
     * @param ptr - vector that stores how many nonzero values are in each
     * line.
     */
    void computeElementsPerLine( const DsGrid3D<Point3D>& grid,
                                 const std::vector<int>& reindexPoints,
                                 LIS_INT *ptr ) const;

    /**
     * Compute the CSR vectors index.
     * @param grid - grid representation.
     * @param activePoints - points that is already known.
     * @param reindexPoints - map the grid index to variables index.
     * @param unknownPoints - number of unknown points on grid.
     * @param nonZero - number of nonzero elements on matrix.
     * @param ptr - vector that stores how many nonzero values are in each
     * line.
     * @param index - vector that stores the column of each element.
     * @param value - matrix values.
     */
    void computeIndexVectors( const DsGrid3D<Point3D>& grid,
                              const std::vector<int>& reindexPoints,
                              const unsigned int unknownPoints,
                              const unsigned int nonZero, std::vector<char>& numActivePoints,
                              LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const;

    /**
     * Compute the matrix columns that have nonzero elements.
     * @param grid - grid representation.
     * @param activePoints - points that is already known.
     * @param reindexPoints - map the grid index to variables index.
     * @param fixedBoundaryCells- number of cells that are fixed on each
     * boundary.
     * @param unknownPoints - number of unknown points on grid.
     * @param nonZero - number of nonzero elements on matrix.
     * @param ptr - vector with the number of elements per line.
     * @param index - vector that stores the columns that have nonzero
     * elements.
     * @param value - matrix values.
     */
    void computeMatrixColumns( const DsGrid3D<Point3D>& grid,
                               const std::vector<int>& reindexPoints,
                               const unsigned int unknownPoints,
                               const unsigned int nonZero, std::vector<char>& numActivePoints,
                               LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const;
private:

    /**
     * Function to be called after each _stepsNumber iterations. This function
     * must return false to interrupt the solve or true to continue.
     */
    bool (*_displayInformation )( const FPType gnorm );

private:

    /**
     * Number of fixed boundary cells.
     */
    unsigned int _fixedBoundaryPoints;
private:
    /**
     * Tolerance that will be used as stopped condition by solver.
     */
    FPType _eps;

    /**
     * Number of iterations.
     */
    unsigned int _iterations;

    /**
     * The maximum sub-iterations number to solve the linear system.
     */
    unsigned int _maxSubIterations;

    /**
     * The maximum iterations number to be performed by the algorithm.
     */
    unsigned int _maxIterations;

    /**
     * The number of threads;
     */
    unsigned int _numberOfThreads;

    FPType _pen;

    ReservoirMesh *_reservoir;

    int _roi, _roj, _rok;

    /**
     * Flag that determines if the solver must show messages or not.
     */
    bool _showLog;

    /**
     * Number of iterations to show the log information.
     */
    unsigned int _stepsNumber;
};


#endif /* LIS_SOLVER_H */

