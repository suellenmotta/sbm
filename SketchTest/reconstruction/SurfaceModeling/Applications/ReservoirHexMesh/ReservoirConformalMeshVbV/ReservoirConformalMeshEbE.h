/* 
 * File:   ReservoirConformalMeshEbE.h
 * Author: jcoelho
 *
 * Created on April 25, 2017, 2:33 PM
 */

#ifndef RESERVOIRCONFORMALMESHEBE_H
#define RESERVOIRCONFORMALMESHEBE_H
#include "../FromReservoirToGeomecanic/ReservoirHorizon.h"
#include "../FromReservoirToGeomecanic/ReservoirMesh.h"
#include <set>

class ReservoirConformalMeshEbE
{
public:
    /**
     * Default constructor.
     */
    ReservoirConformalMeshEbE( );

    /**
     * Add a new horizon.
     * @param horizon - new horizon ready to be used.
     */
    void addHorizon( ReservoirHorizon* horizon );

    bool computeReservoirIndexFromFinalReservoir( int i, int j, int k, int &ri, int &rj, int &rk );
    bool computeReservoirIndexFromOutput( int i, int j, int k, int &ri, int &rj, int &rk );
    bool computeOutputIndexFromReservoir( int i, int j, int k, int &oi, int &oj, int &ok );
    bool computeFinalOutputIndexFromReservoir( int i, int j, int k, int &oi, int &oj, int &ok );
    /**
     * Generate the cell mesh.
     */
    void generateInitialMesh( );
    void optimizeMesh( );

    int getNumCells( ) const;
    int getNumPoints( ) const;
    const DsGrid3D< Point3D >& getGrid( ) const;


    /**
     * Define a new tolerance to be used by solver.
     * @param eps - new tolerance to be used by solver.
     */
    void setEPS( const FPType eps );

    /**
     * Set the increment in meters in the i direction.
     * @param i - the increment in meters in the i direction.
     */
    void setIIncrement( const double i );

    /**
     * Set the increment in meters in the j direction.
     * @param j - the increment in meters in the i direction.
     */
    void setJIncrement( const double j );

    /**
     * Define the number of cells on I direction.
     * @param i - number of cells on I direction.
     */
    void setNumCellsI( const unsigned int i );

    /**
     * Define the number of cells on J direction.
     * @param J - number of cells on J direction.
     */
    void setNumCellsJ( const unsigned int j );

    /**
     * Define the number of cell between each pair of surfaces.
     * @param slices - number of cell between each pair of surfaces.
     */
    void setNumCellsAmongSurfaces( const std::vector<int>& slices );

    /**
     * Set the number of threads that must be used.
     * @param numThreads - number of threads that must be used.
     */
    void setNumThreads( const int numThreads );

    /**
     * Define the penalization to move the active vertices.
     * @param pen - penalization to move the active vertices.
     */
    void setPenalization( const FPType pen );

    /**
     * Set a reservoir mesh.
     * @param mesh - reservoir mesh ready to be used.
     */
    void setReservoirMesh( ReservoirMesh* mesh );

    ReservoirMesh* getReservoirInputMesh( ) const;

    /**
     * Write the model on Neutral File format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writeNeutralFileForInput( const std::string& path );

    /**
     * Write the output model on Neutral File format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writeNeutralFileForOuput( const std::string& path );
    bool writeNeutralFileSimplifiedOuput( const std::string& path );

    bool writeNeutralFileReservoirOuput( const std::string& path );

    /**
     * Destructor.
     */
    virtual ~ReservoirConformalMeshEbE( );

    /**
     * Simplify the final mesh.
     * @return - true if everything is ok.
     */
    bool simplifyMesh( );

    void setFacScal( double facScal );

private:
    bool simplifyKSlices( );
    bool simplifyISlices( );
    bool simplifyJSlices( );
private:

    /**
     * Allocate the output mesh.
     */
    void allocateMemory( );

    /**
     * Check if all data is ready to be processed.
     * @return - true if everything is okay.
     */
    bool checkData( );

    void computeRegularGeometry( );

    /**
     * Compute the initial position points.
     */
    void computeInitialGeometry( );

    /**
     * Compute the number of slices between each pair of surfaces.
     */
    void computeNumberOfSlices( );


    /**
     * Compute initial cells between two layers.
     * @param zi - initial z index.
     * @param zf - final z index.
     * @param zMin - initial z value.
     * @param zMax - final z value.
     */
    void fillCellsBetweenTwoLayers( const int zi, const int zf,
                                    double zMin, const double zMax );
    void fillCellsBetweenTwoLayers2( int zi, int zf,
                                     double zMin, const double zMax );

    /**
     * Build model and compute mesh.
     */
    void optimize( );

    /**
     * Pos-process the data.
     */
    void posprocess( );


    void preprocessGeometryDimensions( );

    /**
     * Preprocess the data.
     */
    void preprocess( );

    /**
     * Check that active cells that must be fixed.
     */
    void preprocessActiveCells( );

    /**
     * Project the cells on horizon.
     * @param h - current horizon.
     * @param cells - cells to be projected.
     */
    void projectCellsOnHorizon( unsigned int h,
                                const std::set<Point3D>& cells );

    /**
     * Project horizon triangles on mesh.
     * @param slice - slice mesh to project the triangles.
     * @param h - horizon to be projected.
     * @param triangles - triangle list to project.
     * @param cells - cell where the triangles were projected.
     */
    void projectHorizonOnMesh( unsigned int slice, unsigned int h,
                               const std::vector<CornerType>& triangles,
                               std::set<Point3D>& cells );

    /**
     * Project one pont on horizon.
     * @param p - point to be projected.
     * @param h - horizon index.
     * @return true if the point was projected on horizon. False otherwise.
     */
    bool projectPointOnHorizon( Point3D& p, unsigned int h );

    /**
     * Rotate a single point.
     * @param p - point to be rotated.
     */
    void rotatePoint( Point3D& p, double angle );

    void computeHorizonsSlices( );

    /**
     * Define that points that cannot move during optimization process.
     */
    void setHardPointsPosition( );

    /**
     * Project points of an horizon and fix the mesh points.
     * @param slice - slice to be fixed.
     * @param h - current horizon to be processed.
     */
    void setHorizonPoints( unsigned int slice, unsigned int h );


    /**
     * Simplify a slice s.
     * @param s - slice index.
     * @param idxs - index when the slices start and finish.
     * @return - true if everything is ok.
     */
    bool simplifySlice( int s, const std::vector<int>& idxs );
    bool simplifyTopReservoir( int ridx, const std::vector<int>& idxs );
    bool simplifyBottomReservoir( int ridx, const std::vector<int>& idxs );
    bool simplifyIRight( );
    bool simplifyILeft( );
    bool simplifyJRight( );
    bool simplifyJLeft( );


private:
    /**
     * Active vertices.
     */
    std::vector < bool > _active;

    /**
     * Distances to be extend on I and J directions.
     */
    double _distanceI, _distanceJ;

    /**
     * Number of fixed boundary cells.
     */
    int _fixedBoundaryCells;

    std::vector<Point3D> _geometryI;
    std::vector<Point3D> _geometryJ;
    std::vector<Point3D> _geometryK;
    std::vector<Point3D> _hardCells;


    /**
     * Horizon list.
     */
    std::vector<ReservoirHorizon*> _horizons;

    /**
     * Slice of points where each horizon is fixed.
     */
    std::vector<int> _horizonSlices;

    /**
     * Input reservoir mesh.
     */
    ReservoirMesh* _inputMesh;

    /**
     * Number of cells to be extended on I and J directions.
     */
    int _numCellsRegularI, _numCellsRegularJ;
    int _numCellsI, _numCellsJ;

    /**
     * Number of threads.
     */
    int _numThreads;

    int _radiusFromFault;
    FPType _eps;

    double _facScal;

    /**
     * Output grid.
     */
    DsGrid3D< Point3D > _outputGrid;

    /**
     * Penalization to move the active vertices.
     */
    FPType _pen;

    /**
     * Slice where the reservoir begins.
     */
    int _reservoirK;

    /**
     * Number of slices between each pair of surfaces.
     */
    std::vector<int> _slicesRegular;
    std::vector<int> _slices;
};

#endif /* RESERVOIRCONFORMALMESHEBE_H */

