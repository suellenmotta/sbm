/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReservoirConformalMeshLIS.cpp
 * Author: jcoelho
 * 
 * Created on 10 de Abril de 2018, 22:31
 */



#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <omp.h>
#include "lis.h"
#include "LISSolver.h"
#include "../grid3d.h"
#include "../Point3D.h"
#include "../../../../../libs/Timer/Timer.h"

//Vector to access the neighborhood to set matrix elements.
const int neighbouhood[25][4] = {
    {+0, +0, -2, +02 },
    {+0, -1, -1, +04 },
    {-1, +0, -1, +04 },
    {+0, +0, -1, -24 },
    {+1, +0, -1, +04 },
    {+0, +1, -1, +04 },
    {+0, -2, +0, +02 },
    {-1, -1, +0, +04 },
    {+0, -1, +0, -24 },
    {+1, -1, +0, +04 },
    {-2, +0, +0, +02 },
    {-1, +0, +0, -24 },
    {+0, +0, +0, +84 },
    {+1, +0, +0, -24 },
    {+2, +0, +0, +02 },
    {-1, +1, +0, +04 },
    {+0, +1, +0, -24 },
    {+1, +1, +0, +04 },
    {+0, +2, +0, +02 },
    {+0, -1, +1, +04 },
    {-1, +0, +1, +04 },
    {+0, +0, +1, -24 },
    {+1, +0, +1, +04 },
    {+0, +1, +1, +04 },
    {+0, +0, +2, +02 }
};



/***********************PUBLIC METHODS*****************************************/

LISSolver::LISSolver( )
{
    _eps = 1e-3;
    _iterations = 0;
    _maxSubIterations = 5000;
    _maxIterations = 3;
    _pen = 5.0;
    _showLog = true;
    _stepsNumber = 1;
    _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
}



void LISSolver::computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                          const std::vector<bool>& activePoints,
                                          ReservoirMesh* reservoir,
                                          int ri, int rj, int rk,
                                          const unsigned int fixedBoundaryPoints )
{
    if (fixedBoundaryPoints < 1)
    {
        if (_showLog)
        {
            printf( "\n    Error: the number of fixed boundary cells must be greater then 0!\n" );
        }
        return;
    }

    if (_showLog)
    {
        printf( "\n    Pre-processing active cells and fixed boundary points...\n" );
        printf( "\n    Computing re-index vector...\n" );
    }

    Timer t;

    //Save the number of fixed boundary cells.
    _fixedBoundaryPoints = fixedBoundaryPoints;
    _roi = ri;
    _roj = rj;
    _rok = rk;
    _reservoir = reservoir;

    //Get the grid size.
    const unsigned int gridSize = grid.Size( );

    //Vector to map between point on grid to variable space.
    std::vector<int> reindexPoints;

    //Compute the fixed points.
    const unsigned int fixedPoints = preprocessFixedPoints( activePoints, reindexPoints );

    //Compute the number of unknown points.
    const unsigned int numberUnknowPoints = gridSize - fixedPoints;

    //Vector to store the number of active points on a vertex.
    std::vector<char> numActivePoints( numberUnknowPoints, 0 );

    if (_showLog)
    {
        t.printTime( "        Total time" );
        printf( "\n    %u points was fixed on preprocessing step...\n", fixedPoints );
        printf( "    %u points have unknown values...\n", numberUnknowPoints );
        printf( "    %u points have unknown values...\n", numberUnknowPoints );
    }

    t.restart( );

    //Initialize the LIS.
    LIS_INT argc = 0;
    char **argv;
    lis_initialize( &argc, &argv );

    //Allocate the memory to store the matrix.
    LIS_INT *ptr, *index;
    LIS_SCALAR *value;
    LIS_MATRIX A;
    LIS_VECTOR b, x;

    //Allocate matrix.
    const unsigned int numberNonZero = computeNumberNonZeroEelements( grid, reindexPoints );

    //Allocate vector to store in CSR format.
    lis_matrix_malloc_csr( numberUnknowPoints, numberNonZero, &ptr, &index, &value );

    //Create the matrix and define the matrix size.
    lis_matrix_create( 0, &A );
    lis_matrix_set_size( A, 0, numberUnknowPoints );

    //Create b vector and define it size.
    lis_vector_create( 0, &b );
    lis_vector_set_size( b, 0, numberUnknowPoints );

    //Create b vector and define it size.
    lis_vector_create( 0, &x );
    lis_vector_set_size( x, 0, numberUnknowPoints );

    //Compute the number of active points.
    computeActivePointsVector( numberUnknowPoints, grid, reindexPoints, numActivePoints );

    //Compute index vectors.
    computeIndexVectors( grid, reindexPoints,
                         numberUnknowPoints, numberNonZero,
                         numActivePoints, ptr, index, value );

    if (_showLog)
    {
        t.printTime( "        Total time" );
        double space = sizeof (unsigned int ) * gridSize + sizeof (FPType ) * ( gridSize + 3 * numberUnknowPoints ) + numberUnknowPoints;
        space /= 1048576.0;

        printf( "\n   Necessary GPU space: %.2lf MB\n", space );
        printf( "\n    **************************OPTIMIZING**************************\n\n" );
        printf( "\n    Optimizing the model with %u variables...\n\n", 3 * numberUnknowPoints );
    }

    LIS_SOLVER solver;
    lis_solver_create( &solver );
    char option[2048];

    omp_set_num_threads( _numberOfThreads );

    //Optimize dimension by dimension.
    for (unsigned int dimension = 0; dimension < 3; dimension++)
    {
        t.restart( );
        if (_showLog)
        {
            printf( "        DIMENSION %u:", dimension + 1 );
            printf( "\n\n            ITERATION           GRADIENT NORM           TIME\n" );
        }

        //Compute the vectors values for and specific dimension.
        computeBVector( dimension, grid, reindexPoints, numberUnknowPoints, x, b );

        //Compute gradient. This is equivalent to compute the initial residual.
        FPType gnorm = computeResidual( x, b, ptr, index, value, numberUnknowPoints );

        //Build the CSR format vectors.
        lis_matrix_set_csr( numberNonZero, ptr, index, value, A );
        lis_matrix_assemble( A );

        _iterations = 0;
        while (_iterations++ < _maxIterations && gnorm > _eps * _eps)
        {
            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                t.printTime( "" );
            }

            sprintf( option, "-i cg -p jacobi -maxiter %u -initx_zeros %d -conv_cond %d"
                     " -tol %e -omp_num_threads %u -print none", _maxSubIterations, 0, 0, _eps, _numberOfThreads );

            lis_solver_set_option( option, solver );
            lis_solve( A, b, x, solver );

            updateSolution( grid, x, reindexPoints, dimension );

            gnorm = computeResidual( x, b, ptr, index, value, numberUnknowPoints );
        }

        if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
        {
            printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
            t.printTime( "" );
        }
    }

    lis_solver_destroy( solver );
    lis_vector_destroy( x );
    lis_vector_destroy( b );
    lis_matrix_destroy( A );

    lis_finalize( );
}



void LISSolver::updateSolution( DsGrid3D<Point3D>& grid,
                                LIS_VECTOR x,
                                const std::vector<int>& reindexPoints,
                                const unsigned int dimension ) const
{
    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    //Compute the b vector.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the grid index.
                int gidx = grid.GetIndex( i, j, k );

                if (reindexPoints[gidx] != -1)
                {
                    //Get the variable index.
                    int vidx = reindexPoints[gidx];

                    //Get current solution.
                    double val = 0.0;
                    lis_vector_get_value( x, ( LIS_INT ) vidx, &val );

                    //Update solution.
                    grid.GetRef( gidx ).x[dimension] = val;
                }
            }
        }
    }
}



FPType LISSolver::getEPS( ) const
{
    return _eps;
}



unsigned int LISSolver::getMaxSubIterations( ) const
{
    return _maxSubIterations;
}



unsigned int LISSolver::getMaxIterations( ) const
{
    return _maxIterations;
}



unsigned int LISSolver::getNumberIterations( ) const
{
    return _iterations;
}



unsigned int LISSolver::getNumberfOfThreads( ) const
{
    return _numberOfThreads;
}



unsigned int LISSolver::getStepsNumber( ) const
{
    return _stepsNumber;
}



LISSolver::~LISSolver( )
{

}



void LISSolver::setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) )
{
    _displayInformation = displayInformation;
}



void LISSolver::setEPS( const FPType eps )
{
    _eps = eps;
}



void LISSolver::setMaxSubIterations( const unsigned int maxSubIter )
{
    _maxSubIterations = maxSubIter;
}



void LISSolver::setMaxIterations( const unsigned int maxIter )
{
    _maxIterations = maxIter;
}



void LISSolver::setNumberOfThreads( const unsigned int numThreads )
{
    _numberOfThreads = numThreads;
}



void LISSolver::setPenalization( const FPType pen )
{
    _pen = pen;
}



void LISSolver::showLog( const bool log )
{
    _showLog = log;
}



void LISSolver::setStepsNumber( const unsigned int sn )
{
    _stepsNumber = sn;
}



/***********************PRIVATE METHODS****************************************/
void LISSolver::computeReindexPointsVector( const std::vector<bool> &activePoints,
                                            std::vector<int>& reindexPoints ) const
{
    //Allocate storage.
    reindexPoints.resize( activePoints.size( ), -1 );

    //Compute new indexes.
    for (unsigned int i = 0, index = 0; i < activePoints.size( ); i++)
    {
        if (!activePoints[i])
        {
            reindexPoints[i] = index;
            index++;
        }
        else
        {
            reindexPoints[i] = -1;
        }
    }
}



void LISSolver::computeBVector( const unsigned int dimension,
                                const DsGrid3D<Point3D>& grid,
                                const std::vector<int>& reindexPoints,
                                const unsigned int unknownPoints,
                                LIS_VECTOR x,
                                LIS_VECTOR b )
{

    //Initialize x and b vectors.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) i, 0.0, x );
        lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) i, 0.0, b );
    }

    //    Get the reservoir dimensions.
    int ri = _reservoir->getGrid( ).Ni( );
    int rj = _reservoir->getGrid( ).Nj( );
    int rk = _reservoir->getGrid( ).Nk( );

    int dx[8][3] = {
        {0, 0, 0 },
        {1, 0, 0 },
        {0, 1, 0 },
        {1, 1, 0 },
        {0, 0, 1 },
        {1, 0, 1 },
        {0, 1, 1 },
        {1, 1, 1 }
    };

    //Check orientations.
    for (int i = 0; i < 8; i++)
    {
        if (_reservoir->invertedX( ))
        {
            dx[i][0] = !dx[i][0];
        }

        if (_reservoir->invertedY( ))
        {
            dx[i][0] = !dx[i][1];
        }

        if (_reservoir->invertedZ( ))
        {
            dx[i][2] = !dx[i][2];
        }
    }

    //Get the reservoir nodes.
    const std::vector<Point3D>& nodes = _reservoir->getReservoirNodes( );

    //Add constraints about active cells and to conform the mesh.
    for (int k = 0; k < rk; k++)
    {
        for (int j = 0; j < rj; j++)
        {
            for (int i = 0; i < ri; i++)
            {
                //Get the cells information.
                const CellMesh& c = _reservoir->getGrid( ).GetRef( i, j, k );

                //Penalize the move on active cell vertices.
                if (c.active( ) && !c.fixed( ))
                {
                    //Compute the cell index.
                    int oi = i + _roi;
                    int oj = j + _roj;
                    int ok = k + _rok;

                    if (_reservoir->invertedX( ))
                        oi = ( ri - 1 ) - i + _roi;

                    if (_reservoir->invertedY( ))
                        oj = ( rj - 1 ) - j + _roj;

                    if (_reservoir->invertedZ( ))
                        ok = ( rk - 1 ) - k + _rok;

                    for (int p = 0; p < 8; p++)
                    {
                        //Get the vertex index.
                        int vi = oi + dx[p][0];
                        int vj = oj + dx[p][1];
                        int vk = ok + dx[p][2];

                        //Get the vertex grid index.
                        int gidx = grid.GetIndex( vi, vj, vk );

                        //Verify if it is a variable.
                        if (reindexPoints[gidx] != -1)
                        {
                            //Get the vertex index on nodes vector.
                            int v = c.point( p );

                            //Get the variable index.
                            int vidx = reindexPoints[gidx];

                            //Add penalizations on b vector.
                            lis_vector_set_value( LIS_ADD_VALUE, ( LIS_INT ) vidx, 2 * _pen * nodes[v][dimension], b );
                        }
                    }
                }
            }
        }
    }

    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    //Compute the b vector.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int gidx = grid.GetIndex( i, j, k );

                if (reindexPoints[gidx] != -1)
                {
                    //Get the variable index.
                    int vidx = reindexPoints[gidx];
                    lis_vector_set_value( LIS_INS_VALUE, vidx, grid.Get( gidx ).x[dimension], x );

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx2 = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx2] == -1)
                            {
                                double val = -neighbouhood[n][3] * grid.Get( gidx2 ).x[dimension];
                                lis_vector_set_value( LIS_ADD_VALUE, vidx, val, b );
                            }
                        }
                    }
                }
            }
        }
    }
}



void LISSolver::computeActivePointsVector( const unsigned int unknownPoints,
                                           const DsGrid3D<Point3D>& grid,
                                           const std::vector<int>& reindexPoints,
                                           std::vector<char>& numActivePoints )
{
    //Initialize vectors.
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        numActivePoints[i] = 0;
    }

    //Get the reservoir dimensions.
    int ri = _reservoir->getGrid( ).Ni( );
    int rj = _reservoir->getGrid( ).Nj( );
    int rk = _reservoir->getGrid( ).Nk( );

    int dx[8][3] = {
        {0, 0, 0 },
        {1, 0, 0 },
        {0, 1, 0 },
        {1, 1, 0 },
        {0, 0, 1 },
        {1, 0, 1 },
        {0, 1, 1 },
        {1, 1, 1 }
    };

    //Check orientations.
    for (int i = 0; i < 8; i++)
    {
        if (_reservoir->invertedX( ))
        {
            dx[i][0] = !dx[i][0];
        }

        if (_reservoir->invertedY( ))
        {
            dx[i][0] = !dx[i][1];
        }

        if (_reservoir->invertedZ( ))
        {
            dx[i][2] = !dx[i][2];
        }
    }

    //Add constraints about active cells and to conform the mesh.
    for (int k = 0; k < rk; k++)
    {
        for (int j = 0; j < rj; j++)
        {
            for (int i = 0; i < ri; i++)
            {
                //Get the cells information.
                const CellMesh& c = _reservoir->getGrid( ).GetRef( i, j, k );

                //Penalize the move on active cell vertices.
                if (c.active( ) && !c.fixed( ))
                {
                    //Compute the cell index.
                    int oi = i + _roi;
                    int oj = j + _roj;
                    int ok = k + _rok;

                    if (_reservoir->invertedX( ))
                        oi = ( ri - 1 ) - i + _roi;

                    if (_reservoir->invertedY( ))
                        oj = ( rj - 1 ) - j + _roj;

                    if (_reservoir->invertedZ( ))
                        ok = ( rk - 1 ) - k + _rok;

                    for (int p = 0; p < 8; p++)
                    {
                        //Get the vertex index.
                        int vi = oi + dx[p][0];
                        int vj = oj + dx[p][1];
                        int vk = ok + dx[p][2];

                        //Get the vertex grid index.
                        int gidx = grid.GetIndex( vi, vj, vk );

                        //Verify if it is a variable.
                        if (reindexPoints[gidx] != -1)
                        {
                            //Get the variable index.
                            int vidx = reindexPoints[gidx];

                            //Count the number penalizations.
                            numActivePoints[vidx]++;
                        }
                    }
                }
            }
        }
    }
}



unsigned int LISSolver::preprocessFixedPoints( const std::vector<bool>& activePoints,
                                               std::vector<int>& reindexPoints ) const
{
    //Get the number of fixed points.
    std::vector<unsigned int> sum( _numberOfThreads, 0 );
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < activePoints.size( ); i++)
    {
        if (activePoints[i])
        {
            int thread = omp_get_thread_num( );
            sum[thread]++;
        }
    }

    unsigned totalFixedPoints = 0;
    for (unsigned int i = 0; i < sum.size( ); i++)
    {
        totalFixedPoints += sum[i];
    }

    //From active points, compute the re-index vector.
    computeReindexPointsVector( activePoints, reindexPoints );

    return totalFixedPoints;
}



/***********************PCG ALG. METHODS***************************************/

double LISSolver::computeResidual( LIS_VECTOR x,
                                          LIS_VECTOR b,
                                          const LIS_INT *ptr,
                                          const LIS_INT *index,
                                          const LIS_SCALAR* value,
                                          const unsigned int unknownPoints ) const
{
    std::vector<double> sum( _numberOfThreads, 0 );
    #pragma omp parallel for num_threads(_numberOfThreads)
    for (unsigned int i = 0; i < unknownPoints; i++)
    {
        double g = 0.0;
        double xx = 0.0;
        for (LIS_INT j = ptr[i]; j < ptr[i + 1]; j++)
        {
            //Get the variable index.
            LIS_INT v = index[j];

            //Get the variable value.
            xx = 0.0;
            lis_vector_get_value( x, v, &xx );

            //Multiply the line by vector.
            g += value[j] * xx;
        }
        xx = 0.0;
        lis_vector_get_value( b, ( LIS_INT ) i, &xx );

        g -= xx;

        int thread = omp_get_thread_num( );
        sum[thread] += g * g;
    }

    double g2 = 0.0;
    for (unsigned int i = 0; i < sum.size( ); i++)
    {
        g2 += sum[i];
    }
    return g2;
}



/***********************PCG ALG. METHODS***************************************/

void LISSolver::computeElementsPerLine( const DsGrid3D<Point3D>& grid,
                                        const std::vector<int>& reindexPoints,
                                        LIS_INT *ptr ) const
{
    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Compute the number of unknown variables per line.
    for (int k = ( int ) _fixedBoundaryPoints; k < nk - ( int ) _fixedBoundaryPoints; k++)
    {
        for (int j = ( int ) _fixedBoundaryPoints; j < nj - ( int ) _fixedBoundaryPoints; j++)
        {
            for (int i = ( int ) _fixedBoundaryPoints; i < ni - ( int ) _fixedBoundaryPoints; i++)
            {
                if (reindexPoints[grid.GetIndex( i, j, k )] != -1)
                {
                    int v = reindexPoints[grid.GetIndex( i, j, k )];
                    for (int n = 0; n < 25; n++)
                    {
                        //Get the neighbor on grid.
                        int di = i + neighbouhood[n][0];
                        int dj = j + neighbouhood[n][1];
                        int dk = k + neighbouhood[n][2];
                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx] != -1)
                            {
                                ptr[v]++;
                            }
                        }
                    }
                }
            }
        }
    }
}



void LISSolver::computeIndexVectors( const DsGrid3D<Point3D>& grid,
                                     const std::vector<int>& reindexPoints,
                                     const unsigned int unknownPoints,
                                     const unsigned int nonZero, std::vector<char>& numActivePoints,
                                     LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const
{
    //Initialize vectors.
    memset( ptr, 0, ( unknownPoints + 1 ) * sizeof (LIS_INT ) );

    //Compute the number of elements per line.
    computeElementsPerLine( grid, reindexPoints, ptr );
    ptr[unknownPoints] = nonZero;

    //Compute the nonzero columns.
    computeMatrixColumns( grid, reindexPoints,
                          unknownPoints, nonZero, numActivePoints,
                          ptr, index, value );
}



void LISSolver::computeMatrixColumns( const DsGrid3D<Point3D>& grid,
                                      const std::vector<int>& reindexPoints,
                                      const unsigned int unknownPoints,
                                      const unsigned int nonZero, std::vector<char>& numActivePoints,
                                      LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const
{
    memset( index, 0, nonZero * sizeof (LIS_INT ) );
    memset( value, 0, nonZero * sizeof (LIS_SCALAR ) );

    //Vector to know about next free position on vectors.
    std::vector<int> freePosition( unknownPoints, 0 );

    //Initialize the last position with the number of nonzero elements on
    //matrix.
    ptr[unknownPoints] = nonZero;

    //Compute free positions and adjust the ptr vector to store where each
    //line start.
    for (int i = ( int ) unknownPoints - 1; i >= 0; i--)
    {
        ptr[i] = ptr[i + 1] - ptr[i];
        freePosition[i] = ptr[i];
    }

    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int currentPoint = grid.GetIndex( i, j, k );
                if (reindexPoints[currentPoint] != -1)
                {
                    //Get variable index.
                    currentPoint = reindexPoints[currentPoint];

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx] != -1)
                            {
                                const int vidx = reindexPoints[gidx];

                                FPType aij = neighbouhood[n][3] +
                                    ( ( vi == 0 && vj == 0 && vk == 0 ) ? ( 2.0 * _pen * numActivePoints[currentPoint] ) : 0.0 );

                                unsigned int f = freePosition[currentPoint];
                                index[f] = vidx;
                                value[f] = aij;
                                freePosition[currentPoint]++;
                            }
                        }
                    }
                }
            }
        }
    }
}



unsigned int LISSolver::computeNumberNonZeroEelements( const DsGrid3D<Point3D>& grid,
                                                       const std::vector<int>& reindexPoints )
{
    //Multiply matrix.
    //Get grid dimensions.
    const int nk = grid.Nk( );
    const int nj = grid.Nj( );
    const int ni = grid.Ni( );

    //Get limits.
    const int lk = nk - _fixedBoundaryPoints;
    const int lj = nj - _fixedBoundaryPoints;
    const int li = ni - _fixedBoundaryPoints;

    unsigned int numberNonZero = 0;

    for (int k = _fixedBoundaryPoints; k < lk; k++)
    {
        for (int j = _fixedBoundaryPoints; j < lj; j++)
        {
            for (int i = _fixedBoundaryPoints; i < li; i++)
            {
                //Get the current point index on grid
                int currentPoint = grid.GetIndex( i, j, k );
                if (reindexPoints[currentPoint] != -1)
                {
                    //Get variable index.
                    currentPoint = reindexPoints[currentPoint];

                    for (int n = 0; n < 25; n++)
                    {
                        int vi = neighbouhood[n][0];
                        int vj = neighbouhood[n][1];
                        int vk = neighbouhood[n][2];

                        //Get the neighbor on grid.
                        int di = i + vi;
                        int dj = j + vj;
                        int dk = k + vk;

                        if (0 <= di && di < ni &&
                            0 <= dj && dj < nj &&
                            0 <= dk && dk < nk)
                        {
                            int gidx = grid.GetIndex( di, dj, dk );
                            if (reindexPoints[gidx] != -1)
                            {
                                numberNonZero++;
                            }
                        }
                    }
                }
            }
        }
    }

    return numberNonZero;
}

