/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CellMesh.h
 * Author: jcoelho
 *
 * Created on February 1, 2017, 11:40 AM
 */

#ifndef CELLMESH_H
#define CELLMESH_H
#include <vector>

class CellMesh
{
public:
    /**
     * Default constructor.
     */
    CellMesh( );
    CellMesh( const CellMesh& c );

    /**
     * Get the p-th cell point.
     * @param p - point index on face: 0 <= p <= 7
     * @return - the point index.
     */
    int point( unsigned int p ) const;

    /**
     * Get the f-th adjacent face.
     * @param f - index of the adjacent face: 0 <= f <= 5
     * @return - the face index.
     */
    int& adjacent( unsigned int f );
    int adjacent( unsigned int f ) const;

    /**
     * Verify if the cell is active or not.
     * @return - true if the cell is active and false otherwise.
     */
    bool active( ) const;
    bool fault( ) const;
    bool fixed( ) const;
    void setFault( bool fault );

    /**
     * Set the points index that define the cell.
     * @param p - set of point index.
     */
    void setPoints( int p[] );
    void setPoint( int p, int v );
    void setFixed( bool fixed );

    /**
     * Set the adjacent cells index.
     * @param a - set of adjacent cells index.
     */
    void setAdjacentCells( int a[] );
    void setAdjacent( int f, int adj );

    /**
     * Define if a cell is active or not.
     * @param ac - true if it is active and false otherwise.
     */
    void active( bool ac );

    CellMesh& operator=( const CellMesh& c );
private:
    /**
     * Store the cell points' index.
     */
    int _points[8];

    /**
     * Store the adjacent cells.
     */
    int _adjacents[6];

    /**
     * Determine if the cell is active or not.
     */
    bool _active;

    /**
     * Flag to determine if a cell is on fault.
     */
    bool _fault;

    /**
     * Flag to determine if a cell must be fixed or not.
     */
    bool _fixed;
};


#endif /* CELLMESH_H */

