/* 
 * File:   ReservoirModeling.cpp
 * Author: jcoelho
 * 
 * Created on February 23, 2017, 4:28 PM
 */
#include <fstream>
#include "ReservoirModeling.h"
#include "../../../Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "../../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"
#include "../../../ModelManager/OptimizationModel.h"

using namespace Solver;
using namespace ModelManager;



static bool compare( const ReservoirHorizon* h1, const ReservoirHorizon* h2 )
{
    return h1->getAverageZ( ) < h2->getAverageZ( );
}

//PUBLIC METHODS.



ReservoirModeling::ReservoirModeling( )
{
    _distanceI = _distanceJ = 0.0;
    _numCellsI = _numCellsJ = 0;
    _inputMesh = NULL;
    _ni = _nj = _nk = 0;
    _fixedBoundaryPoints = 2;
    _reservoirK = 0;
    _numThreads = std::max( omp_get_num_procs( ) - 2, 1 );
}



void ReservoirModeling::addHorizon( ReservoirHorizon* horizon )
{
    _horizons.push_back( horizon );
}



void ReservoirModeling::generateMesh( )
{
    //Preprocess the data.
    preprocess( );

    //Check by inconsistency.
    if (!checkData( ))
        return;

    //Allocate memory to output and model variables.
    allocateMemory( );

    //Compute the initial geometry.
    computeInitialGeometry( );

    //Set hard position.
    setHardPointsPosition( );

    //Build and optimize the model.
    optimize( );

    //Posprocess the data.
    posprocess( );
}



void ReservoirModeling::setIIncrement( const double i )
{
    _distanceI = i;
}



void ReservoirModeling::setJIncrement( const double j )
{
    _distanceJ = j;
}



void ReservoirModeling::setNumCellsI( const unsigned int i )
{
    _numCellsI = i;
}



void ReservoirModeling::setNumCellsJ( const unsigned int j )
{
    _numCellsJ = j;
}



void ReservoirModeling::setNumCellsAmongSurfaces( const std::vector<int>& slices )
{
    _slices = slices;
}



void ReservoirModeling::setNumThreads( const int numThreads )
{
    _numThreads = std::min( numThreads, omp_get_num_procs( ) - 2 );
}



void ReservoirModeling::setReservoirMesh( ReservoirMesh* mesh )
{
    _inputMesh = mesh;
}



bool ReservoirModeling::writeNeutralFileForInput( const std::string& path )
{
    //Get the number of horizons on model.
    const unsigned int n = _horizons.size( );

    //Allocate vector to re-index the mesh.
    std::vector<unsigned int> horizonsNodes( n, 0 );

    //Compute the number of nodes and elements on model.
    unsigned int totalHorizonNodes = 0, totalHorizonElements = 0;
    for (unsigned int i = 0; i < n; i++)
    {
        horizonsNodes[i] = _horizons[i]->getNumberNodes( );
        totalHorizonNodes += horizonsNodes[i];

        totalHorizonElements += _horizons[i]->getNumberElements( );
    }

    horizonsNodes[n - 1] = totalHorizonNodes - horizonsNodes[n - 1];
    for (int i = ( int ) n - 2; i >= 0; i--)
    {
        horizonsNodes[i] = horizonsNodes[i + 1] - horizonsNodes[i];
    }

    //Get the number of nodes on reservoir.
    unsigned int reservoirNodes = _inputMesh->getNumberOfNodes( );

    //Get the number of elements on reservoir.
    int reservoidElements = _inputMesh->getNumberOfElements( );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << totalHorizonNodes + reservoirNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << totalHorizonNodes + reservoirNodes << std::endl << std::endl;
    int idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const std::vector<Point3D>& coord = _horizons[i]->getCoordinates( );
        for (unsigned int i = 0; i < coord.size( ); i++)
        {
            out << idx << " " << coord[i] << std::endl;
            idx++;
        }
    }

    const std::vector<Point3D>& reserNodes = _inputMesh->getReservoirNodes( );
    for (unsigned int i = 0; i < reserNodes.size( ); i++)
    {
        out << idx << " " << reserNodes[i] << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << totalHorizonElements + reservoidElements << std::endl << std::endl;

    out << "%ELEMENT.T3" << std::endl;
    out << totalHorizonElements << std::endl << std::endl;
    idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const CornerType* tri = _horizons[i]->getTriangleList( );
        const CornerType numTriangles = _horizons[i]->getNumberElements( );

        unsigned int inc = horizonsNodes[i] + 1;
        for (unsigned int i = 0; i < numTriangles; i++)
        {
            out << idx << " 0 0 0 ";
            out << tri[3 * i + 0] + inc << " " << tri[3 * i + 1] + inc << " " << tri[3 * i + 2] + inc << std::endl;
            idx++;
        }
    }
    out << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << reservoidElements << std::endl << std::endl;

    unsigned int reservoirToNFFormat[8] = { 1, 3, 7, 5, 0, 2, 6, 4 };

    const DsGrid3D<CellMesh*>& grid = _inputMesh->getGrid( );
    for (int i = 0; i < grid.Size( ); i++)
    {
        out << idx << " 0 0 ";
        CellMesh* c = grid.Get( i );
        for (unsigned int i = 0; i < 8; i++)
        {
            unsigned int p = reservoirToNFFormat[i];
            out << c->point( p ) + totalHorizonNodes + 1 << " ";
        }
        out << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.FACTOR" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
    out << 4 << std::endl;
    out << "'ACTIVE CELL' 'I' 'J' 'K'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
    out << reservoidElements + totalHorizonElements << std::endl;

    idx = 1;
    for (int i = 0; i < ( int ) n; i++)
    {
        for (int t = 0; t < ( int ) _horizons[i]->getNumberElements( ); t++)
        {
            out << idx << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;

            idx++;
        }
    }

    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                out << idx << std::endl;

                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;
                out << ( int ) grid.Get( i, j, k )->active( ) << " " << i << " " << j << " " << k << std::endl;

                idx++;
            }
        }
    }

    out << "%END" << std::endl;
    return true;
}



bool ReservoirModeling::writeNeutralFileForOuput( const std::string& path )
{
    printf( "Writing file...\n" );
    //Get the number of horizons on model.
    const unsigned int n = _horizons.size( );

    //Allocate vector to re-index the mesh.
    std::vector<unsigned int> horizonsNodes( n, 0 );

    //Compute the number of nodes and elements on model.
    unsigned int totalHorizonNodes = 0, totalHorizonElements = 0;
    for (unsigned int i = 0; i < n; i++)
    {
        horizonsNodes[i] = _horizons[i]->getNumberNodes( );
        totalHorizonNodes += horizonsNodes[i];

        totalHorizonElements += _horizons[i]->getNumberElements( );
    }

    horizonsNodes[n - 1] = totalHorizonNodes - horizonsNodes[n - 1];
    for (int i = ( int ) n - 2; i >= 0; i--)
    {
        horizonsNodes[i] = horizonsNodes[i + 1] - horizonsNodes[i];
    }

    //Get the number of nodes on output.
    unsigned int outputNodes = _ni * _nj * _nk;

    //Get the number of nodes on reservoir.
    unsigned int reservoirNodes = _inputMesh->getNumberOfNodes( );

    //Get the number of elements on output.
    unsigned int outputElements = ( _ni - 1 ) * ( _nj - 1 ) * ( _nk - 1 );

    //Get the number of elements on reservoir.
    unsigned int reservoirElements = _inputMesh->getGrid( ).Size( );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << totalHorizonNodes + outputNodes + reservoirNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << totalHorizonNodes + outputNodes + reservoirNodes << std::endl << std::endl;

    int idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const std::vector<Point3D>& coord = _horizons[i]->getCoordinates( );
        for (unsigned int i = 0; i < coord.size( ); i++)
        {
            out << idx << " " << coord[i] << std::endl;
            idx++;
        }
    }

    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                out << idx << " " << _outputGrid[i][j][k].x << std::endl;
                idx++;
            }
        }
    }

    const std::vector<Point3D>& reserNodes = _inputMesh->getReservoirNodes( );
    for (unsigned int i = 0; i < reserNodes.size( ); i++)
    {
        out << idx << " " << reserNodes[i] << std::endl;
        idx++;
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << totalHorizonElements + outputElements + reservoirElements << std::endl << std::endl;

    out << "%ELEMENT.T3" << std::endl;
    out << totalHorizonElements << std::endl << std::endl;
    unsigned int inc = 0;
    idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const unsigned int* tri = _horizons[i]->getTriangleList( );
        inc = horizonsNodes[i] + 1;
        unsigned int numTriangles = _horizons[i]->getNumberElements( );

        for (unsigned int i = 0; i < numTriangles; i++)
        {
            out << idx << " 0 0 0 ";
            out << tri[3 * i + 0] + inc << " " << tri[3 * i + 1] + inc << " " << tri[3 * i + 2] + inc << std::endl;
            idx++;
        }
    }
    out << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << outputElements + reservoirElements << std::endl << std::endl;

    inc = totalHorizonNodes + 1;

    for (int k = 0; k < _nk - 1; k++)
    {
        for (int j = 0; j < _nj - 1; j++)
        {
            for (int i = 0; i < _ni - 1; i++)
            {
                out << idx << " 0 0 ";

                out << computePointIndex( i + 1, j + 0, k + 0 ) + inc << " ";
                out << computePointIndex( i + 1, j + 1, k + 0 ) + inc << " ";
                out << computePointIndex( i + 1, j + 1, k + 1 ) + inc << " ";
                out << computePointIndex( i + 1, j + 0, k + 1 ) + inc << " ";

                out << computePointIndex( i + 0, j + 0, k + 0 ) + inc << " ";
                out << computePointIndex( i + 0, j + 1, k + 0 ) + inc << " ";
                out << computePointIndex( i + 0, j + 1, k + 1 ) + inc << " ";
                out << computePointIndex( i + 0, j + 0, k + 1 ) + inc << " ";

                out << std::endl;

                idx++;
            }
        }
    }

    inc += outputNodes;

    int reservoirToNFFormat[8] = { 1, 3, 7, 5, 0, 2, 6, 4 };

    const DsGrid3D<CellMesh*>& grid = _inputMesh->getGrid( );
    for (int i = 0; i < grid.Size( ); i++)
    {
        out << idx << " 0 0 ";
        CellMesh* c = grid.Get( i );
        for (int i = 0; i < 8; i++)
        {
            int p = reservoirToNFFormat[i];
            out << c->point( p ) + inc << " ";
        }
        out << std::endl;
        idx++;
    }

    out << std::endl;

    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.FACTOR" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
    out << 7 << std::endl;
    out << "'ACTIVE CELL' 'I' 'J' 'K' 'SLICE''BORDER''RESERVOIR'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
    out << outputElements + totalHorizonElements + reservoirElements << std::endl;

    idx = 1;
    for (int i = 0; i < ( int ) n; i++)
    {
        for (int t = 0; t < ( int ) _horizons[i]->getNumberElements( ); t++)
        {
            int b = _horizons[i]->borderTriangle( t );

            out << idx << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << " " << -1 << " " << b << " 0" << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << " " << -1 << " " << b << " 0" << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << " " << -1 << " " << b << " 0" << std::endl;

            idx++;
        }
    }

    //Get the reservoir dimensions.
    int ni = _inputMesh->getGrid( ).Ni( );
    int nj = _inputMesh->getGrid( ).Nj( );
    int nk = _inputMesh->getGrid( ).Nk( );

    //Compute the increment to access the reservoir cells.
    int incI = _numCellsI;
    int incJ = _numCellsJ;
    int incK = _reservoirK;

    int slice = 0;
    for (int k = 0; k < _nk - 1; k++)
    {
        //Define the current slice.
        //        for (int s = slice; s < ( int ) _slicesWithReservoir.size( ) - 1; s++)
        //        {
        //            int s1 = _slicesWithReservoir[s + 0];
        //            int s2 = _slicesWithReservoir[s + 1];
        //            if (s1 <= k && k < s2)
        //            {
        //                slice = s;
        //                break;
        //            }
        //        }

        for (int j = 0; j < _nj - 1; j++)
        {
            for (int i = 0; i < _ni - 1; i++)
            {
                out << idx << std::endl;

                int active = 0;
                int reserv = 0;
                int ri = ni - 1 + incI - i;
                int rj = j - incJ;
                int rk = nk - 1 + incK - k;

                for (int kk = 0; kk <= 1; kk++)
                {
                    for (int jj = 0; jj <= 1; jj++)
                    {
                        for (int ii = 0; ii <= 1; ii++)
                        {
                            active = active || _outputGrid[i + ii][j + jj][k + kk].active;
                        }
                    }
                }
                if (ri >= 0 && ri < ni &&
                    rj >= 0 && rj < nj &&
                    rk >= 0 && rk < nk
                    )
                {
                    CellMesh* c = _inputMesh->getGrid( ).Get( ri, rj, rk );
                    active = c->active( );
                    reserv = 1;
                }

                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;
                out << active << " " << i << " " << j << " " << k << " " << slice << " 0 " << reserv << std::endl;

                idx++;
            }
        }
    }


    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                out << idx << std::endl;
                int active = grid.Get( i, j, k )->active( );

                //Compute the cell index.
                int oi = ( ( ni - 1 ) - i ) + incI;
                int oj = j + incJ;
                int ok = ( ( nk - 1 ) - k ) + incK;

                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;
                out << active << " " << oi << " " << oj << " " << ok << "-1 0 2" << std::endl;

                idx++;
            }
        }
    }

    out << "%END" << std::endl;
    return true;
}



bool ReservoirModeling::writeNeutralFileRodrigo( const std::string& path )
{
    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }


    out << _ni << " " << _nj << " " << _nk << std::endl;

    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                out << _outputGrid[i][j][k].x << std::endl;
            }
        }
    }
    return true;
}

//PRIVATE METHODS.



void ReservoirModeling::setHardPointsPosition( )
{
    //Fix boundary points.
    int f = _fixedBoundaryPoints;
    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                if (i >= f && i <= _ni - 1 - f &&
                    j >= f && j <= _nj - 1 - f &&
                    k >= f && k <= _nk - 1 - f)
                {
                    continue;
                }
                _outputGrid[i][j][k].active = true;
            }
        }
    }

    printf( "Horizonte nas fatias:\n" );
    //Set the horizon points on correct slice.
    for (unsigned int i = 0; i < _horizonSlices.size( ); i++)
    {
        //Fix points on slice s in the horizon i.
        setHorizonPoints( _horizonSlices[i], i );
        printf( "%d\n", _horizonSlices[i] );
    }
}



void ReservoirModeling::setHorizonPoints( unsigned int slice, unsigned int h )
{
    //Get triangle list information.
    const CornerType numTriangles = _horizons[h]->getNumberElements( );
    std::vector<CornerType> triangles;

    //Cells to be fixed.
    std::set<Point3D> cells;

    for (unsigned int t = 0; t < numTriangles; t++)
    {
        triangles.push_back( t );
    }

    //Project the triangles on border.
    projectHorizonOnMesh( slice, h, triangles, cells );

    //Project all cells on triangle mesh.
    projectCellsOnHorizon( h, cells );
}



void ReservoirModeling::projectCellsOnHorizon( unsigned int h,
                                               const std::set<Point3D>& cells )
{
    int k = _horizonSlices[h];
    for (auto it = cells.begin( ); it != cells.end( ); it++)
    {
        int i = ( int ) it->x[0];
        int j = ( int ) it->x[1];
        for (int di = 0; di <= 1; di++)
        {
            for (int dj = 0; dj <= 1; dj++)
            {
                if (!_outputGrid[i + di][j + dj][k].active)
                {
                    //Project point.
                    Point3D &p = _outputGrid[i + di][j + dj][k].x;
                    if (projectPointOnHorizon( p, h ))
                    {
                        //Set the point as active point.
                        _outputGrid[i + di][j + dj][k].active = true;
                    }
                }
            }
        }
    }
}



void ReservoirModeling::projectHorizonOnMesh( unsigned int slice, unsigned int h,
                                              const std::vector<CornerType>& triangles,
                                              std::set<Point3D>& cells )
{
    //Get triangle list information.
    const CornerType *triangleList = _horizons[h]->getTriangleList( );
    const std::vector<Point3D>& coords = _horizons[h]->getCoordinates( );

    for (unsigned int tri = 0; tri < triangles.size( ); tri++)
    {
        CornerType t = triangles[tri];

        //Get cell for each point.
        for (CornerType c = 3 * t; c < 3 * t + 3; c++)
        {
            CornerType v = triangleList[c];
            Point3D p = coords[v];

            bool stop = false;

            //Find the cell.
            for (int i = 0; i < _ni - 1; i++)
            {
                for (int j = 0; j < _nj - 1; j++)
                {
                    double x1 = _outputGrid[i + 0][0][slice].x[0];
                    double x2 = _outputGrid[i + 1][0][slice].x[0];
                    double y1 = _outputGrid[0][j + 0][slice].x[1];
                    double y2 = _outputGrid[0][j + 1][slice].x[1];

                    if (x1 < p[0] && p[0] < x2 && y1 < p[1] && p[1] < y2)
                    {
                        cells.insert( Point3D( i, j, 0 ) );
                        stop = true;
                        break;
                    }
                }
                if (stop) break;
            }
        }
    }
}



bool ReservoirModeling::projectPointOnHorizon( Point3D& p, unsigned int h )
{
    //Get triangle list information.
    const CornerType *triangleList = _horizons[h]->getTriangleList( );
    const std::vector<Point3D>& coords = _horizons[h]->getCoordinates( );

    CornerType t = _horizons[h]->getTriangle( p[0], p[1] );

    if (t == CornerTable::BORDER_CORNER)
        return false;

    //Get vertices index.
    CornerType v0 = triangleList[3 * t + 0];
    CornerType v1 = triangleList[3 * t + 1];
    CornerType v2 = triangleList[3 * t + 2];

    //Get the triangle coordinates.
    Point3D tp[3] = { coords[v0], coords[v1], coords[v2] };

    double S = ( ( tp[2] - tp[1] )^( tp[0] - tp[1] ) )[2];

    double l[3];
    for (int i = 0; i < 3; i++)
    {
        Point3D v = tp[ ( i + 2 ) % 3] - tp[( i + 1 ) % 3];
        Point3D w = p - tp[( i + 1 ) % 3];
        double s = ( v ^ w )[2];
        l[i] = s / S;
    }

    //Define a new z.
    p[2] = l[0] * tp[0][2] + l[1] * tp[1][2] + l[2] * tp[2][2];
    return true;
}



void ReservoirModeling::optimize( )
{
    //Allocate solver and model.
    ConjugateGradientLISOptimizationSolver solver;
    //    LBFGSOptimizationSolver solver;

    OptimizationModel m( &solver );
    m.setSolverEps( 1e-5 );
    m.setMaxIterations( 5 );

    m.setNumberOfThreads( _numThreads );

    printf( "%d pontos e %d variaveis...\n", _ni * _nj * _nk, 3 * _ni * _nj * _nk );

    //Create variables and add boundary constraints.
    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                _v[i][j][k].x = m.addVariable( );
                _v[i][j][k].y = m.addVariable( );
                _v[i][j][k].z = m.addVariable( );

                _v[i][j][k].x = _outputGrid[i][j][k].x[0];
                _v[i][j][k].y = _outputGrid[i][j][k].x[1];
                _v[i][j][k].z = _outputGrid[i][j][k].x[2];

                if (_outputGrid[i][j][k].active)
                {
                    m.addConstraint( _v[i][j][k].x, EQUAL, _outputGrid[i][j][k].x[0] );
                    m.addConstraint( _v[i][j][k].y, EQUAL, _outputGrid[i][j][k].x[1] );
                    m.addConstraint( _v[i][j][k].z, EQUAL, _outputGrid[i][j][k].x[2] );
                }
            }
        }
    }

    //Add constraints
    printf( "Adding conformal constraints...\n" );

    //Add conformal constraints.
    addConformalConstraints( m );

    //Build the objective function.
    QuadraticExpression &obj = m.getObjectiveFunction( );

    omp_lock_t writelock;
    omp_init_lock( &writelock );

    for (int k = 0; k < _nk; k++)
    {
        printf( "\r%6.2lf%% complete...", 100.0 * ( double ) k / _nk );
        fflush( stdout );
        #pragma omp parallel for num_threads(_numThreads)
        for (int j = 0; j < _nj; j++)
        {
            QuadraticExpression aux;
            for (int i = 0; i < _ni; i++)
            {
                QuadraticExpression aux2;
                LinearExpression x, y, z;
                if (i - 1 >= 0)
                {
                    x += _v[i - 1][j][k].x;
                    y += _v[i - 1][j][k].y;
                    z += _v[i - 1][j][k].z;
                }

                if (i + 1 < _ni)
                {
                    x += _v[i + 1][j][k].x;
                    y += _v[i + 1][j][k].y;
                    z += _v[i + 1][j][k].z;
                }

                if (j - 1 >= 0)
                {
                    x += _v[i][j - 1][k].x;
                    y += _v[i][j - 1][k].y;
                    z += _v[i][j - 1][k].z;
                }

                if (j + 1 < _nj)
                {
                    x += _v[i][j + 1][k].x;
                    y += _v[i][j + 1][k].y;
                    z += _v[i][j + 1][k].z;
                }
                if (k - 1 >= 0)
                {
                    x += _v[i][j][k - 1].x;
                    y += _v[i][j][k - 1].y;
                    z += _v[i][j][k - 1].z;
                }

                if (k + 1 < _nk)
                {
                    x += _v[i][j][k + 1].x;
                    y += _v[i][j][k + 1].y;
                    z += _v[i][j][k + 1].z;
                }

                x -= ( x.size( ) * _v[i][j][k].x );
                y -= ( y.size( ) * _v[i][j][k].y );
                z -= ( z.size( ) * _v[i][j][k].z );

                aux2 = x * x + y * y + z * z;
                aux2.update( );
                aux += aux2;
            }
            aux.update( );

            omp_set_lock( &writelock );
            obj += aux;
            omp_unset_lock( &writelock );
        }
    }
    printf( "\r%6.2lf%% complete...\n", 100.0 );

    omp_destroy_lock( &writelock );

    m.setObjectiveFunction( obj );

    m.setSoftConstraintsWeight( 5 );
    m.optimize( );


    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                _outputGrid[i][j][k].x[0] = _v[i][j][k].x.getValue( );
                _outputGrid[i][j][k].x[1] = _v[i][j][k].y.getValue( );
                _outputGrid[i][j][k].x[2] = _v[i][j][k].z.getValue( );
            }
        }
    }
}



void ReservoirModeling::addConformalConstraints( ModelManager::OptimizationModel& m )
{
    //Compute the increment to access the reservoir cells.
    int incI = _numCellsI;
    int incJ = _numCellsJ;
    int incK = _reservoirK;

    //Get the reservoir dimensions.
    int ni = _inputMesh->getGrid( ).Ni( );
    int nj = _inputMesh->getGrid( ).Nj( );
    int nk = _inputMesh->getGrid( ).Nk( );

    int dx[8][3] = {
        {0, 0, 0 },
        {1, 0, 0 },
        {0, 1, 0 },
        {1, 1, 0 },
        {0, 0, 1 },
        {1, 0, 1 },
        {0, 1, 1 },
        {1, 1, 1 }
    };

    //Get the reservoir nodes.
    const std::vector<Point3D>& nodes = _inputMesh->getReservoirNodes( );

    //Add constraints about active cells and to conform the mesh.
    for (int k = 0; k < nk; k++)
    {
        for (int j = 0; j < nj; j++)
        {
            for (int i = 0; i < ni; i++)
            {
                //Get the cells information.
                CellMesh* c = _inputMesh->getGrid( ).Get( i, j, k );

                //Compute the cell index.
                int oi = i + incI;
                int oj = j + incJ;
                int ok = k + incK;

                if (_inputMesh->invertedX( ))
                    oi = ( ni - 1 ) - i + incI;

                if (_inputMesh->invertedY( ))
                    oj = ( nj - 1 ) - j + incJ;

                if (_inputMesh->invertedZ( ))
                    ok = ( nk - 1 ) - k + incK;

                //Penalize the move on active cell vertices.
                if (c->active( ))
                {
                    for (int p = 0; p < 8; p++)
                    {
                        //Get the vertex index.
                        int vi = oi + dx[p][0];
                        int vj = oj + dx[p][1];
                        int vk = ok + dx[p][2];

                        int v = c->point( p );

                        m.addConstraint( _v[vi][vj][vk].x, SOFT, nodes[v][0] );
                        m.addConstraint( _v[vi][vj][vk].y, SOFT, nodes[v][1] );
                        m.addConstraint( _v[vi][vj][vk].z, SOFT, nodes[v][2] );
                    }
                }
            }
        }
    }
}



void ReservoirModeling::allocateMemory( )
{
    //Compute the new dimensions for grid.
    _ni = _inputMesh->getGrid( ).Ni( ) + 2 * _numCellsI + 1;
    _nj = _inputMesh->getGrid( ).Nj( ) + 2 * _numCellsJ + 1;
    _nk = 1;
    for (unsigned int i = 0; i < _slices.size( ); i++)
    {
        _nk += _slices[i];
    }

    //Allocate the output grid.
    _outputGrid.resize( _ni );
    _v.resize( _ni );

    for (int i = 0; i < _ni; i++)
    {
        _outputGrid[i].resize( _nj );
        _v[i].resize( _nj );
        for (int j = 0; j < _nj; j++)
        {
            _outputGrid[i][j].resize( _nk );
            _v[i][j].resize( _nk );
        }
    }
}



bool ReservoirModeling::checkData( )
{
    bool state = true;
    if (_slices.size( ) != _horizons.size( ) + 1)
    {
        printf( "Error: it's necessary to define the number of cells between"
                " each pair of surfaces, included the reservoir.\n" );
        state = false;
    }

    for (unsigned int i = 0; i < _slices.size( ); i++)
    {
        if (( int ) _slices[i] < 1)
        {
            printf( "Error: the number of cells between each surface must be"
                    " greater than zero.\n" );

            state = false;
            break;
        }
    }

    return state;
}



int ReservoirModeling::computePointIndex( int i, int j, int k )
{
    return _ni * _nj * k + _ni * j + i;
}



void ReservoirModeling::computeInitialGeometry( )
{
    //Get the average of z on reservoir.
    double zR = _inputMesh->getAverageZ( );

    //Storage the current z index.
    int currentZ = 0;
    for (unsigned int h = 0, s = 0; h < _horizons.size( ) - 1; h++, s++)
    {
        //Add slice to set horizon.
        _horizonSlices.push_back( currentZ );

        //Get the horizon z average.
        double zH1 = _horizons[h + 0]->getAverageZ( );
        double zH2 = _horizons[h + 1]->getAverageZ( );

        if (zH1 < zR && zR < zH2)
        {
            //Get the bottom and top values of z on reservoir.
            double zRBottom, zRTop;
            if (_inputMesh->invertedZ( ))
            {
                zRBottom = _inputMesh->getAverageZOnBottomSlice( _inputMesh->getGrid( ).Nk( ) - 1 );
                zRTop = _inputMesh->getAverageZOnBottomSlice( 0 );
            }
            else
            {
                zRBottom = _inputMesh->getAverageZOnBottomSlice( 0 );
                zRTop = _inputMesh->getAverageZOnBottomSlice( _inputMesh->getGrid( ).Nk( ) - 1 );
            }

            //Compute limits between the bottom horizon and the reservoir.
            int zi = currentZ;
            int zf = zi + _slices[s];
            s++;

            //Initialize points between the bottom horizon and the reservoir.
            fillCellsBetweenTwoLayers( zi, zf, zH1, zRBottom );

            //Compute the reservoir limits.
            zi = zf;
            zf = zi + _slices[s];
            s++;

            //Save where the reservoir begins.
            _reservoirK = zi;

            //Initialize points on reservoir.
            fillCellsBetweenTwoLayers( zi, zf, zRBottom, zRTop );

            //Compute limits between the reservoir and top horizon.
            zi = zf;
            zf = zi + _slices[s];

            //Initialize points between the reservoir and top horizon.
            fillCellsBetweenTwoLayers( zi, zf, zRTop, zH2 );

            currentZ = zf;
        }
        else
        {
            //Set the values of z-index to start and finish.
            int zi = currentZ;

            //Include the last slice of points at the last iteration.
            int zf = zi + _slices[s] + ( s == _horizons.size( ) ? 1 : 0 );

            //Compute initial points between zi and zf.
            fillCellsBetweenTwoLayers( zi, zf, zH1, zH2 );

            currentZ = zf;
        }
    }

    //Add slice to set horizon.
    _horizonSlices.push_back( currentZ - 1 );
}



void ReservoirModeling::fillCellsBetweenTwoLayers( const int zi, const int zf,
                                                   double zMin, const double zMax )
{
    const double xMin = _inputMesh->getXMin( ) - _distanceI;
    const double xMax = _inputMesh->getXMax( ) + _distanceI;

    const double yMin = _inputMesh->getYMin( ) - _distanceJ;
    const double yMax = _inputMesh->getYMax( ) + _distanceJ;

    int den = zf - zi;

    //Guarantee that the last point has zMax (Horizon z.)
    if (zf == _nk)
        den--;

    for (int k = zi; k < zf; k++)
    {
        double z = zMin + ( k - zi ) * ( zMax - zMin ) / den;

        for (int j = 0; j < _nj; j++)
        {
            double y = yMin + j * ( yMax - yMin ) / ( _nj - 1 );

            for (int i = 0; i < _ni; i++)
            {
                Point p;
                p.x[0] = xMin + i * ( xMax - xMin ) / ( _ni - 1 );
                p.x[1] = y;
                p.x[2] = z;

                _outputGrid[i][j][k] = p;
            }
        }
    }
}



void ReservoirModeling::computeNumberOfSlices( )
{
    //Get the density of cells.
    double cellsByMeters = _inputMesh->getCellsByMeters( ) / 8;
    //        double cellsByMeters = _inputMesh->getCellsByMeters( );

    //Allocate space to store the number of slices.
    unsigned int n = _horizons.size( ) + 1, cont = 0;
    _slices.resize( n, 0 );

    //Get the average of z on reservoir.
    double zR = _inputMesh->getAverageZ( );

    for (unsigned int i = 0; i < _horizons.size( ) - 1; i++)
    {
        //Get horizons max and min z.
        double zH1 = _horizons[i + 0]->getMinZ( );
        double zH2 = _horizons[i + 1]->getMaxZ( );

        double minZ = 0, maxZ = 0;

        if (zH1 < zR && zR < zH2)
        {
            //Set the number of slices between the bottom horizon and reservoir.
            minZ = zH1;
            maxZ = zR;

            _slices[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );

            //Set the number of slices on reservoir.
            _slices[cont++] = _inputMesh->getGrid( ).Nk( );

            //Set the number of slices between the top horizon and reservoir.
            minZ = zR;
            maxZ = zH2;

            _slices[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );
        }
        else
        {
            //Set the number of slices between two horizons.
            minZ = zH1;
            maxZ = zH2;
            _slices[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );
        }
    }
}



void ReservoirModeling::rotatePoint( Point3D& p, double angle )
{
    double x = p[0], y = p[1];
    p[0] = x * cos( angle ) - y * sin( angle );
    p[1] = x * sin( angle ) + y * cos( angle );
}



void ReservoirModeling::posprocess( )
{
    double angle = _inputMesh->getRotationAngle( );

    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                rotatePoint( _outputGrid[i][j][k].x, angle );
            }
        }
    }
    _inputMesh->rotateData( angle );
    for (unsigned int h = 0; h < _horizons.size( ); h++)
    {
        _horizons[h]->rotateData( angle );
    }
}



void ReservoirModeling::preprocess( )
{
    std::sort( _horizons.begin( ), _horizons.end( ), compare );

    //compute the number of slices.
    if (_slices.size( ) == 0)
    {
        computeNumberOfSlices( );
    }

    for (unsigned int i = 0; i < _slices.size( ); i++)
        std::cout << _slices[i] << std::endl;
}



ReservoirModeling::~ReservoirModeling( )
{
}

