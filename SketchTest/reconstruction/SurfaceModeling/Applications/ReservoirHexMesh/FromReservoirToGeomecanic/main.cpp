#include "ReservoirMesh.h"
#include "ReservoirHorizon.h"
#include "ReservoirModeling.h"
#include <map>
#include "../../../../../libs/Timer/Timer.h"


const double tx = 354720.50000000000;
const double ty = 7516884.9999999991;
using namespace std;



int main( int argc, char** argv )
{
    if (argc < 3)
    {
        printf( "Please, give the horizons path...\n" );
        return 0;
    }
    Timer t;

    const unsigned int numHori = 8;
    const unsigned int numThreads = atoi( argv[1] );
    const std::string path = argv[2];

    std::string horizonNames[numHori] = { "Base.dat", "FundoMar.dat",
                                         "Horizonte1.dat", "Horizonte2.dat",
                                         "Horizonte3.dat", "Horizonte3_Base.dat", "Horizonte4.dat",
                                         "Horizonte5.dat" };

    std::vector<ReservoirHorizon> horizons( numHori );

    ReservoirModeling m;

    ReservoirMesh r;
    r.readReservoirGeresimFile( path + "../Pituba.txt" );

    m.setReservoirMesh( &r );

    for (unsigned int i = 0; i < numHori; i++)
    {
        horizons[i].readFile( path + horizonNames[i] );

        double angle = r.getRotationAngle( );
        horizons[i].preprocess( tx, ty, -angle );

        m.addHorizon( &horizons[i] );
    }

    int ni = r.getGrid( ).Ni( ) / 8;
    int nj = r.getGrid( ).Nj( ) / 8;

    m.setNumCellsI( ni );
    m.setNumCellsJ( nj );

    double w = r.getWidth( ) / 8;
    double l = r.getLengh( ) / 8;

    m.setIIncrement( w );
    m.setJIncrement( l );
    m.setNumThreads( numThreads );
    m.generateMesh( );

    //m.writeNeutralFileForInput( "/local/jcoelho/Dropbox/trans/Pituba/ModelInput.pos" );
    m.writeNeutralFileForOuput( "Model.pos" );
    m.writeNeutralFileRodrigo( "Pituba.pos" );
    //    m.writeNeutralFileForOuput( path + "Model.pos" );

    t.printTime( "Total time" );

    return 0;
}