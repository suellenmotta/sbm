/* 
 * File:   ReservoirModeling.h
 * Author: jcoelho
 *
 * Created on February 23, 2017, 4:28 PM
 */

#ifndef RESERVOIRMODELING_H
#define RESERVOIRMODELING_H

#include <set>
#include "ReservoirMesh.h"
#include "ReservoirHorizon.h"

#include "../../../ModelManager/OptimizationModel.h"
#include "../../../ModelManager/Variable.h"

class ReservoirModeling
{
public:
    /**
     * Default constructor.
     */
    ReservoirModeling( );

    /**
     * Add a new horizon.
     * @param horizon - new horizon ready to be used.
     */
    void addHorizon( ReservoirHorizon* horizon );

    /**
     * Generate the cell mesh.
     */
    void generateMesh( );

    /**
     * Set the increment in meters in the i direction.
     * @param i - the increment in meters in the i direction.
     */
    void setIIncrement( const double i );

    /**
     * Set the increment in meters in the j direction.
     * @param j - the increment in meters in the i direction.
     */
    void setJIncrement( const double j );

    /**
     * Define the number of cells on I direction.
     * @param i - number of cells on I direction.
     */
    void setNumCellsI( const unsigned int i );

    /**
     * Define the number of cells on J direction.
     * @param J - number of cells on J direction.
     */
    void setNumCellsJ( const unsigned int j );

    /**
     * Define the number of cell between each pair of surfaces.
     * @param slices - number of cell between each pair of surfaces.
     */
    void setNumCellsAmongSurfaces( const std::vector<int>& slices );

    /**
     * Set the number of threads that must be used.
     * @param numThreads - number of threads that must be used.
     */
    void setNumThreads( const int numThreads );

    /**
     * Set a reservoir mesh.
     * @param mesh - reservoir mesh ready to be used.
     */
    void setReservoirMesh( ReservoirMesh* mesh );

    /**
     * Write the model on Neutral File format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writeNeutralFileForInput( const std::string& path );

    /**
     * Write the output model on Neutral File format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writeNeutralFileForOuput( const std::string& path );

    /**
     * Write the output model on Rodrigo's format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writeNeutralFileRodrigo( const std::string& path );

    /**
     * Destructor.
     */
    virtual ~ReservoirModeling( );
private:

    /**
     * Add constraints to conformal the mesh.
     * @param m - current model.
     */
    void addConformalConstraints( ModelManager::OptimizationModel& m );

    /**
     * Allocate the output mesh.
     */
    void allocateMemory( );

    /**
     * Check if all data is ready to be processed.
     * @return - true if everything is okay.
     */
    bool checkData( );

    /**
     * Compute the initial position points.
     */
    void computeInitialGeometry( );

    /**
     * Compute the number of slices between each pair of surfaces.
     */
    void computeNumberOfSlices( );

    /**
     * Compute the point index.
     * @param i, j, k - grid point position.
     * @return - point index.
     */
    int computePointIndex( int i, int j, int k );

    /**
     * Compute initial cells between two layers.
     * @param zi - initial z index.
     * @param zf - final z index.
     * @param zMin - initial z value.
     * @param zMax - final z value.
     */
    void fillCellsBetweenTwoLayers( const int zi, const int zf,
                                    double zMin, const double zMax );

    /**
     * Build model and compute mesh.
     */
    void optimize( );

    /**
     * Pos-process the data.
     */
    void posprocess( );

    /**
     * Preprocess the data.
     */
    void preprocess( );

    /**
     * Project the cells on horizon.
     * @param h - current horizon.
     * @param cells - cells to be projected.
     */
    void projectCellsOnHorizon( unsigned int h,
                                const std::set<Point3D>& cells );

    /**
     * Project horizon triangles on mesh.
     * @param slice - slice mesh to project the triangles.
     * @param h - horizon to be projected.
     * @param triangles - triangle list to project.
     * @param cells - cell where the triangles were projected.
     */
    void projectHorizonOnMesh( unsigned int slice, unsigned int h,
                               const std::vector<CornerType>& triangles,
                               std::set<Point3D>& cells );

    /**
     * Project one pont on horizon.
     * @param p - point to be projected.
     * @param h - horizon index.
     * @return true if the point was projected on horizon. False otherwise.
     */
    bool projectPointOnHorizon( Point3D& p, unsigned int h );

    /**
     * Rotate a single point.
     * @param p - point to be rotated.
     */
    void rotatePoint( Point3D& p, double angle );

    /**
     * Define that points that cannot move during optimization process.
     */
    void setHardPointsPosition( );

    /**
     * Project points of an horizon and fix the mesh points.
     * @param slice - slice to be fixed.
     * @param h - current horizon to be processed.
     */
    void setHorizonPoints( unsigned int slice, unsigned int h );

private:

    struct Var
    {
        ModelManager::Variable x, y, z;
    };

    struct Point
    {

        Point( )
        {
            active = false;
        }

        Point3D x;
        bool active;
    };

    /**
     * Distances to be extend on I and J directions.
     */
    double _distanceI, _distanceJ;

    /**
     * Number of fixed boundary cells.
     */
    int _fixedBoundaryPoints;

    /**
     * Horizon list.
     */
    std::vector<ReservoirHorizon*> _horizons;

    /**
     * Slice of points where each horizon is fixed.
     */
    std::vector<int> _horizonSlices;

    /**
     * Input reservoir mesh.
     */
    ReservoirMesh* _inputMesh;

    /**
     * Output grid dimensions.
     */
    int _ni, _nj, _nk;

    /**
     * Number of cells to be extended on I and J directions.
     */
    unsigned int _numCellsI, _numCellsJ;

    /**
     * Number of threads.
     */
    int _numThreads;

    /**
     * Output grid.
     */
    std::vector< std::vector< std::vector < Point > > > _outputGrid;

    /**
     * Slice where the reservoir begins.
     */
    int _reservoirK;

    /**
     * Number of slices between each pair of surfaces.
     */
    std::vector<int> _slices;

    /**
     * Variable grid.
     */
    std::vector< std::vector< std::vector < Var > > > _v;
};

#endif /* RESERVOIRMODELING_H */

