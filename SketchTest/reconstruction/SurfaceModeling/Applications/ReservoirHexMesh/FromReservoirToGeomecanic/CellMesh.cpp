/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CellMesh.cpp
 * Author: jcoelho
 * 
 * Created on February 1, 2017, 11:40 AM
 */

#include "CellMesh.h"



CellMesh::CellMesh( )
{
    _active = _fault = _fixed = false;

    for (int f = 0; f < 6; f++)
        _adjacents[f] = -1;

    for (int p = 0; p < 8; p++)
        _points[p] = -1;
}



CellMesh::CellMesh( const CellMesh& c )
{
    _active = c._active;
    _fault = c._fault;
    _fixed = c._fixed;

    for (int f = 0; f < 6; f++)
        _adjacents[f] = c.adjacent( f );

    for (int p = 0; p < 8; p++)
        _points[p] = c.point( p );
}



int CellMesh::point( unsigned int p ) const
{
    return _points[p];
}



int& CellMesh::adjacent( unsigned int f )
{
    return _adjacents[f];
}



int CellMesh::adjacent( unsigned int f ) const
{
    return _adjacents[f];
}



bool CellMesh::active( ) const
{
    return _active;
}



bool CellMesh::fault( ) const
{
    return _fault;
}



bool CellMesh::fixed( ) const
{
    return _fixed;
}



void CellMesh::setFault( bool fault )
{
    _fault = fault;
}



void CellMesh::setPoints( int p[] )
{
    for (unsigned int i = 0; i < 8; i++)
    {
        _points[i] = p[i];
    }
}



void CellMesh::setPoint( int p, int v )
{
    _points[p] = v;
}



void CellMesh::setFixed( bool fixed )
{
    _fixed = fixed;
}



void CellMesh::setAdjacentCells( int a[] )
{
    for (unsigned int i = 0; i < 6; i++)
    {
        _adjacents[i] = a[i];
    }
}



void CellMesh::setAdjacent( int f, int adj )
{
    _adjacents[f] = adj;
}



void CellMesh::active( bool ac )
{
    _active = ac;
}



CellMesh& CellMesh::operator=( const CellMesh& c )
{
    _active = c._active;
    _fault = c._fault;
    _fixed = c._fixed;

    for (int f = 0; f < 6; f++)
        _adjacents[f] = c.adjacent( f );

    for (int p = 0; p < 8; p++)
        _points[p] = c.point( p );

    return *this;
}