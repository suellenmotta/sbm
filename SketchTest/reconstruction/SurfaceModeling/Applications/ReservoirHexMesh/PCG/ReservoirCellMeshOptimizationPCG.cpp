/* 
 * File:   ReservoirCellMeshOptimization.cpp
 * Author: jcoelho
 * 
 * Created on July 26, 2016, 1:45 PM
 */

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <omp.h>


#include "../grid3d.h"
#include "../Point3D.h"
#include "ReservoirCellMeshOptimizationPCG.h"
#include "../../../../../libs/Timer/Timer.h"

namespace MeshModeling
{

    //Vector to access the neighborhood to set matrix elements.
    const int neighbouhood[25][4] = {
        {+0, +0, -2, +02 },
        {+0, -1, -1, +04 },
        {-1, +0, -1, +04 },
        {+0, +0, -1, -24 },
        {+1, +0, -1, +04 },
        {+0, +1, -1, +04 },
        {+0, -2, +0, +02 },
        {-1, -1, +0, +04 },
        {+0, -1, +0, -24 },
        {+1, -1, +0, +04 },
        {-2, +0, +0, +02 },
        {-1, +0, +0, -24 },
        {+0, +0, +0, +84 },
        {+1, +0, +0, -24 },
        {+2, +0, +0, +02 },
        {-1, +1, +0, +04 },
        {+0, +1, +0, -24 },
        {+1, +1, +0, +04 },
        {+0, +2, +0, +02 },
        {+0, -1, +1, +04 },
        {-1, +0, +1, +04 },
        {+0, +0, +1, -24 },
        {+1, +0, +1, +04 },
        {+0, +1, +1, +04 },
        {+0, +0, +2, +02 }
    };



    /***********************PUBLIC METHODS*****************************************/

    ReservoirCellMeshOptimizationPCG::ReservoirCellMeshOptimizationPCG( )
    {
        _eps = 1e-3;
        _iterations = 0;
        _maxSubIterations = 5000;
        _maxIterations = 50;
        _showLog = true;
        _stepsNumber = 1;
        _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    }



    void ReservoirCellMeshOptimizationPCG::computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                                                     const std::vector<bool>& activeCells,
                                                                     const unsigned int fixedBoundaryCells )
    {
        if (_showLog)
        {
            printf( "\n    Pre-processing active cells and fixed boundary points...\n" );
        }
        Timer t;

        //Save the number of fixed boundary cells.
        _fixedBoundaryCells = fixedBoundaryCells;

        //Get the grid size.
        const unsigned int gridSize = grid.Size( );

        //Compute the fixed points.
        std::vector<bool> activePoints( gridSize, false );
        const unsigned int fixedPoints = preprocessFixedPoints( grid, activeCells, activePoints );
        const unsigned int numberUnknowPoints = gridSize - fixedPoints;

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    %u points was fixed on preprocessing step...\n", fixedPoints );
            printf( "    %u points have unknown values...\n", numberUnknowPoints );
            printf( "\n    Computing re-index vector...\n" );
        }

        t.restart( );

        //Vector to store each variable index.
        std::vector<unsigned int> reindexPoints;
        computeReindexPointsVector( activePoints, reindexPoints );
        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    Counting nonzero matrix elements...\n" );
        }

        //Allocate matrix.
        const unsigned int numberNonZero = computeNonZeroMatrixElements( grid, activePoints, numberUnknowPoints );

        //Allocate the memory to store the matrix.
        std::vector<unsigned int> ptr( numberUnknowPoints + 1, 0 ), index( numberNonZero, 0 );
        std::vector<FPType> value( numberNonZero, 0.0 ), b( numberUnknowPoints, 0.0 ), x( numberUnknowPoints, 0.0 );

        t.restart( );

        if (_showLog)
        {
            printf( "\n    The matrix has %u nonzero elements...\n", numberNonZero );
            t.printTime( "        Total time" );
            printf( "\n    Building matrix in CSR format...\n" );
        }

        t.restart( );

        //Compute index vectors.
        computeIndexVectors( grid, activePoints, reindexPoints,
                             numberUnknowPoints, numberNonZero,
                             ptr, index, value );

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    **************************OPTIMIZING**************************\n\n" );
        }

        for (unsigned int dimension = 0; dimension < 3; dimension++)
        {
            t.restart( );
            printf( "        DIMENSION %u", dimension + 1 );

            //Compute the vectors values for and specific dimension.
            computeValuesDimension( dimension, grid, activePoints, reindexPoints, x, b );

            if (_showLog)
                printf( "\n\n            ITERATION           GRADIENT NORM           TIME\n" );

            //Compute gradient.
            FPType gnorm = computeGradientNorm2( x, b, ptr, index, value );

            _iterations = 0;
            while (_iterations++ < _maxIterations && gnorm > _eps * _eps)
            {
                if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
                {
                    printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                    t.printTime( "" );
                }

                //Solve linear system.
                gnorm = PCG( ptr, index, value, x, b );

                //Update store solution.
                updateSolution( grid, x, activePoints, reindexPoints, dimension );
            }

            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                t.printTime( "" );
            }
        }
    }



    FPType ReservoirCellMeshOptimizationPCG::getEPS( ) const
    {
        return _eps;
    }



    unsigned int ReservoirCellMeshOptimizationPCG::getMaxSubIterations( ) const
    {
        return _maxSubIterations;
    }



    unsigned int ReservoirCellMeshOptimizationPCG::getMaxIterations( ) const
    {
        return _maxIterations;
    }



    unsigned int ReservoirCellMeshOptimizationPCG::getNumberIterations( ) const
    {
        return _iterations;
    }



    unsigned int ReservoirCellMeshOptimizationPCG::getNumberfOfThreads( ) const
    {
        return _numberOfThreads;
    }



    unsigned int ReservoirCellMeshOptimizationPCG::getStepsNumber( ) const
    {
        return _stepsNumber;
    }



    ReservoirCellMeshOptimizationPCG::~ReservoirCellMeshOptimizationPCG( )
    {

    }



    void ReservoirCellMeshOptimizationPCG::setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) )
    {
        _displayInformation = displayInformation;
    }



    void ReservoirCellMeshOptimizationPCG::setEPS( const FPType eps )
    {
        _eps = eps;
    }



    void ReservoirCellMeshOptimizationPCG::setMaxSubIterations( const unsigned int maxSubIter )
    {
        _maxSubIterations = maxSubIter;
    }



    void ReservoirCellMeshOptimizationPCG::setMaxIterations( const unsigned int maxIter )
    {
        _maxIterations = maxIter;
    }



    void ReservoirCellMeshOptimizationPCG::setNumberOfThreads( const unsigned int numThreads )
    {
        _numberOfThreads = numThreads;
    }



    void ReservoirCellMeshOptimizationPCG::showLog( const bool log )
    {
        _showLog = log;
    }



    void ReservoirCellMeshOptimizationPCG::setStepsNumber( const unsigned int sn )
    {
        _stepsNumber = sn;
    }



    /***********************PRIVATE METHODS****************************************/
    void ReservoirCellMeshOptimizationPCG::computeElementsPerLine( const DsGrid3D<Point3D>& grid,
                                                                   const std::vector<bool>& activePoints,
                                                                   const std::vector<unsigned int>& reindexPoints,
                                                                   std::vector<unsigned int>& ptr ) const
    {
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        for (int k = _fixedBoundaryCells; k < nk - _fixedBoundaryCells; k++)
        {
            for (int j = _fixedBoundaryCells; j < nj - _fixedBoundaryCells; j++)
            {
                for (int i = _fixedBoundaryCells; i < ni - _fixedBoundaryCells; i++)
                {
                    if (!activePoints[grid.GetIndex( i, j, k )])
                    {
                        for (int n = 0; n < 25; n++)
                        {
                            //Get the neighbor on grid.
                            int di = i + neighbouhood[n][0];
                            int dj = j + neighbouhood[n][1];
                            int dk = k + neighbouhood[n][2];
                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (!activePoints[gidx])
                                {
                                    int vidx = reindexPoints[gidx];
                                    ptr[vidx]++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    FPType ReservoirCellMeshOptimizationPCG::computeGradientNorm2( const std::vector<FPType>& x,
                                                                   const std::vector<FPType>& b,
                                                                   std::vector<unsigned int>& ptr,
                                                                   std::vector<unsigned int>& index,
                                                                   std::vector<FPType>& value ) const
    {
        const unsigned int n = x.size( );
        std::vector<FPType> sum( _numberOfThreads, 0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            FPType g = 0.0;
            for (unsigned int j = ptr[i]; j < ptr[i + 1]; j++)
            {
                //Get the variable index.
                unsigned int v = index[j];

                //Multiply the line by vector.
                g += value[j] * x[v];
            }
            g -= b[i];

            int thread = omp_get_thread_num( );
            sum[thread] += g * g;
        }

        FPType g2 = 0.0;
        for (unsigned int i = 0; i < sum.size( ); i++)
        {
            g2 += sum[i];
        }
        return g2;
    }



    void ReservoirCellMeshOptimizationPCG::computeIndexVectors( const DsGrid3D<Point3D>& grid,
                                                                const std::vector<bool>& activePoints,
                                                                const std::vector<unsigned int>& reindexPoints,
                                                                const unsigned int unknownPoints,
                                                                const unsigned int nonZero,
                                                                std::vector<unsigned int>& ptr,
                                                                std::vector<unsigned int>& index,
                                                                std::vector<FPType>& value ) const
    {
        //Compute the number of elements per line.
        computeElementsPerLine( grid, activePoints, reindexPoints, ptr );

        //Compute the nonzero columns.
        computeMatrixColumns( grid, activePoints, reindexPoints,
                              unknownPoints, nonZero, ptr, index, value );
    }



    void ReservoirCellMeshOptimizationPCG::computeMatrixColumns( const DsGrid3D<Point3D>& grid,
                                                                 const std::vector<bool>& activePoints,
                                                                 const std::vector<unsigned int>& reindexPoints,
                                                                 const unsigned int unknownPoints,
                                                                 const unsigned int nonZero,
                                                                 std::vector<unsigned int>& ptr,
                                                                 std::vector<unsigned int>& index,
                                                                 std::vector<FPType>& value ) const
    {
        memset( &value[0], 0, nonZero * sizeof (FPType ) );

        //Vector to know about next free position on vectors.
        std::vector<int> freePosition( unknownPoints, 0 );

        //Initialize the last position with the number of nonzero elements on
        //matrix.
        ptr[unknownPoints] = nonZero;

        //Compute free positions and adjust the ptr vector to store where each
        //line start.
        for (int i = ( int ) unknownPoints - 1; i >= 0; i--)
        {
            ptr[i] = ptr[i + 1] - ptr[i];
            freePosition[i] = ptr[i];
        }

        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        for (int k = _fixedBoundaryCells; k < nk - _fixedBoundaryCells; k++)
        {
            for (int j = _fixedBoundaryCells; j < nj - _fixedBoundaryCells; j++)
            {
                for (int i = _fixedBoundaryCells; i < ni - _fixedBoundaryCells; i++)
                {
                    //Get the current point index on grid
                    int currentPoint = grid.GetIndex( i, j, k );
                    if (!activePoints[currentPoint])
                    {
                        currentPoint = reindexPoints[currentPoint];
                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (!activePoints[gidx])
                                {
                                    int vidx = reindexPoints[gidx];
                                    unsigned int f = freePosition[currentPoint];

                                    index[f] = vidx;
                                    value[f] = neighbouhood[n][3];
                                    freePosition[currentPoint]++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    unsigned int ReservoirCellMeshOptimizationPCG::computeNonZeroMatrixElements( const DsGrid3D<Point3D>& grid,
                                                                                 const std::vector<bool>& activePoints,
                                                                                 const unsigned int unknownPoints ) const
    {
        const unsigned int nk = grid.Nk( );
        const unsigned int nj = grid.Nj( );
        const unsigned int ni = grid.Ni( );
        unsigned int nonzero = 0;

        //Compute the number of unknown points.
        for (unsigned int k = _fixedBoundaryCells; k < nk - _fixedBoundaryCells; k++)
        {
            for (unsigned int j = _fixedBoundaryCells; j < nj - _fixedBoundaryCells; j++)
            {
                for (unsigned int i = _fixedBoundaryCells; i < ni - _fixedBoundaryCells; i++)
                {
                    int neighbours = 0, bumpX = 0, bumpY = 0, bumpZ = 0;
                    for (int m = -1; m <= 1; m += 2)
                    {
                        if (!activePoints[grid.GetIndex( i + m, j + 0, k + 0 )])
                        {
                            neighbours++;
                            bumpX++;
                        }
                        if (!activePoints[grid.GetIndex( i + 0, j + m, k + 0 )])
                        {
                            neighbours++;
                            bumpY++;
                        }
                        if (!activePoints[grid.GetIndex( i + 0, j + 0, k + m )])
                        {
                            neighbours++;
                            bumpZ++;
                        }
                    }

                    if (!activePoints[grid.GetIndex( i, j, k )])
                    {
                        neighbours++;
                    }

                    //Count the number of crossing products.
                    nonzero += neighbours * ( neighbours - 1 ) / 2;

                    //Correct the crossing products that are counted once.
                    if (bumpX == 2) nonzero++;
                    if (bumpY == 2) nonzero++;
                    if (bumpZ == 2) nonzero++;
                }
            }
        }

        //Count the diagonal variables.
        nonzero += unknownPoints;

        return nonzero;
    }



    void ReservoirCellMeshOptimizationPCG::computeReindexPointsVector( const std::vector<bool> &activePoints,
                                                                       std::vector<unsigned int>& reindexPoints ) const
    {
        //Allocate storage.
        reindexPoints.resize( activePoints.size( ), -1 );

        //Compute new indexes.
        for (unsigned int i = 0, index = 0; i < activePoints.size( ); i++)
        {
            if (!activePoints[i])
            {
                reindexPoints[i] = index;
                index++;
            }
            else
            {
                reindexPoints[i] = -1;
            }
        }
    }



    void ReservoirCellMeshOptimizationPCG::computeValuesDimension( const unsigned int dimension,
                                                                   const DsGrid3D<Point3D>& grid,
                                                                   const std::vector<bool>& activePoints,
                                                                   const std::vector<unsigned int>& reindexPoints,
                                                                   std::vector<FPType>& x,
                                                                   std::vector<FPType>& b ) const
    {
        //Initialize vectors.
        initializeVectors( x, b );

        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < nk - _fixedBoundaryCells; k++)
        {
            for (int j = _fixedBoundaryCells; j < nj - _fixedBoundaryCells; j++)
            {
                for (int i = _fixedBoundaryCells; i < ni - _fixedBoundaryCells; i++)
                {
                    //Get the current point index on grid
                    int gidx = grid.GetIndex( i, j, k );

                    if (!activePoints[gidx])
                    {
                        //Get the variable index.
                        int vidx = reindexPoints[gidx];

                        //Set initial solution.
                        x[vidx] = grid.Get( i, j, k ).x[dimension];

                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx2 = grid.GetIndex( di, dj, dk );
                                if (activePoints[gidx2])
                                {
                                    //Get the variable index.
                                    FPType val = -neighbouhood[n][3] * grid.Get( gidx2 ).x[dimension];
                                    b[vidx] += val;
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    void ReservoirCellMeshOptimizationPCG::initializeVectors( std::vector<FPType>& x,
                                                              std::vector<FPType>& b ) const
    {
        const unsigned int n = x.size( );
        //Initialize x and b vectors.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            x[i] = 0.0;
            b[i] = 0.0;
        }
    }



    unsigned int ReservoirCellMeshOptimizationPCG::preprocessFixedPoints( const DsGrid3D<Point3D>& grid,
                                                                          const std::vector<bool>& activeCells,
                                                                          std::vector<bool>& activePoints ) const
    {
        int nk = grid.Nk( );
        int nj = grid.Nj( );
        int ni = grid.Ni( );
        int celIndex = 0;

        //Fix points on active cells.
        for (int k = 0; k < nk - 1; k++)
        {
            for (int j = 0; j < nj - 1; j++)
            {
                for (int i = 0; i < ni - 1; i++)
                {
                    if (activeCells[celIndex])
                    {
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 1 )] = true;
                    }
                    celIndex++;
                }
            }
        }

        //Fix points on boundary cells.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = 0; k < nk; k++)
        {
            for (int j = 0; j < nj; j++)
            {
                for (int i = 0; i < ni; i++)
                {
                    if (i > ( int ) _fixedBoundaryCells && i < ( int ) ni - 1 - ( int ) _fixedBoundaryCells &&
                        j > ( int ) _fixedBoundaryCells && j < ( int ) nj - 1 - ( int ) _fixedBoundaryCells &&
                        k > ( int ) _fixedBoundaryCells && k < ( int ) nk - 1 - ( int ) _fixedBoundaryCells)
                    {
                        continue;
                    }
                    activePoints[grid.GetIndex( i, j, k )] = true;
                }
            }
        }

        //Get the number of fixed points.
        std::vector<unsigned int> sum( _numberOfThreads, 0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < activePoints.size( ); i++)
        {
            if (activePoints[i])
            {
                int thread = omp_get_thread_num( );
                sum[thread]++;
            }
        }

        unsigned totalFixedPoints = 0;
        for (unsigned int i = 0; i < sum.size( ); i++)
        {
            totalFixedPoints += sum[i];
        }
        return totalFixedPoints;
    }



    void ReservoirCellMeshOptimizationPCG::updateSolution( DsGrid3D<Point3D>& grid,
                                                           const std::vector<FPType>& x,
                                                           const std::vector<bool>& activePoints,
                                                           const std::vector<unsigned int>& reindexPoints,
                                                           const unsigned int dimension ) const
    {
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < nk - _fixedBoundaryCells; k++)
        {
            for (int j = _fixedBoundaryCells; j < nj - _fixedBoundaryCells; j++)
            {
                for (int i = _fixedBoundaryCells; i < ni - _fixedBoundaryCells; i++)
                {
                    //Get the grid index.
                    int gidx = grid.GetIndex( i, j, k );

                    if (!activePoints[gidx])
                    {
                        //Get the variable index.
                        int vidx = reindexPoints[gidx];

                        //Update solution.
                        grid.GetRef( gidx ).x[dimension] = x[vidx];
                    }
                }
            }
        }
    }



    /***********************PCG ALG. METHODS***************************************/
    FPType ReservoirCellMeshOptimizationPCG::computeInitialResidual( const std::vector<unsigned int>& ptr,
                                                                     const std::vector<unsigned int>& index,
                                                                     const std::vector<FPType>& value,
                                                                     const std::vector<FPType>& x,
                                                                     const std::vector<FPType>& b,
                                                                     std::vector<FPType>& r )const
    {
        unsigned int n = x.size( );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            FPType val = 0.0;
            for (unsigned int j = ptr[i]; j < ptr[i + 1]; j++)
            {
                //Get the variable index.
                unsigned int v = index[j];

                //Multiply the line by vector.
                val += value[j] * x[v];
            }
            //Set resultant vector.
            r[i] = val;
        }

        std::vector<FPType> sum( _numberOfThreads, 0 );

        //Compute residual vector.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            r[i] = b[i] - r[i];

            int thread = omp_get_thread_num( );
            sum[thread] += r[i] * r[i];
        }

        FPType rnorm2 = 0.0;
        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            rnorm2 += sum[i];
            sum[i] = 0.0;
        }

        return rnorm2;
    }



    void ReservoirCellMeshOptimizationPCG::matrixVec( const std::vector<unsigned int>& ptr,
                                                      const std::vector<unsigned int>& index,
                                                      const std::vector<FPType>& value,
                                                      const std::vector<FPType>& p,
                                                      std::vector<FPType>& q ) const
    {
        unsigned int n = p.size( );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            FPType val = 0.0;
            for (unsigned int j = ptr[i]; j < ptr[i + 1]; j++)
            {
                //Get the variable index.
                unsigned int v = index[j];

                //Multiply the line by vector.
                val += value[j] * p[v];
            }

            //Set resultant vector.
            q[i] = val;
        }
    }



    FPType ReservoirCellMeshOptimizationPCG::PCG( const std::vector<unsigned int>& ptr,
                                                  const std::vector<unsigned int>& index,
                                                  const std::vector<FPType>& value,
                                                  std::vector<FPType>& x,
                                                  std::vector<FPType>& b,
                                                  FPType reps ) const
    {
        //Get the number of variables.
        unsigned int n = x.size( );

        //Allocate auxiliary vectors.
        std::vector<FPType> r( n, 0.0 ), p( n, 0.0 ), q( n, 0.0 );
        std::vector<FPType> sum( _numberOfThreads, 0 );

        //Compute initial residual.
        FPType rnorm2 = computeInitialResidual( ptr, index, value, x, b, r );
        FPType rho = 0.0, rhoOld = 1.0;

        //Iterate.
        for (unsigned int iter = 0; iter < _maxSubIterations; iter++)
        {
            //For this algorithm rho is equal to <r, r>/84.
            rho = rnorm2;

            //Compute p.
            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                p[i] = ( rhoOld * r[i] + rho * p[i] ) / ( 84.0 * rhoOld );
            }

            //q = A*p and gradient = ||Ax - b||
            matrixVec( ptr, index, value, p, q );

            //<p, q>
            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                int thread = omp_get_thread_num( );
                sum[thread] += p[i] * q[i];
            }

            FPType dotPQ = 0;
            for (unsigned int i = 0; i < _numberOfThreads; i++)
            {
                dotPQ += sum[i];
                sum[i] = 0.0;
            }

            //Compute x(k) = x(k-1) + alpha * p(k)
            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                x[i] = x[i] + rho * p[i] / ( 84.0 * dotPQ );
                r[i] = r[i] - rho * q[i] / ( 84.0 * dotPQ );

                int thread = omp_get_thread_num( );
                sum[thread] += r[i] * r[i];
            }

            rnorm2 = 0;
            for (unsigned int i = 0; i < _numberOfThreads; i++)
            {
                rnorm2 += sum[i];
                sum[i] = 0.0;
            }

            rhoOld = rho / 84.0;
            if (rnorm2 <= _eps * _eps)
            {
                return rnorm2;
            }
        }
        return rnorm2;
    }
}