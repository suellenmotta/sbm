/* 
 * File:   Point3D.h
 * Author: jcoelho
 *
 * Created on July 26, 2016, 2:16 PM
 */

#ifndef POINT3D_H
#define POINT3D_H

typedef double FPType;
#include <iostream>
#include <cstdio>

class Point3D
{
public:
    FPType x[3];

    static constexpr double EPS = 0.1;
public:

    /**
     * Default constructor.
     */
    Point3D( double x = 0, double y = 0, double z = 0 )
    {
        this->x[0] = x;
        this->x[1] = y;
        this->x[2] = z;
    }

    Point3D( const Point3D& p )
    {
        x[0] = p.x[0];
        x[1] = p.x[1];
        x[2] = p.x[2];
    }

    /**
     * Construtor that receives a (x,y,z) as a double vector.
     */
    Point3D( double coord[3] )
    {
        x[0] = coord[0];
        x[1] = coord[1];
        x[2] = coord[2];
    }

    /**
     * Overload the operator [] to access the point coordinates.
     * @param i - coordinate index.
     * @return - reference to coordinate.
     */
    FPType& operator[]( int i )
    {
        return x[i];
    }

    /**
     * Overload the operator [] to access the point coordinates.
     * @param i - coordinate index.
     * @return - reference to coordinate.
     */
    const FPType& operator[]( int i ) const
    {
        return x[i];
    }

    /**
     * Add two points.
     */
    Point3D operator+( Point3D q ) const
    {
        return Point3D( x[0] + q.x[0],
                        x[1] + q.x[1],
                        x[2] + q.x[2] );
    }

    Point3D& operator+=( Point3D q )
    {
        for (int i = 0; i < 3; i++)
            x[i] += q[i];

        return *this;
    }

    Point3D& operator-=( Point3D q )
    {
        for (int i = 0; i < 3; i++)
            x[i] -= q[i];

        return *this;
    }

    Point3D& operator*=( double t )
    {
        for (int i = 0; i < 3; i++)
            x[i] *= t;

        return *this;
    }

    Point3D& operator/=( double t )
    {
        for (int i = 0; i < 3; i++)
            x[i] /= t;

        return *this;
    }

    /**
     * Subtract two points.
     */
    Point3D operator-( Point3D q ) const
    {
        return Point3D( x[0] - q.x[0],
                        x[1] - q.x[1],
                        x[2] - q.x[2] );
    }

    /**
     * Multiply each coordinate by a double.
     */
    Point3D operator*( FPType t ) const
    {
        return Point3D( x[0] * t,
                        x[1] * t,
                        x[2] * t );
    }

    /**
     * Divide each coordinate by a double.
     */
    Point3D operator/( FPType t ) const
    {
        return Point3D( x[0] / t,
                        x[1] / t,
                        x[2] / t );
    }

    /**
     *  Scalar multiplication between points.
     */
    double operator*( Point3D q ) const
    {
        return x[0] * q.x[0] +
            x[1] * q.x[1] +
            x[2] * q.x[2];
    }

    /**
     * Crossproduct between two points.
     */
    Point3D operator^( Point3D q ) const
    {
        return Point3D( x[1] * q.x[2] - x[2] * q.x[1],
                        x[2] * q.x[0] - x[0] * q.x[2],
                        x[0] * q.x[1] - x[1] * q.x[0] );
    }

    /**
     * Compare two number with EPS tolerance.
     * @param x - first number
     * @param y - second number
     * @param tol - tolerance.
     * @return -1 if x < y, 0 if x = y and 1 if x > y.
     */
    inline int cmp( double x, double y = 0, double tol = EPS ) const
    {
        return (x <= y + tol ) ? ( x + tol < y ) ? -1 : 0 : 1;
    }

    /**
     * Compare two points.
     */
    int cmp( Point3D q ) const
    {
        if (int t = cmp( x[0], q[0] ))
            return t;
        if (int t = cmp( x[1], q[1] ))
            return t;

        return cmp( x[2], q[2] );
    }

    /**
     * Compare if two points are equal.
     */
    bool operator==( Point3D q ) const
    {
        return cmp( q ) == 0;
    }

    /**
     * Compare if two points are different. 
     */
    bool operator!=( Point3D q ) const
    {
        return cmp( q ) != 0;
    }

    /**
     * Compare if a point is smaller than other.
     */
    bool operator<( Point3D q ) const
    {
        return cmp( q ) < 0;
    }
    
    Point3D& operator=(const Point3D& p)
    {
        x[0] = p.x[0];
        x[1] = p.x[1];
        x[2] = p.x[2];
        
        return *this;
    }

    /**
     * Compare if a point is greater than other.
     */
    bool operator>( Point3D q ) const
    {
        return cmp( q ) > 0;
    }

    /**
     * Return a ostream to output the point information  (x, y, z).
     */
    friend std::ostream& operator<<( std::ostream& o, const Point3D& p )
    {
        char out[1024];
        sprintf( out, "%.8lf %.8lf %.8lf", p[0], p[1], p[2] );

        return o << out;
    }

    /**
     * Read a point from stream.
     * @param is - the current stream.
     * @param obj - the point to be read.
     * @return - the stream.
     */
    friend std::istream& operator>>( std::istream& is, Point3D& obj )
    {
        //Read obj from stream
        for (int i = 0; i < 3; i++)
            is >> obj.x[i];

        return is;
    }
};

#endif /* POINT3D_H */

