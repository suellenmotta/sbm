/* 
 * File:   ReservoirCellMeshOptimization.cpp
 * Author: jcoelho
 * 
 * Created on July 26, 2016, 1:45 PM
 */

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <omp.h>


#include "../grid3d.h"
#include "../Point3D.h"
#include "ReservoirCellMeshOptimizationEbE.h"
#include "../../../../../libs/Timer/Timer.h"

namespace MeshModeling
{

    //Vector to access the neighborhood to set matrix elements.
    const int neighbouhood[25][4] = {
        {+0, +0, -2, +02 },
        {+0, -1, -1, +04 },
        {-1, +0, -1, +04 },
        {+0, +0, -1, -24 },
        {+1, +0, -1, +04 },
        {+0, +1, -1, +04 },
        {+0, -2, +0, +02 },
        {-1, -1, +0, +04 },
        {+0, -1, +0, -24 },
        {+1, -1, +0, +04 },
        {-2, +0, +0, +02 },
        {-1, +0, +0, -24 },
        {+0, +0, +0, +84 },
        {+1, +0, +0, -24 },
        {+2, +0, +0, +02 },
        {-1, +1, +0, +04 },
        {+0, +1, +0, -24 },
        {+1, +1, +0, +04 },
        {+0, +2, +0, +02 },
        {+0, -1, +1, +04 },
        {-1, +0, +1, +04 },
        {+0, +0, +1, -24 },
        {+1, +0, +1, +04 },
        {+0, +1, +1, +04 },
        {+0, +0, +2, +02 }
    };



    /***********************PUBLIC METHODS*****************************************/

    ReservoirCellMeshOptimizationEbE::ReservoirCellMeshOptimizationEbE( )
    {
        _eps = 1e-1;
        _iterations = 0;
        _maxSubIterations = 5000;
        _maxIterations = 50;
        _showLog = true;
        _stepsNumber = 1;
        _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    }



    void ReservoirCellMeshOptimizationEbE::computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                                                     const std::vector<bool>& activeCells,
                                                                     const unsigned int fixedBoundaryCells )
    {
        if (fixedBoundaryCells < 1)
        {
            if (_showLog)
            {
                printf( "\n    Error: the number of fixed boundary cells must be greater then 0!\n" );
            }
            return;
        }

        if (_showLog)
        {
            printf( "\n    Pre-processing active cells and fixed boundary points...\n" );
            printf( "\n    Computing re-index vector...\n" );
        }

        Timer t;

        //Save the number of fixed boundary cells.
        _fixedBoundaryCells = fixedBoundaryCells;

        //Get the grid size.
        const unsigned int gridSize = grid.Size( );

        //Vector to map between point on grid to variable space.
        std::vector<int> reindexPoints;

        //Compute the fixed points.
        const unsigned int fixedPoints = preprocessFixedPoints( grid, activeCells, reindexPoints );

        //Compute the number of unknown points.
        const unsigned int numberUnknowPoints = gridSize - fixedPoints;

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    %u points was fixed on preprocessing step...\n", fixedPoints );
            printf( "    %u points have unknown values...\n", numberUnknowPoints );
            printf( "    %u points have unknown values...\n", numberUnknowPoints );
        }

        t.restart( );

        //Allocate auxiliary vectors.
        _r.resize( numberUnknowPoints, 0 );
        _p.resize( numberUnknowPoints, 0 );
        _q.resize( numberUnknowPoints, 0 );
        _sum.resize( _numberOfThreads );

        if (_showLog)
        {
            t.printTime( "        Total time" );
            double space = sizeof (unsigned int ) * gridSize + sizeof (FPType ) * ( gridSize + 3 * numberUnknowPoints );
            space /= 1048576.0;

            printf( "\n   Necessary GPU space: %.2lf MB\n", space );
            printf( "\n    **************************OPTIMIZING**************************\n\n" );
            printf( "\n    Optimizing the model with %u variables...\n\n", 3 * numberUnknowPoints );
        }

        //Optimize dimension by dimension.
        for (unsigned int dimension = 0; dimension < 3; dimension++)
        {
            t.restart( );
            if (_showLog)
            {
                printf( "        DIMENSION %u:", dimension + 1 );
                printf( "\n\n            ITERATION           GRADIENT NORM           TIME\n" );
            }

            //Compute the vectors values for and specific dimension.
            computeBVector( dimension, grid, reindexPoints );

            //Compute gradient. This is equivalent to compute the initial residual.
            FPType gnorm = computeInitialResidual( grid, dimension, reindexPoints, _r );

            _iterations = 0;
            while (_iterations++ < _maxIterations && gnorm > _eps * _eps)
            {
                if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
                {
                    printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                    t.printTime( "" );
                }

                //Solve linear system.
                gnorm = PCG( grid, dimension, reindexPoints, gnorm );

                //Compute the vectors values for and specific dimension.
                computeBVector( dimension, grid, reindexPoints );
            }

            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                t.printTime( "" );
            }
        }

        //Free memory.
        _r.clear( );
        _p.clear( );
        _q.clear( );
        _sum.clear( );
    }



    FPType ReservoirCellMeshOptimizationEbE::getEPS( ) const
    {
        return _eps;
    }



    unsigned int ReservoirCellMeshOptimizationEbE::getMaxSubIterations( ) const
    {
        return _maxSubIterations;
    }



    unsigned int ReservoirCellMeshOptimizationEbE::getMaxIterations( ) const
    {
        return _maxIterations;
    }



    unsigned int ReservoirCellMeshOptimizationEbE::getNumberIterations( ) const
    {
        return _iterations;
    }



    unsigned int ReservoirCellMeshOptimizationEbE::getNumberfOfThreads( ) const
    {
        return _numberOfThreads;
    }



    unsigned int ReservoirCellMeshOptimizationEbE::getStepsNumber( ) const
    {
        return _stepsNumber;
    }



    ReservoirCellMeshOptimizationEbE::~ReservoirCellMeshOptimizationEbE( )
    {

    }



    void ReservoirCellMeshOptimizationEbE::setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) )
    {
        _displayInformation = displayInformation;
    }



    void ReservoirCellMeshOptimizationEbE::setEPS( const FPType eps )
    {
        _eps = eps;
    }



    void ReservoirCellMeshOptimizationEbE::setMaxSubIterations( const unsigned int maxSubIter )
    {
        _maxSubIterations = maxSubIter;
    }



    void ReservoirCellMeshOptimizationEbE::setMaxIterations( const unsigned int maxIter )
    {
        _maxIterations = maxIter;
    }



    void ReservoirCellMeshOptimizationEbE::setNumberOfThreads( const unsigned int numThreads )
    {
        _numberOfThreads = numThreads;
    }



    void ReservoirCellMeshOptimizationEbE::showLog( const bool log )
    {
        _showLog = log;
    }



    void ReservoirCellMeshOptimizationEbE::setStepsNumber( const unsigned int sn )
    {
        _stepsNumber = sn;
    }



    /***********************PRIVATE METHODS****************************************/
    void ReservoirCellMeshOptimizationEbE::computeReindexPointsVector( const std::vector<bool> &activePoints,
                                                                       std::vector<int>& reindexPoints ) const
    {
        //Allocate storage.
        reindexPoints.resize( activePoints.size( ), -1 );

        //Compute new indexes.
        for (unsigned int i = 0, index = 0; i < activePoints.size( ); i++)
        {
            if (!activePoints[i])
            {
                reindexPoints[i] = index;
                index++;
            }
            else
            {
                reindexPoints[i] = -1;
            }
        }
    }



    void ReservoirCellMeshOptimizationEbE::computeBVector( const unsigned int dimension,
                                                           const DsGrid3D<Point3D>& grid,
                                                           const std::vector<int>& reindexPoints )
    {
        //Get the number of variables to be computed.
        unsigned int unknownVariables = _p.size( );

        //Initialize vectors.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownVariables; i++)
        {
            _p[i] = 0.0;
        }

        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Get limits.
        const int lk = nk - _fixedBoundaryCells;
        const int lj = nj - _fixedBoundaryCells;
        const int li = ni - _fixedBoundaryCells;

        //Compute the b vector.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < lk; k++)
        {
            for (int j = _fixedBoundaryCells; j < lj; j++)
            {
                for (int i = _fixedBoundaryCells; i < li; i++)
                {
                    //Get the current point index on grid
                    int gidx = grid.GetIndex( i, j, k );

                    if (reindexPoints[gidx] != -1)
                    {
                        //Get the variable index.
                        int vidx = reindexPoints[gidx];

                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx2 = grid.GetIndex( di, dj, dk );
                                if (reindexPoints[gidx2] == -1)
                                {
                                    //Get the variable index.
                                    _p[vidx] -= neighbouhood[n][3] * grid.Get( gidx2 ).x[dimension];
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    unsigned int ReservoirCellMeshOptimizationEbE::preprocessFixedPoints( const DsGrid3D<Point3D>& grid,
                                                                          const std::vector<bool>& activeCells,
                                                                          std::vector<int>& reindexPoints ) const
    {
        //Vector with active points.
        std::vector<bool> activePoints( grid.Size( ), false );

        //Get grid dimensions.
        int nk = grid.Nk( );
        int nj = grid.Nj( );
        int ni = grid.Ni( );
        int celIndex = 0;

        //Fix points on active cells.
        for (int k = 0; k < nk - 1; k++)
        {
            for (int j = 0; j < nj - 1; j++)
            {
                for (int i = 0; i < ni - 1; i++)
                {
                    if (activeCells[celIndex])
                    {
                        //Set all 8 points on active cells as active points.
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 1 )] = true;
                    }
                    celIndex++;
                }
            }
        }

        //Fix points on boundary cells.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = 0; k < nk; k++)
        {
            for (int j = 0; j < nj; j++)
            {
                for (int i = 0; i < ni; i++)
                {
                    if (i > ( int ) _fixedBoundaryCells && i < ( int ) ni - 1 - ( int ) _fixedBoundaryCells &&
                        j > ( int ) _fixedBoundaryCells && j < ( int ) nj - 1 - ( int ) _fixedBoundaryCells &&
                        k > ( int ) _fixedBoundaryCells && k < ( int ) nk - 1 - ( int ) _fixedBoundaryCells)
                    {
                        continue;
                    }
                    activePoints[grid.GetIndex( i, j, k )] = true;
                }
            }
        }

        //Get the number of fixed points.
        std::vector<unsigned int> sum( _numberOfThreads, 0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < activePoints.size( ); i++)
        {
            if (activePoints[i])
            {
                int thread = omp_get_thread_num( );
                sum[thread]++;
            }
        }

        unsigned totalFixedPoints = 0;
        for (unsigned int i = 0; i < sum.size( ); i++)
        {
            totalFixedPoints += sum[i];
        }

        //From active points, compute the re-index vector.
        computeReindexPointsVector( activePoints, reindexPoints );

        return totalFixedPoints;
    }



    /***********************PCG ALG. METHODS***************************************/
    FPType ReservoirCellMeshOptimizationEbE::computeInitialResidual( const DsGrid3D<Point3D>& grid,
                                                                     const unsigned int dimension,
                                                                     const std::vector<int>& reindexPoints,
                                                                     std::vector<FPType>& r )
    {
        //Get the number of variables.
        unsigned int unknownPoints = r.size( );

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownPoints; i++)
        {
            r[i] = 0.0;
        }

        //Multiply matrix.
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Get limits.
        const int lk = nk - _fixedBoundaryCells;
        const int lj = nj - _fixedBoundaryCells;
        const int li = ni - _fixedBoundaryCells;


        //Multiply Ax
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < lk; k++)
        {
            for (int j = _fixedBoundaryCells; j < lj; j++)
            {
                for (int i = _fixedBoundaryCells; i < li; i++)
                {
                    //Get the current point index on grid
                    int currentPoint = grid.GetIndex( i, j, k );
                    if (reindexPoints[currentPoint] != -1)
                    {
                        //Get variable index.
                        currentPoint = reindexPoints[currentPoint];

                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (reindexPoints[gidx] != -1)
                                {
                                    r[currentPoint] += neighbouhood[n][3] * grid.GetRef( gidx ).x[dimension];
                                }
                            }
                        }
                    }
                }
            }
        }

        //Compute residual vector: b - Ax.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownPoints; i++)
        {
            r[i] = _p[i] - r[i];

            int thread = omp_get_thread_num( );
            _sum[thread] += r[i] * r[i];
        }

        FPType rnorm2 = 0.0;
        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            rnorm2 += _sum[i];
            _sum[i] = 0.0;
        }

        return rnorm2;
    }



    void ReservoirCellMeshOptimizationEbE::matrixVec( const DsGrid3D<Point3D>& grid,
                                                      const unsigned int dimension,
                                                      const std::vector<int>& reindexPoints,
                                                      const std::vector<FPType>& p,
                                                      std::vector<FPType>& q )
    {
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Get limits.
        const int lk = nk - _fixedBoundaryCells;
        const int lj = nj - _fixedBoundaryCells;
        const int li = ni - _fixedBoundaryCells;

        //Get the number of unknown points.
        const unsigned int unknownPoints = q.size( );

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownPoints; i++)
        {
            q[i] = 0.0;
        }

        //Multiply Ap.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < lk; k++)
        {
            for (int j = _fixedBoundaryCells; j < lj; j++)
            {
                for (int i = _fixedBoundaryCells; i < li; i++)
                {
                    //Get the current point index on grid
                    int currentPoint = grid.GetIndex( i, j, k );
                    if (reindexPoints[currentPoint] != -1)
                    {
                        //Get variable index.
                        currentPoint = reindexPoints[currentPoint];

                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (reindexPoints[gidx] != -1)
                                {
                                    const int vidx = reindexPoints[gidx];
                                    q[currentPoint] += neighbouhood[n][3] * p[vidx];
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    FPType ReservoirCellMeshOptimizationEbE::PCG( const DsGrid3D<Point3D>& grid,
                                                  const unsigned int dimension,
                                                  const std::vector<int>& reindexPoints,
                                                  FPType rnorm2 )
    {
        //Get the number of variables.
        unsigned int n = _r.size( );

        //        FPType rnorm2 = computeInitialResidual( grid, dimension, reindexPoints, _r );

        //Allocate auxiliary vectors.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < n; i++)
        {
            _p[i] = 0.0;
            _q[i] = 0.0;
        }

        //Compute initial residual.
        FPType rho = 0.0, rhoOld = 1.0;

        //Iterate.
        for (unsigned int iter = 0; iter < _maxSubIterations; iter++)
        {
            //For this algorithm rho is equal to <r, r>/84.
            rho = rnorm2;

            //Compute p.
            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                _p[i] = ( rhoOld * _r[i] + rho * _p[i] ) / ( 84.0 * rhoOld );
            }

            //q = A*p and gradient = ||Ax - b||
            matrixVec( grid, dimension, reindexPoints, _p, _q );

            //<p, q>
            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                int thread = omp_get_thread_num( );
                _sum[thread] += _p[i] * _q[i];
            }

            FPType dotPQ = 0;
            for (unsigned int i = 0; i < _numberOfThreads; i++)
            {
                dotPQ += _sum[i];
                _sum[i] = 0.0;
            }

            //Compute x(k) = x(k-1) + alpha * p(k)
            //x[i] = x[i] + rho * p[i] / ( 84.0 * dotPQ );
            updateSolution( grid, dimension, reindexPoints, _p, rho, dotPQ );

            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int i = 0; i < n; i++)
            {
                _r[i] = _r[i] - rho * _q[i] / ( 84.0 * dotPQ );

                int thread = omp_get_thread_num( );
                _sum[thread] += _r[i] * _r[i];
            }

            rnorm2 = 0;
            for (unsigned int i = 0; i < _numberOfThreads; i++)
            {
                rnorm2 += _sum[i];
                _sum[i] = 0.0;
            }

            rhoOld = rho / 84.0;
            if (rnorm2 <= _eps * _eps)
            {
                return rnorm2;
            }
        }
        return rnorm2;
    }



    void ReservoirCellMeshOptimizationEbE::updateSolution( const DsGrid3D<Point3D>& grid,
                                                           const unsigned int dimension,
                                                           const std::vector<int>& reindexPoints,
                                                           const std::vector<FPType>& p,
                                                           const FPType rho,
                                                           const FPType dotPQ ) const
    {
        //Get limits.
        const int lk = grid.Nk( ) - _fixedBoundaryCells;
        const int lj = grid.Nj( ) - _fixedBoundaryCells;
        const int li = grid.Ni( ) - _fixedBoundaryCells;

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = _fixedBoundaryCells; k < lk; k++)
        {
            for (int j = _fixedBoundaryCells; j < lj; j++)
            {
                for (int i = _fixedBoundaryCells; i < li; i++)
                {
                    //Get the current point index on grid
                    int gidx = grid.GetIndex( i, j, k );
                    if (reindexPoints[gidx] != -1)
                    {
                        //Get variable index.
                        const int vidx = reindexPoints[gidx];

                        Point3D &point = grid.GetRef( gidx );
                        FPType &x = point.x[dimension];

                        //x[i] = x[i] + rho * p[i] / ( 84.0 * dotPQ );
                        x = x + rho * p[vidx] / ( 84.0 * dotPQ );
                    }
                }
            }
        }
    }
}