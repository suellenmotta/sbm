/* 
 * File:   ReservoirCellMeshOptimization.cpp
 * Author: jcoelho
 * 
 * Created on July 26, 2016, 1:45 PM
 */

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <omp.h>


#include "../grid3d.h"
#include "lis.h"
#include "../Point3D.h"
#include "ReservoirCellMeshOptimization.h"
#include "../../../../../libs/Timer/Timer.h"

namespace MeshModeling
{

    //Vector to access the neighborhood to set matrix elements.
    const int neighbouhood[25][4] = {
        {+0, +0, -2, +02 },
        {+0, -1, -1, +04 },
        {-1, +0, -1, +04 },
        {+0, +0, -1, -24 },
        {+1, +0, -1, +04 },
        {+0, +1, -1, +04 },
        {+0, -2, +0, +02 },
        {-1, -1, +0, +04 },
        {+0, -1, +0, -24 },
        {+1, -1, +0, +04 },
        {-2, +0, +0, +02 },
        {-1, +0, +0, -24 },
        {+0, +0, +0, +84 },
        {+1, +0, +0, -24 },
        {+2, +0, +0, +02 },
        {-1, +1, +0, +04 },
        {+0, +1, +0, -24 },
        {+1, +1, +0, +04 },
        {+0, +2, +0, +02 },
        {+0, -1, +1, +04 },
        {-1, +0, +1, +04 },
        {+0, +0, +1, -24 },
        {+1, +0, +1, +04 },
        {+0, +1, +1, +04 },
        {+0, +0, +2, +02 }
    };



    /***********************PUBLIC METHODS*****************************************/

    ReservoirCellMeshOptimization::ReservoirCellMeshOptimization( )
    {
        _eps = 1.2e-1;
        _iterations = 0;
        _maxSubIterations = 5000;
        _maxIterations = 50;
        _showLog = true;
        _stepsNumber = 1;
        _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    }



    void ReservoirCellMeshOptimization::computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                                                  const std::vector<bool>& activeCells,
                                                                  const unsigned int fixedBoundaryCells )
    {
        if (_showLog)
        {
            printf( "\n    Pre-processing active cells and fixed boundary points...\n" );
        }
        Timer t;

        //Get the grid size.
        const unsigned int gridSize = grid.Size( );

        //Compute the fixed points.
        std::vector<bool> activePoints( gridSize, false );
        const unsigned int fixedPoints = preprocessFixedPoints( grid, activeCells, fixedBoundaryCells, activePoints );
        const unsigned int numberUnknowPoints = gridSize - fixedPoints;

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    %u points was fixed on preprocessing step...\n", fixedPoints );
            printf( "    %u points have unknown values...\n", numberUnknowPoints );
            printf( "\n    Computing re-index vector...\n" );
        }

        t.restart( );

        //Vector to store each variable index.
        std::vector<unsigned int> reindexPoints;
        computeReindexPointsVector( activePoints, reindexPoints );
        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    Counting nonzero matrix elements...\n" );
        }

        //Initialize the LIS.
        LIS_INT argc = 0;
        char **argv;
        lis_initialize( &argc, &argv );

        //Allocate the memory to store the matrix.
        LIS_INT *ptr, *index;
        LIS_SCALAR *value;
        LIS_MATRIX A;
        LIS_VECTOR b, x;

        t.restart( );
        //Allocate matrix.
        const unsigned int numberNonZero = computeNonZeroMatrixElements( grid, activePoints, fixedBoundaryCells, numberUnknowPoints );

        if (_showLog)
        {
            printf( "\n    The matrix has %u nonzero elements...\n", numberNonZero );
            t.printTime( "        Total time" );
            printf( "\n    Building matrix in CSR format...\n" );
        }

        //Allocate vector to store in CSR format.
        lis_matrix_malloc_csr( numberUnknowPoints, numberNonZero, &ptr, &index, &value );

        //Create the matrix and define the matrix size.
        lis_matrix_create( 0, &A );
        lis_matrix_set_size( A, 0, numberUnknowPoints );

        //Create b vector and define it size.
        lis_vector_create( 0, &b );
        lis_vector_set_size( b, 0, numberUnknowPoints );

        //Create b vector and define it size.
        lis_vector_create( 0, &x );
        lis_vector_set_size( x, 0, numberUnknowPoints );

        t.restart( );
        //Compute index vectors.
        computeIndexVectors( grid, activePoints, reindexPoints,
                             fixedBoundaryCells, numberUnknowPoints,
                             numberNonZero, ptr, index, value );

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    **************************OPTIMIZING**************************\n\n" );
        }

        LIS_SOLVER solver;
        lis_solver_create( &solver );
        char option[2048];

        omp_set_num_threads( _numberOfThreads );

        for (unsigned int dimension = 0; dimension < 3; dimension++)
        {
            t.restart( );
            printf( "        DIMENSION %u", dimension + 1 );

            //Compute the vectors values for and specific dimension.
            computeValuesDimension( dimension, grid, activePoints, reindexPoints,
                                    fixedBoundaryCells, numberUnknowPoints,
                                    ptr, index, value, x, b );

            if (_showLog)
                printf( "\n\n            ITERATION           GRADIENT NORM           TIME\n" );

            //Compute gradient.
            double gnorm = computeGradientNorm2( x, b, ptr, index, value, numberUnknowPoints );

            _iterations = 0;
            while (_iterations++ < _maxIterations && gnorm > _eps * _eps)
            {
                if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
                {
                    printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                    t.printTime( "" );
                }

                //Build the CSR format vectors.
                lis_matrix_set_csr( numberNonZero, ptr, index, value, A );
                lis_matrix_assemble( A );

                sprintf( option, "-i cg -p jacobi -maxiter %u -initx_zeros %d -conv_cond %d"
                         " -tol %e -omp_num_threads %u -print none", _maxSubIterations, 0, 0, 1e-12, _numberOfThreads );

                lis_solver_set_option( option, solver );
                lis_solve( A, b, x, solver );

                //Update store solution.
                updateSolution( grid, x, activePoints, reindexPoints, dimension,
                                numberUnknowPoints, fixedBoundaryCells );

                //Compute gradient.
                gnorm = computeGradientNorm2( x, b, ptr, index, value, numberUnknowPoints );
            }

            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "            %8u            %e            ", _iterations, sqrt( gnorm ) );
                t.printTime( "" );
            }
        }
        lis_solver_destroy( solver );
        lis_vector_destroy( x );
        lis_vector_destroy( b );
        lis_matrix_destroy( A );

        lis_finalize( );
    }



    double ReservoirCellMeshOptimization::getEPS( ) const
    {
        return _eps;
    }



    unsigned int ReservoirCellMeshOptimization::getMaxSubIterations( ) const
    {
        return _maxSubIterations;
    }



    unsigned int ReservoirCellMeshOptimization::getMaxIterations( ) const
    {
        return _maxIterations;
    }



    unsigned int ReservoirCellMeshOptimization::getNumberIterations( ) const
    {
        return _iterations;
    }



    unsigned int ReservoirCellMeshOptimization::getNumberfOfThreads( ) const
    {
        return _numberOfThreads;
    }



    unsigned int ReservoirCellMeshOptimization::getStepsNumber( ) const
    {
        return _stepsNumber;
    }



    ReservoirCellMeshOptimization::~ReservoirCellMeshOptimization( )
    {

    }



    void ReservoirCellMeshOptimization::setDisplayInformationCallback( bool (*displayInformation )( const double gnorm ) )
    {
        _displayInformation = displayInformation;
    }



    void ReservoirCellMeshOptimization::setEPS( const double eps )
    {
        _eps = eps;
    }



    void ReservoirCellMeshOptimization::setMaxSubIterations( const unsigned int maxSubIter )
    {
        _maxSubIterations = maxSubIter;
    }



    void ReservoirCellMeshOptimization::setMaxIterations( const unsigned int maxIter )
    {
        _maxIterations = maxIter;
    }



    void ReservoirCellMeshOptimization::setNumberOfThreads( const unsigned int numThreads )
    {
        _numberOfThreads = numThreads;
    }



    void ReservoirCellMeshOptimization::showLog( const bool log )
    {
        _showLog = log;
    }



    void ReservoirCellMeshOptimization::setStepsNumber( const unsigned int sn )
    {
        _stepsNumber = sn;
    }



    /***********************PRIVATE METHODS****************************************/
    void ReservoirCellMeshOptimization::computeElementsPerLine( const DsGrid3D<Point3D>& grid,
                                                                const std::vector<bool> &activePoints,
                                                                const std::vector<unsigned int>& reindexPoints,
                                                                const unsigned int fixedBoundaryCells,
                                                                LIS_INT *ptr ) const
    {
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        for (int k = ( int ) fixedBoundaryCells; k < nk - ( int ) fixedBoundaryCells; k++)
        {
            for (int j = ( int ) fixedBoundaryCells; j < nj - ( int ) fixedBoundaryCells; j++)
            {
                for (int i = ( int ) fixedBoundaryCells; i < ni - ( int ) fixedBoundaryCells; i++)
                {
                    if (!activePoints[grid.GetIndex( i, j, k )])
                    {
                        for (int n = 0; n < 25; n++)
                        {
                            //Get the neighbor on grid.
                            int di = i + neighbouhood[n][0];
                            int dj = j + neighbouhood[n][1];
                            int dk = k + neighbouhood[n][2];
                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (!activePoints[gidx])
                                {
                                    int vidx = reindexPoints[gidx];
                                    ptr[vidx]++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    double ReservoirCellMeshOptimization::computeGradientNorm2( const LIS_VECTOR x,
                                                                const LIS_VECTOR b,
                                                                const LIS_INT *ptr,
                                                                const LIS_INT *index,
                                                                const LIS_SCALAR* value,
                                                                const unsigned int unknownPoints ) const
    {
        std::vector<double> sum( _numberOfThreads, 0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownPoints; i++)
        {
            double g = 0.0;
            double xx = 0.0;
            for (LIS_INT j = ptr[i]; j < ptr[i + 1]; j++)
            {
                //Get the variable index.
                LIS_INT v = index[j];

                //Get the variable value.
                xx = 0.0;
                lis_vector_get_value( x, v, &xx );

                //Multiply the line by vector.
                g += value[j] * xx;
            }
            xx = 0.0;
            lis_vector_get_value( b, ( LIS_INT ) i, &xx );

            g -= xx;

            int thread = omp_get_thread_num( );
            sum[thread] += g * g;
        }

        double g2 = 0.0;
        for (unsigned int i = 0; i < sum.size( ); i++)
        {
            g2 += sum[i];
        }
        return g2;
    }



    void ReservoirCellMeshOptimization::computeIndexVectors( const DsGrid3D<Point3D>& grid,
                                                             const std::vector<bool> &activePoints,
                                                             const std::vector<unsigned int>& reindexPoints,
                                                             const unsigned int fixedBoundaryCells,
                                                             const unsigned int unknownPoints,
                                                             const unsigned int nonZero,
                                                             LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const
    {
        //Initialize vectors.
        memset( ptr, 0, ( unknownPoints + 1 ) * sizeof (LIS_INT ) );
        memset( index, 0, nonZero * sizeof (LIS_INT ) );

        //Compute the number of elements per line.
        computeElementsPerLine( grid, activePoints, reindexPoints, fixedBoundaryCells, ptr );

        //Compute the nonzero columns.
        computeMatrixColumns( grid, activePoints, reindexPoints,
                              fixedBoundaryCells, unknownPoints,
                              nonZero, ptr, index, value );
    }



    void ReservoirCellMeshOptimization::computeMatrixColumns( const DsGrid3D<Point3D>& grid,
                                                              const std::vector<bool> &activePoints,
                                                              const std::vector<unsigned int>& reindexPoints,
                                                              const unsigned int fixedBoundaryCells,
                                                              const unsigned int unknownPoints,
                                                              const unsigned int nonZero,
                                                              LIS_INT *ptr, LIS_INT *index, LIS_SCALAR* value ) const
    {
        memset( value, 0, nonZero * sizeof (LIS_SCALAR ) );

        //Vector to know about next free position on vectors.
        std::vector<int> freePosition( unknownPoints, 0 );

        //Initialize the last position with the number of nonzero elements on
        //matrix.
        ptr[unknownPoints] = nonZero;

        //Compute free positions and adjust the ptr vector to store where each
        //line start.
        for (int i = ( int ) unknownPoints - 1; i >= 0; i--)
        {
            ptr[i] = ptr[i + 1] - ptr[i];
            freePosition[i] = ptr[i];
        }

        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        for (int k = ( int ) fixedBoundaryCells; k < nk - ( int ) fixedBoundaryCells; k++)
        {
            for (int j = ( int ) fixedBoundaryCells; j < nj - ( int ) fixedBoundaryCells; j++)
            {
                for (int i = ( int ) fixedBoundaryCells; i < ni - ( int ) fixedBoundaryCells; i++)
                {
                    //Get the current point index on grid
                    int currentPoint = grid.GetIndex( i, j, k );
                    if (!activePoints[currentPoint])
                    {
                        currentPoint = reindexPoints[currentPoint];
                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx = grid.GetIndex( di, dj, dk );
                                if (!activePoints[gidx])
                                {
                                    int vidx = reindexPoints[gidx];
                                    unsigned int f = freePosition[currentPoint];

                                    index[f] = vidx;
                                    value[f] = neighbouhood[n][3];
                                    freePosition[currentPoint]++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    unsigned int ReservoirCellMeshOptimization::computeNonZeroMatrixElements( const DsGrid3D<Point3D>& grid,
                                                                              const std::vector<bool>& activePoints,
                                                                              const unsigned int fixedBoundaryCells,
                                                                              const unsigned int unknownPoints ) const
    {
        const unsigned int nk = grid.Nk( );
        const unsigned int nj = grid.Nj( );
        const unsigned int ni = grid.Ni( );
        unsigned int nonzero = 0;

        //Compute the number of unknown points.
        for (unsigned int k = fixedBoundaryCells; k < nk - fixedBoundaryCells; k++)
        {
            for (unsigned int j = fixedBoundaryCells; j < nj - fixedBoundaryCells; j++)
            {
                for (unsigned int i = fixedBoundaryCells; i < ni - fixedBoundaryCells; i++)
                {
                    int neighbours = 0, bumpX = 0, bumpY = 0, bumpZ = 0;
                    for (int m = -1; m <= 1; m += 2)
                    {
                        if (!activePoints[grid.GetIndex( i + m, j + 0, k + 0 )])
                        {
                            neighbours++;
                            bumpX++;
                        }
                        if (!activePoints[grid.GetIndex( i + 0, j + m, k + 0 )])
                        {
                            neighbours++;
                            bumpY++;
                        }
                        if (!activePoints[grid.GetIndex( i + 0, j + 0, k + m )])
                        {
                            neighbours++;
                            bumpZ++;
                        }
                    }

                    if (!activePoints[grid.GetIndex( i, j, k )])
                    {
                        neighbours++;
                    }

                    //Count the number of crossing products.
                    nonzero += neighbours * ( neighbours - 1 ) / 2;

                    //Correct the crossing products that are counted once.
                    if (bumpX == 2) nonzero++;
                    if (bumpY == 2) nonzero++;
                    if (bumpZ == 2) nonzero++;
                }
            }
        }

        //Count the diagonal variables.
        nonzero += unknownPoints;

        return nonzero;
    }



    void ReservoirCellMeshOptimization::computeReindexPointsVector( const std::vector<bool> &activePoints,
                                                                    std::vector<unsigned int>& reindexPoints ) const
    {
        //Allocate storage.
        reindexPoints.resize( activePoints.size( ), -1 );

        //Compute new indexes.
        for (unsigned int i = 0, index = 0; i < activePoints.size( ); i++)
        {
            if (!activePoints[i])
            {
                reindexPoints[i] = index;
                index++;
            }
            else
            {
                reindexPoints[i] = -1;
            }
        }
    }



    void ReservoirCellMeshOptimization::computeValuesDimension( const unsigned int dimension,
                                                                const DsGrid3D<Point3D>& grid,
                                                                const std::vector<bool> &activePoints,
                                                                const std::vector<unsigned int>& reindexPoints,
                                                                const unsigned int fixedBoundaryCells,
                                                                const unsigned int unknownPoints,
                                                                const LIS_INT *ptr,
                                                                const LIS_INT *index,
                                                                const LIS_SCALAR* value,
                                                                LIS_VECTOR x,
                                                                LIS_VECTOR b ) const
    {
        //Initialize vectors.
        initializeVectors( unknownPoints, x, b );

        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = ( int ) fixedBoundaryCells; k < nk - ( int ) fixedBoundaryCells; k++)
        {
            for (int j = ( int ) fixedBoundaryCells; j < nj - ( int ) fixedBoundaryCells; j++)
            {
                for (int i = ( int ) fixedBoundaryCells; i < ni - ( int ) fixedBoundaryCells; i++)
                {
                    //Get the current point index on grid
                    int gidx = grid.GetIndex( i, j, k );

                    if (!activePoints[gidx])
                    {
                        //Get the variable index.
                        int vidx = reindexPoints[gidx];

                        //Set initial solution.
                        lis_vector_set_value( LIS_INS_VALUE, vidx, grid.Get( i, j, k ).x[dimension], x );

                        for (int n = 0; n < 25; n++)
                        {
                            int vi = neighbouhood[n][0];
                            int vj = neighbouhood[n][1];
                            int vk = neighbouhood[n][2];

                            //Get the neighbor on grid.
                            int di = i + vi;
                            int dj = j + vj;
                            int dk = k + vk;

                            if (0 <= di && di < ni &&
                                0 <= dj && dj < nj &&
                                0 <= dk && dk < nk)
                            {
                                int gidx2 = grid.GetIndex( di, dj, dk );
                                if (activePoints[gidx2])
                                {
                                    //Get the variable index.
                                    double val = -neighbouhood[n][3] * grid.Get( gidx2 ).x[dimension];
                                    lis_vector_set_value( LIS_ADD_VALUE, vidx, val, b );
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    void ReservoirCellMeshOptimization::initializeVectors( const unsigned int unknownPoints,
                                                           LIS_VECTOR x,
                                                           LIS_VECTOR b ) const
    {
        //Initialize x and b vectors.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < unknownPoints; i++)
        {
            lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) i, 0.0, x );
            lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) i, 0.0, b );
        }
    }



    unsigned int ReservoirCellMeshOptimization::preprocessFixedPoints( const DsGrid3D<Point3D>& grid,
                                                                       const std::vector<bool>& activeCells,
                                                                       const unsigned int fixedBoundaryCells,
                                                                       std::vector<bool>& activePoints ) const
    {
        int nk = grid.Nk( );
        int nj = grid.Nj( );
        int ni = grid.Ni( );
        int celIndex = 0;

        //Fix points on active cells.
        for (int k = 0; k < nk - 1; k++)
        {
            for (int j = 0; j < nj - 1; j++)
            {
                for (int i = 0; i < ni - 1; i++)
                {
                    if (activeCells[celIndex])
                    {
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 0, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 0 )] = true;
                        activePoints[grid.GetIndex( i + 1, j + 1, k + 1 )] = true;
                        activePoints[grid.GetIndex( i + 0, j + 1, k + 1 )] = true;
                    }
                    celIndex++;
                }
            }
        }

        //Fix points on boundary cells.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = 0; k < nk; k++)
        {
            for (int j = 0; j < nj; j++)
            {
                for (int i = 0; i < ni; i++)
                {
                    if (i > ( int ) fixedBoundaryCells && i < ( int ) ni - 1 - ( int ) fixedBoundaryCells &&
                        j > ( int ) fixedBoundaryCells && j < ( int ) nj - 1 - ( int ) fixedBoundaryCells &&
                        k > ( int ) fixedBoundaryCells && k < ( int ) nk - 1 - ( int ) fixedBoundaryCells)
                    {
                        continue;
                    }
                    activePoints[grid.GetIndex( i, j, k )] = true;
                }
            }
        }

        //Get the number of fixed points.
        std::vector<unsigned int> sum( _numberOfThreads, 0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < activePoints.size( ); i++)
        {
            if (activePoints[i])
            {
                int thread = omp_get_thread_num( );
                sum[thread]++;
            }
        }

        unsigned totalFixedPoints = 0;
        for (unsigned int i = 0; i < sum.size( ); i++)
        {
            totalFixedPoints += sum[i];
        }
        return totalFixedPoints;
    }



    void ReservoirCellMeshOptimization::updateSolution( DsGrid3D<Point3D>& grid,
                                                        const LIS_VECTOR x,
                                                        std::vector<bool>& activePoints,
                                                        const std::vector<unsigned int>& reindexPoints,
                                                        const unsigned int dimension,
                                                        const unsigned int unknownPoints,
                                                        const unsigned int fixedBoundaryCells ) const
    {
        //Get grid dimensions.
        const int nk = grid.Nk( );
        const int nj = grid.Nj( );
        const int ni = grid.Ni( );

        //Compute the number of unknown variables per line.
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (int k = ( int ) fixedBoundaryCells; k < nk - ( int ) fixedBoundaryCells; k++)
        {
            for (int j = ( int ) fixedBoundaryCells; j < nj - ( int ) fixedBoundaryCells; j++)
            {
                for (int i = ( int ) fixedBoundaryCells; i < ni - ( int ) fixedBoundaryCells; i++)
                {
                    //Get the grid index.
                    int gidx = grid.GetIndex( i, j, k );

                    if (!activePoints[gidx])
                    {
                        //Get the variable index.
                        int vidx = reindexPoints[gidx];

                        //Get current solution.
                        double val = 0.0;
                        lis_vector_get_value( x, ( LIS_INT ) vidx, &val );

                        //Update solution.
                        grid.GetRef( gidx ).x[dimension] = val;
                    }
                }
            }
        }
    }
}