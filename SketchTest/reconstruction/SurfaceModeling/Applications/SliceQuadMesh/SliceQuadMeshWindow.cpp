/* 
 * File:   IupGLCanvasDummy.cpp
 * Author: jeferson
 * 
 * Created on August 31, 2014, 9:28 AM
 */

//#include <GL/glew.h>
//#include <GL/glew.h>

#include <cstdlib>
#include <cstdio>
#include <GL/gl.h>
#include <GL/glu.h>
#include <iup/iup.h>
#include <iup/iupgl.h>
#include <omp.h>
#include "SliceQuadMeshWindow.h"
#include <fstream>
#include <cmath>
#include <cstring>

#include "../../ModelManager/OptimizationModel.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientSLUOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/GradientDescentOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"

Ihandle* canvas = 0;
using namespace Solver;
using namespace ModelManager;
using namespace std;



SliceQuadMeshWindow::SliceQuadMeshWindow( )
{
    //Allocate _solver and model.
    _solver = 0;
    _m = 0;
    _mousePressed = 1;
    _continueSolver = false;

    _pMin.x = -1;
    _pMax.x = +1;
    _pMin.y = -1;
    _pMax.y = +1;

    //Cria janela e define suas configuracoes.
    createWindow( );

    initialize( );
}



Ihandle* SliceQuadMeshWindow::createMenus( )
{
    //Create an item to open a mesh.
    Ihandle *openImages = IupItem( "&Open Mesh", NULL );

    //Create an item to close the window.
    Ihandle *exitWindow = IupItem( "&Exit", NULL );

    //Create an item to save a print screen image.
    Ihandle *savePrint = IupItem( "Salvar &Print", NULL );

    //Create the file menu.
    Ihandle *fileMenu = IupMenu( openImages, savePrint, IupSeparator( ), exitWindow, NULL );

    //Create a submenu for menu file.
    Ihandle *fileSubMenu = IupSubmenu( "Arquivo", fileMenu );

    //Create an item to set a color for known points.
    Ihandle *knownPoints = IupItem( "Known Points Color", NULL );

    //Create an item to set a color for border constraints.
    Ihandle *aspectRatio = IupItem( "Border Constraints Color", NULL );

    //Create an edit menu.
    Ihandle *editMenu = IupMenu( knownPoints, aspectRatio, NULL );

    //Create an edit submenu.
    Ihandle *editSubMenu = IupSubmenu( "Edit", editMenu );


    //Cria menu princial.
    Ihandle *mainMenu = IupMenu( fileSubMenu, editSubMenu, NULL );

    //Create the main menu.
    IupSetAttribute( openImages, IUP_IMAGE, "IUP_FileOpen" );
    IupSetAttribute( savePrint, IUP_IMAGE, "IUP_FileSave" );
    IupSetAttribute( knownPoints, IUP_IMAGE, "IUP_ToolsColor" );
    IupSetAttribute( aspectRatio, IUP_VALUE, IUP_OFF );

    //Define callbacks do menu.
    IupSetCallback( openImages, IUP_ACTION, ( Icallback ) openMeshMenuCallback );

    //Define callback do botao sair.
    IupSetCallback( exitWindow, IUP_ACTION, ( Icallback ) exitButtonCallback );

    //Retorna menu principal.
    return mainMenu;
}



void SliceQuadMeshWindow::createWindow( )
{
    //Cria botao de sair.
    Ihandle *exitButton = IupButton( "Sair", NULL );

    //Cria botao de run.
    Ihandle *runButton = IupButton( "Run", NULL );

    //Cria botao de stop.
    Ihandle *stopButton = IupButton( "Stop", NULL );


    //Cria output label da iteração corrente.
    Ihandle *curInterationLabel = IupLabel( "Current Iteration:" );

    //Cria gradient norm label.
    Ihandle *gNormLabel = IupLabel( "Gradient Norm:" );

    //Cria eps label .
    Ihandle *epsLabel = IupLabel( "EPS: " );

    //Cria max label .
    Ihandle *maxLabel = IupLabel( "Max Iterations: " );

    //Cria method label .
    Ihandle *methodLabel = IupLabel( "Solver: " );


    //Cria eps norm text field.
    Ihandle *epsText = IupText( "  " );

    //Cria max text field.
    Ihandle *maxText = IupText( "  " );


    //Cria method list.
    Ihandle* solverList = IupList( "Select" );

    Ihandle* initList = IupList( "Select" );


    //Cria canvas.
    canvas = IupGLCanvas( NULL );


    //Cria composicao final.
    Ihandle *vboxOutput = IupVbox( curInterationLabel, gNormLabel, NULL );

    //Cria composicao dos botoes de controle (run e stop)
    Ihandle *hboxCtrlButtons = IupHbox( runButton, stopButton, NULL );

    //Cria composicao botões.
    Ihandle *vboxButton = IupVbox( exitButton, NULL );

    //Cria composicao parte inferior.
    Ihandle *hboxFotter = IupHbox( vboxOutput, IupFill( ), vboxButton, NULL );

    //Cria composição dos inputs
    Ihandle *hboxInputs = IupHbox( IupVbox( methodLabel, solverList, NULL ),
                                   IupVbox( maxLabel, maxText, NULL ),
                                   IupVbox( epsLabel, epsText, NULL ),
                                   IupVbox( IupLabel( "Solucao Inicial:" ), initList, NULL ), NULL );

    //Cria composicao parte superior.
    Ihandle *vboxTop = IupVbox( hboxInputs, hboxCtrlButtons, NULL );

    //Cria composicao final.
    Ihandle *vboxFinal = IupVbox( vboxTop, canvas, hboxFotter, NULL );

    //Cria dialogo.
    _dialog = IupDialog( vboxFinal );

    //Cria conjunto de menus.
    Ihandle *mainMenu = createMenus( );

    //Define handles dos elementos dos menus.
    IupSetHandle( "menuBarWindow", mainMenu );

    //Define os atributos dos botoes
    IupSetAttribute( exitButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( exitButton, IUP_TIP, "Fecha a janela." );
    IupSetAttribute( runButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( runButton, IUP_TIP, "Comeca otimizacao." );
    IupSetAttribute( stopButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( stopButton, IUP_TIP, "Para otmizacao." );

    //Define da list
    IupSetAttributes( solverList, "1=\"LBFGS\", 2=\"CG LIS\", 3=\"CG. SLU\", 4=\"G. Descent\", DROPDOWN=YES" );
    IupSetAttribute( solverList, IUP_VALUE, "1" );
    IupSetAttributes( initList, "1=\"Zero\", 2=\"Corrente\",3=\"Entrada\", DROPDOWN=YES" );
    IupSetAttribute( initList, IUP_VALUE, "1" );

    //Define os atributos do canvas.
    IupSetAttribute( canvas, IUP_RASTERSIZE, "600x300" );
    IupSetAttribute( canvas, IUP_BUFFER, IUP_DOUBLE );
    IupSetAttribute( canvas, IUP_EXPAND, IUP_YES );

    IupSetAttribute( hboxInputs, IUP_GAP, "20" );
    IupSetAttribute( hboxCtrlButtons, IUP_GAP, "20" );
    IupSetAttribute( vboxFinal, IUP_MARGIN, "10x5" );

    IupSetAttribute( epsText, IUP_MASK, IUP_MASK_EFLOAT );
    IupSetAttribute( maxText, IUP_MASK, IUP_MASK_UINT );
    IupSetAttribute( epsText, IUP_VALUE, "1e-1" );
    IupSetAttribute( maxText, IUP_VALUE, "5000" );


    //Define propriedades do dialogo.
    IupSetAttribute( _dialog, IUP_TITLE, "Slice Quad Mesh Window" );
    IupSetAttribute( _dialog, "THIS", ( char* ) this );
    IupSetAttribute( _dialog, "CANVAS", ( char* ) canvas );
    IupSetAttribute( _dialog, "SOLVER_LIST", ( char* ) solverList );
    IupSetAttribute( _dialog, "INIT_LIST", ( char* ) initList );
    IupSetAttribute( _dialog, "EPS_TEXT", ( char* ) epsText );
    IupSetAttribute( _dialog, "MAX_TEXT", ( char* ) maxText );
    IupSetAttribute( _dialog, "STOP_BTN", ( char* ) stopButton );
    IupSetAttribute( _dialog, "RUN_BTN", ( char* ) runButton );
    IupSetAttribute( _dialog, "GNORM_LABEL", ( char* ) gNormLabel );
    IupSetAttribute( _dialog, "ITERATION_LABEL", ( char* ) curInterationLabel );
    IupSetAttribute( _dialog, IUP_MENU, "menuBarWindow" );

    //Define callbacks do botao.
    IupSetCallback( _dialog, IUP_CLOSE_CB, ( Icallback ) exitButtonCallback );
    IupSetCallback( exitButton, IUP_ACTION, ( Icallback ) exitButtonCallback );
    IupSetCallback( runButton, IUP_ACTION, ( Icallback ) runButtonCallback );
    IupSetCallback( stopButton, IUP_ACTION, ( Icallback ) stopButtonCallback );

    //Define as callbacks do canvas.
    IupSetCallback( canvas, IUP_ACTION, ( Icallback ) actionCanvasCallback );
    IupSetCallback( initList, IUP_ACTION, ( Icallback ) actionInitListCallback );
    IupSetCallback( solverList, IUP_ACTION, ( Icallback ) actionSolverListCallback );
    IupSetCallback( canvas, IUP_RESIZE_CB, ( Icallback ) resizeCanvasCallback );
    IupSetCallback( canvas, IUP_BUTTON_CB, ( Icallback ) buttonCanvasCallback );
    IupSetCallback( canvas, IUP_MOTION_CB, ( Icallback ) motionCanvasCallback );
    IupSetCallback( canvas, IUP_WHEEL_CB, ( Icallback ) wheelCanvasCallback );

    //Desabilta campos inicialmente
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );



    //Mapeia o dialogo.
    IupMap( _dialog );

    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );
}



SliceQuadMeshWindow::~SliceQuadMeshWindow( )
{
    delete _solver;
    delete _m;
    IupDestroy( _dialog );
}



void SliceQuadMeshWindow::show( )
{
    IupShow( _dialog );
}



void SliceQuadMeshWindow::hide( )
{
    IupHide( _dialog );
}



void SliceQuadMeshWindow::initialize( )
{
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glPointSize( 4.5 );

    //Allocate solver.
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );

    int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
    allocateSolver( solverIndex );

    //Set the frequency that the solver must let the window knows about the
    //current solution.
//    _m->setStepsNumber( 10 );
}



unsigned int SliceQuadMeshWindow::computeVertexIndex( unsigned int i, unsigned int j )
{
    return i * _columns + j;
}



void SliceQuadMeshWindow::openMesh( const std::string& path, unsigned int initIndex )
{
    //Read the file.
    readFile( path );

    //Contruct the model.
    buildModel( );

    //Initialize solution.
    initializeSolution( initIndex );

    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    int den = std::min( viewport[2], viewport[3] );

    double widthFactor = ( double ) viewport[2] / den;
    double heightFactor = ( double ) viewport[3] / den;

    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( _pMin.x - _pMax.x );
    double heightWorld = fabs( _pMin.y - _pMax.y );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( _pMin.x + _pMax.x ) / 2.0;
    double cy = ( _pMin.y + _pMax.y ) / 2.0;

    //calcula o novo sistema de coordenadas
    _pMin.x = cx - 0.5 * widthWorld;
    _pMax.x = cx + 0.5 * widthWorld;
    _pMin.y = cy - 0.5 * heightWorld;
    _pMax.y = cy + 0.5 * heightWorld;

    //Define sistema de coordenadas.
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluOrtho2D( _pMin.x, _pMax.x, _pMin.y, _pMax.y );
}



void SliceQuadMeshWindow::readFile( string fileName )
{
    ifstream in( fileName );
    if (in.fail( ))
    {
        IupMessage( "Erro", "Erro ao abrir o arquivo!" );
        return;
    }
    double trash;

    //Read the number of lines and columns.
    in >> _columns >> _lines >> trash;

    //Allocate memory.
    _entryPoints.clear( );
    _knownPoints.clear( );
    _coordinates.clear( );
    _entryPoints.resize( _lines * _columns );
    _coordinates.reserve( 2 * _lines * _columns );

    _pMin.x = _pMin.y = +1E100;
    _pMax.x = _pMax.y = -1E100;

    //Read the mesh.
    for (unsigned int i = 0; i < _lines; i++)
    {
        for (unsigned int j = 0; j < _columns; j++)
        {
            unsigned int idx = computeVertexIndex( i, j );
            bool known;
            in >> _entryPoints[idx].x >> _entryPoints[idx].y >> trash >> known;

            if (known)
            {
                _knownPoints.push_back( idx );
            }

            _coordinates.push_back( _entryPoints[idx].x );
            _coordinates.push_back( _entryPoints[idx].y );

            _pMin.x = std::min( _pMin.x, _entryPoints[idx].x );
            _pMin.y = std::min( _pMin.y, _entryPoints[idx].y );
            _pMax.x = std::max( _pMax.x, _entryPoints[idx].x );
            _pMax.y = std::max( _pMax.y, _entryPoints[idx].y );
        }
    }

    //Compute the scale.
    _scale = fabs( _pMin.x - _pMax.x ) / fabs( _pMin.y - _pMax.y );

    //Alocatte memory to storage the border points.
    _borderPoints.clear( );
    _borderPoints.reserve( 4 * ( _lines + _columns ) - 16 );

    //Compute the border vertices.
    for (int i = 0; i < ( int ) _lines; i++)
    {
        for (int j = 0; j < ( int ) _columns; j++)
        {
            if (( i > 1 ) && ( i < ( int ) _lines - 2 ) & ( j > 1 ) && ( j < ( int ) _columns - 2 ))
            {
                continue;
            }
            _borderPoints.push_back( computeVertexIndex( i, j ) );
        }
    }

    //Allocate memory to store the whole mesh.
    _quadMesh.clear( );
    _quadMesh.reserve( 4 * ( _lines - 1 ) * ( _columns - 1 ) );

    for (int i = 0; i < ( int ) _lines - 1; i++)
    {
        for (int j = 0; j < ( int ) _columns - 1; j++)
        {
            _quadMesh.push_back( computeVertexIndex( i + 0, j + 0 ) );
            _quadMesh.push_back( computeVertexIndex( i + 0, j + 1 ) );
            _quadMesh.push_back( computeVertexIndex( i + 1, j + 1 ) );
            _quadMesh.push_back( computeVertexIndex( i + 1, j + 0 ) );
        }
    }
}



void SliceQuadMeshWindow::updateWindowLabes( unsigned int numIter, double gnorm )
{
    Ihandle* g = ( Ihandle* ) IupGetAttribute( _dialog, "GNORM_LABEL" );
    Ihandle* iteration = ( Ihandle* ) IupGetAttribute( _dialog, "ITERATION_LABEL" );
    char gradient[1024], iter[1024];
    sprintf( gradient, "Gradient norm:      %.2e", gnorm );
    sprintf( iter, "Current Iteration:  %u", numIter );

    IupSetAttribute( g, IUP_TITLE, gradient );
    IupSetAttribute( iteration, IUP_TITLE, iter );
}



bool SliceQuadMeshWindow::updateSolution( const std::vector<double>& v, double gnorm )
{
    _mtx.lock( );
    unsigned int total = 2 * _lines * _columns;
    //Get the number of threads.
    const int tMax = omp_get_max_threads( );
    const int numThreads = std::max( tMax - 2, 1 );

    #pragma omp parallel for num_threads(numThreads)
    for (unsigned int idx = 0; idx < total; idx++)
    {
        _coordinates[idx] = v[idx];
    }

    updateWindowLabes( _m->getNumberIterations( ), gnorm );
    _mtx.unlock( );
    return _continueSolver;
}



void SliceQuadMeshWindow::renderScene( )
{
    //define que a cor para limpar a janela eh branca
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );

    //Limpa a janela com a cor pre-determinada
    glClear( GL_COLOR_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );
    glScaled( _scale, 1, 1 );

    //Enable API array.
    glEnableClientState( GL_VERTEX_ARRAY );

    if (_coordinates.size( ))
    {
        //Copy the coordinates vector.
        glVertexPointer( 2, GL_DOUBLE, 0, &_coordinates[0] );

        //Quad mesh.
        glColor3d( 0.0, 0.0, 0.0 );
        glDrawElements( GL_QUADS, _quadMesh.size( ), GL_UNSIGNED_INT, &_quadMesh[0] );

        //Known points.
        glColor3d( 0.0, 1.0, 0.0 );
        glDrawElements( GL_POINTS, _knownPoints.size( ), GL_UNSIGNED_INT, &_knownPoints[0] );

        //Border points.
        glColor3d( 1.0, 0.0, 0.0 );
        glDrawElements( GL_POINTS, _borderPoints.size( ), GL_UNSIGNED_INT, &_borderPoints[0] );

        //Disable API array.
        glDisableClientState( GL_VERTEX_ARRAY );
    }
}



bool SliceQuadMeshWindow::displayFunction( const std::vector<double>& v, double gnorm )
{
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );
    return window->updateSolution( v, gnorm );
}



void SliceQuadMeshWindow::buildModel( )
{
    //Read and allocate data.
    std::vector<VarPoint> v;
    v.resize( _lines * _columns );

//    delete _m;
    _m = new OptimizationModel( _solver );

    //Create variables and add constraints.
    for (unsigned int i = 0; i < _lines; i++)
    {
        for (unsigned int j = 0; j < _columns; j++)
        {
            unsigned idx = computeVertexIndex( i, j );

            v[idx].x = _m->addVariable( );
            v[idx].y = _m->addVariable( );

            if (j == 0 || j == _columns - 1)
            {
                _m->addConstraint( v[idx].x, EQUAL, _entryPoints[idx].x );
            }

            if (i == 0 || i == _lines - 1)
            {
                _m->addConstraint( v[idx].y, EQUAL, _entryPoints[idx].y );
            }
        }
    }

    for (unsigned int k = 0; k < _knownPoints.size( ); k++)
    {
        unsigned int idx = _knownPoints[k];
        unsigned int i = idx / _columns;
        unsigned int j = idx % _columns;
        if (j != 0 && j != _columns - 1 && i != 0 && i != _lines - 1)
        {
                _m->addConstraint( v[idx].x, EQUAL, _entryPoints[idx].x );
                _m->addConstraint( v[idx].y, EQUAL, _entryPoints[idx].y );
        }
    }

    //Build the objective function.
    QuadraticExpression &obj = _m->getObjectiveFunction( );
    for (int i = 0; i < ( int ) _lines; i++)
    {
        for (int j = 0; j < ( int ) _columns; j++)
        {
            LinearExpression x, y;
            if (i - 1 >= 0)
            {
                unsigned idx = computeVertexIndex( i - 1, j + 0 );
                x += v[idx].x;
                y += v[idx].y;
            }
            if (i + 1 < ( int ) _lines)
            {
                unsigned idx = computeVertexIndex( i + 1, j + 0 );
                x += v[idx].x;
                y += v[idx].y;
            }

            if (j - 1 >= 0)
            {
                unsigned idx = computeVertexIndex( i + 0, j - 1 );
                x += v[idx].x;
                y += v[idx].y;
            }
            if (j + 1 < ( int ) _columns)
            {
                unsigned idx = computeVertexIndex( i + 0, j + 1 );
                x += v[idx].x;
                y += v[idx].y;
            }
            unsigned idx = computeVertexIndex( i, j );

            x -= x.size( ) * v[idx].x;
            y -= y.size( ) * v[idx].y;

            obj += x * x + y * y;
        }
    }

    _m->setObjectiveFunction( obj );

    for (int i = 0; i < ( int ) _lines; i++)
    {
        for (int j = 0; j < ( int ) _columns; j++)
        {
            if (i > 1 && i < ( int ) _lines - 2 && j > 1 && j < ( int ) _columns - 2)
            {
                continue;
            }
            unsigned idx = computeVertexIndex( i, j );
            _m->addConstraint( v[idx].x, EQUAL, _entryPoints[idx].x );
            _m->addConstraint( v[idx].y, EQUAL, _entryPoints[idx].y );
        }
    }
    _m->setSoftConstraintsWeight( 10 );
}



void SliceQuadMeshWindow::resizeCanvas( int width, int height )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    double widthFactor = ( double ) width / viewport[2];
    double heightFactor = ( double ) height / viewport[3];

    //Define que toda a janela sera usada para desenho. Isso eh feito atras da
    //definicao do viewport, que recebe o ponto inferior esquerdo, a largura e
    //a altura da janela.
    glViewport( 0, 0, width, height );

    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( _pMin.x - _pMax.x );
    double heightWorld = fabs( _pMin.y - _pMax.y );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( _pMin.x + _pMax.x ) / 2.0;
    double cy = ( _pMin.y + _pMax.y ) / 2.0;

    //calcula o novo sistema de coordenadas
    _pMin.x = cx - 0.5 * widthWorld;
    _pMax.x = cx + 0.5 * widthWorld;
    _pMin.y = cy - 0.5 * heightWorld;
    _pMax.y = cy + 0.5 * heightWorld;

    //Define sistema de coordenadas.
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluOrtho2D( _pMin.x, _pMax.x, _pMin.y, _pMax.y );
}



int SliceQuadMeshWindow::exitButtonCallback( Ihandle* button )
{
    //Get the this pointer.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_fut.valid( ))
    {
        window->_continueSolver = false;
        window->_fut.wait( );
    }
    return IUP_CLOSE;
}



void SliceQuadMeshWindow::optimize( double eps, unsigned numIterations )
{
    //Set to solve continue solving the problem.
    _continueSolver = true;

    //Set callback to update data.
    _m->setDisplayInformationCallback( displayFunction );

    //Set the epsilon.
    _m->setSolverEps( eps );

    //The the max interation number.
    _m->setMaxIterations( numIterations );

    //Define the solver.
    _m->setOptimizationSolver( _solver );

    _m->setStepsNumber(10);
    //Optimize the problem.
    _m->optimize( );
}



void SliceQuadMeshWindow::activateComponents( Ihandle* component )
{
    //Habilita campos
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( component, "SOLVER_LIST" );
    Ihandle* initList = ( Ihandle* ) IupGetAttribute( component, "INIT_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( component, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( component, "MAX_TEXT" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( component, "STOP_BTN" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( component, "RUN_BTN" );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_YES );
}



void SliceQuadMeshWindow::optimizeMeshCallback( )
{
    //Get the this pointer.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //The eps and max interations.
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( canvas, "MAX_TEXT" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( canvas, "EPS_TEXT" );
    double eps = atof( IupGetAttribute( epsText, IUP_VALUE ) );
    unsigned int numIterations = atoi( IupGetAttribute( maxText, IUP_VALUE ) );

    //Optimize the problem.
    window->optimize( eps, numIterations );
}



int SliceQuadMeshWindow::checkFunction( )
{
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );
    if (window->_fut.wait_for( std::chrono::milliseconds( 16 ) ) == std::future_status::ready)
    {
        //Enable run button. 
        window->activateComponents( window->_dialog );

        window->_fut.get( );
        IupSetFunction( IUP_IDLE_ACTION, NULL );
    }

    IupUpdate( canvas );

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::runButtonCallback( Ihandle *button )
{
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( button, "THIS" );

    window->_fut = std::async( std::launch::async, SliceQuadMeshWindow::optimizeMeshCallback );
    IupSetFunction( IUP_IDLE_ACTION, ( Icallback ) checkFunction );

    //Desabilta campos 
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( button, "SOLVER_LIST" );
    Ihandle* initList = ( Ihandle* ) IupGetAttribute( button, "INIT_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( button, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( button, "MAX_TEXT" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( button, "RUN_BTN" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( button, "STOP_BTN" );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_VALUE, "2" );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::stopButtonCallback( Ihandle *button )
{
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( button, "THIS" );

    window->_continueSolver = false;

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::openMeshMenuCallback( Ihandle* button )
{
    //Obtem ponteiro para o this.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( button, "THIS" );

    delete window->_solver;
    delete window->_m;

    //Allocate _solver and model.
    window->_solver = 0;
    window->_m = 0;
    window->_continueSolver = false;

    window->initialize();

    Ihandle *dlg = IupFileDlg( );

    IupSetAttribute( dlg, "DIALOGTYPE", "OPEN" );
    IupSetAttribute( dlg, "TITLE", "IupFileDlg Test" );
    IupPopup( dlg, IUP_CURRENT, IUP_CURRENT );

    if (IupGetInt( dlg, "STATUS" ) == -1)
    {
        IupDestroy( dlg );
        return IUP_DEFAULT;
    }

    //Habilita campos necessarios
    window->activateComponents( button );

    string path = IupGetAttribute( dlg, "VALUE" );

    Ihandle* initList = ( Ihandle* ) IupGetAttribute( button, "INIT_LIST" );
    unsigned int initIndex = atoi( IupGetAttribute( initList, "VALUE" ) );
    window->openMesh( path, initIndex );

    IupDestroy( dlg );
    Ihandle* canvas = ( Ihandle* ) IupGetAttribute( button, "CANVAS" );
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void SliceQuadMeshWindow::initializeSolution( unsigned int initIndex )
{
    if (initIndex == 1)
    {
        for (unsigned int i = 0; i < _m->getNumberVariables( ); i++)
        {
            if (!_m->getVariable( i ).isKnownValue( ))
            {
                _m->getVariable( i ) = 0.0;
                //_coordinates[i] = 0.0;
            }
        }
    }
    else if (initIndex == 2)
    {
        for (unsigned int i = 0; i < _m->getNumberVariables( ); i++)
        {
            if (!_m->getVariable( i ).isKnownValue( ))
            {
                _m->getVariable( i ) = _coordinates[i];
            }
        }
    }
    else if (initIndex == 3)
    {
        for (unsigned int i = 0; i < _entryPoints.size( ); i++)
        {
            //x
            if (!_m->getVariable( 2 * i + 0 ).isKnownValue( ))
            {
                _m->getVariable( 2 * i + 0 ) = _entryPoints[i].x;
                _coordinates[ 2 * i + 0] = _entryPoints[i].x;
            }

            //y
            if (!_m->getVariable( 2 * i + 1 ).isKnownValue( ))
            {
                _m->getVariable( 2 * i + 1 ) = _entryPoints[i].y;
                _coordinates[ 2 * i + 1] = _entryPoints[i].y;
            }
        }
    }
}



void SliceQuadMeshWindow::allocateSolver( int index )
{
    delete _solver;

    switch (index)
    {
        case 1:
            _solver = new LBFGSOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
//            _m->setStepsNumber( 10 );
            break;
        case 2:
            _solver = new ConjugateGradientLISOptimizationSolver( );


            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 1 );
            break;
        case 3:
            _solver = new ConjugateGradientSLUOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 1 );
            break;
        case 4:
            _solver = new GradientDescentOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 25 );
            break;
    }
}



int SliceQuadMeshWindow::actionInitListCallback( Ihandle* initList, char *text, int item, int state )
{
    if (state)
    {
        SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( initList, "THIS" );

        int initIndex = atoi( IupGetAttribute( initList, IUP_VALUE ) );
        window->initializeSolution( initIndex );
    }
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::actionSolverListCallback( Ihandle* solverList, char *text, int item, int state )
{
    if (state)
    {
        SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( solverList, "THIS" );

        int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
        window->allocateSolver( solverIndex );
    }
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::actionCanvasCallback( Ihandle* canvas )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );


    //Redesenha a janela.
    window->renderScene( );

    //Troca os buffers.
    IupGLSwapBuffers( canvas );

    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::resizeCanvasCallback( Ihandle *canvas, int width, int height )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //Redesenha a janela.
    window->resizeCanvas( width, height );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void SliceQuadMeshWindow::zoom( int delta )
{
    //Calcula fator de zoom
    float factor = delta > 0 ? 1.05f : 0.95f;

    // Computa a coordenada do centro da área visível
    double cx = 0.5f * ( _pMin.x + _pMax.x );
    double cy = 0.5f * ( _pMin.y + _pMax.y );

    // Computa metade do altura de largura visível
    double halfWidth = 0.5f * ( _pMax.x - _pMin.x );
    double halfHeight = 0.5f * ( _pMax.y - _pMin.y );

    // Aplica o fator de escala para alterar a largura e altura
    halfWidth *= factor;
    halfHeight *= factor;

    _pMin.x = cx - halfWidth;
    _pMax.x = cx + halfWidth;
    _pMin.y = cy - halfHeight;
    _pMax.y = cy + halfHeight;

    //Define a matriz de projecao como matriz corrente. Essa matriz guarda informacoes
    //de como o desenho deve ser projetado na tela, desta forma qualquer definicao
    //relativa a sistema de coordenadas deve ser feita na matriz de projecao
    glMatrixMode( GL_PROJECTION );

    //Carrega a matriz indentida na matriz corrente. Esta operacao substitui a matriz
    //corrente pela matriz identidade
    glLoadIdentity( );

    //Define como o sistema de coordenadas varia dentro do viewport, ou seja,
    //da esquerda para direita o eixo x varia de x1 ate x2 e o eixo y varia de
    //y1 ate y2. Note que nao necessariamente x1 <= x2 ou y1 <= y2
    gluOrtho2D( _pMin.x, _pMax.x,
                _pMin.y, _pMax.y );
}



void SliceQuadMeshWindow::convertPixelToWorld( int px, int py, double& x, double& y )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    //Converte ponto em tela para o sistema de coordenadas.
    double t = ( double ) px / ( viewport[2] - 1 );
    x = ( 1 - t ) * _pMin.x + t * _pMax.x;
    t = 1.0 - ( double ) py / ( viewport[3] - 1 );
    y = ( 1 - t ) * _pMin.y + t * _pMax.y;
}



int SliceQuadMeshWindow::buttonCanvasCallback( Ihandle* canvas, int button, int pressed,
                                               int x, int y, char* status )
{
    if (button == IUP_BUTTON1)
    {
        //Obtem ponteiro para o this.
        SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );
        if (pressed)
        {
            window->_firstPixelX = x;
            window->_firstPixelY = y;
            window->_mousePressed = IUP_BUTTON1;
        }
        else
        {
            window->_mousePressed = -1;
        }
    }
    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::motionCanvasCallback( Ihandle* canvas, int x, int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_mousePressed == IUP_BUTTON1)
    {
        double xi = 0.0, yi = 0.0, xf = 0.0, yf = 0.0f;

        window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );
        window->convertPixelToWorld( x, y, xf, yf );

        double dx = xf - xi;
        double dy = yf - yi;

        window->_pMin.x -= dx;
        window->_pMax.x -= dx;
        window->_pMin.y -= dy;
        window->_pMax.y -= dy;

        window->_firstPixelX = x;
        window->_firstPixelY = y;

        glMatrixMode( GL_PROJECTION );
        glLoadIdentity( );

        gluOrtho2D( window->_pMin.x, window->_pMax.x, window->_pMin.y, window->_pMax.y );
    }

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}



int SliceQuadMeshWindow::wheelCanvasCallback( Ihandle* canvas, float delta, int x,
                                              int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    SliceQuadMeshWindow *window = ( SliceQuadMeshWindow* ) IupGetAttribute( canvas, "THIS" );

    delta < 0 ? window->zoom( -1 ) : window->zoom( 1 );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}
