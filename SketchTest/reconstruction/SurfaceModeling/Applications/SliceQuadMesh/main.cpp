#include <iup/iup.h>
#include <iup/iupgl.h>
#include <cstdio>
#include "SliceQuadMeshWindow.h"



/**
 * Funcao principal.
 * @param argc - numero de argumentos do programa.
 * @param argv - lista de argumentos.
 * @return - 0 caso tenha havido sucesso e 1 caso contrario.
 */
int main( int argc, char** argv )
{
    //Inicializa a IUP.
    IupOpen( &argc, &argv );

    //Inicializa a OpenGL na IUP.
    IupGLCanvasOpen( );

    //Cria objeto.
    SliceQuadMeshWindow *window = new SliceQuadMeshWindow( );

    //Exibe a janela.
    window->show( );

    //Coloca a IUP em loop.
    IupMainLoop( );


    //Deleta o obejto alocado.
    delete window;

    //Fecha a IUP.
    IupClose( );

    return 0;
}


//função para teste do otimizador


//
//int main( int argc, char** argv )
//{
//    double xMax = -1e6, xMin = +1e6;
//    int nPoints;
//    ifstream in( "/local/ncortez/entrada" );
//    in >> nPoints;
//    std::vector<Point> fixedPoints( nPoints );
//    for (int i = 0; i < nPoints; i++)
//    {
//        in >> fixedPoints[i].x >> fixedPoints[i].y;
//        if (fixedPoints[i].x < xMin)
//        {
//            xMin = fixedPoints[i].x;
//        }
//        if (fixedPoints[i].x > xMax)
//        {
//            xMax = fixedPoints[i].x;
//        }
//    }
//    int totalPoints = ( int ) ( nPoints - 1 )*20; //(xMax - xMin + 1);
//    printf( "Total de pontos a serem otimizados: %d\n", totalPoints );
//
//    //cria o solver e o model
//    OptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
////    solver->setDisplayInformationCallback( 1 );
//    solver->setEPS( 1e-1 );
//    solver->setMaxIterations( 5000 );
//    OptimizationModel* model = new OptimizationModel( solver );
//    model->showLog( false );
//    //Set callback to update data.
//
//
//    //cria um vetor para as variaveis, e relaciona com as variaveis do modelo
//    std::vector<Variable> allPointsX( totalPoints );
//    std::vector<Variable> allPointsY( totalPoints );
//    //std::vector<Variable> allPoints( totalPoints );
//    for (int i = 0; i < totalPoints; i++)
//    {
//        allPointsX[i] = model->addVariable( );
//        allPointsY[i] = model->addVariable( );
//    }
//
//    //cria a função objetivo
//    QuadraticExpression &obj = model->getObjectiveFunction( );
//
//    //adiciona regras da funcão objetivo
//    LinearExpression x = allPointsX[0] - allPointsX[1];
//    LinearExpression y = allPointsY[0] - allPointsY[1];
//
//    obj += x * x + y * y;
//    for (int i = 1; i < ( totalPoints - 1 ); i++)
//    {
//        x = allPointsX[i - 1] - 2 * allPointsX[i] + allPointsX[i + 1];
//        y = allPointsY[i - 1] - 2 * allPointsY[i] + allPointsY[i + 1];
//
//        obj += x * x + y * y;
//    }
//    x = allPointsX[totalPoints - 2] - allPointsX[totalPoints - 1];
//    y = allPointsY[totalPoints - 2] - allPointsY[totalPoints - 1];
//
//    obj += x * x + y * y;
//    model->setObjectiveFunction( obj );
//
//    double distancies[nPoints - 1];
//    double totalDistance = 0;
//    for (int i = 1; i < ( nPoints ); i++)
//    {
//        double deltaX = fixedPoints[i].x - fixedPoints[i - 1].x;
//        double deltaY = fixedPoints[i].y - fixedPoints[i - 1].y;
//        distancies[i - 1] = sqrt( deltaX * deltaX + deltaY * deltaY );
//        totalDistance += distancies[i - 1];
//    }
//
//    model->addConstraint( allPointsX[0], EQUAL, fixedPoints[0].x );
//    model->addConstraint( allPointsY[0], EQUAL, fixedPoints[0].y );
//    printf( "RegraPontoDeIndex %d: (%.2f,%.2f) = (%.2f,%.2f)\n", 0, allPointsX[0].getValue( ), allPointsY[0].getValue( ), fixedPoints[0].x, fixedPoints[0].y );
//    int countP = 0;
//    for (int i = 1; i < ( nPoints - 1 ); i++)
//    {
//        //printf("%f %f %d\n",fabs(fixedPoints[i].x - fixedPoints[i-1].x), (xMax - xMin), totalPoints );
//        int nPointsInterval = ( int ) ( fabs( distancies[i - 1] / totalDistance ) * totalPoints );
//        int index = countP + nPointsInterval;
//        //model->addConstraint( allPoints[( int )fixedPoints[i].x], EQUAL, fixedPoints[i].y );
//        model->addConstraint( allPointsX[index], EQUAL, fixedPoints[i].x );
//        model->addConstraint( allPointsY[index], EQUAL, fixedPoints[i].y );
//        printf( "RegraPontoDeIndex %d: (%.2f,%.2f) = (%.2f,%.2f)\n", index, allPointsX[index].getValue( ), allPointsY[index].getValue( ), fixedPoints[i].x, fixedPoints[i].y );
//        countP += nPointsInterval;
//    }
//    model->addConstraint( allPointsX[totalPoints - 1], EQUAL, fixedPoints[nPoints - 1].x );
//    model->addConstraint( allPointsY[totalPoints - 1], EQUAL, fixedPoints[nPoints - 1].y );
//    printf( "RegraPontoDeIndex %d: (%.2f,%.2f) = (%.2f,%.2f)\n", totalPoints - 1, allPointsX[totalPoints - 1].getValue( ), allPointsY[totalPoints - 1].getValue( ), fixedPoints[nPoints - 1].x, fixedPoints[nPoints - 1].y );
//
//    model->optimize( );
//    for (int i = 0; i < totalPoints; i++)
//    {
//        cout << allPointsX[i].getValue( ) << " " << allPointsY[i].getValue( ) << endl;
//    }
//
//    return 0;
//}


//
//int main( int argc, char** argv )
//{
//    int xMax = -1e6, xMin = +1e6;
//    int nPoints;
//    cin >> nPoints;
//    std::vector<Point> fixedPoints( nPoints );
//    for (int i = 0; i < nPoints; i++)
//    {
//        cin >> fixedPoints[i].x >> fixedPoints[i].y;
//        if (fixedPoints[i].x < xMin)
//        {
//            xMin = fixedPoints[i].x;
//        }
//        if (fixedPoints[i].x > xMax)
//        {
//            xMax = fixedPoints[i].x;
//        }
//    }
//    int totalPoints = xMax - xMin + 1;
//
//    //cria o solver e o model
//    OptimizationSolver* solver = new LBFGSOptimizationSolver( );
//    OptimizationModel* model = new OptimizationModel( solver );
//
//    //cria um vetor para as variaveis, e relaciona com as variaveis do modelo
//    std::vector<Variable> allPoints( totalPoints );
//    for (int i = 0; i < totalPoints; i++)
//    {
//        allPoints[i] = model->addVariable( );
//    }
//
//    //cria a função objetivo
//    QuadraticExpression &obj = model->getObjectiveFunction( );
//
//    //adiciona regras da funcão objetivo
//    LinearExpression x = allPoints[0] - allPoints[1];
//
//    obj += x * x;
//    for (int i = 1; i < ( totalPoints - 1 ); i++)
//    {
//        x = allPoints[i - 1] - 2 * allPoints[i] + allPoints[i + 1];
//
//        obj += x * x;
//    }
//    x = allPoints[totalPoints - 2] - allPoints[totalPoints - 1];
//
//    obj += x * x;
//    model->setObjectiveFunction( obj );
//
//
//    //adiciona restrições 
//    for (int i = 0; i < nPoints; i++)
//    {
//        model->addConstraint( allPoints[( int ) fixedPoints[i].x], EQUAL, fixedPoints[i].y );
//    }
//
//
//    model->optimize( );
//    for (int i = 0; i < totalPoints; i++)
//    {
//        cout << i << " " << allPoints[i].getValue( ) << endl;
//    }
//
//    return 0;
//}
