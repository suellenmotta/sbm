#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "parameterswindow.h"
#include "files.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int getUpperHorizonsNumber();
    int getDownHorizonsNumber();

private:
    Ui::MainWindow *ui;
    ParametersWindow *parameterWindow;
    Files *files;
    int HorizonsUp;
    int HorizonsDown;

private slots:
    void onParameterButtonClicked();
    void onExportarButtonClicked();
    void on_actionSair_triggered();
    void on_actionCarregar_Reservat_rio_triggered();
    void on_actionCarregar_Horizonte_triggered();
};

#endif // MAINWINDOW_H

