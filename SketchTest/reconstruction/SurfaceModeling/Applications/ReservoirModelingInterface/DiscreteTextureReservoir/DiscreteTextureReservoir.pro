#-------------------------------------------------
#
# Project created by QtCreator 2018-02-27T17:07:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DiscreteTextureReservoir
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    parameterswindow.cpp \
    files.cpp \
    horizonsmaterials.cpp \
    savematerialsdata.cpp \
    squarematerialcolor.cpp \
    ../../ReservoirHexMesh/FromReservoirToGeomecanic/CellMesh.cpp

HEADERS += \
        mainwindow.h \
    parameterswindow.h \
    files.h \
    horizonsmaterials.h \
    savematerialsdata.h \
    squarematerialcolor.h \
    ../../ReservoirHexMesh/FromReservoirToGeomecanic/CellMesh.h

FORMS += \
    mainwindow.ui \
    Files.ui \
    parameterswindow.ui \
    squarematerialcolor.ui

DISTFILES += \
    fragmentshader.glsl \
    vertexshader.glsl

#Para utilizar glPointSize:
win32{
LIBS += -lopengl32 -lglu32
}

linux{
LIBS += -lGL
}

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../libs/libecl-master/lib/ -llibecl.dll
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../libs/libecl-master/lib/ -llibecl.dll
else:unix: LIBS += -L$$PWD/../../../libs/libecl-master/lib/ -llibecl.dll

INCLUDEPATH += $$PWD/../../../libs/libecl-master/include
DEPENDPATH += $$PWD/../../../libs/libecl-master/include
