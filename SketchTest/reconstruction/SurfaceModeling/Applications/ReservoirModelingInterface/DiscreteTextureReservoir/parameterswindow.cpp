#include "parameterswindow.h"
#include "ui_parameterswindow.h"
#include <QLineEdit>

ParametersWindow::ParametersWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ParametersWindow)
{
    ui->setupUi(this);
}

ParametersWindow::~ParametersWindow()
{
    delete ui;
}

