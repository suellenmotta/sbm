#ifndef RENDEROPENGL_H
#define RENDEROPENGL_H

#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>

#include <QMatrix4x4>
#include <QVector3D>
#include <vector>


class RenderOpenGL :
        public QOpenGLWidget
        , protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit RenderOpenGL(QWidget *parent = 0);

    ~RenderOpenGL();

    void initializeGL() override;

    void paintGL() override;

    void resizeGL(int width, int height) override;

private:
    QMatrix4x4 view;
    QMatrix4x4 proj;

    /***        Shaders          ***/
    //  Visualização das posicoes
    QOpenGLShaderProgram* vertexProgram;
    //  Visualização das propriedades
    QOpenGLShaderProgram* propProgram;

    /*******************************/

};
#endif // RENDEROPENGL_H
