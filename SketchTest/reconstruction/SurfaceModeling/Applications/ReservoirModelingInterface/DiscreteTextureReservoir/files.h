#ifndef FILES_H
#define FILES_H

#include <QMainWindow>
#include<QLineEdit>
#include "savematerialsdata.h"
#include <QSignalMapper>
#include<QTableWidget>
namespace Ui {
class Files;
}

class Files : public QMainWindow
{
    Q_OBJECT

public:
    explicit Files(QWidget *parent = 0);
    ~Files();
    std::vector<std::pair<double,double>> GetMaterialsProperties();
    void setNumberHorizons(int Up,int Down);

private:
    Ui::Files *ui;
    int countNewMaterial;
    QSignalMapper  *signalMapper;
    QString lastSelected;
    std::vector<SaveMaterialsData> Material;

    //.dat
    float _numbThreads;
    float _numbSteps;
    float _initialTime;

    //.sig
    float _stressGradient;
    float _stressX;
    float _stressY;

    //.ppi
    float _reference;
    float _poreReeference;
    float _gradient;

    //.bco
    int _pressureChanges;
    int _numElements;



private:
    void Treat_Materials(std::string Name,QColor color);
    void UpdateInfoMaterials();
private slots:
    void on_Next2_clicked();
    void on_Next3_clicked();
    void on_Next4_clicked();
    void on_Next5_clicked();
    void on_Next6_clicked();
    void on_Final_clicked();
    void on_Back1_clicked();
    void on_Back2_clicked();
    void on_Back3_clicked();
    void on_Back4_clicked();
    void on_Back5_clicked();
    void on_SimplifiedSig_clicked();
    void on_DetailedSig_clicked();
    void on_pushButton_clicked();
    void on_addMaterialButton_clicked();
    void on_ClearButton_clicked();
    void on_changeColorButton_clicked();
    void on_doubleSpinBoxYoung_valueChanged(double arg1);
    void on_doubleSpinBoxPoisson_valueChanged(double arg1);
    void on_tableWidgetMaterials_itemClicked(QTableWidgetItem *item);
    void on_tableWidgetMaterials_itemSelectionChanged();
    void on_tableWidgetMaterials_itemChanged(QTableWidgetItem *item);
    void on_tableWidgetOptionsMaterials_itemClicked(QTableWidgetItem *item);

    void on_Initial_valueChanged(double arg1);
    void on_DetailedPost_clicked();
    void on_SimplifiedPost_clicked();
    void on_AddFileMaterials_clicked();
    void on_SigFile_clicked();
    void on_pushButton_3_clicked();
};

#endif // FILES_H
