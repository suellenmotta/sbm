#include "savematerialsdata.h"


SaveMaterialsData::SaveMaterialsData() :
    _Young(1000.00),
    _Poisson(0.30)
//    doubleSpinBoxSpecificWeight(0.01),
//    doubleSpinBoxCohesion(0.00),
//    doubleSpinBoxDilationAngle(0.00),
//    doubleSpinBoxFrictionAngle(0.00),
//    doubleSpinBoxFluidSpecificWeight(0.00),
//    doubleSpinBoxDrySpecificWeight(0.00)
{


}
SaveMaterialsData::SaveMaterialsData(std::string name,double Poisson,double Young,QVector3D color)
//    doubleSpinBoxSpecificWeight(0.01),
//    doubleSpinBoxCohesion(0.00),
//    doubleSpinBoxDilationAngle(0.00),
//    doubleSpinBoxFrictionAngle(0.00),
//    doubleSpinBoxFluidSpecificWeight(0.00),
//    doubleSpinBoxDrySpecificWeight(0.00)
{
    _name=name;
    _Poisson=Poisson;
    _Young=Young;
    _colorMaterial=color;

}

double SaveMaterialsData::getYoung() const
{

    return _Young;
}

void SaveMaterialsData::setYoung(double value)
{
    _Young = value;
}

double SaveMaterialsData::getPoisson() const
{

    return _Poisson;
}

void SaveMaterialsData::setPoisson(double value)
{

    _Poisson = value;
}
std::string SaveMaterialsData:: getName() const
{
    return _name;
}

void SaveMaterialsData::setName(std::string value)
{
    _name=value;
}
QVector3D SaveMaterialsData:: getColor() const
{
    return _colorMaterial;
}
QColor SaveMaterialsData:: getQColor() const
{
    return _QcolorMaterial;
}
void SaveMaterialsData::setColor(QColor value)
{
    _QcolorMaterial=value;
    _colorMaterial.setX(value.red()/255.0);
    _colorMaterial.setY(value.green()/255.0);
    _colorMaterial.setZ(value.blue()/255.0);
}
