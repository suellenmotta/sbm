#include "renderopengl.h"
#include <cfloat>
#include <QMouseEvent>
#include <QWheelEvent>


RenderOpenGL::RenderOpenGL(QWidget *parent) : QOpenGLWidget(parent), vertexProgram(nullptr), propProgram(nullptr)
{
}

RenderOpenGL::~RenderOpenGL()
{
    makeCurrent();
    doneCurrent();
    delete vertexProgram;
    delete propProgram;
}


void RenderOpenGL::initializeGL()
{
    initializeOpenGLFunctions();

    makeCurrent();

    glViewport(0,0,width(),height());

    //Layout de ponto e linha:
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_POINT_SMOOTH);

    glPointSize(8.0f);

    vertexProgram = new QOpenGLShaderProgram();

    //Vertex Shader
    if (vertexProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, "../DiscreteTexture/vertexShader.glsl"))
        printf("Vertex Shader added successfully\n");
    else
        printf("Deu ruim addando o vertex shader\n");

    //Fragment Shader
    if (vertexProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, "../DiscreteTexture/fragmentShader.glsl"))
        printf("Fragment Shader added successfully\n");
    else
        printf("Deu ruim addando o fragment shader\n");

    vertexProgram->link();

    if (!vertexProgram->isLinked())
        printf("O programa não foi linkado");


}






void RenderOpenGL::paintGL()
{


}


void RenderOpenGL::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}







