#include "files.h"
#include "ui_Files.h"
#include "mainwindow.h"
#include "squarematerialcolor.h"
#include <QComboBox>
#include <QTableWidget>
#include <QTableWidgetItem>
#include<QColorDialog>
#include <QString>
#include <QFileDialog>
Q_DECLARE_METATYPE(SaveMaterialsData*)

Files::Files(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Files)
{
    ui->setupUi(this);
    ui->SimplifiedSig->setChecked(true);
    ui->SimplifiedPost->setChecked(true);
    ui->stackedWidget->setCurrentIndex(0);
    countNewMaterial=0;
    signalMapper = new QSignalMapper(this);
    ui->frame_8->setStyleSheet("background-color: white");
    ui->ClearButton->setEnabled(false);
    ui->changeColorButton->setEnabled(false);
    ui->Next3->setEnabled(false);
    ui->doubleSpinBoxYoung->setSuffix(" kPa");
}

Files::~Files()
{
    delete ui;
}

void Files::on_Next2_clicked() //Next do .dat para o .prp pagina 1 para 2
{
    //Gravando info do .dat
    _numbThreads=ui->NumbThreads->value();
    _numbSteps=ui->NumbSteps->value();
    _initialTime=ui->Initial->value();

    ui->stackedWidget->setCurrentIndex(1);
}
void Files::on_Back1_clicked() //Back do .prp para o .dat pagina 2 para 1
{
      ui->stackedWidget->setCurrentIndex(0);
}
void Files::on_Next3_clicked() //Next do .prp para .post.msh pagina 2 para 3
{
    if(ui->SimplifiedPost->isChecked())
    {
        UpdateInfoMaterials();
    }
    else
    {
        //lidar com arquivo
    }

   ui->stackedWidget->setCurrentIndex(2);
}

void Files::on_Back2_clicked() //Back do .post.msh para .prp pagina 3 para 2
{
    ui->tableWidgetOptionsMaterials->setRowCount(0);
    ui->stackedWidget->setCurrentIndex(1);
}
void Files::on_Next4_clicked() //Next do .post.msh para .sig pagina 3 para 4
{
    ui->stackedWidget->setCurrentIndex(3);
}

void Files::on_Back3_clicked() //Back do .sig para .post.msh pagina 4 para 3
{
      ui->stackedWidget->setCurrentIndex(2);
}
void Files::on_Next5_clicked() //Next do .sig para .ppi pagina 4 para 5
{
    //Gravando info do .sig
    if(ui->SimplifiedSig->isChecked())
    {
        _stressGradient= ui->StressGradient->value();
        _stressX=ui->Stressx->value();
        _stressY=ui->Stressy->value();
    }
    else
    {
        //lidar com arquivo
    }

    ui->stackedWidget->setCurrentIndex(4);
}

void Files::on_Back4_clicked() //Back do .ppi para .sig pagina 5 para 4
{
      ui->stackedWidget->setCurrentIndex(3);
}
void Files::on_Next6_clicked() //Next do .ppi para .bco pagina 5 para 6
{
    //Gravando info do .ppi
    _reference=ui->Reference->value();
    _poreReeference=ui->PoreReference->value();
    _gradient=ui->Gradient->value();

    ui->stackedWidget->setCurrentIndex(5);
}
void Files::on_Back5_clicked() //Back do .bco para .ppi pagina 6 para 5
{
      ui->stackedWidget->setCurrentIndex(4);
}
void Files::on_Final_clicked() //Next do .bco para final pagina 6 para final
{
    //Gravando info do .bco
    _pressureChanges=ui->PressureChanges->value();
    _numElements=ui->NumElements->value();

}


void Files::on_SimplifiedSig_clicked()
{
        ui->SigFile->setEnabled(false);
        ui->StressGradient->setEnabled(true);
        ui->Stressx->setEnabled(true);
        ui->Stressy->setEnabled(true);
}

void Files::on_DetailedSig_clicked()
{
    ui->SigFile->setEnabled(true);
    ui->StressGradient->setEnabled(false);
    ui->Stressx->setEnabled(false);
    ui->Stressy->setEnabled(false);
}

void Files::on_pushButton_clicked()
{
   int nMaterials = ui->tableWidgetMaterials->rowCount();
   for(int row=0;row<nMaterials;row++)
   {
       std::string name = ui->tableWidgetMaterials->item(row, 0)->text().toStdString(); //Retorna o que foi escrito de nome na row da tabela
       QVariant v = ui->tableWidgetMaterials->item(row, 0)->data(Qt::UserRole);
       SaveMaterialsData* materialsData = v.value<SaveMaterialsData*>();
       std::cout<<""<<materialsData->getName()<<std::endl;
       std::cout<<"Young: "<<materialsData->getYoung()<<std::endl;
       std::cout<<"Poisson: "<<materialsData->getPoisson()<<std::endl;

       //Pegando a cor RGB
       QVector3D color;
       color=materialsData->getColor();
       printf("Cor: %f %f %f \n",color.x(),color.y(),color.z());

   }
}

void Files:: Treat_Materials(std::string Name,QColor color)
{

    QTableWidgetItem *materialTableItem = new QTableWidgetItem();


    materialTableItem->setText(QString::fromStdString(Name));

    int rowNum = ui->tableWidgetOptionsMaterials->rowCount();
    ui->tableWidgetOptionsMaterials->insertRow(rowNum);
    ui->tableWidgetOptionsMaterials->setItem(rowNum,0,materialTableItem);
    ui->tableWidgetOptionsMaterials->selectRow(rowNum);

    auto squareMaterialColor = new SquareMaterialColor();
    connect(squareMaterialColor->getToolButtonSquareMaterialColor(), SIGNAL(clicked()), signalMapper, SLOT(map()));
    squareMaterialColor->getToolButtonSquareMaterialColor()->setStyleSheet("background-color:"+color.name());
    ui->tableWidgetOptionsMaterials->setCellWidget(rowNum, 1, squareMaterialColor);
    ui->tableWidgetOptionsMaterials->setColumnWidth(1, 20);
}

void Files::on_addMaterialButton_clicked()
{

    SaveMaterialsData *materialsData = new SaveMaterialsData();

    QTableWidgetItem *materialTableItem = new QTableWidgetItem();

    QVariant qv;

    qv.setValue(materialsData);

    materialTableItem->setData(Qt::UserRole,qv);
    materialTableItem->setTextColor("blue");


    QString stringMaterial = "New material ";
    stringMaterial.append(QString::number(countNewMaterial));
    countNewMaterial++;

     materialTableItem->setText(stringMaterial);


    int rowNum = ui->tableWidgetMaterials->rowCount();
    ui->tableWidgetMaterials->insertRow(rowNum);
    ui->tableWidgetMaterials->setItem(rowNum,0,materialTableItem);
    ui->tableWidgetMaterials->selectRow(rowNum);

  auto squareMaterialColor = new SquareMaterialColor();
    connect(squareMaterialColor->getToolButtonSquareMaterialColor(), SIGNAL(clicked()), signalMapper, SLOT(map()));
    signalMapper->setMapping(squareMaterialColor->getToolButtonSquareMaterialColor(), rowNum);
    squareMaterialColor->getToolButtonSquareMaterialColor()->setStyleSheet("background-color: #be773f; border:1px solid #be773f;");
    ui->tableWidgetMaterials->setCellWidget(rowNum, 1, squareMaterialColor);


    ui->tableWidgetMaterials->setColumnWidth(1, 20);
    ui->ClearButton->setEnabled(true);

    QList <QTableWidgetItem*> materialListItem = ui->tableWidgetMaterials->selectedItems();

    QVariant qvPtr = materialListItem[0]->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();
    materialsLocalData->setPoisson(1000);
    materialsLocalData->setPoisson(0.3);
    materialsLocalData->setName(stringMaterial.toLocal8Bit().constData());
    int r =squareMaterialColor->getToolButtonSquareMaterialColor()->palette().color(squareMaterialColor->backgroundRole()).toRgb().red();
    int g =squareMaterialColor->getToolButtonSquareMaterialColor()->palette().color(squareMaterialColor->backgroundRole()).toRgb().green();
    int b =squareMaterialColor->getToolButtonSquareMaterialColor()->palette().color(squareMaterialColor->backgroundRole()).toRgb().blue();
    materialsLocalData->setColor(QColor(r,g,b));
}

void Files::on_ClearButton_clicked()
{
    QString itemRemoved;
    QList <QTableWidgetItem*> itemSelected = ui->tableWidgetMaterials->selectedItems();
    int item = ui->tableWidgetMaterials->currentRow();
    ui->tableWidgetMaterials->removeRow(item);

    itemRemoved = itemSelected.at(0)->text();

    if(ui->tableWidgetMaterials->rowCount()==0)
    {
        ui->ClearButton->setEnabled(false);
        ui->changeColorButton->setEnabled(false);
    }
}

void Files::on_changeColorButton_clicked()
{
    QColor color;
    color = QColorDialog::getColor(Qt::white, this);
    if (!color.isValid())
    {
        return;
    }

    QList <QTableWidgetItem*> itemSelected = ui->tableWidgetMaterials->selectedItems();

    int row = itemSelected.at(0)->row();

    SquareMaterialColor* colorWidget = (SquareMaterialColor*)ui->tableWidgetMaterials->cellWidget(row, 1);
    colorWidget->getToolButtonSquareMaterialColor()->setStyleSheet("background-color:"+color.name());

    QString itemMaterialName = itemSelected.at(0)->text();

    QList <QTableWidgetItem*> materialListItem = ui->tableWidgetMaterials->selectedItems();
    QVariant qvPtr = materialListItem[0]->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();

    int r =colorWidget->getToolButtonSquareMaterialColor()->palette().color(colorWidget->backgroundRole()).toRgb().red();
    int g =colorWidget->getToolButtonSquareMaterialColor()->palette().color(colorWidget->backgroundRole()).toRgb().green();
    int b =colorWidget->getToolButtonSquareMaterialColor()->palette().color(colorWidget->backgroundRole()).toRgb().blue();
    materialsLocalData->setColor(QColor(r,g,b));
}

void Files::on_doubleSpinBoxYoung_valueChanged(double arg1)
{
    QList <QTableWidgetItem*> materialListItem = ui->tableWidgetMaterials->selectedItems();

    QVariant qvPtr = materialListItem[0]->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();
    materialsLocalData->setYoung(arg1);
}

void Files::on_doubleSpinBoxPoisson_valueChanged(double arg1)
{
    QList <QTableWidgetItem*> materialListItem = ui->tableWidgetMaterials->selectedItems();

    QVariant qvPtr = materialListItem[0]->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();
    materialsLocalData->setPoisson(arg1);
}

void Files::on_tableWidgetMaterials_itemClicked(QTableWidgetItem *item)
{
    if(item == 0)
    {
        return;
    }

    if (item->column() == 1)
    {
        QTableWidgetItem* materialListItem = ui->tableWidgetMaterials->item(item->row(),0);
        materialListItem->setSelected(true);
        return;
    }

    lastSelected= item->text();

    QVariant qvPtr = item->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();


    ui->doubleSpinBoxYoung->setValue(materialsLocalData->getYoung());
    ui->doubleSpinBoxPoisson->setValue(materialsLocalData->getPoisson());
    ui->doubleSpinBoxPoisson->setValue(materialsLocalData->getPoisson());

    QList <QTableWidgetItem*> itemSelected = ui->tableWidgetMaterials->selectedItems();

    if (itemSelected.count() > 0)
    {
        ui->changeColorButton->setEnabled(true);
        ui->Next3->setEnabled(true);
    }
}

void Files::on_tableWidgetMaterials_itemChanged(QTableWidgetItem *item)
{
    int row=ui->tableWidgetMaterials->row(item);
    QVariant qvPtr = item->data(Qt::UserRole);
    SaveMaterialsData* materialsLocalData = qvPtr.value<SaveMaterialsData*>();
    materialsLocalData->setName(ui->tableWidgetMaterials->item(row, 0)->text().toStdString());
}

void Files::on_tableWidgetMaterials_itemSelectionChanged()
{
    on_tableWidgetMaterials_itemClicked(ui->tableWidgetMaterials->currentItem());

}

void Files::setNumberHorizons(int Up,int Down)
{
    ui->openGLWidget->setNumberHorizons(Up,Down);
}

void Files:: UpdateInfoMaterials()
{
    int nMaterials = ui->tableWidgetMaterials->rowCount();
    Material.clear();
    for(int row=0;row<nMaterials;row++)
    {
      QVariant v = ui->tableWidgetMaterials->item(row, 0)->data(Qt::UserRole);
      SaveMaterialsData* materialsData = v.value<SaveMaterialsData*>();
      SaveMaterialsData Aux(materialsData->getName(),materialsData->getPoisson(),materialsData->getYoung(),materialsData->getColor());
      Material.push_back(Aux);
      Treat_Materials(materialsData->getName(),materialsData->getQColor());
    }
    ui->openGLWidget->setMaterialsVector(Material);
}

void Files::on_tableWidgetOptionsMaterials_itemClicked(QTableWidgetItem *item)
{
    QVariant v = ui->tableWidgetMaterials->item(item->row(), 0)->data(Qt::UserRole);
    SaveMaterialsData* materialsData = v.value<SaveMaterialsData*>();
    SaveMaterialsData Aux(materialsData->getName(),materialsData->getPoisson(),materialsData->getYoung(),materialsData->getColor());
    ui->openGLWidget->setMaterialNewColor(materialsData->getColor(),Aux);
}

void Files::on_Initial_valueChanged(double arg1)
{
    ui->Final_2->setValue(arg1);
    ui->Timebco->setValue(arg1);
}


void Files::on_DetailedPost_clicked()
{
    ui->tableWidgetOptionsMaterials->hide();
    ui->openGLWidget->hide();
    ui->AddFileMaterials->setEnabled(true);
}

void Files::on_SimplifiedPost_clicked()
{
    ui->openGLWidget->show();
    ui->tableWidgetOptionsMaterials->show();
    ui->AddFileMaterials->setEnabled(false);

}

void Files::on_AddFileMaterials_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Arquivo de Materiais"), "", tr("Arquivos de materiais (*.txt)"));
}

void Files::on_SigFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Arquivo de Stress"), "", tr("Arquivos de Stress (*.txt)"));
}

void Files::on_pushButton_3_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Arquivo de Pore Pressure Changes"), "", tr("Arquivos de Pore Pressure Changes (*.txt)"));
}
