#include <vector>
#include <cmath>
#include <set>
#include "LinearExpression.h"
#include "QuadraticExpression.h"
#include "PreprocessingEqualityConstraints.h"

namespace ModelManager
{



    PreprocessingEqualityConstraints::PreprocessingEqualityConstraints( std::vector<LinearExpression*>& c,
                                                                        unsigned int numberOfThreads )
    {
        _normalRankMatrix = _augmentedRankMatrix = 0;
        _equalityConstraints = c;
        _numberOfThreads = numberOfThreads;

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < c.size( ); i++)
        {
            LinearExpression* exp = c[i];

            //Remove the known variables.
            for (unsigned int j = 0; j < exp->size( ); j++)
            {
                Variable v = exp->getVariable( j );
                double c = exp->getCoefficient( j );

                if (v.isKnownValue( ))
                {
                    exp->setCoefficient( j, 0.0 );
                    ( *exp ) += c * v.getValue( );
                }
            }
            exp->update( );
        }
    }



    PreprocessingEqualityConstraints::~PreprocessingEqualityConstraints( )
    {
        for (unsigned int i = 0; i < _equalityConstraints.size( ); i++)
        {
            delete _equalityConstraints[i];
        }
    }



    void PreprocessingEqualityConstraints::buildSolution( )
    {
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _pivots.size( ); i++)
        {
            //Subtract the pivot.
            ( *_equalityConstraints[i] ) -= _pivots[i];

            //Multiply by -1, so that we have pivot = c[i].
            ( *_equalityConstraints[i] ) *= -1;
            _equalityConstraints[i]->update( );
        }
    }



    void PreprocessingEqualityConstraints::computeRank( )
    {
        _normalRankMatrix = _augmentedRankMatrix = 0;

        for (unsigned int i = 0; i < _equalityConstraints.size( ); i++)
        {
            if (_equalityConstraints[i]->size( ) == 0)
            {
                double constant = _equalityConstraints[i]->getConstant( );
                if (fabs( constant ) > 1e-8)
                {
                    _augmentedRankMatrix++;
                }
            }
            else
            {
                _normalRankMatrix++;
                _augmentedRankMatrix++;
            }
        }
    }



    bool PreprocessingEqualityConstraints::findNextLine( int line, const Variable v )
    {
        //Get the next line that v has a non-zero coefficient.
        for (unsigned int i = line; i < _equalityConstraints.size( ); i++)
        {
            if (findVariable( _equalityConstraints[i], v ) != -1)
            {
                //Swap lines such that the next line has a non-zero coefficient
                //for v.
                std::swap( _equalityConstraints[i], _equalityConstraints[line] );
                return true;
            }
        }

        return false;
    }



    int PreprocessingEqualityConstraints::findVariable( const LinearExpression* l,
                                                        const Variable v ) const
    {
        int lb = 0, ub = l->size( );
        while (lb <= ub)
        {
            int mid = ( lb + ub ) / 2;
            Variable midV = l->getVariable( mid );

            if (midV == v)
                return mid;
            else if (midV < v)
                lb = mid + 1;
            else
                ub = mid - 1;
        }
        return -1;
    }



    void PreprocessingEqualityConstraints::getVariablesUsedOnConstraints( std::vector<Variable>& usedVars )
    {
        std::set<Variable> used;
        for (unsigned int i = 0; i < _equalityConstraints.size( ); i++)
        {
            for (unsigned int v = 0; v < _equalityConstraints[i]->size( ); v++)
            {
                Variable var = _equalityConstraints[i]->getVariable( v );
                used.insert( var );
            }
        }
        usedVars.insert( usedVars.end( ), used.begin( ), used.end( ) );
    }



    int PreprocessingEqualityConstraints::searchPivot( const Variable v ) const
    {
        int lb = 0, ub = _pivots.size( ) - 1;
        while (lb <= ub)
        {
            int mid = ( lb + ub ) / 2;
            Variable midV = _pivots[mid];

            if (midV == v)
                return mid;
            else if (midV < v)
                lb = mid + 1;
            else
                ub = mid - 1;
        }
        return -1;
    }



    void PreprocessingEqualityConstraints::processObjectiveFunction( QuadraticExpression* obj ) const
    {
        int cacheVariable = -1;
        Variable oldVariable;
        unsigned int objSize = obj->size( );
        for (unsigned int v = 0; v < objSize; v++)
        {
            double coef = obj->getCoefficient( v );
            Variable v1 = obj->getVar1( v );
            Variable v2 = obj->getVar2( v );

            //Get the variables index on pivots vector.
            int v1Index = -1, v2Index = searchPivot( v2 );
            if (v1 != oldVariable || cacheVariable == -1)
            {
                v1Index = searchPivot( v1 );
                cacheVariable = v1Index;
                oldVariable = v1;
            }
            else
            {
                v1Index = cacheVariable;
            }

            //Remove the fixed variables from the objective function.
            if (v1Index != -1 && v2Index != -1)
            {
                obj->setCoefficient( v, 0.0 );
                QuadraticExpression q = coef * ( *_equalityConstraints[v1Index] ) * ( *_equalityConstraints[v2Index] );
                q.update( );
                
                ( *obj ) += q;
            }
            else if (v1Index != -1 && v2Index == -1)
            {
                obj->setCoefficient( v, 0.0 );
                QuadraticExpression q = coef * ( *_equalityConstraints[v1Index] ) * v2;
                q.update( );
                
                ( *obj ) += q;
            }
            else if (v1Index == -1 && v2Index != -1)
            {
                obj->setCoefficient( v, 0.0 );
                QuadraticExpression q = coef * v1 * ( *_equalityConstraints[v2Index] );
                q.update( );
                
                ( *obj ) += q;
            }
        }

        LinearExpression& l = obj->getLinearExpression( );
        for (unsigned int f = 0; f < l.size( ); f++)
        {
            Variable vi = l.getVariable( f );
            double c = l.getCoefficient( f );

            int index = searchPivot( vi );
            if (index != -1)
            {
                l.setCoefficient( f, 0.0 );
                l += c * ( *_equalityConstraints[index] );
            }
        }

        obj->update( );
    }



    void PreprocessingEqualityConstraints::rowReducedForm( const std::vector<Variable>& vars )
    {
        //Vector to pivots.
        _pivots.reserve( vars.size( ) );

        //Put the matrix on row reduced form.
        unsigned int line = 0;
        for (unsigned int v = 0; v < vars.size( ) && line < _equalityConstraints.size( ); v++, line++)
        {
            if (v % 1000 == 0)
            {
                printf( "\r        %6.2lf%% complete...", 100.0 * ( double ) v / vars.size( ) );
                fflush( stdout );
            }

            if (!findNextLine( line, vars[v] ))
            {
                line--;
                continue;
            }

            _pivots.push_back( vars[v] );

            double coef = _equalityConstraints[line]->getCoefficient( 0 );
            ( *_equalityConstraints[line] ) /= coef;

            #pragma omp parallel for num_threads(_numberOfThreads)
            for (unsigned int l = 0; l < _equalityConstraints.size( ); l++)
            {
                if (l != line)
                {
                    int idx = findVariable( _equalityConstraints[l], vars[v] );
                    if (idx != -1)
                    {
                        double coeff = _equalityConstraints[l]->getCoefficient( idx );
                        ( *_equalityConstraints[l] ) -= coeff * ( *_equalityConstraints[line] );
                        _equalityConstraints[l]->update( );
                    }
                }
            }
        }
        printf( "\r        %6.2lf%% complete...\n", 100.0 );
    }



    void PreprocessingEqualityConstraints::computeValuesForFixedVariables( )
    {
        for (unsigned int i = 0; i < _pivots.size( ); i++)
        {
            _pivots[i] = _equalityConstraints[i]->getValue( );
        }
    }



    unsigned int PreprocessingEqualityConstraints::getAugmentedMatrixRank( ) const
    {
        return _augmentedRankMatrix;
    }



    unsigned int PreprocessingEqualityConstraints::getNomalMatrixRank( ) const
    {
        return _normalRankMatrix;
    }



    std::vector<Variable>& PreprocessingEqualityConstraints::getPivots( )
    {
        return _pivots;
    }



    bool PreprocessingEqualityConstraints::processingEqualityConstraints( QuadraticExpression* obj )
    {
        std::vector<Variable> vars;
        getVariablesUsedOnConstraints( vars );

        printf( "        Computing row reduced form...\n" );
        //Put the matrix in the rown reduced form.
        rowReducedForm( vars );

        printf( "        Computing rank...\n" );

        //Compute the ranks.
        computeRank( );

        //Verify if the system is impossible.
        if (_augmentedRankMatrix != _normalRankMatrix)
            return false;

        printf( "        Fixing degrees of freedom...\n" );

        //Get the solution from the row reduced form.
        buildSolution( );


        printf( "        Simplifying objective function...\n" );

        //Simplify the objective function.
        processObjectiveFunction( obj );

        return true;
    }
} // namespace ModelManager
