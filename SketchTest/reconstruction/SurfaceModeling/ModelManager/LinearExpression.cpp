#include <map>
#include <cmath>

#include "LinearExpression.h"

namespace ModelManager
{



    LinearExpression::LinearExpression( double constant )
    {
        _constant = constant;
    }



    LinearExpression::LinearExpression( const Variable v, double coefficient )
    {
        _coefficients.push_back( coefficient );
        _constant = 0.0;
        _variables.push_back( v );
    }



    void LinearExpression::clear( )
    {
        _coefficients.clear( );
        _constant = 0.0;
        _variables.clear( );
    }



    Variable LinearExpression::getVariable( unsigned int v ) const
    {
        if (v >= _variables.size( ))
            return Variable( );
        return _variables[v];
    }



    double LinearExpression::getCoefficient( unsigned int v ) const
    {
        if (v >= _coefficients.size( ))
            return 0.0;
        return _coefficients[v];
    }



    double LinearExpression::getCoefficient( const Variable v ) const
    {
        unsigned int it = 0;
        unsigned int first = 0;
        unsigned int step = 0;
        unsigned int count = size( );
        while (count > 0)
        {
            it = first;
            step = count / 2;
            it += step;
            if (_variables[it] < v)
            {
                first = ++it;
                count -= step + 1;
            }
            else count = step;
        }
        return _variables[first] == v ? _coefficients[first] : 0.0;
    }



    double LinearExpression::getConstant( ) const
    {
        return _constant;
    }



    double LinearExpression::getValue( ) const
    {
        double value = 0.0;
        for (unsigned int i = 0; i < _coefficients.size( ); i++)
        {
            value += _coefficients[i] * _variables[i].getValue( );
        }
        value += _constant;
        return value;
    }



    LinearExpression LinearExpression::operator=( const LinearExpression &rhs )
    {
        _coefficients = rhs._coefficients;
        _constant = rhs._constant;
        _variables = rhs._variables;
        return *this;
    }



    void LinearExpression::operator+=( const LinearExpression &exp )
    {
        _variables.insert( _variables.end( ), exp._variables.begin( ), exp._variables.end( ) );
        _coefficients.insert( _coefficients.end( ), exp._coefficients.begin( ), exp._coefficients.end( ) );
        _constant += exp.getConstant( );
    }



    void LinearExpression::operator-=( const LinearExpression &exp )
    {
        _variables.insert( _variables.end( ), exp._variables.begin( ), exp._variables.end( ) );
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            _coefficients.push_back( -exp.getCoefficient( i ) );
        }
        _constant -= exp.getConstant( );
    }



    void LinearExpression::operator*=( double mult )
    {
        if (mult == 0)
        {
            clear( );
            return;
        }

        for (unsigned int i = 0; i < _coefficients.size( ); i++)
        {
            _coefficients[i] *= mult;
        }
        _constant *= mult;
    }



    void LinearExpression::operator/=( double a )
    {
        if (a == 0.0)
        {
            return;
        }

        for (unsigned int i = 0; i < _coefficients.size( ); i++)
        {
            _coefficients[i] /= a;
        }
        _constant /= a;
    }



    LinearExpression LinearExpression::operator+( const LinearExpression &rhs )
    {
        LinearExpression exp = *this;
        exp._variables.insert( exp._variables.end( ), rhs._variables.begin( ), rhs._variables.end( ) );
        exp._coefficients.insert( exp._coefficients.end( ), rhs._coefficients.begin( ), rhs._coefficients.end( ) );
        exp._constant += rhs.getConstant( );

        return exp;
    }



    LinearExpression LinearExpression::operator-( const LinearExpression &rhs )
    {
        LinearExpression exp = *this;
        exp._variables.insert( exp._variables.end( ), rhs._variables.begin( ), rhs._variables.end( ) );

        for (unsigned int i = 0; i < rhs.size( ); i++)
        {
            exp._coefficients.push_back( -rhs.getCoefficient( i ) );
        }

        exp._constant -= rhs.getConstant( );
        return exp;
    }



    void LinearExpression::remove( unsigned int i )
    {
        if (i >= _variables.size( ))return;
        _coefficients.erase( _coefficients.begin( ) + i );
        _variables.erase( _variables.begin( ) + i );
    }



    bool LinearExpression::remove( const Variable v )
    {
        if (_variables.size( ) == 0)return false;

        bool found = false;
        for (int i = ( int ) _variables.size( ) - 1; i >= 0; i--)
        {
            if (v == _variables[i])
            {
                _coefficients.erase( _coefficients.begin( ) + i );
                _variables.erase( _variables.begin( ) + i );
                found = true;
            }
        }
        return found;
    }



    void LinearExpression::setCoefficient( unsigned int i, double c )
    {
        if (i < size( ))
            _coefficients[i] = c;
    }



    unsigned int LinearExpression::size( ) const
    {
        return _variables.size( );
    }



    void LinearExpression::update( )
    {
        std::map<Variable, double> v;
        for (unsigned int i = 0; i < size( ); i++)
        {
            v[_variables[i]] += _coefficients[i];
        }

        _coefficients.reserve( v.size( ) );
        _variables.reserve( v.size( ) );

        double cons = _constant;
        clear( );
        _constant = cons;

        for (std::map<Variable, double>::iterator it = v.begin( ); it != v.end( ); it++)
        {
            if (fabs( it->second ) > 1e-8)
            {
                _variables.push_back( it->first );
                _coefficients.push_back( it->second );
            }
        }
    }



    std::ostream& operator<<( std::ostream &stream, const LinearExpression &exp )
    {
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            double c = exp.getCoefficient( i );
            if (c < 0.0)
            {
                stream << "- " << -c << " * " << exp.getVariable( i ).getName( ) << " ";
            }
            else
            {
                stream << "+ " << c << " * " << exp.getVariable( i ).getName( ) << " ";
            }
        }

        double c = exp.getConstant( );
        if (c != 0.0)
        {
            if (c < 0.0)
            {
                stream << "- " << -c;
            }
            else
            {
                stream << "+ " << c;
            }
        }
        else if (exp.size( ) == 0)
        {
            stream << 0;
        }
        return stream;
    }



    LinearExpression operator+( const LinearExpression &x, const LinearExpression &y )
    {
        LinearExpression exp = x;
        exp._variables.insert( exp._variables.end( ), y._variables.begin( ), y._variables.end( ) );
        exp._coefficients.insert( exp._coefficients.end( ), y._coefficients.begin( ), y._coefficients.end( ) );

        exp._constant += y.getConstant( );

        return exp;
    }



    LinearExpression operator+( const LinearExpression &x )
    {
        return x;
    }



    LinearExpression operator+( const Variable x, const Variable y )
    {
        LinearExpression exp;
        exp._coefficients.push_back( 1.0 );
        exp._coefficients.push_back( 1.0 );
        exp._variables.push_back( x );
        exp._variables.push_back( y );

        return exp;
    }



    LinearExpression operator+( double a, const Variable x )
    {
        LinearExpression exp;
        exp._coefficients.push_back( 1.0 );
        exp._variables.push_back( x );
        exp._constant = a;
        return exp;
    }



    LinearExpression operator-( const LinearExpression &x, const LinearExpression &y )
    {
        LinearExpression exp = x;
        exp._variables.insert( exp._variables.end( ), y._variables.begin( ), y._variables.end( ) );

        for (unsigned int i = 0; i < y.size( ); i++)
        {
            exp._coefficients.push_back( -y.getCoefficient( i ) );
        }
        exp._constant -= y.getConstant( );
        return exp;
    }



    LinearExpression operator-( const LinearExpression &x )
    {
        LinearExpression exp = x;
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            exp._coefficients[i] *= -1;
        }
        exp._constant *= -1;

        return exp;
    }



    LinearExpression operator-( const Variable x, const Variable y )
    {
        LinearExpression exp;
        exp._coefficients.push_back( 1.0 );
        exp._coefficients.push_back( -1.0 );
        exp._variables.push_back( x );
        exp._variables.push_back( y );

        return exp;
    }



    LinearExpression operator-( double a, const Variable x )
    {
        LinearExpression exp;
        exp._coefficients.push_back( -1.0 );
        exp._variables.push_back( x );
        exp._constant = a;
        return exp;
    }



    LinearExpression operator*( double a, const Variable x )
    {
        LinearExpression exp;
        exp._coefficients.push_back( a );
        exp._variables.push_back( x );
        exp._constant = 0.0;

        return exp;
    }



    LinearExpression operator*( const Variable x, double a )
    {
        LinearExpression exp;
        exp._coefficients.push_back( a );
        exp._variables.push_back( x );
        exp._constant = 0.0;

        return exp;
    }



    LinearExpression operator*( double a, const LinearExpression &x )
    {
        if (a == 0.0)
            return LinearExpression( );

        LinearExpression exp = x;
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            exp._coefficients[i] *= a;
        }
        exp._constant *= a;
        return exp;
    }



    LinearExpression operator*( const LinearExpression &x, double a )
    {
        if (a == 0.0)
            return LinearExpression( );

        LinearExpression exp = x;
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            exp._coefficients[i] *= a;
        }
        exp._constant *= a;
        return exp;
    }
} // namespace ModelManager