#include "DSIModel.h"

namespace ModelManager
{



    DSIModel::DSIModel( Solver::DSIBasedSolver *solver )
    {
        _solver = solver;
    }



    bool DSIModel::optimize( )
    {
        update( );
        return false;
    }



    void DSIModel::setGraph( Graph::DSIGraph *graph )
    {
        _graph = graph;
    }



    unsigned int DSIModel::getNumberIterations( ) const
    {
        return 0;
    }



    unsigned int DSIModel::getMaxIterations( ) const
    {
        return 0;
    }



    unsigned int DSIModel::getStepsNumber( ) const
    {
        return 0;
    }



    void DSIModel::setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm ) )
    {
    }



    void DSIModel::setMaxIterations( const unsigned int maxIter )
    {
    }



    void DSIModel::setStepsNumber( unsigned int sn )
    {
    }
} // namespace ModelManager
