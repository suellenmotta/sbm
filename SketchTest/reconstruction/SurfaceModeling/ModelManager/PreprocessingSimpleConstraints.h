#pragma once

namespace ModelManager
{
    class Constraint;
    class QuadraticExpression;

    class PreprocessingSimpleConstraints
    {
    private:
        /**
         * Number of threads to be used.
         */
        unsigned int _numberOfThreads;

    private:
        /**
         * Compute the variable values.
         * @param c - the simple constraints.
         * @return - true if the problem is feasible and false otherwise.
         */
        bool computeValues( std::vector<Constraint>& c );

        /**
         * Use the variable values to simplify the objective function.
         * @param obj - the objective function.
         */
        void simplifyObjectiveFunction( QuadraticExpression* obj );

    public:

        /**
         * Compute the value to variables, check the feasibility and simplify
         * objective function.
         * @param c - vector with all simple constraints.
         * @param obj- current objective function.
         * @param numberOfThreads - number of threads to be used on preprocessing step.
         * @return - true if the problem is feasible and false otherwise.
         */
        bool preprocessingConstrainst( std::vector<Constraint>& c, QuadraticExpression* obj, unsigned int numberOfThreads );

    };

} // namespace ModelManager
