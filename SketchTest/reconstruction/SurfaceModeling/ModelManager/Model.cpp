#include <cmath>
#include "Model.h"
#include "PreprocessingEqualityConstraints.h"
#include <omp.h>
#include "../Timer/Timer.h"
#include "PreprocessingSimpleConstraints.h"


using namespace std;

namespace ModelManager
{



    bool Model::initialize( )
    {

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _variables.size( ); i++)
        {
            _variables[i].knownValue( false );
        }

        Timer t;
        if (_showLog)
            printf( "\n    Compressing constraints...\n" );

        t.restart( );

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _constraints.size( ); i++)
        {
            _constraints[i].update( );
        }

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _simpleConstraints.size( ); i++)
        {
            _simpleConstraints[i].update( );
        }

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _softConstraints.size( ); i++)
        {
            _softConstraints[i].update( );
        }

        bool feasible = true;
        for (unsigned int i = 0; i < _simpleConstraints.size( ); i++)
        {
            const LinearExpression& exp = _simpleConstraints[i].getLine( );
            if (exp.size( ) == 0)
            {
                double b = exp.getConstant( );
                if (fabs( b ) >= 1e-8)
                {
                    feasible = false;
                    break;
                }
            }
        }

        if (_showLog)
            t.printTime( "        Total time" );

        return feasible;
    }



    bool Model::processEqualityConstraints( )
    {
        //Vector with all equality constraints.
        std::vector<LinearExpression*> constraints;

        //Copy all equality constraints.
        for (unsigned int i = 0; i < _constraints.size( ); i++)
        {
            if (_constraints[i].getSense( ) == EQUAL)
            {
                constraints.push_back( new LinearExpression( _constraints[i].getLine( ) ) );
            }
        }

        //Object to preprocessing constraints.
        _processEqualityConstrs = new PreprocessingEqualityConstraints( constraints, _numberOfThreads );

        //Process the equality constraints.
        bool feasible = _processEqualityConstrs->processingEqualityConstraints( &_objectiveFunction );

        if (!feasible)
            return false;

        //Fix the pivots.
        std::vector<Variable>& pivots = _processEqualityConstrs->getPivots( );
        for (unsigned int i = 0; i < pivots.size( ); i++)
        {
            pivots[i].knownValue( true );
        }

//        for (unsigned int i = 0; i < constraints.size( ); i++)
//            delete constraints[i];
        
        return true;
    }



    void Model::processSoftConstraints( )
    {
        Timer t;

        //Allocate vector to store the quadratic expressions.
        std::vector<QuadraticExpression> sum( _numberOfThreads, 0.0 );

        double p = _softConstraintsWeight;

        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _softConstraints.size( ); i++)
        {
            LinearExpression l = _softConstraints[i].getLine( );

            for (unsigned int j = 0; j < l.size( ); j++)
            {
                Variable v = l.getVariable( j );
                double c = l.getCoefficient( j );

                if (v.isKnownValue( ))
                {
                    l.setCoefficient( j, 0.0 );
                    l += c * v.getValue( );
                }
            }
            l.update( );

            unsigned int tr = omp_get_thread_num( );
            sum[tr] += p * ( l * l );
        }

        if (_showLog)
        {
            t.printTime( "        Total time" );
            printf( "\n    Compressing expanded soft expressions...\n" );
        }

        //Update quadratic expressions.
        t.restart( );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            sum[i].update( );
        }

        if (_showLog)
        {
            t.printTime( "        Total time" );
            t.restart( );
            printf( "\n    Adding soft constraints to objective function...\n" );
        }

        for (unsigned int i = 0; i < _numberOfThreads; i++)
        {
            _objectiveFunction += sum[i];
        }
        if (_showLog)
        {
            t.printTime( "        Total time" );
        }
    }



    Model::Model( )
    {
        _softConstraintsWeight = 1.0;
        _showLog = true;
        //        _feasible = true;
        _preprocessedModel = false;
        _processEqualityConstrs = nullptr;
        _truncationEps = 1e-8;
        _solverEps = 1e-4;

        const int tMax = omp_get_max_threads( );
        _numberOfThreads = std::max( tMax - 2, 1 );
    }



    Model::~Model( )
    {
        for (unsigned int i = 0; i < _variables.size( ); i++)
        {
            _variables[i].destroy( );
        }

        for (unsigned int i = 0; i < _constraints.size( ); i++)
        {
            _constraints[i].destroy( );
        }

        for (unsigned int i = 0; i < _simpleConstraints.size( ); i++)
        {
            _simpleConstraints[i].destroy( );
        }

        for (unsigned int i = 0; i < _softConstraints.size( ); i++)
        {
            _softConstraints[i].destroy( );
        }

        delete _processEqualityConstrs;
    }



    Constraint Model::addConstraint( const LinearExpression &exp, char sense, double rhs, double wc, std::string name )
    {
        Constraint c( exp, sense, rhs, wc, name );

        if (exp.size( ) == 1 && sense == EQUAL)
        {
            //Add the constraints.
            _simpleConstraints.push_back( c );
        }
        else if (sense != SOFT)
        {
            _constraints.push_back( c );
        }
        else
        {
            _softConstraints.push_back( c );
        }

        _preprocessedModel = false;

        return c;
    }



    Constraint Model::addRange( const LinearExpression &exp, double lb, double up, std::string name )
    {
        Constraint c( exp, lb, up, name );
        if (lb == up && exp.size( ) == 1)
        {
            Variable x = exp.getVariable( 0 );
            double a = exp.getCoefficient( 0 );
            double c = exp.getConstant( );
            x.setValue( -c / a );
            x.knownValue( true );
        }

        _constraints.push_back( c );

        _preprocessedModel = false;

        return c;
    }



    Variable Model::addVariable( std::string name, double lb, double up )
    {
        Variable v( _variables.size( ), name, lb, up );
        _variables.push_back( v );

        _preprocessedModel = false;

        return v;
    }



    Variable* Model::addVariables( double* lb, double* up, std::string* names, unsigned int len )
    {
        unsigned int size = _variables.size( );
        for (unsigned int i = 0; i < len; i++)
        {
            Variable v( size + i, names[i], lb[i], up[i] );
            _variables.push_back( v );
        }

        _preprocessedModel = false;

        return &_variables[size];
    }



    Variable* Model::addVariables( unsigned int count )
    {
        if (count == 0)return nullptr;

        unsigned int size = _variables.size( );
        _variables.resize( size + count );

        _preprocessedModel = false;

        return &_variables[size];
    }



    bool Model::processSimpleConstraints( )
    {
        //Allocate object to preprocess the simple constraints.
        PreprocessingSimpleConstraints s;

        //Preprocess the soft constraints.
        return s.preprocessingConstrainst( _simpleConstraints, &_objectiveFunction, _numberOfThreads );
    }



    double Model::getCoefficient( const Constraint c, const Variable v ) const
    {
        const LinearExpression& exp = c.getLine( );
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            if (v == exp.getVariable( i ))
            {
                return exp.getCoefficient( i );
            }
        }
        return 0.0;
    }



    Constraint* Model::getConstraints( ) const
    {
        if (_constraints.size( ) == 0) return nullptr;
        return ( Constraint* ) ( &_constraints[0] );
    }



    Constraint Model::getConstraint( unsigned int i ) const
    {
        return _constraints[i];
    }



    unsigned int Model::getNumberVariables( ) const
    {
        return _variables.size( );
    }



    QuadraticExpression& Model::getObjectiveFunction( )
    {
        return _objectiveFunction;
    }



    const QuadraticExpression& Model::getObjectiveFunction( ) const
    {
        return _objectiveFunction;
    }



    unsigned int Model::getNumberConstraints( ) const
    {
        return _constraints.size( );
    }



    unsigned int Model::getNumberSoftConstraints( ) const
    {
        return _softConstraints.size( );
    }



    double Model::getObjectiveFunctionValue( ) const
    {
        return _objectiveFunction.getValue( );
    }



    Constraint Model::getSofConstraint( unsigned int i ) const
    {
        return _softConstraints[i];
    }



    Constraint* Model::getSoftConstraints( ) const
    {
        if (_softConstraints.size( ) == 0) return nullptr;
        return ( Constraint* ) ( &_softConstraints[0] );
    }



    double Model::getSoftConstraintWeight( ) const
    {
        return _softConstraintsWeight;
    }



    Variable Model::getVariable( unsigned int i ) const
    {
        return _variables[i];
    }



    Variable* Model::getVariables( ) const
    {
        if (_variables.size( ) == 0)return nullptr;
        return ( Variable* ) ( &_variables[0] );
    }



    double Model::getTruncationEps( ) const
    {
        return _truncationEps;

    }



    double Model::getSolverEps( ) const
    {
        return _solverEps;
    }



    unsigned int Model::getNumberOfThreads( ) const
    {
        return _numberOfThreads;

    }



    void Model::setNumberOfThreads( const unsigned int numberOfThreads )
    {
        _numberOfThreads = numberOfThreads;
    }



    void Model::setTruncationEps( const double eps )
    {
        _truncationEps = eps;
    }



    void Model::setSolverEps( const double eps )
    {
        _solverEps = eps;
    }



    void Model::removeConstraint( const Constraint c )
    {
        for (unsigned int i = 0; i < _constraints.size( ); i++)
        {
            if (c == _constraints[i])
            {
                _constraints.erase( _constraints.begin( ) + i );
                return;
            }
        }
    }



    void Model::removeSoftConstraint( const Constraint c )
    {
        for (unsigned int i = 0; i < _softConstraints.size( ); i++)
        {
            if (c == _softConstraints[i])
            {
                _softConstraints.erase( _softConstraints.begin( ) + i );
                return;
            }
        }
    }



    void Model::setSoftConstraintsWeight( double scw )
    {
        _softConstraintsWeight = scw;
    }



    void Model::showLog( bool log )
    {
        _showLog = log;
    }



    bool Model::update( )
    {
        //Assume that the model is feasible.
        //        _feasible = true;

        //Prepare variables and constraints to pre-processing step.
        bool feasible = initialize( );

        if (!feasible)
            return false;

        Timer t;

         //Process the soft constraints before to update the expressions.
        if (_softConstraints.size( ))
        {
            if (_showLog)
                printf( "\n    Pre-processing soft constraints...\n" );

            processSoftConstraints( );

            _objectiveFunction.update( _showLog );
        }
        
        //Process simple equality constraints.
        if (_simpleConstraints.size( ))
        {
            if (_showLog)
                printf( "\n    Pre-processing simple constraints...\n" );

            t.restart( );
            bool feasible = processSimpleConstraints( );

            if (_showLog)
                t.printTime( "        Total time" );

            if (!feasible)
                return false;
        }

        //Process the equality constraints, fixing some freedom degrees and
        // simplifying the objective function in order to eliminate known variables.
        if (_constraints.size( ))
        {
            if (_showLog)
                printf( "\n    Pre-processing equality constraints...\n" );

            t.restart( );

            bool feasible = processEqualityConstraints( );

            if (_showLog)
                t.printTime( "        Total time" );

            if (!feasible)
                return false;
        }

        //Compressing objective function.
        {
            if (_showLog)
                printf( "\n    Compressing the objective function with %u quadratic terms and %u linear terms...\n",
                        _objectiveFunction.size( ),
                        _objectiveFunction.getLinearExpression( ).size( ) );

            t.restart( );
            _objectiveFunction.update( _showLog );
            if (_showLog)
            {
                t.printTime( "        Total compressing time" );
            }
        }

        return true;
    }



    std::ostream& operator<<( std::ostream &stream, const Model &m )
    {
        stream << "Minimize " << m.getObjectiveFunction( ) << endl;
        stream << "Subject to: " << endl;
        for (unsigned int i = 0; i < m._constraints.size( ); i++)
        {
            stream << "\t" << m._constraints[i].getName( ) << ": " << m._constraints[i] << endl;
        }

        for (unsigned int i = 0; i < m._simpleConstraints.size( ); i++)
        {
            stream << "\t" << m._simpleConstraints[i].getName( ) << ": " << m._simpleConstraints[i] << endl;
        }

        for (unsigned int i = 0; i < m._softConstraints.size( ); i++)
        {
            stream << "\t" << m._softConstraints[i].getName( ) << ": " << m._softConstraints[i] << endl;
        }
        return stream;
    }
} // namespace ModelManager
