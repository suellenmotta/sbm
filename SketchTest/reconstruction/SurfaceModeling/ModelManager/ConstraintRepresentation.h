#pragma once

#include "LinearExpression.h"

namespace ModelManager
{

    class ConstraintRepresentation
    {
    public:
        friend class Constraint;
    private:

        /**
         * The constraint lower bound.
         */
        double _lowerBound;

        /**
         * A linear expression that defines the constraint.
         */
        LinearExpression _line;

        /**
         * The constraint name.
         */
        std::string _name;
        
        /**
         * The constraint weight.
         */
        double _wc;

        /**
         * The constraint sense.
         */
        char _sense;

        /**
         * Indicates if the constraint already has a known value.
         */
        bool _isKnown;

        /**
         * The constraint upper bound.
         */
        double _upperBound;
    private:
        /**
         * The ConstraintRepresentation constructor that receives a linear expression,
         * the sense and the constraint name.
         * @param line - a linear expression with the constraint.
         * @param sense - the constraint sense.
         * @param wc - the constraint weight.
         * @param name - the constraint name.
         */
        ConstraintRepresentation( const LinearExpression &line, char sense, double wc = 1, std::string name = "" );

        /**
         * The ConstraintRepresentation constructor that receives a linear
         * expression, the sense a constant, and the constraint name.
         * @param line - a linear expression with the constraint.
         * @param sense - the constraint sense.
         * @param constant - the constraint right hand side.
         * @param wc - the constraint weight.
         * @param name - the constraint name.
         */
        ConstraintRepresentation( const LinearExpression &line, char sense, double constant, double wc = 1, std::string name = "" );

        /**
         * The constraint constructor that receives a linear expression and set
         * a range constraint.
         * @param line - a linear expression with the constraint.
         * @param lb - lower bound.
         * @param up - upper bound.
         * @param name - the constraint name.
         */
        ConstraintRepresentation( const LinearExpression &line, double lb, double up, std::string name = "" );

        /**
         * Get the linear expression that defines the constraint.
         * @return - the linear expression that defines the constraint.
         */
        LinearExpression& getLine( );
        
        /**
         * Get the linear expression that defines the constraint.
         * @return - the linear expression that defines the constraint.
         */
        const LinearExpression& getLine( ) const;

        /**
         * Get the constraint name.
         * @return - the constraint name.
         */
        const std::string& getName( ) const;
        
        /**
         * Get the weight constraint.
         * @return - the weight constraint.
         */
        const double getWc( ) const;

        /**
         * Get the constraint sense.
         * @return - the constraint sense.
         */
        char getSense( ) const;

        /**
         * Return if the constraint has a known value.
         * @return - true if the constraint value has a known value and false
         * otherwise.
         */
        bool getIsKnown( ) const;
        
        /**
         * Define if the constraint value is known or not.
         * @return - true if the constraint is known and false otherwise.
         */
        void setIsKnown( const bool known );

        /**
         * Set a new name to constraint.
         * @param name - new name.
         */
        void setName( const std::string &name );
        
        /**
         * Set a new constraint weight.
         * @param wc - new constraint weight.
         */
        void setWc( const double wc );
        
        /**
         * Set a new constraint sense.
         * @param sense - new constraint sense.
         */
        void setSense( char sense );

        /**
         * Group the expression and sort it.
         */
        void update( );
    };

} // namespace ModelManager
