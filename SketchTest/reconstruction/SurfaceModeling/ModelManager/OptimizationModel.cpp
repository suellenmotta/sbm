#include <cstdio>
#include <cmath>
#include <omp.h>
#include "../Solver/OptimizationSolvers/OptimizationSolver.h"
#include "OptimizationModel.h"
#include "../Timer/Timer.h"
#include "PreprocessingEqualityConstraints.h"

using namespace Solver;

namespace ModelManager
{



    OptimizationModel::OptimizationModel( OptimizationSolver *solver )
    {
        _solver = solver;
    }



    const Solver::OptimizationSolver *OptimizationModel::getOptimizationSolver( ) const
    {
        return _solver;
    }



    unsigned int OptimizationModel::computeNewIndex( )
    {
        _newVariableIndex.resize( _variables.size( ) );
        unsigned int numberUnknownV = 0;
        for (unsigned int i = 0; i < _variables.size( ); i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                _newVariableIndex[i] = numberUnknownV;
                numberUnknownV++;
            }
            else
            {
                _newVariableIndex[i] = -1;
            }
        }
        return numberUnknownV;
    }



    bool OptimizationModel::optimize( )
    {
        Timer time;
        if (_showLog)
            printf( "\n    **************************PRE-SOLVER**************************\n\n" );

        Timer t;
        if (!_preprocessedModel || !_objectiveFunction.isUpdated( ))
        {
            bool feasible = update( );
            if (!feasible)
            {
                if (_showLog)
                {
                    printf( "\n    The model is INFEASIBLE!!\n" );
                }
                return false;
            }

            _preprocessedModel = true;
        }

        //Compute the re-index vector.
        _numberUnknowVariables = computeNewIndex( );

        //Scale objective function.
        {
            Timer t;
            if (_showLog)
            {
                printf( "\n    Scaling objective function...\n" );
            }

            scaleObjectiveFunction( _numberUnknowVariables );

            const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _variables.size( ); i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    double x = _variables[i].getValue( ) * _scale[_newVariableIndex[i]];
                    _variables[i].setValue( x );
                }
            }
            if (_showLog)
                t.printTime( "        Total time" );
        }

        _solver->setObjectiveFunction( &_objectiveFunction );
        _solver->setConstraints( &_constraints[0], _constraints.size( ) );
        _solver->setVariables( &_variables[0], _variables.size( ) );
        _solver->setReindexVector( &_newVariableIndex[0], _numberUnknowVariables );
        _solver->setScaleVector( &_scale[0] );
        _solver->showLog( _showLog );
        _solver->setEPS( _solverEps );
        _solver->setNumberThreads( _numberOfThreads );


        if (_showLog)
        {
            printf( "\n    %u variables have been removed from the model by pre-solver...\n", ( ( unsigned int ) _variables.size( ) - _numberUnknowVariables ) );
            printf( "    %u constraints have been removed from the model by pre-solver...\n", ( ( unsigned int ) _variables.size( ) - _numberUnknowVariables ) );

            t.printTime( "\n    Total pre-solver time " );
            printf( "\n    **************************OPTIMIZING**************************\n\n" );
            printf( "\n    Optimizing the model with %u variables, %u quadratic terms, and %u linear terms...\n\n",
                    ( unsigned int ) _variables.size( ), _objectiveFunction.size( ),
                    _objectiveFunction.getLinearExpression( ).size( ) );
        }

        bool status = _solver->optimize( );

        //Un-scaling objective function.
        {
            Timer t;
            if (_showLog)
            {
                printf( "\n    Un-scaling objective function...\n" );
            }

            unscaleObjectiveFunction( _numberUnknowVariables );

            const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );
            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _variables.size( ); i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    double x = _variables[i].getValue( ) / _scale[_newVariableIndex[i]];
                    _variables[i].setValue( x );
                }
            }

            if (_showLog)
                t.printTime( "        Total time" );
        }

        //Compute the value for that variables that was fixed in the preprocessing
        //time.
        if (_processEqualityConstrs)
        {
            _processEqualityConstrs->computeValuesForFixedVariables( );
        }
       
        if (_showLog)
            time.printTime( "        Optimization Processing Time" );
        
        return status;
    }



    void OptimizationModel::scaleObjectiveFunction( unsigned int n )
    {
        //Allocate the vector and initialize with 1.0.
        _scale.resize( n, 1.0 );
        //return;

        //Compute the scale.
        for (unsigned int i = 0; i < _objectiveFunction.size( ); i++)
        {
            //Get quadratic terms.
            Variable vi = _objectiveFunction.getVar1( i );
            Variable vj = _objectiveFunction.getVar2( i );
            double c = _objectiveFunction.getCoefficient( i );

            if (vi == vj)
            {
                unsigned int id = _newVariableIndex[vi.getIndex( )];
                _scale[id] = sqrt( c );
            }
        }

        //Scale quadratic terms.
        for (unsigned int i = 0; i < _objectiveFunction.size( ); i++)
        {
            //Get quadratic term.
            Variable vi = _objectiveFunction.getVar1( i );
            Variable vj = _objectiveFunction.getVar2( i );
            unsigned int idi = _newVariableIndex[vi.getIndex( )];
            unsigned int idj = _newVariableIndex[vj.getIndex( )];
            double c = _objectiveFunction.getCoefficient( i );

            //Compute the scale factor.
            c = c / ( _scale[idi] * _scale[idj] );

            //Define the new coefficient.
            _objectiveFunction.setCoefficient( i, c );
        }

        //Scale the linear terms.
        LinearExpression& l = _objectiveFunction.getLinearExpression( );
        for (unsigned int i = 0; i < l.size( ); i++)
        {
            //Get linear term.
            Variable v = l.getVariable( i );
            unsigned int id = _newVariableIndex[v.getIndex( )];
            double c = l.getCoefficient( i );

            //Compute the scale factor.
            c = c / ( _scale[id] );

            //Define the new coefficient.
            l.setCoefficient( i, c );
        }
    }



    void OptimizationModel::unscaleObjectiveFunction( unsigned int n )
    {
        //Scale quadratic terms.
        for (unsigned int i = 0; i < _objectiveFunction.size( ); i++)
        {
            //Get quadratic term.
            Variable vi = _objectiveFunction.getVar1( i );
            Variable vj = _objectiveFunction.getVar2( i );
            unsigned int idi = _newVariableIndex[vi.getIndex( )];
            unsigned int idj = _newVariableIndex[vj.getIndex( )];
            double c = _objectiveFunction.getCoefficient( i );

            //Compute the scale factor.
            c = c * ( _scale[idi] * _scale[idj] );

            //Define the new coefficient.
            _objectiveFunction.setCoefficient( i, c );
        }

        //Scale the linear terms.
        LinearExpression& l = _objectiveFunction.getLinearExpression( );
        for (unsigned int i = 0; i < l.size( ); i++)
        {
            //Get linear term.
            Variable v = l.getVariable( i );
            unsigned int id = _newVariableIndex[v.getIndex( )];
            double c = l.getCoefficient( i );

            //Compute the scale factor.
            c = c * ( _scale[id] );

            //Define the new coefficient.
            l.setCoefficient( i, c );
        }
    }



    void OptimizationModel::setObjectiveFunction( const QuadraticExpression& obj )
    {
        if (&_objectiveFunction != &obj)
            _objectiveFunction = obj;
    }



    void OptimizationModel::setOptimizationSolver( Solver::OptimizationSolver *solver )
    {
        _solver = solver;
    }



    unsigned int OptimizationModel::getNumberIterations( ) const
    {
        return _solver->getNumberIterations( );
    }



    unsigned int OptimizationModel::getMaxIterations( ) const
    {
        return _solver->getMaxIterations( );
    }



    unsigned int OptimizationModel::getStepsNumber( ) const
    {
        return _solver->getStepsNumber( );
    }



    void OptimizationModel::setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm, void *obj ), void *object )
    {
        _solver->setDisplayInformationCallback( displayInformation, object );
    }



    void OptimizationModel::setMaxIterations( const unsigned int maxIter )
    {
        _solver->setMaxIterations( maxIter );
    }



    void OptimizationModel::setStepsNumber( unsigned int sn )
    {
        _solver->setStepsNumber( sn );
    }

} // namespace ModelManager
