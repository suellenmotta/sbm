#pragma once

#include <string>

#define VAR_INFINITY 1e100


namespace ModelManager
{
    class VariableRepresentation;

    class Variable
    {
    private:
        /**
         * Object to represent the allocated variable.
         */
        VariableRepresentation* _var;

    private:

        /**
         * The constructor of a variable that receives its limits and name.
         * @param index - variable index.
         * @param name - variable name.
         * @param lb - the lower bound of the variable.
         * @param up - the upper bound of the variable.
         */
        Variable( unsigned int index, std::string name, double lb, double up );

        /**
         * Destroy the object.
         */
        void destroy( );

        /**
         * Define if the variable value is known or not.
         * @return - true if the variable is known and false otherwise.
         */
        void knownValue( const bool known );
    public:

        friend class Model;
        friend class PreprocessingSimpleConstraints;

        /**
         * The default constructor.
         */
        Variable( );

        /**
         * The copy constructor.
         * @param v - the variable used to create a new variable.
         */
        Variable( const Variable& v );

        /**
         * The destructor.
         */
        ~Variable( );

        /**
         * Get the variable index.
         * @return - the variable index.
         */
        unsigned int getIndex( ) const;

        /**
         * Get the variable lower bound value.
         * @return - the variable lower bound value.
         */
        double getLowerBound( ) const;

        /**
         * Get the variable name.
         * @return - the variable name.
         */
        const std::string getName( ) const;

        /**
         * Get the variable upper bound value.
         * @return - the variable upper bound value.
         */
        double getUpperBound( ) const;

        /**
         * Get the current value of the variable.
         * @return - the current value of the variable.
         */
        double getValue( ) const;

        /**
         * Return if the variable has a known value.
         * @return - true if the variable value has a known value and false
         * otherwise.
         */
        bool isKnownValue( ) const;

        /**
         * The overload of the operator = to make a copy of the rhs variable to
         * the current variable.
         * @param v - the rhs variable.
         * @return - the current variable.
         */
        Variable operator=( const Variable& v);

        /**
         * The operator's = overload to set a new variable value.
         * @param value - new variable value.
         */
        void operator=( const double value );

        /**
         * The operator's = overload to set a new variable name.
         * @param name - new variable name.
         */
        void operator=( const std::string& name);

        /**
         * Compare to variables.
         * @param v - variable to be compared with the current variable.
         * @return - true if both are equal and false otherwise.
         */
        bool operator==( const Variable& v) const;

        /**
         * Compare to variables.
         * @param v - variable to be compared with the current variable.
         * @return - false if both are equal and true otherwise.
         */
        bool operator!=( const Variable& v) const;

        /**
         * Compare to variables.
         * @param v - variable to be compared with the current variable.
         * @return - true if current variable is less that the variable v.
         */
        bool operator<( const Variable& v) const;

        /**
         * Cast from variable do value.
         * @return -  variable's value.
         */
        explicit operator double()
        {
            return getValue();
        }

        /**
         * Define a new lower bound to variable.
         * @param lb - new lower bound.
         */
        void setLowerBound( double lb );

        /**
         * Define a new name to variable.
         * @param name - new name.
         */
        void setName( const std::string &name );

        /**
         * Set a new upper bound to variable.
         * @param up - new upper bound.
         */
        void setUpperBound( double up );

        /**
         * Set a new current value to variable.
         * @param value - new value.
         */
        void setValue( double value );
    };

} // namespace ModelManager
