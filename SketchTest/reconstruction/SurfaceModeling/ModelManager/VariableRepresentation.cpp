#include <vector>

#include "VariableRepresentation.h"

namespace ModelManager
{



    VariableRepresentation::VariableRepresentation( unsigned int index, std::string name, double lb, double up )
    {
        _index = index;
        _lowerBound = lb;
        _upperBound = up;
        _name = name;
        _value = 0.0;
        _knownValue = false;
    }



    unsigned int VariableRepresentation::getIndex( ) const
    {
        return _index;
    }



    double VariableRepresentation::getLowerBound( ) const
    {
        return _lowerBound;
    }



    const std::string& VariableRepresentation::getName( ) const
    {
        return _name;
    }



    double VariableRepresentation::getUpperBound( ) const
    {
        return _upperBound;
    }



    double VariableRepresentation::getValue( ) const
    {
        return _value;
    }



    bool VariableRepresentation::isKnownValue( ) const
    {
        return _knownValue;
    }



    void VariableRepresentation::knownValue( const bool known )
    {
        _knownValue = known;
    }



    void VariableRepresentation::setLowerBound( double lb )
    {
        _lowerBound = lb;
    }



    void VariableRepresentation::setName( const std::string &name )
    {
        _name = name;
    }



    void VariableRepresentation::setUpperBound( double up )
    {
        _upperBound = up;
    }



    void VariableRepresentation::setValue( double value )
    {
        _value = value;
    }
} // namespace ModelManager
