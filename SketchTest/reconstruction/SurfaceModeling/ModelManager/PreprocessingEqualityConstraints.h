#pragma once

#include <vector>

namespace ModelManager
{
    class LinearExpression;
    class QuadraticExpression;

    class PreprocessingEqualityConstraints
    {
    private:
        /**
         * Augmented rank matrix that must be computed using the matrix in the
         * row reduced form.
         */
        unsigned int _augmentedRankMatrix;

        /**
         * Vector with all equality constraints.
         */
        std::vector<LinearExpression*> _equalityConstraints;

        /**
         * Normal rank matrix that must be computed using the matrix in the row
         * reduced.
         */
        unsigned int _normalRankMatrix;

        /**
         * The number of threads that must to be used.
         */
        unsigned int _numberOfThreads;

        /**
         * Pivots used in the row reduced form to equality constraints
         * pre-processing. The vector is always sorted, because the the elements
         * are inserted in order.
         */
        std::vector<Variable> _pivots;

    private:
        /**
         * From row reduced form, build the solution to preprocessing equality
         * constraints.
         */
        void buildSolution( );

        /**
         * Compute the matrix rank.
         * @param c - current matrix.
         */
        void computeRank( );

        /**
         * Find the next line to pivot v, if it exist.
         * @param line - current line.
         * @param v - variable to pivot.
         * @return true in case to find the next line and false otherwise.
         */
        bool findNextLine( int line, const Variable v );

        /**
         * Find an specific variable v on a linear expression l.
         * @param l - linear expression.
         * @param v - variable.
         * @return - the variable index or -1 if the variable does not exist.
         */
        int findVariable( const LinearExpression* l, const Variable v ) const;

        /**
         * Build a vector with just used variables on constraints.
         * @param usedVars - used variables.
         */
        void getVariablesUsedOnConstraints( std::vector<Variable>& usedVars );

        /**
         * Search a pivot on pivot vectors.
         * @param v - variable.
         * @return - the vector index or -1 if the variable does not exist in the
         * vector.
         */
        int searchPivot( const Variable v ) const;

        /**
         * Simplify the objective function with the after the preprocessing step.
         * @param obj - objective function to be simplified.
         */
        void processObjectiveFunction( QuadraticExpression* obj ) const;

        /**
         * Put the matrix on row reduced form.
         * @param vars - a vector with all variables sorted by index
         */
        void rowReducedForm( const std::vector<Variable>& vars );

    public:

        /**
         * Default constructor.
         * @param c - all equality constraints.
         * @param numberOfThreads - the number of threads that must to be used.
         */
        PreprocessingEqualityConstraints( std::vector<LinearExpression*>& c,
                                          unsigned int numberOfThreads );

        /**
         * Destructor.
         */
        ~PreprocessingEqualityConstraints( );

        /**
         * After optimization problem be solved, compute the values for that
         * variables that was fixed on preprocessing step.
         */
        void computeValuesForFixedVariables( );

        /**
         * Get the augmented matrix rank.
         * @return - the augmented matrix rank.
         */
        unsigned int getAugmentedMatrixRank( ) const;

        /**
         * Get the normal matrix rank.
         * @return - the augmented matrix rank.
         */
        unsigned int getNomalMatrixRank( ) const;

        /**
         * Get the pivots.
         * @return - the vector with the used pivots.
         */
        std::vector<Variable>& getPivots( );

        /**
         * Preprocessing the equality constraints and fix freedom degrees.
         * @param obj - objective function.
         * @return - true if the problem is possible and false otherwise.
         */
        bool processingEqualityConstraints( QuadraticExpression* obj );
    };

} // namespace ModelManager
