#pragma once

#include <string>

namespace ModelManager
{

    class VariableRepresentation
    {

    public:
        friend class Variable;

    private:
        /**
         * The variable index.
         */
        unsigned int _index;

        /**
         * Indicates if the variable already has a known value.
         */
        bool _knownValue;

        /**
         * The variable lower bound.
         */
        double _lowerBound;

        /**
         * The variable name.
         */
        std::string _name;

        /**
         * The variable upper bound.
         */
        double _upperBound;

        /**
         * The current variable value.
         */
        double _value;

    private:

        /**
         * The constructor of a variable that receives its limits and name.
         * @param index - variable index.
         * @param name - variable name.
         * @param lb - the lower bound of the variable.
         * @param up - the upper bound of the variable.
         */
        VariableRepresentation( unsigned int index, std::string name, double lb, double up );

        /**
         * Get the variable index.
         * @return - the variable index.
         */
        unsigned int getIndex( ) const;

        /**
         * Get the variable lower bound value.
         * @return - the variable lower bound value.
         */
        double getLowerBound( ) const;

        /**
         * Get the variable name.
         * @return - the variable name.
         */
        const std::string& getName( ) const;

        /**
         * Get the variable upper bound value.
         * @return - the variable upper bound value.
         */
        double getUpperBound( ) const;

        /**
         * Get the current value of the variable.
         * @return - the current value of the variable.
         */
        double getValue( ) const;

        /**
         * Return if the variable has a known value.
         * @return - true if the variable value has a known value and false
         * otherwise.
         */
        bool isKnownValue( ) const;

        /**
         * Define if the variable value is known or not.
         * @return - true if the variable is known and false otherwise.
         */
        void knownValue( const bool known );

        /**
         * Define a new lower bound to variable.
         * @param lb - new lower bound.
         */
        void setLowerBound( double lb );

        /**
         * Define a new name to variable.
         * @param name - new name.
         */
        void setName( const std::string &name );

        /**
         * Set a new upper bound to variable.
         * @param up - new upper bound.
         */
        void setUpperBound( double up );

        /**
         * Set a new current value to variable.
         * @param value - new value.
         */
        void setValue( double value );
    };
} // namespace ModelManager
