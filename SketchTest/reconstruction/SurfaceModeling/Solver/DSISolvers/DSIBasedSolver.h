#pragma once

#include "../Solver.h"
#include "../../Graph/DSIGraph.h"

namespace ModelManager
{
    class DSIModel;
}
namespace Solver
{

    class DSIBasedSolver : public Solver
    {

    protected:
        /**
         * A graph to be used by solver to require topological infomation.
         */
        Graph::DSIGraph* _graph;
    private:
        /**
         * Get the graph. This function can be accessed just by the DSIModel
         * class.
         * @return - the graph.
         */
        Graph::DSIGraph* getGraph( ) const;

        /**
         * Define a new graph to be used by the solver. This function can be
         * accessed just by the DSIModel class.
         */
        void setGraph( Graph::DSIGraph *g );
    public:
        
        friend class ModelManager::DSIModel;
        
        /**
         * Destructor.
         */
        virtual ~DSIBasedSolver();
        
         /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        virtual bool optimize( ) = 0;
    };
} // namespace Solver

