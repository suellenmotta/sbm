#include "PseudoConjugateGradientDSIBasedSolver.h"
#include "../../../Timer/Timer.h"

namespace Solver
{



    bool PseudoConjugateGradientDSIBasedSolver::optimize( )
    {
        //Start the time.
        Timer finaltime;

        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );
        
        return false;
    }
} // namespace Solver
