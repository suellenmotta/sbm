#pragma once

#include "../../ModelManager/LinearExpression.h"
#include "../Solver.h"

namespace ModelManager
{
    class OptimizationModel;
    class QuadraticExpression;
}

namespace Solver
{

    class OptimizationSolver : public Solver
    {
    protected:
        /**
         * The gradient vector.
         */
        std::vector<ModelManager::LinearExpression> _gradient;

        /**
         * Unknown variable indexes.
         */
        unsigned int *_newVariableIndex;

        /**
         * Number of unknown variables.
         */
        unsigned int _numberUnknowVariables;

        /**
         * The objective function to be minimized.
         */
        ModelManager::QuadraticExpression *_objectiveFunction;

    protected:
        /**
         * Compute the gradient vector.
         * @param obj - objective function.
         * @param n - the number of variables on objective function.
         */
        void computeGradient( const ModelManager::QuadraticExpression* obj, unsigned int n );

        /**
         * Compute the gradient values at d-component.
         * @param d - d-component to compute the gradient values.
         * @param gs - scaled gradient value.
         * @param g - non-scaled gradient value.
         */
        void computeGradientValues( unsigned int d, double& gs, double& g ) const;

        /**
         * Compute the best step t to move on the opposite gradient direction.
         * @param obj - objective function.
         * @param direction - direction to move.
         * @return 
         */
        double exactLineSearch( const ModelManager::QuadraticExpression& obj, const std::vector<double>& direction ) const;

        /**
         * Initialize the optimization solver. This function must be called at 
         * the very first line of the optimize function.
         * In this function, the follow operations are performed:
         *      - compute new index vector for variables.
         */
        void initializeOptimizationSolver( );

        /**
         * Show the current solution.
         * @param gnorm - gradient norm.
         * @return - false if the solver must be stopped and true if it must to
         * continue.
         */
        bool showCurrentSolution( const double gnorm = 0.0 );
    private:
        /**
         * Vector to scale the objective function.
         */
        double *_scale;

    private:
        /**
         * Get the objective function. Jus the OptimizationModel is allowed to
         * access this function.
         * @return - the objective function.
         */
        ModelManager::QuadraticExpression* getObjectiveFunction( ) const;

        /**
         * Define a new objective function to be minimized. Jus the
         * OptimizationModel is allowed to access this function.
         * @param obj - new objective function to be minimized
         */
        void setObjectiveFunction( ModelManager::QuadraticExpression* obj );

        /**
         * Set the vector that re-index the variables.
         * @param newIndex - vector to re-index the variables.
         * @param numberUnknownVariables - number of unknown variables.
         */
        void setReindexVector( unsigned int* newIndex, unsigned int numberUnknownVariables );

        /**
         * Define the scale vector.
         * @param scaleVector - scale vector.
         */
        void setScaleVector( double *scaleVector );
    public:
        /**
         * Set OptimizationModel to access the private information
         */
        friend class ModelManager::OptimizationModel;

        /**
         * Default constructor.
         */
        OptimizationSolver( );

        /**
         * Destructor.
         */
        virtual ~OptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        virtual bool optimize( ) = 0;
    };
} // namespace Solver
