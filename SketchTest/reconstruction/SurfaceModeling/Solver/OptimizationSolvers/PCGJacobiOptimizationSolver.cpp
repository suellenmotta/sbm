#include "PCGJacobiOptimizationSolver.h"
#include "../../ModelManager/Variable.h"
#include "../../ModelManager/QuadraticExpression.h"
#include "../../../Timer/Timer.h"
#include <cstdio>
#include <cmath>
#include <omp.h>

using namespace ModelManager;

namespace Solver
{



    PCGJacobiOptimizationSolver::PCGJacobiOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 5000;
        _eps = 1e-4;
    }



    double PCGJacobiOptimizationSolver::computeInitialResidual( std::vector<double>& p,
                                                                std::vector<double>& q,
                                                                std::vector<double>& r ) const
    {
        //Defining how many threads will be used
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        //Fill x0 vector
        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _numberVariables; i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                unsigned int idx = _variables[i].getIndex( );
                idx = _newVariableIndex[idx];
                p[idx] = _variables[i].getValue( );
            }
        }

        //Matrix and Vector multiplication giving q = Ax.
        computeMatrixVectorMultiplication( p, q );

        double dotRR = 0.0;
        #pragma omp parallel for reduction(+:dotRR)
        for (unsigned int i = 0; i < _numberUnknowVariables; i++)
        {
            r[i] = -q[i];
            dotRR += r[i] * r[i];
        }

        //Get the linear expression from objective functions. This linear expression implicitly
        //represents the b vector.
        const LinearExpression& linear = _objectiveFunction->getLinearExpression( );

        //Compute initial residual.
        #pragma omp parallel for reduction(+:dotRR) 
        for (unsigned int i = 0; i < linear.size( ); i++)
        {
            //Get the global variable index.
            unsigned int idx = linear.getVariable( i ).getIndex( );

            //Get local variable index.
            idx = _newVariableIndex[idx];
            
            double aux = -r[idx];
            
            dotRR += aux;

            //Compute initial residual vector.
            r[idx] += -linear.getCoefficient( i );

            //Sum new dot value for this position.
            dotRR += r[idx] * r[idx];
        }

        return dotRR;
    }



    void PCGJacobiOptimizationSolver::computePreconditioner( std::vector<double>& m ) const
    {
        //Defining how many threads will be used
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        //Fill Jacobi preconditioner vector 
        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
        {
            //Catch global indexes
            unsigned int idx1 = _objectiveFunction->getVar1( i ).getIndex( );
            unsigned int idx2 = _objectiveFunction->getVar2( i ).getIndex( );

            if (idx1 == idx2)
            {
                unsigned int idx = _newVariableIndex[idx1];
                m[idx] = 2.0 * _objectiveFunction->getCoefficient( i );
            }
        }
    }



    double PCGJacobiOptimizationSolver::dotProduct( const std::vector<double>& v,
                                                    const std::vector<double>& w ) const
    {
        //Defining how many threads will be used
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        double sum = 0.0;
        #pragma omp parallel for reduction(+:sum)
        for (unsigned int i = 0; i < _numberUnknowVariables; i++)
        {
            sum += v[i] * w[i];
        }
        return sum;
    }



    void PCGJacobiOptimizationSolver::computeMatrixVectorMultiplication( const std::vector<double>& p,
                                                                         std::vector<double>& q ) const
    {
        //Defining how many threads will be used
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        //Initialization of q vector with zero.
        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _numberUnknowVariables; i++)
        {
            q[i] = 0.0;
        }

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
        {
            //Catch global index
            unsigned int idx1 = _objectiveFunction->getVar1( i ).getIndex( );
            unsigned int idx2 = _objectiveFunction->getVar2( i ).getIndex( );


            //Catch local index
            idx1 = _newVariableIndex[idx1];
            idx2 = _newVariableIndex[idx2];

            //Catch coefficient 
            double c = _objectiveFunction->getCoefficient( i );
            #pragma omp atomic
            q[idx1] += c * p[idx2];
            #pragma omp atomic   
            q[idx2] += c * p[idx1];
        }
    }



    bool PCGJacobiOptimizationSolver::optimize( )
    {
        //Start the time.
        Timer finaltime;
        if (_showLog)
            printf( "    Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        //Allocate auxiliary vectors.
        std::vector<double> p( _numberUnknowVariables, 0.0 );
        std::vector<double> q( _numberUnknowVariables, 0.0 );
        std::vector<double> r( _numberUnknowVariables, 0.0 );
        std::vector<double> m( _numberUnknowVariables, 0.0 );

        double dotRR = 0.0;
        double dotPQ = 0.0;
        double rho = 0.0, rhoOld = 0.0;
        rhoOld = rho = 1.0;

        if (_showLog)
            printf( "    Computing the initial residual...\n" );

        //Compute initial residual vector r.
        dotRR = computeInitialResidual( p, q, r );

        if (dotRR <= _eps * _eps)
        {
            return true;
        }

        //Print current solution.
        bool continueSolver = showCurrentSolution( sqrt( dotRR ) );

        if (_showLog)
            printf( "    Computing the preconditioner...\n" );

        //Fill Jacobi preconditioner vector.
        computePreconditioner( m );

        if (_showLog)
        {
            printf( "\n    Performing the PCG Method:\n" );
            printf( "    ITERATION           GRADIENT NORM\n" );
            printf( "    %8u            %e           \n", _iterations, sqrt( dotRR ) );
        }

        //Initialize vector that was used at initial residual calculation.
        p.assign( _numberUnknowVariables, 0.0 );
        
        //Defining how many threads will be used
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        while (_iterations++ <_maxIterations && dotRR > _eps * _eps && continueSolver)
        {
            if (_iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "    %8u            %e           \n", _iterations, sqrt( dotRR ) );
            }

            if (_iterations % _stepsNumber == 0)
            {
                //Print current solution.
                continueSolver = showCurrentSolution( sqrt( dotRR ) );
            }

            //Compute new rho.
            rhoOld = rho;
            rho = 0.0;
            #pragma omp parallel for reduction (+:rho)
            for (unsigned int i = 0; i < _numberUnknowVariables; i++)
            {
                rho += r[i] * ( 1.0 / m[i] ) * r[i];
            }

            //Compute p(k) with beta being also calculated.
            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _numberUnknowVariables; i++)
            {
                p[i] = r[i] / m[i] + ( rho / rhoOld ) * p[i];
            }

            //Compute q
            computeMatrixVectorMultiplication( p, q );

            //Update variable's values 
            dotPQ = dotProduct( p, q );
            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    unsigned int idx = _variables[i].getIndex( );
                    idx = _newVariableIndex[i];
                    _variables[i] = ( double ) _variables[i] + ( rho / dotPQ ) * p[idx];
                }
            }

            //Update residual vector.
            dotRR = 0.0;
            #pragma omp parallel for reduction (+:dotRR)
            for (unsigned int i = 0; i < _numberUnknowVariables; i++)
            {
                r[i] = r[i] - ( rho / dotPQ ) * q[i];
                dotRR += r[i] * r[i];
            }
        }

        if (_showLog)
            printf( "    %8u            %e           \n", _iterations, sqrt( dotRR ) );

        //Print current solution.
        continueSolver = showCurrentSolution( sqrt( dotRR ) );

        bool status = dotRR < _eps * _eps && _iterations < _maxIterations;
        if (_showLog)
        {
            if (status)
            {
                printf( "    ****OPTIMAL SOLUTION FOUND****\n" );
            }
            else
            {
                printf( "    ****OPTIMAL SOLUTION NOT FOUND****\n" );
            }
        }
        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );
        return status;
    }
} // namespace Solver
