#include <cstdio>
#include <glpk.h>

#include "../../ModelManager/Enums.h"
#include "../../ModelManager/LinearExpression.h"
#include "../../ModelManager/Constraint.h"
#include "../../../../libs/Timer/Timer.h"
#include "LagrangeMultiplierOptimizationSolver.h"
#include "lis.h"
#include <omp.h>
#include <algorithm>

using namespace ModelManager;

namespace Solver
{



    LagrangeMultiplierOptimizationSolver::LagrangeMultiplierOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 200;
        _numberOfSubIterations = 5000;
        _eps = 1e-5;
        _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    }



    void LagrangeMultiplierOptimizationSolver::preprocessingCSRFormat( unsigned int numberUnknownV,
                                                                       std::vector<double>* b,
                                                                       std::vector<Element>* elements ) const
    {
        b->resize( numberUnknownV + _numberConstraints );
        for (unsigned int g = 0; g < _gradient.size( ); g++)
        {
            for (unsigned int v = 0; v < _gradient[g].size( ); v++)
            {
                unsigned int id = _gradient[g].getVariable( v ).getIndex( );
                id = _newVariableIndex[id];

                Element e;
                e.i = g;
                e.j = id;
                e.value = _gradient[g].getCoefficient( v );
                elements->push_back( e );
            }
            b->at( g ) += -_gradient[g].getConstant( );
        }

        for (unsigned int c = 0; c < _numberConstraints; c++)
        {
            const LinearExpression& l = _constraints[c].getLine( );
            for (unsigned int v = 0; v < l.size( ); v++)
            {
                unsigned int id = l.getVariable( v ).getIndex( );
                id = _newVariableIndex[id];

                Element e;
                e.i = id;
                e.j = numberUnknownV + c;
                e.value = l.getCoefficient( v );
                elements->push_back( e );

                e.i = numberUnknownV + c;
                e.j = id;
                elements->push_back( e );
            }
            b->at( numberUnknownV + c ) += -l.getConstant( );
        }

        std::sort( elements->begin( ), elements->end( ) );
    }



    unsigned int LagrangeMultiplierOptimizationSolver::getNumberOfSubIterations( ) const
    {
        return _numberOfSubIterations;
    }



    unsigned int LagrangeMultiplierOptimizationSolver::getNumberOfThreads( ) const
    {
        return _numberOfThreads;
    }



    bool LagrangeMultiplierOptimizationSolver::optimize( )
    {
        //Start the time.
        Timer t;

        if (_showLog)
            printf( "Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        if (_showLog)
        {
            printf( "%u variables have been removed from the model...\n", ( _numberVariables - _numberUnknowVariables ) );
            printf( "%u constraints have been removed from the model...\n", ( _numberVariables - _numberUnknowVariables ) );
        }

        if (_showLog)
            printf( "Computing the gradient vector...\n" );

        computeGradient( _objectiveFunction, _numberUnknowVariables );

        //Preprocessing the elements to put on a CSR format.
        std::vector<Element> *elements = new std::vector<Element>( );
        std::vector<double> *bVector = new std::vector<double>( );
        preprocessingCSRFormat( _numberUnknowVariables, bVector, elements );
        unsigned int numberNonZero = elements->size( );

        for (unsigned int i = 0; i < numberNonZero; i++)
        {
            printf( "(%u, %u) = %.1lf\n", elements->at( i ).i, elements->at( i ).j, elements->at( i ).value );
        }

        //Initialize the LIS.
        LIS_INT argc = 0;
        char **argv;
        lis_initialize( &argc, &argv );

        if (_showLog)
            printf( "Building b vector..\n" );

        unsigned int totalVariables = _numberUnknowVariables + _numberConstraints;

        //Create b vector.
        LIS_VECTOR b;
        lis_vector_create( 0, &b );
        lis_vector_set_size( b, 0, totalVariables );

        double bNorm = 0.0;

        //Set the b values.
        for (unsigned int i = 0; i < bVector->size( ); i++)
        {
            lis_vector_set_value( LIS_INS_VALUE, i, ( LIS_SCALAR ) bVector->at( i ), b );
            bNorm += bVector->at( i ) * bVector->at( i );
        }
        delete bVector;

        lis_vector_print( b );

        //Allocate the memory to store the matrix.
        LIS_INT *ptr, *index;
        LIS_SCALAR *value;
        LIS_MATRIX A;
        lis_matrix_malloc_csr( totalVariables, numberNonZero, &ptr, &index, &value );
        lis_matrix_create( 0, &A );
        lis_matrix_set_size( A, 0, totalVariables );

        //Build the CSR format vectors.
        unsigned int p = 0;
        unsigned int currentElement = 0;
        ptr[p] = currentElement;
        for (unsigned int i = 0; i < elements->size( ); i++)
        {
            printf( "%u: (%u, %u, %.0lf\n", i, elements->at( i ).i, elements->at( i ).j, elements->at( i ).value );
            if (p != elements->at( i ).i)
            {
                p = elements->at( i ).i;
                ptr[p] = currentElement;
            }
            index[currentElement] = elements->at( i ).j;
            value[currentElement] = elements->at( i ).value;
            currentElement++;
        }
        ptr[p + 1] = numberNonZero;

        for (unsigned int i = 0; i <= totalVariables; i++)
        {
            printf( "%3d ", ( int ) ptr[i] );
        }
        printf( "\n" );
        for (unsigned int i = 0; i < currentElement; i++)
        {
            printf( "%3d ", ( int ) index[i] );
        }
        printf( "\n" );
        for (unsigned int i = 0; i < currentElement; i++)
        {
            printf( "%.1lf ", value[i] );
        }
        printf( "\n" );

        //Free memory.
        delete elements;


        lis_matrix_set_csr( numberNonZero, ptr, index, value, A );
        lis_matrix_assemble( A );

        //Create solver.
        LIS_VECTOR x;
        lis_vector_create( 0, &x );
        lis_vector_set_size( x, 0, totalVariables );

        if (_showLog)
            printf( "Setting an initial solution...\n" );

        for (unsigned int i = 0, v = 0; i < totalVariables; i++)
        {
            lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) i, 0.0, x );
            if (i < _numberVariables)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) v, ( LIS_SCALAR ) _variables[i].getValue( ), x );
                    v++;
                }
            }
        }

        lis_vector_print( x );

        if (_showLog)
            printf( "Solving the linear system Qx = -b using %u threads and tol = %.2e...\n", _numberOfThreads, _eps );

        LIS_SOLVER solver;
        lis_solver_create( &solver );

        char option[1024];
        sprintf( option, "-i cg -p jacobi -maxiter %u -initx_zeros %d -conv_cond %d"
                 " -tol %e -omp_num_threads %u -print out%c", _numberOfSubIterations, 0, 0, 1e-12, _numberOfThreads, '\0' );

        double gnorm = 0.0;
        for (unsigned int g = 0; g < _gradient.size( ); g++)
        {
            double c = _gradient[g].getValue( );
            gnorm += c * c;
        }

        for (unsigned int i = 0; i < _numberConstraints; i++)
        {
            double c = _constraints[i].getLine( ).getValue( );
            gnorm += c * c;
        }

        //Print current solution.
        bool continueSolver = showCurrentSolution( sqrt( gnorm ) );

        if (_showLog)
            printf( "\n\nITERATION           GRADIENT NORM\n" );

        double time, itime, ptime, pCtime, pItime;
        double linearSolverTime = 0.0, preConditionatorTime = 0.0;
        _iterations = 0;
        while (gnorm > _eps * _eps && _iterations++ < _maxIterations && continueSolver)
        {
            if (_showLog && _iterations % _stepsNumber == 0)
            {
                printf( "%8u            %e           \n", _iterations, sqrt( gnorm ) );
            }

            if (_iterations % _stepsNumber == 0)
            {
                //Print current solution.
                continueSolver = showCurrentSolution( sqrt( gnorm ) );
            }

            lis_solver_set_option( option, solver );
            lis_solve( A, b, x, solver );
            lis_solver_get_timeex( solver, &time, &itime, &ptime, &pCtime, &pItime );
            linearSolverTime += itime;
            preConditionatorTime += ptime;
            lis_vector_print( x );
            lis_vector_print( b );

            for (unsigned int i = 0, v = 0; i < _numberVariables; i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    LIS_SCALAR s;
                    lis_vector_get_value( x, v, &s );
                    _variables[i].setValue( s );
                    v++;
                }
            }

            gnorm = 0.0;
            for (unsigned int g = 0; g < _gradient.size( ); g++)
            {
                double c = _gradient[g].getValue( );
                gnorm += c * c;
            }

            for (unsigned int i = 0; i < _numberConstraints; i++)
            {
                double c = _constraints[i].getLine( ).getValue( );
                gnorm += c * c;
            }
        }

        if (_showLog)
            printf( "%8u            %e           \n\n\n", _iterations, sqrt( gnorm ) );

        //Print current solution.
        showCurrentSolution( sqrt( gnorm ) );

        LIS_INT nsol;
        char solvername[128];
        lis_solver_get_solver( solver, &nsol );
        lis_solver_get_solvername( nsol, solvername );

        bool status = !( gnorm > _eps * _eps || _iterations > _maxIterations );

        if (_showLog)
        {
            printf( "Preconditioner       = %e sec.\n", preConditionatorTime );
            printf( "linear solver (%s)   = %e sec.\n", solvername, linearSolverTime );
            printf( "Total Solver Time:   = %e sec.\n", t.elapsed( ) );
            if (!status)
            {
                printf( "****Optimal Solution NOT Found\n" );
            }
            else
            {
                printf( "****OPTIMAL SOLUTION FOUND****\n" );
            }
        }

        for (unsigned int i = 0, v = 0; i < _numberVariables; i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                LIS_SCALAR s;
                lis_vector_get_value( x, v, &s );
                _variables[i].setValue( s );
                v++;
            }
        }

        lis_solver_destroy( solver );
        lis_vector_destroy( x );
        lis_vector_destroy( b );
        lis_matrix_destroy( A );

        lis_finalize( );
        
        return status;
    }



    void LagrangeMultiplierOptimizationSolver::setNumberOfSubIterations( unsigned int i )
    {
        _numberOfSubIterations = i > 0 ? i : 100;
    }



    void LagrangeMultiplierOptimizationSolver::setNumberOfThreads( unsigned int nt )
    {
        _numberOfThreads = std::min( nt, _numberOfThreads + 2 );
    }
} // namespace Solver
