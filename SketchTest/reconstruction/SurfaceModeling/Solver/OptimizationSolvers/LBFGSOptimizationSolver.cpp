#include "LBFGSOptimizationSolver.h"
#include "../../ModelManager/Variable.h"
#include "../../../Timer/Timer.h"
#include <omp.h>
#include <cstdio>
#include <cmath>

using namespace ModelManager;

namespace Solver
{



    LBFGSOptimizationSolver::LBFGSOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 500000;
        _kmax = 10;
    }



    void LBFGSOptimizationSolver::computeDirection( const std::vector<std::vector<double> >& w,
                                                    const std::vector<std::vector<double> >& v,
                                                    const std::vector<double>& g,
                                                    const unsigned int iteration,
                                                    std::vector<double>& direction )
    {
        unsigned int dimension = g.size( );
        std::vector<double> rhsv( g );

        unsigned int last = 0, size = 0;
        if (iteration < _kmax)
        {
            last = iteration - 1;
            size = iteration;
        }
        else
        {
            last = ( iteration - 1 ) % _kmax;
            size = _kmax;
        }

        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );
        std::vector<double> sum( numThreads, 0.0 );

        int k = last;
        //Compute the right matrix product.
        for (unsigned int i = 0; i < size; i++)
        {
            double wd = 0.0;
            #pragma omp parallel for reduction(+:wd)
            for (unsigned int d = 0; d < dimension; d++)
            {
                unsigned int tr = omp_get_thread_num( );
                wd += w[k][d] * rhsv[d];
            }

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int d = 0; d < dimension; d++)
            {
                rhsv[d] += wd * v[k][d];
            }
            k = ( k + size - 1 ) % size;
        }

        //Compute the left matrix product.
        for (unsigned int i = 0; i < size; i++)
        {
            k = ( k + 1 ) % size;
            double vd = 0.0;
            #pragma omp parallel for reduction(+:vd)
            for (unsigned int d = 0; d < dimension; d++)
            {
                unsigned int tr = omp_get_thread_num( );
                vd += v[k][d] * rhsv[d];
            }

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int d = 0; d < dimension; d++)
            {
                rhsv[d] += vd * w[k][d];
            }
        }

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int d = 0; d < dimension; d++)
        {
            direction[d] = -rhsv[d];
        }
    }



    double LBFGSOptimizationSolver::updateDG( std::vector<double>& deltaG, const std::vector<double>& grad,
                                              const std::vector<double>& deltaX, const std::vector<double> & direction,
                                              double& dDg, double& dxG, double& dxDg )const
    {
        dDg = 0.0;
        dxG = 0.0;
        dxDg = 0.0;
        double gnorm = 0.0;
        unsigned int dimension = _gradient.size( );

        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        #pragma omp parallel for reduction (+:dDg) reduction (+:dxG) reduction (+:dxDg) reduction (+:gnorm)
        for (unsigned int d = 0; d < dimension; d++)
        {
            double g = 0.0, gs = 0.0;
            computeGradientValues( d, gs, g );

            double newG = gs;
            deltaG[d] = newG - grad[d];
            dDg += direction[d] * deltaG[d];
            dxG += deltaX[d] * grad[d];
            dxDg += deltaX[d] * deltaG[d];
            gnorm += g * g;
        }

        return gnorm;
    }



    void LBFGSOptimizationSolver::updateVariablesValues( std::vector<double>& deltaX, const std::vector<double> & direction, const double t )
    {
        //Compute new x and dx values.
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _numberVariables; i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                unsigned int d = _newVariableIndex[_variables[i].getIndex( )];
                double x = _variables[i].getValue( );
                double newX = x + t * direction[d];
                deltaX[d] = newX - x;
                _variables[i].setValue( newX );

            }
        }
    }



    void LBFGSOptimizationSolver::updateVWVectors( const double t, std::vector<double>&v, std::vector<double>& w,
                                                   const std::vector<double>& deltaG, std::vector<double>& grad,
                                                   const std::vector<double>& deltaX, const double dDg, const double dxG,
                                                   const double dxDg ) const
    {
        unsigned int dimension = _gradient.size( );

        //Compute w^k and v^k.
        const double constant = 1 + t * sqrt( fabs( dDg / dxG ) );
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int d = 0; d < dimension; d++)
        {
            double newG = grad[d] + deltaG[d];
            v[d] = grad[d] * constant - newG;
            w[d] = deltaX[d] / dxDg;

            //Update the gradient.
            grad[d] = newG;
        }
    }



    bool LBFGSOptimizationSolver::optimize( )
    {
        //Start the time.
        Timer finaltime;

        if (_showLog)
            printf( "    Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        if (_showLog && _gradient.size( ) == 0)
            printf( "    Computing the gradient vector...\n" );

        computeGradient( _objectiveFunction, _numberUnknowVariables );

        std::vector<std::vector<double> > w, v;
        std::vector<double> grad( _numberUnknowVariables ), deltaX( _numberUnknowVariables );
        std::vector<double> deltaG( _numberUnknowVariables ), direction( _numberUnknowVariables );
        w.resize( _kmax );
        v.resize( _kmax );
        for (unsigned int i = 0; i < _kmax; i++)
        {
            w[i].resize( _numberUnknowVariables );
            v[i].resize( _numberUnknowVariables );
        }
        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        double gnorm = 0.0;
        #pragma omp parallel for reduction (+:gnorm)
        for (unsigned int d = 0; d < _numberUnknowVariables; d++)
        {
            double g = 0.0, gs = 0.0;
            computeGradientValues( d, gs, g );
            grad[d] = gs;

            direction[d] = -grad[d];

            gnorm += g * g;
        }

        //Print current solution.
        bool continueSolver = showCurrentSolution( sqrt( gnorm ) );

        if (_showLog)
        {
            printf( "\n    Performing the L-BFGS Method:\n" );
            printf( "    ITERATION           GRADIENT NORM\n" );
            printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );
        }
        double t = 0.0;
        double dDg = 0.0, dxG = 0.0, dxDg = 0.0;

        if (gnorm > _eps * _eps && continueSolver)
        {
            //Optimize the function on opposite gradient direction.
            t = exactLineSearch( *_objectiveFunction, direction );

            //Update variables values and the delta X vector.
            updateVariablesValues( deltaX, direction, t );

            //Compute new gradient and dg values.
            gnorm = updateDG( deltaG, grad, deltaX, direction, dDg, dxG, dxDg );

            //Update the vw vectors and the gradient..
            updateVWVectors( t, v[0], w[0], deltaG, grad, deltaX, dDg, dxG, dxDg );
        }

        while (_iterations++ <_maxIterations && gnorm > _eps * _eps && continueSolver)
        {
            if (_iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );
            }

            if (_iterations % _stepsNumber == 0)
            {
                //Print current solution.
                continueSolver = showCurrentSolution( sqrt( gnorm ) );
            }

            //Compute the new direction vector.
            computeDirection( w, v, grad, _iterations, direction );

            //Perform the exact line search.
            t = exactLineSearch( *_objectiveFunction, direction );

            updateVariablesValues( deltaX, direction, t );

            //Compute new gradient and dg values.
            gnorm = updateDG( deltaG, grad, deltaX, direction, dDg, dxG, dxDg );

            unsigned int k = _iterations % _kmax;
            //Update the vw vectors and the gradient..
            updateVWVectors( t, v[k], w[k], deltaG, grad, deltaX, dDg, dxG, dxDg );
        }

        if (_showLog)
            printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );

        //Print current solution.
        continueSolver = showCurrentSolution( sqrt( gnorm ) );

        bool status = gnorm < _eps * _eps && _iterations < _maxIterations;
        if (_showLog)
        {
            if (status)
            {
                printf( "    ****OPTIMAL SOLUTION FOUND****\n" );
            }
            else
            {
                printf( "    ****OPTIMAL SOLUTION NOT FOUND****\n" );
            }
        }
        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );

        return status;
    }
} // namespace Solver
