#include "ConjugateGradientLISOptimizationSolver.h"
#include "../../ModelManager/Variable.h"
#include "../../ModelManager/QuadraticExpression.h"
#include "../../Timer/Timer.h"
#include "lis.h"
#include <omp.h>
#include <cstdio>
#include <cmath>
#include <cstring>

using namespace ModelManager;

namespace Solver
{



    ConjugateGradientLISOptimizationSolver::ConjugateGradientLISOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 50;
        _numberOfSubIterations = 5000;
        _eps = 1e-5;
        _numberOfThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    }



    void ConjugateGradientLISOptimizationSolver::preprocessingCSRFormat( LIS_INT* ptr, LIS_INT* index, LIS_SCALAR* value ) const
    {
        //Initialize vector.
        memset( ptr, 0, ( _numberUnknowVariables + 1 ) * sizeof (LIS_INT ) );

        //Compute the number of element by row.
        unsigned int numberNonZero = 2 * _objectiveFunction->size( ) - _numberUnknowVariables;

        for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
        {
            Variable vi = _objectiveFunction->getVar1( i );
            Variable vj = _objectiveFunction->getVar2( i );
            unsigned int idi = _newVariableIndex[vi.getIndex( )];
            unsigned int idj = _newVariableIndex[vj.getIndex( )];
            if (idi == idj)
            {
                ptr[idi]++;
            }
            else
            {
                ptr[idi]++;
                ptr[idj]++;
            }
        }

        //Vector to know about next free position on vectors.
        std::vector<unsigned int> freePosition( _numberUnknowVariables, 0 );

        ptr[_numberUnknowVariables] = numberNonZero;
        //Compute free positions.
        for (int i = ( int ) _numberUnknowVariables - 1; i >= 0; i--)
        {
            ptr[i] = ptr[i + 1] - ptr[i];
            freePosition[i] = ptr[i];
        }

        //Fill the values.
        for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
        {
            Variable vi = _objectiveFunction->getVar1( i );
            Variable vj = _objectiveFunction->getVar2( i );
            double c = _objectiveFunction->getCoefficient( i );
            unsigned int idi = _newVariableIndex[vi.getIndex( )];
            unsigned int idj = _newVariableIndex[vj.getIndex( )];
            if (idi == idj)
            {
                unsigned int rowColumn = freePosition[idi];
                index[rowColumn] = idi;
                value[rowColumn] = 2 * c;
                freePosition[idi]++;
            }
            else
            {
                unsigned int row = freePosition[idi];
                index[row] = idj;
                value[row] = c;
                freePosition[idi]++;

                unsigned int column = freePosition[idj];
                index[column] = idi;
                value[column] = c;
                freePosition[idj]++;
            }
        }
    }



    unsigned int ConjugateGradientLISOptimizationSolver::getNumberOfSubIterations( ) const
    {
        return _numberOfSubIterations;
    }



    unsigned int ConjugateGradientLISOptimizationSolver::getNumberOfThreads( ) const
    {
        return _numberOfThreads;
    }



    bool ConjugateGradientLISOptimizationSolver::optimize( )
    {
        if (_showLog)
            printf( "    Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        if (_showLog)
            printf( "    Computing the gradient vector expressions...\n" );

        computeGradient( _objectiveFunction, _numberUnknowVariables );

        if (_showLog)
            printf( "    Pre-building Q matrix\n" );

        //Initialize the LIS.
        LIS_INT argc = 0;
        char **argv;
        lis_initialize( &argc, &argv );

        if (_showLog)
            printf( "    Building b vector..\n" );

        //Create b vector.
        LIS_VECTOR b;
        lis_vector_create( 0, &b );
        lis_vector_set_size( b, 0, _numberUnknowVariables );

        //Set the b values.
        const LinearExpression& exp = _objectiveFunction->getLinearExpression( );
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            Variable var = exp.getVariable( i );
            double c = -exp.getCoefficient( i );
            unsigned int id = _newVariableIndex[var.getIndex( )];
            lis_vector_set_value( LIS_INS_VALUE, id, ( LIS_SCALAR ) c, b );
        }

        if (_showLog)
            printf( "    Building Q matrix\n" );

        //Use batch insertion constructor to build the matrix faster than
        //consecutively inserting.
        unsigned int numberNonZero = 2 * _objectiveFunction->size( ) - _numberUnknowVariables;

        //Allocate the memory to store the matrix.
        LIS_INT *ptr, *index;
        LIS_SCALAR *value;
        LIS_MATRIX A;
        lis_matrix_malloc_csr( _numberUnknowVariables, numberNonZero, &ptr, &index, &value );
        lis_matrix_create( 0, &A );
        lis_matrix_set_size( A, 0, _numberUnknowVariables );

        //Build the CSR format vectors.
        preprocessingCSRFormat( ptr, index, value );
        lis_matrix_set_csr( numberNonZero, ptr, index, value, A );
        lis_matrix_assemble( A );

        //Create solver.
        LIS_VECTOR x;
        lis_vector_create( 0, &x );
        lis_vector_set_size( x, 0, _numberUnknowVariables );

        if (_showLog)
            printf( "    Setting an initial solution...\n" );

        for (unsigned int i = 0, v = 0; i < _numberVariables; i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                lis_vector_set_value( LIS_INS_VALUE, ( LIS_INT ) v, ( LIS_SCALAR ) _variables[i].getValue( ), x );
                v++;
            }
        }

        //        std::ofstream out( "outCorrect.txt" );
        //        for (unsigned int i = 0; i < _numberUnknowVariables; i++)
        //        {
        //            LIS_SCALAR v = 0.0;
        //            lis_vector_get_value( b, i, &v );
        //            out << v << std::endl;
        //        }
        //        for (unsigned int i = 0; i <= _numberUnknowVariables; i++)
        //        {
        //            out << ptr[i] << std::endl;
        //        }
        //        for (unsigned int i = 0; i < numberNonZero; i++)
        //        {
        //            out << index[i] << std::endl;
        //        }
        //        for (unsigned int i = 0; i < numberNonZero; i++)
        //        {
        //            out << value[i] << std::endl;
        //        }
        //        exit( 0 );


        if (_showLog)
            printf( "    Solving the linear system Qx = -b using %u threads and tol = %.2e...\n", _numberOfThreads, _eps );

        LIS_SOLVER solver;
        lis_solver_create( &solver );

        char option[1024];
        sprintf( option, "-i cg -p jacobi -maxiter %u -initx_zeros %d -conv_cond %d"
                 " -tol %e -omp_num_threads %u -print none", _numberOfSubIterations, 0, 0, 1e-12, _numberOfThreads );

        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );
        double gnorm = 0.0;

        #pragma omp parallel for reduction (+:gnorm)
        for (unsigned int v = 0; v < _gradient.size( ); v++)
        {
            double g = 0.0, gs = 0.0;
            computeGradientValues( v, gs, g );

            unsigned int tr = omp_get_thread_num( );
            gnorm += g * g;
        }

        //Print the current solution.
        bool continueSolver = showCurrentSolution( sqrt( gnorm ) );

        if (_showLog)
            printf( "\n\n    ITERATION           GRADIENT NORM\n" );

        double time, itime, ptime, pCtime, pItime;
        double linearSolverTime = 0.0, preConditionatorTime = 0.0;
        Timer finaltime;

        omp_set_num_threads( _numberOfThreads );
        while (_iterations++ < _maxIterations && gnorm > _eps * _eps && continueSolver)
        {
            if (_showLog && _iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );
            }

            if (_showLog && _iterations % _stepsNumber == 0)
            {
                //Print current solution.
                continueSolver = showCurrentSolution( sqrt( gnorm ) );
            }

            lis_solver_set_option( option, solver );
            lis_solve( A, b, x, solver );
            lis_solver_get_timeex( solver, &time, &itime, &ptime, &pCtime, &pItime );
            linearSolverTime += itime;
            preConditionatorTime += ptime;

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    LIS_SCALAR s;
                    lis_vector_get_value( x, _newVariableIndex[i], &s );
                    _variables[i].setValue( s );
                }
            }

            gnorm = 0.0;
            #pragma omp parallel for reduction (+:gnorm)
            for (unsigned int v = 0; v < _gradient.size( ); v++)
            {
                double g = 0.0, gs = 0.0;
                computeGradientValues( v, gs, g );

                gnorm += g * g;
            }
        }

        if (_showLog)
            printf( "    %8u            %e           \n\n\n", _iterations, sqrt( gnorm ) );

        //Print current solution.
        showCurrentSolution( sqrt( gnorm ) );

        LIS_INT nsol;
        char solvername[128];
        lis_solver_get_solver( solver, &nsol );
        lis_solver_get_solvername( nsol, solvername );

        bool status = !( gnorm > _eps * _eps || _iterations > _maxIterations );

        if (_showLog)
        {
            printf( "    Preconditioner       = %e sec.\n", preConditionatorTime );
            printf( "    linear solver (%s)   = %e sec.\n", solvername, linearSolverTime );
            if (!status)
            {
                printf( "    ****Optimal Solution NOT Found\n" );
            }
            else
            {
                printf( "    ****OPTIMAL SOLUTION FOUND****\n" );
            }
        }

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int i = 0; i < _numberVariables; i++)
        {
            if (!_variables[i].isKnownValue( ))
            {
                LIS_SCALAR s;
                lis_vector_get_value( x, _newVariableIndex[i], &s );
                _variables[i].setValue( s );
            }
        }

        lis_solver_destroy( solver );
        lis_vector_destroy( x );
        lis_vector_destroy( b );
        lis_matrix_destroy( A );

        lis_finalize( );

        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );

        return status;
    }



    void ConjugateGradientLISOptimizationSolver::setNumberOfSubIterations( unsigned int i )
    {
        _numberOfSubIterations = i > 0 ? i : 100;
    }

} // namespace Solver
