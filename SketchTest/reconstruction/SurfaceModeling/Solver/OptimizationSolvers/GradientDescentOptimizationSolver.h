#pragma once

#include "OptimizationSolver.h"
#include <vector>

namespace ModelManager
{
    class QuadraticExpression;
}
namespace Solver
{

    class GradientDescentOptimizationSolver : public OptimizationSolver
    {
    public:
        /**
         * Default construtor.
         */
        GradientDescentOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
