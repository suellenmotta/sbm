#include "../../../../libs/Timer/Timer.h"
#include "CplexOptimizationSolver.h"

namespace Solver
{



    bool CplexOptimizationSolver::optimize( )
    {
        {
            //Start the time.
            Timer finaltime;
            
            if (_showLog)
                finaltime.printTime( "        Total Solver Time" );

            return false;
        }
    } // namespace Solver
}