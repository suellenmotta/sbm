#pragma once

#include "OptimizationSolver.h"
namespace Solver
{

    class ConjugateGradientSLUOptimizationSolver : public OptimizationSolver
    {
     
    public:
        /**
         * Default construtor.
         */
        ConjugateGradientSLUOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
