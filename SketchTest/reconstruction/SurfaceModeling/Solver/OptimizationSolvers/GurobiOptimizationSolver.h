#pragma once

#include "OptimizationSolver.h"

namespace Solver
{

    class GurobiOptimizationSolver : public OptimizationSolver
    {

    private:
        /**
         * Flag that determines if the linear problem must be wrote or not.
         */
        bool _writeLP;
    public:
        /**
         * Default constructor.
         */
        GurobiOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );

        /**
         * Set if the model must be write during the optimization or not.
         * @param write - true if the model need to be write and false otherwise.
         */
        void writeLP( bool write );
    };
} // namespace Solver
