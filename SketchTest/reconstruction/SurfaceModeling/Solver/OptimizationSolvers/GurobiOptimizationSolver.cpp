#include "../../ModelManager/Enums.h"
#include "../../ModelManager/QuadraticExpression.h"
#include "../../ModelManager/Constraint.h"
#include "../../../../libs/Timer/Timer.h"
#include "gurobi_c++.h"
#include "GurobiOptimizationSolver.h"

using namespace ModelManager;

namespace Solver
{



    GurobiOptimizationSolver::GurobiOptimizationSolver( ) : OptimizationSolver( ) { }



    bool GurobiOptimizationSolver::optimize( )
    {
        //Start the time.
        Timer finaltime;
        bool optimalSolution = false;

        if (_showLog)
            printf( "Converting on a Gurobi Model...\n" );

        try
        {
            //Cria o modelo.
            GRBEnv env = GRBEnv( );

            if (!_showLog)
            {
                env.set( GRB_IntParam_LogToConsole, 0 );
            }

            GRBModel model = GRBModel( env );

            if (_showLog)
                printf( "    Allocating variables...\n" );

            //Aloca vetor para armazenar variavies.
            GRBVar* y = new GRBVar[_numberVariables];

            //Cria variaveis.
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                y[i] = model.addVar( _variables[i].getLowerBound( ), _variables[i].getUpperBound( ), 0.0, GRB_CONTINUOUS, _variables[i].getName( ) );
            }

            //Integra novas variaveis.
            model.update( );

            if (_showLog)
                printf( "    Setting initial solution...\n" );

            //Define initial values to variables.
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                y[i].set( GRB_DoubleAttr_Start, _variables[i].getValue( ) );
            }

            //Integra novas variaveis.
            model.update( );

            if (_showLog)
                printf( "    Building objective function...\n" );

            //Constroi funcao objetivo
            GRBQuadExpr obj;
            for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
            {
                const unsigned int v1 = _objectiveFunction->getVar1( i ).getIndex( );
                const unsigned int v2 = _objectiveFunction->getVar2( i ).getIndex( );
                const double c = _objectiveFunction->getCoefficient( i );
                obj += c * y[v1] * y[v2];
            }

            const LinearExpression& exp = _objectiveFunction->getLinearExpression( );
            for (unsigned int i = 0; i < exp.size( ); i++)
            {
                const unsigned int v = exp.getVariable( i ).getIndex( );
                const double c = exp.getCoefficient( i );
                obj += c * y[v];
            }

            model.setObjective( obj );

            if (_showLog)
                printf( "    Adding constraints...\n" );

            //Adiciona restricoes.
            for (unsigned int i = 0; i < _numberConstraints; i++)
            {
                GRBLinExpr exp;
                const LinearExpression& l = _constraints[i].getLine( );
                for (unsigned int j = 0; j < l.size( ); j++)
                {
                    unsigned int index = l.getVariable( j ).getIndex( );
                    exp += l.getCoefficient( j ) * y[index];
                }
                exp += l.getConstant( );
                model.addConstr( exp, _constraints[i].getSense( ), 0 );
            }

            //Integra novas variaveis.
            model.update( );

            if (_writeLP)
            {
                model.write( "Gurobi.lp" );
            }

            if (_showLog)
                printf( "\nPerforming the Gurobi Solver:\n\n" );

            //Otimiza o modelo.
            model.optimize( );

            if (model.get( GRB_IntAttr_Status ) == GRB_OPTIMAL)
            {
                optimalSolution = true;
            }
            else
            {
                //Compute an Irreducible Inconsistent Subsystem (IIS).
                model.computeIIS( );
                model.write( "Gurobi.ilp" );
            }

            //Obtem os valores das variaveis.
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                _variables[i].setValue( y[i].get( GRB_DoubleAttr_X ) );
            }
            delete [] y;
        }
        catch ( GRBException e )
        {
            std::cout << "Error code = " << e.getErrorCode( ) << std::endl;
            std::cout << e.getMessage( ) << std::endl;
        }
        catch ( ... )
        {
            std::cout << "Exception during optimization" << std::endl;
        }

        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );
        
        return optimalSolution;
    }



    void GurobiOptimizationSolver::writeLP( bool write )
    {
        _writeLP = write;
    }
} // namespace Solver
