#include "GradientDescentOptimizationSolver.h"
#include "../../ModelManager/Variable.h"
#include "../../ModelManager/QuadraticExpression.h"
#include "../../../../libs/Timer/Timer.h"
#include <cstdio>
#include <cmath>
#include <omp.h>

using namespace ModelManager;

namespace Solver
{



    GradientDescentOptimizationSolver::GradientDescentOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 5000000;
        _eps = 1e-4;
    }



    bool GradientDescentOptimizationSolver::optimize( )
    {
        //Start the time.
        Timer finaltime;
        if (_showLog)
            printf( "    Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        if (_showLog)
            printf( "    Computing the gradient vector expressions...\n" );

        computeGradient( _objectiveFunction, _numberUnknowVariables );

        const int numThreads = std::max( omp_get_num_procs( ) - 2, 1 );

        std::vector<double> grad( _numberUnknowVariables );
        double gnorm = 10;

        #pragma omp parallel for reduction (+:gnorm)
        for (unsigned int v = 0; v < _numberUnknowVariables; v++)
        {
            grad[v] = -_gradient[v].getValue( );

            double g = 0.0, gs = 0.0;
            computeGradientValues( v, gs, g );
            gnorm += g * g;
        }

        //Print current solution.
        bool continueSolver = showCurrentSolution( sqrt( gnorm ) );

        if (_showLog)
            printf( "\n    Performing the Gradient Descent Method:\n" );

        _iterations = 0;

        if (_showLog)
        {
            printf( "    ITERATION           GRADIENT NORM\n" );
            printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );
        }

        while (gnorm > _eps * _eps && _iterations++ < _maxIterations && continueSolver)
        {
            if (_iterations % _stepsNumber == 0 && _showLog)
            {
                printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );
            }

            if (_iterations % _stepsNumber == 0)
            {
                //Print current solution.
                continueSolver = showCurrentSolution( sqrt( gnorm ) );
            }

            //Compute the step to move on the opposite gradient direction.
            double t = exactLineSearch( *_objectiveFunction, grad );

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int v = 0; v < _numberVariables; v++)
            {
                if (!_variables[v].isKnownValue( ))
                {
                    _variables[v].setValue( _variables[v].getValue( ) + t * grad[_newVariableIndex[v]] );
                }
            }

            gnorm = 0.0;
            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int v = 0; v < _numberUnknowVariables; v++)
            {
                grad[v] = -_gradient[v].getValue( );

                double g = 0.0, gs = 0.0;
                computeGradientValues( v, gs, g );
                gnorm += g * g;
            }

        }
        if (_showLog)
            printf( "    %8u            %e           \n", _iterations, sqrt( gnorm ) );

        //Print current solution.
        showCurrentSolution( sqrt( gnorm ) );


        bool status = gnorm <= _eps * _eps && _iterations < _maxIterations;
        if (_showLog)
        {
            if (status)
            {
                printf( "    ****OPTIMAL SOLUTION FOUND****\n" );
            }
            else
            {
                printf( "    ****OPTIMAL SOLUTION NOT FOUND****\n" );
            }
        }
        if (_showLog)
            finaltime.printTime( "        Total Solver Time" );

        return status;
    }
} // namespace Solver
