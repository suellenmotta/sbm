#pragma once

#include "OptimizationSolver.h"
#include <vector>

namespace ModelManager
{
    class QuadraticExpression;
}
namespace Solver
{

    class PCGJacobiOptimizationSolver : public OptimizationSolver
    {
    private:
        /**
         * Compute the initial residual.
         * @param p - vector used as auxiliary to put the variable's initial values.
         * @param q - vector used as Ax.
         * @param r - vector of the residual.
         * @return - the norm squared of the residual.
         */
        double computeInitialResidual( std::vector<double>& p, std::vector<double>& q,
                                       std::vector<double>& r ) const;

        /**
         * Compute the preconditioner.
         * @param m - vector with the preconditioner.
         */
        void computePreconditioner( std::vector<double>& m ) const;

        /**
         * Compute the dot product.
         * @param v - first vector.
         * @param w - second vector.
         * @return - the dot product of v and w.
         */
        double dotProduct( const std::vector<double>& v, const std::vector<double>& w ) const;

        /**
         * Compute the product of a vector p and the coefficients of the unknown variables.
         * @param p - vector that going to be used on the calculus.
         * @param q - vector with the result.
         */
        void computeMatrixVectorMultiplication( const std::vector<double>& p,
                                                std::vector<double>& q ) const;

    public:
        /**
         * Default construtor.
         */
        PCGJacobiOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver