#pragma once

#include "OptimizationSolver.h"

namespace Solver
{

    class NewtonRaphsonOptimizationSolver : public OptimizationSolver
    {
    private:
    public:
        /**
         * Default construtor.
         */
        NewtonRaphsonOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
