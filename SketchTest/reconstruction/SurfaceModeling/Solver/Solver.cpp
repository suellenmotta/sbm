#include <omp.h>
#include <cmath>
#include "../ModelManager/Variable.h"
#include "Solver.h"

namespace Solver
{


    using namespace ModelManager;



    Solver::Solver( )
    {
        _constraints = 0;
        _eps = 1e-6;
        _iterations = 0;
        _maxIterations = 200;
        _numberConstraints = 0;
        _numberVariables = 0;
        _variables = 0;
        _displayInformation = 0;
        _stepsNumber = 1;
    }



    void Solver::setConstraints( ModelManager::Constraint *c, unsigned int m )
    {
        _constraints = c;
        _numberConstraints = m;
    }



    void Solver::setVariables( ModelManager::Variable *v, const unsigned int n )
    {
        _variables = v;
        _numberVariables = n;
    }



    Solver::~Solver( )
    {

    }



    double Solver::getEPS( ) const
    {
        return _eps;
    }



    unsigned int Solver::getNumberIterations( ) const
    {
        return _iterations;
    }



    unsigned int Solver::getMaxIterations( ) const
    {
        return _maxIterations;
    }



    unsigned int Solver::getStepsNumber( ) const
    {
        return _stepsNumber;
    }



    void Solver::setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm, void *obj ), void *object )
    {
        _displayInformation = displayInformation;
        _object = object;
    }



    void Solver::setEPS( const double eps )
    {
        _eps = eps;
    }



    void Solver::setMaxIterations( const unsigned int maxIter )
    {
        _maxIterations = maxIter;
    }



    void Solver::setNumberThreads( const int numThreads )
    {
        _numberOfThreads = numThreads;
    }



    void Solver::showLog( bool log )
    {
        _showLog = log;
    }



    void Solver::setStepsNumber( unsigned int sn )
    {
        _stepsNumber = sn;
    }



    void Solver::initializeSolver( )
    {
        _iterations = 0;
        if (_displayInformation)
        {
            _x.resize( _numberVariables );
        }
    }



    bool Solver::showCurrentSolution( const double gnorm )
    {
        if (_iterations % _stepsNumber == 0 && _displayInformation)
        {
            const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _numberVariables; i++)
            {

                _x[i] = _variables[i].getValue( );
            }

            return _displayInformation( _x, gnorm, _object );
        }
        return true;
    }

} // namespace Solver
