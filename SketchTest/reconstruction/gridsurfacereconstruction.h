#ifndef GRIDSURFACERECONSTRUCTOR_H
#define GRIDSURFACERECONSTRUCTOR_H

#include "surfacereconstruction.h"
#include "defs.h"

#include "SurfaceModeling/ModelManager/OptimizationModel.h"
using namespace ModelManager;

class GridSurfaceReconstructor
        : public SurfaceReconstructor3D
{
public:
    GridSurfaceReconstructor(const std::vector< glm::vec3 >& pointCloud, bool closed = false);

    void setGridResolution(unsigned int width, unsigned int height, unsigned int depth);

    void exportNeutralFile(const std::string& path);

protected:
    void optimize() override;
    void reconstruct() override;

    void setupVariables(OptimizationModel& model);
    void setupObjectiveFunction(OptimizationModel& model);
    void setupInputPointsConstraints(OptimizationModel& model,
                                     const std::vector<SamplePoint> &sampledPoints);
    void setupBoundaryConstraints(OptimizationModel& model);

    void samplePoints(std::vector<SamplePoint> &sampledPoints);

    RegularGrid grid;

};

#endif // GRIDSURFACERECONSTRUCTOR_H
