#ifndef DEFS_H
#define DEFS_H

#include <vector>
#include <glm/glm.hpp>
#include <iostream>

class RegularGrid
{
public:
    unsigned int nx;
    unsigned int ny;
    unsigned int nz;

    float sx;
    float sy;
    float sz;

    std::vector<float> functionValue;

    unsigned int index(unsigned int x, unsigned int y, unsigned int z = 0)
    {
        return z*ny*nx +
               y*nx + x;
    }
};

class TetrahedralMesh
{
public:
    std::vector<glm::vec3> vertices;
    std::vector<unsigned int> indices;
    std::vector<float> functionValue;
};

struct SamplePoint
{
    glm::vec3 coords;
    glm::uvec3 gridPos;

    std::vector<float> bCoords;
};

struct SamplePoint2D
{
    glm::vec2 coords;
    glm::uvec2 gridPos;

    std::vector<float> bCoords;
};

class TSamplePoint
{
public:
    glm::vec3 coords;    //spatial coordinates
    glm::vec4 bCoord;   //barycentric coordinates
    int tetrahedronIdx; //tetrahedron index
};

std::ostream& operator << (std::ostream& output, const glm::vec3& point);
std::ostream& operator << (std::ostream& output, const glm::vec2& point);


#endif // DEFS_H
