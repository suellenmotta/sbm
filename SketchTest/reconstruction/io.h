#ifndef IO_H
#define IO_H

#include "defs.h"
//#include "octree.h"
//#include "quadtree.h"
#include <string>
#include <iostream>
#include <fstream>


void writeXYZ( const std::string& path, const std::vector< glm::vec3 >& points );

//void writeXYZ( const std::string& path, const std::vector< glm::vec2 >& points );

template< typename T >
void writeXYZ( const std::string& path, const std::vector< T >& points )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.coords.x << " " << p.coords.y << " " << p.coords.z << "\n";
        }

        output.close( );
    }
}

void writeOFF(const std::string& path, const RegularGrid& grid);

//Writes a tetrahedral mesh in an off file
void writeOFF(const std::string& path, const TetrahedralMesh& mesh);

//template<typename T>
//void writeOFF(const std::string& path, const Octree<T>& oct)
//{
//    std::ofstream out( path );
//    if (!out)
//    {
//        printf( "Error writing the file %s\n", path.c_str( ) );
//        return;
//    }

//    out << "OFF\n";

//    auto numElements = oct.getNumLeafNodes()*6;
//    auto vertices = oct.getVertices();

//    out << vertices.size( ) << " " << numElements << " 0\n";

//    for (const auto& p : vertices)
//    {
//        out << p.x << " " << p.y << " " << p.z << "\n";
//    }

//    for(const auto& lBin : oct)
//    {
//        out << "4 " << lBin.indices[0] << " " << lBin.indices[1] << " "
//                    << lBin.indices[3] << " " << lBin.indices[2] << "\n";

//        out << "4 " << lBin.indices[4] << " " << lBin.indices[0] << " "
//                    << lBin.indices[2] << " " << lBin.indices[6] << "\n";

//        out << "4 " << lBin.indices[2] << " " << lBin.indices[3] << " "
//                    << lBin.indices[7] << " " << lBin.indices[6] << "\n";


//        out << "4 " << lBin.indices[1] << " " << lBin.indices[5] << " "
//                    << lBin.indices[7] << " " << lBin.indices[3] << "\n";

//        out << "4 " << lBin.indices[5] << " " << lBin.indices[4] << " "
//                    << lBin.indices[6] << " " << lBin.indices[7] << "\n";



//        out << "4 " << lBin.indices[4] << " " << lBin.indices[5] << " "
//                    << lBin.indices[1] << " " << lBin.indices[0] << "\n";
//    }

//    out.close( );
//}

//template<typename T>
//void writeOFF(const std::string& path, const Quadtree<T>& qdt)
//{
//    std::ofstream out( path );
//    if (!out)
//    {
//        printf( "Error writing the file %s\n", path.c_str( ) );
//        return;
//    }

//    out << "OFF\n";

//    auto numElements = qdt.getNumLeafNodes();
//    auto vertices = qdt.getVertices();

//    out << vertices.size( ) << " " << numElements << " 0\n";

//    for (const auto& p : vertices)
//    {
//        out << p.x << " " << p.y << " " << 0 << "\n";
//    }

//    for(const auto& lBin : qdt)
//    {
//        out << "4 " << lBin.indices[0] << " " << lBin.indices[1] << " "
//                    << lBin.indices[3] << " " << lBin.indices[2] << "\n";
//    }

//    out.close( );
//}

void writeOFF( const std::string& path, const std::vector<glm::vec3>& vertices,
                          const std::vector<unsigned int>& indices);

//void writeNeutralFile(const std::string& path, const RegularGrid& grid, const std::vector<glm::vec3>& vertices,
//                      const std::vector<unsigned int>& indices);

//void writeNeutralFile(const std::string& path, const Octree<float>& oct, const std::vector<glm::vec3>& vertices,
//                      const std::vector<unsigned int>& indices);

//void writeNeutralFile(const std::string& path, const Quadtree<float>& qdt, const std::vector<glm::vec2> &vertices,
//                      const std::vector<unsigned int>& indices);

//void writeDCFFile(const std::string& path, const Octree<float>& oct);


//void readXYZ( const std::string& path, std::vector< glm::vec3 >& points);

#endif // IO_H
