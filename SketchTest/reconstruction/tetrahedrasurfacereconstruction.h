#ifndef TETRAHEDRASURFACERECONSTRUCTION_H
#define TETRAHEDRASURFACERECONSTRUCTION_H

#include "surfacereconstruction.h"
#include "defs.h"

#include "newvolume.h"

#include "SurfaceModeling/ModelManager/OptimizationModel.h"
using namespace ModelManager;


class TetrahedraSurfaceReconstructor : public SurfaceReconstructor3D
{
public:
    TetrahedraSurfaceReconstructor(const std::vector< glm::vec3 >& pointCloud, bool closed = false);

    void setGridResolution(unsigned int width, unsigned int height, unsigned int depth);

    void setVolumeInfo(const NewVolume::Info& info);

    NewVolume* getDistanceField();

protected:
    void normalizePoints() override;
    void optimize() override;
    void reconstruct() override;

    void computeDistanceField();
    void createTetrahedralMesh();
    void samplePoints(std::vector<TSamplePoint> &sampledPoints);

    void setupVariables(OptimizationModel& model);
    void setupObjectiveFunction(OptimizationModel& model);
    void setupInputPointsConstraints(OptimizationModel& model,
                                     const std::vector<TSamplePoint> &sampledPoints);
    void setupBoundaryConstraints(OptimizationModel& model);

    RegularGrid grid;
    TetrahedralMesh mesh;
    NewVolume* distanceField;
    NewVolume::Info info;

    //Points in grid coordinates
    std::vector<glm::uvec3> gPoints;

//    std::list<unsigned int> externalVertices;
};

#endif // TETRAHEDRASURFACERECONSTRUCTION_H
