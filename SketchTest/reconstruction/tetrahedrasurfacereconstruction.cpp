#include "tetrahedrasurfacereconstruction.h"

#include "newvolume.h"
#include "volumealgorithms.h"
#include "graph.h"

#include <algorithm>

#define INF 1e20
#define CGAL_TO_GLM_VEC3(p) glm::vec3(p.x(),p.y(),p.z())

#define DUMB_TEST 0


//#include <iostream>
//#include <CGAL/Simple_cartesian.h>
//typedef CGAL::Simple_cartesian<double> Kernel;
//typedef Kernel::Point_2 Point_2;
//typedef Kernel::Segment_2 Segment_2;
//int testeCGAL()
//{
//  Point_2 p(1,1), q(10,10);
//  std::cout << "p = " << p << std::endl;
//  std::cout << "q = " << q.x() << " " << q.y() << std::endl;
//  std::cout << "sqdist(p,q) = "
//            << CGAL::squared_distance(p,q) << std::endl;

//  Segment_2 s(p,q);
//  Point_2 m(5, 9);

//  std::cout << "m = " << m << std::endl;
//  std::cout << "sqdist(Segment_2(p,q), m) = "
//            << CGAL::squared_distance(s,m) << std::endl;
//  std::cout << "p, q, and m ";
//  switch (CGAL::orientation(p,q,m)){
//  case CGAL::COLLINEAR:
//    std::cout << "are collinear\n";
//    break;
//  case CGAL::LEFT_TURN:
//    std::cout << "make a left turn\n";
//    break;
//  case CGAL::RIGHT_TURN:
//    std::cout << "make a right turn\n";
//    break;
//  }
//  std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;
//  return 0;
//}

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/boost/graph/helpers.h>
#include <CGAL/Mesh_polyhedron_3.h>
#include <CGAL/Polyhedral_mesh_domain_3.h>
#include <CGAL/Polyhedral_mesh_domain_with_features_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/refine_mesh_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>


#include "SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "SurfaceModeling/Solver/OptimizationSolvers/PCGJacobiOptimizationSolver.h"
#include "SurfaceModeling/Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"

#include "SurfaceModeling/ModelManager/QuadraticExpression.h"
#include "SurfaceModeling/ModelManager/OptimizationModel.h"

// Domain
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::FT FT;
typedef CGAL::Mesh_polyhedron_3<K>::type Polyhedron;
typedef CGAL::Polyhedral_mesh_domain_with_features_3<K> Mesh_domain;
#ifdef CGAL_CONCURRENT_MESH_3
typedef CGAL::Parallel_tag Concurrency_tag;
#else
typedef CGAL::Sequential_tag Concurrency_tag;
#endif
// Triangulation
typedef CGAL::Mesh_triangulation_3<Mesh_domain,CGAL::Default,Concurrency_tag>::type Tr;
typedef CGAL::Mesh_complex_3_in_triangulation_3<Tr> C3t3;
// Criteria
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;
// To avoid verbose function and named parameters call
using namespace CGAL::parameters;



TetrahedraSurfaceReconstructor::TetrahedraSurfaceReconstructor(
        const std::vector<glm::vec3> &pointCloud, bool closed)
    : SurfaceReconstructor3D(pointCloud,closed)
    , distanceField(nullptr)
{

}

void TetrahedraSurfaceReconstructor::setGridResolution(unsigned int width,
                                                       unsigned int height,
                                                       unsigned int depth)
{
    if(width%2 > 0)  width++;
    if(height%2 > 0) height++;
    if(depth%2 > 0)  depth++;

    grid.nx = 2*width;
    grid.ny = 2*height;
    grid.nz = 2*depth;

    grid.sx = 4.0f / (grid.nx-1);
    grid.sy = 4.0f / (grid.ny-1);
    grid.sz = 4.0f / (grid.nz-1);
}

void TetrahedraSurfaceReconstructor::setVolumeInfo(const NewVolume::Info &info)
{
    this->info = info;
}

NewVolume *TetrahedraSurfaceReconstructor::getDistanceField()
{
    return distanceField;
}

void TetrahedraSurfaceReconstructor::normalizePoints()
{
    gPoints = pointCoordsToVolumeIndexes(info,pts);
}

void TetrahedraSurfaceReconstructor::optimize()
{
    computeDistanceField();

    createTetrahedralMesh();


    Solver::ConjugateGradientLISOptimizationSolver solver;
//    Solver::PCGJacobiOptimizationSolver solver;
//    Solver::LBFGSOptimizationSolver solver;
    ModelManager::OptimizationModel model(&solver);

    std::vector<TSamplePoint> sampledPoints;
    samplePoints(sampledPoints);

//    writeXYZ("sampled_points.xyz", sampledPoints);

    setupVariables(model);
    setupObjectiveFunction(model);
    setupInputPointsConstraints(model,sampledPoints);
    setupBoundaryConstraints(model);


    //DEBUG
    std::cout << "\nVariables:\n";
    auto variables = model.getVariables();
    for(int i = 0; i < model.getNumberVariables(); ++i)
    {
        std::cout << variables[i].getName() << " = " << variables[i].getValue() << std::endl;
    }

    std::cout << "\nObjective function:\n" << model.getObjectiveFunction() << std::endl;

    std::cout << "\nConstraints:\n";
    auto constraints = model.getConstraints();
    for(int i = 0; i < model.getNumberConstraints(); ++i)
    {
        std::cout << "C" << i << ": " << constraints[i] << std::endl;
    }




    model.optimize();

    auto numVariables = model.getNumberVariables();
    mesh.functionValue.resize(numVariables,0.0f);

//    auto variables = model.getVariables();

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        double x = variables[i].getValue();
        mesh.functionValue[i] = x;
    }
}

void TetrahedraSurfaceReconstructor::reconstruct()
{
    std::map< std::pair<unsigned int, unsigned int>,
              unsigned int > edgeVertices;

    //returns the index of the new vertex between i0 and i1, creating it when it did not exist
    auto getVertexIndex = [&](unsigned int i0, unsigned int i1)
    {
        auto i01 = meshVertices.size();

        auto edge = i0 < i1 ? std::make_pair(i0,i1) : std::make_pair(i1,i0);
        auto it = edgeVertices.find(edge);
        if(it==edgeVertices.end())
        {
            auto v0 = mesh.vertices[i0];
            auto q0 = mesh.functionValue[i0];

            auto v1 = mesh.vertices[i1];
            auto q1 = mesh.functionValue[i1];

            meshVertices.push_back(v0 + (0-q0) * (v1-v0) / (q1-q0));
            edgeVertices[edge] = i01;
        }
        else
        {
            i01 = it->second;
        }

        return i01;
    };

    //i0 is the vertex with different sign
    auto createOneTriangle = [&](unsigned int i0, unsigned int i1, unsigned int i2, unsigned int i3)
    {
        auto i01 = getVertexIndex(i0,i1);
        auto i02 = getVertexIndex(i0,i2);
        auto i03 = getVertexIndex(i0,i3);

        meshIndices.push_back(i01);
        if(mesh.functionValue[i0]<0) //does this work to find the orientation?
        {
            meshIndices.push_back(i03);
            meshIndices.push_back(i02);
        }
        else
        {
            meshIndices.push_back(i02);
            meshIndices.push_back(i03);
        }
    };

    //i0 and i1 have the same sign; i2 and i3 have the same sign, opposite to i0 and i1
    auto createTwoTriangles = [&](unsigned int i0, unsigned int i1, unsigned int i2, unsigned int i3)
    {
        auto i02 = getVertexIndex(i0,i2);
        auto i03 = getVertexIndex(i0,i3);
        auto i12 = getVertexIndex(i1,i2);
        auto i13 = getVertexIndex(i1,i3);

        if(mesh.functionValue[i0]<0)
        {
            meshIndices.push_back(i02);
            meshIndices.push_back(i03);
            meshIndices.push_back(i12);

            meshIndices.push_back(i12);
            meshIndices.push_back(i03);
            meshIndices.push_back(i13);
        }
        else
        {
            meshIndices.push_back(i02);
            meshIndices.push_back(i12);
            meshIndices.push_back(i03);

            meshIndices.push_back(i03);
            meshIndices.push_back(i12);
            meshIndices.push_back(i13);
        }
    };

    auto numTetrahedrons = mesh.indices.size()/4;

    for(unsigned int i = 0; i < numTetrahedrons; ++i)
    {
        auto i0 = mesh.indices[4*i+0];
        auto i1 = mesh.indices[4*i+1];
        auto i2 = mesh.indices[4*i+2];
        auto i3 = mesh.indices[4*i+3];

        auto s0 = std::signbit(mesh.functionValue[i0]);
        auto s1 = std::signbit(mesh.functionValue[i1]);
        auto s2 = std::signbit(mesh.functionValue[i2]);
        auto s3 = std::signbit(mesh.functionValue[i3]);

        if(s0 == s1 && s0 == s2 && s0 == s3) continue; // + + + +

        if(s0 == s1) // + + ? ?
        {
            if(s0 == s2) // + + + -
                createOneTriangle(i3,i0,i1,i2);
            else
            {
                if(s0 == s3) // + + - +
                    createOneTriangle(i2,i0,i1,i3);
                else // + + - -
                    createTwoTriangles(i0,i1,i2,i3);
            }
        }
        else // + - ? ?
        {
            if(s0 == s2) // + - + ?
            {
                if(s0 == s3) // + - + +
                    createOneTriangle(i1,i0,i2,i3);
                else // + - + -
                    createTwoTriangles(i0,i2,i1,i3);
            }
            else // + - - ?
            {
                if(s0 == s3) // + - - +
                    createTwoTriangles(i0,i3,i1,i2);
                else // + - - -
                    createOneTriangle(i0,i1,i2,i3);
            }
        }
    }
}

void TetrahedraSurfaceReconstructor::computeDistanceField()
{
    NewVolume* binaryVolume = new NewVolume(info);
    auto binaryBuffer = binaryVolume->getBuffer();

    auto size = info.numXSlices*info.numYSlices*info.numZSlices;
    std::fill(binaryBuffer,binaryBuffer+size,INF);

    for(const auto& p : gPoints)
    {
        auto idx = binaryVolume->getIndex(p.x,p.y,p.z);
        binaryBuffer[idx] = 0.0f;
    }

    distanceField = computeEDT(binaryVolume);
    delete binaryVolume;
}

void TetrahedraSurfaceReconstructor::createTetrahedralMesh()
{
#if DUMB_TEST
    //Dumb test
    mesh.vertices = {
      {-1,-1, 0},
      { 1,-1, 0},
      { 0, 1, 0},
      { 0, 0,-1},
      { 0, 0, 1}
    };
    mesh.indices = {
        0,1,2,4,
        0,2,1,3
    };

    return;
#endif




    // Create input polyhedron
    Polyhedron polyhedron;

    CGAL::Polyhedron_incremental_builder_3<Polyhedron::HalfedgeDS> B(polyhedron.hds());

    B.begin_surface(8,12);

    auto info = distanceField->getInfo();
    glm::vec3 min = {0.5f, 0.5f, 0.5f};
    glm::vec3 max = {info.numXSlices-0.5f,
                     info.numYSlices-0.5f,
                     info.numZSlices-0.5f};

    typedef typename Polyhedron::HalfedgeDS::Vertex   Vertex;
    typedef typename Vertex::Point Point;
    B.add_vertex( Point(min.x, min.y, min.z) );
    B.add_vertex( Point(max.x, min.y, min.z) );
    B.add_vertex( Point(max.x, min.y, max.z) );
    B.add_vertex( Point(min.x, min.y, max.z) );
    B.add_vertex( Point(min.x, max.y, min.z) );
    B.add_vertex( Point(max.x, max.y, min.z) );
    B.add_vertex( Point(max.x, max.y, max.z) );
    B.add_vertex( Point(min.x, max.y, max.z) );


    std::vector<unsigned int> indices =
    {
        0, 1, 2,
        0, 2, 3,
        4, 0, 3,
        4, 3, 7,
        6, 2, 1,
        6, 1, 5,
        7, 3, 2,
        7, 2, 6,
        5, 1, 0,
        5, 0, 4,
        7, 6, 5,
        7, 5, 4
    };

    for(int i = 0; i < 12; ++i)
    {
        B.begin_facet();
        B.add_vertex_to_facet(indices[3*i+0]);
        B.add_vertex_to_facet(indices[3*i+1]);
        B.add_vertex_to_facet(indices[3*i+2]);
        B.end_facet();
    }

    B.end_surface();

//    std::ofstream out("cube_test.off");
//    out << polyhedron;

    // Create domain
    Mesh_domain domain(polyhedron);
    domain.detect_features();

    struct SizingField
    {
        SizingField(NewVolume* distanceField) : distanceField(distanceField)
        {
//            auto w = distanceField->numXSlices();
//            auto h = distanceField->numYSlices();
//            auto d = distanceField->numZSlices();

//            factor.x = w / 2.0f;
//            factor.y = h / 2.0f;
//            factor.z = d / 2.0f;
        }

        typedef ::FT FT; //double
        typedef Point Point_3;
        typedef Mesh_domain::Index Index;

        NewVolume* distanceField;
        glm::vec3 factor;

        FT operator()(const Point_3& pm, const int dimension, const Index&) const
        {


//            if(pm.z()>0)
//                return 0.1;
//            else
//                return 2.0;


            glm::vec3 p = CGAL_TO_GLM_VEC3(pm);
//            glm::vec3 min(0.5f);
//            glm::vec3 p = (glmPoint - min);//*factor;

            glm::ivec3 pi((int)p.x,(int)p.y,(int)p.z);

            int x0,x1,y0,y1,z0,z1;

            x0 = (int)(pi.x-0.5);
            x1 = (int)(pi.x+0.5);

            y0 = (int)(pi.y-0.5);
            y1 = (int)(pi.y+0.5);

            z0 = (int)(pi.z-0.5);
            z1 = (int)(pi.z+0.5);

            float q000 = distanceField->getValue(x0,y0,z0);
            float q100 = distanceField->getValue(x1,y0,z0);
            float q010 = distanceField->getValue(x0,y1,z0);
            float q110 = distanceField->getValue(x1,y1,z0);

            float q001 = distanceField->getValue(x0,y0,z1);
            float q101 = distanceField->getValue(x1,y0,z1);
            float q011 = distanceField->getValue(x0,y1,z1);
            float q111 = distanceField->getValue(x1,y1,z1);

            glm::vec3 pd(p.x-pi.x-0.5, p.y-pi.y-0.5, p.z-pi.z-0.5);

            float q00 = q000 * (1.0f-pd.x) + q100 * pd.x;
            float q01 = q001 * (1.0f-pd.x) + q101 * pd.x;
            float q10 = q010 * (1.0f-pd.x) + q110 * pd.x;
            float q11 = q011 * (1.0f-pd.x) + q111 * pd.x;

            float q0 = q00 * (1.0f-pd.y) + q10 * pd.y;
            float q1 = q01 * (1.0f-pd.y) + q11 * pd.y;

            float q = q0 * (1.0f-pd.z) + q1 * pd.z;

            //std::cout << q << std::endl;
/*
            if (dimension == 1 )
                std::cout << "EDGE!!! ";// << pm.x() << " " << pm.y() << " " << pm.z() << std::endl;
            else if (dimension == 2)
                std::cout << "FACET!!! ";// << pm.x() << " " << pm.y() << " " << pm.z() << std::endl;
*/

            if( dimension == 1)// std::cout << p << " size: " << q << std::endl;
                return std::abs(q) / 2.0 + 0.01;
            else
                return std::abs(q) + 1.0;



        }
    };

    SizingField size(distanceField);


    // Mesh criteria (no cell_size set)
    Mesh_criteria criteria(edge_size=0.5, cell_size=size, cell_radius_edge_ratio=2);

    // Mesh generation
    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain,criteria/*,no_exude(),no_perturb()*/,manifold());
    // Output
    std::ofstream medit_file("out_1.off");
    c3t3.output_facets_in_complex_to_off(medit_file);
    medit_file.close();
    // Set tetrahedron size (keep cell_radius_edge_ratio), ignore facets
  //  Mesh_criteria new_criteria(cell_radius_edge_ratio=3, cell_size=0.03);
  //  // Mesh refinement (and make the output manifold)
  //  refine_mesh_3(c3t3, domain, new_criteria,manifold());
//    refine_mesh_3(c3t3,domain,criteria,manifold());

    auto t = c3t3.triangulation();

    std::cout << "Number of vertices: " << t.number_of_vertices() << std::endl;

    std::map<Tr::Point,unsigned int> indexing;
    int count = 0;
    for(auto it = t.all_vertices_begin(); it != t.all_vertices_end(); ++it)
    {
        auto p = it->point();
        auto f = indexing.find(p);
        if(f==indexing.end())
        {
            indexing[p] = count;
            count++;
        }
    }

    mesh.indices.reserve(c3t3.number_of_cells_in_complex());
    for(auto it = c3t3.cells_begin(); it != c3t3.cells_end(); ++it)
    {
        mesh.indices.push_back(indexing[it->vertex(0)->point()]);
        mesh.indices.push_back(indexing[it->vertex(1)->point()]);
        mesh.indices.push_back(indexing[it->vertex(2)->point()]);
        mesh.indices.push_back(indexing[it->vertex(3)->point()]);
    }

    auto n = c3t3.number_of_vertices_in_complex();
    std::cout << "num: " << n << std::endl;
    for(auto it = c3t3.vertices_in_complex_begin(); it != c3t3.vertices_in_complex_end(); ++it)
    {
        std::cout << indexing[it->point()] << ": "
                                           << it->point().x() << ","
                                           << it->point().y() << ","
                                           << it->point().z() << ")" << std::endl;
    }
    std::cout << std::endl;



//    for(auto it = t.all_vertices_begin(); it != t.all_vertices_end(); ++it)
//    {
//        Polyhedron::HalfedgeDS::Vertex v;
//        v.vertex_begin();
////        auto hv = it->vertex_begin();
////        do
////        {

////        } while( hv != it->vertex_begin();)
//    }


    mesh.vertices.resize(indexing.size());
    for(const auto& p : indexing)
    {
        mesh.vertices[p.second] = CGAL_TO_GLM_VEC3(p.first);
    }
    indexing.clear();



    writeOFF("teste_bunny.off",mesh);

  //  for(unsigned int i = 0; i < mesh.indices.size(); i+=4)
  //  {
  //      std::cout << mesh.indices[i+0] << " " <<
  //                   mesh.indices[i+1] << " " <<
  //                   mesh.indices[i+2] << " " <<
  //                   mesh.indices[i+3] << std::endl;
  //  }

    std::cout <<"Done." << std::endl;


    // Output
    medit_file.open("out_2.off");
    c3t3.output_facets_in_complex_to_off(medit_file);


}

void TetrahedraSurfaceReconstructor::samplePoints(std::vector<TSamplePoint> &sampledPoints)
{
#if DUMB_TEST
    //Dumb test
    std::vector<glm::vec3> gPoints;
    glm::vec3 p0(0.2,-0.52,0.12);
    glm::vec3 p1(0, -0.25,-0.25);

    std::cout << "p0 = " << p0 << std::endl;
    std::cout << "p1 = " << p1 << std::endl;
    gPoints.push_back(p0);
    gPoints.push_back(p1);
#endif

    //sctp computes the scalar triple product
    auto sctp = [](const glm::vec3& a, const glm::vec3& b, const glm::vec3& c)
    {
        return glm::dot(a,glm::cross(b,c));
    };

    auto volume = [&](const glm::vec3& a, const glm::vec3& b, const glm::vec3& c, const glm::vec3& d)
    {
        glm::vec3 vab = b - a;
        glm::vec3 vac = c - a;
        glm::vec3 vad = d - a;

        return /*std::abs(*/sctp(vab,vac,vad) / 6.0f;
    };

    int numTetrahedrons = mesh.indices.size() / 4;

    std::cout << "num tetahedrons: " << numTetrahedrons << std::endl;

    std::cout << "num points: " << gPoints.size() << std::endl;

    //writeXYZ("samplepoints0.xyz",gPoints);

    //Keeps only one point per tetrahedron: dumb implementation


    std::vector<bool> tetrahedronsWithPoint(numTetrahedrons,false);

    for(int i = 0; i < numTetrahedrons; ++i)
    {
        auto a = mesh.vertices[mesh.indices[4*i+0]];
        auto b = mesh.vertices[mesh.indices[4*i+1]];
        auto c = mesh.vertices[mesh.indices[4*i+2]];
        auto d = mesh.vertices[mesh.indices[4*i+3]];

        auto totalVolume = volume(a,b,c,d);

        int j = 0;
        for(const auto& p : gPoints)
        {
            auto bCoords = glm::vec4(
                        volume(p,b,c,d)/totalVolume,
                        volume(a,p,c,d)/totalVolume,
                        volume(a,b,p,d)/totalVolume,
                        volume(a,b,c,p)/totalVolume);

            float eps = 0.1f;
            if(/*std::abs(bCoords.x+bCoords.y+bCoords.z+bCoords.w-1)<=eps*/
               bCoords.x >= eps && bCoords.y >= eps && bCoords.z >= eps && bCoords.w >= eps)
            {
                std::cout << j << std::endl;
                TSamplePoint point;
                point.coords = p;
                point.bCoord = bCoords;
                point.tetrahedronIdx = i;

                //if(j==586)
                //{
                    sampledPoints.push_back(point);
                    tetrahedronsWithPoint[i] = true;
                //}
                break;
            }

            j++;
        }
    }

    std::cout << "num sampled points: " << sampledPoints.size() << std::endl;

    for(auto& p : sampledPoints)
    {
        std::cout << p.tetrahedronIdx << std::endl;
    }

    TetrahedralMesh newMesh;
    for(int i = 0; i < numTetrahedrons; ++i)
    {
        if(tetrahedronsWithPoint[i])
        {
            auto a = mesh.indices[4*i+0];
            auto b = mesh.indices[4*i+1];
            auto c = mesh.indices[4*i+2];
            auto d = mesh.indices[4*i+3];

            newMesh.indices.push_back(a);
            newMesh.indices.push_back(b);
            newMesh.indices.push_back(c);
            newMesh.indices.push_back(d);
        }
    }

    newMesh.vertices = mesh.vertices;

    writeOFF("teste.off",newMesh);


    writeXYZ("samplepoints1.xyz",sampledPoints);
}

void TetrahedraSurfaceReconstructor::setupVariables(OptimizationModel &model)
{
    auto numVariables = mesh.vertices.size();

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        std::stringstream name;
        name << "x" << i;
        model.addVariable( name.str(), -1000, 1000 );
    }

    model.update();
}

void TetrahedraSurfaceReconstructor::setupObjectiveFunction(OptimizationModel &model)
{
    //Uses a graph to find the neighborhood information
    Graph G(mesh.vertices.size());
    auto numTetrahedrons = mesh.indices.size()/4;
    for(unsigned int i = 0; i < numTetrahedrons; ++i)
    {
        auto i0 = mesh.indices[4*i+0];
        auto i1 = mesh.indices[4*i+1];
        auto i2 = mesh.indices[4*i+2];
        auto i3 = mesh.indices[4*i+3];

        G.insertEdge(i0,i1);
        G.insertEdge(i0,i2);
        G.insertEdge(i0,i3);
        G.insertEdge(i1,i2);
        G.insertEdge(i1,i3);
        G.insertEdge(i2,i3);
    }

    //Set the objective function
    auto& obj = model.getObjectiveFunction();
    const auto& y = model.getVariables();

    std::cout << "\nObjective function expressions:\n";

    for(unsigned int u = 0; u < mesh.vertices.size(); ++u)
    {
        const auto& neighbors = G.getNeighbors(u);

        ModelManager::LinearExpression exp;

        for(const auto& n : neighbors)
        {
            exp += y[n.v];
        }

        exp -= neighbors.size()*y[u];

        std::cout << exp << std::endl;

        obj += exp*exp;
    }
}

void TetrahedraSurfaceReconstructor::setupInputPointsConstraints(OptimizationModel &model, const std::vector<TSamplePoint> &sampledPoints)
{
    auto sense = isSoft ? SOFT : EQUAL;

    auto variables = model.getVariables();

    std::cout << "\nInput points constraints:\n";
    for (unsigned int i = 0; i < sampledPoints.size( ); i++)
    {
        const TSamplePoint& p = sampledPoints[i];

        auto tPos = 4 * p.tetrahedronIdx;

        ModelManager::LinearExpression exp;

        exp += p.bCoord.x * variables[mesh.indices[tPos+0]];
        exp += p.bCoord.y * variables[mesh.indices[tPos+1]];
        exp += p.bCoord.z * variables[mesh.indices[tPos+2]];
        exp += p.bCoord.w * variables[mesh.indices[tPos+3]];

        std::cout << exp << std::endl;
        model.addConstraint(exp,sense,0);
    }

    if(isSoft)
        model.setSoftConstraintsWeight(1000.0);
}

void TetrahedraSurfaceReconstructor::setupBoundaryConstraints(OptimizationModel &model)
{
    auto variables = model.getVariables();

#if DUMB_TEST
    model.addConstraint(variables[0],EQUAL,1); return;
#endif

    float value = 10.0f;

    if(isClosed)
    {
        for(int i = 0; i < 8; ++i)
            model.addConstraint(variables[i], EQUAL, value);

        //And the center point?
    }
    else
    {
        glm::vec3 min = {0.5f, 0.5f, 0.5f};
        glm::vec3 max = {info.numXSlices-0.5f,
                         info.numYSlices-0.5f,
                         info.numZSlices-0.5f};



        float topY = info.numYSlices-0.5f - 0.001f;
        float bottomY = 0.5f + 0.001f;

        std::cout << "Boundary constraints:\n";
        for(unsigned int i = 0; i < mesh.vertices.size(); ++i)
        {
            if(mesh.vertices[i].y > topY)
            {
                auto c = model.addConstraint(variables[i],EQUAL,value);
                std::cout << c << std::endl;
            }
            else if(mesh.vertices[i].y < bottomY)
            {
                auto c = model.addConstraint(variables[i],EQUAL,-value);
                std::cout << c << std::endl;
            }
        }
    }





}
