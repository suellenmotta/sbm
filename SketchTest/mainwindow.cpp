#include "mainwindow.h"

#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QSpinBox>
#include <QGroupBox>
#include <QListWidget>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>

#include <sstream>

#include "newvolume.h"
#include "canvas3dwidget.h"
#include "surfacemodelingdockwidget.h"
#include "volumealgorithms.h"

#include "reconstruction/gridsurfacereconstruction.h"
#include "reconstruction/tetrahedrasurfacereconstruction.h"

#define NUM_SLICES 100



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    resize(800, 600);

    toolBar = new QToolBar(this);
    toolBar->setStyleSheet("QToolBar{spacing:6px;}");
    addToolBar(toolBar);

    QLabel* volumeDimsLabel = new QLabel("Volume dimensions:", this);
    volumeXSizeSpinBox = new QSpinBox(this);
    volumeYSizeSpinBox = new QSpinBox(this);
    volumeZSizeSpinBox = new QSpinBox(this);

    volumeXSizeSpinBox->setRange(2,5000);
    volumeYSizeSpinBox->setRange(2,5000);
    volumeZSizeSpinBox->setRange(2,5000);
    volumeXSizeSpinBox->setValue(NUM_SLICES);
    volumeYSizeSpinBox->setValue(NUM_SLICES);
    volumeZSizeSpinBox->setValue(NUM_SLICES);

    QAction* resizeVolumeAct = new QAction("Apply",this);
    QAction* wireframeAct = new QAction("Wireframe",this);
    wireframeAct->setCheckable(true);
    connect(resizeVolumeAct,&QAction::triggered,this,&MainWindow::onResizeVolume);
    connect(wireframeAct,&QAction::toggled,this,&MainWindow::onWireframeToggled);

    toolBar->addWidget(volumeDimsLabel);
    toolBar->addWidget(volumeXSizeSpinBox);
    toolBar->addWidget(volumeYSizeSpinBox);
    toolBar->addWidget(volumeZSizeSpinBox);
    toolBar->addAction(resizeVolumeAct);
    toolBar->addSeparator();
    toolBar->addAction(wireframeAct);

    centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);

    centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);

    openGLWidget = new Canvas3DWidget(centralWidget);
    openGLWidget->setMouseTracking(true);
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(2);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(openGLWidget->sizePolicy().hasHeightForWidth());
    openGLWidget->setSizePolicy(sizePolicy);

    QHBoxLayout* horizontalLayout = new QHBoxLayout(centralWidget);
    horizontalLayout->setSpacing(6);
    horizontalLayout->setContentsMargins(11, 11, 11, 11);

    horizontalLayout->addWidget(openGLWidget);

    connect(openGLWidget,SIGNAL(canvasInitialized()),this,SLOT(onCanvasInitialized()));


//


    createSideWidget(horizontalLayout);



//    horizontalLayout->addWidget(surfaceModelingWidget);


}

MainWindow::~MainWindow()
{
}

void MainWindow::createSideWidget(QHBoxLayout* horizontalLayout)
{
    QWidget* rightSideWidget = new QWidget(centralWidget);
    QVBoxLayout* verticalLayout = new QVBoxLayout(rightSideWidget);
    verticalLayout->setSpacing(6);
    verticalLayout->setContentsMargins(11, 11, 11, 11);
    verticalLayout->setContentsMargins(0, 0, 0, 0);

//    QVBoxLayout* rightSideLayout = new QVBoxLayout();
//    rightSideLayout->setSpacing(6);

    QGroupBox* slicesGroupBox = new QGroupBox(rightSideWidget);
    fillSlicesGroup(slicesGroupBox);

    QGroupBox* sketchesGroupBox = new QGroupBox(rightSideWidget);
    fillSketchesGroup(sketchesGroupBox);

    QGroupBox* optionsGroupBox = new QGroupBox(rightSideWidget);
    fillOptionsGroup(optionsGroupBox);

    QGroupBox* surfacesGroupBox = new QGroupBox(rightSideWidget);
    fillSurfacesGroup(surfacesGroupBox);

    verticalLayout->addWidget(slicesGroupBox);
    verticalLayout->addWidget(sketchesGroupBox);
    verticalLayout->addWidget(optionsGroupBox);
    verticalLayout->addWidget(surfacesGroupBox);

//    verticalLayout->addLayout(rightSideLayout);
    horizontalLayout->addWidget(rightSideWidget);


}

void MainWindow::fillSlicesGroup(QGroupBox *group)
{
    group->setTitle("Volume slices");

    QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
    sizePolicy1.setHorizontalStretch(0);
    sizePolicy1.setVerticalStretch(0);
    sizePolicy1.setHeightForWidth(group->sizePolicy().hasHeightForWidth());
    group->setSizePolicy(sizePolicy1);
    group->setFlat(false);
    group->setCheckable(false);

    QLabel* xLabel = new QLabel("X:", group);
    QLabel* yLabel = new QLabel("Y:", group);
    QLabel* zLabel = new QLabel("Z:", group);

    QLabel* xStepLabel = new QLabel("Step:", group);
    QLabel* yStepLabel = new QLabel("Step:", group);
    QLabel* zStepLabel = new QLabel("Step:", group);

    QCheckBox* showXSliceCheckbox = new QCheckBox(group);
    QCheckBox* showYSliceCheckbox = new QCheckBox(group);
    QCheckBox* showZSliceCheckbox = new QCheckBox(group);
    showXSliceCheckbox->setChecked(true);
    showYSliceCheckbox->setChecked(true);
    showZSliceCheckbox->setChecked(true);

    xSpinBox = new QSpinBox(group);
    ySpinBox = new QSpinBox(group);
    zSpinBox = new QSpinBox(group);

    xStepSpinBox = new QSpinBox(group);
    yStepSpinBox = new QSpinBox(group);
    zStepSpinBox = new QSpinBox(group);

    QGridLayout* gridLayout = new QGridLayout(group);
    gridLayout->setSpacing(6);
    gridLayout->setContentsMargins(11, 11, 11, 11);

    gridLayout->addWidget(xLabel, 0, 1, 1, 1);
    gridLayout->addWidget(xSpinBox, 0, 2, 1, 1);
    gridLayout->addWidget(yStepLabel, 1, 3, 1, 1);
    gridLayout->addWidget(zSpinBox, 2, 2, 1, 1);
    gridLayout->addWidget(xStepSpinBox, 0, 4, 1, 1);
    gridLayout->addWidget(xStepLabel, 0, 3, 1, 1);
    gridLayout->addWidget(showXSliceCheckbox, 0, 0, 1, 1);
    gridLayout->addWidget(showYSliceCheckbox, 1, 0, 1, 1);
    gridLayout->addWidget(showZSliceCheckbox, 2, 0, 1, 1);
    gridLayout->addWidget(yStepSpinBox, 1, 4, 1, 1);
    gridLayout->addWidget(zLabel, 2, 1, 1, 1);
    gridLayout->addWidget(zStepLabel, 2, 3, 1, 1);
    gridLayout->addWidget(zStepSpinBox, 2, 4, 1, 1);
    gridLayout->addWidget(yLabel, 1, 1, 1, 1);
    gridLayout->addWidget(ySpinBox, 1, 2, 1, 1);

    connect(showXSliceCheckbox,SIGNAL(clicked(bool)),this,SLOT(onXSliceShow(bool)));
    connect(showYSliceCheckbox,SIGNAL(clicked(bool)),this,SLOT(onYSliceShow(bool)));
    connect(showZSliceCheckbox,SIGNAL(clicked(bool)),this,SLOT(onZSliceShow(bool)));

    connect(xSpinBox,SIGNAL(valueChanged(int)),this,SLOT(onXSliceValueChanged(int)));
    connect(ySpinBox,SIGNAL(valueChanged(int)),this,SLOT(onYSliceValueChanged(int)));
    connect(zSpinBox,SIGNAL(valueChanged(int)),this,SLOT(onZSliceValueChanged(int)));

    connect(xStepSpinBox,SIGNAL(valueChanged(int)),this,SLOT(onXSliceStepValueChanged(int)));
    connect(yStepSpinBox,SIGNAL(valueChanged(int)),this,SLOT(onYSliceStepValueChanged(int)));
    connect(zStepSpinBox,SIGNAL(valueChanged(int)),this,SLOT(onZSliceStepValueChanged(int)));
}

void MainWindow::fillSketchesGroup(QGroupBox *group)
{
    group->setTitle("Sketches");
    surfaceModelingWidget = new SurfaceModelingWidget(group,openGLWidget);

    QVBoxLayout* layout = new QVBoxLayout(group);
    layout->addWidget(surfaceModelingWidget);

//                QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
//                sizePolicy.setHorizontalStretch(0);
//                sizePolicy.setVerticalStretch(0);
//                sizePolicy.setHeightForWidth(group->sizePolicy().hasHeightForWidth());
    //                group->setSizePolicy(sizePolicy);
}

void MainWindow::fillOptionsGroup(QGroupBox *group)
{
    group->setTitle("Options");

    QLabel* methodLabel = new QLabel(group);
    QLabel* environmentLabel = new QLabel(group);
    methodLabel->setText("Method:");
    environmentLabel->setText("Environment:");

    methodComboBox = new QComboBox(group);
    structureComboBox = new QComboBox(group);

    methodComboBox->insertItems(0, QStringList() << "Laplace operator");
    structureComboBox->insertItems(0, QStringList()
                                   << "Regular grid"
                                   << "Octree"
                                   << "Tetrahedral mesh");

    surfaceClosedCheckBox = new QCheckBox("Closed surface",group);
    smoothnessCheckBox = new QCheckBox("Smooth surface",group);
    smoothnessCheckBox->setChecked(true);

    QPushButton* executeButton = new QPushButton("Run",group);
    connect(executeButton,SIGNAL(clicked()),this,SLOT(onRunReconstruction()));

    QWidget* gridSizeWidget = new QWidget(group);
    QHBoxLayout* horizontalLayout = new QHBoxLayout(gridSizeWidget);
    QLabel* gridSizeLabel = new QLabel("Grid size:",gridSizeWidget);
    QSpinBox* gridXSizeSpinBox = new QSpinBox(gridSizeWidget);
    QSpinBox* gridYSizeSpinBox = new QSpinBox(gridSizeWidget);
    QSpinBox* gridZSizeSpinBox = new QSpinBox(gridSizeWidget);

    horizontalLayout->addWidget(gridSizeLabel);
    horizontalLayout->addWidget(gridXSizeSpinBox);
    horizontalLayout->addWidget(gridYSizeSpinBox);
    horizontalLayout->addWidget(gridZSizeSpinBox);

    gridSizeWidget->hide();

    QGridLayout* reconstructionGridLayout = new QGridLayout(group);
    reconstructionGridLayout->setContentsMargins(0, 0, 0, 0);
    reconstructionGridLayout->addWidget(methodLabel, 0, 0, 1, 2);
    reconstructionGridLayout->addWidget(methodComboBox, 0, 2, 1, 1);
    reconstructionGridLayout->addWidget(environmentLabel, 1, 0, 1, 2);
    reconstructionGridLayout->addWidget(structureComboBox, 1, 2, 1, 1);
    reconstructionGridLayout->addWidget(smoothnessCheckBox, 2, 0, 1, 1);
    reconstructionGridLayout->addWidget(gridSizeWidget, 2, 1, 1, 2);
    reconstructionGridLayout->addWidget(surfaceClosedCheckBox, 3, 0, 1, 2);
    reconstructionGridLayout->addWidget(executeButton, 3, 2, 1, 1);
}

void MainWindow::fillSurfacesGroup(QGroupBox *group)
{
    group->setTitle("Surfaces");

    QVBoxLayout* layout = new QVBoxLayout(group);

    surfacesList = new QListWidget(group);
    layout->addWidget(surfacesList);

    connect(surfacesList,SIGNAL(itemChanged(QListWidgetItem*)),this,SLOT(onSurfaceListItemChanged(QListWidgetItem*)));
}

void MainWindow::fillSurfacesList()
{
    auto& scene = openGLWidget->getScene();

    surfacesList->clear();

    QStringList names;
    for(const auto& surface : scene.objects)
    {
        names << surface.name.c_str();
    }

    surfacesList->addItems(names);
}

void MainWindow::onCanvasInitialized()
{
    onResizeVolume();

    doTest();

    auto sketchingOperator = openGLWidget->createSliceSketchingOperator();
    surfaceModelingWidget->setSliceSketchingOperator(sketchingOperator);
}

void MainWindow::onResizeVolume()
{
    NewVolume::Info info;
    info.numXSlices = volumeXSizeSpinBox->value();
    info.numYSlices = volumeYSizeSpinBox->value();
    info.numZSlices = volumeZSizeSpinBox->value();
    info.computeWorldPoints({1.0f,1.0f,1.0f});

    NewVolume* volume = new NewVolume(info);
    openGLWidget->setVolume(volume);

    xSpinBox->setRange(0,info.numXSlices-1);
    ySpinBox->setRange(0,info.numYSlices-1);
    zSpinBox->setRange(0,info.numZSlices-1);

    xStepSpinBox->setRange(1,info.numXSlices-1);
    yStepSpinBox->setRange(1,info.numYSlices-1);
    zStepSpinBox->setRange(1,info.numZSlices-1);
}

void MainWindow::onRunReconstruction()
{
    std::vector< glm::vec3 > pointCloud;
    surfaceModelingWidget->getSketchesPointCloud(pointCloud);

    auto isClosed = surfaceClosedCheckBox->isChecked();
    auto isSmooth = smoothnessCheckBox->isChecked();

    auto structure = structureComboBox->currentIndex();

    std::vector<glm::vec3> vertices;
    std::vector< unsigned int> indices;

    if(structure == 0) //Regular grid
    {
        const auto& info = openGLWidget->getVolume()->getInfo();

        GridSurfaceReconstructor sfRec(pointCloud,isClosed);
        sfRec.setGridResolution(info.numXSlices+2,info.numYSlices+2,info.numZSlices+2);
        sfRec.setSmooth(isSmooth);
        sfRec.run();

        sfRec.getSurfaceMesh(vertices,indices);
    }
    else if(structure == 2) //Tetrahedral mesh
    {
        const auto& info = openGLWidget->getVolume()->getInfo();

        auto points = pointCoordsToVolumeIndexes(info,pointCloud);

//        auto size = info.numXSlices*info.numYSlices*info.numZSlices;
//        NewVolume* binaryVolume = new NewVolume(info);
//        auto binaryBuffer = binaryVolume->getBuffer();

//        const float INF = 1e20;

//        std::fill(binaryBuffer,binaryBuffer+size,INF);

//        for(const auto& p : points)
//        {
//            auto idx = binaryVolume->getIndex(p.x,p.y,p.z);

//            binaryBuffer[idx] = 0.0f;
//        }

//        NewVolume* distanceImage = computeEDT(binaryVolume);
//        delete binaryVolume;

//        openGLWidget->setVolume(distanceImage);

        TetrahedraSurfaceReconstructor sfRec(pointCloud,isClosed);
        sfRec.setVolumeInfo(info);
        sfRec.run();

        openGLWidget->setVolume(sfRec.getDistanceField());
    }
    else
        return;

    std::vector<glm::vec3> normals(vertices.size(),glm::vec3(0,0,0));
    for(int i = 0; i < (int)indices.size()-2; i+=3)
    {
        auto i0 = indices[i+0];
        auto i1 = indices[i+1];
        auto i2 = indices[i+2];

        auto u = vertices[i1]-vertices[i0];
        auto v = vertices[i2]-vertices[i0];

        auto n = glm::cross(u,v);

        normals[i0] += n;
        normals[i1] += n;
        normals[i2] += n;
    }

    auto& scene = openGLWidget->getScene();

    auto nObjs = scene.objects.size();


    std::stringstream name;
    name << "surface_" << (isClosed ? "closed_" : "open_") << nObjs;

    scene.addObject(vertices,normals,indices,
                    glm::vec3(0.1f,0.04f,0.37f), name.str());

    openGLWidget->update();

    QListWidgetItem* item = new QListWidgetItem(surfacesList);
    item->setText(name.str().c_str());
    item->setCheckState(Qt::CheckState::Checked);

    surfaceModelingWidget->stopSketching();



//    auto inputVolume = canvas->getScene().getVolume();
//    auto indexes = pointCoordsToVolumeIndexes(inputVolume,pointCloud);

//    std::ofstream output;
//    output.open("pointcloud.txt",std::ios::out);

//    output << pointCloud.size() << "\n";
//    for(unsigned int i = 0; i < pointCloud.size(); ++i)
//    {
//        output << pointCloud[i].x << " " << pointCloud[i].y << " " << pointCloud[i].z << " ";
//        output << indexes[i].x << " " << indexes[i].y << " " << indexes[i].z << "\n";
//    }

//    output.close();

//    writeOFF("teste.off",vertices,indices);

//    auto info = inputVolume->getInfo();
//    auto size = info.numXSlices*info.numYSlices*info.numZSlices;

//    auto binaryVolume = inputVolume->getCopy();
//    float* buffer = binaryVolume->getBuffer();
//    std::fill(buffer, buffer+size, 0.0f);

//    for(auto pointCoord : indexes)
//    {
//        auto idx = inputVolume->getIndex(pointCoord.x,pointCoord.y,pointCoord.z);
//        buffer[idx] = 1.0f;
//    }

//    auto distanceImage = distanceTransform(binaryVolume);
//    delete binaryVolume;

//    auto& renderer = canvas->getScene().getVolumeRenderer();
//    renderer.setVolume(distanceImage);

//    buffer = distanceImage->getBuffer();
//    auto min = *(std::min_element(buffer,buffer+size));
//    auto max = *(std::max_element(buffer,buffer+size));
//    renderer.setMinPixelValue(min);
//    renderer.setMaxPixelValue(max);

    //    canvas->update();
}

void MainWindow::onWireframeToggled(bool wireframeOn)
{
    openGLWidget->setWireframeOn(wireframeOn);
}

void MainWindow::onXSliceShow(bool show)
{
    openGLWidget->showXSlice(show);
}

void MainWindow::onYSliceShow(bool show)
{
    openGLWidget->showYSlice(show);
}

void MainWindow::onZSliceShow(bool show)
{
    openGLWidget->showZSlice(show);
}

void MainWindow::onXSliceValueChanged(int value)
{
    openGLWidget->updateXSlice(value);
}

void MainWindow::onYSliceValueChanged(int value)
{
    openGLWidget->updateYSlice(value);
}

void MainWindow::onZSliceValueChanged(int value)
{
    openGLWidget->updateZSlice(value);
}

void MainWindow::onXSliceStepValueChanged(int value)
{
    xSpinBox->setSingleStep(value);
}

void MainWindow::onYSliceStepValueChanged(int value)
{
    ySpinBox->setSingleStep(value);
}

void MainWindow::onZSliceStepValueChanged(int value)
{
    zSpinBox->setSingleStep(value);
}

void MainWindow::onSurfaceListItemChanged(QListWidgetItem *item)
{
    auto index = surfacesList->row(item);

    openGLWidget->getScene().showObject(index,item->checkState() == Qt::Checked);

    openGLWidget->update();
}

void samplePoints(unsigned int numXPoints, unsigned int numYPoints,
                  std::function<float(float x,float y)> f,
                  const glm::vec3& min, const glm::vec3& max,
                  std::vector<glm::vec3>& pts)
{


    float xStep = (max.x-min.x)/(numXPoints-1);
    float yStep = (max.y-min.y)/(numYPoints-1);

    float y = min.y;
    for(unsigned int j = 0; j < numYPoints; ++j)
    {
        float x = min.x;
        for(unsigned int i = 0; i < numXPoints; ++i)
        {
            pts.push_back({x,f(x,y),y});
            x+=xStep;
        }

        y+=yStep;
    }
}

void MainWindow::doTest()
{
    //SenxCosY - Tetraedros
    std::string senxcosyResultMeshPath = "senxcosy-tetrahedra.off";

    auto f = [](float x,float y) {return std::sin(x)*std::cos(y);};

    glm::vec3 min(0,0,-1);
    glm::vec3 max(2*M_PI,2*M_PI,1);

    std::vector<glm::vec3> pts;
    samplePoints(NUM_SLICES,NUM_SLICES,f,min,max,pts);

    /* Volume bounding box global coordinates
    *
    *       p4------------p3
    *      / |           / |
    *     /  |          /  |
    *    /   p8--------/--p7
    *   p1-----------p2  /
    *    | /          | /
    *    |/           |/
    *   p5-----------p6
    *
    */

    NewVolume::Info info;
    info.numXSlices = info.numYSlices = info.numZSlices = NUM_SLICES;
    info.p1.x = info.p4.x = info.p5.x = info.p8.x = min.x;
    info.p2.x = info.p3.x = info.p6.x = info.p7.x = max.x;
    info.p5.y = info.p6.y = info.p7.y = info.p8.y = min.z*2;
    info.p1.y = info.p2.y = info.p3.y = info.p4.y = max.z*2;
    info.p1.z = info.p2.z = info.p5.z = info.p6.z = min.y;
    info.p3.z = info.p4.z = info.p7.z = info.p8.z = max.y;

    TetrahedraSurfaceReconstructor sfRec(pts);
    sfRec.setVolumeInfo(info);
    sfRec.run();
    openGLWidget->setVolume(sfRec.getDistanceField());

//    GridSurfaceReconstructor sfRec(pts);
//    sfRec.setGridResolution(20,20,20);
//    sfRec.run();


    std::vector<glm::vec3> vertices;
    std::vector< unsigned int> indices;
    sfRec.getSurfaceMesh(vertices,indices);

    writeOFF(senxcosyResultMeshPath,vertices,indices);
}
