#ifndef RAYCASTING_H
#define RAYCASTING_H

#include "scene.h"
#include <glm/glm.hpp>

glm::vec3 rayDirection( const glm::vec2& screenPoint, const ArcballCamera& camera );

/**
 * Implemented follow this site: http://geomalgorithms.com/
 * a06-_intersect-2.html
 */
bool interceptsTriangle( const glm::vec3& orig, const glm::vec3& dir,
                        const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2,
                        float& dist );

// Verifica se o raio intercepta algum triângulo de algum objeto da cena
// Algoritmo de Moller-Trumbore.
// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
// Se sim, retorna true e preenche o índice "object" do objeto interceptado
// e o ponto de interseção "point"
bool intercepts( const glm::vec2& screenPoint, const Scene& scene, int& object, glm::vec3& point );


#endif // RAYCASTING_H
