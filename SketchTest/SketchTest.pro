#-------------------------------------------------
#
# Project created by QtCreator 2018-06-28T14:03:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SketchTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    canvas3dwidget.cpp \
    scene.cpp \
    volumerenderer.cpp \
    mesh.cpp \
    camera.cpp \
    raycasting.cpp \
    newvolume.cpp \
    slicesketchingoperator.cpp \
    surfacemodelingdockwidget.cpp \
    reconstruction/io.cpp \
    reconstruction/gridsurfacereconstruction.cpp \
    reconstruction/SurfaceModeling/Graph/DSIGraph.cpp \
    reconstruction/SurfaceModeling/ModelManager/Constraint.cpp \
    reconstruction/SurfaceModeling/ModelManager/ConstraintRepresentation.cpp \
    reconstruction/SurfaceModeling/ModelManager/DSIModel.cpp \
    reconstruction/SurfaceModeling/ModelManager/LinearExpression.cpp \
    reconstruction/SurfaceModeling/ModelManager/Model.cpp \
    reconstruction/SurfaceModeling/ModelManager/OptimizationModel.cpp \
    reconstruction/SurfaceModeling/ModelManager/PreprocessingEqualityConstraints.cpp \
    reconstruction/SurfaceModeling/ModelManager/PreprocessingSimpleConstraints.cpp \
    reconstruction/SurfaceModeling/ModelManager/QuadraticExpression.cpp \
    reconstruction/SurfaceModeling/ModelManager/Variable.cpp \
    reconstruction/SurfaceModeling/ModelManager/VariableRepresentation.cpp \
    reconstruction/SurfaceModeling/Solver/DSISolvers/ConjugateGradientDSIBasedSolver.cpp \
    reconstruction/SurfaceModeling/Solver/DSISolvers/DSIBasedSolver.cpp \
    reconstruction/SurfaceModeling/Solver/DSISolvers/GlobalDSIBasedSolver.cpp \
    reconstruction/SurfaceModeling/Solver/DSISolvers/LocalDSIBasedSolver.cpp \
    reconstruction/SurfaceModeling/Solver/DSISolvers/PseudoConjugateGradientDSIBasedSolver.cpp \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/LBFGSOptimizationSolver.cpp \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/OptimizationSolver.cpp \
    reconstruction/SurfaceModeling/Solver/Solver.cpp \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.cpp \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/PCGJacobiOptimizationSolver.cpp \
    reconstruction/MarchingCubes/MarchingCubes.cpp \
    reconstruction/MarchingCubes/ply.c \
    reconstruction/surfacereconstruction.cpp \
    volumealgorithms.cpp \
    reconstruction/tetrahedrasurfacereconstruction.cpp \
    reconstruction/defs.cpp \
    reconstruction/graph.cpp

HEADERS  += mainwindow.h \
    canvas3dwidget.h \
    scene.h \
    volumerenderer.h \
    mesh.h \
    camera.h \
    raycasting.h \
    newvolume.h \
    operator.h \
    slicesketchingoperator.h \
    surfacemodelingdockwidget.h \
    reconstruction/io.h \
    reconstruction/gridsurfacereconstruction.h \
    reconstruction/defs.h \
    reconstruction/SurfaceModeling/Graph/DSIGraph.h \
    reconstruction/SurfaceModeling/ModelManager/Constraint.h \
    reconstruction/SurfaceModeling/ModelManager/ConstraintRepresentation.h \
    reconstruction/SurfaceModeling/ModelManager/DSIModel.h \
    reconstruction/SurfaceModeling/ModelManager/Enums.h \
    reconstruction/SurfaceModeling/ModelManager/LinearExpression.h \
    reconstruction/SurfaceModeling/ModelManager/Model.h \
    reconstruction/SurfaceModeling/ModelManager/OptimizationModel.h \
    reconstruction/SurfaceModeling/ModelManager/PreprocessingEqualityConstraints.h \
    reconstruction/SurfaceModeling/ModelManager/PreprocessingSimpleConstraints.h \
    reconstruction/SurfaceModeling/ModelManager/QuadraticExpression.h \
    reconstruction/SurfaceModeling/ModelManager/Variable.h \
    reconstruction/SurfaceModeling/ModelManager/VariableRepresentation.h \
    reconstruction/SurfaceModeling/Solver/DSISolvers/ConjugateGradientDSIBasedSolver.h \
    reconstruction/SurfaceModeling/Solver/DSISolvers/DSIBasedSolver.h \
    reconstruction/SurfaceModeling/Solver/DSISolvers/GlobalDSIBasedSolver.h \
    reconstruction/SurfaceModeling/Solver/DSISolvers/LocalDSIBasedSolver.h \
    reconstruction/SurfaceModeling/Solver/DSISolvers/PseudoConjugateGradientDSIBasedSolver.h \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/LBFGSOptimizationSolver.h \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/OptimizationSolver.h \
    reconstruction/SurfaceModeling/Solver/Solver.h \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h \
    reconstruction/SurfaceModeling/Solver/OptimizationSolvers/PCGJacobiOptimizationSolver.h \
    reconstruction/MarchingCubes/ply.h \
    reconstruction/MarchingCubes/MarchingCubes.h \
    reconstruction/MarchingCubes/LookUpTable.h \
    reconstruction/surfacereconstruction.h \
    volumealgorithms.h \
    reconstruction/tetrahedrasurfacereconstruction.h \
    reconstruction/graph.h

FORMS    += \
    surfacemodelingdockwidget.ui

linux {
    INCLUDEPATH += $$PWD/../dependencies/linux/include
    DEPENDPATH += $$PWD/../dependencies/linux/include

    LIBS += -L$$PWD/../dependencies/linux/lib/

    LIBS += -lOpenMeshCore -lartme -lCGAL -lgmp -lmpfr
}

RESOURCES += \
    data.qrc

QMAKE_CXXFLAGS+= -fopenmp -D_LONG__LONG
QMAKE_LFLAGS +=  -fopenmp -D_LONG__LONG

unix:!macx: LIBS += -L$$PWD/reconstruction/SurfaceModeling/libs/lis/lib/ -llis

INCLUDEPATH += $$PWD/reconstruction/SurfaceModeling/libs/lis/include
DEPENDPATH += $$PWD/reconstruction/SurfaceModeling/libs/lis/include

unix:!macx: PRE_TARGETDEPS += $$PWD/reconstruction/SurfaceModeling/libs/lis/lib/liblis.a
