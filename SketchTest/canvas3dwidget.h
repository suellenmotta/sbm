#ifndef CANVAS3DWIDGET_H
#define CANVAS3DWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QVector2D>
#include <QMessageBox>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

#include "scene.h"
#include "operator.h"
//#include "seismicvolume.h"

//#include "borderselectionoperator.h"
#include "slicesketchingoperator.h"

#include <memory>

class NewVolume;
class SliceSketchingOperator;

class Canvas3DWidget
    : public QOpenGLWidget
    , protected QOpenGLExtraFunctions
{
Q_OBJECT

public:
    explicit Canvas3DWidget(QWidget *parent = 0);
    ~Canvas3DWidget();

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    void setBackgroundColor(const QColor& color);
    QColor getBackgroundColor();

    void setVolume(NewVolume* volume);
    const NewVolume* getVolume();

    void showXSlice(bool show = true);
    void showYSlice(bool show = true);
    void showZSlice(bool show = true);

    void updateXSlice(unsigned int sliceNum);
    void updateYSlice(unsigned int sliceNum);
    void updateZSlice(unsigned int sliceNum);

    void setWireframeOn(bool on);

    void setOperator(std::shared_ptr<Operator> op );
    std::shared_ptr<Operator> getOperator();

//    std::shared_ptr<BorderSelectionOperator> createBorderSelectionOperator();
    std::shared_ptr<SliceSketchingOperator> createSliceSketchingOperator();

    Scene& getScene();

signals:
    void canvasInitialized();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    Scene scene;
    std::shared_ptr<Operator> currentOperator;
    glm::ivec2 oldPos;





//    void clearScene();
//    void loadCube();
//    void loadSphere();
//    void loadBunny();
//    void loadVolume(const QString& path);
//    void loadObject(const QString &path,
//                    const glm::vec3& color = glm::vec3(0.4,0.4,0.4),
//                    const QString &name = "");



//    void getVolumeBounds(unsigned int& xSliceFirst,
//                         unsigned int& xSliceLast,
//                         unsigned int& xSliceStep,
//                         unsigned int& ySliceFirst,
//                         unsigned int& ySliceLast,
//                         unsigned int& ySliceStep,
//                         unsigned int& zSliceFirst,
//                         unsigned int& zSliceLast,
//                         unsigned int& zSliceStep);



//    void showObject(unsigned int objIndex, bool show = true);

//    void setROISelectionOperator();

//    void setOperator(std::shared_ptr<Operator> op );
//    std::shared_ptr<Operator> getOperator();

//    std::shared_ptr<BorderSelectionOperator> createBorderSelectionOperator();
//    std::shared_ptr<SliceSketchingOperator> createSliceSketchingOperator();

//signals:
//    void clicked();
//    void updateMousePositionText(const QString& message);
//    void viewChanged();



//private:


//    seismic::Info info;

//

};

#endif // CANVAS3DWIDGET_H
