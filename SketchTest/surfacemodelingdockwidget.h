#ifndef SURFACEMODELINGDOCKWIDGET_H
#define SURFACEMODELINGDOCKWIDGET_H

#include <QWidget>
#include <memory>

class BorderSelectionOperator;
class SliceSketchingOperator;
class Canvas3DWidget;
class QListWidget;
class QToolButton;

#include <glm/glm.hpp>

class SurfaceModelingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SurfaceModelingWidget(QWidget* parent, Canvas3DWidget* canvas);

    void setSliceSketchingOperator(std::shared_ptr<SliceSketchingOperator> sketchingOperator);

    void getSketchesPointCloud(std::vector<glm::vec3>& pointCloud);

    ~SurfaceModelingWidget();

    void stopSketching();

public slots:
    void onSketchFinished();

private slots:
    void on_addSketchButton_toggled(bool checked);
    void on_deleteSketchButton_clicked();
    void on_showAllSketches_toggled(bool checked);
    void on_sketchesListWidget_itemSelectionChanged();

    void on_pointCloudButton_clicked(bool checked);
    void on_viewModeButton_clicked(bool checked);
    void on_resetButton_clicked();
    void on_generateButton_clicked();

    void on_SurfaceModelingDockWidget_visibilityChanged(bool visible);

private:
    void fillSketchesList();

    Canvas3DWidget* canvas;
    QListWidget* sketchesListWidget;

    QToolButton* addSketchButton;

    std::shared_ptr<SliceSketchingOperator> sketchingOperator;
};

#endif // SURFACEMODELINGDOCKWIDGET_H
