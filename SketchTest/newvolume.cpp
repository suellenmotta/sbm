#include "newvolume.h"
#include <nrrd/NRRD/nrrd_image.hxx>

#include <algorithm>

void NewVolume::Info::computeWorldPoints(const glm::vec3& spacing)
{
    auto lastX = (numXSlices-1)*spacing.x;
    auto lastY = (numYSlices-1)*spacing.y;
    auto lastZ = (numZSlices-1)*spacing.z;

    p1 = glm::vec3(0,     lastY, 0    );
    p2 = glm::vec3(lastX, lastY, 0    );
    p3 = glm::vec3(lastX, lastY, lastZ);
    p4 = glm::vec3(0,     lastY, lastZ);
    p5 = glm::vec3(0,     0,     0    );
    p6 = glm::vec3(lastX, 0,     0    );
    p7 = glm::vec3(lastX, 0,     lastZ);
    p8 = glm::vec3(0,     0,     lastZ);
}


NewVolume::NewVolume()
    : buffer(nullptr)
{

}

NewVolume::NewVolume(const Info& info)
    : info(info)
    , buffer(nullptr)
{
    auto size=info.numZSlices*info.numYSlices*info.numXSlices;

    if(size>0)
    {
        buffer=new float[size];
        std::fill(buffer,buffer+size,0.0f);
    }
}

NewVolume::NewVolume(const Info& info, float* buffer)
    : info(info)
    , buffer(buffer)
{

}

NewVolume::NewVolume(const NewVolume &other)
    : info(other.info)
{
    auto size = other.numXSlices()*other.numYSlices()*other.numZSlices();
    buffer = new float[size];
    std::copy(other.buffer, other.buffer+size, buffer);
}

NewVolume::~NewVolume()
{
    if(buffer)
        delete[] buffer;
}

NewVolume *NewVolume::getCopy() const
{
    return new NewVolume(*this);
}

float NewVolume::getValue(unsigned int x, unsigned int y, unsigned int z) const
{
    return buffer[getIndex(x,y,z)];
}

unsigned int NewVolume::getIndex(unsigned int x,
                                 unsigned int y,
                                 unsigned int z) const
{
    return info.numXSlices*info.numYSlices*z + info.numXSlices*y + x;
}

NewVolume::Info NewVolume::getInfo() const
{
    return info;
}

float *NewVolume::getBuffer() const
{
    return buffer;
}

float* NewVolume::getXSliceBuffer(unsigned int index,
                                   unsigned int &width, unsigned int &height)
{
    if(index>=info.numXSlices)
        return nullptr;

    width = info.numZSlices;
    height = info.numYSlices;

    auto sliceSize = width*height;
    float* slice = new float[sliceSize];

    int pos = 0;
    for(unsigned int y = 0; y < height; ++y)
    {
        for(unsigned int z = 0; z < width; ++z)
        {
            slice[pos++] = buffer[getIndex(index,y,z)];
        }
    }

    return slice;
}

float* NewVolume::getYSliceBuffer(unsigned int index,
                                   unsigned int &width, unsigned int &height)
{
    if(index>=info.numYSlices)
        return nullptr;

    width = info.numXSlices;
    height = info.numZSlices;

    auto sliceSize = width*height;
    float* slice = new float[sliceSize];

    int pos = 0;
    for(unsigned int z = 0; z < height; ++z)
    {
        for(unsigned int x = 0; x < width; ++x)
        {
            slice[pos++] = buffer[getIndex(x,index,z)];
        }
    }

    return slice;
}

float* NewVolume::getZSliceBuffer(unsigned int index,
                                  unsigned int& width, unsigned int& height)
{
    if(index>=info.numZSlices)
        return nullptr;

    width = info.numXSlices;
    height = info.numYSlices;

    auto sliceSize = width*height;
    float* slice = new float[sliceSize];

    int pos = 0;
    for(unsigned int y = 0; y < height; ++y)
    {
        for(unsigned int x = 0; x < width; ++x)
        {
            slice[pos++] = buffer[getIndex(x,y,index)];
        }
    }

    return slice;
}

void NewVolume::getXSlicePoints(unsigned int index,
                      std::vector<glm::vec3> &points)
{
    if(index>=info.numXSlices)
        return;

    auto step = info.numXSlices>0 ?
                (float)index*(info.p2-info.p1)/(info.numXSlices-1.0f) : glm::vec3();

    points.reserve(4);
    points.push_back(info.p5+step);
    points.push_back(info.p8+step);
    points.push_back(info.p1+step);
    points.push_back(info.p4+step);
}

void NewVolume::getYSlicePoints(unsigned int index,
                      std::vector<glm::vec3> &points)
{
    if(index>=info.numYSlices)
        return;

    auto step = info.numYSlices>0 ?
                (float)index*(info.p1-info.p5)/(info.numYSlices-1.0f) : glm::vec3();

    points.reserve(4);
    points.push_back(info.p5+step);
    points.push_back(info.p6+step);
    points.push_back(info.p8+step);
    points.push_back(info.p7+step);
}

void NewVolume::getZSlicePoints(unsigned int index,
                      std::vector<glm::vec3> &points)
{
    if(index>=info.numZSlices)
        return;

    auto step = info.numZSlices>0 ?
                (float)index*(info.p4-info.p1)/(info.numZSlices-1.0f) : glm::vec3();

    points.reserve(4);
    points.push_back(info.p5+step);
    points.push_back(info.p6+step);
    points.push_back(info.p1+step);
    points.push_back(info.p2+step);
}

glm::vec3 NewVolume::getCenter()
{
    return (info.p1+info.p7)*0.5f;
}

float NewVolume::getCircumscribedSphereRadius()
{
    return glm::distance(getCenter(),info.p1);
}

glm::mat4 NewVolume::getStartModelMatrix()
{
    return glm::mat4();
}

NewVolume* readNRRD(const std::string& filePath)
{
    NRRD::Image<float> img(filePath);
    if (!img)
    {
        std::cerr << "Failed to read file.nrrd.\n";
        return nullptr;
    }

    if (img.dimension()!=3)
    {
        std::cerr << "Image dimension different than 3.\n";
        return nullptr;
    }

    NewVolume::Info info;
    info.numXSlices = img.size(0);
    info.numYSlices = img.size(1);
    info.numZSlices = img.size(2);
    info.computeWorldPoints({img.spacing(0),img.spacing(1),img.spacing(2)});

    return new NewVolume(info,img);
}
