#ifndef VOLUMERENDERER_H
#define VOLUMERENDERER_H


#include <QOpenGLExtraFunctions>
#include <QOpenGLShaderProgram>

#include "newvolume.h"
#include "camera.h"

class VolumeRenderer
    : public QOpenGLExtraFunctions
{
public:
    VolumeRenderer(ArcballCamera &camera);
    virtual ~VolumeRenderer();

    void init();

    const NewVolume* getVolume();
    void setVolume(NewVolume* volume);

    void updateXSlice(unsigned int index);
    void updateYSlice(unsigned int index);
    void updateZSlice(unsigned int index);

    unsigned int getCurrentXSlice() const;
    unsigned int getCurrentYSlice() const;
    unsigned int getCurrentZSlice() const;

    void showXSlice(bool show = true);
    void showYSlice(bool show = true);
    void showZSlice(bool show = true);

    bool isXSliceShown() const;
    bool isYSliceShown() const;
    bool isZSliceShown() const;

    void setMinPixelValue(float minValue);
    void setMaxPixelValue(float maxValue);

    void draw();

    const ArcballCamera& getCamera();

    bool intercepts(const glm::ivec2 &screenPos, NewVolume::Axis& axis, glm::vec3& point) const;
    bool interceptsXSlice(const glm::vec3 &orig, const glm::vec3 &dir,
                          float& dist) const;
    bool interceptsYSlice(const glm::vec3& orig, const glm::vec3& dir,
                          float& dist) const;
    bool interceptsZSlice(const glm::vec3 &orig, const glm::vec3 &dir,
                          float& dist) const;

protected:
    void updateVBO(const std::vector<glm::vec3>& vertices, GLuint vboID);
    void updateTexture(float* slice, unsigned int width,
                       unsigned int height, GLuint textureID);
    void updateColorTableTexture(float* colorTable, unsigned int numColors);

    QOpenGLShaderProgram program;

    GLuint VAO;

    GLuint xSliceVBO;
    GLuint ySliceVBO;
    GLuint zSliceVBO;

    GLuint xSliceTexture;
    GLuint ySliceTexture;
    GLuint zSliceTexture;

    int xSliceIndex;
    int ySliceIndex;
    int zSliceIndex;

    bool xSliceShow;
    bool ySliceShow;
    bool zSliceShow;

    float xSliceMinVal, xSliceMaxVal;
    float ySliceMinVal, ySliceMaxVal;
    float zSliceMinVal, zSliceMaxVal;

    NewVolume* volume;

    ArcballCamera& cam;

    std::vector<float> lutMarkers;
    GLuint lutTexture;

    bool isInitialized;
    bool correctColor;
};

#endif // VOLUMERENDERER_H
