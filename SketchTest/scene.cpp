#include "scene.h"

#include <glm/ext.hpp>


Scene::Scene()
    : changed(true)
    , volumeRenderer(camera)
    , wireframeOn(false)
{

}

Scene::~Scene()
{
    glDeleteTextures(1,&sceneTex);
    glDeleteTextures(1,&pickingTex);
    glDeleteTextures(1,&depthTex);
    glDeleteFramebuffers(1,&fbo);

    glDeleteTextures(1,&wireframeTex);
    objects.clear();
    glDeleteVertexArrays(1,&objectsVAO);
}

void Scene::init()
{
    initializeOpenGLFunctions();

    volumeRenderer.init();

    objectsProgram.addShaderFromSourceFile( QOpenGLShader::Vertex, ":/shaders/bp-primitive-vert" );
    objectsProgram.addShaderFromSourceFile( QOpenGLShader::Geometry, ":/shaders/bp-primitive-geom" );
    objectsProgram.addShaderFromSourceFile( QOpenGLShader::Fragment, ":/shaders/bp-primitive-frag" );
    objectsProgram.link();

    glGenVertexArrays(1, &objectsVAO);
    glBindVertexArray(objectsVAO);
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
    glVertexAttribBinding(0,0);
    glVertexAttribBinding(1,0);
    glBindVertexArray(0);

    createFramebuffer();

    createWireframeTexture(1.0f);
}

void Scene::reset()
{
    volumeRenderer.setVolume(nullptr);

    objects.clear();

    changed = true;
}

const NewVolume *Scene::getVolume()
{
    return volumeRenderer.getVolume();
}

void Scene::setVolume(NewVolume* volume)
{
    volumeRenderer.setVolume(volume);
    changed = true;
}

VolumeRenderer &Scene::getVolumeRenderer()
{
    return volumeRenderer;
}

void Scene::addObject(const std::vector<glm::vec3>& vertices,
               const std::vector<glm::vec3>& normals,
               const std::vector<unsigned int>& indices,
               const glm::vec3& diffuseColor,
               const std::string& name)
{
    std::vector<opengl::Vertex> vertexBuffer;

    auto size = glm::min(vertices.size(),normals.size());

    for(auto i = 0; i < size; ++i)
    {
        vertexBuffer.push_back({vertices[i],glm::normalize(normals[i])});
    }

    addObject(vertexBuffer,indices,diffuseColor,name);
}

void Scene::addObject(const std::vector<opengl::Vertex>& vertexBuffer,
               const std::vector<unsigned int> &indices,
               const glm::vec3& diffuseColor,
               const std::string &name)
{
    objects.emplace_back();
    showObjects.push_back(true);

    auto& model = objects[objects.size()-1];

    model.vertexBuffer = vertexBuffer;
    model.indices = indices;
    model.material.diffuse = diffuseColor;
    model.name = name;

    model.create();
    changed = true;
}

void Scene::bindVAO()
{
    glBindVertexArray(objectsVAO);
}

void Scene::draw(GLuint defaultFBO)
{
    if(changed || camera.changed)
    {
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glClearColor(clearColor.r,clearColor.g,clearColor.b,1.0f);

        glBindFramebuffer(GL_FRAMEBUFFER,fbo);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        GLint invalidIDs[2] = {-1,-1};
        glClearBufferiv(GL_COLOR,1,invalidIDs);

        volumeRenderer.draw();

        objectsProgram.bind();

        QMatrix4x4 m(glm::value_ptr(glm::transpose(camera.modelMatrix())));
        QMatrix4x4 v(glm::value_ptr(glm::transpose(camera.viewMatrix())));
        QMatrix4x4 p(glm::value_ptr(glm::transpose(camera.projMatrix())));

        auto mv = v*m;
        auto mvp = p*mv;

        objectsProgram.setUniformValue("MV", mv);
        objectsProgram.setUniformValue("MV_TI", mv.inverted().transposed());
        objectsProgram.setUniformValue("MVP", mvp);

        objectsProgram.setUniformValue("light.position", QVector3D(0.0f,0.0f,0.0f) );
        objectsProgram.setUniformValue("material.ambient", QVector3D(0.1f, 0.1f, 0.1f));
        objectsProgram.setUniformValue("material.specular", QVector3D(0.25f, 0.25f, 0.25f));
        objectsProgram.setUniformValue("material.shininess", 32.0f);

        objectsProgram.setUniformValue("wireframe", wireframeOn);
        glActiveTexture( GL_TEXTURE0 );
        glBindTexture( GL_TEXTURE_1D, wireframeTex );
        objectsProgram.setUniformValue("wireframeTexture",0);
        objectsProgram.setUniformValue("wireframeColor", QVector3D(0.9f,0.9f,0.9f));

        bindVAO();

        for(unsigned int i = 0; i < objects.size(); ++i)
        {
            if(showObjects[i])
            {
                auto& obj = objects[i];
                const auto& color = obj.material.diffuse;
                objectsProgram.setUniformValue("material.diffuse",
                                               color.r, color.g, color.b);
                objectsProgram.setUniformValue("objectID",(int)i);
                obj.draw();
            }
        }

        glBindVertexArray(0);
        objectsProgram.release();

        changed = camera.changed = false;
    }

    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, defaultFBO);
    glBlitFramebuffer(0, 0, camera.width, camera.height,
                      0, 0, camera.width, camera.height,
                      GL_COLOR_BUFFER_BIT, GL_NEAREST);

    glReadBuffer(GL_DEPTH_ATTACHMENT);
    glBlitFramebuffer(0, 0, camera.width, camera.height,
                      0, 0, camera.width, camera.height,
                      GL_DEPTH_BUFFER_BIT, GL_NEAREST);
}

void Scene::resize(int width, int height)
{
    glViewport(0, 0, width, height);
    camera.resize(width,height);

    glBindTexture(GL_TEXTURE_2D,sceneTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

    glBindTexture(GL_TEXTURE_2D,pickingTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32I, width, height, 0, GL_RG_INTEGER, GL_INT, 0);

    glBindTexture(GL_TEXTURE_2D,depthTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

    changed = true;
}

void Scene::setWireframeOn(bool wireframeOn)
{
    this->wireframeOn = wireframeOn;
    changed = true;
}

void Scene::setClearColor(const glm::vec3& color)
{
    clearColor = color;
    changed = true;
}

glm::vec3 Scene::getClearColor()
{
    return clearColor;
}

void Scene::showXSlice(bool show)
{
    volumeRenderer.showXSlice(show);
    changed = true;
}

void Scene::showYSlice(bool show)
{
    volumeRenderer.showYSlice(show);
    changed = true;
}

void Scene::showZSlice(bool show)
{
    volumeRenderer.showZSlice(show);
    changed = true;
}

bool Scene::isXSliceShown()
{
    return volumeRenderer.isXSliceShown();
}

bool Scene::isYSliceShown()
{
    return volumeRenderer.isYSliceShown();
}

bool Scene::isZSliceShown()
{
    return volumeRenderer.isZSliceShown();
}

void Scene::showObject(unsigned int objIndex, bool show)
{
    if(objIndex<objects.size())
    {
        showObjects[objIndex] = show;
        changed = true;
    }
}

bool Scene::isObjectShown(unsigned int objIndex)
{
    if(objIndex<objects.size())
    {
        return showObjects[objIndex];
    }
    return false;
}

void Scene::updateXSlice(unsigned int index)
{
    volumeRenderer.updateXSlice(index);
    changed = true;
}

void Scene::updateYSlice(unsigned int index)
{
    volumeRenderer.updateYSlice(index);
    changed = true;
}

void Scene::updateZSlice(unsigned int index)
{
    volumeRenderer.updateZSlice(index);
    changed = true;
}

unsigned int Scene::getCurrentXSlice()
{
    return volumeRenderer.getCurrentXSlice();
}

unsigned int Scene::getCurrentYSlice()
{
    return volumeRenderer.getCurrentYSlice();
}

unsigned int Scene::getCurrentZSlice()
{
    return volumeRenderer.getCurrentZSlice();
}

void Scene::pick(glm::ivec2 screenPos, int &object, int &triangle)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glReadBuffer(GL_COLOR_ATTACHMENT1);

    int value[2] = {-1,-1};
    glReadPixels(screenPos.x, camera.height-screenPos.y, 1, 1, GL_RG_INTEGER, GL_INT, &value[0] );

    object = value[0];
    triangle = value[1];
}

void Scene::getDepth(glm::ivec2 screenPos, float &depth)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glReadBuffer(GL_DEPTH_ATTACHMENT);
    glReadPixels(screenPos.x, camera.height-screenPos.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth );
}

void Scene::createFramebuffer()
{
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer( GL_FRAMEBUFFER, fbo );

    glGenTextures(1, &sceneTex );
    glBindTexture(GL_TEXTURE_2D,sceneTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, camera.width, camera.height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneTex, 0);

    glGenTextures(1, &pickingTex );
    glBindTexture(GL_TEXTURE_2D,pickingTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32I, camera.width, camera.height, 0, GL_RG_INTEGER, GL_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, pickingTex, 0);

    glGenTextures(1, &depthTex );
    glBindTexture(GL_TEXTURE_2D,depthTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, camera.width, camera.height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

    GLenum drawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers( 2, drawBuffers );

    if ( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    {
//        QMessageBox::critical(nullptr,"Erro","Impossivel gerar framebuffer!");
        exit(1);
    }
}

void Scene::createWireframeTexture(int thickness)
{
    glGenTextures(1,&wireframeTex);
    glBindTexture(GL_TEXTURE_1D, wireframeTex);

    int levels = 12;
    int size = pow(2,levels);

    std::vector< float > data( size, 0.0f );
    auto fillData = [&data](float halfThickness, int start, int size)
    {
        std::fill(data.begin()+start, data.end(), 0.f);

        if(halfThickness > 1)
        {
            int borderTexels = (int)halfThickness;
            for(int i = size-1; i >= size-borderTexels && i>=0; i--)
                data[i] = 1.0f;

            if(size-borderTexels > 1)
                data[size-borderTexels-1] = 0.5f;
        }
        else
        {
            data[size-1] = halfThickness;
        }
    };

    fillData(thickness/2.f,0,size);

    int level = 0;
    int curSize = size;
    for(; curSize>4; level++, curSize/=2)
    {
        glTexImage1D( GL_TEXTURE_1D, level, GL_RED, curSize,
                      0, GL_RED, GL_FLOAT, &data[size-curSize] );
    }

    for(float atenuation = 3.f; curSize>0; level++, curSize/=2, atenuation++)
    {
        fillData(thickness/(2*atenuation),size-curSize,size);
        glTexImage1D( GL_TEXTURE_1D, level, GL_RED, curSize,
                      0, GL_RED, GL_FLOAT, &data[size-curSize] );
    }

    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
}

