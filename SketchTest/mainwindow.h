#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>

class QHBoxLayout;
class QGroupBox;
class QSpinBox;
class QComboBox;
class QCheckBox;
class QListWidget;
class Canvas3DWidget;
class SurfaceModelingWidget;
class QListWidgetItem;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:

    void createSideWidget(QHBoxLayout *horizontalLayout);

    void fillSlicesGroup(QGroupBox* group);
    void fillSketchesGroup(QGroupBox* group);
    void fillOptionsGroup(QGroupBox* group);
    void fillSurfacesGroup(QGroupBox* group);

    void fillSurfacesList();

    QToolBar* toolBar;

    QSpinBox* volumeXSizeSpinBox;
    QSpinBox* volumeYSizeSpinBox;
    QSpinBox* volumeZSizeSpinBox;

    QWidget* centralWidget;
    Canvas3DWidget* openGLWidget;
    QListWidget* surfacesList;
    SurfaceModelingWidget* surfaceModelingWidget;

    QAction* drawSketchAct;

    QSpinBox* xSpinBox;
    QSpinBox* ySpinBox;
    QSpinBox* zSpinBox;

    QSpinBox* xStepSpinBox;
    QSpinBox* yStepSpinBox;
    QSpinBox* zStepSpinBox;

    QComboBox* methodComboBox;
    QComboBox* structureComboBox;
    QCheckBox* surfaceClosedCheckBox;
    QCheckBox* smoothnessCheckBox;
    QWidget* gridSizeWidget;

    void doTest();

private slots:
    void onCanvasInitialized();
    void onResizeVolume();
    void onRunReconstruction();

    void onWireframeToggled(bool wireframeOn);

    void onXSliceShow(bool show);
    void onYSliceShow(bool show);
    void onZSliceShow(bool show);

    void onXSliceValueChanged(int value);
    void onYSliceValueChanged(int value);
    void onZSliceValueChanged(int value);

    void onXSliceStepValueChanged(int value);
    void onYSliceStepValueChanged(int value);
    void onZSliceStepValueChanged(int value);

    void onSurfaceListItemChanged(QListWidgetItem* item);

};

#endif // MAINWINDOW_H
