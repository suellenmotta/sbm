#include "canvas3dwidget.h"

//#include <QGLFormat>
//#include <QOpenGLShaderProgram>
//#include <QOpenGLTexture>

//#include <QMouseEvent>
//#include <QFileInfo>
//#include <QtMath>

//#include <QMatrix4x4>

//#include <glm/gtc/type_ptr.hpp>

//#include <OpenMesh/Core/IO/MeshIO.hh>
//#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

//#include "seismicvolume.h"
//#include "surfacereader.h"
//#include "roiselectionoperator.h"
//#include "boundaryhighlightoperator.h"
//#include "borderselectionoperator.h"

//typedef OpenMesh::PolyMesh_ArrayKernelT<>  MyMesh;

//#define OM_TO_VEC3(V) glm::vec3( V[0], V[1], V[2] )
//#define GLMVEC3_TO_QVEC3(V) QVector3D( V.x, V.y, V.z )

#include "newvolume.h"

Canvas3DWidget::Canvas3DWidget(QWidget *parent)
    : QOpenGLWidget(parent)//,
//      currentOperator(nullptr)
{
}

Canvas3DWidget::~Canvas3DWidget()
{
}

QSize Canvas3DWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize Canvas3DWidget::sizeHint() const
{
    return QSize(200, 200);
}

void Canvas3DWidget::setBackgroundColor(const QColor& color)
{
//    scene.setClearColor({color.red()/255.f,color.green()/255.f,color.blue()/255.f});
}

QColor Canvas3DWidget::getBackgroundColor()
{
//    auto color = scene.getClearColor();
//    return QColor(color.r*255,color.g*255,color.b*255);
}

void Canvas3DWidget::setVolume(NewVolume *volume)
{
    scene.setVolume(volume);
    update();
}

const NewVolume *Canvas3DWidget::getVolume()
{
    return scene.getVolume();
}

void Canvas3DWidget::showXSlice(bool show)
{
    scene.showXSlice(show);
    update();
}


void Canvas3DWidget::showYSlice(bool show)
{
    scene.showYSlice(show);
    update();
}


void Canvas3DWidget::showZSlice(bool show)
{
    scene.showZSlice(show);
    update();
}

void Canvas3DWidget::updateXSlice(unsigned int sliceNum)
{
    scene.updateXSlice(sliceNum);
    update();
}


void Canvas3DWidget::updateYSlice(unsigned int sliceNum)
{
    scene.updateYSlice(sliceNum);
    update();
}


void Canvas3DWidget::updateZSlice(unsigned int sliceNum)
{
    scene.updateZSlice(sliceNum);
    update();
}

void Canvas3DWidget::setWireframeOn(bool on)
{
    scene.setWireframeOn(on);
    update();
}

void Canvas3DWidget::initializeGL()
{
    initializeOpenGLFunctions();

    scene.init();
    scene.setClearColor({1.f,1.f,1.f});
    scene.camera.resize(width(),height());

    emit canvasInitialized();
}


void Canvas3DWidget::paintGL()
{
    if(scene.camera.changed)
    {
//        emit viewChanged();
    }

    scene.draw(defaultFramebufferObject());

    if( currentOperator )
    {
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebufferObject());
        currentOperator->draw();
    }
}


void Canvas3DWidget::resizeGL(int width, int height)
{
    scene.resize(width,height);
}


void Canvas3DWidget::mousePressEvent(QMouseEvent *event)
{
    if( currentOperator )
    {
        if( currentOperator->mousePress(event) )
            update();
    }
    else
    {
        if (event->buttons() & Qt::LeftButton)
        {
            oldPos = glm::ivec2(event->x(),event->y());
        }
    }
}


void Canvas3DWidget::mouseMoveEvent(QMouseEvent *event)
{
    glm::ivec2 curPos(event->x(),event->y());

    QString posText = tr("X = %1, Y = %2").arg(curPos.x).arg(curPos.y);
//    emit updateMousePositionText(posText);

   if( currentOperator )
   {
       makeCurrent();
       bool redraw = false;
       if (event->buttons() & Qt::RightButton)
       {
          scene.camera.rotate(oldPos,curPos);
          redraw = true;
       }

       if( redraw || currentOperator->mouseMove(event) )
       {
           update();
       }

       oldPos = curPos;
   }

   else
   {
      if (event->buttons() & Qt::LeftButton)
      {
          scene.camera.rotate(oldPos,curPos);
          update();
          oldPos = curPos;
      }
   }



}


void Canvas3DWidget::mouseReleaseEvent(QMouseEvent* event)
{
    if( currentOperator )
    {
        currentOperator->mouseRelease(event);
        update();
    }
}

void Canvas3DWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
}

void Canvas3DWidget::wheelEvent(QWheelEvent *event)
{
    scene.camera.zoom(event->delta());
    update();
}

void Canvas3DWidget::setOperator( std::shared_ptr<Operator> op )
{
    currentOperator = op;
}

std::shared_ptr<Operator> Canvas3DWidget::getOperator()
{
    return currentOperator;
}

//std::shared_ptr<BorderSelectionOperator> Canvas3DWidget::createBorderSelectionOperator()
//{
//    makeCurrent();

//    auto borderOperator = std::make_shared<BorderSelectionOperator>(scene,defaultFramebufferObject());


//    return borderOperator;
//}

std::shared_ptr<SliceSketchingOperator> Canvas3DWidget::createSliceSketchingOperator()
{
//    makeCurrent();

    return std::make_shared<SliceSketchingOperator>(scene/*,defaultFramebufferObject()*/);
}

Scene &Canvas3DWidget::getScene()
{
    return scene;
}











//void Canvas3DWidget::setROISelectionOperator()
//{
//    auto newOperator = std::make_shared<ROISelectionOperator>(scene);
//    setOperator(std::static_pointer_cast<Operator>(newOperator));
//}





//void Canvas3DWidget::clearScene()
//{
//    scene.reset();
//    update();
//}




//void Canvas3DWidget::loadCube()
//{
//    makeCurrent();

//    std::vector<opengl::Vertex> vertexBuffer;
//    std::vector<unsigned int> indices;

//    vertexBuffer = {
//         //Position      //Normal
//        {{ -1, -1, -1 }, {  0, -1,  0 }},
//        {{ -1, -1, -1 }, { -1,  0,  0 }},
//        {{ -1, -1, -1 }, {  0,  0, -1 }},
//        {{ +1, -1, -1 }, {  0, -1,  0 }},
//        {{ +1, -1, -1 }, { +1,  0,  0 }},
//        {{ +1, -1, -1 }, {  0,  0, -1 }},
//        {{ +1, -1, +1 }, {  0, -1,  0 }},
//        {{ +1, -1, +1 }, { +1,  0,  0 }},
//        {{ +1, -1, +1 }, {  0,  0, +1 }},
//        {{ -1, -1, +1 }, {  0, -1,  0 }},
//        {{ -1, -1, +1 }, { -1,  0,  0 }},
//        {{ -1, -1, +1 }, {  0,  0, +1 }},
//        {{ -1, +1, -1 }, { -1,  0,  0 }},
//        {{ -1, +1, -1 }, {  0,  0, -1 }},
//        {{ -1, +1, -1 }, {  0, +1,  0 }},
//        {{ +1, +1, -1 }, { +1,  0,  0 }},
//        {{ +1, +1, -1 }, {  0,  0, -1 }},
//        {{ +1, +1, -1 }, {  0, +1,  0 }},
//        {{ +1, +1, +1 }, { +1,  0,  0 }},
//        {{ +1, +1, +1 }, {  0,  0, +1 }},
//        {{ +1, +1, +1 }, {  0, +1,  0 }},
//        {{ -1, +1, +1 }, { -1,  0,  0 }},
//        {{ -1, +1, +1 }, {  0,  0, +1 }},
//        {{ -1, +1, +1 }, {  0, +1,  0 }},
//    };

//    indices = {
//        0,   3,  6, //normal: (  0, -1,  0 )
//        0,   6,  9, //normal: (  0, -1,  0 )
//        12,  1, 10, //normal: ( -1,  0,  0 )
//        12, 10, 21, //normal: ( -1,  0,  0 )
//        18,  7,  4, //normal: ( +1,  0,  0 )
//        18,  4, 15, //normal: ( +1,  0,  0 )
//        22, 11,  8, //normal: (  0,  0, +1 )
//        22,  8, 19, //normal: (  0,  0, +1 )
//        16,  5,  2, //normal: (  0,  0, -1 )
//        16,  2, 13, //normal: (  0,  0, -1 )
//        23, 20, 17, //normal: (  0, +1,  0 )
//        23, 17, 14  //normal: (  0, +1,  0 )
//    };

//    scene.addObject(vertexBuffer,indices,{0.45f,0.45f,0.45f});
//    update();
//}


//void Canvas3DWidget::loadSphere()
//{
//    loadObject("../data/sphere.off");
//}


//void Canvas3DWidget::loadBunny()
//{
//    loadObject("../data/bunny.obj");
//}


//void Canvas3DWidget::loadVolume(const QString& path)
//{
//    auto ext = path.right(path.size()-1-path.lastIndexOf('.')).toLower();

//    if(ext == tr("nrrd"))
//    {
//        NewVolume* volume = readNRRD(path.toStdString());
//        scene.setVolume(volume);

//        info.firstInline    = 1;
//        info.firstCrossline = 1;
//        info.firstDepth     = 1;
//        info.stepInline     = 1;
//        info.stepCrossline  = 1;
//        info.stepDepth      = 1;
//        info.lastInline     = volume->numZSlices();
//        info.lastCrossline  = volume->numYSlices();
//        info.lastDepth      = volume->numXSlices();
//    }
//    else if (ext == "dat")
//    {
//        info.firstInline    = 1499;
//        info.firstCrossline = 1499;
//        info.firstDepth     = 0;
//        info.stepInline     = 6;
//        info.stepCrossline  = 6;
//        info.stepDepth      = 20;
//        info.lastInline     = 8507;
//        info.lastCrossline  = 7505;
//        info.lastDepth      = 15000;
//        info.p1 = {2490,2490};
//        info.p2 = {32520,2490};
//        info.p3 = {32520,37530};
//        info.p4 = {2490,37530};

////        info.firstInline    = 100;
////        info.firstCrossline = 300;
////        info.firstDepth     = 0;
////        info.stepInline     = 1;
////        info.stepCrossline  = 1;
////        info.stepDepth      = 4;
////        info.lastInline     = 750;
////        info.lastCrossline  = 1250;
////        info.lastDepth      = 1848;
////        info.p1 = {605835.51668886f,6073556.38221996f};
////        info.p2 = {629576.25771336f,6074219.892946f};
////        info.p3 = {629122.54650645f,6090463.1688065f};
////        info.p4 = {605381.80548195f,6089799.65808046f};

//        NewVolume* volume = seismic::readSeismicVolume(path.toStdString(),info);
//        scene.setVolume(volume);
//    }
//}


//void Canvas3DWidget::loadObject(const QString& path, const glm::vec3 &color,
//                                const QString& name)
//{
//    std::vector<glm::vec3> vertices;
//    std::vector<unsigned int> indices;

//    if(readOffFile(path.toStdString(),vertices,indices))
//    {
//        makeCurrent();

//        qDebug() << "Read successfully: " << path;

//        std::vector<glm::vec3> normals(vertices.size(),glm::vec3(0,0,0));
//        for(unsigned int i = 0; i < indices.size()-2; i+=3)
//        {
//            auto i0 = indices[i+0];
//            auto i1 = indices[i+1];
//            auto i2 = indices[i+2];

//            auto u = vertices[i1]-vertices[i0];
//            auto v = vertices[i2]-vertices[i0];

//            auto n = glm::cross(u,v);

//            normals[i0] += n;
//            normals[i1] += n;
//            normals[i2] += n;
//        }

//        scene.addObject(vertices,normals,indices,color,name.toStdString());
//    }

////    MyMesh mesh;
////    mesh.request_vertex_normals();
////    mesh.request_face_normals();

////    OpenMesh::IO::Options opt;
////    if (!OpenMesh::IO::read_mesh(mesh, path.toStdString(),opt))
////    {
////        QMessageBox msgBox(QMessageBox::Warning, tr("Loading mesh"),
////                           "Error opening mesh file!", 0, this);
////        msgBox.addButton(tr("OK"), QMessageBox::AcceptRole);
////        msgBox.exec();
////        return;
////    }

////    // If the file did not provide vertex normals, then calculate them
////    if ( !opt.check( OpenMesh::IO::Options::VertexNormal ) &&
////       mesh.has_face_normals() && mesh.has_vertex_normals() )
////    {
////        // let the mesh update the normals
////        mesh.update_normals();
////    }

////    std::vector<opengl::Vertex> vertexBuffer;
////    std::vector<unsigned int> indices;

////    for( MyMesh::VertexIter it = mesh.vertices_begin(); it!=mesh.vertices_end(); ++it)
////    {
////        glm::vec3 position = OM_TO_VEC3(mesh.point(*it));
////        glm::vec3 normal = OM_TO_VEC3(mesh.normal(*it));
////        vertexBuffer.push_back( {position, normal} );
////    }

////    for( MyMesh::FaceIter it = mesh.faces_begin(); it!=mesh.faces_end(); ++it )
////    {
////        auto f_it = mesh.cfv_ccwbegin(*it);

////        auto end = mesh.cfv_ccwend(*it);

////        for( ; f_it != end; ++f_it )
////        {
////            indices.push_back(f_it->idx());
////        }
////    }


//}






//void Canvas3DWidget::getVolumeBounds(
//        unsigned int& xSliceFirst,
//        unsigned int& xSliceLast,
//        unsigned int& xSliceStep,
//        unsigned int& ySliceFirst,
//        unsigned int& ySliceLast,
//        unsigned int& ySliceStep,
//        unsigned int& zSliceFirst,
//        unsigned int& zSliceLast,
//        unsigned int& zSliceStep)
//{
//    zSliceFirst = info.firstInline;
//    zSliceLast = info.lastInline;
//    zSliceStep = info.stepInline;
//    ySliceFirst = info.firstCrossline;
//    ySliceLast = info.lastCrossline;
//    ySliceStep = info.stepCrossline;
//    xSliceFirst = info.firstDepth;
//    xSliceLast = info.lastDepth;
//    xSliceStep = info.stepDepth;
//}







//void Canvas3DWidget::showObject(unsigned int objIndex, bool show)
//{
//    scene.showObject(objIndex,show);
//    update();
//}



