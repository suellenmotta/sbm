#ifndef OPERATOR_H
#define OPERATOR_H

#include <QOpenGLExtraFunctions>
#include <QMouseEvent>
#include "scene.h"

class Operator
        : public QOpenGLExtraFunctions
        , public QObject
{
public:
    Operator(Scene& scene) : scene(scene) {}

    virtual void draw() = 0;
    virtual bool mouseMove( QMouseEvent * ){ return false; }
    virtual bool mousePress( QMouseEvent * ){ return false; }
    virtual bool mouseRelease( QMouseEvent * ){ return false; }
    virtual bool mouseDoubleClick( QMouseEvent * ){ return false; }

protected slots:
    virtual void onViewChanged() {}
    virtual void onVolumeChanged() {}
    virtual void onModelChanged() {}

protected:
    std::set<unsigned int> highlightedTriangles;
    Scene& scene;
};

#endif // OPERATOR_H
