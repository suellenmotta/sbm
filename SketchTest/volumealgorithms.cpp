#include "volumealgorithms.h"

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <glm/gtx/norm.hpp>

#define INF 1e20

NewVolume *distanceTransform(NewVolume *inputVolume)
{
    if(!inputVolume)
        return nullptr;

    float a = 1;
    float b = std::sqrt( 2.0f );
    float c = std::sqrt( 3.0f );
    float d = std::sqrt( 5.0f );
    float e = std::sqrt( 6.0f );
    float f = 3.0f;

    float forwardTemplate[3][5][5] =
    {
        -1, f, -1, f, -1, f, e, d, e, f, -1, d, -1,  d, -1,  f,  e,  d,  e,  f, -1,  f, -1,  f, -1, // z = -2
         f, e,  d, e,  f, e, c, b, c, e,  d, b,  a,  b,  d,  e,  c,  b,  c,  e,  f,  e,  d,  e,  f, // z = -1
        -1, d, -1, d, -1, d, b, a, b, d, -1, a, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // z = 0
    };

//    for ( int k = 0; k <= 2; k++ )
//    {
//        for( int i = 0; i <= 4; i++ )
//        {
//            for( int j = 0; j <= 4; j++ )
//            {
//                std::cout << std::setw(6) << std::setprecision(3) << forwardTemplate[k][i][j];
//            }
//            std::cout << std::endl;
//        }
//        std::cout << std::endl;
//    }

    float backwardTemplate[3][5][5] =
    {
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, a, -1, d, b, a, b, d, -1, d, -1, d, -1, // z = 0
         f,  e,  d,  e,  f,  e,  c,  b,  c,  e,  d,  b,  a, b,  d, e, c, b, c, e,  f, e,  d, e,  f, // z = +1
        -1,  f, -1,  f, -1,  f,  e,  d,  e,  f, -1,  d, -1, d, -1, f, e, d, e, f, -1, f, -1, f, -1  // z = +2
    };

    auto info = inputVolume->getInfo();
    int width = info.numXSlices;
    int height = info.numYSlices;
    int depth = info.numZSlices;
    auto size = width*height*depth;

    auto inputBuffer = inputVolume->getBuffer();

    NewVolume* output = inputVolume->getCopy();
    float* outputBuffer = output->getBuffer();

    //Initialize the distance image with the maximum distance
    std::fill(outputBuffer,outputBuffer+size,FLT_MAX);

    //Initialize distance image with zeros where the object is present
    for(int i=0; i<size; ++i)
        if(inputBuffer[i]>0)
            outputBuffer[i]=0;

    bool updated = true;

    int stop = 0;

    std::cout << std::fixed << std::setprecision(2);

    while(stop<1)
    {
        stop++;
        std::cout << "Iteration " << stop << "..." << std::endl;

        updated = false;

        int count = 0;

        // Forward pass
        for( int z = 0; z < depth; z++ )
        {
            std::cout << "Forward pass... " << std::setw(5) << count * 100.0f / size << "%\r" << std::flush;

            for( int y = 0; y < height; y++ )
            {
                for( int x = 0; x < width; x++ )
                {
                    count++;
                    auto imageIndex = output->getIndex(x,y,z);
                    auto distance = outputBuffer[imageIndex];

                    for( int k = -2; k <= 0; k++ )
                    {
                        for( int j = -2; j <= 2; j++ )
                        {
                            for( int i = -2; i <= 2; i++ )
                            {
                                if ( forwardTemplate[k+2][j+2][i+2] >= 0 )
                                {
                                    auto idx = x+i;
                                    auto idy = y+j;
                                    auto idz = z+k;

                                    if ( idx >= 0 && idx < width &&
                                         idy >= 0 && idy < height &&
                                         idz >= 0 && idz < depth )
                                    {
    //                                    std::cout << i << " " << j << " " << k << std::endl;
                                        auto index = output->getIndex(idx,idy,idz);
                                        auto newDistance = outputBuffer[index]+forwardTemplate[k+2][j+2][i+2];
                                        if(newDistance<distance)
                                        {
                                            distance = newDistance;
                                            updated = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    outputBuffer[imageIndex] = distance;
                }
            }
        }

        count = 0;

        // Backward pass
        for( int z = depth - 1; z >= 0; z-- )
        {
            std::cout << "Backward pass... " << std::setw(5) << count * 100.0f / size << "%\r" << std::flush;

            for( int y = height - 1; y >= 0; y-- )
            {
                for( int x = width - 1; x >= 0; x-- )
                {
                    count++;

                    auto imageIndex = output->getIndex(x,y,z);
                    auto distance = outputBuffer[imageIndex];

                    for( int k = 0; k <= 2; k++ )
                    {
                        for( int j = -2; j <= 2; j++ )
                        {
                            for( int i = -2; i <= 2; i++ )
                            {
                                if ( backwardTemplate[k][j+2][i+2] >= 0 )
                                {
                                    auto idx = x+i;
                                    auto idy = y+j;
                                    auto idz = z+k;

                                    if ( idx >= 0 && idx < width &&
                                         idy >= 0 && idy < height &&
                                         idz >= 0 && idz < depth )
                                    {
                                        auto index = output->getIndex(idx,idy,idz);
                                        auto newDistance = outputBuffer[index]+backwardTemplate[k][j+2][i+2];
                                        if(newDistance<distance)
                                        {
                                            distance = newDistance;
                                            updated = true;
                                        }
                                    }
                                }

                            }
                        }
                    }

                    outputBuffer[imageIndex] = distance;
                }
            }
        }
    }

    std::cout << "Finished computing distance transform." << std::endl;
    return output;
}

std::vector<glm::uvec3> pointCoordsToVolumeIndexes(const NewVolume::Info& volumeInfo, const std::vector<glm::vec3> &points)
{
    std::vector<glm::uvec3> indexes;

    auto xStep = glm::distance(volumeInfo.p2,volumeInfo.p1)/(volumeInfo.numXSlices-1.0f);
    auto yStep = glm::distance(volumeInfo.p1,volumeInfo.p5)/(volumeInfo.numYSlices-1.0f);
    auto zStep = glm::distance(volumeInfo.p4,volumeInfo.p1)/(volumeInfo.numZSlices-1.0f);

    auto xN = glm::normalize(glm::cross(volumeInfo.p5-volumeInfo.p1,volumeInfo.p4-volumeInfo.p1));
    auto yN = glm::normalize(glm::cross(volumeInfo.p2-volumeInfo.p1,volumeInfo.p4-volumeInfo.p1));
    auto zN = glm::normalize(glm::cross(volumeInfo.p1-volumeInfo.p5,volumeInfo.p6-volumeInfo.p5));

    for(const auto& point : points)
    {
        auto distX = glm::length(glm::dot(point-volumeInfo.p5,xN));
        auto distY = glm::length(glm::dot(point-volumeInfo.p5,yN));
        auto distZ = glm::length(glm::dot(point-volumeInfo.p5,zN));

        unsigned int xSlice = (unsigned int)(distX/xStep+0.5f);
        unsigned int ySlice = (unsigned int)(distY/yStep+0.5f);
        unsigned int zSlice = (unsigned int)(distZ/zStep+0.5f);

        indexes.push_back({xSlice,ySlice,zSlice});
    }

    return indexes;
}

/**
 * @brief computeSquaredEDT
 * @param f is a function defined as 0 in the region of interest, and infinity in the rest.
 * @param n is the input image size. Probably useless, let's see.
 * @param w is the pixel size
 * @param output is the computed squared distance is each point
 */
void computeSquaredEDT(const std::vector<float>& f, int n, float w,
                       std::vector<float>& output)
{
    output = std::vector<float>(n,+INF);
    std::vector<int> v(n,0);
    std::vector<float> z(n+1,0.0f);

    v[0] = 0;
    z[0] = -INF;
    z[1] = +INF;

    double w2 = w*w;
    int k = 0;

    for(int q = 1; q < n; ++q)
    {
        for (;; k--)
        {
            int p = v[k];
            float s = ( 0.5 * ( f[q] + w2 * q * q - f[p] - w2 * p * p ) ) / ( w2 * ( q - p ) );
//            double den = w2 * ( q - p );
//            double s = ( 0.5 * ( (f[q] - f[p]) / den + (w2 * q / den ) * q - (w2 * p / den) * p ) );
            if (s > z[k])
            {
                k++;
                v[k] = q;
                z[k] = s;
                z[k + 1] = +INF;
                break;
            }
        }
    }

    k = 0;
    for (int q = 0; q < n; ++q)
    {
        while (z[k + 1] < w * q)
        {
            k++;
        }

        //p is the closest parabola from q
        int p = v[k];

        //Squared distance from p to q scaled by w
        output[q] = w2 * ( q - p ) * ( q - p ) + f[p];
    }
}

NewVolume* computeEDT(NewVolume* inputVolume)
{
    const auto& info = inputVolume->getInfo();
    const auto& data = inputVolume->getBuffer();

    float sizes[] = {info.numXSlices, info.numYSlices, info.numZSlices};
    float cellSize[] = {1.0f, 1.0f, 1.0f}; //TODO: Get real values

    unsigned int maxSize = std::max(info.numXSlices,std::max(info.numYSlices,info.numZSlices));
    unsigned int totalSize = info.numXSlices*info.numYSlices*info.numZSlices;

    std::vector<float> f(maxSize,+INF);

    float* distances = new float[totalSize];
    std::fill(distances,distances+totalSize,+INF);

    for(unsigned int i = 0; i < totalSize; ++i)
    {
        if(data[i]==0)
            distances[i] = 0.0f;
    }

    //First dimension
    for(unsigned int z = 0; z < sizes[2]; ++z)
    {
        for(unsigned int y = 0; y < sizes[1]; ++y)
        {
            int k = 0;
            for(unsigned int x = 0; x < sizes[0]; ++x)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                f[k++] = distances[idx];
            }

            std::vector<float> output;
            computeSquaredEDT(f,sizes[2],cellSize[0],output);

            k = 0;
            for(unsigned int x = 0; x < sizes[0]; ++x)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                distances[idx] = output[k++];
            }
        }
    }


    //Second dimension
    for(unsigned int z = 0; z < sizes[2]; ++z)
    {
        for(unsigned int x = 0; x < sizes[0]; ++x)
        {
            int k = 0;
            for(unsigned int y = 0; y < sizes[1]; ++y)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                f[k++] = distances[idx];
            }

            std::vector<float> output;
            computeSquaredEDT(f,sizes[1],cellSize[1],output);

            k = 0;
            for(unsigned int y = 0; y < sizes[1]; ++y)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                distances[idx] = output[k++];
            }
        }
    }


    //Third dimension
    for(unsigned int y = 0; y < sizes[1]; ++y)
    {
        for(unsigned int x = 0; x < sizes[0]; ++x)
        {
            int k = 0;
            for(unsigned int z = 0; z < sizes[2]; ++z)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                f[k++] = distances[idx];
            }

            std::vector<float> output;
            computeSquaredEDT(f,sizes[2],cellSize[2],output);

            k = 0;
            for(unsigned int z = 0; z < sizes[2]; ++z)
            {
                auto idx = inputVolume->getIndex(x,y,z);
                distances[idx] = output[k++];
            }
        }
    }


    for(unsigned int i = 0; i < totalSize; ++i)
    {
        distances[i] = std::sqrt(distances[i]);
    }

    return new NewVolume(info, distances);
}
