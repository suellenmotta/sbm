#include "volumerenderer.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>

#include <QMatrix>

#include "raycasting.h"

struct vertex
{
    glm::vec3 pos;
    glm::vec2 texCoord;
};

#define MIN_VAL -93.0f
#define MAX_VAL 88.0f

VolumeRenderer::VolumeRenderer(ArcballCamera& camera)
    : VAO(0)
    , xSliceVBO(0)      , ySliceVBO(0)      , zSliceVBO(0)
    , xSliceTexture(0)  , ySliceTexture(0)  , zSliceTexture(0)
    , xSliceIndex(-1)   , ySliceIndex(-1)   , zSliceIndex(-1)
    , xSliceShow(false) , ySliceShow(false) , zSliceShow(false)
    , xSliceMinVal(MIN_VAL), ySliceMinVal(MIN_VAL), zSliceMinVal(MIN_VAL)
    , xSliceMaxVal(MAX_VAL), ySliceMaxVal(MAX_VAL), zSliceMaxVal(MAX_VAL)
    , volume(nullptr)
    , cam(camera)
    , lutTexture(0)
    , isInitialized(false)
    , correctColor(true)
{

}

VolumeRenderer::~VolumeRenderer()
{
    if(volume)
        delete volume;

    if(isInitialized)
    {
        glDeleteVertexArrays(1, &VAO);

        glDeleteBuffers(1, &xSliceVBO);
        glDeleteBuffers(1, &ySliceVBO);
        glDeleteBuffers(1, &zSliceVBO);

        glDeleteTextures(1, &xSliceTexture);
        glDeleteTextures(1, &ySliceTexture);
        glDeleteTextures(1, &zSliceTexture);
    }
}

void VolumeRenderer::init()
{
    initializeOpenGLFunctions();
    isInitialized = true;

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/textured-quad-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/textured-quad-frag");
    program.link();

    if(program.isLinked())
    {
        glGenBuffers(1, &zSliceVBO);
        glGenBuffers(1, &ySliceVBO);
        glGenBuffers(1, &xSliceVBO);

        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);
        glEnableVertexAttribArray( 0 );
        glEnableVertexAttribArray( 1 );
        glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
        glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
        glVertexAttribBinding(0,0);
        glVertexAttribBinding(1,0);

        glGenTextures(1, &zSliceTexture);
        glGenTextures(1, &ySliceTexture);
        glGenTextures(1, &xSliceTexture);

        float colorTable[] =
        {
            170/255.0f,0.0f,0.0f,
            1.0f,28/255.0f,0.0f,
            1.0f,200/255.0f,0.0f,
            243/255.0f,243/255.0f,243/255.0f,
            56/255.0f,70/255.0f,127/255.0f,
            0.0f,0.0f,0.0f
        };
        lutMarkers = {0.0f,0.070352f,0.25f,0.5f,0.883249f,1.0f};

        glGenTextures(1, &lutTexture);
        updateColorTableTexture(colorTable,6);
    }
}

const NewVolume *VolumeRenderer::getVolume()
{
    return volume;
}

void VolumeRenderer::setVolume(NewVolume* volume)
{
    if(this->volume)
        delete this->volume;

    this->volume = volume;

    if(volume)
    {
        auto startModel = volume->getStartModelMatrix();
        auto center = glm::vec3(startModel*glm::vec4(volume->getCenter(),1));
        cam.focus(center,volume->getCircumscribedSphereRadius(),startModel);

        updateXSlice(0);
        updateYSlice(0);
        updateZSlice(0);

        xSliceShow = ySliceShow = zSliceShow = true;
    }
    else
    {
        xSliceIndex = -1;
        ySliceIndex = -1;
        zSliceIndex = -1;

        xSliceShow = ySliceShow = zSliceShow = true;
    }
}

void VolumeRenderer::updateXSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getXSlicePoints(index, vertices);
    updateVBO(vertices,xSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getXSliceBuffer(index,width,height);
    updateTexture(slice,width,height,xSliceTexture);

    if(correctColor)
    {
        xSliceMinVal = *(std::min_element(slice,slice+width*height));
        xSliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    xSliceIndex = index;
    delete[] slice;
}

void VolumeRenderer::updateYSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getYSlicePoints(index, vertices);
    updateVBO(vertices,ySliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getYSliceBuffer(index,width,height);
    updateTexture(slice,width,height,ySliceTexture);

    if(correctColor)
    {
        ySliceMinVal = *(std::min_element(slice,slice+width*height));
        ySliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    ySliceIndex = index;
    delete[] slice;
}

void VolumeRenderer::updateZSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getZSlicePoints(index, vertices);
    updateVBO(vertices,zSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getZSliceBuffer(index,width,height);
    updateTexture(slice,width,height,zSliceTexture);

    if(correctColor)
    {
        zSliceMinVal = *(std::min_element(slice,slice+width*height));
        zSliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    zSliceIndex = index;
    delete[] slice;
}

unsigned int VolumeRenderer::getCurrentXSlice() const
{
    return xSliceIndex;
}

unsigned int VolumeRenderer::getCurrentYSlice() const
{
    return ySliceIndex;
}

unsigned int VolumeRenderer::getCurrentZSlice() const
{
    return zSliceIndex;
}

void VolumeRenderer::showXSlice(bool show)
{
    xSliceShow = show;
}

void VolumeRenderer::showYSlice(bool show)
{
    ySliceShow = show;
}

void VolumeRenderer::showZSlice(bool show)
{
    zSliceShow = show;
}

bool VolumeRenderer::isXSliceShown() const
{
    return xSliceShow;
}

bool VolumeRenderer::isYSliceShown() const
{
    return ySliceShow;
}

bool VolumeRenderer::isZSliceShown() const
{
    return zSliceShow;
}

void VolumeRenderer::setMinPixelValue(float minValue)
{
    xSliceMinVal = ySliceMinVal = zSliceMinVal = minValue;
}

void VolumeRenderer::setMaxPixelValue(float maxValue)
{
    xSliceMaxVal = ySliceMaxVal = zSliceMaxVal = maxValue;
}

void VolumeRenderer::draw()
{
    if(!volume)
        return;

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    program.bind();

    auto mvp = cam.projMatrix() * cam.viewMatrix() * cam.modelMatrix();
    program.setUniformValue("mvp", QMatrix4x4(glm::value_ptr(glm::transpose(mvp))));

    program.setUniformValue("valueSampler",0);
    program.setUniformValue("lutSampler", 1);
    program.setUniformValueArray("colorMarkers",&lutMarkers[0],6,1);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_1D, lutTexture);

    glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(VAO);

    if(xSliceShow)
    {
        glBindVertexBuffer(0,xSliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, xSliceTexture);
        program.setUniformValue("minValue",xSliceMinVal);
        program.setUniformValue("maxValue",xSliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(ySliceShow)
    {
        glBindVertexBuffer(0,ySliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, ySliceTexture);
        program.setUniformValue("minValue",ySliceMinVal);
        program.setUniformValue("maxValue",ySliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(zSliceShow)
    {
        glBindVertexBuffer(0,zSliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, zSliceTexture);
        program.setUniformValue("minValue",zSliceMinVal);
        program.setUniformValue("maxValue",zSliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    glBindVertexArray(0);
}

const ArcballCamera &VolumeRenderer::getCamera()
{
    return cam;
}

bool VolumeRenderer::intercepts(const glm::ivec2 &screenPos, NewVolume::Axis &axis, glm::vec3 &point) const
{
    float xDist=FLT_MAX,yDist=FLT_MAX,zDist=FLT_MAX;
    auto dir = rayDirection(screenPos,cam);

    bool interceptsX = interceptsXSlice(cam.eye,dir,xDist);
    bool interceptsY = interceptsYSlice(cam.eye,dir,yDist);
    bool interceptsZ = interceptsZSlice(cam.eye,dir,zDist);

    float dist = FLT_MAX;
    if(interceptsX)
    {
        axis = NewVolume::X;
        dist = xDist;
    }
    if(interceptsY && yDist<dist)
    {
        axis = NewVolume::Y;
        dist = yDist;
    }
    if(interceptsZ && zDist<dist)
    {
        axis = NewVolume::Z;
        dist = zDist;
    }

    if(interceptsX || interceptsY || interceptsZ)
    {
        point = glm::vec3(glm::inverse(cam.modelMatrix())*glm::vec4(cam.eye + dist * dir,1));
        return true;
    }

    return false;
}

bool VolumeRenderer::interceptsXSlice(const glm::vec3& orig, const glm::vec3& dir, float &dist) const
{
    if(xSliceShow && xSliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getXSlicePoints(xSliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

bool VolumeRenderer::interceptsYSlice(const glm::vec3 &orig, const glm::vec3& dir, float &dist) const
{
    if(ySliceShow && ySliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getYSlicePoints(ySliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

bool VolumeRenderer::interceptsZSlice(const glm::vec3& orig, const glm::vec3& dir, float &dist) const
{
    if(zSliceShow && zSliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getZSlicePoints(zSliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

void VolumeRenderer::updateVBO(const std::vector<glm::vec3>& vertices, GLuint vboID)
{
    std::vector< vertex > vbo;
    vbo.reserve( 4 );

    vbo.push_back({vertices[0], {0.0f,0.0f}});
    vbo.push_back({vertices[1], {1.0f,0.0f}});
    vbo.push_back({vertices[2], {0.0f,1.0f}});
    vbo.push_back({vertices[3], {1.0f,1.0f}});

    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, vbo.size()*sizeof(vertex), &vbo[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VolumeRenderer::updateTexture(float* slice, unsigned int width,
    unsigned int height, GLuint textureID)
{
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, width, height,
                  0, GL_RED, GL_FLOAT, slice );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D,0);
}

void VolumeRenderer::updateColorTableTexture(float* colorTable, unsigned int numColors)
{
    glActiveTexture(GL_TEXTURE1);

    glBindTexture(GL_TEXTURE_1D, lutTexture);

    glTexImage1D( GL_TEXTURE_1D, 0, GL_RGB, numColors,
                  0, GL_RGB, GL_FLOAT, colorTable );

    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_1D,0);
}
