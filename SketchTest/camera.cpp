#include "camera.h"

#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>


ArcballCamera::ArcballCamera()
    : changed(true)
    , center(width/2,height/2)
    , radius((float)glm::max(width,height))
{

}

ArcballCamera::ArcballCamera(const glm::ivec2& center, float radius)
    : changed(true)
    , center(center)
    , radius(radius)
{

}

void ArcballCamera::rotate(const glm::ivec2& oldPos, const glm::ivec2& newPos)
{
    auto q0 = screenToArcsphere({oldPos.x,height-oldPos.y});
    auto q1 = screenToArcsphere({newPos.x,height-newPos.y});
    auto q = q1*glm::conjugate(q0);

    auto Rot = glm::mat4_cast(q);
    model = Rot * model;

    changed = true;
}

void ArcballCamera::focus(const glm::vec3& center, float radius, const glm::mat4& startModel)
{
    model = glm::translate(glm::mat4(),glm::vec3(-center))*startModel;

    fovy = 60.0f;
    auto aspect = (float)width/height;
    auto fovx = fovy*aspect;

    auto distance = glm::max(
                radius / glm::tan(glm::radians(fovx*0.5f)),
                radius / glm::tan(glm::radians(fovy*0.5f)));

    at = glm::vec3();
    eye = at;
    eye.z += distance;
    up = glm::vec3(0.f,1.f,0.f);
    zNear = 0.005f*distance;
    zFar  = 100*distance;

    view = glm::lookAt(eye, at, up);
    proj = glm::perspective(glm::radians(fovy), aspect, zNear, zFar);

    changed = true;
}

void ArcballCamera::zoom(float delta)
{
    eye += (at - eye) * 0.001f * delta;
    view = glm::lookAt(eye, at, up);

    changed = true;
}

void ArcballCamera::resize(int width, int height)
{
    this->width = width;
    this->height = height;

    center = {width/2,height/2};
    radius = (float)glm::max(width,height);

    auto aspect = (float)width/height;
    proj = glm::perspective(glm::radians(fovy), aspect, zNear, zFar);

    changed = true;
}

glm::mat4 ArcballCamera::modelMatrix() const
{
    return model;
}

glm::mat4 ArcballCamera::viewMatrix() const
{
    return view;
}

glm::mat4 ArcballCamera::projMatrix() const
{
    return proj;
}

glm::quat ArcballCamera::screenToArcsphere(const glm::ivec2 & screenPos)
{
    if(radius > 0)
    {
        glm::vec3 v(glm::vec2(screenPos-center)/radius, 0.0f);
        auto r = glm::length2(v);
        if(r > 1)
            v *= (1.0f/glm::sqrt(r));
        else
            v.z = glm::sqrt(1-r);

        return glm::quat(0.0f, v);
    }

    return glm::quat();
}

