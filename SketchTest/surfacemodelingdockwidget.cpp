#include "surfacemodelingdockwidget.h"
//#include "ui_surfacemodelingdockwidget.h"
#include "canvas3dwidget.h"
#include "mainwindow.h"
#include "reconstruction/gridsurfacereconstruction.h"
#include "volumealgorithms.h"

#include <QPair>
#include <QDebug>

#include <fstream>
#include <algorithm>

#include <QListWidget>
#include <QHBoxLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QToolButton>

typedef QPair<NewVolume::Axis,unsigned int> SketchIDType;
Q_DECLARE_METATYPE(SketchIDType)

SurfaceModelingWidget::SurfaceModelingWidget(QWidget *parent, Canvas3DWidget *canvas)
    : QWidget(parent)
    , canvas(canvas)
{
    QHBoxLayout* horizontalLayout = new QHBoxLayout(this);

    sketchesListWidget = new QListWidget(this);
    sketchesListWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);

    addSketchButton    = new QToolButton(this);
    QToolButton* deleteSketchButton = new QToolButton(this);
    QToolButton* showAllSketches    = new QToolButton(this);

    QIcon addIcon;
    addIcon.addFile(QStringLiteral(":/icons/images/add-icon.png"), QSize(), QIcon::Normal, QIcon::On);
    addSketchButton->setIcon(addIcon);
    addSketchButton->setIconSize(QSize(24, 24));

    QIcon deletIcon;
    deletIcon.addFile(QStringLiteral(":/icons/images/delete-icon.png"), QSize(), QIcon::Normal, QIcon::On);
    deleteSketchButton->setIcon(deletIcon);
    deleteSketchButton->setIconSize(QSize(24, 24));

    QIcon showAllIcon;
    showAllIcon.addFile(QStringLiteral(":/icons/images/view-all-sketches-icon.png"), QSize(), QIcon::Normal, QIcon::Off);
    showAllSketches->setIcon(showAllIcon);
    showAllSketches->setIconSize(QSize(24, 24));

    addSketchButton->setCheckable(true);
    showAllSketches->setCheckable(true);

    QVBoxLayout* sketchesButtonsLayout = new QVBoxLayout();
    sketchesButtonsLayout->addWidget(addSketchButton, 0, Qt::AlignTop);
    sketchesButtonsLayout->addWidget(deleteSketchButton, 0, Qt::AlignTop);
    sketchesButtonsLayout->addWidget(showAllSketches, 0, Qt::AlignBottom);
    sketchesButtonsLayout->setStretch(2, 1);

    horizontalLayout->addWidget(sketchesListWidget);
    horizontalLayout->addLayout(sketchesButtonsLayout);

    connect(addSketchButton,SIGNAL(toggled(bool)),this,SLOT(on_addSketchButton_toggled(bool)));
    connect(deleteSketchButton,SIGNAL(clicked()),this,SLOT(on_deleteSketchButton_clicked()));
    connect(showAllSketches,SIGNAL(toggled(bool)),this,SLOT(on_showAllSketches_toggled(bool)));

}

void SurfaceModelingWidget::setSliceSketchingOperator(std::shared_ptr<SliceSketchingOperator> sketchingOperator)
{
    if(this->sketchingOperator)
    {
        disconnect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
                   this,SLOT(onSketchFinished()));
    }

    this->sketchingOperator = sketchingOperator;

    connect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
            this,SLOT(onSketchFinished()));
}

void SurfaceModelingWidget::getSketchesPointCloud(std::vector<glm::vec3> &pointCloud)
{
    if(sketchingOperator)
    {
        pointCloud = sketchingOperator->getSketchesPointCloud();
    }
}

SurfaceModelingWidget::~SurfaceModelingWidget()
{
}

void SurfaceModelingWidget::stopSketching()
{
    addSketchButton->toggle();
}

void SurfaceModelingWidget::onSketchFinished()
{
    fillSketchesList();
}

void SurfaceModelingWidget::on_addSketchButton_toggled(bool checked)
{
    if(checked)
    {
        if(sketchingOperator)
        {
            canvas->setOperator(std::static_pointer_cast<Operator>(sketchingOperator));
            canvas->update();
        }
    }
    else
    {
        canvas->setOperator(nullptr);
        canvas->update();
    }
}

void SurfaceModelingWidget::on_deleteSketchButton_clicked()
{
    if(sketchingOperator)
    {
        auto selected = sketchesListWidget->selectedItems();

        for(auto item : selected)
        {
            auto sketch = item->data(Qt::UserRole).value<SketchIDType>();

            sketchingOperator->deleteSketch(sketch.first,sketch.second);
        }

        qDeleteAll(selected);
        canvas->update();
    }
}

void SurfaceModelingWidget::on_showAllSketches_toggled(bool checked)
{
    if(sketchingOperator)
    {
        if(checked)
            sketchingOperator->showAllSketches();
        else
            sketchingOperator->showOnlyVisible();

        canvas->update();
    }
}

void SurfaceModelingWidget::on_sketchesListWidget_itemSelectionChanged()
{
}

void SurfaceModelingWidget::on_pointCloudButton_clicked(bool checked)
{
    if(sketchingOperator)
    {
        if(checked)
            sketchingOperator->showPointCloud();
        else
            sketchingOperator->showLines();

        canvas->update();
    }
}

void SurfaceModelingWidget::on_viewModeButton_clicked(bool checked)
{

}

void SurfaceModelingWidget::on_resetButton_clicked()
{
    addSketchButton->setChecked(false);
    sketchingOperator->deleteSketches();
    sketchesListWidget->clear();

    canvas->update();
}

void SurfaceModelingWidget::on_generateButton_clicked()
{

}

void SurfaceModelingWidget::on_SurfaceModelingDockWidget_visibilityChanged(bool visible)
{
    if(!visible)
    {
        sketchesListWidget->clear();
        addSketchButton->setChecked(false);
    }
}

void SurfaceModelingWidget::fillSketchesList()
{
    if(sketchingOperator)
    {
        sketchesListWidget->clear();

        auto sketchedSlices = sketchingOperator->getSketchedSlices();
        auto sketchNames = sketchingOperator->getSketchNames();

        for(unsigned int i = 0; i < sketchedSlices.size(); ++i)
        {
            SketchIDType sketch(sketchedSlices[i].first,sketchedSlices[i].second);

            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(sketchNames[i]));
            item->setData(Qt::UserRole, QVariant::fromValue(sketch));

            sketchesListWidget->addItem(item);
        }
    }
}


