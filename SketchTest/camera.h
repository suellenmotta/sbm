#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera
{
public:
    Camera()
        : eye( {0.0f, 0.0f, 1.0f} )
        , at( {0.0f, 0.0f, 0.0f} )
        , up( {0.0f, 1.0f, 0.0f} )
        , fovy( glm::radians(60.0f) )
        , width( 640 )
        , height( 480 )
        , zNear(0.001f)
        , zFar(1000.0f) {}

    glm::vec3 eye;
    glm::vec3 at;
    glm::vec3 up;
    float fovy;
    int width;
    int height;
    float zNear;
    float zFar;
};

class ArcballCamera : public Camera
{
public:
    ArcballCamera();
    ArcballCamera(const glm::ivec2& center, float radius);

    void rotate(const glm::ivec2& oldPos, const glm::ivec2& newPos);

    void focus(const glm::vec3 &center, float radius, const glm::mat4 &startModel = glm::mat4());

    void zoom(float delta);

    void resize(int width, int height);

    glm::mat4 modelMatrix() const;

    glm::mat4 viewMatrix() const;

    glm::mat4 projMatrix() const;

    bool changed;

private:
    glm::quat screenToArcsphere(const glm::ivec2 & screenPos);

    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;

    //Center and radius in screen coordinates
    glm::ivec2 center;
    float radius;

};

#endif // CAMERA_H
