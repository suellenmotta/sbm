#version 400 core

layout( location = 0 ) in vec3 m_vertexPos;

out vec2 UV;

void main()
{
    gl_Position = vec4( m_vertexPos, 1 );

    UV = ( m_vertexPos.xy + 1 ) / 2.0;
}
