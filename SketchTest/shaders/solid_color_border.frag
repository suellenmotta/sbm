#version 400 core

uniform vec3 color;
uniform int object;
uniform int border;

layout( location = 0 ) out vec3 finalColor;
layout( location = 1 ) out ivec2 borderID;

void main()
{
    borderID = ivec2(object,border);
    finalColor = color;
}
