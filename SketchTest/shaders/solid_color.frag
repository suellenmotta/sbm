#version 400 core

uniform vec3 color;
out vec3 finalColor;

void main()
{
    finalColor = color;
}
