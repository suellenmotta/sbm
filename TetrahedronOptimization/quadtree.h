#ifndef QUADTREE_H
#define QUADTREE_H

#include <vector>
#include <array>
#include <stack>
#include <glm/glm.hpp>
#include "graph.h"

template< typename T >
class Quadtree
{
public:
    Quadtree(const glm::vec2 &minPoint, const glm::vec2 &maxPoint, const std::vector<glm::vec2>& points);
    Quadtree(const glm::vec2 &minPoint, const glm::vec2 &maxPoint);

    ~Quadtree();

    void create();

    const std::vector<glm::vec2>& getVertices() const;
    const std::vector<T>& getData() const;
    std::vector<unsigned int> getBoundaryIndices() const;

    unsigned int getNumNodes() const;
    unsigned int getNumLeafNodes() const;

    void setPoints(const std::vector<glm::vec2>& points);
    void setData(unsigned int index, const T& element);
    void setData(const std::vector<T>& data);

    void setMinimumSize(float size);

    struct Node
    {
        //Level of the node in the tree
        unsigned int level;
        Node* parent;

        //Number of points inside this node
        unsigned int numPoints;

        //Pointers to the children. It contains eight or zero elements.
        std::vector<Node*> children;

        /* Indices order:
         *
         *     2----------3
         *     |          |
         *     |          |     y
         *     |          |     |
         *     0----------1     |_____x
         *
         */
        std::array<unsigned int, 4> indices;

        //The one point inside the node, when numPoints is equal to 1
        glm::vec2 point;
    };

    //Find the neighbors of a node in a specific direction
    Node* findNorthGreatNeighbor(const Node* node) const
    {
        if(node->parent==nullptr) //root
            return nullptr;

        if(node->parent->children[0]==node)
            return node->parent->children[2];

        if(node->parent->children[1]==node)
            return node->parent->children[3];

        auto neighbor = findNorthGreatNeighbor(node->parent);

        if(neighbor==nullptr || neighbor->children.empty())
            return neighbor;

        if(node->parent->children[2]==node)
            return neighbor->children[0];
        else
            return neighbor->children[1];
    }

    Node* findSouthGreatNeighbor(const Node* node) const
    {
        if(node->parent==nullptr) //root
            return nullptr;

        if(node->parent->children[2]==node)
            return node->parent->children[0];

        if(node->parent->children[3]==node)
            return node->parent->children[1];

        auto neighbor = findSouthGreatNeighbor(node->parent);

        if(neighbor==nullptr || neighbor->children.empty())
            return neighbor;

        if(node->parent->children[0]==node)
            return neighbor->children[2];
        else
            return neighbor->children[3];
    }

    Node* findEastGreatNeighbor(const Node* node) const
    {
        if(node->parent==nullptr) //root
            return nullptr;

        if(node->parent->children[0]==node)
            return node->parent->children[1];

        if(node->parent->children[2]==node)
            return node->parent->children[3];

        auto neighbor = findEastGreatNeighbor(node->parent);

        if(neighbor==nullptr || neighbor->children.empty())
            return neighbor;

        if(node->parent->children[1]==node)
            return neighbor->children[0];
        else
            return neighbor->children[2];
    }

    Node* findWestGreatNeighbor(const Node* node) const
    {
        if(node->parent==nullptr) //root
            return nullptr;

        if(node->parent->children[1]==node)
            return node->parent->children[0];

        if(node->parent->children[3]==node)
            return node->parent->children[2];

        auto neighbor = findWestGreatNeighbor(node->parent);

        if(neighbor==nullptr || neighbor->children.empty())
            return neighbor;

        if(node->parent->children[0]==node)
            return neighbor->children[1];
        else
            return neighbor->children[3];
    }

private:

    Node* root;

    std::vector<glm::vec2> initPoints;

    std::vector< glm::vec2 > vertices;

    std::vector< T > data;

    unsigned int numNodes;
    unsigned int numLNodes;

    bool useMinimumSize;
    float minimumSize;

    bool isInside(const glm::vec2& point, const glm::vec2& minPoint,
                  const glm::vec2& maxPoint, float eps = 0.0f);

    void destructor(Node* node);

public:


    //Iterating functionality
    class iterator
    {
    public:
        iterator operator++(int){ nextLeaf(); return *this; }
        iterator operator++(){ auto it = *this; nextLeaf(); return it; }
        bool operator==(const iterator& rhs) { return cur == rhs.cur; }
        bool operator!=(const iterator& rhs) { return cur != rhs.cur; }
        Quadtree::Node* operator->() { return cur; }
        Quadtree::Node& operator*() { return *cur; }


        friend class Quadtree;

        Quadtree::Node* curParent;
        Quadtree::Node* cur;

    private:
        iterator(Quadtree::Node* root)
            : cur(nullptr)
        {
            if(root)
            {
                stack.push(root);
                nextLeaf();
            }
        }

        void nextLeaf()
        {
            if(stack.empty())
                cur = nullptr;

            else
            {
                while(!stack.empty())
                {
                    Node* n = stack.top();
                    stack.pop();

                    if(!n->children.empty())
                    {
                        for(auto it = n->children.rbegin(); it != n->children.rend(); ++it)
                        {
                            stack.push(*it);
                        }
                    }

                    else
                    {
                        cur = n;
                        break;
                    }
                }
            }
        }

        //For recursive call simulation
        std::stack<Node*> stack;

    };


    iterator begin() const
    {
        return iterator(root);
    }

    iterator end() const
    {
        return iterator(nullptr);
    }
};

template class Quadtree<float>;

template<typename T>
Graph quadtreeToGraph(const Quadtree<T> &quadtree)
{
    auto& vertices = quadtree.getVertices();
    Graph G(vertices.size());

    for(const auto& node : quadtree)
    {
        auto neighbor = quadtree.findNorthGreatNeighbor(&node);

        auto distance = glm::distance(vertices[node.indices[2]],vertices[node.indices[3]]);

        if(neighbor==nullptr || neighbor->children.empty())
            G.insertEdge(node.indices[2],node.indices[3],distance);
    }

    for(const auto& node : quadtree)
    {
        auto neighbor = quadtree.findSouthGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
            G.insertEdge(node.indices[0],node.indices[1]);
    }

    for(const auto& node : quadtree)
    {
        auto neighbor = quadtree.findEastGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
            G.insertEdge(node.indices[1],node.indices[3]);
    }

    for(const auto& node : quadtree)
    {
        auto neighbor = quadtree.findWestGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
            G.insertEdge(node.indices[0],node.indices[2]);
    }

    return G;

}

#endif // QUADTREE_H
