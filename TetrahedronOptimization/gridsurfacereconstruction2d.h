#ifndef GRIDSURFACERECONSTRUCTOR2D_H
#define GRIDSURFACERECONSTRUCTOR2D_H

#include "surfacereconstruction.h"
#include "defs.h"

#include "SurfaceModeling/ModelManager/OptimizationModel.h"
using namespace ModelManager;

class GridSurfaceReconstructor2D
        : public SurfaceReconstructor2D
{
public:
    GridSurfaceReconstructor2D(const std::vector< glm::vec2 >& pointCloud, bool closed = false);

    void setGridResolution(unsigned int width, unsigned int height);

    void exportNeutralFile(const std::string& path);

protected:
    void optimize() override;
    void reconstruct() override;

    void setupVariables(OptimizationModel& model);
    void setupObjectiveFunction(OptimizationModel& model);
    void setupInputPointsConstraints(OptimizationModel& model,
                                     const std::vector<SamplePoint2D> &sampledPoints);
    void setupBoundaryConstraints(OptimizationModel& model);

    void samplePoints(std::vector<SamplePoint2D> &sampledPoints);

    RegularGrid grid;

};


#endif // GRIDSURFACERECONSTRUCTOR2D_H
