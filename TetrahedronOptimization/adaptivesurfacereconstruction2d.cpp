#include "adaptivesurfacereconstruction2d.h"

#include "io.h"

#include "SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "SurfaceModeling/ModelManager/QuadraticExpression.h"
#include "SurfaceModeling/ModelManager/OptimizationModel.h"

#include <sstream>

#define DEBUGING 0
#define WEIGHTED 0

AdaptiveSurfaceReconstructor2D::AdaptiveSurfaceReconstructor2D(const std::vector<glm::vec2>& pointCloud, bool closed)
    : SurfaceReconstructor2D(pointCloud,closed)
    , qdt({-2.0f,-2.0f},{2.0f,2.0f})
{
}

void AdaptiveSurfaceReconstructor2D::setMinimumQuadtreeLeafSize(float size)
{
    qdt.setMinimumSize(size);
}

void AdaptiveSurfaceReconstructor2D::exportNeutralFile(const std::string &path)
{
    writeNeutralFile(path,qdt,meshVertices,meshIndices);
}

void AdaptiveSurfaceReconstructor2D::optimize()
{
    qdt.setPoints(pts);
    qdt.create();
    writeOFF("quadtree.off",qdt);

    std::vector<glm::vec2> qdtPoints;
    for(const auto& l : qdt)
    {
        if(l.numPoints==1)
            qdtPoints.push_back(l.point);
    }
    writeXYZ("qdt_points.xyz",qdtPoints);

    Solver::ConjugateGradientLISOptimizationSolver solver;
    ModelManager::OptimizationModel model(&solver);

    setupVariables(model);
    setupObjectiveFunction(model);
    setupInputPointsConstraints(model);
    setupBoundaryConstraints(model);

    model.optimize( );

    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();

#if DEBUGING
    std::cout << "\nResult:\n";
#endif
    for(unsigned int i = 0; i < numVariables; ++i)
    {
        qdt.setData(i,variables[i].getValue());

#if DEBUGING
        std::cout << variables[i].getName() << " = " << variables[i].getValue() << std::endl;
#endif
    }
}

void AdaptiveSurfaceReconstructor2D::setupVariables(OptimizationModel &model)
{
    auto numVariables = qdt.getVertices().size();

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        std::stringstream name;
        name << "x" << i;
        model.addVariable( name.str(), -1000, 1000 );
    }

    model.update();
}

void AdaptiveSurfaceReconstructor2D::setupObjectiveFunction(OptimizationModel &model)
{
    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();

#if WEIGHTED
    struct Node
    {
        Node() : left(-1),right(-1),top(-1),bottom(-1) {}

        int left, right, top, bottom;
    };

    std::vector<Node> G(numVariables);

    for(const auto& node : qdt)
    {
        auto neighbor = qdt.findNorthGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
        {
            G[node.indices[2]].right = node.indices[3];
            G[node.indices[3]].left  = node.indices[2];
        }
    }

    for(const auto& node : qdt)
    {
        auto neighbor = qdt.findSouthGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
        {
            G[node.indices[0]].right = node.indices[1];
            G[node.indices[1]].left  = node.indices[0];
        }
    }

    for(const auto& node : qdt)
    {
        auto neighbor = qdt.findEastGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
        {
            G[node.indices[1]].top     = node.indices[3];
            G[node.indices[3]].bottom  = node.indices[1];
        }
    }

    for(const auto& node : qdt)
    {
        auto neighbor = qdt.findWestGreatNeighbor(&node);

        if(neighbor==nullptr || neighbor->children.empty())
        {
            G[node.indices[0]].top     = node.indices[2];
            G[node.indices[2]].bottom  = node.indices[0];
        }
    }


#if DEBUGING
    std::cout << "\nGrid neighborhood: \n";
    for(unsigned int u = 0; u < numVariables; ++u)
    {
        std::cout << u << ": l:" << G[u].left
                       << ", r:" << G[u].right
                       << ", t:" << G[u].top
                       << ", b:" << G[u].bottom << std::endl;
    }
#endif

    auto& vertices = qdt.getVertices();

    ModelManager::QuadraticExpression& obj = model.getObjectiveFunction();


#if DEBUGING
    std::cout << "\nLaplace expressions:\n";
#endif

    for(unsigned int u = 0; u < numVariables; ++u)
    {
        std::vector<float> distances(5,0.0f);

        if(G[u].left != -1 && G[u].right != -1)
        {
            distances[0] = glm::distance(vertices[u],vertices[G[u].right]); //left
            distances[1] = glm::distance(vertices[u],vertices[G[u].left]);  //right
        }
        if(G[u].top != -1 && G[u].bottom != -1)
        {
            distances[2] = glm::distance(vertices[u],vertices[G[u].bottom]); //top
            distances[3] = glm::distance(vertices[u],vertices[G[u].top]);    //bottom
        }

        for(int i = 0; i < 4; ++i)
            distances[4] += distances[i];

        float length = 0.0f;
        for(int i = 0; i < 5; ++i)
            length += distances[i]*distances[i];
        length = glm::sqrt(length);

        for(int i = 0; i < 5; ++i)
            distances[i] /= length;

        ModelManager::LinearExpression exp;
        if(G[u].left != -1 && G[u].right != -1)
        {
            exp += distances[0] * variables[G[u].left] +
                   distances[1] * variables[G[u].right];
        }
        if(G[u].top != -1 && G[u].bottom != -1)
        {
            exp += distances[2] * variables[G[u].top] +
                   distances[3] * variables[G[u].bottom];
        }

        if(exp.size()>1)
        {
            exp -= distances[4]*variables[u];


#if DEBUGING
            std::cout << exp << std::endl;
#endif
            obj += exp*exp;
        }
    }
#else
    ModelManager::QuadraticExpression& obj = model.getObjectiveFunction();
    auto G = quadtreeToGraph(qdt);
    for(unsigned int u = 0; u < numVariables; ++u)
    {
        auto neighbors = G.getNeighbors(u);

        ModelManager::LinearExpression exp;

        for(const auto& v : neighbors)
            exp += variables[v.v];

        exp -= exp.size()*variables[u];

        obj += exp*exp;
    }


#endif
}

void AdaptiveSurfaceReconstructor2D::setupInputPointsConstraints(OptimizationModel &model)
{
    auto sense = isSoft ? SOFT : EQUAL;

    auto variables = model.getVariables();

    auto calcArea = [](const glm::vec2& p0, const glm::vec2& p1)
    {
        auto dist = glm::abs(p1-p0);
        return dist.x*dist.y;
    };


#if DEBUGING
    std::cout << "\nInput points:\n";
    for(int i = 0; i < pts.size(); ++i)
        std::cout << "p" << i << ": " << pts[i] << std::endl;
#endif

    auto vertices = qdt.getVertices();


#if DEBUGING
    std::cout << "\nInput points constraints:\n";
#endif

    for(const auto leaf : qdt)
    {
        if(leaf.numPoints>0)
        {
            auto minPoint = vertices[leaf.indices.front()];
            auto maxPoint = vertices[leaf.indices.back()];

            float area = calcArea(minPoint,maxPoint);

            ModelManager::LinearExpression exp;
            for(unsigned int i = 0; i < 4; ++i)
            {
                auto iArea = calcArea(leaf.point,vertices[leaf.indices[3-i]]);
                auto bCoord  = iArea / area;

                exp += bCoord * variables[leaf.indices[i]];
            }


#if DEBUGING
            std::cout << leaf.point << ": ";
            std::cout << model.addConstraint(exp,sense,0) << std::endl;
#else
            model.addConstraint(exp,sense,0);
#endif
        }
    }

    model.setSoftConstraintsWeight(1000.0);
}

void AdaptiveSurfaceReconstructor2D::setupBoundaryConstraints(OptimizationModel &model)
{
    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();
    auto vertices = qdt.getVertices();

    float value = 10.0f;

    std::cout << "\nBoundary constraints:\n";
    if(isClosed)
    {
        for(unsigned int u  = 0; u < numVariables; ++u)
        {
            if(vertices[u].x > 1.999f || vertices[u].x < -1.9999f ||
               vertices[u].y > 1.999f || vertices[u].y < -1.9999f)
            {
#if DEBUGING
                std::cout << model.addConstraint(variables[u], EQUAL, value) << std::endl;
#else
                model.addConstraint(variables[u], EQUAL, value);
#endif
            }
        }
        //Assuming the center index is inside the object
        auto centerIdx = 8;
#if DEBUGING
        std::cout << model.addConstraint(variables[centerIdx],LESS_EQUAL,-value) << std::endl;
#else
        model.addConstraint(variables[centerIdx],LESS_EQUAL,-value);
#endif
    }

    else
    {
        for(unsigned int u  = 0; u < numVariables; ++u)
        {
            //Top indices
            if(vertices[u].y > 1.999f)
            {
#if DEBUGING
                std::cout << model.addConstraint(variables[u], EQUAL, value) << std::endl;
#else
                model.addConstraint(variables[u], EQUAL, value);
#endif
            }

            //Bottom indices
            else if(vertices[u].y < -1.9999f)
            {
#if DEBUGING
                std::cout << model.addConstraint(variables[u], EQUAL, -value) << std::endl;
#else
                model.addConstraint(variables[u], EQUAL, -value);
#endif
            }
        }
    }

}
