#include "octree.h"

#include <iostream>
#include <unordered_map>
#include <queue>

template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std
{
  template<typename S, typename T> struct hash<pair<S, T>>
  {
    inline size_t operator()(const pair<S, T> & v) const
    {
      size_t seed = 0;
      ::hash_combine(seed, v.first);
      ::hash_combine(seed, v.second);
      return seed;
    }
  };
}


template<typename T>
Octree<T>::Octree(const glm::vec3& minPoint, const glm::vec3& maxPoint, const std::vector<glm::vec3>& points)
    : Octree(minPoint,maxPoint)
{
    setPoints(points);
}

template<typename T>
Octree<T>::Octree(const glm::vec3 &minPoint, const glm::vec3 &maxPoint)
    : numNodes(0)
    , numLNodes(0)
    , minimumSize(0.0f)
    , useMinimumSize(false)
{
    root = new Node;
    root->numPoints = 0;

    vertices.push_back(minPoint);
    vertices.push_back({maxPoint.x,minPoint.y,minPoint.z});
    vertices.push_back({minPoint.x,maxPoint.y,minPoint.z});
    vertices.push_back({maxPoint.x,maxPoint.y,minPoint.z});

    vertices.push_back({minPoint.x,minPoint.y,maxPoint.z});
    vertices.push_back({maxPoint.x,minPoint.y,maxPoint.z});
    vertices.push_back({minPoint.x,maxPoint.y,maxPoint.z});
    vertices.push_back(maxPoint);

    root->indices = {0,1,2,3,4,5,6,7};

    data.resize(vertices.size());
}

template<typename T>
Octree<T>::~Octree()
{
    destructor(root);
    root = nullptr;
}

template<typename T>
void Octree<T>::create()
{
    root->numPoints = initPoints.size();
    if(root->numPoints < 2)
    {
        numLNodes = 1;
        if(root->numPoints == 1)
            root->point = initPoints[0];
    }
    numNodes = 1;

    using Edge = std::pair<unsigned int, unsigned int>;
    std::unordered_map<Edge, unsigned int> knownEdges;

    auto getMidIndex = [&](unsigned int i, unsigned int j)
    {
        if(j<i) std::swap(i,j);

        auto it = knownEdges.find({i,j});

        if(it!=knownEdges.end())
            return it->second;
        else
        {
            auto v = (vertices[i]+vertices[j])*0.5f;
            unsigned int k = vertices.size();
//            std::cout << "mid(" << i << "," << j << ") = " << k << "(" << v.x << ", " << v.y << ", " << v.z << ")" << std::endl;
            vertices.push_back(v);
            data.push_back(T());
            knownEdges[{i,j}] = k;
            return k;
        }
    };

    struct QNode
    {
        Node* node;
        std::vector<glm::vec3> points;
    };
    std::queue<QNode> queue;

    if(root->numPoints>1)
        queue.push({root,initPoints});

    auto curLevel = root->level;

    while(!queue.empty())
    {
        auto& node = queue.front().node;
        auto& points = queue.front().points;

        //For memory cleaning up purpose
        if(node->level != curLevel)
        {
            knownEdges.clear();
            curLevel = node->level;
        }

        /* Elements created in the subdivision:
         *
         * New bins:                                        New vertices:
         *                                                  Obs: 13 is really always new. The others may
         *                                                       have been created already.
         *
         *           X-----------X-----------X                                          25
         *          /   Bin     /   Bin     /|                                         /|
         *         /      6    /      7    / |-> Bin 7                                / |
         *        X-----------X-----------X  |                          15-----------16----------17
         *       /     Bin   /     Bin   /|  X                           | 21-------/|--22-------|--23
         *      /        2  /        3  / | /|                           | /       / | /|        | /
         *     X-----------X-----------X  |/ |-> Bin 5                   |/       7  |/ |        |/
         *     |   Bin     |   Bin     |  X  |                          12--------|-13-----------14
         *     |     2     |     3     | /|  X                          /|        | /|  19      /|
         *     |           |           |/ | /                          / |        |/ | /       / |
         *     X-----------X-----------X  |/                          3-----------4-----------5  |
         *     |   Bin     |   Bin     |  X                              9--------|-10-----------11
         *     |     0     |     1     | /                                        | /
         *     |           |           |/                                         |/
         *     X-----------X-----------X                                          1
         *
         *
         */

        std::array<unsigned int,27> indices;

        //Corners
        indices[0]  = node->indices[0];
        indices[2]  = node->indices[1];
        indices[6]  = node->indices[2];
        indices[8]  = node->indices[3];
        indices[18] = node->indices[4];
        indices[20] = node->indices[5];
        indices[24] = node->indices[6];
        indices[26] = node->indices[7];

        indices[1]  = getMidIndex(indices[0],indices[2]);
        indices[3]  = getMidIndex(indices[0],indices[6]);
        indices[5]  = getMidIndex(indices[2],indices[8]);
        indices[7]  = getMidIndex(indices[6],indices[8]);
        indices[9]  = getMidIndex(indices[0],indices[18]);
        indices[11] = getMidIndex(indices[2],indices[20]);
        indices[15] = getMidIndex(indices[6],indices[24]);
        indices[17] = getMidIndex(indices[8],indices[26]);
        indices[19] = getMidIndex(indices[18],indices[20]);
        indices[21] = getMidIndex(indices[18],indices[24]);
        indices[23] = getMidIndex(indices[20],indices[26]);
        indices[25] = getMidIndex(indices[24],indices[26]);

        indices[4]  = getMidIndex(indices[3],indices[5]);
        indices[10] = getMidIndex(indices[9],indices[11]);
        indices[12] = getMidIndex(indices[3],indices[21]);
        indices[14] = getMidIndex(indices[5],indices[23]);
        indices[16] = getMidIndex(indices[15],indices[17]);
        indices[22] = getMidIndex(indices[21],indices[23]);

        indices[13] = getMidIndex(indices[12],indices[14]);

        //Create the new nodes and set their indices
        for(unsigned int k = 0; k < 2; ++k)
        {
            for(unsigned int j = 0; j < 2; ++j)
            {
                for(unsigned int i = 0; i < 2; ++i)
                {
                    Node* n = new Node;

                    int pos = 0;
                    for(unsigned int nk = 0; nk < 2; ++nk)
                    {
                        for(unsigned int nj = 0; nj < 2; ++nj)
                        {
                            for(unsigned int ni = 0; ni < 2; ++ni)
                            {
                                auto idx = 9*(k+nk) + 3*(j+nj) + (i+ni);
                                n->indices[pos++]=indices[idx];
                            }
                        }
                    }

                    n->level = node->level+1;
                    node->sons.push_back(n);
                }
            }
        }

        numNodes += 8;

        for(auto n : node->sons)
        {
            auto min = vertices[n->indices[0]];
            auto max = vertices[n->indices[7]];

            std::vector<glm::vec3> nPoints;

            for(const auto p : points)
            {
                if(isInside(p,min,max))
                    nPoints.push_back(p);
            }

            n->numPoints = nPoints.size();
            if(n->numPoints > 1 || (useMinimumSize && max.x-min.x > minimumSize))
            {
                queue.push({n,nPoints});
            }
            else
            {
                if(n->numPoints == 1)
                    n->point = nPoints[0];
                numLNodes++;
            }

        }

        queue.pop();
    }
}

template<typename T>
const std::vector<glm::vec3> &Octree<T>::getVertices() const
{
    return vertices;
}

template<typename T>
const std::vector<T> &Octree<T>::getData() const
{
    return data;
}

template<typename T>
std::vector<unsigned int> Octree<T>::getBoundaryIndices() const
{
    std::vector<unsigned int> indices;

//    enum Side
//    {
//        LEFT,RIGHT,UP,BOTTOM,FRONT,BACK
//    };

//    auto processBin = [&](Bin* bin, Side side)
//    {
//        switch(side)
//        {
//        case LEFT:
//        {

//            if(!bin->sons.empty())
//            {
//                processBin(bin->sons[0],side);
//                processBin(bin->sons[2],side);
//                processBin(bin->sons[4],side);
//                processBin(bin->sons[6],side);
//            }
//            break;
//        }
//        case RIGHT:
//        {
//            break;
//        }
//        case UP:
//        {
//            break;
//        }
//        case BOTTOM:
//        {
//            break;
//        }
//        case FRONT:
//        {
//            break;
//        }
//        case BACK:
//        {
//            break;
//        }
//        default:
//            break;
//        }

//    };

//    indices = {0,1,2,3,4,5,6,7};
//    for(int s = 0; s < 6; ++s)
//        processBin(root,(Side)s);

    return indices;

}

template<typename T>
unsigned int Octree<T>::getNumNodes() const
{
    return numNodes;
}

template<typename T>
unsigned int Octree<T>::getNumLeafNodes() const
{
    return numLNodes;
}

template<typename T>
void Octree<T>::setPoints(const std::vector<glm::vec3> &points)
{
    initPoints = points;
}

template<typename T>
void Octree<T>::setData(unsigned int index, const T &element)
{
    if(index < data.size())
        data[index] = element;
}

template<typename T>
void Octree<T>::setData(const std::vector<T> &data)
{
    this->data = data;
}

template<typename T>
void Octree<T>::setMinimumSize(float size)
{
    minimumSize = size;
    useMinimumSize = true;
}

template<typename T>
bool Octree<T>::isInside(const glm::vec3& point, const glm::vec3& minPoint, const glm::vec3& maxPoint, float eps)
{
    return (point.x > minPoint.x+eps && point.x < maxPoint.x-eps &&
            point.y > minPoint.y+eps && point.y < maxPoint.y-eps &&
            point.z > minPoint.z+eps && point.z < maxPoint.z-eps);
}

//void Octree<T>::subdivide(Bin* bin, const std::vector<glm::vec3>& points,
//                       std::map<std::pair<unsigned int, unsigned int>,unsigned int>& edges)
//{
//    bin->numPoints = points.size();
//    numBins++;

//    if(bin->numPoints < 2)
//    {
//        numLBins++;

//        if(bin->numPoints==1)
//            bin->point = points[0];
//    }
//    else
//    {


////        0 1
////        2 3
////        4 5
////        6 7
////        0 4
////        2 6
////        1 5
////        3 7

//        auto minPoint = vertices[bin->indices[0]];
//        auto maxPoint = vertices[bin->indices[7]];
//        auto halfPoint = (maxPoint-minPoint)*0.5f;

//        //Size before new vertices
//        unsigned int size = vertices.size();

//        //Create the new vertices
//        for(unsigned int k = 0; k < 3; ++k)
//        {
//            for(unsigned int j = 0; j < 3; ++j)
//            {
//                for(unsigned int i = 0; i < 3; ++i)
//                {
//                    if(i==1 || j==1 || k==1)
//                    {
//                        auto p = minPoint + glm::vec3(i,j,k)*halfPoint;

//                        vertices.push_back(p);
//                    }
//                }
//            }
//        }

//        std::vector<unsigned int> indices;
//        indices.resize(27,0);

//        for(unsigned int k = 0; k < 3; ++k)
//        {
//            for(unsigned int j = 0; j < 3; ++j)
//            {
//                for(unsigned int i = 0; i < 3; ++i)
//                {
//                    if(i==1 || j==1 || k==1)
//                    {

//                    }
//                    else //is a corner
//                    {
//                        auto idx1 = 2*k +   j + (i/2);
//                        auto idx2 = 9*k + 3*j + i;

//                        indices[idx1] = bin->indices[idx2];
//                    }
//                }
//            }
//        }
//        auto idx = 9*(k+nk) + 3*(j+nj) + (i+ni);

//        //Corners
//        indices[0] = bin->indices[0];
//        indices[2] = bin->indices[1];
//        indices[6] = bin->indices[2];
//        indices[8] = bin->indices[3];
//        indices[18 = bin->indices[4]]



//        //Indices to be used by the new nodes
//        std::vector<unsigned int > indices = {bin->indices[0], size+0, bin->indices[1],
//                                              size+1,          size+2,          size+3,
//                                              bin->indices[2], size+4, bin->indices[3],

//                                              size+5,          size+6,          size+7,
//                                              size+8,          size+9,         size+10,
//                                              size+11,         size+12,         size+13,

//                                              bin->indices[4], size+14, bin->indices[5],
//                                              size+15,         size+16,         size+17,
//                                              bin->indices[6], size+18, bin->indices[7]};





//    }
//}

template<typename T>
void Octree<T>::destructor(Node* node)
{
    for(const auto& s : node->sons)
        destructor(s);

    delete node;
}

void octreeMarchingCubes(const Octree<float> &octree, std::vector<unsigned int> &indices, std::vector<glm::vec3> &vertices)
{
    const auto& data = octree.getData();

    for(const auto& node : octree)
    {
        //Contains the function value for this cube node
        float cube[8];

        //TODO: Check this order
        for(unsigned int i = 0; i < 7; ++i)
            cube[i] = data[node.indices[i]];



    }
}

