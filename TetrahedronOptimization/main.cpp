#include <glm/glm.hpp>
#include <vector>
#include <functional>

#include "io.h"
#include "gridsurfacereconstruction.h"
#include "gridsurfacereconstruction2d.h"
#include "adaptivesurfacereconstruction.h"
#include "adaptivesurfacereconstruction2d.h"
#include "quadtree.h"

void samplePoints(unsigned int numXPoints, unsigned int numYPoints,
                  std::function<float(float x,float y)> f,
                  const glm::vec3& min, const glm::vec3& max,
                  std::vector<glm::vec3>& pts)
{


    float xStep = (max.x-min.x)/(numXPoints-1);
    float yStep = (max.y-min.y)/(numYPoints-1);

    float y = min.y;
    for(unsigned int j = 0; j < numYPoints; ++j)
    {
        float x = min.x;
        for(unsigned int i = 0; i < numXPoints; ++i)
        {
            pts.push_back({x,y,f(x,y)});
            x+=xStep;
        }

        y+=yStep;
    }
}

void samplePoints2d(unsigned int numPoints,
                    std::function<float(float x)> f,
                    float min, float max,
                    std::vector<glm::vec2>& pts)
{
    float xStep = (max-min)/(numPoints-1);

    float x = min;
    for(unsigned int i = 0; i < numPoints; ++i)
    {
        pts.push_back({x,f(x)});
        x+=xStep;
    }
}

void normalizePoints(std::vector<glm::vec2>& pts)
{
    writeXYZ("input_points_2d.xyz", pts);

    auto min = pts[0], max = pts[0];
    for(const auto& p : pts)
    {
        if(p.x < min.x) min.x = p.x;
        if(p.y < min.y) min.y = p.y;

        if(p.x > max.x) max.x = p.x;
        if(p.y > max.y) max.y = p.y;
    }

    auto med = (min + max)*0.5f;
    auto size = max-min;

    float factor = 1.0f;

    float offset = 2.f*0.01f;

    if(size.x >= size.y) //axis x is the largest
    {
        factor = (2.0f-offset) / size.x;
    }
    else //axis y is the largest
    {
        factor = (2.0f-offset) / size.y;
    }

    for (auto& p : pts)
    {
        p = ( p - med ) * factor;
    }

    writeXYZ("normalized_points_2d.xyz", pts);
}

void teste3dRegularBunny()
{
    std::string bunnyPath = "/local/suellen/projects/sbm/data/bunny.xyz";
    std::string bunnyResultPath = "/home/a/suellen/testes-reconstruction/bunny-gridregular.nf";
    std::string bunnyResultMeshPath = "/home/a/suellen/testes-reconstruction/bunny-gridregular.off";

    std::vector<glm::vec3> pts;
    readXYZ(bunnyPath,pts);

    GridSurfaceReconstructor sfRec(pts,true);
    sfRec.setGridResolution(45,45,45);
    sfRec.setSmooth(true);
    sfRec.run();

    std::vector<glm::vec3> vertices;
    std::vector< unsigned int> indices;
    sfRec.getSurfaceMesh(vertices,indices);

    writeOFF(bunnyResultMeshPath,vertices,indices);
    sfRec.exportNeutralFile(bunnyResultPath);
}

void teste3dRegularSenxCosy()
{
    std::string senxcosyResultPath = "/home/a/suellen/senxcosy-gridregular.nf";
    std::string senxcosyResultMeshPath = "/home/a/suellen/senxcosy-gridregular.off";

    auto f = [](float x,float y) {return std::sin(x)*std::cos(y);};

    glm::vec3 min(0,0,-1);
    glm::vec3 max(2*M_PI,2*M_PI,1);

    std::vector<glm::vec3> pts;
    samplePoints(100,100,f,min,max,pts);

    GridSurfaceReconstructor sfRec(pts);
    sfRec.setGridResolution(45,45,45);
    sfRec.setSmooth(true);
    sfRec.run();

    std::vector<glm::vec3> vertices;
    std::vector< unsigned int> indices;
    sfRec.getSurfaceMesh(vertices,indices);

    writeOFF(senxcosyResultMeshPath,vertices,indices);
    sfRec.exportNeutralFile(senxcosyResultPath);
}

void teste3dAdaptativoBunny()
{
    std::string bunnyPath = "/local/suellen/projects/sbm/data/bunny.xyz";
    std::string resultPath = "/home/a/suellen/testes-reconstruction/bunny-octree.nf";

    std::vector<glm::vec3> pts;
    readXYZ(bunnyPath,pts);

    AdaptiveSurfaceReconstructor sfRec(pts,true);
    sfRec.run();
    sfRec.exportNeutralFile(resultPath);
}

void teste3dAdaptativoSenxCosy()
{
    std::string resultPath = "/home/a/suellen/testes-reconstruction/senxcosy-octree.nf";

    auto f = [](float x,float y) {return std::sin(x)*std::cos(y);};

    glm::vec3 min(0,0,-1);
    glm::vec3 max(2*M_PI,2*M_PI,1);

    std::vector<glm::vec3> pts;
    samplePoints(100,100,f,min,max,pts);

    AdaptiveSurfaceReconstructor sfRec(pts);
    sfRec.run();
    sfRec.exportNeutralFile(resultPath);
}





void teste2dRegular()
{
    auto f = [](float x) {return std::sin(x);};

    std::vector<glm::vec2> pts;
    samplePoints2d(100,f,-2*M_PI,2*M_PI,pts);

    GridSurfaceReconstructor2D sfRec(pts);
    sfRec.setGridResolution(45,45);
    sfRec.run();

//    sfRec.exportNeutralFile("/home/a/suellen/senx-gridregular.nf");
    sfRec.exportNeutralFile("senx-regulargrid.nf");
}


void teste2dAdaptativo()
{
    auto f = [](float x) {return std::sin(x);};

    std::vector<glm::vec2> pts;
    samplePoints2d(100,f,-2*M_PI,2*M_PI,pts);

    AdaptiveSurfaceReconstructor2D sfRec(pts);
    sfRec.run();
//    sfRec.exportNeutralFile("/home/a/suellen/senx10.nf");
    sfRec.exportNeutralFile("senx-quadtree.nf");
}

int main(int, char**)
{
//    teste3dRegularBunny();
//    teste3dRegularSenxCosy();
//    teste3dAdaptativoBunny();
//    teste3dAdaptativoSenxCosy();
//    teste2dAdaptativo();
    teste2dRegular();
    return 0;

    std::vector<glm::vec3> pts;
    std::string resultPath;

//    readXYZ("saltdome-dente.xyz",pts);
//    resultPath = "saltdome-dente.nf";

    auto f = [](float x,float y) {return std::sin(x)*std::cos(y);};

    glm::vec3 min(0,0,-1);
    glm::vec3 max(2*M_PI,2*M_PI,1);

    samplePoints(100,100,f,min,max,pts);

//    auto f = [](float x,float y) {return (0.6f*(x*x+y*y));};
//    glm::vec3 min(-1,-1,0);
//    glm::vec3 max(+1,+1,0);
//    samplePoints(100,100,f,min,max,pts);

    bool octree = true;

    if(octree)
    {
        resultPath = "/home/a/suellen/senxcosy-octree.nf";

        AdaptiveSurfaceReconstructor sfRec(pts);
        sfRec.setMinimumOctreeLeafSize(0.1f);
        sfRec.run();
        sfRec.exportNeutralFile(resultPath);
    }

    else
    {
        GridSurfaceReconstructor sfRec(pts);
        sfRec.setGridResolution(25,25,25);
        sfRec.setSmooth(true);
        sfRec.run();

        std::vector<glm::vec3> vertices;
        std::vector<unsigned int> indices;
        sfRec.getSurfaceMesh(vertices,indices);

        writeOFF("teste.off",vertices,indices);
    }

    return 0;
}
