#include "quadtree.h"

#include <unordered_map>
#include <queue>

template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std
{
  template<typename S, typename T> struct hash<pair<S, T>>
  {
    inline size_t operator()(const pair<S, T> & v) const
    {
      size_t seed = 0;
      ::hash_combine(seed, v.first);
      ::hash_combine(seed, v.second);
      return seed;
    }
  };
}

template<typename T>
Quadtree<T>::Quadtree(const glm::vec2& minPoint, const glm::vec2& maxPoint, const std::vector<glm::vec2>& points)
    : Quadtree(minPoint,maxPoint)
{
    setPoints(points);
}

template<typename T>
Quadtree<T>::Quadtree(const glm::vec2 &minPoint, const glm::vec2 &maxPoint)
    : numNodes(0)
    , numLNodes(0)
    , minimumSize(0.0f)
    , useMinimumSize(false)
{
    root = new Node;
    root->numPoints = 0;
    root->parent = nullptr;

    vertices.push_back(minPoint);
    vertices.push_back({maxPoint.x,minPoint.y});
    vertices.push_back({minPoint.x,maxPoint.y});
    vertices.push_back(maxPoint);

    root->indices = {0,1,2,3};

    data.resize(vertices.size());
}

template<typename T>
Quadtree<T>::~Quadtree()
{
    destructor(root);
    root = nullptr;
}

template<typename T>
void Quadtree<T>::create()
{
    root->numPoints = initPoints.size();
    if(root->numPoints < 2)
    {
        numLNodes = 1;
        if(root->numPoints == 1)
            root->point = initPoints[0];
    }
    numNodes = 1;

    using Edge = std::pair<unsigned int, unsigned int>;
    std::unordered_map<Edge, unsigned int> knownEdges;

    auto getMidIndex = [&](unsigned int i, unsigned int j)
    {
        if(j<i) std::swap(i,j);

        auto it = knownEdges.find({i,j});

        if(it!=knownEdges.end())
            return it->second;
        else
        {
            auto v = (vertices[i]+vertices[j])*0.5f;
            unsigned int k = vertices.size();
            vertices.push_back(v);
            data.push_back(T());
            knownEdges[{i,j}] = k;
            return k;
        }
    };

    struct QNode
    {
        Node* node;
        std::vector<glm::vec2> points;
    };
    std::queue<QNode> queue;

    if(root->numPoints>1)
        queue.push({root,initPoints});

    auto curLevel = root->level;

    while(!queue.empty())
    {
        auto& node = queue.front().node;
        auto& points = queue.front().points;

        //For memory cleaning up purpose
        if(node->level != curLevel)
        {
            knownEdges.clear();
            curLevel = node->level;
        }

        /* Elements created in the subdivision:
         *
         * New nodes                                        New vertices:
         *                                                  Obs: 4 is really always new. The others may
         *                                                       have been created already.
         *
         *     X-----------X-----------X                              6-----------7-----------8
         *     |   Node    |   Node    |                              |           |           |
         *     |     2     |     3     |                              |           |           |
         *     |           |           |                              |           |           |
         *     X-----------X-----------X                              3-----------4-----------5
         *     |   Node    |   Node    |                              |           |           |
         *     |     0     |     1     |                              |           |           |
         *     |           |           |                              |           |           |
         *     X-----------X-----------X                              0-----------1-----------2
         *
         *
         */

        std::array<unsigned int,9> indices;

        //Corners
        indices[0]  = node->indices[0];
        indices[2]  = node->indices[1];
        indices[6]  = node->indices[2];
        indices[8]  = node->indices[3];

        indices[1]  = getMidIndex(indices[0],indices[2]);
        indices[3]  = getMidIndex(indices[0],indices[6]);
        indices[5]  = getMidIndex(indices[2],indices[8]);
        indices[7]  = getMidIndex(indices[6],indices[8]);

        indices[4]  = getMidIndex(indices[3],indices[5]);

        //Create the new nodes and set their indices
        for(unsigned int j = 0; j < 2; ++j)
        {
            for(unsigned int i = 0; i < 2; ++i)
            {
                Node* n = new Node;

                int pos = 0;
                for(unsigned int nj = 0; nj < 2; ++nj)
                {
                    for(unsigned int ni = 0; ni < 2; ++ni)
                    {
                        auto idx = 3*(j+nj) + (i+ni);
                        n->indices[pos++]=indices[idx];
                    }
                }

                n->level = node->level+1;
                n->parent = node;
                node->children.push_back(n);
            }
        }

        numNodes += 4;

        for(auto n : node->children)
        {
            auto min = vertices[n->indices[0]];
            auto max = vertices[n->indices[3]];

            std::vector<glm::vec2> nPoints;

            for(const auto p : points)
            {
                if(isInside(p,min,max,0.005f))
                    nPoints.push_back(p);
            }

            n->numPoints = nPoints.size();
            if(n->numPoints > 1 || (useMinimumSize && max.x-min.x > minimumSize))
            {
                queue.push({n,nPoints});
            }
            else
            {
                if(n->numPoints == 1)
                    n->point = nPoints[0];
                numLNodes++;
            }
        }

        queue.pop();
    }
}

template<typename T>
const std::vector<glm::vec2> &Quadtree<T>::getVertices() const
{
    return vertices;
}

template<typename T>
const std::vector<T> &Quadtree<T>::getData() const
{
    return data;
}

template<typename T>
std::vector<unsigned int> Quadtree<T>::getBoundaryIndices() const
{
    std::vector<unsigned int> indices;
    return indices;
}

template<typename T>
unsigned int Quadtree<T>::getNumNodes() const
{
    return numNodes;
}

template<typename T>
unsigned int Quadtree<T>::getNumLeafNodes() const
{
    return numLNodes;
}

template<typename T>
void Quadtree<T>::setPoints(const std::vector<glm::vec2> &points)
{
    initPoints = points;
}

template<typename T>
void Quadtree<T>::setData(unsigned int index, const T &element)
{
    if(index < data.size())
        data[index] = element;
}

template<typename T>
void Quadtree<T>::setData(const std::vector<T> &data)
{
    this->data = data;
}

template<typename T>
void Quadtree<T>::setMinimumSize(float size)
{
    minimumSize = size;
    useMinimumSize = true;
}

template<typename T>
bool Quadtree<T>::isInside(const glm::vec2& point, const glm::vec2& minPoint, const glm::vec2& maxPoint, float eps)
{
    return (point.x > minPoint.x+eps && point.x < maxPoint.x-eps &&
            point.y > minPoint.y+eps && point.y < maxPoint.y-eps);
}

template<typename T>
void Quadtree<T>::destructor(Node* node)
{
    for(const auto& s : node->children)
        destructor(s);

    delete node;
}
