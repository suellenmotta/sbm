#include <omp.h>
#include <cmath>
#include "OptimizationSolver.h"
#include "../../ModelManager/QuadraticExpression.h"
#include "../../Timer/Timer.h"

namespace Solver
{

    using namespace ModelManager;



    OptimizationSolver::OptimizationSolver( ) : Solver( )
    {
        _objectiveFunction = 0;
    }



    void OptimizationSolver::computeGradient( const QuadraticExpression* obj, unsigned int n )
    {
        //Avoid to compute the gradient twice.
        if (_gradient.size( ))
        {
            _gradient.clear();
        }

        //Start the time.
        Timer t;

        //Alocate storage to gradient expressions.
        _gradient.resize( n );

        int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );

        //Allocate a vector of lockers.
        std::vector<omp_lock_t> lockers( n );
        for (unsigned int i = 0; i < n; i++)
        {
            omp_init_lock( &( lockers[i] ) );
        }

        //Get a pointer to be shared by threads.
        LinearExpression* g = &_gradient[0];

        #pragma omp parallel for num_threads(numThreads) shared(g, lockers)
        for (unsigned int f = 0; f < obj->size( ); f++)
        {
            Variable vi = obj->getVar1( f );
            Variable vj = obj->getVar2( f );
            unsigned int viId = _newVariableIndex[vi.getIndex( )];
            unsigned int vjId = _newVariableIndex[vj.getIndex( )];
            double c = obj->getCoefficient( f );

            if (vi == vj)
            {
                omp_set_lock( &( lockers[viId] ) );
                g[viId] += 2 * c * vi;
                omp_unset_lock( &( lockers[viId] ) );
            }
            else
            {
                omp_set_lock( &( lockers[viId] ) );
                g[viId] += c * vj;
                omp_unset_lock( &( lockers[viId] ) );

                omp_set_lock( &( lockers[vjId] ) );
                g[vjId] += c * vi;
                omp_unset_lock( &( lockers[vjId] ) );

            }
        }

        //Destroy lockers.
        for (unsigned int i = 0; i < n; i++)
        {
            omp_destroy_lock( &( lockers[i] ) );
        }

        const LinearExpression& l = obj->getLinearExpression( );
        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int f = 0; f < l.size( ); f++)
        {
            Variable vi = l.getVariable( f );
            unsigned int vid = _newVariableIndex[vi.getIndex( )];
            _gradient[vid] += l.getCoefficient( f );
        }

        if (_showLog)
            t.printTime( "        Total time" );
    }



    double OptimizationSolver::exactLineSearch( const QuadraticExpression& obj, const std::vector<double>& direction ) const
    {
        int t = omp_get_max_threads( );
        int numThreads = std::max( t - 2, 1 );

        std::vector<double> aSum( numThreads, 0.0 );
        std::vector<double> bSum( numThreads, 0.0 );

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int f = 0; f < obj.size( ); f++)
        {
            const Variable vi = obj.getVar1( f );
            const Variable vj = obj.getVar2( f );
            unsigned int viId = _newVariableIndex[vi.getIndex( )];
            unsigned int vjId = _newVariableIndex[vj.getIndex( )];
            double coeffici = obj.getCoefficient( f );

            double a = vi.getValue( );
            double b = direction[viId];

            double c = vj.getValue( );
            double d = direction[vjId];

            unsigned int tr = omp_get_thread_num( );
            aSum[tr] += coeffici * b * d;
            bSum[tr] += coeffici * ( a * d + b * c );
        }

        const LinearExpression& l = obj.getLinearExpression( );

        #pragma omp parallel for num_threads(numThreads)
        for (unsigned int f = 0; f < l.size( ); f++)
        {
            unsigned int viId = _newVariableIndex[l.getVariable( f ).getIndex( )];
            double coeffici = l.getCoefficient( f );

            unsigned int tr = omp_get_thread_num( );
            bSum[tr] += coeffici * direction[viId];
        }

        double ca = 0.0, cb = 0.0;
        for (int i = 0; i < numThreads; i++)
        {
            ca += aSum[i];
            cb += bSum[i];
        }

        return -cb / ( 2 * ca );
    }



    void OptimizationSolver::initializeOptimizationSolver( )
    {
        //Initialize solver.
        initializeSolver( );
    }



    ModelManager::QuadraticExpression * OptimizationSolver::getObjectiveFunction( ) const
    {
        return _objectiveFunction;
    }



    void OptimizationSolver::setObjectiveFunction( ModelManager::QuadraticExpression * obj )
    {
        _objectiveFunction = obj;
    }



    void OptimizationSolver::setReindexVector( unsigned int* newIndex, unsigned int numberUnknownVariables )
    {
        _newVariableIndex = newIndex;
        _numberUnknowVariables = numberUnknownVariables;
    }



    void OptimizationSolver::setScaleVector( double *scaleVector )
    {
        _scale = scaleVector;
    }



    OptimizationSolver::~OptimizationSolver( )
    {

    }



    void OptimizationSolver::computeGradientValues( unsigned int d, double& gs, double& g ) const
    {
        gs = g = 0.0;
        for (unsigned int i = 0; i < _gradient[d].size( ); i++)
        {
            //Get linear term.
            Variable v = _gradient[d].getVariable( i );
            double c = _gradient[d].getCoefficient( i );

            //Compute gradient values.
            gs += c * v.getValue( );
        }
        gs += _gradient[d].getConstant( );
        g = gs * _scale[d];
    }



    bool OptimizationSolver::showCurrentSolution( const double gnorm )
    {
        if (_displayInformation)
        {
            const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );

            #pragma omp parallel for num_threads(numThreads)
            for (unsigned int i = 0; i < _numberVariables; i++)
            {
                if (_variables[i].isKnownValue( ))
                {
                    _x[i] = _variables[i].getValue( );
                }
                else
                {
                    _x[i] = _variables[i].getValue( );
                    _x[i] /= _scale[_newVariableIndex[i]];
                }
            }

            return _displayInformation( _x, gnorm, _object );
        }
        return true;
    }
} // namespace Solver
