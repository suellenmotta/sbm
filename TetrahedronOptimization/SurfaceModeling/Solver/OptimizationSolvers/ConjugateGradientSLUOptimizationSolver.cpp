#include "ConjugateGradientSLUOptimizationSolver.h"
#include "../../ModelManager/Variable.h"
#include "../../ModelManager/QuadraticExpression.h"
#include <cstdio>
#include <cmath>
#include <armadillo>

using namespace arma;
using namespace ModelManager;

namespace Solver
{



    ConjugateGradientSLUOptimizationSolver::ConjugateGradientSLUOptimizationSolver( ) : OptimizationSolver( )
    {
        _maxIterations = 500;
    }



    bool ConjugateGradientSLUOptimizationSolver::optimize( )
    {
        if (_showLog)
            printf( "    Initializing optimization solver...\n" );

        initializeOptimizationSolver( );

        if (_showLog)
            printf( "    Building Q matrix\n" );

        //Use batch insertion constructor to build the matrix faster than
        //consecutively inserting.
        umat locations( 2, 2 * _objectiveFunction->size( ) - _numberUnknowVariables );
        vec values( 2 * _objectiveFunction->size( ) - _numberUnknowVariables );

        unsigned long long element = 0;
        for (unsigned int i = 0; i < _objectiveFunction->size( ); i++)
        {
            Variable vi = _objectiveFunction->getVar1( i );
            Variable vj = _objectiveFunction->getVar2( i );
            double c = _objectiveFunction->getCoefficient( i );
            unsigned int idi = _newVariableIndex[vi.getIndex( )];
            unsigned int idj = _newVariableIndex[vj.getIndex( )];
            if (vi == vj)
            {
                locations( 0, element ) = idi;
                locations( 1, element ) = idi;
                values( element ) = 2 * c;
                element++;
            }
            else
            {
                locations( 0, element ) = idi;
                locations( 1, element ) = idj;
                values( element ) = c;
                element++;

                locations( 0, element ) = idj;
                locations( 1, element ) = idi;
                values( element ) = c;
                element++;
            }
        }

        sp_mat Q( locations, values, _numberUnknowVariables, _numberUnknowVariables );

        vec b( _numberUnknowVariables );
        locations.clear( );
        values.clear( );

        if (_showLog)
            printf( "    Building b vector..\n" );

        const LinearExpression& exp = _objectiveFunction->getLinearExpression( );
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            Variable v = exp.getVariable( i );
            double c = exp.getCoefficient( i );
            unsigned int id = _newVariableIndex[v.getIndex( )];
            b( id ) += -c;
        }

        //Print current solution.
        showCurrentSolution( );

        if (_showLog)
            printf( "    Solving the linear system Qx = -b...\n" );

        superlu_opts settings;
        settings.symmetric = true;
        vec x;
        bool status = spsolve( x, Q, b, "superlu", settings );
        if (status)
        {
            if (_showLog)
                printf( "    ****OPTIMAL SOLUTION FOUND****\n" );

            for (unsigned int i = 0, v = 0; i < _numberVariables; i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    _variables[i].setValue( x( v ) );
                    v++;
                }
            }

            //Print current solution.
            showCurrentSolution( );
        }
        else
        {
            if (_showLog)
                printf( "    ****OPTIMAL SOLUTION NOT FOUND****\n" );
        }

        computeGradient( _objectiveFunction, _numberUnknowVariables );
        double gnorm = 0.0;
        std::vector<double> sum( _numberOfThreads, 0.0 );
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int v = 0; v < _gradient.size( ); v++)
        {
            double g = 0.0, gs = 0.0;
            computeGradientValues( v, gs, g );

            unsigned int tr = omp_get_thread_num( );
            sum[tr] += g * g;
        }

        for (int i = 0; i < _numberOfThreads; i++)
        {
            gnorm += sum[i];
            sum[i] = 0.0;
        }

        printf( "\n\n    ITERATION           GRADIENT NORM\n" );

        if (_showLog)
        {
            printf( "    %8u            %e           \n", 0, sqrt( gnorm ) );
        }

        return status;
    }
} // namespace Solver
