#pragma once

#include "OptimizationSolver.h"
#include <armadillo>
namespace Solver
{

    class LagrangeMultiplierSLUOptimizationSolver : public OptimizationSolver
    {

    private:

        class Element
        {

        public:
            unsigned int i, j;
            double value;
        public:

            bool operator<( const Element& e ) const
            {
                if ( i < e.i )
                    return true;
                if ( i == e.i && j < e.j )
                    return true;
                return false;
            }
        };
    private:
        /**
         * Preprocessing the matrix to put in a CSR format.
         * @param numberUnknownV - number of unknown variables.
         * @param b - b vector.
         * @param matrix - matrix positions.
         * @param values - matrix values.
         */
        void preprocessingFormat( unsigned int numberUnknownV,
                                  arma::vec &b,
                                  arma::umat &matrix,
                                  arma::vec& values ) const;

    public:
        /**
         * Default construtor.
         */
        LagrangeMultiplierSLUOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
