#pragma once

#include "OptimizationSolver.h"
#include "lis.h"
namespace Solver
{

    class ConjugateGradientLISOptimizationSolver : public OptimizationSolver
    {

    private:
        /**
         * Number of iterations to iterative linear system solve.
         */
        unsigned int _numberOfSubIterations;
    private:

        class Element
        {

        public:
            unsigned int i, j;
            double value;
        public:

            bool operator<( const Element& e ) const
            {
                if ( i < e.i )
                    return true;
                if ( i == e.i && j < e.j )
                    return true;
                return false;
            }
        };

    private:
        /**
         * Compute CSR format.
         * @param ptr - start element on the i-th line.
         * @param index - column element index.
         * @param value - element's value to be stored.
         */
        void preprocessingCSRFormat( LIS_INT *ptr, LIS_INT*index, LIS_SCALAR *value ) const;

    public:
        /**
         * Default construtor.
         */
        ConjugateGradientLISOptimizationSolver( );

        /**
         * Get the max iteration number that must be used by the iterative linear
         * solver in each minimization iteration.
         * @return - the max iteration number that must be used by the iterative
         * linear solver
         */
        unsigned int getNumberOfSubIterations( ) const;

        /**
         * Get the number of threads that will be used by the solvers.
         * @param nt - number of threads that will be used by the solvers.
         */
        unsigned int getNumberOfThreads( ) const;

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );

        /**
         * Define the max iteration number to be used by the iterative linear
         * solver in each minimization iteration.
         * @param i - the max iteration number to be used by the iterative linear
         * solver
         */
        void setNumberOfSubIterations( unsigned int i );
    };
} // namespace Solver

