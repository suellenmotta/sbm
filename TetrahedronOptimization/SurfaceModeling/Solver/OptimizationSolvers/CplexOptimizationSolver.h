#pragma once

#include "OptimizationSolver.h"

namespace Solver
{

    class CplexOptimizationSolver : public OptimizationSolver
    {

    public:
        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
