#include <cstdio>
#include <glpk.h>

#include "../../ModelManager/Enums.h"
#include "../../ModelManager/Constraint.h"
#include "../../../../libs/Timer/Timer.h"
#include "LagrangeMultiplierSLUOptimizationSolver.h"
#include <armadillo>
#include <algorithm>


using namespace arma;
using namespace ModelManager;

namespace Solver
{



    LagrangeMultiplierSLUOptimizationSolver::LagrangeMultiplierSLUOptimizationSolver( ) : OptimizationSolver( )
    {
    }



    void LagrangeMultiplierSLUOptimizationSolver::preprocessingFormat( unsigned int numberUnknownV,
                                                                       vec& b, umat& matrix, vec& values ) const
    {
        unsigned int element = 0;
        for (unsigned int g = 0; g < _gradient.size( ); g++)
        {
            for (unsigned int v = 0; v < _gradient[g].size( ); v++)
            {
                unsigned int id = _gradient[g].getVariable( v ).getIndex( );
                id = _newVariableIndex[id];
                matrix( 0, element ) = g;
                matrix( 1, element ) = id;
                values( element ) = _gradient[g].getCoefficient( v );
                element++;

            }
            b( g ) = -_gradient[g].getConstant( );
        }

        for (unsigned int c = 0; c < _numberConstraints; c++)
        {
            const LinearExpression& l = _constraints[c].getLine( );
            for (unsigned int v = 0; v < l.size( ); v++)
            {
                unsigned int id = l.getVariable( v ).getIndex( );
                id = _newVariableIndex[id];

                matrix( 0, element ) = id;
                matrix( 1, element ) = numberUnknownV + c;
                values( element ) = l.getCoefficient( v );
                element++;

                matrix( 0, element ) = numberUnknownV + c;
                matrix( 1, element ) = id;
                values( element ) = l.getCoefficient( v );
                element++;
            }
            b( numberUnknownV + c ) = -l.getConstant( );
        }
    }



    bool LagrangeMultiplierSLUOptimizationSolver::optimize( )
    {
        if (_showLog)
            printf( "Initializing optimization solver...\n" );

        initializeOptimizationSolver( );
        
        if (_showLog)
        {
            printf( "%u variables have been removed from the model...\n", ( _numberVariables - _numberUnknowVariables ) );
            printf( "%u constraints have been removed from the model...\n", ( _numberVariables - _numberUnknowVariables ) );
        }

        if (_showLog)
            printf( "Computing the gradient vector...\n" );

        computeGradient( _objectiveFunction, _numberUnknowVariables );

        unsigned int numberNonZero = 0;
        for (unsigned int i = 0; i < _gradient.size( ); i++)
        {
            numberNonZero += _gradient[i].size( );
        }
        for (unsigned int i = 0; i < _numberConstraints; i++)
        {
            numberNonZero += 2 * _constraints[i].getLine( ).size( );
        }
        if (_showLog)
            printf( "Building b vector..\n" );

        unsigned int totalVariables = _numberUnknowVariables + _numberConstraints;

        //Use batch insertion constructor to build the matrix faster than
        //consecutively inserting.
        umat *locations = new umat( 2, numberNonZero );
        vec *values = new vec( numberNonZero );
        vec b( totalVariables );
        preprocessingFormat( _numberUnknowVariables, b, *locations, *values );
        _gradient.clear( );

        sp_mat Q( *locations, *values, totalVariables, totalVariables, true, false );
        delete locations;
        delete values;

        vec x;
        bool status = spsolve( x, Q, b, "superlu" );
        if (status)
        {
            if (_showLog)
                printf( "****OPTIMAL SOLUTION FOUND****\n" );

            for (unsigned int i = 0, v = 0; i < _numberVariables; i++)
            {
                if (!_variables[i].isKnownValue( ))
                {
                    _variables[i].setValue( x( v ) );
                    v++;
                }
            }
        }
        else
        {
            if (_showLog)
                printf( "****OPTIMAL SOLUTION NOT FOUND****\n" );
        }

        return status;
    }



} // namespace Solver
