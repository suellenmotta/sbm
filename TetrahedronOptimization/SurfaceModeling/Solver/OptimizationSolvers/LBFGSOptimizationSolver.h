#pragma once

#include "OptimizationSolver.h"

namespace Solver
{

    class LBFGSOptimizationSolver : public OptimizationSolver
    {
    private:
        /**
         * Auxilliar vector used for display information callback.
         */
        std::vector<double> _currentValues;

        /**
         * Number of max of vectors to be stores.
         */
        unsigned int _kmax;
    private:

        /**
         * Compute a new direction to move.
         * @param w - the vector of w's.
         * @param v - the vector of v's.
         * @param g - the current gradient.
         * @param iteration - the iteration number.
         * @param direction - the computed direction.
         */
        void computeDirection( const std::vector<std::vector<double> >& w,
                               const std::vector<std::vector<double> >& v,
                               const std::vector<double>& g,
                               const unsigned int iteration,
                               std::vector<double>& direction );

        /**
         * Update deltaG vector and compute dot product between: direction and detaG,
         * deltaX and gradient, and deltaX and deltaG.
         * @param deltaG - delta G vector.
         * @param grad - gradient vector.
         * @param deltaX - delta X vector.
         * @param direction - direction vector.
         * @param dDg - result of the dot product between the direction and delta G
         * vectors.
         * @param dxG - result of the dot product between the gradient and delta X
         * vectors.
         * @param dxDg - result of the dot product between the delta X and delta G
         * vectors.
         * @return - the square gradient norm.
         */
        double updateDG( std::vector<double>& deltaG, const std::vector<double>& grad,
                       const std::vector<double>& deltaX, const std::vector<double> & direction,
                       double& dDg, double& dxG, double& dxDg )const;

        /**
         * Update the variable values and the delta X vector.
         * @param deltaX - delta X vector.
         * @param direction - the direction to move.
         * @param t - the step size to move.
         */
        void updateVariablesValues( std::vector<double>& deltaX, const std::vector<double> & direction, const double t );

        /**
         * Update the vectors v and w.
         * @param t - the step size to move.
         * @param v - vector v.
         * @param w - vector w.
         * @param deltaG - delta G vector.
         * @param grad - gradient vector.
         * @param deltaX - delta X vector.
         * @param dDg - result of the dot product between the direction and delta G
         * vectors.
         * @param dxG - result of the dot product between the gradient and delta X
         * vectors.
         * @param dxDg - result of the dot product between the delta X and delta G
         * vectors.
         */
        void updateVWVectors( const double t, std::vector<double>&v, std::vector<double>& w,
                                const std::vector<double>& deltaG, std::vector<double>& grad,
                                const std::vector<double>& deltaX, const double dDg, const double dxG,
                                const double dxDg ) const;
    public:
        /**
         * Default construtor.
         */
        LBFGSOptimizationSolver( );

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );
    };
} // namespace Solver
