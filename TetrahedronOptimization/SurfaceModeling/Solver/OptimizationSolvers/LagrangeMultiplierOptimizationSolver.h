#pragma once

#include "OptimizationSolver.h"

namespace Solver
{

    class LagrangeMultiplierOptimizationSolver : public OptimizationSolver
    {

    private:
        /**
         * Number of iterations to iterative linear system solve.
         */
        unsigned int _numberOfSubIterations;

        /**
         * The number of threads that will be used by the solver.
         */
        unsigned int _numberOfThreads;
    private:

        class Element
        {

        public:
            unsigned int i, j;
            double value;
        public:

            bool operator<( const Element& e ) const
            {
                if ( i < e.i )
                    return true;
                if ( i == e.i && j < e.j )
                    return true;
                return false;
            }
        };
    private:
        /**
         * Preprocessing the matrix to put in a CSR format.
         * @param numberUnknownV - number of unknown variables.
         * @param b - b vector.
         * @param elements - pre-processed matrix.
         */
        void preprocessingCSRFormat( unsigned int numberUnknownV,
                                     std::vector<double>* b,
                                     std::vector<Element> *elements ) const;

    public:
        /**
         * Default construtor.
         */
        LagrangeMultiplierOptimizationSolver( );

        /**
         * Get the max iteration number that must be used by the iterative linear
         * solver in each minimization iteration.
         * @return - the max iteration number that must be used by the iterative
         * linear solver
         */
        unsigned int getNumberOfSubIterations( ) const;

        /**
         * Get the number of threads that will be used by the solvers.
         * @param nt - number of threads that will be used by the solvers.
         */
        unsigned int getNumberOfThreads( ) const;

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );

        /**
         * Define the max iteration number to be used by the iterative linear
         * solver in each minimization iteration.
         * @param i - the max iteration number to be used by the iterative linear
         * solver
         */
        void setNumberOfSubIterations( unsigned int i );

        /**
         * Define the number of threads to be used by the solver.
         * @param nt - the number of threads.
         */
        void setNumberOfThreads( unsigned int nt );
    };
} // namespace Solver
