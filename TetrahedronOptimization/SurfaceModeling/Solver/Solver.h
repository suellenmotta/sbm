#pragma once

#include <vector>

namespace ModelManager
{
    class Model;
    class OptimizationModel;
    class DSIModel;
    class Variable;
    class Constraint;
    class QuadraticExpression;
}

namespace Solver
{

    class Solver
    {
    protected:
        /**
         * Tolerance that will be used as stopped condition by the algorithm.
         */
        double _eps;

        /**
         * Number of iterations.
         */
        unsigned int _iterations;

        /**
         * The maximum iterations number to be performed by the algorithm.
         */
        unsigned int _maxIterations;

        /**
         * The number of variables on the problem.
         */
        unsigned int _numberVariables;

        /**
         * The number of constraints on the problem.
         */
        unsigned int _numberConstraints;

        /**
         * Number of threads to be used on solver.
         */
        int _numberOfThreads;

        /**
         * Flag that determines if the solver must show messages or not.
         */
        bool _showLog;

        /**
         * Number of iterations to show the log information.
         */
        unsigned int _stepsNumber;

        /**
         * The variables vector.
         */
        ModelManager::Variable *_variables;

        /**
         * The constraints vector.
         */
        ModelManager::Constraint *_constraints;

        /**
         * Show the current solution.
         * @param gnorm - gradient norm.
         * @return - false if the solver must be stopped and true if it must to
         * continue.
         */
        virtual bool showCurrentSolution( const double gnorm = 0.0 );

        /**
         * Get the current tolerance.
         * @return - the current tolerance.
         */
        double getEPS( ) const;

        /**
         * Get the number of iterations used to solve the last model.
         * @return - number of iterations used to solve the last model.
         */
        unsigned int getNumberIterations( ) const;

        /**
         * Get the current maximum iterations.
         * @return - the current maximum iterations.
         */
        unsigned int getMaxIterations( ) const;

        /**
         * Get the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @return - steps number.
         */
        unsigned int getStepsNumber( ) const;

        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        virtual bool optimize( ) = 0;

        /**
         * Set a callback function to be called after stepsNumber iterations, i.e,
         * after each stepsNumber iterations this function is called with the current
         * variable values. If stepsNumber have no set, this function will be called
         * after each iteration. For direct methods, this function will be called just
         * two times: the former with the initial solution, and the latter with
         * the final solution.
         * @param displayInformation - pointer to a function that must be called
         * with the current variables values. This function must return false to
         * interrupt the solve or true to continue.
         * @param object - object passed to be used in static function.
         */
        void setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm, void *obj ), void *object );

        /**
         * Define a new upper bound to the number of iterations.
         * @param maxIter - new upper bound to the number of iterations.
         */
        void setMaxIterations( const unsigned int maxIter );

        /**
         * Set the number of threads that must be used.
         * @param numThreads - number of threads that must be used.
         */
        void setNumberThreads( const int numThreads );

        /**
         * Set if the solver must show the log or not.
         * @param log - true if the solve need to show the log and false otherwise.
         */
        void showLog( bool log );

        /**
         * Set the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @param sn - steps number.
         */
        void setStepsNumber( unsigned int sn );

    private:
        /**
         * Initialize the solver.
         */
        void initializeSolver( );

    private:

        /**
         * Function to be called after each _stepsNumber iterations with the current
         * variable values. This function must return false to interrupt the solve
         * or true to continue.
         */
        bool (*_displayInformation )( const std::vector<double>& x, double gnorm, void *obj );

        /**
         * Object to be used in static function.
         */
        void *_object;

        /** 
         * Auxiliar vector to print the current solution.
         */
        std::vector<double> _x;

        /**
         * Define a new tolerance to be used by the solver.
         * @param eps - new tolerance to be used by the solver.
         */
        void setEPS( const double eps );

    private:

        /**
         * Define the set of constraints of the problem. Just the model use
         * this function.
         * @param c - the constraints vector.
         * @param m - the number of constraints.
         */
        void setConstraints( ModelManager::Constraint *c, const unsigned int m );

        /**
         * Define the set of variables of the problem. Just the model use
         * this function.
         * @param v - the variables vector.
         * @param n - the number of variables.
         */
        void setVariables( ModelManager::Variable *v, const unsigned int n );
    public:
        /**
         * Set Model to access the private information
         */
        friend class ModelManager::Model;
        friend class ModelManager::OptimizationModel;
        friend class ModelManager::DSIModel;
        friend class OptimizationSolver;
        friend class DSIBasedSolver;

        /**
         * Default constructor.
         */
        Solver( );

        /**
         * Destructor.
         */
        virtual ~Solver( );

    };

} // namespace Solver
