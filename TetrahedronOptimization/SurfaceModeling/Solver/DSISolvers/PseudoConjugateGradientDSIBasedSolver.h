#pragma once

#include "DSIBasedSolver.h"

namespace Solver
{

    class PseudoConjugateGradientDSIBasedSolver : public DSIBasedSolver
    {

    public:
        bool optimize( );

    };
} // namespace Solver

