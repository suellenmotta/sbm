#pragma once

#include "DSIBasedSolver.h"

namespace Solver
{

    class GlobalDSIBasedSolver : public DSIBasedSolver
    {

    public:
        bool optimize( );

    };
} // namespace Solver

