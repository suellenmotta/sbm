#pragma once

#include "DSIBasedSolver.h"

namespace Solver
{

    class ConjugateGradientDSIBasedSolver : public DSIBasedSolver
    {

    public:
        bool optimize( );

    };
} // namespace Solver

