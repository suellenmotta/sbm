#pragma once

#include "DSIBasedSolver.h"

namespace Solver
{

    class LocalDSIBasedSolver : public DSIBasedSolver
    {

    public:
        /**
         * Optimize the model.
         * @return - true if the problem converges and false otherwise.
         */
        bool optimize( );

    };
} // namespace Solver

