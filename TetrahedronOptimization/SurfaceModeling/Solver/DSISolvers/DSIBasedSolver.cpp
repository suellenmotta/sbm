#include "DSIBasedSolver.h"

namespace Solver
{



    Graph::DSIGraph* DSIBasedSolver::getGraph( ) const
    {
        return _graph;
    }



    void DSIBasedSolver::setGraph( Graph::DSIGraph *g )
    {
        _graph = g;
    }



    DSIBasedSolver::~DSIBasedSolver( )
    {

    }

} // namespace Solver
