/*
 * Timer.h
 *
 *  Created on: Nov 26, 2013
 *      Author: jduartejr
 */

#ifndef TIMER_H_
#define TIMER_H_
#include <cstdio>
#include <ctime>

class Timer
{

public:

    Timer( )
    {
        clock_gettime( CLOCK_REALTIME, &_startTime );
    }

    inline void restart( )
    {
        clock_gettime( CLOCK_REALTIME, &_startTime );
    }

    inline double elapsed( ) const
    {
        timespec endTime;
        clock_gettime( CLOCK_REALTIME, &endTime );
        return (endTime.tv_sec - _startTime.tv_sec ) + ( double ) ( endTime.tv_nsec - _startTime.tv_nsec ) / 1000000000.0;
    }

    inline void printTime( const char* timerString ) const
    {
        double time = elapsed( );
        printf( "%s: ", timerString );

        if ( time < 1.0 )
        {
            printMS( timerString, ( int ) ( 10 * time + 0.5 ) );
        }
        else if ( time < 60 )
        {
            printS( timerString, ( int ) ( time + 0.5 ) );
        }
        else if ( time < 3600 )
        {
            printM( timerString, ( int ) ( time + 0.5 ) );
        }
        else
        {
            printH( timerString, ( int ) ( time + 0.5 ) );
        }
    }

private:
    timespec _startTime;

private:

    inline void printMS( const char* timerString, int ms ) const
    {
        printf( "%d ms\n", ms );
    }

    inline void printS( const char* timerString, int s ) const
    {
        printf( "%d secs\n", s );
    }

    inline void printM( const char* timerString, int m ) const
    {
        if ( m % 60 == 0 )
        {
            printf( "%d mins\n", m / 60 );
        }
        else
        {
            printf( "%d mins %d secs\n", m / 60, m % 60 );
        }
    }

    inline void printH( const char* timerString, int m ) const
    {
        if ( m % 3600 == 0 )
        {
            printf( "%d hs\n", m / 3600 );
        }
        else if ( ( m / 3600 ) % 60 == 0 )
        {
            printf( "%d hs %d mins\n", m / 3600, ( m % 3600 ) / 60 );
        }
        else
        {
            printf( "%d hs %d mins %d secs\n", m / 3600, ( m % 3600 ) / 60, ( m % 3600 ) % 60 );
        }
    }
};

#endif /* TIMER_H_ */