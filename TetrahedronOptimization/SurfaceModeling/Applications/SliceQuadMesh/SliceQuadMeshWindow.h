/* 
 * File:   IupGLCanvasDummy.h
 * Author: jeferson
 *
 * Created on August 31, 2014, 9:28 AM
 */

#ifndef IUPGLCANVASDUMMY_H
#define IUPGLCANVASDUMMY_H
#include <string>
#include <mutex> 
#include <future>
#include <vector>
#include <thread>
#include <string>
#include <vector>
#include "../../ModelManager/Variable.h"

namespace Solver
{
    class OptimizationSolver;
}

namespace ModelManager
{
    class OptimizationModel;
}

class SliceQuadMeshWindow
{
public:
    /**
     * Construtor default da classe.
     */
    SliceQuadMeshWindow( );

    /**
     * Destrutor da classe.
     */
    virtual ~SliceQuadMeshWindow( );

    /**
     * Exibe a janela.
     */
    void show( );

    /**
     * Oculta a janela.
     */
    void hide( );

private:

    /**
     * Structure to represent a point by your variables.
     */
    struct VarPoint
    {
        ModelManager::Variable x, y;
    };

    /**
     * Structure to represent a point by your values.
     */
    struct Point
    {
        double x, y;
    };
private:
    /**
     * Mesh incidencies.
     */
    std::vector<unsigned int> _quadMesh;

    /**
     * Known points to be rendered.
     */
    std::vector<unsigned int> _knownPoints;

    /**
     * Border to be rendered.
     */
    std::vector<unsigned int> _borderPoints;

    /**
     * Mesh coordinates.
     */
    std::vector<double> _coordinates;
private:

    /**
     * Number of data lines and columns
     */
    unsigned int _lines, _columns;

    /**
     * Entry points values.
     */
    std::vector<Point> _entryPoints;

    /**
     * Store wich mouse button are pressed.
     */
    int _mousePressed;

    /**
     * Store the first clicked point in pixel coordinates. This data is used to
     * move the slice.
     */
    int _firstPixelX, _firstPixelY;

    /**
     * Variable used to scale to data.
     */
    double _scale = 1;

    /**
     * Store the AABB.
     */
    Point _pMin, _pMax;

    /**
     * Current solver.
     */
    Solver::OptimizationSolver *_solver;

    /**
     * Current optimization model.
     */
    ModelManager::OptimizationModel *_m;

    /**
     * Mutex used to protect the current solution copy.
     */
    std::mutex _mtx;

    /**
     * Future variable used to check if the thread is finished.
     */
    std::future<void> _fut;

    /**
     * Flag used to cancel the thread.
     */
    bool _continueSolver;
private:
    /**
     * Dialog pointer.
     */
    Ihandle *_dialog;

private:

    /**
     * Create a set of menus.
     * @return - pointer to main menu. That is a menu's composition.
     */
    Ihandle* createMenus( );

    /**
     * Cria janela da IUP e define suas configuracoes e callbacks.
     */
    void createWindow( );

    /**
     * Incializa algumas propriedades do canvas OpenGL.
     */
    void initialize( );

    /**
     * Compute the (i, j) linear index point.
     * @param (i, j) - matrix index of the point.
     * @return - the linear index.
     */
    unsigned int computeVertexIndex( unsigned int i, unsigned int j );

    /**
     * Open mesh and initialize the variables.
     * @param path - mesh's path.
     * @param initIndex - init mesh index.
     */
    void openMesh( const std::string& path, unsigned int initIndex );

    /**
     * Lee o arquivo de entrada.
     */
    void readFile( std::string fileName );


    void renderScene( );

    /**
     * Update window labels with solver information.
     * @param numIter - number of iterations.
     * @param gnorm - gradient norm.
     */
    void updateWindowLabes( unsigned int numIter, double gnorm );

    /**
     * 
     * @param v
     */
    bool updateSolution( const std::vector<double>& v, double gnorm );

    /**
     * 
     */
    void buildModel( );

    /**
     * Trata evento de redimensionar o canvas OpenGL.
     * @param width - nova larguda do canvas.
     * @param height - nova altura do canvas.
     */
    void resizeCanvas( int width, int height );

    void zoom( int delta );

    void convertPixelToWorld( int px, int py, double& x, double& y );

    void optimize( double eps, unsigned numIterations );

    /**
     * Habilita os componentes exceto o Stop
     * @param component
     */
    void activateComponents( Ihandle* component );

    /**
     * Initialize the initial solution with zero or entry solution.
     * @param initIndex - 1 - zero, 2 - entry mesh.
     */
    void initializeSolution( unsigned int initIndex );

    /**
     * Allocate a solver.
     * @param index - solver index in the dropdown list.
     */
    void allocateSolver( int index );

private:
    /**
     * Callback do botao de fechar a janela.
     * @param button - ihandle do botao de sair.
     * @return - retorna IUP_CLOSE para que a janela seja fechada.
     */
    static int exitButtonCallback( Ihandle *button );
    static int runButtonCallback( Ihandle *button );
    static int stopButtonCallback( Ihandle *button );
    static int checkFunction( );
    static void optimizeMeshCallback( );


    /**
     * Callback do botao de fechar a janela.
     * @param button - ihandle do botao de sair.
     * @return - retorna IUP_CLOSE para que a janela seja fechada.
     */
    static int openMeshMenuCallback( Ihandle *button );

    /**
     * 
     * @param initList
     * @return 
     */
    static int actionInitListCallback( Ihandle* initList, char *text, int item, int state );
    static int actionSolverListCallback( Ihandle* solverList, char *text, int item, int state );


    /**
     * Callback responsavel por receber evento de redesenho do canvas.
     * @param canvas - ponteiro para o canvas.
     * @return  - IUP_DEFAULT.
     */
    static int actionCanvasCallback( Ihandle *canvas );

    /**
     * Callback responsavel por receber eventos do whell do mouse no canvas para
     * realizar a operacao de Zoom.
     * @param canvas - ponteiro para o canvas.
     * @param delta - vale -1 ou 1 e indica a direcao da rotacao do botao whell.
     * @param x - posicao x do mouse na tela.
     * @param y - posicao y do mouse na tela.
     * @param status - status dos botoes do mouse e certas teclas do teclado no 
     * momento que o evento foi gerado.
     * @return - IUP_DEFAULT.
     */
    static int wheelCanvasCallback( Ihandle *canvas, float delta, int x,
                                    int y, char *status );

    /**
     * Callback responsavel por receber eventos de resize do canvas.
     * @param canvas - ponteiro para o canvas.
     * @param width - nova largura, em pixeis, da janela.
     * @param heigth - nova altura, em pixeis, da janela.
     * @return - IUP_DEFAULT.
     */
    static int resizeCanvasCallback( Ihandle *canvas, int width, int height );

    /**
     * Callback responsavel por receber eventos de teclado do canvas.
     * @param canvas - ponteiro para o canvas.
     * @param button - identificador do botao, podem ser BUTTON_1, BUTTON_2, ...
     * @param pressed - 1 para o caso do botao esta sendo pressionado e 0 caso
     * contrario.
     * @param x - posicao x do mouse na tela.
     * @param y - posiao y do mouse na tela.
     * @param status - status dos botoes do mouse e certas teclas do teclado no 
     * momento que o evento foi gerado.
     * @return - IUP_DEFAULT. 
     */
    static int buttonCanvasCallback( Ihandle* canvas, int button, int pressed,
                                     int x, int y, char* status );

    static int motionCanvasCallback( Ihandle* canvas, int x, int y, char* status );

    /**
     * 
     * @param v
     */
    static bool displayFunction( const std::vector<double>& v, double gnorm );

};

#endif /* IUPGLCANVASDUMMY_H */

