#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "renderopengl.h"
#include "meshgrid.h"
#include "meshelements.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_Mesh_triggered();

    void on_pushButton_clicked();

    void on_actionExit_triggered();

    void on_pushButtonRun_clicked();

    void on_pushButtonStop_clicked();

    void on_Initial_currentIndexChanged(const QString &arg1);

    void on_Solver_currentIndexChanged(const QString &arg1);

    void on_actionSalvar_Print_triggered();

    void on_actionSalve_Mesh_triggered();

private:
    Ui::MainWindow *ui;
    void fill_initial_solution();
    void fill_solver();
    int lines,col;
};

#endif // MAINWINDOW_H
