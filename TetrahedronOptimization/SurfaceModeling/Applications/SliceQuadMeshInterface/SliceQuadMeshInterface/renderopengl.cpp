#include "renderopengl.h"
#include <QMouseEvent>
#include <iostream>
#include <string>
#include <QElapsedTimer>
#include <QMatrix4x4>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include "ModelManager/Variable.h"
#include "ModelManager/OptimizationModel.h"
#include "Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"

struct VarPoint
{
  ModelManager::Variable x, y;
};

RenderOpenGl::RenderOpenGl(QWidget* parent)
    : QOpenGLWidget(parent),
      program(nullptr),programPoints(nullptr)
{
    pass=false;
    _m1 = 0;
    _sol1 = 0;
    _m2 = 0;
    _sol2 = 0;
    _zoomaspect=1;
    _distance=QVector3D(0,0,0);
    _mousepress=false;
    this->setFocus();
    _thread = 0;
}

RenderOpenGl::~RenderOpenGl()
{
    makeCurrent();
    doneCurrent();
    delete _m1;
    delete _m2;
    delete _sol1;
    delete _sol2;
    delete program;
    delete programPoints;
}

void RenderOpenGl::initializeGL()
{
    initializeOpenGLFunctions();
    makeCurrent();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0,0,width(),height());

    //Layout de ponto e linha:
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_POINT_SMOOTH);
    glPointSize(4.0f);

    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    program = new QOpenGLShaderProgram();
    programPoints = new QOpenGLShaderProgram();

    //Vertex Shader
    if (program->addShaderFromSourceFile(QOpenGLShader::Vertex, "../SliceQuadMeshInterface/vertexShader.glsl"))
        printf("Vertex Shader added successfully\n");
    else
        printf("Problemas ao adicionar vertex shader\n");

    //Fragment Shader
    if (program->addShaderFromSourceFile(QOpenGLShader::Fragment, "../SliceQuadMeshInterface/fragmentShader.glsl"))
        printf("Fragment Shader added successfully\n");
    else
        printf("Problemas ao adicionar fragment shader\n");

    //Geometry Shader
    if (program->addShaderFromSourceFile(QOpenGLShader::Geometry, "../SliceQuadMeshInterface/geometryShader.glsl"))
        printf("Geometry Shader added successfully\n");
    else
        printf("Deu ruim addando o geometry shader\n");

    program->link();
    if (!program->isLinked())
        printf("O programa não foi linkado");

    //Vertex Shader Points
    if (programPoints->addShaderFromSourceFile(QOpenGLShader::Vertex, "../SliceQuadMeshInterface/vertexShaderPoints.glsl"))
        printf("Vertex Shader added successfully\n");
    else
        printf("Problemas ao adicionar vertex shader\n");

    //Fragment Shader Points
    if (programPoints->addShaderFromSourceFile(QOpenGLShader::Fragment, "../SliceQuadMeshInterface/fragmentShaderPoints.glsl"))
        printf("Fragment Shader added successfully\n");
    else
        printf("Problemas ao adicionar fragment shader\n");

    programPoints->link();

    if (!programPoints->isLinked())
        printf("O programa não foi linkado");

    program->bind();
    createWireFrameTexture();
    GLint wireFrameLocation = glGetUniformLocation(program->programId(), "wireFrameTexture");
    glUniform1i(wireFrameLocation,  1);

}

void RenderOpenGl:: set_Minmax(const std::vector< std::vector< Point > >& matrix)
{
    min = max = minfirst=maxfirst=QVector3D(matrix[0][0].x,matrix[0][0].y,0);
    for (unsigned int i = 0; i < matrix.size(); i++ )
    {
        for ( unsigned int j = 0; j < matrix[i].size(); j++ )
        {
            if(matrix[i][j].x>max.x())
            {
                max.setX(matrix[i][j].x);
            }
            else if (matrix[i][j].x<min.x())
            {
                min.setX(matrix[i][j].x);
            }
            if(matrix[i][j].y>max.y())
            {
                max.setY(matrix[i][j].y);
            }
            else if (matrix[i][j].y<min.y())
            {
                min.setY(matrix[i][j].y);
            }
        }
    }

    _scale = fabs( min.x() - max.x() ) / fabs( min.y() - max.y() );
    minfirst=min;
    maxfirst=max;
    updateProjectionMatrixOpenFile();
}

void RenderOpenGl::createWireFrameTexture()
{
    glGenTextures( 1, &WireframeId);

    glBindTexture(GL_TEXTURE_1D, WireframeId);

    std::vector<unsigned char> texture(1022, 0);
    texture.push_back( 80 );
    texture.push_back( 255 );

    //Enviar a imagem para o OpenGL
    unsigned int i = 0, j = 0, TAM = texture.size();
    for (i = 0, j = TAM; j > 4; j /= 2, i++)
    {
        glTexImage1D( GL_TEXTURE_1D, i, GL_RED, j, 0, GL_RED, GL_UNSIGNED_BYTE, &texture[0] + ( TAM - j ) );
    }

    //To avoid saturation
    texture[TAM - 2] /= 3;
    texture[TAM - 1] /= 3;

    for (; j > 0; j /= 2, i++)
    {
        glTexImage1D( GL_TEXTURE_1D, i, GL_RED, j, 0, GL_RED, GL_UNSIGNED_BYTE,  &texture[0] + ( TAM - j ) );
    }

    // Define os filtros.
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
}

int RenderOpenGl::computeIndex(int i, int j) const
{
    return i * _columns + j;
}

void RenderOpenGl::set_Triangles()
{
    triangles.reserve( 6 *  ( _columns - 1 ) * ( _lines - 1) );
    for ( int i = 0; i < _lines - 1; i++ )
    {
        for ( int j = 0; j < _columns - 1; j++ )
        {
            /* First Triangle
             * 1
             * 2  3
             */
            triangles.push_back( computeIndex( i + 0, j + 0 ) );
            triangles.push_back( computeIndex( i + 1, j + 0 ) );
            triangles.push_back( computeIndex( i + 1, j + 1 ) );

            /* Second Triangle
             * 3  2
             *    1
             */
            triangles.push_back( computeIndex(i + 1, j + 1) );
            triangles.push_back( computeIndex(i + 0, j + 1) );
            triangles.push_back( computeIndex(i + 0, j + 0) );
        }
    }
    pass = true;
}

void RenderOpenGl::set_Vector( const std::vector< std::vector< Point > >& matrix)
{
    points.clear();
    for ( unsigned int i = 0; i < matrix.size(); i++ )
    {
        for ( unsigned int j = 0; j < matrix[i].size(); j++ )
        {
            points.push_back( matrix[i][j] );
        }
    }
}

void RenderOpenGl::updateRenderPoints(  )
{
    //ver se isso é possivel
    glGenBuffers(1, &Buffer);
    glBindBuffer(GL_ARRAY_BUFFER,Buffer);
    glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(Point),
                 &points[0],GL_DYNAMIC_DRAW);
}

void RenderOpenGl::setMatrixInfo( int lines, int columns, const std::vector< std::vector< Point > >& Matrix )
{
    int count=0,aux;
    _input = Matrix;
    _lines = lines;
    _columns = columns;

    //matrix= Matrix;

    set_Minmax( _input );
    set_Triangles();
    set_Vector( _input );

    updateRenderPoints( );

    //Preenchendo vetor de borda e de pontos conhecidos
    for ( int j = 0; j < _columns; j++ )
    {
        for ( int i = 0; i < _lines; i++ )
        {
            aux=computeIndex(i,j);
            if(i > 1 && i < _lines - 2 && j > 1 && j<_columns - 2)
            {

                if(points[aux].known==true)
                {
                    _indknown.push_back(aux);
                }
                continue;
            }
            _indborda.push_back(aux);
            count++;
        }
    }
}

void RenderOpenGl::paintGL()
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    program->bind();
    //Emite sinal para que mostre o tempo não funcionando corretamente ainda
    QElapsedTimer aux;
    aux.start();
    QString posText = tr("Current Iteration = %1         Gradient Norm = %2").arg( aux.msecsSinceReference() ).arg(
                aux.msecsSinceReference() );

    view.setToIdentity();
    view.scale(QVector3D(_scale, 1, 1));
    //emit updateTime(posText);
    /*///////////////////////////////////////*/
    glEnableVertexAttribArray(0);
    //glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, Buffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,sizeof(Point),0);
    program->setUniformValue("transformMatrix", proj*view);
    program->setUniformValue("color", QVector3D(1,1,1)); // Branco
    GLuint wireFrameLocation = glGetUniformLocation(program->programId(), "wireFrameTexture");


    if(points.size()!=0 && pass==true)
    {
        glEnable(GL_TEXTURE_1D);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_1D, WireframeId);
        glDrawElements(GL_TRIANGLES, triangles.size(), GL_UNSIGNED_INT, &triangles[0]);
        programPoints->bind();
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, Buffer);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,sizeof(Point),0);
        programPoints->setUniformValue("transformMatrix", proj*view);
        programPoints->setUniformValue("color", QVector3D(1,0,0)); // Vermelho
        glDrawElements(GL_POINTS, _indborda.size(), GL_UNSIGNED_INT, &_indborda[0]);
        programPoints->setUniformValue("color", QVector3D(0,1,0)); // Vermelho
        glDrawElements(GL_POINTS, _indknown.size(), GL_UNSIGNED_INT, &_indknown[0]);

    }
}

void RenderOpenGl::resizeGL(int width, int height)
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    double widthFactor = ( double ) width / viewport[2];
    double heightFactor = ( double ) height / viewport[3];

    glViewport(0, 0, width, height);


    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( min.x() - max.x() );
    double heightWorld = fabs( min.y() - max.y() );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( min.x() + max.x() ) / 2.0;
    double cy = ( min.y() + max.y() ) / 2.0;

    //calcula o novo sistema de coordenadas
    min.setX( cx - 0.5 * widthWorld );
    max.setX( cx + 0.5 * widthWorld );
    min.setY( cy - 0.5 * heightWorld );
    max.setY( cy + 0.5 * heightWorld );

    proj.setToIdentity();
    proj.ortho( min.x(), max.x(), min.y(), max.y(), -1, 1 );
}

void RenderOpenGl::mouseMoveEvent(QMouseEvent *event)
{
    //Emite sinal para que mostre o tempo não funcionando corretamente ainda
    //     QElapsedTimer aux;
    //     aux.start();
    //     QString posText = tr("Current Iteration = %1         Gradient Norm = %2").arg( aux.msecsSinceReference() ).arg(
    //                           aux.msecsSinceReference() );
    //     emit updateTime(posText);

    //Pan
    if(_mousepress)
    {
        QVector3D point(event->x(), height()-event->y(), 0 );
        _currentpoint = point.unproject( view, proj, QRect(0,0,width(),height()));
        _currentpoint.setZ(0.f);
        _distance = _currentpoint-_firstpoint;

        min.setX( min.x() -_distance.x() );
        min.setY( min.y() -_distance.y() );
        max.setX( max.x() -_distance.x() );
        max.setY( max.y() -_distance.y() );

        proj.setToIdentity();
        proj.ortho(min.x(), max.x(), min.y(), max.y(), -1, 1);
    }
    update();
}

void RenderOpenGl::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::MiddleButton || event->button() == Qt::RightButton )
    {
        //Zoom Fit
        _distance=QVector3D(0,0,0);
        _zoomaspect=1;
        min=minfirst;
        max=maxfirst;
        updateProjectionMatrixOpenFile();
    }
    if(_mousepress == false && event->button() == Qt::LeftButton)
    {
        _mousepress=true;
        QVector3D point(event->x(), height()-event->y(), 0 );
        _firstpoint = point.unproject( view, proj, QRect(0,0,width(),height()));
        _firstpoint.setZ(0.f);
    }
    update();
}

void RenderOpenGl::mouseReleaseEvent(QMouseEvent *event)
{
    _mousepress = false;
    update();
}

void RenderOpenGl::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Plus)
    {
        ZoomIn();

    }
    else if (event->key() == Qt::Key_Minus)
    {
        ZoomOut();
    }
    update();
}

void RenderOpenGl::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 0)
    {
        ZoomIn();
    }
    else if(event->delta() < 0)
    {
        ZoomOut();

    }

    update();
}

void RenderOpenGl::ZoomIn()
{
    _zoomaspect = 0.95;
    updateProjectionMatrixZoom( _zoomaspect );
}

void RenderOpenGl::ZoomOut()
{
    _zoomaspect = 1.05;
    updateProjectionMatrixZoom( _zoomaspect );
}

void RenderOpenGl::updateProjectionMatrixPan()
{

}

void RenderOpenGl::updateProjectionMatrixOpenFile()
{
    if (max == min)
        return;

    //Recupera o viewport para obter as dimensoes da janela.
    int width = this->width();
    int height = this->height();
    printf("%d %d\n", width, height);

    int den = std::min( width, height );

    double widthFactor = ( double ) width / den;
    double heightFactor = ( double ) height / den;

    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( min.x() - max.x() );
    double heightWorld = fabs( min.y() - max.y() );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( min.x() + max.x() ) / 2.0;
    double cy = ( min.y() + max.y() ) / 2.0;

    //calcula o novo sistema de coordenadas
    min.setX( cx - 0.5 * widthWorld );
    max.setX( cx + 0.5 * widthWorld );
    min.setY( cy - 0.5 * heightWorld );
    max.setY( cy + 0.5 * heightWorld );

    proj.setToIdentity();
    proj.ortho( min.x(), max.x(), min.y(), max.y(), -1, 1 );
}


void RenderOpenGl::updateProjectionMatrixZoom( double delta )
{
    // Computa a coordenada do centro da área visível
    double cx = 0.5 * ( min.x() + max.x() );
    double cy = 0.5 * ( min.y() + max.y() );

    // Computa metade do altura de largura visível
    double halfWidth = 0.5 * ( max.x() - min.x() );
    double halfHeight = 0.5 * ( max.y() - min.y() );

    // Aplica o fator de escala para alterar a largura e altura
    halfWidth *= delta;
    halfHeight *= delta;

    min.setX( cx - halfWidth );
    max.setX( cx + halfWidth );
    min.setY( cy - halfHeight );
    max.setY( cy + halfHeight );

    proj.setToIdentity();
    proj.ortho( min.x(), max.x(), min.y(), max.y(), -1, 1 );
}



void RenderOpenGl::updateInitialSolution( InitialSolutionId ini )
{
    _initSolution = ini;
}



void RenderOpenGl::updateCurrentSolver (CurrentSolver solver)
{
    _solver = solver;
}



void RenderOpenGl::setMaxIterations(int maxIterations )
{
    _maxIterations = maxIterations;
}



void RenderOpenGl::setEPS( double eps )
{
    _eps = eps;
}



void RenderOpenGl::setSeparatedDimensions( bool separetedDimensions )
{
    _separetedDimensions = separetedDimensions;
}


void RenderOpenGl::getSolution( const std::vector<double>& x, double gnorm )
{
    if ( (int)x.size() == 2 * _lines * _columns )
    {
        for (int i = 0; i < _lines * _columns; i++)
        {
            points[i].x = x[2 * i + 0];
            points[i].y = x[2 * i + 1];
        }
    }
    else
    {
        for (int i = 0; i < _lines * _columns; i++)
        {
            if (_dimension == 0)
            {
                points[i].x = x[i];
            }
            else
            {
                points[i].y = x[i];
            }
        }
    }
    updateRenderPoints();
    update();
}


void RenderOpenGl::finishedThread()
{
    _thread->wait();
    delete _thread;
    _thread = 0;
    if (_separetedDimensions)
    {
        _dimension = 1;
        runThreads( _m2 );
        _separetedDimensions = false;
    }
}



void RenderOpenGl::runThreads( ModelManager::OptimizationModel *m)
{
    _thread = new OptimizeThread( m );
    connect( _thread, &OptimizeThread::currentSolutionUpdate, this, &RenderOpenGl::getSolution, Qt::QueuedConnection );
    connect( _thread, &OptimizeThread::finished, this, &RenderOpenGl::finishedThread);
    _thread->start();
}



void RenderOpenGl::allocateModels()
{
    using namespace ModelManager;
    using namespace Solver;
    delete _m1;
    delete _sol1;
    delete _m2;
    delete _sol2;
    _m1 = _m2 = 0;
    _sol1 = _sol2 = 0;
    _sol1 = new LBFGSOptimizationSolver();
    _m1 = new OptimizationModel( _sol1 );
    if (_separetedDimensions)
    {
        _sol2 = new LBFGSOptimizationSolver();
        _m2 = new OptimizationModel( _sol2 );
    }
}



void RenderOpenGl::buildModels()
{
    using namespace ModelManager;
    using namespace Solver;

    allocateModels();

    std::vector< std::vector< VarPoint > > v;
    v.resize( _lines );
    for (int i = 0; i < _lines; i++)
    {
        v[i].resize( _columns );
    }

    for (int i = 0; i < _lines; i++)
    {
        for (int j = 0; j < _columns; j++)
        {
            //Allocate variables.
            v[i][j].x = _m1->addVariable();
            if (_separetedDimensions)
            {
                v[i][j].y = _m2->addVariable();
            }
            else
            {
                v[i][j].y = _m1->addVariable();
            }

            //Set initial solutions.
            if (_initSolution  == ZEROCOORD)
            {
                v[i][j].x = 0;
                v[i][j].y = 0;
                points[computeIndex(i, j)].x = 0;
                points[computeIndex(i, j)].y = 0;
            }
            else if (_initSolution == INPUTCOORD)
            {
                v[i][j].x = _input[i][j].x;
                v[i][j].y = _input[i][j].y;
                points[computeIndex(i, j)].x = _input[i][j].x;
                points[computeIndex(i, j)].y = _input[i][j].y;
            }
            else
            {
                v[i][j].x = points[computeIndex(i, j)].x;
                v[i][j].y = points[computeIndex(i, j)].y;
            }

            if (_input[i][j].known)
            {
                LinearExpression l1 = - _input[i][j].x + v[i][j].x;
                LinearExpression l2 = - _input[i][j].y + v[i][j].y ;

                _m1->addConstraint( l1, SOFT, 0 );
                if (_separetedDimensions)
                {
                    _m2->addConstraint( l2, SOFT, 0 );
                }
                else
                {
                    _m1->addConstraint( l2, SOFT, 0 );
                }
            }
        }
    }

    //Build the objective function.
    QuadraticExpression &obj1 = _m1->getObjectiveFunction( );
    QuadraticExpression &obj2 = _m2->getObjectiveFunction( );

    for (int i = 0; i < _lines; i++)
    {
        for (int j = 0; j < _columns; j++)
        {
            LinearExpression x, y;
            if (i - 1 >= 0)
            {
                x += v[i - 1][j + 0].x;
                y += v[i - 1][j + 0].y;
            }
            if (i + 1 < _lines)
            {
                x += v[i + 1][j + 0].x;
                y += v[i + 1][j + 0].y;
            }

            if (j - 1 >= 0)
            {
                x += v[i + 0][j - 1].x;
                y += v[i + 0][j - 1].y;
            }
            if (j + 1 < _columns)
            {
                x += v[i + 0][j + 1].x;
                y += v[i + 0][j + 1].y;
            }

            x -= x.size( ) * v[i][j].x;
            y -= y.size( ) * v[i][j].y;

            obj1 += x * x;
            if (_separetedDimensions)
            {
                obj2 += y * y;
            }
            else
            {
                obj1 += y * y;
            }
        }
    }

    for (int i = 0; i <  _lines; i++)
    {
        for (int j = 0; j < _columns; j++)
        {
            if (i > 1 && i < _lines - 2 && j > 1 && j < _columns - 2)
            {
                continue;
            }

            _m1->addConstraint( v[i][j].x, EQUAL, _input[i][j].x );
            if (_separetedDimensions)
            {
                _m2->addConstraint( v[i][j].y, EQUAL, _input[i][j].y );
            }
            else
            {
                _m1->addConstraint( v[i][j].y, EQUAL, _input[i][j].y );
            }
        }
    }

    _m1->setSolverEps( _eps );
    _m1->setMaxIterations( _maxIterations );
    _m1->setStepsNumber( 10 );
    _m1->setSoftConstraintsWeight(100);

    if (_separetedDimensions)
    {
        _m2->setSolverEps( _eps );
        _m2->setMaxIterations( _maxIterations );
        _m2->setStepsNumber( 10 );
        _m2->setSoftConstraintsWeight(100);
    }
}


void RenderOpenGl::runOptimization()
{
    buildModels();

    _dimension = 0;
    runThreads( _m1 );
}

void RenderOpenGl::stopThread()
{
    if (_thread)
    {
        _thread->stopThread();
    }
}
std::vector<Point> RenderOpenGl:: Get_Matrix()
{
    return points;
}
