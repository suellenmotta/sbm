#ifndef RENDEROPENGL_H
#define RENDEROPENGL_H

#include <QOpenGLWidget>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include "optimizethread.h"
#include "slicemeshoptimization.h"
#include "ModelManager/OptimizationModel.h"
#include "Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"

enum InitialSolutionId
{
    CURRENTCOORD,
    INPUTCOORD,
    ZEROCOORD
};

enum CurrentSolver
{
    LBFGS,
    LIS,
    SLU
};


class RenderOpenGl
    : public QOpenGLWidget
    , protected QOpenGLFunctions
{
Q_OBJECT

public:
    explicit RenderOpenGl(QWidget* parent = 0);
    ~RenderOpenGl();
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void setMatrixInfo(int lines, int columns, const std::vector< std::vector< Point > >& Matrix);
    std::vector<Point>  Get_Matrix();

public:
    void updateInitialSolution( InitialSolutionId ini );
    void updateCurrentSolver (CurrentSolver solver);
    void setMaxIterations(int maxIterations );
    void setEPS( double eps );
    void setSeparatedDimensions( bool separetedDimensions );
    void runOptimization();
    void stopThread();
signals:
    void updateTime(const QString& message);

private slots:
    void getSolution( const std::vector<double>& x, double gnorm );

private:
    QOpenGLShaderProgram* program;
    QOpenGLShaderProgram* programPoints;
    OptimizeThread *_thread;
    QMatrix4x4 view;
    QMatrix4x4 proj;
    std::vector< std::vector< Point > > _input;
    std::vector< std::vector< Point > > matrix;
    std::vector< int > triangles;
    std::vector< int > _indborda;
    std::vector< int > _indknown;
    std::vector<Point> points;



    // OpenGL buffers ids
    unsigned int Buffer;
    unsigned int WireframeId;
    QVector3D min,max,minfirst,maxfirst;
    int _lines;
    int _columns;
    bool pass;
    float _zoomaspect;
    QVector3D _firstpoint, _currentpoint,_distance;
    bool _mousepress;
    double _scale;
    int _dimension;

    InitialSolutionId _initSolution;
    CurrentSolver _solver;
    int _maxIterations;
    double _eps;
    bool _separetedDimensions;
    ModelManager::OptimizationModel *_m1;
    Solver::LBFGSOptimizationSolver *_sol1;
    ModelManager::OptimizationModel *_m2;
    Solver::LBFGSOptimizationSolver *_sol2;
private:
    int computeIndex(int i, int j) const;
    void set_Ortho(const std::vector< std::vector< Point > >& matrix);
    void set_Minmax(const std::vector< std::vector< Point > >& matrix);
    void set_Triangles();
    void set_Vector(const std::vector< std::vector< Point > >& matrix);
    void createWireFrameTexture();
    void ZoomIn();
    void ZoomOut();
    void updateProjectionMatrixOpenFile();
    void updateProjectionMatrixPan();
    void updateProjectionMatrixZoom(double delta);
    void updateRenderPoints( );
    void runThreads( ModelManager::OptimizationModel* m );
    void buildModels();
    void allocateModels();
    void finishedThread();
};

#endif // RENDEROPENGL_H
