#ifndef SLICEMESHOPTIMIZATION_H
#define SLICEMESHOPTIMIZATION_H
#include <iostream>
#include <vector>
struct Point
{
    float x, y;
    bool known;
};
class SliceMeshOptimization
{
public:
    SliceMeshOptimization(std::string fileName);
    std::vector< std::vector< Point > > GetMatrix();
    void GetSize(int &lines, int &columns);

private:
void readFile( std::string fileName );
void set_Ortho();

private:
    std::vector< std::vector< Point > > matrix;
    int _lines;
    int _columns;
};

#endif // SLICEMESHOPTIMIZATION_H
