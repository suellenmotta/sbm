#ifndef HORIZON_SURFACE_H
#define HORIZON_SURFACE_H

#include <vector>

#include "Surface.h"
#include "Curve.h"
#include "CurveIntersection.h"
#include "../../../../../../libs/DataStructures/Topologicals/CornerTable/CornerTable.h"

/**
 * Class to represent a surface of an horizon.
 */
class HorizonSurface : public Surface
{
public:
    /**
     * Default constructor.
     */
    HorizonSurface( );

    /**
     * Constructor that receives a set of curves.
     */
    HorizonSurface( std::vector<Curve*>& h, std::vector< std::vector<CurveIntersection> >& i );

    /**
     * Constructor that receives a set of curves, a triangle mesh and the time 
     * index.
     * @param h - a set of curves.
     * @param tmesh - triangle mesh.
     * @param time - time index.
     */
    HorizonSurface( std::vector<Curve*>& h, CornerTable* tmesh, unsigned int time );

    /**
     * Constructor that receives a set of curves, a triangle mesh, and the time.
     */
    HorizonSurface( std::vector<Curve*>& h, CornerTable* tmesh, std::vector< std::vector<CurveIntersection> >& i, unsigned int time );

    /**
     * Add a new horizon curve.
     */
    void addHorizonCurve( Curve* c );

    /**
     * Get the horizon of index i.
     */
    Curve* getHorizonCurve( unsigned int i );

    /**
     * Get the horizon of index i.
     */
    const Curve* getHorizonCurve( unsigned int i ) const;

    /**
     * Get the set of horizon curves.
     */
    std::vector<Curve*>& getHorizonCurves( );

    /**
     * Get the set of horizon curves.
     */
    const std::vector<Curve*>& getHorizonCurves( ) const;

    /**
     * Define set of horizon curves.
     */
    void setHorizonCurves( std::vector<Curve*>& horizonCurves );

    /**
     * Generate the surface mesh.
     */
    CornerTable* generateMesh( );

};
#endif
