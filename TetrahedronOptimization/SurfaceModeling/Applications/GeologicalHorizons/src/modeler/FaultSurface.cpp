#include "FaultSurface.h"



FaultSurface::FaultSurface( ) : Surface( )
{
}



FaultSurface::FaultSurface( std::vector<Curve*>& f ) : Surface( )
{
    _curves = f;
}



FaultSurface::FaultSurface( std::vector<Curve*>& f, CornerTable* tmesh, unsigned int time ) : Surface( )
{
    setFaultCurves( f );
    setTriangleMesh( tmesh );
    setTime( time );
}



FaultSurface::FaultSurface( std::vector<Curve*>& f, CornerTable* tmesh, std::vector< std::vector<CurveIntersection> >& i, unsigned int time )
{
    setFaultCurves( f );
    setTriangleMesh( tmesh );
    setTime( time );
    _intersections = i;
}



void FaultSurface::addFaultCurve( Curve* c )
{
    _curves.push_back( c );
    _intersections.resize( _curves.size( ) );
}



Curve* FaultSurface::getFaultCurve( unsigned int c )
{
    return _curves[c];
}



const Curve* FaultSurface::getFaultCurve( unsigned int c ) const
{
    return _curves[c];
}



std::vector<Curve*>& FaultSurface::getFaultCurves( )
{
    return _curves;
}



const std::vector<Curve*>& FaultSurface::getFaultCurves( ) const
{
    return _curves;
}



void FaultSurface::setFaultCurves( std::vector<Curve*>& faultCurves )
{
    _curves = faultCurves;
    _intersections.resize( _curves.size( ) );
}



CornerTable* FaultSurface::generateMesh( )
{
    return 0;
}
