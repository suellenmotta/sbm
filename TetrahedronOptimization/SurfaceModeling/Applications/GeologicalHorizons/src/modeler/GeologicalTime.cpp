#include "GeologicalTime.h"



GeologicalTime::GeologicalTime( unsigned int t )
{
}


GeologicalTime::~GeologicalTime(  )
{
}



GeologicalTime::GeologicalTime( std::vector<Horizon*>& h, std::vector<Fault*>& f, unsigned int t )
{
    _horizons = h;
    _faults = f;
    _time = t;
}



void GeologicalTime::addFault( Fault* fault )
{
    _faults.push_back(fault);
}



void GeologicalTime::addHorizon( Horizon* horizon )
{
    _horizons.push_back(horizon);
}



Fault* GeologicalTime::getFault( unsigned int i )
{
    return _faults[i];
}



const Fault* GeologicalTime::getFault( unsigned int i ) const
{
    return _faults[i];
}



Horizon* GeologicalTime::getHorizon( unsigned int i )
{
    return _horizons[i];
}



const Horizon* GeologicalTime::getHorizon( unsigned int i ) const
{
    return _horizons[i];
}



unsigned int GeologicalTime::getNumberFaults( ) const
{
    return _faults.size();
}



unsigned int GeologicalTime::getNumberHorizons( ) const
{
    return _horizons.size();
}



unsigned int GeologicalTime::getTime( ) const
{
    return _time;
}



void GeologicalTime::run( )
{
}
