#ifndef SURFACE_H
#define SURFACE_H

#include <vector>

#include "Curve.h"
#include "../../../../../../libs/DataStructures/Topologicals/CornerTable/CornerTable.h"
#include "CurveIntersection.h"

/**
 * Abstract class to represent a surface, your curves and intersections.
 */
class Surface
{
protected:

    /**
     * The set of intersections with this curve. For each surface curve, there is an curve intersection.
     */
    std::vector< std::vector<CurveIntersection> > _intersections;

    /**
     * The time index.
     */
    unsigned int _time;

    /**
     * The triangle surface's triangle mesh.
     */
    CornerTable* _triangleMesh;

    /**
     * Generate the surface mesh.
     */
    virtual CornerTable* generateMesh( ) = 0;

    /**
     * A set of curves that defines the horizons.
     */
    std::vector<Curve*> _curves;

public:
    /**
     * Default constructor.
     */
    Surface( );
    
    /**
     * Destructor.
     */
    ~Surface( );

    /**
     * Constructor that receives the triangle mesh and time index.
     */
    Surface( CornerTable* tmesh, unsigned int time );

    /**
     * Add a new intersection to the current curve.
     */
    void addIntersection( unsigned int c, CurveIntersection i );

    /**
     * The the intersection of index i.
     */
    CurveIntersection& getIntersection( unsigned int c, unsigned int i );

    /**
     * The the intersection of index i.
     */
    const CurveIntersection& getIntersection( unsigned int c, unsigned int i ) const;

    /**
     * Get the number of intersections.
     */
    unsigned int getNumberIntersections( unsigned int c ) const;

    /**
     * Get all curve intersections.
     */
    std::vector<CurveIntersection>& getIntersection( unsigned int c );

    /**
     * Get all curve intersections.
     */
    const std::vector<CurveIntersection>& getIntersection( unsigned int c ) const;

    /**
     * Get the current triangle mesh.
     */
    CornerTable* getTriangleMesh( );

    /**
     * Get the current triangle mesh.
     */
    const CornerTable* getTriangleMesh( ) const;

    /**
     * Set a new triangle mesh
     */
    void setTriangleMesh( CornerTable* triangleMesh );

    /**
     * Get the time index.
     */
    unsigned int getTime( ) const;

    /**
     * Define the set of intersections.
     */
    void setIntersection( unsigned int c, std::vector<CurveIntersection>& intersection );

    /**
     * Define the time index.
     */
    void setTime( unsigned int time );
    
};
#endif
