#include "GeologicalSurfaceModeling.h"



GeologicalSurfaceModeling::GeologicalSurfaceModeling( )
{
}



GeologicalSurfaceModeling::~GeologicalSurfaceModeling( )
{
    for (unsigned int i = 0; i < _horizons.size( ); i++)
    {
        delete _horizons[i];
    }

    for (unsigned int i = 0; i < _faults.size( ); i++)
    {
        delete _faults[i];
    }

    for (unsigned int i = 0; i < _geologicalTimes.size( ); i++)
    {
        delete _geologicalTimes[i];
    }
}



GeologicalSurfaceModeling::GeologicalSurfaceModeling( std::vector<Horizon*>& horizons,
                                                      std::vector<Fault*>& faults,
                                                      std::vector<GeologicalTime*>& times )
{
    _horizons = horizons;
    _faults = faults;
    _geologicalTimes = times;
    _epsIntersections = 0.00005;

    computeIntersections( );
}



void GeologicalSurfaceModeling::computeIntersections( )
{
    //For each horizon
    for (unsigned int hor = 0; hor < _horizons.size( ); hor++)
    {
        //For each time t.
        for (unsigned int tim = 0; tim < _horizons[hor]->getHorizonSurfaces( ).size( ); tim++)
        {
            //Get the horizon surface.
            HorizonSurface* surf = _horizons[hor]->getHorizonSurface( tim );

            //For each curve.
            for (unsigned int curv = 0; curv < surf->getHorizonCurves( ).size( ); curv++)
            {
                //Get the curve.
                Curve* curve = surf->getHorizonCurve( curv );

                //For each polyline.
                for (unsigned int pol = 0; pol < curve->getNumberPolylines( ); pol++)
                {
                    //Get the polyline.
                    const Polyline& polyline = curve->getPolyline( pol );

                    //For each point.
                    for (unsigned int p = 0; p < polyline.size( ); p++)
                    {
                        computeHorizonPointIntersections( hor, tim, curv, pol, p, polyline[p] );
                    }
                }
            }
        }
    }
}



void GeologicalSurfaceModeling::computeHorizonPointIntersections( const unsigned int hor,
                                                                  const unsigned int timeH,
                                                                  const unsigned int curvH,
                                                                  const unsigned int polH,
                                                                  const unsigned int pH,
                                                                  const Point3D& pointH )
{
    //For each fault.
    for (unsigned int f = 0; f < _faults.size( ); f++)
    {

        if (timeH <= _faults[f]->getLastTime( ))
        {
            //Get the surface fault.
            FaultSurface* surf = _faults[f]->getFaultSurface( timeH );

            //For each curve.
            for (unsigned int cF = 0; cF < surf->getFaultCurves( ).size( ); cF++)
            {
                //get curves that will possibly intersect
                Curve* curveF = surf->getFaultCurve( cF );
                Curve* curveH = _horizons[hor]->getHorizonSurface( timeH )->getHorizonCurve( curvH );

                for (unsigned int polF = 0; polF < curveF->getNumberPolylines( ); polF++)
                {
                    Polyline polyline = curveF->getPolyline( polF );
                    for (unsigned int pF = 0; pF < polyline.size( ); pF++)
                    {
                        Point3D pointF = polyline[pF];

                        //verify intersection
                        Point3D v = pointF - pointH;
                        if (v * v < _epsIntersections * _epsIntersections)
                        {
                            CurveIntersection curveIntersectionH( curveF, polH, pH );
                            CurveIntersection curveIntersectionF( curveH, polF, pF );

                            _horizons[hor]->getHorizonSurface( timeH )->addIntersection( curvH, curveIntersectionH );
                            _faults[f]->getFaultSurface( timeH )->addIntersection( cF, curveIntersectionF );
                        }
                    }
                }
            }
        }
    }
}



void GeologicalSurfaceModeling::addHorizon( Horizon* h )
{
    _horizons.push_back( h );
}



void GeologicalSurfaceModeling::addFault( Fault* f )
{
    _faults.push_back( f );
}



void GeologicalSurfaceModeling::addGeologicalTime( GeologicalTime* g )
{
    _geologicalTimes.push_back( g );
}



unsigned int GeologicalSurfaceModeling::getNumberFaults( ) const
{
    return _faults.size( );
}



unsigned int GeologicalSurfaceModeling::getNumberGeologicalTimes( ) const
{
    return _geologicalTimes.size( );
}



unsigned int GeologicalSurfaceModeling::getNumberHorizons( ) const
{
    return _horizons.size( );
}



Fault* GeologicalSurfaceModeling::getFault( unsigned int f )
{
    return _faults[f];
}



const Fault* GeologicalSurfaceModeling::getFault( unsigned int f ) const
{
    return _faults[f];
}



Horizon* GeologicalSurfaceModeling::getHorizon( unsigned int h )
{
    return _horizons[h];
}



const Horizon* GeologicalSurfaceModeling::getHorizon( unsigned int h ) const
{
    return _horizons[h];
}



GeologicalTime* GeologicalSurfaceModeling::getGeologicalTime( unsigned int g )
{
    return _geologicalTimes[g];
}



const GeologicalTime* GeologicalSurfaceModeling::getGeologicalTime( unsigned int g ) const
{
    return _geologicalTimes[g];
}



void GeologicalSurfaceModeling::run( )
{
}
