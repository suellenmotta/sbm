#ifndef POLYLINE_H
#define POLYLINE_H

#include "Point3D.h"

/**
 * Just a typedef for std::vector<Point3D> in oder to define a polyline.
 * A polyline is a sequence of points that define a segment of a curve.
 * A closed polyline is a polygon. But in this project we will use just
 * open polylines.
 */
typedef std::vector<Point3D> Polyline;

#endif
