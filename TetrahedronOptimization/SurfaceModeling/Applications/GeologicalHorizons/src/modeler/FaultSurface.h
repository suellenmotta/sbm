#ifndef FAULT_SURFACE_H
#define FAULT_SURFACE_H

#include "Surface.h"
#include "Curve.h"
#include "../../../../../../libs/DataStructures/Topologicals/CornerTable/CornerTable.h"

/**
 * Class to represent a surface of a fault.
 */
class FaultSurface : public Surface
{
public:
    /**
     * Default constructor.
     */
    FaultSurface( );

    /**
     * Constructor that receives a set of curves.
     */
    FaultSurface( std::vector<Curve*>& f );

    /**
     * Constructor that receives a set of curves, a triangle mesh, and the time.
     */
    FaultSurface( std::vector<Curve*>& f, CornerTable* tmesh, unsigned int time );

    FaultSurface( std::vector<Curve*>& f, CornerTable* tmesh, std::vector<std::vector<CurveIntersection> >& i, unsigned int time );

    /**
     * Add a new curve.
     */
    void addFaultCurve( Curve* c );

    /**
     * Get a curve at the position c.
     */
    Curve* getFaultCurve( unsigned int c );

    /**
     * Get a curve at the position c.
     */
    const Curve* getFaultCurve( unsigned int c ) const;

    /**
     * Get all fault curves.
     */
    std::vector<Curve*>& getFaultCurves( );

    const std::vector<Curve*>& getFaultCurves( ) const;

    /**
     * Define a set of fault curves.
     */
    void setFaultCurves( std::vector<Curve*>& faultCurves );

    /**
     * Generate the surface mesh.
     */
    CornerTable* generateMesh( );

};
#endif
