#ifndef GEOLOGICAL_TIME_H
#define GEOLOGICAL_TIME_H

#include <vector>

#include "Horizon.h"
#include "Fault.h"

/**
 * Store information about one geological time; that is, all horizons and faces on that geological time.
 */
class GeologicalTime
{
private:
    /**
     * Store all horizons on geological time.
     */
    std::vector<Horizon*> _horizons;

    /**
     * Store all faults on geological time.
     */
    std::vector<Fault*> _faults;

    /**
     * The time index.
     */
    unsigned int _time;


public:
    /**
     * Constructor that receives the time index.
     */
    GeologicalTime( unsigned int t );
    
    /**
     * Destrutor
     */
    ~GeologicalTime( );

    /**
     * Constructor that receives the horizon list, the fault list that belongs to the geological time, and the time index.
     */
    GeologicalTime( std::vector<Horizon*>& h, std::vector<Fault*>& f, unsigned int t );

    /**
     * Add a fault to the vector.The faults can be added in any order.
     */
    void addFault( Fault* Fault );

    /**
     * Add an horizon to the vector.The horizons can be added in any order.
     */
    void addHorizon( Horizon* horizon );

    /**
     * Return the fault of the position i.
     */
    Fault* getFault( unsigned int i );

    /**
     * Return the fault of the position i.
     */
    const Fault* getFault( unsigned int i ) const;

    /**
     * Return the horizon of the position i.
     */
    Horizon* getHorizon( unsigned int i );

    /**
     * Return the horizon of the position i.
     */
    const Horizon* getHorizon( unsigned int i ) const;

    /**
     * Return the number of faults.
     */
    unsigned int getNumberFaults( ) const;

    /**
     * Return the number of horizons.
     */
    unsigned int getNumberHorizons( ) const;

    /**
     * Return the time index.
     */
    unsigned int getTime( ) const;

    /**
     * Model all surfaces on an specific geological . This is the most expensive method.
     */
    void run( );

};
#endif
