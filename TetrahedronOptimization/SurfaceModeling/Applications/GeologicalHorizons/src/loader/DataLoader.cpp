/* 
 * File:   DataLoader.cpp
 * Author: ncortez
 * 
 * Created on October 13, 2016, 1:56 PM
 */

#include <set>
#include "../modeler/FaultSurface.h"
#include "DataLoader.h"



DataLoader::DataLoader( )
{

}



void DataLoader::getCountInformation( std::vector< unsigned int >& hTimes,
                                      std::vector< unsigned int >& fTimes,
                                      unsigned int& maxTime )
{
    //Try to open the file.
    std::ifstream in( _curvesPath.c_str( ) );

    //Verify if the file could be opened.
    if (!in)
    {
        printf( "Fail opening the file %s\n", _curvesPath.c_str( ) );
        return;
    }

    //Read the file to find the number of horizons and faults.
    unsigned int section;
    std::string pathSurface;
    unsigned int time, surfaceType, surfaceIndex;

    //Horizon and fault numbers.
    std::set<unsigned int> horizonSet, faultSet;

    //Read the surface header in order to get the horizon and fault numbers.
    while (in >> section >> time >> surfaceType >> surfaceIndex)
    {
        std::getline( in, pathSurface );

        if (surfaceType == HORIZON)
        {
            horizonSet.insert( surfaceIndex );
        }
        else if (surfaceType == FAULT)
        {
            faultSet.insert( surfaceIndex );
        }
        else
        {

        }
    }

    //Allocate space to store all horizons and faluts.
    hTimes.resize( horizonSet.size( ), 0 );
    fTimes.resize( faultSet.size( ), 0 );

    //Return to begin.
    in.clear( );
    in.seekg( 0, in.beg );

    //Read the surface header in order to get the max time that each surface
    //appears.
    maxTime = 0;
    while (in >> section >> time >> surfaceType >> surfaceIndex)
    {
        std::getline( in, pathSurface );

        //Get max time.
        maxTime = std::max( maxTime, time );

        //Get max time for each surface.
        if (surfaceType == HORIZON)
        {
            hTimes[surfaceIndex] = std::max( hTimes[surfaceIndex], time );
        }
        else if (surfaceType == FAULT)
        {
            fTimes[surfaceIndex] = std::max( fTimes[surfaceIndex], time );
        }
        else
        {

        }
    }
}



GeologicalSurfaceModeling* DataLoader::load( const std::string& path, const std::string& meshPath )
{
    //Save the file's path.
    _curvesPath = path;
    _meshesPath = meshPath;

    std::vector< unsigned int > hTimes;
    std::vector< unsigned int > fTimes;
    unsigned int maxTime;

    //Get information about the number of horizons and faults and your times.
    getCountInformation( hTimes, fTimes, maxTime );

    //Build the GeologicalSurfaceModeling object.
    return generateGeologicalModel( hTimes, fTimes, maxTime );
}



void DataLoader::allocateAuxilliarVectors( std::vector< std::vector< HorizonSurface* > >& hSurfaces,
                                           std::vector< std::vector< FaultSurface* > >& fSurfaces,
                                           std::vector< Horizon* >& horizons,
                                           std::vector< Fault* >& faults,
                                           std::vector< GeologicalTime* >& geoTimes,
                                           const std::vector<unsigned int>& hTimes,
                                           const std::vector<unsigned int>& fTimes,
                                           const unsigned int& maxTime )
{
    //Get horizon and faults numbers.
    unsigned int numHorizons = hTimes.size( );
    unsigned int numFaults = fTimes.size( );

    hSurfaces.resize( numHorizons );
    fSurfaces.resize( numFaults );

    //Horizon and fault vectors.
    horizons.resize( numHorizons );
    faults.resize( numFaults );

    //Geological times.
    geoTimes.resize( maxTime + 1 );

    //Allocate space to storage the surfaces information.
    for (unsigned int i = 0; i < numHorizons; i++)
    {
        hSurfaces[i].resize( hTimes[i] + 1, 0 );
        for (unsigned int j = 0; j < hSurfaces[i].size( ); j++)
        {
            hSurfaces[i][j] = new HorizonSurface( );
        }
    }

    for (unsigned int i = 0; i < numFaults; i++)
    {
        fSurfaces[i].resize( fTimes[i] + 1 );
        for (unsigned int j = 0; j < fSurfaces[i].size( ); j++)
        {
            fSurfaces[i][j] = new FaultSurface( );
        }
    }

    for (unsigned int i = 0; i < horizons.size( ); i++)
    {
        horizons[i] = new Horizon( );
    }

    for (unsigned int i = 0; i < faults.size( ); i++)
    {
        faults[i] = new Fault( );
    }


    for (unsigned int i = 0; i < geoTimes.size( ); i++)
    {
        geoTimes[i] = new GeologicalTime( i );
    }
}



std::string DataLoader::getNameFromPath( std::string path )
{

    std::size_t begin = path.find_last_of( "/\\" );
    std::string name = path.substr( begin + 1 );
    std::size_t end = name.find_last_of( ".\\" );
    name = name.substr( 0, end );
    return name;
}



CornerTable* DataLoader::getCornerTableFromFile( std::string path, std::string &type )
{
    std::ifstream in( path.c_str( ) );
    if (in.fail( ))
    {
        printf( "Fail to open the file %s\n", path.c_str( ) );
        return 0;
    }

    //Read the number of vertices.
    unsigned int numVertices;
    in >> numVertices;

    //Read the number of triangles.
    unsigned int numTriangles;
    in >> numTriangles;

    //Read the number of triangles.
    in >> type;

    //allocate vectors
    std::vector< double > vertices( numVertices * 3 );
    std::vector< unsigned int > triangles( numTriangles * 3 );

    for (unsigned int i = 0; i < numVertices * 3; i = i + 3)
    {
        in >> vertices[i + 0] >> vertices[i + 1] >> vertices[i + 2];
    }

    for (unsigned int i = 0; i < numTriangles * 3; i = i + 3)
    {
        in >> triangles[i + 0] >> triangles[i + 1] >> triangles[i + 2];
    }

    CornerTable* cornerTable = new CornerTable( &triangles[0], &vertices[0], numTriangles, numVertices, 3 );

    return cornerTable;
}



GeologicalSurfaceModeling* DataLoader::generateGeologicalModel( const std::vector<unsigned int>& hTimes,
                                                                const std::vector<unsigned int>& fTimes,
                                                                const unsigned int& maxTime )
{
    //Two vectors that store a surface for each time of horizons and surfaces.
    std::vector< std::vector< HorizonSurface* > > hSurfaces;
    std::vector< std::vector< FaultSurface* > > fSurfaces;

    //Horizon and fault vectors.
    std::vector< Horizon* > horizons;
    std::vector< Fault* > faults;

    //Geological times.
    std::vector< GeologicalTime* > geoTimes;

    //Allocate space to storage the information.
    allocateAuxilliarVectors( hSurfaces, fSurfaces, horizons, faults, geoTimes,
                              hTimes, fTimes, maxTime );

    //Try to open the file.
    std::ifstream in( _curvesPath.c_str( ) );

    if (!in)
    {
        printf( "Fail opening the file %s\n", _curvesPath.c_str( ) );
        return 0;
    }

    //Read the files.
    unsigned int section;
    std::string surfacePath;
    unsigned int time, surfaceType, surfaceIndex;
    while (in >> section >> time >> surfaceType >> surfaceIndex)
    {
        std::getline( in, surfacePath );

        if (surfaceType == HORIZON || surfaceType == FAULT)
        {
            //Read a curve from the file.
            Curve* curve = readCurve( surfacePath.c_str( ) + 1 );

            if (surfaceType == HORIZON)
            {
                horizons[surfaceIndex]->setName( getNameFromPath( surfacePath ) );
                hSurfaces[surfaceIndex][time]->addHorizonCurve( curve );
            }
            else if (surfaceType == FAULT)
            {
                faults[surfaceIndex]->setName( getNameFromPath( surfacePath ) );
                fSurfaces[surfaceIndex][time]->addFaultCurve( curve );
            }
            else
            {
                printf( "Surface was not recognized\n" );
            }
        }
    }
    in.close( );


    //Fill the horizons.
    for (unsigned int h = 0; h < hSurfaces.size( ); h++)
    {
        for (unsigned int t = 0; t < hSurfaces[h].size( ); t++)
        {
            horizons[h]->addHorizonSurface( hSurfaces[h][t] );
        }
    }

    //Fill the faults.
    for (int f = 0; f < fSurfaces.size( ); f++)
    {
        for (int t = 0; t < fSurfaces[f].size( ); t++)
        {
            faults[f]->addFaultSurface( fSurfaces[f][t] );
        }
    }

    //Fill the geological times.
    for (int t = 0; t < maxTime + 1; t++)
    {
        for (int h = 0; h < horizons.size( ); h++)
        {
            if (t <= horizons[h]->getLastTime( ))
            {
                geoTimes[t]->addHorizon( horizons[h] );
            }
        }

        for (int f = 0; f < faults.size( ); f++)
        {
            if (t <= faults[f]->getLastTime( ))
            {
                geoTimes[t]->addFault( faults[f] );
            }
        }
    }

    //Build the geological surface modeling.
    GeologicalSurfaceModeling* gSurfModel = new GeologicalSurfaceModeling( horizons,
                                                                           faults,
                                                                           geoTimes );

    //Read the triangle meshes.
    readSurfaceMeshes( gSurfModel );

    return gSurfModel;
}



Curve* DataLoader::readCurve( const char* path )
{
    //Sum 1 to skip the space.
    std::ifstream curveFile( path );

    if (!curveFile)
    {
        printf( "Fail opening the file: %s\n", path );
        return 0;
    }

    //Auxiliary variable to read the file.
    Point3D point;
    unsigned int polylineIndex = 0, lastPolylineIndex = 0;

    //Creating polylines and adding to curve
    Curve* curve = new Curve( );
    Polyline polyline;
    while (curveFile >> point >> polylineIndex)
    {
        if (lastPolylineIndex == polylineIndex)
        {
            //add current point 
            polyline.push_back( point );
        }
        else
        {
            //add previous polyline
            curve->addPolyline( polyline );

            //clear last polyline to begin new one 
            polyline.clear( );

            //add current point
            polyline.push_back( point );

        }
        lastPolylineIndex = polylineIndex;
    }

    //Add last polyline
    curve->addPolyline( polyline );

    return curve;
}



void DataLoader::readSurfaceMeshes( GeologicalSurfaceModeling* gSurfModel )
{
    //Open the file.
    std::ifstream in( ( _meshesPath ).c_str( ) );
    if (!in)
    {
        printf( "Error opening the surface meshes!!\n" );
        return;
    }

    //Read the surfaces.
    std::string surfPath;
    while (in >> surfPath)
    {
        //Get the surface name form the surface path.
        std::string type, surfName = getNameFromPath( surfPath );

        //Read the surface.
        CornerTable* cornerTable = getCornerTableFromFile( surfPath, type );

        //Set the surface on the correct feature.
        if (type == "horizon")
        {
            for (int i = 0; i < gSurfModel->getNumberHorizons( ); i++)
            {
                if (gSurfModel->getHorizon( i )->getName( ) == surfName)
                {
                    gSurfModel->getHorizon( i )->getHorizonSurface( 0 )->setTriangleMesh( cornerTable );
                    break;
                }
            }
        }
        else if (type == "fault")
        {
            for (int i = 0; i < gSurfModel->getNumberFaults( ); i++)
            {
                if (gSurfModel->getFault( i )->getName( ) == surfName)
                {
                    gSurfModel->getFault( i )->getFaultSurface( 0 )->setTriangleMesh( cornerTable );
                    break;
                }
            }
        }
    }
}



DataLoader::~DataLoader( )
{
}

