/* 
 * File:   ModelingData.cpp
 * Author: jcoelho
 * 
 * Created on April 8, 2016, 2:45 PM
 */

#include "ModelingData.h"
#include "TimeData.h"


namespace SurfaceModeling
{



    ModelingData::ModelingData( )
    {

    }



    void ModelingData::addTimeData( TimeData* data )
    {
        _timeData.push_back( data );
    }



    const TimeData* ModelingData::getTimeData( const unsigned int t ) const
    {
        return t < _timeData.size( ) ? _timeData[t] : 0;
    }



    ModelingData::~ModelingData( )
    {
        for (unsigned int i = 0; i < _timeData.size( ); i++)
        {
            delete _timeData[i];
        }
    }

};