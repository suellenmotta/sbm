/* 
 * File:   TiimeData.cpp
 * Author: jcoelho
 * 
 * Created on April 9, 2016, 7:46 PM
 */

#include "TimeData.h"
#include "SurfaceData.h"
#include "SectionData.h"


namespace SurfaceModeling
{



    TimeData::TimeData( )
    {
    }



    void TimeData::addSectionData( SectionData* section )
    {
        _sections.push_back( section );
    }



    TimeData::~TimeData( )
    {
        for (unsigned int i = 0; i < _sections.size( ); i++)
        {
            delete _sections[i];
        }
    }
}
