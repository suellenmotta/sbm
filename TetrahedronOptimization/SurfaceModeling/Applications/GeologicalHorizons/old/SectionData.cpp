/* 
 * File:   Section.cpp
 * Author: jcoelho
 * 
 * Created on April 6, 2016, 8:19 PM
 */

#include "SectionData.h"
#include "SurfaceData.h"

namespace SurfaceModeling
{



    SectionData::SectionData( const unsigned int sectionId )
    {
        _sectionId = sectionId;
    }



    void SectionData::addFaultData( SurfaceData* faultData )
    {
        _faults.push_back( faultData );
    }



    void SectionData::addHorizonData( SurfaceData* horizonData )
    {
        _horizons.push_back( horizonData );
    }



    SectionData::~SectionData( )
    {
        for (unsigned int i = 0; i < _horizons.size(); i++)
        {
            delete _horizons[i];
        }
        
        for (unsigned int i = 0; i < _faults.size(); i++)
        {
            delete _faults[i];
        }
    }

}