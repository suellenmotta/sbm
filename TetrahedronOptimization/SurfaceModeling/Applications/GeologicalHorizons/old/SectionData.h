/* 
 * File:   Section.h
 * Author: jcoelho
 *
 * Created on April 6, 2016, 8:19 PM
 */

#ifndef SECTION_H
#define	SECTION_H
#include <vector>

namespace SurfaceModeling
{
    class SurfaceData;
    
    class SectionData
    {
    public:
        /**
         * Constructor that receives the section id.
         * @param sectionId - section id.
         */
        SectionData( const unsigned int sectionId );

        /**
         * Add a new horizon data at the section.
         * @param horizonData - new horizon data to be added.
         */
        void addHorizonData( SurfaceData* horizonData );

        /**
         * Add a new horizon data at the section.
         * @param faultData - new fault data to be added.
         */
        void addFaultData( SurfaceData* faultData );

        /**
         * Destructor.
         */
        virtual ~SectionData( );
    private:

        /**
         * The set of horizons that are at the same time.
         */
        std::vector<SurfaceData*> _horizons;

        /**
         * The set of faults that are at the same time.
         */
        std::vector<SurfaceData*> _faults;

        /**
         * Id of the section;
         */
        unsigned int _sectionId;
    };
}
#endif	/* SECTION_H */

