/* 
 * File:   Surface.h
 * Author: jcoelho
 *
 * Created on April 6, 2016, 8:23 PM
 */

#ifndef SURFACE_H
#define	SURFACE_H
#include <vector>
#include "../../../../libs/geom/Objects/Vector/Vector3D.h"

namespace SurfaceModeling
{
    typedef std::vector<Point3Dd> Polyline;

    class SurfaceData
    {
    public:
        /**
         * Surface construtor that receis the surfaces' id and section's id.
         * @param surfaceId - surfaces' id.
         * @param sectionId - section where this surface are from.
         */
        SurfaceData( const unsigned int surfaceId, const unsigned int sectionId );

        /**
         * Add a new polyline to surface data.
         * @param poly - new polyline to be added.
         */
        void addPolyline( const Polyline& poly );

        /**
         * Desctructor.
         */
        virtual ~SurfaceData( );
    private:
        /**
         * The set of polylines to form a surface at a section.
         */
        std::vector<Polyline> _surface;

        /**
         * Id of the surface.
         */
        unsigned int _surfaceId;

        /**
         * Id of the section that the surface belongs.
         */
        unsigned int _sectionId;
    };
};
#endif	/* SURFACE_H */

