/* 
 * File:   LoaderData.cpp
 * Author: jcoelho
 * 
 * Created on April 9, 2016, 8:59 PM
 */

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <fstream>

#include "SurfaceData.h"
#include "LoaderData.h"
#include "ModelingData.h"
#include "SectionData.h"
#include "TimeData.h"

namespace SurfaceModeling
{



    LoaderData::LoaderData( )
    {
    }



    ModelingData* LoaderData::loadData( const std::string& path )
    {
        //Allocate an object to store the data.
        ModelingData *data = new ModelingData( );

        //Try to open the file.
        std::ifstream in( path.c_str( ) );

        if (!in)
        {
            printf( "Fail opening the file %s\n", path.c_str( ) );
            return data;
        }

        //Vector to store pre-loader information.
        std::vector<PreLoaderData> preLoader;
        PreLoaderData line;

        unsigned int numberOfHorizons = 0, numberOfFaults = 0, numberSections = 0, times = 0;

        while (in >> line._section >> line._time >> line._type >> line._surfaceIndex)
        {
            std::getline( in, line._path );
            line._path = std::string( line._path.c_str( ) + 1 );

            preLoader.push_back( line );

            numberSections = std::max( numberSections, line._section );
            times = std::max( times, line._time );

            if (line._type == HORIZON)
            {
                numberOfHorizons = std::max( numberOfHorizons, line._surfaceIndex );
            }
            else if (line._type == FAULT)
            {
                numberOfFaults = std::max( numberOfFaults, line._surfaceIndex );
            }
        }
        in.close( );

        //Get the number of horizons and faults;
        numberOfFaults++;
        numberOfHorizons++;
        numberSections++;
        times++;

        std::sort( preLoader.begin( ), preLoader.end( ) );
        unsigned int p = 0;
        for (unsigned int i = 0; i < times; i++)
        {
            //Allocate the sections.
            std::vector<SectionData*> sections;
            for (unsigned int j = 0; j < numberSections; j++)
            {
                sections.push_back( new SectionData( j ) );
            }

            while (p < preLoader.size( ) && preLoader[p]._time == i)
            {
                SurfaceData* surface = loadSurface( preLoader[p]._path,
                                                    preLoader[p]._surfaceIndex,
                                                    preLoader[p]._section );
                if (preLoader[p]._type == HORIZON)
                {
                    sections[preLoader[p]._section]->addHorizonData( surface );
                }
                else if (preLoader[p]._type == FAULT)
                {
                    sections[preLoader[p]._section]->addFaultData( surface );
                }
                p++;
            }

            //Allocate new time data information.
            TimeData* time = new TimeData( );
            for (unsigned int j = 0; j < sections.size( ); j++)
            {
                time->addSectionData( sections[j] );
            }

            //Add new time data at modeling data.
            data->addTimeData( time );
        }
        //Return the loaded data
        return data;
    }



    SurfaceData* LoaderData::loadSurface( const std::string& path,
                                          const unsigned int surfaceId,
                                          const unsigned int sectionId )
    {
        //Allocate surface data.
        SurfaceData *surface = new SurfaceData( surfaceId, sectionId );

        //Open the file.
        std::ifstream in( path.c_str( ) );
        if (!in)
        {
            printf( "Fail opening the file%s.\n", path.c_str( ) );
            return surface;
        }

        //Read the file.
        Point3Dd point;
        Polyline poly;
        int index = 0, oldIndex = 0;
        while (in >> point.x >> point.y >> point.z >> index)
        {
            if (oldIndex != index)
            {
                oldIndex = index;
                surface->addPolyline( poly );
                poly.clear( );
            }
            poly.push_back( point );
        }
        surface->addPolyline( poly );

        //Return the surface data.
        return surface;
    }



    LoaderData::~LoaderData( )
    {
    }

};