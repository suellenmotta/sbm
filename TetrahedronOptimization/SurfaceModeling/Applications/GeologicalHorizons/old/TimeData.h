/* 
 * File:   TiimeData.h
 * Author: jcoelho
 *
 * Created on April 9, 2016, 7:46 PM
 */

#ifndef TIIMEDATA_H
#define	TIIMEDATA_H
#include <vector>


namespace SurfaceModeling
{
    class SurfaceData;
    class SectionData;

    class TimeData
    {
    public:
        /**
         * Default constructor.
         */
        TimeData( );

        /**
         * Add a new section data.
         * @param section - new section data.
         */
        void addSectionData( SectionData* section );

        /**
         * Destructor.
         */
        virtual ~TimeData( );
    private:

        /**
         * The set of sections.
         */
        std::vector<SectionData*> _sections;
    };
};

#endif	/* TIIMEDATA_H */

