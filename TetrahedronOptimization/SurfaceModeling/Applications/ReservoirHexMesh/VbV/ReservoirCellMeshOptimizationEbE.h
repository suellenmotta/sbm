/* 
 * File:   ReservoirCellMeshOptimization.h
 * Author: jcoelho
 *
 * Created on July 26, 2016, 1:45 PM
 */

#ifndef RESERVOIRCELLMESHOPTIMIZATIONEBE_H
#define RESERVOIRCELLMESHOPTIMIZATIONEBE_H
#include "../Point3D.h"
#include <vector>

//Forward declaration.
template <typename TYPE> class DsGrid3D;


namespace MeshModeling
{

    class ReservoirCellMeshOptimizationEbE
    {
    public:
        /**
         * Default constructor.
         */
        ReservoirCellMeshOptimizationEbE( );

        /**
         * Compute the mesh geometry by an optimization process that minimize the
         * discrete Laplacian norm operator. This is equivalent to maximize the
         * elements regularity or/and Jacobian. ATTENTION: the grid geometry will
         * be changed by the optimization method.
         * @param grid - the cell grid with the geometry and topology.
         * @param activeCells - the active cells on grid.
         * @param fixedBoundaryCells - the number of cell slices must be fixed. The
         * minimum is 1.
         */
        void computeReservoirCellMesh( DsGrid3D<Point3D>& grid,
                                       const std::vector<bool>& activeCells,
                                       const unsigned int fixedBoundaryCells = 1 );

        /**
         * Get the current tolerance.
         * @return - the current tolerance.
         */
        FPType getEPS( ) const;

        /**
         * Get the to the number of sub-iterations. It was used as an upper bound to
         * solve the linear system.
         * @param maxSubIter - new upper bound to the number of sub-iterations.
         */
        unsigned int getMaxSubIterations( ) const;

        /**
         * Get the current maximum iterations.
         * @return - the current maximum iterations.
         */
        unsigned int getMaxIterations( ) const;

        /**
         * Get the number of iterations used to solve the last model.
         * @return - number of iterations used to solve the last model.
         */
        unsigned int getNumberIterations( ) const;

        /**
         * Get the number of threads that must be used in the computations.
         * @return - number of threads that must be used in the computations.
         */
        unsigned int getNumberfOfThreads( ) const;

        /**
         * Get the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @return - steps number.
         */
        unsigned int getStepsNumber( ) const;

        /**
         * Destructor.
         */
        virtual ~ReservoirCellMeshOptimizationEbE( );

        /**
         * Set a callback function to be called after 'stepsNumber' iterations, i.e,
         * after each 'stepsNumber' iterations this function is called with gradient
         * norm of the current solution. If stepsNumber have no set, this function
         * will be called after each iteration. The current solution can be accessed
         * in Grid3D, once the current solution replace the the grid geometry.
         * @param displayInformation - pointer to a function that must be called
         * after a new solution is found. This function must return false to
         * interrupt the solve or true to continue.
         */
        void setDisplayInformationCallback( bool (*displayInformation )( const FPType gnorm ) );

        /**
         * Define a new tolerance to be used by solver.
         * @param eps - new tolerance to be used by solver.
         */
        void setEPS( const FPType eps );

        /**
         * Define a new upper bound to the number of sub-iterations. It will be used
         * as an upper bound to solve the linear system.
         * @param maxSubIter - new upper bound to the number of sub-iterations.
         */
        void setMaxSubIterations( const unsigned int maxSubIter );

        /**
         * Define a new upper bound to the number of iterations.
         * @param maxIter - new upper bound to the number of iterations.
         */
        void setMaxIterations( const unsigned int maxIter );

        /**
         * Define the number of threads that must be used in the computations.
         * @param numThreads - number of threads that must be used in the computations.
         */
        void setNumberOfThreads( const unsigned int numThreads );

        /**
         * Set if the solver must show the log or not.
         * @param log - true if the solve need to show the log and false otherwise.
         */
        void showLog( const bool log );

        /**
         * Set the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @param sn - steps number.
         */
        void setStepsNumber( const unsigned int sn );
    private:
        /**
         * Compute the new index for points. This is equivalent to consider
         * the grid without the fixed points.
         * @param activePoints - vector that stores a boolean to know if each
         * points is fixed or not.
         * @param reindexPoints - new points index.
         */
        void computeReindexPointsVector( const std::vector<bool> &activePoints,
                                         std::vector<int>& reindexPoints ) const;

        /**
         * Compute b vector.
         * @param dimension - dimension to compute the values: 0-x, 1-y, 2-z.
         * @param grid - grid representation.
         * @param reindexPoints - map the grid index to variables index.
         */
        void computeBVector( const unsigned int dimension,
                             const DsGrid3D<Point3D>& grid,
                             const std::vector<int>& reindexPoints );

        /**
         * Define which points will have fixed positions by computing a re-index
         * vector. This vector maps from grid points to variables space.
         * @param grid - grid.
         * @param activeCells - vector that defines if a cell is active or not.
         * @param fixedBoundaryCells - the number of cell slices must be fixed.
         * @param reindexPoints - maps from grid points to variables space. If the
         * re-index is equal to -1, it means that the point is known.
         * @return - the number of active points.
         */
        unsigned int preprocessFixedPoints( const DsGrid3D<Point3D>& grid,
                                            const std::vector<bool>& activeCells,
                                            std::vector<int>& reindexPoints )const;
    private:

        /**
         * Compute the initial residual.
         * @param grid - grid representation.
         * @param index - vector that stores the columns that have nonzero
         * elements.
         * @param value - matrix values.
         * @param x - vector to store the initial solution.
         * @param b - vector to store right-hand side of the system.
         * @param reps - residual tolerance.
         * @return - squared residual norm.
         */
        /**
         * Compute the initial residual.
         * @param grid - grid representation.
         * @param dimension - dimension that the initial residual must be computed.
         * @param reindexPoints - maps from grid points to variables space. If the
         * re-index is equal to -1, it means that the point is known.
         * @param r - vector to store the residual.
         * @return - the squared gradient norm.
         */
        FPType computeInitialResidual( const DsGrid3D<Point3D>& grid,
                                       const unsigned int dimension,
                                       const std::vector<int>& reindexPoints,
                                       std::vector<FPType>& r );

        /**
         * Compute the product Ap and store it in q.
         * @param grid - grid representation.
         * @param dimension - dimension to compute the values: 0-x, 1-y, 2-z.
         * @param reindexPoints - map the grid index to variables index.
         * @param p - vector that will be multiplied by A.
         * @param q - vector to store the multiply result.
         */
        void matrixVec( const DsGrid3D<Point3D>& grid,
                        const unsigned int dimension,
                        const std::vector<int>& reindexPoints,
                        const std::vector<FPType>& p,
                        std::vector<FPType>& q );


        /**
         * Perform the pre-conditioned conjugate gradient algorithm.
         * @param grid - grid representation.
         * @param dimension - dimension to compute the values: 0-x, 1-y, 2-z.
         * @param reindexPoints - map the grid index to variables index.
         * @param rnorm2 - squared residual norm.
         * @return - squared gradient norm.
         */
        FPType PCG( const DsGrid3D<Point3D>& grid,
                    const unsigned int dimension,
                    const std::vector<int>& reindexPoints,
                    FPType rnorm2 );

        /**
         * Update current solution doing x[i] = x[i] + rho * p[i] / ( 84.0 * dotPQ ).
         * @param grid - grid representation.
         * @param dimension - dimension to compute the values: 0-x, 1-y, 2-z.
         * @param reindexPoints - map the grid index to variables index.
         * @param p - p vector.
         * @param rho - rho
         * @param dotPQ - dot product between vectors P and Q.
         */
        void updateSolution( const DsGrid3D<Point3D>& grid,
                             const unsigned int dimension,
                             const std::vector<int>& reindexPoints,
                             const std::vector<FPType>& p,
                             const FPType rho,
                             const FPType dotPQ ) const;
    private:

        /**
         * Function to be called after each _stepsNumber iterations. This function
         * must return false to interrupt the solve or true to continue.
         */
        bool (*_displayInformation )( const FPType gnorm );

    private:
        /**
         * Auxiliary vector to accumulate sums.
         */
        std::vector<FPType> _sum;

        /**
         * Auxiliary vector to compute residual.
         */
        std::vector<FPType> _r;

        /**
         * Auxiliary vector to perform the PCG algorithm.
         */
        std::vector<FPType> _p;

        /**
         * Auxiliary vector to perform the PCG algorithm.
         */
        std::vector<FPType> _q;

        /**
         * Number of fixed boundary cells.
         */
        unsigned int _fixedBoundaryCells;
    private:
        /**
         * Tolerance that will be used as stopped condition by solver.
         */
        FPType _eps;

        /**
         * Number of iterations.
         */
        unsigned int _iterations;

        /**
         * The maximum sub-iterations number to solve the linear system.
         */
        unsigned int _maxSubIterations;

        /**
         * The maximum iterations number to be performed by the algorithm.
         */
        unsigned int _maxIterations;

        /**
         * The number of threads;
         */
        unsigned int _numberOfThreads;

        /**
         * Flag that determines if the solver must show messages or not.
         */
        bool _showLog;

        /**
         * Number of iterations to show the log information.
         */
        unsigned int _stepsNumber;
    };
}

#endif /* RESERVOIRCELLMESHOPTIMIZATION_H */

