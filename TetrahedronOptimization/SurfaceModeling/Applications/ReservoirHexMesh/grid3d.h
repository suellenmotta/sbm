// grid3d.h
// rodesp@tecgraf.puc-rio.br
// Mar 2015

#ifndef DS_GRID3D_H
#define DS_GRID3D_H

#include <new>
#include <algorithm>
#include <assert.h>

/**
 * A container class for arranging data as a three-dimensional grid.
 */
template <typename TYPE>
class DsGrid3D
{
public:

    /**
     * Creates a grid of initial size 'ni * nj * nk'. 
     */
    DsGrid3D( int ni = 0, int nj = 0, int nk = 0 )
    {
        m_ni = ni;
        m_nj = nj;
        m_nk = nk;
        m_size = ni * nj * nk;

        if (ni * nj * nk > 0)
            m_data = new TYPE[ni * nj * nk];
        else
            m_data = NULL;
    }

    ~DsGrid3D( )
    {
        delete[] m_data;
    }

    bool Resize( int ni, int nj, int nk )
    {

        if (ni == m_ni && nj == m_nj && nk == m_nk)
            return true;

        if (ni * nj * nk == 0)
        {
            delete[] m_data;
            m_data = NULL;
        }
        else
        {
            TYPE *new_data = new (std::nothrow ) TYPE[ni * nj * nk];
            if (new_data == NULL)
                return false;

            for (int k = 0; k < nk; k++)
            {
                for (int j = 0; j < nj; j++)
                {
                    for (int i = 0; i < ni; i++)
                    {
                        if (i < m_ni && j < m_nj && k < m_nk)
                            new_data[k * ni * nj + j * ni + i] = m_data[k * m_ni * m_nj + j * m_ni + i];
                    }
                }
            }
            delete[] m_data;
            m_data = new_data;
        }

        m_ni = ni;
        m_nj = nj;
        m_nk = nk;
        m_size = ni * nj * nk;
        return true;
    }

    bool EraseSliceK( int ki, int kf )
    {
        if (kf < ki)
            return false;

        int nSlices = kf - ki - 1;
        if (nSlices <= 0)
            return false;

        //Compute new dimensions.        
        int ni = m_ni, nj = m_nj, nk = m_nk - nSlices;


        if (ni * nj * nk == 0)
        {
            delete[] m_data;
            m_data = NULL;
        }
        else
        {
            TYPE *new_data = new (std::nothrow ) TYPE[ni * nj * nk];
            if (new_data == NULL)
                return false;

            for (int k = 0, newK = 0; k < m_nk; k++)
            {
                //Do not copy any information in this interval.
                if (k > ki && k < kf)
                    continue;

                for (int j = 0; j < m_nj; j++)
                {
                    for (int i = 0; i < m_ni; i++)
                    {
                        new_data[newK * ni * nj + j * ni + i] = m_data[k * m_ni * m_nj + j * m_ni + i];
                    }
                }
                newK++;
            }
            delete[] m_data;
            m_data = new_data;
        }

        m_ni = ni;
        m_nj = nj;
        m_nk = nk;
        m_size = ni * nj * nk;
        return true;
    }

    bool EraseSliceJ( int ji, int jf )
    {
        if (jf < ji)
            return false;

        int nSlices = jf - ji - 1;
        if (nSlices <= 0)
            return false;

        //Compute new dimensions.        
        int ni = m_ni, nj = m_nj - nSlices, nk = m_nk;


        if (ni * nj * nk == 0)
        {
            delete[] m_data;
            m_data = NULL;
        }
        else
        {
            TYPE *new_data = new (std::nothrow ) TYPE[ni * nj * nk];
            if (new_data == NULL)
                return false;

            for (int k = 0; k < m_nk; k++)
            {
                for (int j = 0, newJ = 0; j < m_nj; j++)
                {
                    //Do not copy any information in this interval.
                    if (j > ji && j < jf)
                        continue;

                    for (int i = 0; i < m_ni; i++)
                    {
                        new_data[k * ni * nj + newJ * ni + i] = m_data[k * m_ni * m_nj + j * m_ni + i];
                    }
                    newJ++;
                }
            }
            delete[] m_data;
            m_data = new_data;
        }

        m_ni = ni;
        m_nj = nj;
        m_nk = nk;
        m_size = ni * nj * nk;
        return true;
    }

    bool EraseSliceI( int ii, int ifin )
    {
        if (ifin < ii)
            return false;

        int nSlices = ifin - ii - 1;
        if (nSlices <= 0)
            return false;
        //Compute new dimensions.        
        int ni = m_ni - nSlices, nj = m_nj, nk = m_nk;


        if (ni * nj * nk == 0)
        {
            delete[] m_data;
            m_data = NULL;
        }
        else
        {
            TYPE *new_data = new (std::nothrow ) TYPE[ni * nj * nk];
            if (new_data == NULL)
                return false;

            for (int k = 0; k < m_nk; k++)
            {
                for (int j = 0; j < m_nj; j++)
                {
                    for (int i = 0, newI = 0; i < m_ni; i++)
                    {
                        //Do not copy any information in this interval.
                        if (i > ii && i < ifin)
                            continue;

                        new_data[k * ni * nj + j * ni + newI] = m_data[k * m_ni * m_nj + j * m_ni + i];
                        newI++;
                    }

                }
            }
            delete[] m_data;
            m_data = new_data;
        }

        m_ni = ni;
        m_nj = nj;
        m_nk = nk;
        m_size = ni * nj * nk;
        return true;
    }

    /**
     * Sets all grid locations to 'value'
     */
    void SetAll( const TYPE& value )
    {
        for (int i = 0; i < m_size; i++)
            m_data[i] = value;
    }

    /**
     * Sets the value at the location with the given linear index.
     */
    void Set( int index, TYPE& t ) const
    {
        assert( ValidIndex( index ) );
        m_data[index] = t;
    }

    void SetConst( int index, TYPE t )
    {
        assert( ValidIndex( index ) );
        m_data[index] = t;
    }

    /**
     * Sets the value at the location with indices 'i,j,k'.
     */
    void Set( int i, int j, int k, TYPE& t )
    {
        assert( ValidIndex( GetIndex( i, j, k ) ) );
        m_data[GetIndex( i, j, k )] = t;
    }

    void SetConst( int i, int j, int k, TYPE t )
    {
        assert( ValidIndex( GetIndex( i, j, k ) ) );
        m_data[GetIndex( i, j, k )] = t;
    }

    TYPE Get( int i, int j, int k ) const
    {
        assert( ValidIndex( GetIndex( i, j, k ) ) );
        return m_data[GetIndex( i, j, k )];
    }

    TYPE Get( int index ) const
    {
        assert( ValidIndex( index ) );
        return m_data[index];
    }

    TYPE& GetRef( int i, int j, int k ) const
    {
        assert( ValidIndex( GetIndex( i, j, k ) ) );
        return m_data[GetIndex( i, j, k )];
    }

    TYPE& GetRef( int index ) const
    {
        assert( ValidIndex( index ) );
        return m_data[index];
    }

    /**
     * Copies data from another grid to location (i0, j0, k0) of this grid.
     */
    void SetSubGrid( const DsGrid3D<TYPE>& subgrid,
                     int i0, int j0, int k0 )
    {
        int ni = subgrid.Ni( );
        int nj = subgrid.Nj( );
        int nk = subgrid.Nk( );

        assert( ValidIndex( GetIndex( i0, j0, k0 ) ) );
        assert( ValidIndex( GetIndex( i0 + ni - 1, j0 + nj - 1, k0 + nk - 1 ) ) );

        for (int k = 0; k < nk; k++)
            for (int j = 0; j < nj; j++)
                for (int i = 0; i < ni; i++)
                    SetConst( i0 + i, j0 + j, k0 + k, subgrid.Get( i, j, k ) );
    }

    /**
     * Copies data at location (i0, j0, k0) of this grid to another grid. 
     */
    void GetSubGrid( DsGrid3D<TYPE>& subgrid,
                     int i0, int j0, int k0 ) const
    {
        int ni = subgrid.Ni( );
        int nj = subgrid.Nj( );
        int nk = subgrid.Nk( );

        assert( ValidIndex( GetIndex( i0, j0, k0 ) ) );
        assert( ValidIndex( GetIndex( i0 + ni - 1, j0 + nj - 1, k0 + nk - 1 ) ) );

        for (int k = 0; k < nk; k++)
            for (int j = 0; j < nj; j++)
                for (int i = 0; i < ni; i++)
                    subgrid.SetConst( i, j, k, Get( i0 + i, j0 + j, k0 + k ) );
    }

    int GetIndex( int i, int j, int k ) const
    {
        return m_ni * m_nj * k + m_ni * j + i;
    }

    int Ni( ) const
    {
        return m_ni;
    }

    int Nj( ) const
    {
        return m_nj;
    }

    int Nk( ) const
    {
        return m_nk;
    }

    int Size( ) const
    {
        return m_size;
    }

    bool ValidIndex( int i ) const
    {
        return 0 <= i && i < Size( );
    }

    /**
     * Returns whether the indices 'i,j,k' are valid with respect to this grid.
     */
    bool ValidIndices( int i, int j, int k ) const
    {
        return 0 <= i && i < Ni( ) &&
            0 <= j && j < Nj( ) &&
            0 <= k && k < Nk( );
    }

    DsGrid3D<TYPE>& operator=( const DsGrid3D<TYPE>& o )
    {
        if (m_data)
            delete [] m_data;

        m_ni = o.m_ni;
        m_nj = o.m_nj;
        m_nk = o.m_nk;
        m_size = o.m_size;

        if (o.m_data)
        {
            m_data = new TYPE[m_size];
            memcpy( m_data, o.m_data, m_size * sizeof (TYPE ) );
        }
    }

    DsGrid3D( const DsGrid3D& o )
    {
        m_ni = o.m_ni;
        m_nj = o.m_nj;
        m_nk = o.m_nk;
        m_size = o.m_size;

        if (o.m_data)
        {
            m_data = new TYPE[m_size];
            memcpy( m_data, o.m_data, m_size * sizeof (TYPE ) );
        }
    }

private:
    TYPE* m_data;
    int m_ni;
    int m_nj;
    int m_nk;
    int m_size;
};

#endif
