/* 
 * File:   ReservoirConformalMeshEbE.cpp
 * Author: jcoelho
 * 
 * Created on April 25, 2017, 2:33 PM
 */

#include "ReservoirConformalMeshEbE.h"
#include "VertexVertexSolver.h"
#include "LISSolver.h"
#include <omp.h>
#include <fstream>
#include <queue>

int dx[8][3] = {
    {0, 0, 0 },
    {1, 0, 0 },
    {0, 1, 0 },
    {1, 1, 0 },
    {0, 0, 1 },
    {1, 0, 1 },
    {0, 1, 1 },
    {1, 1, 1 }
};



static bool compare( const ReservoirHorizon* h1, const ReservoirHorizon* h2 )
{
    return h1->getAverageZ( ) < h2->getAverageZ( );
}

//PUBLIC METHODS.

//double facScal = 0.030;
//double facScal = 0.100;
//double facScal = 0.150;
//double facScal = 0.200;
//double facScal = 0.238;
//double facScal = 0.3;
//double facScal = 0.300;
//double facScal = 0.7;



ReservoirConformalMeshEbE::ReservoirConformalMeshEbE( )
{
    _distanceI = _distanceJ = 0.0;
    _numCellsRegularI = _numCellsRegularJ = 0;
    _numCellsI = _numCellsJ = 0;
    _inputMesh = 0;
    _fixedBoundaryCells = 3;
    _reservoirK = 0;
    _radiusFromFault = 500;
    _numThreads = std::max( omp_get_num_procs( ) - 2, 1 );
    _eps = 1e-4;
}



void ReservoirConformalMeshEbE::addHorizon( ReservoirHorizon* horizon )
{
    _horizons.push_back( horizon );
}



int ReservoirConformalMeshEbE::getNumCells( ) const
{
    return (_outputGrid.Ni( ) - 1 ) * ( _outputGrid.Nj( ) - 1 ) * ( _outputGrid.Nk( ) - 1 );
}



int ReservoirConformalMeshEbE::getNumPoints( ) const
{
    return _outputGrid.Size( );
}



const DsGrid3D< Point3D >& ReservoirConformalMeshEbE::getGrid( ) const
{
    return _outputGrid;
}



void ReservoirConformalMeshEbE::generateInitialMesh( )
{
    //Preprocess the data.
    preprocess( );

    //Check by inconsistency.
    if (!checkData( ))
        return;

    //Allocate memory to output and model variables.
    allocateMemory( );

    //Compute the initial geometry.
    computeInitialGeometry( );

    //Check the cells that must be fixed.
    preprocessActiveCells( );
}



void ReservoirConformalMeshEbE::optimizeMesh( )
{
    computeHorizonsSlices( );

    //Set hard position.
    setHardPointsPosition( );

    //Build and optimize the model.
    optimize( );

    //Posprocess the data.
    posprocess( );
}



void ReservoirConformalMeshEbE::setEPS( const FPType eps )
{
    _eps = eps;
}



void ReservoirConformalMeshEbE::setIIncrement( const double i )
{
    _distanceI = i;
    double factor = _inputMesh->getWidth( ) / i;
    _numCellsRegularI = ( int ) ( _facScal * _inputMesh->getGrid( ).Ni( ) / factor );
}



void ReservoirConformalMeshEbE::setJIncrement( const double j )
{
    _distanceJ = j;
    double factor = _inputMesh->getLengh( ) / j;
    _numCellsRegularJ = ( int ) ( _facScal * _inputMesh->getGrid( ).Nj( ) / factor );
}



void ReservoirConformalMeshEbE::setNumCellsI( const unsigned int i )
{
    _numCellsI = i;
}



void ReservoirConformalMeshEbE::setNumCellsJ( const unsigned int j )
{
    _numCellsJ = j;
}



void ReservoirConformalMeshEbE::setNumCellsAmongSurfaces( const std::vector<int>& slices )
{
    _slices = slices;
}



void ReservoirConformalMeshEbE::setNumThreads( const int numThreads )
{
    _numThreads = std::min( numThreads, omp_get_num_procs( ) - 2 );
}



void ReservoirConformalMeshEbE::setPenalization( const FPType pen )
{
    _pen = pen;
}



void ReservoirConformalMeshEbE::setReservoirMesh( ReservoirMesh* mesh )
{
    _inputMesh = mesh;

    //Check orientations.
    for (int i = 0; i < 8; i++)
    {
        if (_inputMesh->invertedX( ))
        {
            dx[i][0] = !dx[i][0];
        }

        if (_inputMesh->invertedY( ))
        {
            dx[i][0] = !dx[i][1];
        }

        if (_inputMesh->invertedZ( ))
        {
            dx[i][2] = !dx[i][2];
        }

    }
}



ReservoirMesh* ReservoirConformalMeshEbE::getReservoirInputMesh( ) const
{
    return _inputMesh;
}



bool ReservoirConformalMeshEbE::writeNeutralFileForInput( const std::string& path )
{
    //Get the number of horizons on model.
    const unsigned int n = _horizons.size( );

    //Allocate vector to re-index the mesh.
    std::vector<unsigned int> horizonsNodes( n, 0 );

    //Compute the number of nodes and elements on model.
    unsigned int totalHorizonNodes = 0, totalHorizonElements = 0;
    for (unsigned int i = 0; i < n; i++)
    {
        horizonsNodes[i] = _horizons[i]->getNumberNodes( );
        totalHorizonNodes += horizonsNodes[i];

        totalHorizonElements += _horizons[i]->getNumberElements( );
    }

    horizonsNodes[n - 1] = totalHorizonNodes - horizonsNodes[n - 1];
    for (int i = ( int ) n - 2; i >= 0; i--)
    {
        horizonsNodes[i] = horizonsNodes[i + 1] - horizonsNodes[i];
    }

    //Get the number of nodes on reservoir.
    unsigned int reservoirNodes = _inputMesh->getNumberOfNodes( );

    //Get the number of elements on reservoir.
    int reservoidElements = _inputMesh->getNumberOfElements( );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << totalHorizonNodes + reservoirNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << totalHorizonNodes + reservoirNodes << std::endl << std::endl;
    int idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const std::vector<Point3D>& coord = _horizons[i]->getCoordinates( );
        for (unsigned int i = 0; i < coord.size( ); i++)
        {
            out << idx << " " << coord[i] << std::endl;
            idx++;
        }
    }

    const std::vector<Point3D>& reserNodes = _inputMesh->getReservoirNodes( );
    for (unsigned int i = 0; i < reserNodes.size( ); i++)
    {
        out << idx << " " << reserNodes[i] << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << totalHorizonElements + reservoidElements << std::endl << std::endl;

    out << "%ELEMENT.T3" << std::endl;
    out << totalHorizonElements << std::endl << std::endl;
    idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const CornerType* tri = _horizons[i]->getTriangleList( );
        const CornerType numTriangles = _horizons[i]->getNumberElements( );

        unsigned int inc = horizonsNodes[i] + 1;
        for (unsigned int i = 0; i < numTriangles; i++)
        {
            out << idx << " 0 0 0 ";
            out << tri[3 * i + 0] + inc << " " << tri[3 * i + 1] + inc << " " << tri[3 * i + 2] + inc << std::endl;
            idx++;
        }
    }
    out << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << reservoidElements << std::endl << std::endl;

    unsigned int reservoirToNFFormat[8] = { 1, 3, 7, 5, 0, 2, 6, 4 };

    const DsGrid3D<CellMesh>& grid = _inputMesh->getGrid( );
    for (int i = 0; i < grid.Size( ); i++)
    {
        out << idx << " 0 0 ";
        const CellMesh& c = grid.GetRef( i );
        for (unsigned int i = 0; i < 8; i++)
        {
            unsigned int p = reservoirToNFFormat[i];
            out << c.point( p ) + totalHorizonNodes + 1 << " ";
        }
        out << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.FACTOR" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
    out << 4 << std::endl;
    out << "'ACTIVE CELL' 'I' 'J' 'K'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
    out << reservoidElements + totalHorizonElements << std::endl;

    idx = 1;
    for (int i = 0; i < ( int ) n; i++)
    {
        for (int t = 0; t < ( int ) _horizons[i]->getNumberElements( ); t++)
        {
            out << idx << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << std::endl;

            idx++;
        }
    }

    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                out << idx << std::endl;

                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;
                out << ( int ) grid.GetRef( i, j, k ).active( ) << " " << i + 1 << " " << j + 1 << " " << k + 1 << std::endl;

                idx++;
            }
        }
    }

    out << "%END" << std::endl;
    return true;
}



bool ReservoirConformalMeshEbE::writeNeutralFileForOuput( const std::string& path )
{
    std::vector<int> auxSlice( _slicesRegular );

    auxSlice.insert( auxSlice.begin( ), 0 );
    for (int s = 1; s < auxSlice.size( ); s++)
    {
        auxSlice[s] += auxSlice[s - 1];
    }

    printf( "Writing file %s...\n", path.c_str( ) );

    //Get the number of horizons on model.
    const unsigned int n = _horizons.size( );

    //Allocate vector to re-index the mesh.
    std::vector<unsigned int> horizonsNodes( n, 0 );

    //Compute the number of nodes and elements on model.
    unsigned int totalHorizonNodes = 0, totalHorizonElements = 0;
    for (unsigned int i = 0; i < n; i++)
    {
        horizonsNodes[i] = _horizons[i]->getNumberNodes( );
        totalHorizonNodes += horizonsNodes[i];

        totalHorizonElements += _horizons[i]->getNumberElements( );
    }

    horizonsNodes[n - 1] = totalHorizonNodes - horizonsNodes[n - 1];
    for (int i = ( int ) n - 2; i >= 0; i--)
    {
        horizonsNodes[i] = horizonsNodes[i + 1] - horizonsNodes[i];
    }

    //Get the number of nodes on output.
    unsigned int outputNodes = _outputGrid.Ni( ) * _outputGrid.Nj( ) * _outputGrid.Nk( );

    //Get the number of nodes on reservoir.
    unsigned int reservoirNodes = _inputMesh->getNumberOfNodes( );

    //Get the number of elements on output.
    unsigned int outputElements = ( _outputGrid.Ni( ) - 1 ) * ( _outputGrid.Nj( ) - 1 ) * ( _outputGrid.Nk( ) - 1 );

    //Get the number of elements on reservoir.
    unsigned int reservoirElements = _inputMesh->getGrid( ).Size( );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << totalHorizonNodes + outputNodes + reservoirNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << totalHorizonNodes + outputNodes + reservoirNodes << std::endl << std::endl;

    //Write the horizon coordinates.
    int idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const std::vector<Point3D>& coord = _horizons[i]->getCoordinates( );
        for (unsigned int i = 0; i < coord.size( ); i++)
        {
            out << idx << " " << coord[i] << std::endl;
            idx++;
        }
    }

    //Write the reservoir output coordinates.
    for (int i = 0; i < _outputGrid.Size( ); i++)
    {
        const Point3D &p = _outputGrid.GetRef( i );
        out << idx << " " << p << std::endl;
        idx++;
    }

    //Write the input reservoir coordinates.
    const std::vector<Point3D>& reserNodes = _inputMesh->getReservoirNodes( );
    for (unsigned int i = 0; i < reserNodes.size( ); i++)
    {
        out << idx << " " << reserNodes[i] << std::endl;
        idx++;
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << totalHorizonElements + outputElements + reservoirElements << std::endl << std::endl;

    out << "%ELEMENT.T3" << std::endl;
    out << totalHorizonElements << std::endl << std::endl;

    //Write the horizon triangles.
    unsigned int inc = 0;
    idx = 1;
    for (unsigned int i = 0; i < n; i++)
    {
        const unsigned int* tri = _horizons[i]->getTriangleList( );
        inc = horizonsNodes[i] + 1;
        unsigned int numTriangles = _horizons[i]->getNumberElements( );

        for (unsigned int i = 0; i < numTriangles; i++)
        {
            out << idx << " 0 0 1 ";
            out << tri[3 * i + 0] + inc << " " << tri[3 * i + 1] + inc << " " << tri[3 * i + 2] + inc << std::endl;
            idx++;
        }
    }
    out << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << outputElements + reservoirElements << std::endl << std::endl;

    inc = totalHorizonNodes + 1;

    //Write the reservoir brick8 element.
    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                out << idx << " 0 1 ";

                out << _outputGrid.GetIndex( i + 1, j + 0, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 1, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 1, k + 1 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 0, k + 1 ) + inc << " ";

                out << _outputGrid.GetIndex( i + 0, j + 0, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 1, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 1, k + 1 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 0, k + 1 ) + inc << " ";

                out << std::endl;

                idx++;
            }
        }
    }

    inc += outputNodes;

    int reservoirToNFFormat[8] = { 1, 3, 7, 5, 0, 2, 6, 4 };

    //Write the input reservoir.
    const DsGrid3D<CellMesh>& grid = _inputMesh->getGrid( );
    for (int i = 0; i < grid.Size( ); i++)
    {
        out << idx << " 0 1 ";
        const CellMesh& c = grid.GetRef( i );
        for (int i = 0; i < 8; i++)
        {
            int p = reservoirToNFFormat[i];
            out << c.point( p ) + inc << " ";
        }
        out << std::endl;
        idx++;
    }

    out << std::endl;

    out << std::endl;

    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR" << std::endl;
    out << 9 << std::endl;
    out << "'ACTIVE CELL' 'I' 'J' 'K' 'BOUNDARY CONDITION''RESERVOIR''FAULT' 'FIXED CELLS' 'SLICES'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR.DATA" << std::endl;

    out << outputElements + totalHorizonElements + reservoirElements << std::endl;

    idx = 1;
    for (int i = 0; i < ( int ) n; i++)
    {
        for (int t = 0; t < ( int ) _horizons[i]->getNumberElements( ); t++)
        {
            out << idx << " 1" << std::endl;
            out << 0 << " " << -i - 1 << " " << -i - 1 << " " << -i - 1 << " " << 0 << " 3 0 0 -1" << std::endl;

            idx++;
        }
    }

    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                out << idx << " 1" << std::endl;

                int border = 0;
                for (int kk = 0; kk <= 1; kk++)
                {
                    for (int jj = 0; jj <= 1; jj++)
                    {
                        for (int ii = 0; ii <= 1; ii++)
                        {
                            int idx = _outputGrid.GetIndex( i + ii, j + jj, k + kk );
                            border = border || _active[idx];
                        }
                    }
                }

                int slice = 0;
                for (int s = 0; s < auxSlice.size( ) - 1; s++)
                {
                    if (k >= auxSlice[s] && k < auxSlice[s + 1])
                    {
                        slice = s;
                        break;
                    }
                }

                if (k == auxSlice[auxSlice.size( ) - 1])
                {
                    slice = auxSlice.size( ) - 1;
                }

                int ri, rj, rk;

                int active = 0;
                int reserv = 0;
                int fault = 0;
                int fixed = 0;
                if (computeReservoirIndexFromOutput( i, j, k, ri, rj, rk ))
                {
                    const CellMesh& c = _inputMesh->getGrid( ).GetRef( ri, rj, rk );
                    active = ( int ) c.active( );
                    reserv = 1;
                    fault = ( int ) c.fault( );
                    fixed = ( int ) c.fixed( );
                }

                Point3D bc( ri, rj, rk );
                for (unsigned int i = 0; i < _hardCells.size( ); i++)
                {
                    if (bc == _hardCells[i])
                    {
                        border = 2;
                        break;
                    }
                }

                out << active << " " << i + 1 << " " << j + 1 << " " << k + 1 << " " << " " << border << " " << reserv << " " << fault << " " << fixed << " " << slice << std::endl;

                idx++;
            }
        }
    }

    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                out << idx << " 1" << std::endl;

                int active = ( int ) grid.GetRef( i, j, k ).active( );

                //Get output index.
                int oi, oj, ok;
                computeOutputIndexFromReservoir( i, j, k, oi, oj, ok );
                oi++;
                oj++;
                ok++;

                const CellMesh& c = _inputMesh->getGrid( ).GetRef( i, j, k );
                int fault = ( int ) c.fault( );
                int fixed = ( int ) c.fixed( );

                out << active << " " << oi << " " << oj << " " << ok << " 0 2 " << fault << " " << fixed << " -1" << std::endl;

                idx++;
            }
        }
    }

    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
    out << 1 << std::endl;
    out << "'ACTIVE POINT'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
    out << outputElements + totalHorizonElements + reservoirElements << std::endl;

    idx = 1;
    for (int i = 0; i < ( int ) n; i++)
    {
        for (int t = 0; t < ( int ) _horizons[i]->getNumberElements( ); t++)
        {
            out << idx << std::endl;
            out << -1 << std::endl;
            out << -1 << std::endl;
            out << -1 << std::endl;

            idx++;
        }
    }

    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                out << idx << std::endl;

                out << ( int ) _active[_outputGrid.GetIndex( i + 1, j + 0, k + 0 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 1, j + 1, k + 0 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 1, j + 1, k + 1 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 1, j + 0, k + 1 )] << std::endl;

                out << ( int ) _active[_outputGrid.GetIndex( i + 0, j + 0, k + 0 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 0, j + 1, k + 0 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 0, j + 1, k + 1 )] << std::endl;
                out << ( int ) _active[_outputGrid.GetIndex( i + 0, j + 0, k + 1 )] << std::endl;

                idx++;
            }
        }
    }

    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                out << idx << std::endl;

                out << -1 << std::endl;
                out << -1 << std::endl;
                out << -1 << std::endl;
                out << -1 << std::endl;

                out << -1 << std::endl;
                out << -1 << std::endl;
                out << -1 << std::endl;
                out << -1 << std::endl;

                idx++;
            }
        }
    }

    out << "%END" << std::endl;
    return true;
}



bool ReservoirConformalMeshEbE::writeNeutralFileSimplifiedOuput( const std::string& path )
{
    printf( "Writing file %s...\n", path.c_str( ) );

    //Get the number of nodes on output.
    unsigned int outputNodes = _outputGrid.Ni( ) * _outputGrid.Nj( ) * _outputGrid.Nk( );

    //Get the number of elements on output.
    unsigned int outputElements = ( _outputGrid.Ni( ) - 1 ) * ( _outputGrid.Nj( ) - 1 ) * ( _outputGrid.Nk( ) - 1 );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << outputNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << outputNodes << std::endl << std::endl;

    //Write the horizon coordinates.
    int idx = 1;

    //Write the reservoir output coordinates.
    for (int i = 0; i < _outputGrid.Size( ); i++)
    {
        const Point3D &p = _outputGrid.GetRef( i );
        out << idx << " " << p << std::endl;
        idx++;
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << outputElements << std::endl << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << outputElements << std::endl << std::endl;

    idx = 1;
    int inc = 1;

    //Write the reservoir brick8 element.
    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                out << idx << " 0 1 ";

                out << _outputGrid.GetIndex( i + 1, j + 0, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 1, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 1, k + 1 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 1, j + 0, k + 1 ) + inc << " ";

                out << _outputGrid.GetIndex( i + 0, j + 0, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 1, k + 0 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 1, k + 1 ) + inc << " ";
                out << _outputGrid.GetIndex( i + 0, j + 0, k + 1 ) + inc << " ";

                out << std::endl;

                idx++;
            }
        }
    }

    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR" << std::endl;
    out << 5 << std::endl;
    out << "'ACTIVE CELL' 'I' 'J' 'K' 'MATERIAL'" << std::endl;
    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR.DATA" << std::endl;

    out << outputElements << std::endl;

    idx = 1;

    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                out << idx << " 1" << std::endl;

                int ri, rj, rk;

                int active = 0;
                int material = 0;
                if (computeReservoirIndexFromFinalReservoir( i, j, k, ri, rj, rk ))
                {
                    const CellMesh& c = _inputMesh->getGrid( ).GetRef( ri, rj, rk );

                    if (c.active( ))
                    {
                        active = 1;
                    }
                }

                if (rk < 0)
                {
                    if (_inputMesh->invertedZ( ))
                    {
                        material = 1;
                    }
                    else
                    {
                        material = 5;
                    }
                }
                else if (rk > _inputMesh->getGrid( ).Nk( ) - 1)
                {
                    if (_inputMesh->invertedZ( ))
                    {
                        material = 5;
                    }
                    else
                    {
                        material = 1;
                    }
                }
                else if (ri < 0 || rj < 0 ||
                         ri > _inputMesh->getGrid( ).Ni( ) - 1 ||
                         rj > _inputMesh->getGrid( ).Nj( ) - 1)
                {
                    material = 2;
                }
                else
                {
                    if (_inputMesh->getGrid( ).GetRef( ri, rj, rk ).active( ))
                    {
                        material = 3;
                    }
                    else
                    {
                        material = 4;
                    }
                }

                out << active << " " << i + 1 << " " << j + 1 << " " << k + 1 << " " << material << std::endl;

                idx++;
            }
        }
    }

    out << "%END" << std::endl;
    return true;
}



bool ReservoirConformalMeshEbE::writeNeutralFileReservoirOuput( const std::string& path )
{
    printf( "Writing file %s...\n", path.c_str( ) );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return false;
    }

    out << _outputGrid.Ni( ) << " " << _outputGrid.Nj( ) << " " << _outputGrid.Nk( ) << std::endl;
    //        out << props[0].size( ) << " ";
    //        for (unsigned int i = 0; i < props[0].size( ); i++)
    //        {
    //            out << props[0][i]._name << " ";
    //        }
    out << 3 << "x y z";

    out << std::endl;

    for (int k = 0; k < _outputGrid.Nk( ); k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ); j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ); i++)
            {
                out << _outputGrid.Get( i, j, k ) << std::endl;
            }
        }
    }
    for (int d = 0; d < 3; d++)
    {
        for (int k = 0; k < _outputGrid.Nk( ); k++)
        {
            for (int j = 0; j < _outputGrid.Nj( ); j++)
            {
                for (int i = 0; i < _outputGrid.Ni( ); i++)
                {
                    out << _outputGrid.Get( i, j, k )[d] << std::endl;
                }
            }
        }
    }
    //    for (unsigned int p = 0; p < props[0].size( ); p++)
    //    {
    //        for (int k = oki; k < okf; k++)
    //        {
    //            for (int j = oji; j < ojf; j++)
    //            {
    //                for (int i = oii; i < oif; i++)
    //                {
    //                    int ri, rj, rk;
    //                    computeReservoirIndexFromOutput( i, j, k, ri, rj, rk );
    //
    //                    int idx = _inputMesh->getGrid( ).GetIndex( ri, rj, rk );
    //
    //                    if (idx < ( _outputGrid.Ni( ) - 1 ) * ( _outputGrid.Nj( ) - 1 ) * ( nk - 1 ))
    //                        out << props[0][p]._p[idx] << std::endl;
    //                    else
    //                        out << 0 << std::endl;
    //                }
    //            }
    //        }
    //    }

    return true;
}


//PRIVATE METHODS.



void ReservoirConformalMeshEbE::setHardPointsPosition( )
{
    _active.resize( _outputGrid.Size( ) );
    const std::vector<Point3D>& nodes = _inputMesh->getReservoirNodes( );
    const DsGrid3D<CellMesh>& grid = _inputMesh->getGrid( );
    int contFixed = 0;
    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                if (grid.GetRef( i, j, k ).fixed( ))
                {
                    contFixed++;

                    //Get output index.
                    int oi, oj, ok;
                    computeOutputIndexFromReservoir( i, j, k, oi, oj, ok );

                    for (int p = 0; p < 8; p++)
                    {
                        //Get the vertex index.
                        int vi = oi + dx[p][0];
                        int vj = oj + dx[p][1];
                        int vk = ok + dx[p][2];

                        int pnt = grid.GetRef( i, j, k ).point( p );
                        int gidx = _outputGrid.GetIndex( vi, vj, vk );

                        _outputGrid.SetConst( gidx, nodes[pnt] );
                        _active[gidx] = true;
                    }
                }
            }
        }
    }

    printf( "%d cells were fixed and %d points were maked as actived\n", contFixed, ( int ) std::count( _active.begin( ), _active.end( ), true ) );

    //Fix boundary points.
    int f = _fixedBoundaryCells;
    for (int k = 0; k < _outputGrid.Nk( ); k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ); j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ); i++)
            {
                if (i >= f && i <= _outputGrid.Ni( ) - 1 - f &&
                    j >= f && j <= _outputGrid.Nj( ) - 1 - f &&
                    k >= f && k <= _outputGrid.Nk( ) - 1 - f)
                {
                    continue;
                }
                int idx = _outputGrid.GetIndex( i, j, k );
                _active[idx] = true;
            }
        }
    }

    //setReservoirBoundaryConditions( );

    printf( "Horizonte nas fatias:\n" );
    //Set the horizon points on correct slice.
    for (unsigned int i = 0; i < _horizonSlices.size( ); i++)
    {
        //Fix points on slice s in the horizon i.
        setHorizonPoints( _horizonSlices[i], i );
        printf( "%d\n", _horizonSlices[i] );
    }

    printf( "%d points were maked as actived at the end\n", ( int ) std::count( _active.begin( ), _active.end( ), true ) );

}



void ReservoirConformalMeshEbE::setHorizonPoints( unsigned int slice, unsigned int h )
{
    //Get triangle list information.
    const CornerType numTriangles = _horizons[h]->getNumberElements( );
    std::vector<CornerType> triangles;

    //Cells to be fixed.
    std::set<Point3D> cells;

    for (unsigned int t = 0; t < numTriangles; t++)
    {
        triangles.push_back( t );
    }

    //Project the triangles on border.
    projectHorizonOnMesh( slice, h, triangles, cells );

    //Project all cells on triangle mesh.
    projectCellsOnHorizon( h, cells );
}



void ReservoirConformalMeshEbE::projectCellsOnHorizon( unsigned int h,
                                                       const std::set<Point3D>& cells )
{
    int k = _horizonSlices[h];
    for (auto it = cells.begin( ); it != cells.end( ); it++)
    {
        int i = ( int ) it->x[0];
        int j = ( int ) it->x[1];
        for (int di = 0; di <= 1; di++)
        {
            for (int dj = 0; dj <= 1; dj++)
            {
                int idx = _outputGrid.GetIndex( i + di, j + dj, k );
                if (!_active[idx])
                {
                    //Project point.
                    Point3D &p = _outputGrid.GetRef( idx );
                    if (projectPointOnHorizon( p, h ))
                    {
                        //Set the point as active point.
                        _active[idx] = true;
                    }
                }
            }
        }
    }
}



void ReservoirConformalMeshEbE::projectHorizonOnMesh( unsigned int slice, unsigned int h,
                                                      const std::vector<CornerType>& triangles,
                                                      std::set<Point3D>& cells )
{
    //Get triangle list information.
    const CornerType *triangleList = _horizons[h]->getTriangleList( );
    const std::vector<Point3D>& coords = _horizons[h]->getCoordinates( );

    for (unsigned int tri = 0; tri < triangles.size( ); tri++)
    {
        CornerType t = triangles[tri];

        //Get cell for each point.
        for (CornerType c = 3 * t; c < 3 * t + 3; c++)
        {
            CornerType v = triangleList[c];
            Point3D p = coords[v];

            bool stop = false;

            //Find the cell.
            for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
            {
                for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
                {
                    double x1 = _outputGrid.GetRef( i + 0, 0, slice )[0];
                    double x2 = _outputGrid.GetRef( i + 1, 0, slice )[0];
                    double y1 = _outputGrid.GetRef( 0, j + 0, slice )[1];
                    double y2 = _outputGrid.GetRef( 0, j + 1, slice )[1];

                    if (x1 < p[0] && p[0] < x2 && y1 < p[1] && p[1] < y2)
                    {
                        cells.insert( Point3D( i, j, 0 ) );
                        stop = true;
                        break;
                    }
                }
                if (stop) break;
            }
        }
    }
}



bool ReservoirConformalMeshEbE::projectPointOnHorizon( Point3D& p, unsigned int h )
{
    //Get triangle list information.
    const CornerType *triangleList = _horizons[h]->getTriangleList( );
    const std::vector<Point3D>& coords = _horizons[h]->getCoordinates( );

    CornerType t = _horizons[h]->getTriangle( p[0], p[1] );

    if (t == CornerTable::BORDER_CORNER)
        return false;

    //Get vertices index.
    CornerType v0 = triangleList[3 * t + 0];
    CornerType v1 = triangleList[3 * t + 1];
    CornerType v2 = triangleList[3 * t + 2];

    //Get the triangle coordinates.
    Point3D tp[3] = { coords[v0], coords[v1], coords[v2] };

    double S = ( ( tp[2] - tp[1] )^( tp[0] - tp[1] ) )[2];

    double l[3];
    for (int i = 0; i < 3; i++)
    {
        Point3D v = tp[ ( i + 2 ) % 3] - tp[( i + 1 ) % 3];
        Point3D w = p - tp[( i + 1 ) % 3];
        double s = ( v ^ w )[2];
        l[i] = s / S;
    }

    //Define a new z.
    p[2] = l[0] * tp[0][2] + l[1] * tp[1][2] + l[2] * tp[2][2];
    return true;
}



void ReservoirConformalMeshEbE::optimize( )
{
    printf( "Dimensao: %d x %d x %d\n", _outputGrid.Ni( ), _outputGrid.Nj( ), _outputGrid.Nk( ) );
    //Allocate solver and model.
    VertexVertexSolver solver1;
    LISSolver solver;

//    solver.setEPS( _eps );
//    solver.setNumberOfThreads( _numThreads );
//    solver.setPenalization( _pen );
//    solver.computeReservoirCellMesh( _outputGrid, _active,
//                                     _inputMesh,
//                                     _numCellsRegularI, _numCellsRegularJ, _reservoirK,
//                                     _fixedBoundaryCells );

    solver1.setEPS( _eps );
    solver1.setNumberOfThreads( _numThreads );
    solver1.setPenalization( _pen );
    solver1.computeReservoirCellMesh( _outputGrid, _active,
                                      _inputMesh,
                                      _numCellsRegularI, _numCellsRegularJ, _reservoirK,
                                      _fixedBoundaryCells );
}



void ReservoirConformalMeshEbE::allocateMemory( )
{
    //Compute the new dimensions for grid.
    int ni = _inputMesh->getGrid( ).Ni( ) + 2 * _numCellsRegularI + 1;
    int nj = _inputMesh->getGrid( ).Nj( ) + 2 * _numCellsRegularJ + 1;
    int nk = 1;
    for (unsigned int i = 0; i < _slicesRegular.size( ); i++)
    {
        nk += _slicesRegular[i];
    }

    //Allocate the output grid.
    _outputGrid.Resize( ni, nj, nk );
}



bool ReservoirConformalMeshEbE::checkData( )
{
    bool state = true;
    if (_slicesRegular.size( ) != _horizons.size( ) + 1)
    {
        printf( "Error: it's necessary to define the number of cells between"
                " each pair of surfaces, included the reservoir.\n" );
        state = false;
    }

    for (unsigned int i = 0; i < _slicesRegular.size( ); i++)
    {
        if (( int ) _slicesRegular[i] < 1)
        {
            printf( "Error: the number of cells between each surface must be"
                    " greater than zero.\n" );

            state = false;
            break;
        }
    }

    return state;
}



bool ReservoirConformalMeshEbE::computeReservoirIndexFromFinalReservoir( int i, int j, int k, int &ri, int &rj, int &rk )
{
    //Get the reservoir dimensions.
    int ni = _inputMesh->getGrid( ).Ni( );
    int nj = _inputMesh->getGrid( ).Nj( );
    int nk = _inputMesh->getGrid( ).Nk( );

    ri = i - _numCellsI;
    rj = j - _numCellsJ;
    rk = k - _slices[0];

    if (_inputMesh->invertedX( ))
        ri = ( ni - 1 ) - i + _numCellsI;

    if (_inputMesh->invertedY( ))
        rj = ( nj - 1 ) - j + _numCellsJ;

    if (_inputMesh->invertedZ( ))
        rk = ( nk - 1 ) - k + _slices[0];

    bool inReservoir = ( ( ri >= 0 && ri < ni ) &&
                         ( rj >= 0 && rj < nj ) &&
                         ( rk >= 0 && rk < nk ) );

    return inReservoir;
}



bool ReservoirConformalMeshEbE::computeReservoirIndexFromOutput( int i, int j, int k, int &ri, int &rj, int &rk )
{
    //Get the reservoir dimensions.
    int ni = _inputMesh->getGrid( ).Ni( );
    int nj = _inputMesh->getGrid( ).Nj( );
    int nk = _inputMesh->getGrid( ).Nk( );

    ri = i - _numCellsRegularI;
    rj = j - _numCellsRegularJ;
    rk = k - _reservoirK;

    if (_inputMesh->invertedX( ))
        ri = ( ni - 1 ) - i + _numCellsRegularI;

    if (_inputMesh->invertedY( ))
        rj = ( nj - 1 ) - j + _numCellsRegularJ;

    if (_inputMesh->invertedZ( ))
        rk = ( nk - 1 ) - k + _reservoirK;

    bool inReservoir = ( ( ri >= 0 && ri < ni ) &&
                         ( rj >= 0 && rj < nj ) &&
                         ( rk >= 0 && rk < nk ) );

    return inReservoir;
}



bool ReservoirConformalMeshEbE::computeOutputIndexFromReservoir( int i, int j, int k, int &oi, int &oj, int &ok )
{
    //Get the reservoir dimensions.
    const int ri = _inputMesh->getGrid( ).Ni( );
    const int rj = _inputMesh->getGrid( ).Nj( );
    const int rk = _inputMesh->getGrid( ).Nk( );

    oi = i + _numCellsRegularI;
    oj = j + _numCellsRegularJ;
    ok = k + _reservoirK;

    if (_inputMesh->invertedX( ))
        oi = ( ri - 1 ) - i + _numCellsRegularI;

    if (_inputMesh->invertedY( ))
        oj = ( rj - 1 ) - j + _numCellsRegularJ;

    if (_inputMesh->invertedZ( ))
        ok = ( rk - 1 ) - k + _reservoirK;

    return true;
}



bool ReservoirConformalMeshEbE::computeFinalOutputIndexFromReservoir( int i, int j, int k, int &oi, int &oj, int &ok )
{
    //Get the reservoir dimensions.
    const int ri = _inputMesh->getGrid( ).Ni( );
    const int rj = _inputMesh->getGrid( ).Nj( );
    const int rk = _inputMesh->getGrid( ).Nk( );

    oi = i + _numCellsI;
    oj = j + _numCellsJ;
    ok = k + _slices[0];

    if (_inputMesh->invertedX( ))
        oi = ( ri - 1 ) - i + _numCellsI;

    if (_inputMesh->invertedY( ))
        oj = ( rj - 1 ) - j + _numCellsJ;

    if (_inputMesh->invertedZ( ))
        ok = ( rk - 1 ) - k + _slices[0];

    return true;
}



void ReservoirConformalMeshEbE::preprocessGeometryDimensions( )
{
    //Get the reservoir dimensions.
    const int ri = _inputMesh->getGrid( ).Ni( );
    const int rj = _inputMesh->getGrid( ).Nj( );
    const int rk = _inputMesh->getGrid( ).Nk( );

    //Convention: for each position we have the min and max average for that
    //dimension in the (x, y) position and the number of active cells in the
    //z position. i.e; (min, max, numActiveCells)
    //Allocate memory.
    _geometryI.resize( ri );
    _geometryJ.resize( rj );
    _geometryK.resize( rk );

    for (int k = 0; k < rk; k++)
    {
        for (int j = 0; j < rj; j++)
        {
            for (int i = 0; i < ri; i++)
            {
                const CellMesh& c = _inputMesh->getGrid( ).GetRef( i, j, k );

                if (c.active( ))
                {
                    //Count the number of active cells.
                    _geometryI[i][2]++;
                    _geometryJ[j][2]++;
                    _geometryK[k][2]++;

                    //Count i dimensions.
                    _geometryI[i][0] += _inputMesh->getXMinFace( i, j, k );
                    _geometryI[i][1] += _inputMesh->getXMaxFace( i, j, k );

                    //Count k dimensions.
                    _geometryJ[j][0] += _inputMesh->getYMinFace( i, j, k );
                    _geometryJ[j][1] += _inputMesh->getYMaxFace( i, j, k );

                    //Count k dimensions.
                    _geometryK[k][0] += _inputMesh->getZMinFace( i, j, k );
                    _geometryK[k][1] += _inputMesh->getZMaxFace( i, j, k );
                }
            }
        }
    }

    //Compute the averages.
    for (unsigned int i = 0; i < _geometryI.size( ); i++)
    {
        if (_geometryI[i][2] != 0)
        {
            _geometryI[i][0] /= _geometryI[i][2];
            _geometryI[i][1] /= _geometryI[i][2];
        }
    }

    for (unsigned int j = 0; j < _geometryJ.size( ); j++)
    {
        if (_geometryJ[j][2] != 0)
        {
            _geometryJ[j][0] /= _geometryJ[j][2];
            _geometryJ[j][1] /= _geometryJ[j][2];
        }
    }

    for (unsigned int k = 0; k < _geometryK.size( ); k++)
    {
        if (_geometryK[k][2] != 0)
        {
            _geometryK[k][0] /= _geometryK[k][2];
            _geometryK[k][1] /= _geometryK[k][2];
        }
    }
}



void ReservoirConformalMeshEbE::computeRegularGeometry( )
{
    //Get the average of z on reservoir.
    double zR = _inputMesh->getAverageZ( );

    //Storage the current z index.
    int currentZ = 0;
    for (unsigned int h = 0, s = 0; h < _horizons.size( ) - 1; h++, s++)
    {
        //Add slice to set horizon.
        _horizonSlices.push_back( currentZ );

        //Get the horizon z average.
        double zH1 = _horizons[h + 0]->getAverageZ( );
        double zH2 = _horizons[h + 1]->getAverageZ( );

        if (zH1 < zR && zR < zH2)
        {
            //Get the bottom and top values of z on reservoir.
            double zRBottom, zRTop;
            if (_inputMesh->invertedZ( ))
            {
                zRBottom = _inputMesh->getActiveZMin( );
                zRTop = _inputMesh->getActiveZMax( );
            }
            else
            {
                zRBottom = _inputMesh->getActiveZMin( );
                zRTop = _inputMesh->getActiveZMax( );
            }

            //Compute limits between the bottom horizon and the reservoir.
            int zi = currentZ;
            int zf = zi + _slicesRegular[s];
            s++;

            //Initialize points between the bottom horizon and the reservoir.
            fillCellsBetweenTwoLayers( zi, zf, zH1, zRBottom );

            //Compute the reservoir limits.
            zi = zf;
            zf = zi + _slicesRegular[s];
            s++;

            //Save where the reservoir begins.
            _reservoirK = zi;

            //Initialize points on reservoir.
            fillCellsBetweenTwoLayers( zi, zf, zRBottom, zRTop );

            //Compute limits between the reservoir and top horizon.
            zi = zf;
            zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            //Initialize points between the reservoir and top horizon.
            fillCellsBetweenTwoLayers( zi, zf, zRTop, zH2 );

            currentZ = zf;
        }
        else
        {
            //Set the values of z-index to start and finish.
            int zi = currentZ;

            //Include the last slice of points at the last iteration.
            int zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            //Compute initial points between zi and zf.
            fillCellsBetweenTwoLayers( zi, zf, zH1, zH2 );

            currentZ = zf;
        }
    }

    //Add slice to set horizon.
    _horizonSlices.push_back( currentZ - 1 );
}



void ReservoirConformalMeshEbE::setFacScal( double facScal )
{
    _facScal = facScal;
}



void ReservoirConformalMeshEbE::computeHorizonsSlices( )
{
    _horizonSlices.clear( );
    //Get the average of z on reservoir.
    double zR = _inputMesh->getAverageZ( );

    //Storage the current z index.
    int currentZ = 0;
    for (unsigned int h = 0, s = 0; h < _horizons.size( ) - 1; h++, s++)
    {
        //Add slice to set horizon.
        _horizonSlices.push_back( currentZ );

        //Get the horizon z average.
        double zH1 = _horizons[h + 0]->getAverageZ( );
        double zH2 = _horizons[h + 1]->getAverageZ( );

        if (zH1 < zR && zR < zH2)
        {

            //Compute limits between the bottom horizon and the reservoir.
            int zi = currentZ;
            int zf = zi + _slicesRegular[s];
            s++;

            //Compute the reservoir limits.
            zi = zf;
            zf = zi + _slicesRegular[s];
            s++;

            //Save where the reservoir begins.
            _reservoirK = zi;

            //Compute limits between the reservoir and top horizon.
            zi = zf;
            zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            currentZ = zf;
        }
        else
        {
            //Set the values of z-index to start and finish.
            int zi = currentZ;

            //Include the last slice of points at the last iteration.
            int zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            currentZ = zf;
        }
    }

    //Add slice to set horizon.
    _horizonSlices.push_back( currentZ - 1 );
}



void ReservoirConformalMeshEbE::computeInitialGeometry( )
{
    computeRegularGeometry( );
    return;

    //Preprocess the geometry on reservoir.
    preprocessGeometryDimensions( );

    //Get the reservoir dimensions.
    const int ni = _inputMesh->getGrid( ).Ni( );
    const int nj = _inputMesh->getGrid( ).Nj( );
    const int nk = _inputMesh->getGrid( ).Nk( );

    for (int k = 0; k < _outputGrid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ) - 1; i++)
            {
                int ri, rj, rk;
                if (computeReservoirIndexFromOutput( i, j, k, ri, rj, rk ))
                {
                    double bounds[3][2] = {
                        { _geometryI[ri][0],
                         _geometryI[ri][1] },
                        {_geometryJ[rj][0],
                         _geometryJ[rj][1] },
                        {_geometryK[rk][0], _geometryK[rk][1] }
                    };

                    Point3D p;
                    for (int dk = 0; dk <= 1; dk++)
                    {
                        for (int dj = 0; dj <= 1; dj++)
                        {
                            for (int di = 0; di <= 1; di++)
                            {
                                p[0] = bounds[0][di];
                                p[1] = bounds[1][dj];
                                p[2] = bounds[2][dk];
                                _outputGrid.Set( i + di, j + dj, k + dk, p );
                            }
                        }
                    }
                }
                else
                {
                    if (ri >= 0 && ri < ni)
                    {
                        double x[2] = { _geometryI[ri][0], _geometryI[ri][1] };

                        for (int dk = 0; dk <= 1; dk++)
                        {
                            for (int dj = 0; dj <= 1; dj++)
                            {
                                for (int di = 0; di <= 1; di++)
                                {
                                    Point3D &p = _outputGrid.GetRef( i + di, j + dj, k + dk );
                                    p[0] = x[di];
                                }
                            }
                        }
                    }
                    else
                    {
                        double xMin, xMax;

                        if (ri < 0)
                        {
                            xMax = _geometryI[0][0];
                            xMin = xMax - _distanceI;
                        }
                        else
                        {
                            xMin = _geometryI[ni - 1][1];
                            xMax = xMin + _distanceI;
                        }

                        for (int dk = 0; dk <= 1; dk++)
                        {
                            for (int dj = 0; dj <= 1; dj++)
                            {
                                for (int di = 0; di <= 1; di++)
                                {
                                    Point3D &p = _outputGrid.GetRef( i + di, j + dj, k + dk );
                                    if (ri < 0)
                                    {
                                        p[0] = xMin + ( i + di ) * ( xMax - xMin ) / _numCellsRegularI;
                                    }
                                    else
                                    {
                                        p[0] = xMin + ( i + di - ( _numCellsRegularI + ni ) ) * ( xMax - xMin ) / _numCellsRegularI;
                                    }
                                }
                            }
                        }
                    }

                    if (rj >= 0 && rj < nj)
                    {
                        double y[2] = { _geometryJ[rj][0], _geometryJ[rj][1] };

                        for (int dk = 0; dk <= 1; dk++)
                        {
                            for (int dj = 0; dj <= 1; dj++)
                            {
                                for (int di = 0; di <= 1; di++)
                                {
                                    Point3D &p = _outputGrid.GetRef( i + di, j + dj, k + dk );
                                    p[1] = y[dj];
                                }
                            }
                        }
                    }
                    else
                    {
                        double yMin, yMax;

                        if (rj < 0)
                        {
                            yMax = _geometryJ[0][0];
                            yMin = yMax - _distanceJ;
                        }
                        else
                        {
                            yMin = _geometryJ[nj - 1][1];
                            yMax = yMin + _distanceJ;
                        }

                        for (int dk = 0; dk <= 1; dk++)
                        {
                            for (int dj = 0; dj <= 1; dj++)
                            {
                                for (int di = 0; di <= 1; di++)
                                {
                                    Point3D &p = _outputGrid.GetRef( i + di, j + dj, k + dk );
                                    if (rj < 0)
                                    {
                                        p[1] = yMin + ( j + dj ) * ( yMax - yMin ) / _numCellsRegularJ;
                                    }
                                    else
                                    {
                                        p[1] = yMin + ( j + dj - ( _numCellsRegularJ + nj ) ) * ( yMax - yMin ) / _numCellsRegularJ;
                                    }
                                }
                            }
                        }
                    }


                    if (rk >= 0 && rk < nk)
                    {
                        double z[2] = { _geometryK[rk][0], _geometryK[rk][1] };

                        for (int dk = 0; dk <= 1; dk++)
                        {
                            for (int dj = 0; dj <= 1; dj++)
                            {
                                for (int di = 0; di <= 1; di++)
                                {
                                    Point3D &p = _outputGrid.GetRef( i + di, j + dj, k + dk );
                                    p[2] = z[dk];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    double zRBottom = _geometryK[0][0], zRTop = _geometryK[nk - 1][1];
    double zR = _inputMesh->getAverageZ( );

    if (_inputMesh->invertedZ( ))
        std::swap( zRTop, zRBottom );

    //Storage the current z index.
    int currentZ = 0;
    for (unsigned int h = 0, s = 0; h < _horizons.size( ) - 1; h++, s++)
    {
        //Get the horizon z average.
        double zH1 = _horizons[h + 0]->getAverageZ( );
        double zH2 = _horizons[h + 1]->getAverageZ( );

        if (zH1 < zR && zR < zH2)
        {
            //Compute limits between the bottom horizon and the reservoir.
            int zi = currentZ;
            int zf = zi + _slicesRegular[s];
            s++;

            //Initialize points between the bottom horizon and the reservoir.
            fillCellsBetweenTwoLayers2( zi, zf, zH1, zRBottom );

            //Compute the reservoir limits.
            zi = zf;
            zf = zi + _slicesRegular[s];
            s++;

            //Compute limits between the reservoir and top horizon.
            zi = zf + 1;
            zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            //Initialize points between the reservoir and top horizon.
            fillCellsBetweenTwoLayers2( zi, zf, zRTop + ( zH2 - zRTop ) / ( zf - zi ), zH2 );

            currentZ = zf;
        }
        else
        {
            //Set the values of z-index to start and finish.
            int zi = currentZ;

            //Include the last slice of points at the last iteration.
            int zf = zi + _slicesRegular[s] + ( s == _horizons.size( ) ? 1 : 0 );

            currentZ = zf;
        }
    }
}



void ReservoirConformalMeshEbE::fillCellsBetweenTwoLayers( const int zi, const int zf,
                                                           double zBot, const double zMax )
{
    const double xMin = _inputMesh->getXMin( ) - _distanceI;
    const double xMax = _inputMesh->getXMax( ) + _distanceI;

    const double yMin = _inputMesh->getYMin( ) - _distanceJ;
    const double yMax = _inputMesh->getYMax( ) + _distanceJ;

    int den = zf - zi;

    //Guarantee that the last point has zMax (Horizon z.)
    if (zf == _outputGrid.Nk( ))
        den--;

    for (int k = zi; k < zf; k++)
    {
        double z = zBot + ( k - zi ) * ( zMax - zBot ) / den;

        for (int j = 0; j < _outputGrid.Nj( ); j++)
        {
            double y = yMin + j * ( yMax - yMin ) / ( _outputGrid.Nj( ) - 1 );

            for (int i = 0; i < _outputGrid.Ni( ); i++)
            {
                Point3D p;
                p.x[0] = xMin + i * ( xMax - xMin ) / ( _outputGrid.Ni( ) - 1 );
                p.x[1] = y;
                p.x[2] = z;

                _outputGrid.Set( i, j, k, p );
            }
        }
    }
}



void ReservoirConformalMeshEbE::fillCellsBetweenTwoLayers2( int zi, int zf,
                                                            double zBot, const double zMax )
{
    zf = std::min( zf, _outputGrid.Nk( ) );

    int den = zf - zi;

    //Guarantee that the last point has zMax (Horizon z.)
    if (zf == _outputGrid.Nk( ))
        den--;

    for (int k = zi; k < zf; k++)
    {
        double z = zBot + ( k - zi ) * ( zMax - zBot ) / den;
        for (int j = 0; j < _outputGrid.Nj( ); j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ); i++)
            {
                Point3D &p = _outputGrid.GetRef( i, j, k );
                p[2] = z;
            }
        }
    }
}



void ReservoirConformalMeshEbE::computeNumberOfSlices( )
{
    //Get the density of cells.
    //        double cellsByMeters = _inputMesh->getCellsByMeters( );
    double cellsByMeters = _facScal * _inputMesh->getActiveCellsByMeters( );

    //Allocate space to store the number of slices.
    unsigned int n = _horizons.size( ) + 1, cont = 0;
    _slicesRegular.resize( n, 0 );

    //Get the average of z on reservoir.
    double zR = _inputMesh->getAverageZ( );

    for (unsigned int i = 0; i < _horizons.size( ) - 1; i++)
    {
        //Get horizons max and min z.
        double zH1 = _horizons[i + 0]->getMinZ( );
        double zH2 = _horizons[i + 1]->getMaxZ( );

        double minZ = 0, maxZ = 0;

        if (zH1 < zR && zR < zH2)
        {
            //Set the number of slices between the bottom horizon and reservoir.
            minZ = zH1;
            maxZ = zR;

            _slicesRegular[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );

            _reservoirK = 0;
            for (int j = 0; j < cont; j++)
                _reservoirK += _slicesRegular[j];


            //Set the number of slices on reservoir.
            _slicesRegular[cont++] = _inputMesh->getGrid( ).Nk( );

            //Set the number of slices between the top horizon and reservoir.
            minZ = zR;
            maxZ = zH2;

            _slicesRegular[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );
        }
        else
        {
            //Set the number of slices between two horizons.
            minZ = zH1;
            maxZ = zH2;
            _slicesRegular[cont++] = ( int ) ( ( maxZ - minZ ) * cellsByMeters + 1.5 );
        }
    }
}



void ReservoirConformalMeshEbE::rotatePoint( Point3D& p, double angle )
{
    double x = p[0], y = p[1];
    p[0] = x * cos( angle ) - y * sin( angle );
    p[1] = x * sin( angle ) + y * cos( angle );
}



void ReservoirConformalMeshEbE::posprocess( )
{
    double angle = _inputMesh->getRotationAngle( );

    Point3D transl( 0, 0, 0 );
    _inputMesh->getTranslateVector( transl[0], transl[1] );

    for (int k = 0; k < _outputGrid.Nk( ); k++)
    {
        for (int j = 0; j < _outputGrid.Nj( ); j++)
        {
            for (int i = 0; i < _outputGrid.Ni( ); i++)
            {
                rotatePoint( _outputGrid.GetRef( i, j, k ), angle );
                Point3D &p = _outputGrid.GetRef( i, j, k );
                p += transl;
            }
        }
    }
    _inputMesh->rotateData( angle );
    _inputMesh->untranslateData( );

    for (unsigned int h = 0; h < _horizons.size( ); h++)
    {
        _horizons[h]->rotateData( angle );
        _horizons[h]->translateData( transl[0], transl[1] );
    }
}



void ReservoirConformalMeshEbE::preprocess( )
{
    std::sort( _horizons.begin( ), _horizons.end( ), compare );

    std::cout << ( _inputMesh->getActiveZMax( ) - _inputMesh->getActiveZMin( ) ) / 2.0 << std::endl;
    for (unsigned int i = 0; i < _horizons.size( ); i++)
    {
        std::cout << _horizons[i]->getAverageZ( ) << std::endl;
    }

    //compute the number of slices.
    if (_slicesRegular.size( ) == 0)
    {
        computeNumberOfSlices( );
    }

    for (unsigned int i = 0; i < _slicesRegular.size( ); i++)
        std::cout << _slicesRegular[i] << std::endl;
}



void ReservoirConformalMeshEbE::preprocessActiveCells( )
{

    struct CellsDistance
    {
        int i, j, k;
        int dist;
    };

    std::queue<CellsDistance> q;

    //Get the input grid.
    DsGrid3D<CellMesh>& grid = _inputMesh->getGrid( );

    //A vector to know if a cell was already processed or not.
    std::vector<bool> visited( grid.Size( ), false );

    //Get all faces on fault and set all active cells as fixed.
    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                if (grid.GetRef( i, j, k ).fault( ))
                {
                    CellsDistance cd;
                    cd.i = i;
                    cd.j = j;
                    cd.k = k;
                    cd.dist = 0;

                    q.push( cd );
                    visited[grid.GetIndex( i, j, k )] = true;
                }

                if (grid.GetRef( i, j, k ).active( ))
                {
                    grid.GetRef( i, j, k ).setFixed( true );
                }
            }
        }
    }

    //Find all cells with a distance less than equal to max radius.
    while (!q.empty( ))
    {
        //Get the first element from the queue.
        CellsDistance cd = q.front( );
        //Remove the first element.
        q.pop( );

        grid.GetRef( cd.i, cd.j, cd.k ).setFixed( false );

        for (int k = -1; k <= 1; k++)
        {
            for (int j = -1; j <= 1; j++)
            {
                for (int i = -1; i <= 1; i++)
                {
                    if (grid.ValidIndices( i + cd.i, j + cd.j, k + cd.k ))
                    {
                        int idx = grid.GetIndex( i + cd.i, j + cd.j, k + cd.k );
                        if (!visited[idx] && cd.dist + 1 <= _radiusFromFault)
                        {
                            CellsDistance newcd;
                            newcd.i = i + cd.i;
                            newcd.j = j + cd.j;
                            newcd.k = k + cd.k;
                            newcd.dist = cd.dist + 1;

                            q.push( newcd );
                            visited[idx] = true;
                        }
                    }
                }
            }
        }
    }
}



bool ReservoirConformalMeshEbE::simplifySlice( int s, const std::vector<int>& idxs )
{
    int numFinalSlices = _slices[s];

    if (numFinalSlices >= _slicesRegular[s])
    {
        return false;
    }
    std::vector<int> sliceIndex( _slicesRegular[s], 0 );

    //Compute the angular coefficient.
    double factor = ( double ) numFinalSlices / _slicesRegular[s];

    //Verify where the slices will be on.
    for (int i = 0; i < _slicesRegular[s]; i++)
    {
        //k(s) = s * kMax / sMax.
        sliceIndex[i] = ( int ) ( ( i - 1 ) * factor );
    }

    int lastSlice = idxs[s + 1] - 1;

    int value = sliceIndex.back( );
    for (int i = _slicesRegular[s] - 1; i >= 0; i--)
    {
        if (sliceIndex[i] != value)
        {
            value = sliceIndex[i];
            int init = idxs[s + 1] - _slicesRegular[s] + 1 + i;
            _outputGrid.EraseSliceK( init, lastSlice );

            lastSlice = init - 2;
        }
    }
    int init = idxs[s] + 1;
    _outputGrid.EraseSliceK( init, lastSlice );

    return true;
}



bool ReservoirConformalMeshEbE::simplifyTopReservoir( int ridx, const std::vector<int>& idxs )
{
    int s = ridx + 1;
    int numFinalSlices = _slices[s];

    if (numFinalSlices >= _slicesRegular[s])
    {
        return false;
    }

    std::vector<int> sliceIndex( _slicesRegular[s], 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _slicesRegular[s]; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _slicesRegular[s] - 1 );
        double num = ( numFinalSlices - 1 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _slicesRegular[s] - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _slicesRegular[s]; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    int lastSlice = idxs[s + 1];

    int value = sliceIndex.back( );
    for (int i = _slicesRegular[s] - 1; i >= 0; i--)
    {
        if (sliceIndex[i] != value)
        {
            value = sliceIndex[i];
            int init = idxs[s + 1] - _slicesRegular[s] + 1 + i;
            _outputGrid.EraseSliceK( init, lastSlice );

            lastSlice = init;
        }
    }
    int init = idxs[s];
    _outputGrid.EraseSliceK( init, lastSlice );
}



bool ReservoirConformalMeshEbE::simplifyIRight( )
{
    if (_numCellsI >= _numCellsRegularI)
    {
        return false;
    }

    std::vector<int> sliceIndex( _numCellsRegularI, 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _numCellsRegularI; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _numCellsRegularI - 1 );
        double num = ( _numCellsI - 2 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _numCellsRegularI - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _numCellsRegularI; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    int finalSlice = _outputGrid.Ni( );
    int value = sliceIndex.back( );
    for (int i = _outputGrid.Ni( ), j = _numCellsRegularI - 1; j >= 0; i--, j--)
    {
        if (sliceIndex[j] != value)
        {
            value = sliceIndex[j];
            _outputGrid.EraseSliceI( i, finalSlice );

            finalSlice = i;
        }
    }

    return true;
}



bool ReservoirConformalMeshEbE::simplifyILeft( )
{
    if (_numCellsI >= _numCellsRegularI)
    {
        return false;
    }

    std::vector<int> sliceIndex( _numCellsRegularI, 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _numCellsRegularI; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _numCellsRegularI - 1 );
        double num = ( _numCellsI - 1 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _numCellsRegularI - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _numCellsRegularI; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    std::reverse( sliceIndex.begin( ), sliceIndex.end( ) );

    int lastISlice = _numCellsRegularI - 1;
    int finalSlice = lastISlice;

    int value = sliceIndex.back( );
    for (int i = _numCellsRegularI - 1; i >= 0; i--)
    {
        if (sliceIndex[i] != value)
        {
            value = sliceIndex[i];
            int init = lastISlice - _numCellsRegularI + i + 1;
            _outputGrid.EraseSliceI( init, finalSlice );

            finalSlice = init;
        }
    }
    int init = 0;
    _outputGrid.EraseSliceI( init, finalSlice );
    return true;
}



bool ReservoirConformalMeshEbE::simplifyJRight( )
{
    if (_numCellsJ >= _numCellsRegularJ)
    {
        return false;
    }

    std::vector<int> sliceIndex( _numCellsRegularJ, 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _numCellsRegularJ; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _numCellsRegularJ - 1 );
        double num = ( _numCellsJ - 2 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _numCellsRegularJ - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _numCellsRegularJ; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    int finalSlice = _outputGrid.Nj( );
    int value = sliceIndex.back( );
    for (int i = _outputGrid.Nj( ), j = _numCellsRegularJ - 1; j >= 0; i--, j--)
    {
        if (sliceIndex[j] != value)
        {
            value = sliceIndex[j];
            _outputGrid.EraseSliceJ( i, finalSlice );

            finalSlice = i;
        }
    }

    return true;
}



bool ReservoirConformalMeshEbE::simplifyJLeft( )
{
    if (_numCellsJ >= _numCellsRegularJ)
    {
        return false;
    }

    std::vector<int> sliceIndex( _numCellsRegularJ, 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _numCellsRegularJ; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _numCellsRegularJ - 1 );
        double num = ( _numCellsJ - 1 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _numCellsRegularJ - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _numCellsRegularJ; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    std::reverse( sliceIndex.begin( ), sliceIndex.end( ) );

    int lastJSlice = _numCellsRegularJ - 1;
    int finalSlice = lastJSlice;

    int value = sliceIndex.back( );
    for (int i = _numCellsRegularJ - 1; i >= 0; i--)
    {
        if (sliceIndex[i] != value)
        {
            value = sliceIndex[i];
            int init = lastJSlice - _numCellsRegularJ + i + 1;
            _outputGrid.EraseSliceJ( init, finalSlice );

            finalSlice = init;
        }
    }
    int init = 0;
    _outputGrid.EraseSliceJ( init, finalSlice );
    return true;
}



bool ReservoirConformalMeshEbE::simplifyBottomReservoir( int ridx, const std::vector<int>& idxs )
{
    int s = ridx - 1;
    int numFinalSlices = _slices[s];

    if (numFinalSlices >= _slicesRegular[s])
    {
        return false;
    }

    std::vector<int> sliceIndex( _slicesRegular[s], 0 );

    //Verify where the slices will be on.
    for (int i = 0; i < _slicesRegular[s]; i++)
    {
        //k(i) = sqrt(i * (kmax + e)^2/ smax).
        double eps = 0.49 * i / ( _slicesRegular[s] - 1 );
        double num = ( numFinalSlices - 1 + eps );

        sliceIndex[i] = ( int ) ( sqrt( i * num * num / ( _slicesRegular[s] - 1 ) ) + 0.5 );
    }

    //Fix the vector. Sometimes there is a difference greater than 1 unit between
    //two consecutive vector positions.
    for (int i = 1; i < _slicesRegular[s]; i++)
    {
        if (sliceIndex[i] - sliceIndex[i - 1] > 1)
        {
            sliceIndex[i] = sliceIndex[i - 1] + 1;
        }
    }

    std::reverse( sliceIndex.begin( ), sliceIndex.end( ) );

    int lastSlice = idxs[s + 1];

    int value = sliceIndex.back( );
    for (int i = _slicesRegular[s] - 1; i >= 0; i--)
    {
        if (sliceIndex[i] != value)
        {
            value = sliceIndex[i];
            int init = idxs[s + 1] - _slicesRegular[s] + 1 + i;
            _outputGrid.EraseSliceK( init, lastSlice );

            lastSlice = init;
        }
    }
    int init = idxs[s];
    _outputGrid.EraseSliceK( init, lastSlice );
    return true;
}



bool ReservoirConformalMeshEbE::simplifyKSlices( )
{
    std::vector<int> idxs( _slicesRegular.size( ) + 1, 0 );

    //Get the reservoir index.
    int ridx = -1;
    for (unsigned int i = 0; i < _slicesRegular.size( ); i++)
    {
        idxs[i + 1] = idxs[i] + _slicesRegular[i];
        if (idxs[i + 1] > _reservoirK && ridx == -1)
        {
            ridx = ( int ) i;
        }
    }

    for (int i = ( int ) _slicesRegular.size( ) - 1; i > ridx + 1; i--)
    {
        simplifySlice( i, idxs );
    }

    simplifyTopReservoir( ridx, idxs );

    //Simplify the reservoir bottom.
    simplifyBottomReservoir( ridx, idxs );

    //Simplify the bottom.
    for (int i = ridx - 2; i >= 0; i--)
    {
        simplifySlice( i, idxs );
    }

    _reservoirK = 0;
    for (int i = 0; i < ridx - 1; i++)
    {
        _reservoirK += _slices[i];
    }
    return true;
}



bool ReservoirConformalMeshEbE::simplifyISlices( )
{
    simplifyIRight( );

    //Simplify the reservoir bottom.
    simplifyILeft( );

    return true;
}



bool ReservoirConformalMeshEbE::simplifyJSlices( )
{
    simplifyJRight( );

    //Simplify the reservoir bottom.
    simplifyJLeft( );

    return true;
}



bool ReservoirConformalMeshEbE::simplifyMesh( )
{
    simplifyKSlices( );
    simplifyJSlices( );
    simplifyISlices( );
    _numCellsRegularI = _numCellsI;
    _numCellsRegularJ = _numCellsJ;
}



ReservoirConformalMeshEbE::~ReservoirConformalMeshEbE( )
{
}

