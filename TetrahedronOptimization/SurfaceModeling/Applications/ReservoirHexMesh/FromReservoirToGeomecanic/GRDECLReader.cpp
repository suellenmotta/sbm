/* 
 * File:   GRDECLReader.cpp
 * Author: jcoelho
 * 
 * Created on August 28, 2017, 2:10 PM
 */
#include <fstream>
#include "GRDECLReader.h"
#include <map>
#include <set>


unsigned int cellFaceToVertex[6][4] = {
    {0, 1, 3, 2 },
    {4, 5, 7, 6 },
    {1, 5, 7, 3 },
    {0, 4, 6, 2 },
    {0, 4, 5, 1 },
    {2, 6, 7, 3 }
};



GRDECLReader::GRDECLReader( )
{
    _numActiveCells = 0;
    _ni = _nj = _nk = 0;
}



const DsGrid3D<CellMesh>& GRDECLReader::getGridMesh( ) const
{
    return _mesh;
}



const std::vector<Point3D>& GRDECLReader::getGeometry( ) const
{
    return _coordinates;
}



const std::vector<Property>& GRDECLReader::getProperties( ) const
{
    return _properties;
}



int GRDECLReader::getNumActiveCells( ) const
{
    return _numActiveCells;
}



bool GRDECLReader::read( const std::string & path )
{
    //A vector to store the keywords and your positions. 
    std::vector< KeywordAndFilePos > keywordsAndFilePos;

    //Find for keywords and your positions on file.
    findKeywordsOnFile( path, keywordsAndFilePos );

    //Read the reservoir geometry.
    readGeometry( keywordsAndFilePos, path );

    //Read the properties.
    readProperties( keywordsAndFilePos, path );

    //Avoid repeated vertices.
    preprocessMesh( );
}



void GRDECLReader::findKeywordsOnFile( const std::string &fileName, std::vector< KeywordAndFilePos >& keywords )
{
    std::ifstream data( fileName.c_str( ) );
    if (!data)
    {
        printf( "Error oppening %s\n", fileName.c_str( ) );
        return;
    }

    std::string line;

    //Read all line.
    while (std::getline( data, line ))
    {
        //Verify if the line starts with a letter.
        if (line.size( ) && std::isalpha( line[0] ))
        {
            KeywordAndFilePos keyPos;

            //Get the position on file.
            long long l = data.tellg( );

            //Get the position of the first letter.
            keyPos.filePos = l - line.size( );

            size_t first = line.find_first_not_of( " \r\n\t" );
            size_t last = line.find_last_not_of( " \r\n\t" );

            //Extract the keyword.
            keyPos.keyword = line.substr( first, ( last - first + 1 ) );

            //Save the keyword and position.
            keywords.push_back( keyPos );
        }
    }
}



void GRDECLReader::findGridKeywordPositions( const std::vector< KeywordAndFilePos >& keywordsAndFilePos,
                                             long long& coordPos, long long& zcornPos, long long& specgridPos,
                                             long long& actnumPos, long long& mapaxesPos )
{
    for (unsigned int i = 0; i < keywordsAndFilePos.size( ); i++)
    {
        if (keywordsAndFilePos[i].keyword == "COORD")
        {
            coordPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "ZCORN")
        {
            zcornPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "SPECGRID")
        {
            specgridPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "ACTNUM")
        {
            actnumPos = keywordsAndFilePos[i].filePos;
        }
        else if (keywordsAndFilePos[i].keyword == "MAPAXES")
        {
            mapaxesPos = keywordsAndFilePos[i].filePos;
        }
    }
}



const std::vector<std::string>& GRDECLReader::invalidPropertyDataKeywords( )
{

    static std::vector<std::string> keywords;
    static bool isInitialized = false;
    if (!isInitialized)
    {
        // Related to geometry
        keywords.push_back( "COORD" );
        keywords.push_back( "ZCORN" );
        keywords.push_back( "SPECGRID" );
        keywords.push_back( "MAPAXES" );
        keywords.push_back( "FAULTS" );
        keywords.push_back( "MAPUNITS" );
        keywords.push_back( "GRIDUNIT" );
        keywords.push_back( "COORDSYS" );

        isInitialized = true;
    }
    return keywords;
}



bool GRDECLReader::isValidDataKeyword( const std::string & keyword )
{
    const std::vector<std::string>& keywordsToSkip = invalidPropertyDataKeywords( );
    for (const std::string keywordToSkip : keywordsToSkip)
    {
        if (keywordToSkip == keyword)
        {
            return false;
        }
    }

    return true;
}



int GRDECLReader::findOrCreateResult( const std::string & newResultName )
{
    int resultIndex = -1;
    for (int i = 0; i < _properties.size( ); i++)
    {
        if (newResultName == _properties[i]._name)
        {
            return i;
        }
    }

    return resultIndex;
}



void GRDECLReader::preprocessMesh( )
{
    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                CellMesh& c = _mesh.GetRef( i, j, k );

                //(i, j, k + 1)
                if (checkFaces( i, j, k, i, j, k + 1, 1, 0 ))
                {
                    CellMesh& nc = _mesh.GetRef( i, j, k + 1 );

                    c.setAdjacent( 1, _mesh.GetIndex( i, j, k + 1 ) );
                    nc.setAdjacent( 0, _mesh.GetIndex( i, j, k ) );
                }

                //(i, j + 1, k)
                if (checkFaces( i, j, k, i, j + 1, k, 5, 4 ))
                {
                    CellMesh& nc = _mesh.GetRef( i, j + 1, k );

                    c.setAdjacent( 5, _mesh.GetIndex( i, j + 1, k ) );
                    nc.setAdjacent( 4, _mesh.GetIndex( i, j, k ) );
                }

                //(i + 1, j, k)
                if (checkFaces( i, j, k, i + 1, j, k, 2, 3 ))
                {
                    CellMesh& nc = _mesh.GetRef( i + 1, j, k );

                    c.setAdjacent( 2, _mesh.GetIndex( i + 1, j, k ) );
                    nc.setAdjacent( 3, _mesh.GetIndex( i, j, k ) );
                }
            }
        }
    }

    int faultAux[6][3] = {
        {0, 0, -1 },
        {0, 0, +1 },
        {+1, 0, 0 },
        {-1, 0, 0 },
        {0, -1, 0 },
        {0, +1, 0 }
    };

    // The indexing conventions for vertices in ECLIPSE
    //
    //      2-------------3              
    //     /|            /|                  
    //    / |           / |               /j   
    //   /  |          /  |              /     
    //  0-------------1   |             *---i  
    //  |   |         |   |             | 
    //  |   6---------|---7             |
    //  |  /          |  /              |k
    //  | /           | /
    //  |/            |/
    //  4-------------5
    //  vertex indices

    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                CellMesh& c = _mesh.GetRef( i, j, k );
                c.setFault( false );
                if (c.active( ))
                {
                    for (int f = 0; f < 6; f++)
                    {
                        if (c.adjacent( f ) == -1)
                        {
                            int ai = i + faultAux[f][0];
                            int aj = j + faultAux[f][1];
                            int ak = k + faultAux[f][2];

                            if (ai >= 0 && ai < _ni &&
                                aj >= 0 && aj < _nj &&
                                ak >= 0 && ak < _nk)
                            {
                                if (_mesh.GetRef( ai, aj, ak ).active( ))
                                {
                                    c.setFault( true );
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}



bool GRDECLReader::checkFaces( int i, int j, int k, int ni, int nj, int nk, int f, int nf )
{
    if (!( ni < _ni && nj < _nj && nk < _nk ))
    {
        return false;
    }

    const CellMesh& c = _mesh.GetRef( i, j, k );
    const CellMesh& nc = _mesh.GetRef( ni, nj, nk );

    for (unsigned int p = 0; p < 4; p++)
    {
        unsigned int idx = c.point( cellFaceToVertex[f][p] );
        unsigned int nidx = nc.point( cellFaceToVertex[nf][p] );
        if (_coordinates[idx] != _coordinates[nidx])
        {
            return false;
        }
    }
    return true;
}



bool GRDECLReader::readDataPropertyFromKeyword( ecl_kw_type* eclipseKeywordData, const std::string & resultName )
{
    int numCells = _mesh.Size( );
    int numActiveCells = _numActiveCells;

    bool mathingItemCount = false;
    {
        size_t itemCount = static_cast < size_t > ( ecl_kw_get_size( eclipseKeywordData ) );
        if (itemCount == numCells)
        {
            mathingItemCount = true;
        }
        if (itemCount == numActiveCells)
        {
            mathingItemCount = true;
        }
    }

    if (!mathingItemCount)
        return false;

    int resultIndex = findOrCreateResult( resultName );
    if (resultIndex < 0)
        return false;

    std::vector< double >& newPropertyData = _properties[resultIndex]._p;

    newPropertyData.resize( ecl_kw_get_size( eclipseKeywordData ), 1e100 );
    ecl_kw_get_data_as_double( eclipseKeywordData, newPropertyData.data( ) );

    return true;
}



void GRDECLReader::allocateProperties( const std::vector< KeywordAndFilePos >& fileKeywords )
{
    for (unsigned int i = 0; i < fileKeywords.size( ); ++i)
    {
        if (isValidDataKeyword( fileKeywords[i].keyword ))
        {
            Property p;
            p._name = fileKeywords[i].keyword;
            _properties.push_back( p );
        }
    }
}



bool GRDECLReader::readProperties( const std::vector< KeywordAndFilePos >& fileKeywords, const std::string & fileName )
{
    //Allocate properties on model.
    allocateProperties( fileKeywords );

    //Open file.
    FILE* gridFilePointer = fopen( fileName.c_str( ), "r" );

    if (!gridFilePointer || !fileKeywords.size( ))
    {
        return false;
    }

    for (unsigned int i = 0; i < fileKeywords.size( ); ++i)
    {
        //Check if the world represents a property.
        if (!isValidDataKeyword( fileKeywords[i].keyword ))
            continue;

        //Move to correct position on file.
        fseek( gridFilePointer, fileKeywords[i].filePos, SEEK_SET );

        //Read the data.
        ecl_kw_type* eclipseKeywordData = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) );

        //Check if the data is correct.
        if (eclipseKeywordData)
        {
            readDataPropertyFromKeyword( eclipseKeywordData, fileKeywords[i].keyword );
            ecl_kw_free( eclipseKeywordData );
        }
    }

    fclose( gridFilePointer );
    return true;
}



bool GRDECLReader::readGeometry( const std::vector< KeywordAndFilePos >& keywordsAndFilePos, const std::string & fileName )
{
    long long coordPos = -1;
    long long zcornPos = -1;
    long long specgridPos = -1;
    long long actnumPos = -1;
    long long mapaxesPos = -1;

    findGridKeywordPositions( keywordsAndFilePos, coordPos, zcornPos, specgridPos, actnumPos, mapaxesPos );

    if (coordPos < 0 || zcornPos < 0 || specgridPos < 0)
    {
        printf( "Error! Invalid grid\n" );
        return false;
    }

    FILE* gridFilePointer = fopen( fileName.c_str( ), "r" );
    if (!gridFilePointer)
        return false;


    ecl_kw_type* specGridKw = NULL;
    ecl_kw_type* zCornKw = NULL;
    ecl_kw_type* coordKw = NULL;
    ecl_kw_type* actNumKw = NULL;
    ecl_kw_type* mapAxesKw = NULL;

    // Try to read all the needed keywords. Early exit if some are not found
    bool allKwReadOk = true;

    fseek( gridFilePointer, specgridPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( specGridKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_INT_TYPE ) ) );

    fseek( gridFilePointer, zcornPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( zCornKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) ) );

    fseek( gridFilePointer, coordPos, SEEK_SET );
    allKwReadOk = allKwReadOk && NULL != ( coordKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) ) );

    // If ACTNUM is not defined, this pointer will be NULL, which is a valid condition
    if (actnumPos >= 0)
    {
        fseek( gridFilePointer, actnumPos, SEEK_SET );
        allKwReadOk = allKwReadOk && NULL != ( actNumKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_INT_TYPE ) ) );
    }

    // If MAPAXES is not defined, this pointer will be NULL, which is a valid condition
    if (mapaxesPos >= 0)
    {
        fseek( gridFilePointer, mapaxesPos, SEEK_SET );
        mapAxesKw = ecl_kw_fscanf_alloc_current_grdecl__( gridFilePointer, false, ecl_type_create_from_type( ECL_FLOAT_TYPE ) );
    }

    if (!allKwReadOk)
    {
        if (specGridKw) ecl_kw_free( specGridKw );
        if (zCornKw) ecl_kw_free( zCornKw );
        if (coordKw) ecl_kw_free( coordKw );
        if (actNumKw) ecl_kw_free( actNumKw );
        if (mapAxesKw) ecl_kw_free( mapAxesKw );

        fclose( gridFilePointer );

        return false;
    }

    _ni = ecl_kw_iget_int( specGridKw, 0 );
    _nj = ecl_kw_iget_int( specGridKw, 1 );
    _nk = ecl_kw_iget_int( specGridKw, 2 );

    ecl_grid_type* inputGrid = ecl_grid_alloc_GRDECL_kw( _ni, _nj, _nk, zCornKw, coordKw, actNumKw, mapAxesKw );



    // The indexing conventions for vertices in ECLIPSE
    //
    //      2-------------3              
    //     /|            /|                  
    //    / |           / |               /j   
    //   /  |          /  |              /     
    //  0-------------1   |             *---i  
    //  |   |         |   |             | 
    //  |   6---------|---7             |
    //  |  /          |  /              |k
    //  | /           | /
    //  |/            |/
    //  4-------------5
    //  vertex indices


    _mesh.Resize( _ni, _nj, _nk );
    _coordinates.reserve( 8 * _ni * _nj * _nk );

    int cellPointIndex[8];

    int countPoints = 0;

    for (int k = 0; k < _nk; k++)
    {
        for (int j = 0; j < _nj; j++)
        {
            for (int i = 0; i < _ni; i++)
            {
                CellMesh& c = _mesh.GetRef( i, j, k );

                //Define if a cell is active or not.
                int active = ecl_grid_get_active_index3( inputGrid, i, j, k );
                if (active != -1)
                {
                    _numActiveCells++;
                    c.active( true );
                }

                //Get the geometry.
                for (int v = 0; v < 8; ++v)
                {
                    Point3D pt;
                    ecl_grid_get_cell_corner_xyz3( inputGrid, i, j, k, v, &pt.x[0], &pt.x[1], &pt.x[2] );
                    pt.x[2] *= -1;
                    _coordinates.push_back( pt );

                    cellPointIndex[v] = countPoints;
                    countPoints++;
                }

                c.setPoints( cellPointIndex );
            }
        }
    }

    fclose( gridFilePointer );

    return true;
}



void GRDECLReader::writeNeutralFile( const std::string & name )
{

    unsigned int numberNodes = _coordinates.size( );
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << name << std::endl;
        return;
    }

    out << "%HEADER\n"
        "%HEADER.VERSION\n"
        "'Aug/17'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Converted from GRDECL format'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numberNodes << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numberNodes << std::endl << std::endl;
    int idx = 1;
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        out << idx << " " << _coordinates[i] << std::endl;
        idx++;
    }

    out << "%ELEMENT" << std::endl;
    out << _mesh.Size( ) << std::endl << std::endl;

    idx = 1;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << _mesh.Size( ) << std::endl << std::endl;

    //Vector to translate from eclipse format to neutral file format.
    int grdeclToNF[8] = { 4, 0, 5, 1, 7, 3, 6, 2 };

    for (int i = 0; i < _mesh.Size( ); i++)
    {
        out << idx << " 0 1 ";
        const CellMesh& c = _mesh.GetRef( i );
        for (unsigned int v = 0; v < 8; v++)
        {
            int p = grdeclToNF[v];
            out << c.point( p ) + 1 << " ";
        }
        out << std::endl;
        idx++;
    }
    out << std::endl;

    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


    out << "%RESULT" << std::endl;
    out << 1 << std::endl;
    out << 1 << " " << "'Mesh'" << std::endl << std::endl;

    out << "%RESULT.CASE" << std::endl;
    out << 1 << " " << 1 << std::endl;
    out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP" << std::endl;
    out << 1 << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.TIME" << std::endl;
    out << "0.000000" << std::endl << std::endl;

    out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR" << std::endl;
    out << _properties.size( ) << std::endl;
    if (_properties.size( ))
    {
        std::string propertiesString;

        for (unsigned int i = 0; i < _properties.size( ); i++)
        {
            propertiesString += "'" + _properties[i]._name + "' ";
        }
        out << propertiesString << std::endl << std::endl;

        out << "%RESULT.CASE.STEP.ELEMENT.GAUSS.SCALAR.DATA" << std::endl;

        out << _mesh.Size( ) << std::endl;

        idx = 1;
        for (unsigned int i = 0; i < _mesh.Size( ); i++)
        {
            out << idx << " 1" << std::endl;
            for (unsigned int p = 0; p < _properties.size( ); p++)
            {
                out << _properties[p]._p[i] << " ";
            }
            out << std::endl;
            idx++;
        }
    }
    out << "%END" << std::endl;
}



GRDECLReader::~GRDECLReader( )
{
}

