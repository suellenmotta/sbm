/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReservoirHorizons.h
 * Author: jcoelho
 *
 * Created on February 23, 2017, 4:29 PM
 */

#ifndef RESERVOIRHORIZONS_H
#define RESERVOIRHORIZONS_H
#include <string>
#include <vector>
#include "../Point3D.h"
#include "../../../../../libs/DataStructures/Topologicals/CornerTable/CornerTable.h"

class ReservoirHorizon
{
public:
    /**
     * Default constructor.
     */
    ReservoirHorizon( );

    /**
     * Get the point with the average of coordinates.
     * @return point with the average of coordinates.
     */
    double getAverageZ( ) const;

    /**
     * Get the max value of z.
     * @return - the max value of z.
     */
    double getMaxZ( ) const;

    /**
     * Get the min value of z.
     * @return - the min value of z.
     */
    double getMinZ( ) const;


    /**
     * Get horizon coordinates.
     * @return - horizon coordinates.
     */
    const std::vector<Point3D>& getCoordinates( ) const;

    /**
     * Get horizon name.
     */
    std::string getName( ) const;

    /**
     * Get triangle normal.
     * @param t - triangle to get the normal.
     * @return - the normal.
     */
    Point3D getNormal( CornerType t ) const;

    /**
     * Get the number of nodes on horizon.
     * @return - number of nodes on horizon.
     */
    unsigned int getNumberNodes( ) const;

    /**
     * Get the number of elements on horizon.
     * @return - number of elements on horizon.
     */
    CornerType getNumberElements( ) const;

    /**
     * Get the triangle that contains the coordinate (x, y).
     * @param (x, y) - coordinates to search for.
     * @return - the triangle index that contains the coordinate (x, y).
     */
    CornerType getTriangle( double x, double y );

    /**
     * Get the triangle list.
     * @return - triangle list.
     */
    const CornerType* getTriangleList( ) const;

    /**
     * Preprocess the data: translate the data, compute the triangulation, and
     * normals.
     * @param tx - x translation.
     * @param ty - y translation.
     * @param rotationAngle - angle to rotate the data.
     */
    void preprocess( const double tx = 0, const double ty = 0, const double rotationAngle = 0 );

    /**
     * Read a horizon.
     * @param path - horizon path file.
     * @return - true if everything is okay.
     */
    bool readFile( const std::string& path );

    /**
     * Rotate the horizon points.
     * @param rotationAgle - angle to rotate.
     */
    void rotateData( const double rotationAgle );

    /**
     * Translate the data to turn the data at the same reservoir coordinate
     * system.
     * @param tx - x translation.
     * @param ty - y translation.
     */
    void translateData( const double tx, const double ty );

    /**
     * Write the mesh on a POS 3d file format.
     * @param path - path to write the file.
     * @return - true if everything was okay.
     */
    bool writePos3DFile( const std::string& path );

    /**
     * Destructor.
     */
    virtual ~ReservoirHorizon( );
private:

    /**
     * Compute the angle at vertex p1.
     * @param p1, p2, p3 - triangle vertices.
     * @return - angle at vertex p1.
     */
    double computeAngle( const Point3D& p1, const Point3D& p2, const Point3D& p3 );

    /**
     * Compute the coordinates average.
     */
    void computeAveragePoint( );

    /**
     * Rotate a single point.
     * @param p - point to be rotated.
     * @param rotationAgle - angle to rotate.
     */
    void rotatePoint( Point3D& p, const double rotationAngle );

    /**
     * Triangulate the points using a Delaunay Triangulation.
     */
    void triangulate( );

    /**
     * Verify if the triangle is a valid triangle, e.i, the minimum angle is
     * greater then a limit.
     * @param v1, v2, v3 - triangle vertices index.
     * @param limit - max angle allowed.
     * @return - true if it is a valid triangle and false otherwise.
     */
    bool validTriangle( const int v1, const int v2, const int v3, const double limit = 10.0 );
private:

    /**
     * Horizon coordinates.
     */
    std::vector<Point3D> _coordinates;

    /**
     * Topological data structure to represent the triangle mesh.
     */
    CornerTable* _mesh;

    /**
     * Min and max values for z on horizon.
     */
    double _minZ, _maxZ;

    /**
     * Horizon name.
     */
    std::string _name;

    /**
     * Point normals.
     */
    std::vector<Point3D> _normals;

    /**
     * Average of horizon coordinates.
     */
    double _z;
};

#endif /* RESERVOIRHORIZONS_H */

