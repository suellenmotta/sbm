/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReservoirHorizons.cpp
 * Author: jcoelho
 * 
 * Created on February 23, 2017, 4:29 PM
 */

#include <fstream>
#include <cmath>
#include <set>
#include "ReservoirHorizon.h"
#include "../../../../../libs/Triangle/Triangle.h"
using namespace std;
#ifndef M_PI
#define M_PI  3.14159265358979323846
#endif



ReservoirHorizon::ReservoirHorizon( )
{
    _maxZ = _minZ = _z = 0.0;
    _mesh = 0;
    srand( time( 0 ) );
}



void ReservoirHorizon::preprocess( const double tx, const double ty, const double rotationAngle )
{
    translateData( tx, ty );
    rotateData( rotationAngle );
    triangulate( );
}



bool ReservoirHorizon::writePos3DFile( const std::string& path )
{
    //Get triangle list information.
    const CornerType numTriangles = _mesh->getNumTriangles( );
    const CornerType *triangleList = _mesh->getTriangleList( );

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        cout << "Falha ao abrir o arquivo " << path << endl;
        return false;
    }

    out << "%HEADER\n"
        "Neutral file created by ESCOLHERNOME program\n\n"

        "%HEADER.VERSION\n"
        "'Jun/16'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Reservatorio'\n" << endl;

    out << "%NODE" << endl;
    out << _coordinates.size( ) << endl << endl;

    out << "%NODE.COORD\n";
    out << _coordinates.size( ) << endl << endl;

    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        out << i + 1 << " " << _coordinates[i] << endl;
    }
    out << endl;
    out << "%ELEMENT" << endl;
    out << numTriangles << endl << endl;

    out << "%ELEMENT.T3" << endl;
    out << numTriangles << endl << endl;
    for (unsigned int i = 0; i < numTriangles; i++)
    {
        out << i + 1 << " 0 0 0 ";
        out << triangleList[3 * i + 0] + 1 << " " << triangleList[3 * i + 1] + 1 << " " << triangleList[3 * i + 2] + 1 << endl;
    }

    out << endl;
    out << "%RESULT" << endl;
    out << 1 << endl;
    out << 1 << " " << "'Mesh'" << endl << endl;

    out << "%RESULT.CASE" << endl;
    out << 1 << " " << 1 << endl;
    out << 1 << " " << "'Mesh1'" << endl << endl;

    out << "%RESULT.CASE.STEP" << endl;
    out << 1 << endl << endl;

    out << "%RESULT.CASE.STEP.TIME" << endl;
    out << "0.000000" << endl << endl;

    out << "%RESULT.CASE.STEP.FACTOR" << endl;
    out << "0.000000" << endl << endl;

    out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << endl;
    out << 0 << endl;

    out << "%END" << endl;

    return true;
}



bool ReservoirHorizon::readFile( const std::string& path )
{
    std::ifstream in( path.c_str( ) );
    if (!in)
    {
        cout << "Falha ao abrir o arquivo " << path << endl;
        return false;
    }

    _name = path;

    std::set<Point3D> input;

    Point3D p;
    while (in >> p)
    {
        input.insert( p );
    }

    _coordinates.insert( _coordinates.begin( ), input.begin( ), input.end( ) );
    return true;
}



double ReservoirHorizon::getAverageZ( ) const
{
    return _z;
}



double ReservoirHorizon::getMaxZ( ) const
{
    return _maxZ;
}



double ReservoirHorizon::getMinZ( ) const
{
    return _minZ;
}



std::string ReservoirHorizon::getName( ) const
{
    return _name;
}



Point3D ReservoirHorizon::getNormal( CornerType t ) const
{
    return _normals[t];
}



unsigned int ReservoirHorizon::getNumberNodes( ) const
{
    return _coordinates.size( );
}



CornerType ReservoirHorizon::getNumberElements( ) const
{
    return _mesh->getNumTriangles( );
}



CornerType ReservoirHorizon::getTriangle( double x, double y )
{
    const CornerType *triangleList = getTriangleList( );
    CornerType numTriangles = _mesh->getNumTriangles( );

    Point3D p( x, y, 0 );

    for (CornerType t = 0; t < numTriangles; t++)
    {
        //Get vertices index.
        CornerType v0 = triangleList[3 * t + 0];
        CornerType v1 = triangleList[3 * t + 1];
        CornerType v2 = triangleList[3 * t + 2];

        //Get the triangle coordinates.
        Point3D tp[3] = { _coordinates[v0], _coordinates[v1], _coordinates[v2] };
        int cont = 0;

        //Get the possible edges to cross.
        for (int i = 0; i < 3; i++)
        {
            Point3D v = tp[ ( i + 2 ) % 3] - tp[( i + 1 ) % 3];
            Point3D w = p - tp[( i + 1 ) % 3];
            if (( v ^ w )[2] < 0)
            {
                cont++;
            }
        }
        if (cont == 0)
            return t;
    }
    return CornerTable::BORDER_CORNER;
}



double ReservoirHorizon::computeAngle( const Point3D& p1, const Point3D& p2, const Point3D& p3 )
{
    Point3D v1 = p2 - p1;
    Point3D v2 = p3 - p1;

    double norm1 = sqrt( v1 * v1 );
    double norm2 = sqrt( v2 * v2 );

    if (norm1 < Point3D::EPS || norm2 < Point3D::EPS)
        return 0;

    double cosTheta = ( v1 * v2 ) / ( norm1 * norm2 );

    if (cosTheta < -1 || cosTheta > 1)
        return 0;

    return 180.0 * acos( cosTheta ) / M_PI;
}



void ReservoirHorizon::computeAveragePoint( )
{
    _z = 0.0;
    _minZ = _coordinates[0][2];
    _maxZ = _minZ;
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        _z += _coordinates[i][2];
        _minZ = std::min( _minZ, ( double ) _coordinates[i][2] );
        _maxZ = std::max( _maxZ, ( double ) _coordinates[i][2] );
    }
    _z /= _coordinates.size( );
}



const std::vector<Point3D>& ReservoirHorizon::getCoordinates( ) const
{
    return _coordinates;
}



const CornerType* ReservoirHorizon::getTriangleList( ) const
{
    return _mesh->getTriangleList( );
}



void ReservoirHorizon::rotatePoint( Point3D& p, const double rotationAngle )
{
    double x = p[0], y = p[1];
    p[0] = x * cos( rotationAngle ) - y * sin( rotationAngle );
    p[1] = x * sin( rotationAngle ) + y * cos( rotationAngle );
}



void ReservoirHorizon::rotateData( const double rotationAgle )
{
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        rotatePoint( _coordinates[i], rotationAgle );
    }
}



void ReservoirHorizon::translateData( const double tx, const double ty )
{
    Point3D t( tx, ty, 0 );
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        _coordinates[i] += t;
    }

    computeAveragePoint( );
}



bool ReservoirHorizon::validTriangle( const int v1, const int v2, const int v3, const double limit )
{
    //obtem pontos
    Point3D p1 = _coordinates[v1];
    Point3D p2 = _coordinates[v2];
    Point3D p3 = _coordinates[v3];

    p1[2] = p2[2] = p3[2] = 0.0;

    double angle = computeAngle( p1, p2, p3 );
    if (angle < limit)
        return false;

    angle = computeAngle( p2, p1, p3 );
    if (angle < limit)
        return false;

    angle = computeAngle( p3, p2, p1 );
    if (angle < limit)
        return false;

    return true;
}



void ReservoirHorizon::triangulate( )
{
    std::vector<unsigned int> triangleList;

    //Construct a vector with 2D coordinates.
    unsigned int numPoints = _coordinates.size( );
    double *xyCoordinates = new double[2 * numPoints];

    for (unsigned int i = 0; i < numPoints; i++)
    {
        xyCoordinates[2 * i + 0] = _coordinates[i][0];
        xyCoordinates[2 * i + 1] = _coordinates[i][1];
    }

    struct geom::triangulateio tri_in, tri_out;

    //Set input information.
    tri_in.pointlist = xyCoordinates;
    tri_in.numberofpoints = numPoints;
    tri_in.numberofpointattributes = 0;
    tri_in.pointmarkerlist = NULL;
    tri_in.pointattributelist = NULL;

    //Set output information.
    tri_out.pointlist = xyCoordinates;
    tri_out.trianglelist = NULL;
    tri_out.neighborlist = NULL;
    tri_out.numberoftriangles = 0;
    tri_out.segmentlist = NULL;
    tri_out.edgelist = NULL;

    //Triangulation parameters.
    char switches[] = "zNQ";

    //Delaunay triangulation.
    try
    {
        geom::triangulate( switches, &tri_in, &tri_out, NULL );
    }
    catch (...)
    {
        return;
    }

    //Get the triangulation.
    for (int i = 0; i < tri_out.numberoftriangles; i++)
    {
        unsigned int v1 = tri_out.trianglelist[3 * i + 0];
        unsigned int v2 = tri_out.trianglelist[3 * i + 1];
        unsigned int v3 = tri_out.trianglelist[3 * i + 2];
        if (validTriangle( v1, v2, v3 ))
        {
            triangleList.push_back( v1 );
            triangleList.push_back( v2 );
            triangleList.push_back( v3 );
        }
    }

    delete [] tri_out.trianglelist;

    tri_out.trianglelist = 0;

    delete [] xyCoordinates;

    //Build corner table.
    _mesh = new CornerTable( &triangleList[0], 0, triangleList.size( ) / 3, _coordinates.size( ), 0 );
}



ReservoirHorizon::~ReservoirHorizon( )
{
    delete _mesh;
}

