/* 
 * File:   WriteOutputFiles.cpp
 * Author: jcoelho
 * 
 * Created on January 25, 2018, 4:58 PM
 */
#include <fstream>
#include "WriteOutputFiles.h"



WriteOutputFiles::WriteOutputFiles( ReservoirConformalMeshEbE* m, std::string poroPressureFile )
{
    _reservoir = m;
    std::ifstream in( poroPressureFile.c_str( ) );
    if (!in)
    {
        printf( "Error opening %s\n", poroPressureFile.c_str( ) );
    }

    std::string aux;
    in >> aux >> aux >> aux;
    in >> _numSteps;
    in >> aux >> aux >> aux;
    in >> _numCells;

    //Allocate memory to storage data.
    _cells.Resize( _reservoir->getGrid( ).Ni( ) - 1, _reservoir->getGrid( ).Nj( ) - 1, _reservoir->getGrid( ).Nk( ) - 1 );
    for (unsigned int i = 0; i < _cells.Size( ); i++)
    {
        _cells.GetRef( i )._poroPressure.resize( _numSteps, 0 );
    }

    int sf = 6;
    for (int i = 0; i < sf; i++)
    {
        _steps.push_back( (int)(i  * ( (_numSteps - 1.0) / ( sf - 1 ) ) + 0.5));
    }

    for (int s = 0; s < _numSteps; s++)
    {
        in >> aux >> aux >> aux >> aux;
        in >> aux >> aux >> aux >> aux;
        for (int c = 0; c < _numCells; c++)
        {
            int i, j, k;
            in >> i >> j >> k;
            i--;
            j--;
            k--;
            
            int oi, oj, ok;
            _reservoir->computeFinalOutputIndexFromReservoir( i, j, k, oi, oj, ok );

            in >> _cells.GetRef( oi, oj, ok )._poroPressure[s];
            _cells.GetRef( oi, oj, ok )._poroPressure[s] *= 100;
            _cells.GetRef( oi, oj, ok )._active = true;
        }
    }

    _numThreads = 10;
    _numberOfGaussPoints = 8;
    _overburdenStressGradient = 22;
    _horizontalStressRatioX = 0.7;
    _horizontalStressRatioY = 0.8;

    _materialYoung[0] = 138000;
    _materialYoung[1] = 68000;
    _materialYoung[2] = 68000;
    _materialYoung[3] = 68000;
    _materialYoung[4] = 207000;

    _materialPoisson[0] = 0.3;
    _materialPoisson[1] = 0.25;
    _materialPoisson[2] = 0.25;
    _materialPoisson[3] = 0.25;
    _materialPoisson[4] = 0.3;

    for (int i = 0; i < 4; i++)
    {
        _stressGradient[i] = 0.0;
    }

    _referenceLevel = 0;
    _poroReferenceLevel = 0;
    _poroGradient = 10.0;
}



bool WriteOutputFiles::writeFiles( std::string name ) const
{
    writeDatFile( name + ".dat" );
    writePostMshFile( name + ".post.msh" );
    writePrpFile( name + ".prp" );
    writePpiFile( name + ".ppi" );
    writeSigFile( name + ".sig" );
    writeBcoFile( name + ".bco" );
}



bool WriteOutputFiles::writeDatFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "AUTORUN: YES\n";
    out << "POS3D: YES\n";
    out << "==================================================================================================================================\n";
    out << "GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "Author: Multiphysics Modeling and Simulation Group\n";
    out << "TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%GENERAL_DATA\n";
    out << "PROBLEM_TYPE: MECHANICAL\n";
    out << "ANALYSIS_TYPE: 3D\n";
    out << std::endl;
    out << "   NPOIN   NELEM   NMATS   NDIME  NNOMAX  USEGRA  NELINT   GRAVITY\n";
    out << "-------+-------+-------+-------+-------+-------+-------+---------+\n";
    out << "     " << _reservoir->getNumPoints( ) << "     " << _reservoir->getNumCells( ) << "       5       3       8       0       0         0\n";
    out << std::endl;
    out << "NPOIN:   Number of nodes\n";
    out << "NELEM:   Number of continuous elements\n";
    out << "NMATS:   Number of materials\n";
    out << "NDIME:   Number of dimensions\n";
    out << "NNOMAX:  Number of nodes of the element with the highest number\n";
    out << "USEGRA:  Use gravitational force (0=NO, 1=YES)\n";
    out << "NELINT:  Number of interface elements\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%SOLUTION_PARAMETERS\n";
    out << "    THREADS\n";
    out << "---------+\n";
    out << "         " << _numThreads << "\n";
    out << std::endl;

    out << "      TINT      DTOL       EPS\n";
    out << "---------+---------+---------+\n";
    out << "         1     0.001     1e-16\n";
    out << std::endl;
    out << "      NLES  MAXIT_NL      ITOL\n";
    out << "---------+---------+---------+\n";
    out << "         1        10    0.0001\n";
    out << std::endl;
    out << "      LEQS  MAXIT_LE     CGTOL\n";
    out << "---------+---------+---------+\n";
    out << "         1    100000     1e-05\n";
    out << std::endl;
    out << "      STIN      FTOL      STOL      LTOL\n";
    out << "---------+---------+---------+---------+\n";
    out << "         1     1e-05     1e-05     1e-05\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%STEPS_DEFINITION\n";
    out << "    NUMSTEPS      TIME_0\n";
    out << "-----------+-----------+\n";
    out << "           " << _steps.size( ) - 1 << "           0\n";
    out << std::endl;
    out << "       T_ini       T_fin       D_ini       D_min       D_max      D_freq\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+\n";
    for (int i = 1; i < _steps.size( ); i++)
    {
        out << "           " << i << "           " << i << "           1       1e-03           0           1\n";
    }

    out << "==================================================================================================================================\n";
    out << "%OUTPUT_DEFINITION\n";
    out << "    IMPPPR    IMPDIS    IMPTTR    IMPTEN    IMPVEL  NUMCONTR   REDUPRI\n";
    out << "---------+---------+---------+---------+---------+---------+---------+\n";
    out << "         1         1         1         1         1         1         1\n";
    out << std::endl;
    out << " NODES_HISTORY\n";
    out << "---------+\n";
    out << "         1\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%TIME_FUNCTIONS\n";
    out << "    NFUNCT\n";
    out << "---------+\n";
    out << "         1\n";
    out << std::endl;
    out << "         i    points Description\n";
    out << "---------+---------+-----------+\n";
    out << "         1         2   tf_1\n";
    out << std::endl;
    out << " FUNCTIONS POINTS ith\n";
    out << "   Time(i,j), Value(i,j)\n";
    out << "-----------+-----------+\n";
    out << "           0           0\n";
    out << "           1           0\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%MESH_DATA\n";
    out << " Information available in file.post.msh\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%MATERIALS_DATA\n";
    out << " Information available in file.prp\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%BOUNDARY_CONDITIONS_DATA\n";
    out << " Information available in file.bco\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%END_DAT";
    out << std::endl;
}



bool WriteOutputFiles::writePostMshFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "#==================================================================================================================================\n";
    out << "#GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "#Author: Multiphysics Modeling and Simulation Group\n";
    out << "#TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "MESH dimension 3 ElemType Hexahedra Nnode 8\n";
    out << "Coordinates\n";
    out << "#     NODE                   X                   Y                   Z\n";
    out << "#--------+-------------------+-------------------+-------------------+\n";

    const DsGrid3D< Point3D >& grid = _reservoir->getGrid( );
    for (unsigned int i = 0; i < grid.Size( ); i++)
    {
        out << "         " << i + 1 << "            " << grid.GetRef( i )[0] << "         " << grid.GetRef( i )[1] << "        " << grid.GetRef( i )[2] << std::endl;
    }

    out << "End Coordinates\n";
    out << "Elements\n";
    out << "#     " << _reservoir->getNumCells( ) << "       8       8       3\n";
    out << "#  Ielem     Conectivities                                               Lmat LcoH LcoM LcoT Ltip Nnom Nnof Ngau\n";
    out << "#------+-------+-------+-------+-------+-------+-------+-------+-------+----+----+----+----+----+----+----+----+\n";

    int idx = 1;
    //Write the reservoir brick8 element.
    for (int k = 0; k < grid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < grid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < grid.Ni( ) - 1; i++)
            {
                out << "       " << idx << "     ";

                out << grid.GetIndex( i + 0, j + 0, k + 0 ) + 1 << "     ";
                out << grid.GetIndex( i + 1, j + 0, k + 0 ) + 1 << "     ";
                out << grid.GetIndex( i + 1, j + 1, k + 0 ) + 1 << "     ";
                out << grid.GetIndex( i + 0, j + 1, k + 0 ) + 1 << "     ";

                out << grid.GetIndex( i + 0, j + 0, k + 1 ) + 1 << "     ";
                out << grid.GetIndex( i + 1, j + 0, k + 1 ) + 1 << "     ";
                out << grid.GetIndex( i + 1, j + 1, k + 1 ) + 1 << "      ";
                out << grid.GetIndex( i + 0, j + 1, k + 1 ) + 1 << "    ";

                int ri, rj, rk;
                _reservoir->computeReservoirIndexFromFinalReservoir( i, j, k, ri, rj, rk );
                int material = 0;
                if (rk < 0)
                {
                    if (_reservoir->getReservoirInputMesh( )->invertedZ( ))
                    {
                        material = 1;
                    }
                    else
                    {
                        material = 5;
                    }
                }
                else if (rk > _reservoir->getReservoirInputMesh( )->getGrid( ).Nk( ) - 1)
                {
                    if (_reservoir->getReservoirInputMesh( )->invertedZ( ))
                    {
                        material = 5;
                    }
                    else
                    {
                        material = 1;
                    }
                }
                else if (ri < 0 || rj < 0 ||
                         ri > _reservoir->getReservoirInputMesh( )->getGrid( ).Ni( ) - 1 ||
                         rj > _reservoir->getReservoirInputMesh( )->getGrid( ).Nj( ) - 1)
                {
                    material = 2;
                }
                else
                {
                    if (_reservoir->getReservoirInputMesh( )->getGrid( ).GetRef( ri, rj, rk ).active( ))
                    {
                        material = 3;
                    }
                    else
                    {
                        material = 4;
                    }
                }

                out << material << "    " << 1 << "    " << 1 << "    " << 1 << "    " << 5 << "    " << 8 << "    " << 8 << "    " << _numberOfGaussPoints << std::endl;
                idx++;
            }
        }
    }

    out << "End Elements" << std::endl;
}



bool WriteOutputFiles::writePrpFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "==================================================================================================================================\n";
    out << "GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "Author: Multiphysics Modeling and Simulation Group\n";
    out << "TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%MATERIALS_DATA(NMATS)\n";
    out << std::endl;
    out << "  1 Overburden\n";
    out << "  Hydraulic Properties:\n";
    out << "         Kff           -           -           -           -           -           -           -   Fluid_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20           0           0           0           0           0           0           0          10           0\n";
    out << "  Mechanical Properties:\n";
    out << "         Kss           E          nu           -           -           -           -           -           -           -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20     " << _materialYoung[0] << "         " << _materialPoisson[0] << "           0           0           0           0           0           0           0\n";
    out << "           -           -           -           -           -           -      UpdPor      UpdKhy  Grain_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0          23           0\n";
    out << "  Thermal Properties:\n";
    out << "grain_alphav    grain_cp fluid_alphav   fluid_cp\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0           0           0\n";
    out << std::endl;
    out << "  2 Sideburden\n";
    out << "  Hydraulic Properties:\n";
    out << "         Kff           -           -           -           -           -           -           -   Fluid_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20           0           0           0           0           0           0           0          10           0\n";
    out << "  Mechanical Properties:\n";
    out << "         Kss           E          nu           -           -           -           -           -           -           -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20     " << _materialYoung[1] << "         " << _materialPoisson[1] << "           0           0           0           0           0           0           0\n";
    out << "           -           -           -           -           -           -      UpdPor      UpdKhy  Grain_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0          23           0\n";
    out << "  Thermal Properties:\n";
    out << "grain_alphav    grain_cp fluid_alphav   fluid_cp\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0           0           0\n";
    out << std::endl;
    out << "  3 ReservoirAct\n";
    out << "  Hydraulic Properties:\n";
    out << "         Kff           -           -           -           -           -           -           -   Fluid_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20           0           0           0           0           0           0           0          10           0\n";
    out << "  Mechanical Properties:\n";
    out << "         Kss           E          nu           -           -           -           -           -           -           -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20     " << _materialYoung[2] << "         " << _materialPoisson[2] << "           0           0           0           0           0           0           0\n";
    out << "           -           -           -           -           -           -      UpdPor      UpdKhy  Grain_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0          23           0\n";
    out << "  Thermal Properties:\n";
    out << "grain_alphav    grain_cp fluid_alphav   fluid_cp\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0           0           0\n";
    out << std::endl;
    out << "  4 ReservoirNonAct\n";
    out << "  Hydraulic Properties:\n";
    out << "         Kff           -           -           -           -           -           -           -   Fluid_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20           0           0           0           0           0           0           0          10           0\n";
    out << "  Mechanical Properties:\n";
    out << "         Kss           E          nu           -           -           -           -           -           -           -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20     " << _materialYoung[3] << "         " << _materialPoisson[3] << "           0           0           0           0           0           0           0\n";
    out << "           -           -           -           -           -           -      UpdPor      UpdKhy  Grain_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0          23           0\n";
    out << "  Thermal Properties:\n";
    out << "grain_alphav    grain_cp fluid_alphav   fluid_cp\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0           0           0\n";
    out << std::endl;
    out << "  5 Underburden\n";
    out << "  Hydraulic Properties:\n";
    out << "         Kff           -           -           -           -           -           -           -   Fluid_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20           0           0           0           0           0           0           0          10           0\n";
    out << "  Mechanical Properties:\n";
    out << "         Kss           E          nu           -           -           -           -           -           -           -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "       1e+20     " << _materialYoung[4] << "         " << _materialPoisson[4] << "           0           0           0           0           0           0           0\n";
    out << "           -           -           -           -           -           -      UpdPor      UpdKhy  Grain_gama          -\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0          23           0\n";
    out << "  Thermal Properties:\n";
    out << "grain_alphav    grain_cp fluid_alphav   fluid_cp\n";
    out << "-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    out << "           0           0           0           0           0           0           0           0           0           0\n";
}



bool WriteOutputFiles::writePpiFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "===============================================================================================================================================================\n";
    out << "GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "Author: Multiphysics Modeling and Simulation Group\n";
    out << "TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "FILE TO ASSIGN INITIAL PORE PRESSURES.\n";
    out << "      elem                 pp0\n";
    out << "================================================================================================================================================================\n";
    out << "%INITIAL_ELEMENT_PORE_PRESSURES\n";
    out << "---------+-------------------+\n";

    int idx = 1;
    const DsGrid3D< Point3D >& grid = _reservoir->getGrid( );

    for (int k = 0; k < grid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < grid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < grid.Ni( ) - 1; i++)
            {
                double h = ( grid.GetRef( i + 0, j + 0, k + 0 )[2] +
                             grid.GetRef( i + 1, j + 0, k + 0 )[2] +
                             grid.GetRef( i + 0, j + 1, k + 0 )[2] +
                             grid.GetRef( i + 1, j + 1, k + 0 )[2] +
                             grid.GetRef( i + 0, j + 0, k + 1 )[2] +
                             grid.GetRef( i + 1, j + 0, k + 1 )[2] +
                             grid.GetRef( i + 0, j + 1, k + 1 )[2] +
                             grid.GetRef( i + 1, j + 1, k + 1 )[2] ) / 8.0;

                //Compute the poro pressure.
                double p = _poroGradient * ( _referenceLevel - h ) + _poroReferenceLevel;

                if (_cells.GetRef( i, j, k )._active)
                {
                    p = _cells.GetRef( i, j, k )._poroPressure[0];
                }
                else
                {
                    for (int s = 0; s < _numSteps; s++)
                    {
                        _cells.GetRef( i, j, k )._poroPressure[s] = p;
                    }
                }

                out << "         " << idx << "         " << p << std::endl;
                idx++;
            }
        }
    }
    out << "%END";
    out << std::endl;
}



bool WriteOutputFiles::writeSigFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "===============================================================================================================================================================\n";
    out << "GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "Author: Multiphysics Modeling and Simulation Group\n";
    out << "TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "FILE TO ASSIGN INITIAL STRESSES.\n";
    out << "(label=0)\n";
    out << "         0     Ielem         sxx         syy         szz     valuep0      kindp0\n";
    out << "(label=1)\n";
    out << "         1     Ielem    RefLevel        sxx0      gradxx        syy0      gradyy        szz0      gradzz     valuep0      kindp0\n";
    out << "(label=2)\n";
    out << "         2     Ielem     usegama        gama       usek0         k0x         k0y     valuep0      kindp0\n";
    out << "(label=3)\n";
    out << "         3     Ielem READ_FILE\n";
    out << "================================================================================================================================================================\n";
    out << "%INITIAL_STRESS\n";
    out << "---------+---------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+\n";
    int idx = 1;
    const DsGrid3D< Point3D >& grid = _reservoir->getGrid( );

    for (int k = 0; k < grid.Nk( ) - 1; k++)
    {
        for (int j = 0; j < grid.Nj( ) - 1; j++)
        {
            for (int i = 0; i < grid.Ni( ) - 1; i++)
            {
                out << "         2         " << idx << "           2          " << _overburdenStressGradient;
                out << "           1         " << _horizontalStressRatioX << "         " << _horizontalStressRatioY << "           0           0\n";
                idx++;
            }
        }
    }
    out << "%END";
    out << std::endl;
}



bool WriteOutputFiles::writeBcoFile( std::string name ) const
{
    std::ofstream out( name.c_str( ) );
    if (!out)
    {
        printf( "Erro ao abrir o arquivo %s\n", name.c_str( ) );
        return false;
    }

    out << "==================================================================================================================================\n";
    out << "GEOFLUX3D - A parallel finite element simulator for the analysis of thermo-hydro-mechanical (THM) processes in porous media\n";
    out << "Author: Multiphysics Modeling and Simulation Group\n";
    out << "TECGRAF / PUC-RIO\n";
    out << std::endl;
    out << "==================================================================================================================================\n";
    out << "%NODES_FIXED_DISPLACEMENT\n";
    out << "      NNFD\n";
    out << "---------+\n";
    const DsGrid3D< Point3D >& grid = _reservoir->getGrid( );
    int numBorderNodes = 0;
    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                if (k == 0 || j == 0 || i == 0 ||
                    j == grid.Nj( ) - 1 || i == grid.Ni( ) - 1)
                {
                    numBorderNodes++;
                }
            }
        }
    }
    out << "       " << numBorderNodes << std::endl;

    out << "   Node(i)   Id(i,1)   Id(i,2)   Id(i,3)\n";
    out << "---------+---------+---------+---------+\n";

    for (int k = 0; k < grid.Nk( ); k++)
    {
        for (int j = 0; j < grid.Nj( ); j++)
        {
            for (int i = 0; i < grid.Ni( ); i++)
            {
                int x = 0, y = 0, z = 0;
                if (i == 0 || i == grid.Ni( ) - 1)
                {
                    x = 1;
                }
                if (j == 0 || j == grid.Nj( ) - 1)
                {
                    y = 1;
                }
                if (k == 0)
                {
                    z = 1;
                }

                if (x || y || z)
                {
                    out << "         " << grid.GetIndex( i, j, k ) + 1 << "         " << x << "         " << y << "         " << z << std::endl;
                }
            }
        }
    }

    out << "==================================================================================================================================\n";
    out << "%ELEMS_INSTANTANEOUS_POREPRESSURE\n";
    out << "      NEPP\n";
    out << "---------+\n";
    out << "         " << _steps.size( ) - 1 << std::endl;

    for (int i = 1; i < _steps.size( ); i++)
    {
        out << "      Time  NumElems\n";
        out << "---------+---------+\n";
        out << "         " << i << "        " << _cells.Size( ) << std::endl;
        out << "   Element        DP\n";
        out << "---------+---------+\n";
        for (unsigned int c = 0; c < _cells.Size( ); c++)
        {
            double diff = _cells.GetRef( c )._poroPressure[_steps[i]] - _cells.GetRef( c )._poroPressure[_steps[i - 1]];
            out << "       " << c + 1 << "     " << diff << std::endl;
        }

    }
    out << "==================================================================================================================================\n";
    out << "%NODES_REFERENCE_LEVEL\n";
    out << "      NNRL\n";
    out << "---------+\n";
    out << "        " << grid.Nj( ) * grid.Ni( ) << std::endl;
    out << std::endl;
    out << "     NODES\n";
    out << "---------+\n";
    for (int j = 0; j < grid.Nj( ); j++)
    {
        for (int i = 0; i < grid.Ni( ); i++)
        {
            out << "       " << grid.GetIndex( i, j, grid.Nk( ) - 1 ) + 1 << std::endl;
        }
    }
    out << "==================================================================================================================================\n";
    out << "%END";
    out << std::endl;
}



WriteOutputFiles::~WriteOutputFiles( )
{
}

