/* 
 * File:   ReservoirMesh.cpp
 * Author: jcoelho
 * 
 * Created on January 23, 2017, 3:59 PM
 */

#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include "ReservoirMesh.h"

using namespace std;


/*
                                   e9
                  v3               -->                v7
                  n3----------------------------------n7
                  /.                                  /|
                 / .                                 / |
                /  .                                /  |
               /   .                               /   |
        e2 /  /    .         f5                   /    |
          v  /     . ^                     e6 /  /     |   ^
            /      . |e1                     v  /      |   | e5
           /       .                f2         /       |
          /        .                          /        |
        n2----------------------------------n6         |
        v2         .      -->               v6         |
         |         .      e10                |         |
         |   f0    .              e8         |    f1   |
         |         . /            -->        |         |
         |        n1 . . . . . . . . . . . . | . . . .n5
         |       .v1                         |        v5
   e3 |  |    ^ .       f3             e7 |  |       /
      v  | e0/ .                          v  |      /
         |    .                              |     /   ^
         |   .               f4              |    /   / e4
         |  .                                |   /
         | .                                 |  /
         |.                                  | /
        n0-----------------------------------n4
        v0                -->                v4
 y ^                      e11
   |  ^ x
   | /
   |/   
   . ---> 
        z                
 */

unsigned int faceToVertex[6][4] = {
    {0, 1, 3, 2 },
    {4, 5, 7, 6 },
    {1, 5, 7, 3 },
    {0, 4, 6, 2 },
    {0, 4, 5, 1 },
    {2, 6, 7, 3 }
};

//PUBLIC METHODS.



ReservoirMesh::ReservoirMesh( )
{
    _numActiveCells = 0;
    _numSteps = 0;
    _rotationAngle = 0.0;
    _z = 0.0;
    _invX = _invY = _invZ = false;
}



ReservoirMesh::ReservoirMesh( const int ni, const int nj, const int nk )
{
    _numActiveCells = 0;
    _numSteps = 0;
    _rotationAngle = 0.0;
    _z = 0.0;
    _invX = _invY = _invZ = false;
    _mesh.Resize( ni, nj, nk );
}



double ReservoirMesh::getAverageZ( ) const
{
    return _z;
}



double ReservoirMesh::getAverageZOnBottomSlice( int k ) const
{
    double z = 0.0;
    for (int i = 0; i < _mesh.Ni( ); i++)
    {
        for (int j = 0; j < _mesh.Nj( ); j++)
        {
            const CellMesh& c = _mesh.GetRef( i, j, k );
            for (int p = 0; p < 4; p++)
            {
                z += _coordinates[c.point( p )][2] / 4.0;
            }
        }
    }
    return z / ( _mesh.Ni( ) * _mesh.Nj( ) );
}



double ReservoirMesh::getAverageZOnTopSlice( int k ) const
{
    double z = 0.0;
    for (int i = 0; i < _mesh.Ni( ); i++)
    {
        for (int j = 0; j < _mesh.Nj( ); j++)
        {
            const CellMesh& c = _mesh.GetRef( i, j, k );
            for (int p = 4; p < 8; p++)
            {
                z += _coordinates[c.point( p )][2] / 4.0;
            }
        }
    }
    return z / ( _mesh.Ni( ) * _mesh.Nj( ) );
}



double ReservoirMesh::getCellsByMeters( ) const
{
    return _mesh.Nk( ) / ( _AABB[1][2] - _AABB[0][2] );
}



double ReservoirMesh::getActiveCellsByMeters( ) const
{
    return _mesh.Nk( ) / ( getReservoirThinckness( ) );
}



double ReservoirMesh::getReservoirThinckness( ) const
{
    double thinckness = -1e100;
    for (int j = 0; j < _mesh.Nj( ); j++)
    {
        for (int i = 0; i < _mesh.Ni( ); i++)
        {
            double minZ = +1e100, maxZ = -1e100;
            for (int k = 0; k < _mesh.Nk( ); k++)
            {
                //Get cell information.
                const CellMesh& c = _mesh.GetRef( i, j, k );
                if (c.active( ))
                {
                    for (int v = 0; v < 8; v++)
                    {
                        int idx = c.point( v );
                        const double z = _coordinates[idx][2];

                        minZ = std::min( minZ, z );
                        maxZ = std::max( maxZ, z );
                    }
                }
            }
            thinckness = std::max( thinckness, maxZ - minZ );
        }
    }
    return thinckness;
}



const DsGrid3D<CellMesh>& ReservoirMesh::getGrid( ) const
{
    return _mesh;
}



DsGrid3D<CellMesh>& ReservoirMesh::getGrid( )
{
    return _mesh;
}



double ReservoirMesh::getHeight( ) const
{
    return _AABB[1][2] - _AABB[0][2];
}



double ReservoirMesh::getLengh( ) const
{
    return _AABB[1][1] - _AABB[0][1];
}



unsigned int ReservoirMesh::getNumberOfNodes( ) const
{
    return _coordinates.size( );
}



int ReservoirMesh::getNumberOfElements( ) const
{
    return _mesh.Size( );
}



const std::vector<Point3D>& ReservoirMesh::getReservoirNodes( ) const
{
    return _coordinates;
}



double ReservoirMesh::getRotationAngle( ) const
{
    return _rotationAngle;
}



double ReservoirMesh::getWidth( ) const
{
    return _AABB[1][0] - _AABB[0][0];
}



double ReservoirMesh::getXMax( ) const
{
    return _AABB[1][0];
}



double ReservoirMesh::getXMin( ) const
{
    return _AABB[0][0];
}



double ReservoirMesh::getYMax( ) const
{
    return _AABB[1][1];
}



double ReservoirMesh::getYMin( ) const
{
    return _AABB[0][1];
}



double ReservoirMesh::getZMax( ) const
{
    return _AABB[1][2];
}



double ReservoirMesh::getZMin( ) const
{
    return _AABB[0][2];
}



double ReservoirMesh::getActiveXMax( ) const
{
    return _activeAABB[1][0];
}



double ReservoirMesh::getActiveXMin( ) const
{
    return _activeAABB[0][0];
}



double ReservoirMesh::getActiveYMax( ) const
{
    return _activeAABB[1][1];
}



double ReservoirMesh::getActiveYMin( ) const
{
    return _activeAABB[0][1];
}



double ReservoirMesh::getActiveZMax( ) const
{
    return _activeAABB[1][2];
}



double ReservoirMesh::getActiveZMin( ) const
{
    return _activeAABB[0][2];
}



double ReservoirMesh::getXMaxFace( int i, int j, int k )
{
    if (_invX)
        return getFaceAverage( i, j, k, 3, 0 );

    return getFaceAverage( i, j, k, 2, 0 );
}



double ReservoirMesh::getXMinFace( int i, int j, int k )
{
    if (_invX)
        return getFaceAverage( i, j, k, 2, 0 );

    return getFaceAverage( i, j, k, 3, 0 );
}



double ReservoirMesh::getYMaxFace( int i, int j, int k )
{
    if (_invY)
        return getFaceAverage( i, j, k, 4, 1 );

    return getFaceAverage( i, j, k, 5, 1 );
}



double ReservoirMesh::getYMinFace( int i, int j, int k )
{
    if (_invY)
        return getFaceAverage( i, j, k, 5, 1 );

    return getFaceAverage( i, j, k, 4, 1 );
}



double ReservoirMesh::getZMaxFace( int i, int j, int k )
{
    if (_invZ)
        return getFaceAverage( i, j, k, 0, 2 );

    return getFaceAverage( i, j, k, 1, 2 );
}



double ReservoirMesh::getZMinFace( int i, int j, int k )
{
    if (_invZ)
        return getFaceAverage( i, j, k, 1, 2 );

    return getFaceAverage( i, j, k, 0, 2 );
}



void ReservoirMesh::getTranslateVector( double &tx, double &ty ) const
{
    tx = _tx;
    ty = _ty;
}



bool ReservoirMesh::invertedX( ) const
{
    return _invX;
}



bool ReservoirMesh::invertedY( ) const
{
    return _invY;
}



bool ReservoirMesh::invertedZ( ) const
{
    return _invZ;
}


//
//bool ReservoirMesh::readReservoirGeresimFile( const std::string& path )
//{
//    freeMemory( );
//
//    std::ifstream in( path.c_str( ) );
//    if (in.fail( ))
//    {
//        printf( "Erro na abertura do arquivo: %s\n", path.c_str( ) );
//        return false;
//    }
//
//    //Read header file.
//    unsigned int ni, nj, nk, numVertices;
//    in >> ni >> nj >> nk >> _numActiveCells >> numVertices >> _numSteps;
//
//    std::string aux;
//    getline( in, aux );
//    for (unsigned int i = 0; i < _numSteps; i++)
//    {
//        getline( in, aux );
//    }
//
//    //Allocate the grid.
//    _mesh.Resize( ni, nj, nk );
//
//    //Allocate vector to the coordinates.
//    _coordinates.reserve( numVertices );
//    for (unsigned int v = 0; v < numVertices; v++)
//    {
//        Point3D p;
//        in >> p;
//        _coordinates.push_back( p );
//    }
//
//    unsigned int totalCells = ni * nj * nk;
//
//    _props.resize( _numSteps );
//    for (unsigned int i = 0; i < _props.size( ); i++)
//    {
//        _props[i].resize( 2 );
//        _props[i][0]._name = "ACTNUM";
//        _props[i][0]._p.resize( totalCells );
//
//        _props[i][1]._name = "GERESIM";
//        _props[i][1]._p.resize( totalCells );
//    }
//
//    for (unsigned int cell = 0; cell < totalCells; cell++)
//    {
//        CellMesh& c = _mesh.GetRef( cell );
//        bool active;
//        unsigned i, j, k;
//        int points[8];
//        int adjs[6];
//
//        in >> active >> i >> j >> k;
//
//        in >> points[0] >> points[1] >> points[2] >> points[3] >> points[4]
//            >> points[5] >> points[6] >> points[7];
//
//        in >> adjs[0] >> adjs[1] >> adjs[2] >> adjs[3] >> adjs[4] >> adjs[5];
//
//        _props[0][0]._p[cell] = active;
//        for (unsigned int p = 0; p < _numSteps; p++)
//        {
//            double property;
//            in >> property;
//            _props[p][1]._p[cell] = property;
//        }
//
//        //Set cell information.
//        c.setAdjacentCells( adjs );
//        c.setPoints( points );
//        c.active( active );
//
//        //Define the cell on grid.
//        _mesh.Set( i, j, k, c );
//    }
//
//    return preprocessReservoidData( );
//}
//



bool ReservoirMesh::readReservoirEclipseGridFile( const std::string& path )
{
    GRDECLReader r;
    r.read( path );
    _mesh = r.getGridMesh( );
    _numActiveCells = r.getNumActiveCells( );
    _coordinates = r.getGeometry( );
    _props.push_back( r.getProperties( ) );
    _numSteps++;

    return preprocessReservoidData( );
}



ReservoirMesh::~ReservoirMesh( )
{
    freeMemory( );
}

//PRIVATE METHODS.



bool ReservoirMesh::checkInvertedDirections( )
{
    int ni = _mesh.Ni( );
    int nj = _mesh.Nj( );
    int nk = _mesh.Nk( );

    int right = 0, wrong = 0;

    //Check z direction.
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < nj; j++)
        {
            const CellMesh& c1 = _mesh.GetRef( i, j, 0 );
            const CellMesh& c2 = _mesh.GetRef( i, j, nk - 1 );
            double zi = _coordinates[c1.point( 0 )][2];
            double zf = _coordinates[c2.point( 0 )][2];

            zf - zi < 0 ? wrong++ : right++;
        }
    }

    wrong > right ? _invZ = true : _invZ = false;

    right = 0;
    wrong = 0;

    //Check x direction.
    for (int k = 0; k < nk; k++)
    {
        for (int j = 0; j < nj; j++)
        {
            const CellMesh& c1 = _mesh.GetRef( 0, j, k );
            const CellMesh& c2 = _mesh.GetRef( ni - 1, j, k );

            double xi = _coordinates[c1.point( 0 )][0];
            double xf = _coordinates[c2.point( 0 )][0];

            xf - xi < 0 ? wrong++ : right++;
        }
    }

    wrong > right ? _invX = true : _invX = false;

    right = 0;
    wrong = 0;

    //Check y direction.
    for (int i = 0; i < ni; i++)
    {
        for (int k = 0; k < nk; k++)
        {
            const CellMesh& c1 = _mesh.GetRef( i, 0, k );
            const CellMesh& c2 = _mesh.GetRef( i, nj - 1, k );

            double yi = _coordinates[c1.point( 0 )][1];
            double yf = _coordinates[c2.point( 0 )][1];

            yf - yi < 0 ? wrong++ : right++;
        }
    }

    wrong > right ? _invY = true : _invY = false;

    return true;
}



void ReservoirMesh::computeAABB( )
{
    _AABB[0] = _coordinates[0];
    _AABB[1] = _coordinates[0];

    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        const double x = _coordinates[i][0];
        const double y = _coordinates[i][1];
        const double z = _coordinates[i][2];

        //The min point.
        _AABB[0][0] = min( x, _AABB[0][0] );
        _AABB[0][1] = min( y, _AABB[0][1] );
        _AABB[0][2] = min( z, _AABB[0][2] );

        //The max point.
        _AABB[1][0] = max( x, _AABB[1][0] );
        _AABB[1][1] = max( y, _AABB[1][1] );
        _AABB[1][2] = max( z, _AABB[1][2] );
    }
}



void ReservoirMesh::computeActiveAABB( )
{
    _activeAABB[0] = Point3D( +1e100, +1e100, +1e100 );
    _activeAABB[1] = Point3D( -1e100, -1e100, -1e100 );

    for (int k = 0; k < _mesh.Nk( ); k++)
    {
        for (int j = 0; j < _mesh.Nj( ); j++)
        {
            for (int i = 0; i < _mesh.Ni( ); i++)
            {
                //Get cell information.
                const CellMesh& c = _mesh.GetRef( i, j, k );
                if (c.active( ))
                {
                    for (int v = 0; v < 8; v++)
                    {
                        int idx = c.point( v );
                        const double x = _coordinates[idx][0];
                        const double y = _coordinates[idx][1];
                        const double z = _coordinates[idx][2];

                        //The min point.
                        _activeAABB[0][0] = std::min( x, _activeAABB[0][0] );
                        _activeAABB[0][1] = std::min( y, _activeAABB[0][1] );
                        _activeAABB[0][2] = std::min( z, _activeAABB[0][2] );

                        //The max point.
                        _activeAABB[1][0] = std::max( x, _activeAABB[1][0] );
                        _activeAABB[1][1] = std::max( y, _activeAABB[1][1] );
                        _activeAABB[1][2] = std::max( z, _activeAABB[1][2] );
                    }
                }
            }
        }
    }
}



void ReservoirMesh::computeAverageZ( )
{
    _z = 0.0;
    int count = 0;
    for (int k = 0; k < _mesh.Nk( ); k++)
    {
        for (int j = 0; j < _mesh.Nj( ); j++)
        {
            for (int i = 0; i < _mesh.Ni( ); i++)
            {
                //Get cell information.
                const CellMesh& c = _mesh.GetRef( i, j, k );
                if (c.active( ) || _numActiveCells == 0)
                {
                    for (int v = 0; v < 8; v++)
                    {
                        _z += _coordinates[c.point( v )][2];
                        count++;
                    }
                }
            }
        }
    }

    _z /= count;
}



void ReservoirMesh::computeMainDirections( )
{
    _mainDirections[0] = _mainDirections[1] = Point3D( 0, 0, 0 );
    for (int k = 0; k < _mesh.Nk( ); k++)
    {
        for (int j = 0; j < _mesh.Nj( ); j++)
        {
            for (int i = 0; i < _mesh.Ni( ); i++)
            {
                //Sum the x direction.
                Point3D f2 = getFaceCentralPoint( i, j, k, 2 );
                Point3D f3 = getFaceCentralPoint( i, j, k, 3 );
                _mainDirections[0] += f2 - f3;

                //Sum the y direction.
                Point3D f5 = getFaceCentralPoint( i, j, k, 5 );
                Point3D f4 = getFaceCentralPoint( i, j, k, 4 );
                _mainDirections[1] += f5 - f4;
            }
        }
    }

    //Define z as 0.0.
    _mainDirections[0][2] = _mainDirections[1][2] = 0.0;

    double normX = _mainDirections[0] * _mainDirections[0];
    double normY = _mainDirections[1] * _mainDirections[1];

    _mainDirections[0] = _mainDirections[0] / sqrt( normX );
    _mainDirections[1] = _mainDirections[1] / sqrt( normY );

    _rotationAngle = atan2( _mainDirections[0][1], _mainDirections[0][0] );
}



void ReservoirMesh::freeMemory( )
{
    _numActiveCells = 0;
    _numSteps = 0;
    _rotationAngle = 0.0;
    _z = 0.0;
}



bool ReservoirMesh::preprocessReservoidData( )
{
    //Compute the data main directions.
    computeMainDirections( );

    //Translate the data for origin.
    translateData( );

    //Rotate the data to be aligned with xy-axis.
    rotateData( );

    //Compute the axis aligned bounding box.
    computeAABB( );

    computeActiveAABB( );

    //Verify the inverted directions.
    bool check = checkInvertedDirections( );

    //Pre-compute the average of Z.
    computeAverageZ( );

    return check;
}



void ReservoirMesh::translateData( )
{
    _tx = _ty = 1e100;
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        const double x = _coordinates[i][0];
        const double y = _coordinates[i][1];

        //The min point.
        _tx = min( x, _tx );
        _ty = min( y, _ty );
    }

    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        _coordinates[i][0] -= _tx;
        _coordinates[i][1] -= _ty;
    }
}



void ReservoirMesh::untranslateData( )
{
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        _coordinates[i][0] += _tx;
        _coordinates[i][1] += _ty;
    }
}



void ReservoirMesh::rotateData( )
{
    //Rotate all data.
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        rotatePoint( _coordinates[i], -_rotationAngle );
    }
}



void ReservoirMesh::rotateData( double angle )
{
    //Rotate all data.
    for (unsigned int i = 0; i < _coordinates.size( ); i++)
    {
        rotatePoint( _coordinates[i], angle );
    }
}



void ReservoirMesh::rotatePoint( Point3D& p, double angle )
{
    double x = p[0], y = p[1];
    p[0] = x * cos( angle ) - y * sin( angle );
    p[1] = x * sin( angle ) + y * cos( angle );
}



double ReservoirMesh::getFaceAverage( int i, int j, int k, int f, int d )
{
    //Get cell information.
    const CellMesh& c = _mesh.GetRef( i, j, k );

    //Sum all face points.
    double x = 0;
    for (unsigned int i = 0; i < 4; i++)
    {
        unsigned int idx = c.point( faceToVertex[f][i] );
        x = x + _coordinates[idx][d];
    }
    return x / 4.0;
}



Point3D ReservoirMesh::getFaceCentralPoint( int i, int j, int k, int f )
{
    //Get cell information.
    const CellMesh& c = _mesh.GetRef( i, j, k );

    //Sum all face points.
    Point3D p( 0, 0, 0 );
    for (unsigned int i = 0; i < 4; i++)
    {
        unsigned int idx = c.point( faceToVertex[f][i] );
        p = p + _coordinates[idx];
    }
    return p / 4.0;
}



const std::vector< std::vector< Property > >& ReservoirMesh::getProperties( ) const
{
    return _props;
}
