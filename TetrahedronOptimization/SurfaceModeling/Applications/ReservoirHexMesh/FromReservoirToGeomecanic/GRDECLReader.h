/* 
 * File:   GRDECLReader.h
 * Author: jcoelho
 *
 * Created on August 28, 2017, 2:10 PM
 */

#ifndef GRDECLREADER_H
#define GRDECLREADER_H

#include <string>
#include <vector>
#include "../grid3d.h"
#include "../Point3D.h"
#include "CellMesh.h"

#include "ert/ecl/ecl_box.h"
#include "ert/ecl/ecl_kw.h"

struct Property
{
    std::vector<double> _p;
    std::string _name;
};

class GRDECLReader
{
public:
    /**
     * Constructor
     */
    GRDECLReader( );

    /**
     * Get the grid.
     * @return - the grid cells.
     */
    const DsGrid3D<CellMesh>& getGridMesh( ) const;

    /**
     * Get the coordinate points.
     * @return - a vector with the coords.
     */
    const std::vector<Point3D>& getGeometry( ) const;

    /**
     * Get the properties.
     * @return - properties vector.
     */
    const std::vector<Property>& getProperties( ) const;

    /**
     * Get the number of active cells.
     * @return - number of active cells.
     */
    int getNumActiveCells( ) const;

    /**
     * Read the file.
     * @param path - path for the file to be opened.
     * @return - true if everything is ok.
     */
    bool read( const std::string& path );


    /**
     * Write the data on a neutral file format.
     * @param name - neutral file name.
     */
    void writeNeutralFile( const std::string& name );

    /**
     * Destructor.
     */
    virtual ~GRDECLReader( );
private:

    //Auxiliary structure.

    struct KeywordAndFilePos
    {
        std::string keyword;
        long long filePos;
    };

    /**
     * Grid to store the mesh.
     */
    DsGrid3D<CellMesh> _mesh;

    /**
     * A set of properties.
     */
    std::vector<Property> _properties;
    /**
     * Mesh coordinates.
     */
    std::vector<Point3D> _coordinates;

    int _numActiveCells;
    int _ni, _nj, _nk;

private:

    /**
     * Get the geometry grid keyword positions.
     * @param keywordsAndFilePos - all keywords and your positions.
     * @param coordPos - coordinates position.
     * @param zcornPos - z corners position.
     * @param specgridPos - specification of grid position.
     * @param actnumPos - active cells position.
     * @param mapaxesPos - map axes position.
     */
    void findGridKeywordPositions( const std::vector< KeywordAndFilePos >& keywordsAndFilePos,
                                   long long& coordPos, long long& zcornPos, long long& specgridPos,
                                   long long& actnumPos, long long& mapaxesPos );

    /**
     * Search for keywords on file.
     * @param fileName - file's path.
     * @param keywords - list of keywords on file.
     */
    void findKeywordsOnFile( const std::string &fileName, std::vector< KeywordAndFilePos >& keywords );

    /**
     * Get the list of invalid property data keyword.
     * @return - list of invalid property data keyword.
     */
    const std::vector<std::string>& invalidPropertyDataKeywords( );

    /**
     * Verify if a keyword is valid.
     * @param keyword
     * @return - true if a keyword is valid.
     */
    bool isValidDataKeyword( const std::string& keyword );

    /**
     * Get the property name.
     * @param newPropertyName - property name.
     * @param m - model.
     * @return - property index.
     */
    int findOrCreateResult( const std::string& newPropertyName );

    /**
     * Reorganize the mesh in order to avoid repeated vertices on mesh and define
     * a neighborhood.
     */
    void preprocessMesh( );

    /**
     * Read an specific property.
     * @param eclipseKeywordData - eclipse object to read the keyword.
     * @param resultName - property name.
     * @param m - model.
     * @return - true if everything was okay.
     */
    bool readDataPropertyFromKeyword( ecl_kw_type* eclipseKeywordData, const std::string& resultName );

    bool checkFaces( int i, int j, int k, int ni, int nj, int nk, int f, int nf );

    /**
     * Create the properties objects.
     * @param fileKeywords - vector with a set of keywords and your positions.
     * @param m - model.
     */
    void allocateProperties( const std::vector< KeywordAndFilePos >& fileKeywords );

    /**
     * Read all properties.
     * @param fileKeywords - vector with a set of keywords and your positions.
     * @param fileName - path for the file.
     * @param m - model
     * @return - true if everything was okay.
     */
    bool readProperties( const std::vector< KeywordAndFilePos >& fileKeywords, const std::string& fileName );

    /**
     * Read the geometry.
     * @param keywordsAndFilePos - vector with a set of keywords and your positions.
     * @param fileName - path for the file.
     * @param m - model.
     * @return - true if everything was okay.
     */
    bool readGeometry( const std::vector< KeywordAndFilePos >& keywordsAndFilePos, const std::string& fileName );
};

#endif /* GRDECLREADER_H */

