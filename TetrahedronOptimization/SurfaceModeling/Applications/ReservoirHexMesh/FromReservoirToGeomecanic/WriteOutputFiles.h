/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   WriteOutputFiles.h
 * Author: jcoelho
 *
 * Created on January 25, 2018, 4:58 PM
 */

#ifndef WRITEOUTPUTFILES_H
#define WRITEOUTPUTFILES_H
#include "../ReservoirConformalMeshVbV/ReservoirConformalMeshEbE.h"

struct PoroPressureCell
{
    std::vector<double> _poroPressure;
    bool _active;
    PoroPressureCell()
    {
        _active = false;
    }
};

class WriteOutputFiles
{
public:
    /**
     * Constructor that receives the model object. 
     * @param m - model object.
     */
    WriteOutputFiles( ReservoirConformalMeshEbE* m, std::string poroPressureFile );

    /**
     * Write all files.
     * @return - true if everything is okay.
     */
    bool writeFiles( std::string name ) const;

    /**
     * Destructor.
     */
    virtual ~WriteOutputFiles( );
private:
    /**
     * Write the .dat file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writeDatFile( std::string name ) const;

    /**
     * Write the .post.msh file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writePostMshFile( std::string name ) const;

    /**
     * Write the .prp file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writePrpFile( std::string name ) const;

    /**
     * Write the .ppi file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writePpiFile( std::string name ) const;

    /**
     * Write the .sig file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writeSigFile( std::string name ) const;

    /**
     * Write the .bco file.
     * @param name - file name.
     * @return - true if everything is okay.
     */
    bool writeBcoFile( std::string name ) const;

private:
    /**
     * Model object.
     */
    ReservoirConformalMeshEbE* _reservoir;

    /**
     * Cell property.
     */
    DsGrid3D< PoroPressureCell > _cells;
    
    std::vector<int> _steps;

    /**
     * Number of simulation steps.
     */
    int _numSteps;

    /**
     * Number of cells.
     */
    int _numCells;
    
    /**
     * Number of threads that will be used in simulation process.
     */
    int _numThreads;
    
    /**
     * Number of Gauss points by element.
     */
    int _numberOfGaussPoints;
    
    double _materialYoung[5];
    double _materialPoisson[5];
    double _stressGradient[5];
    
    double _referenceLevel;
    double _poroReferenceLevel;
    double _poroGradient;
    
    double _overburdenStressGradient;
    double _horizontalStressRatioX;
    double _horizontalStressRatioY;
};

#endif /* WRITEOUTPUTFILES_H */

