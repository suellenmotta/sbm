/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ReservoirMesh.h
 * Author: jcoelho
 *
 * Created on January 23, 2017, 3:59 PM
 */

#ifndef RESERVOIRMESH_H
#define RESERVOIRMESH_H
#include <vector>
#include <string>
#include "GRDECLReader.h"
#include "CellMesh.h"
#include "../Point3D.h"
#include "../grid3d.h"

class ReservoirMesh
{
public:
    /**
     * Default constructor.
     */
    ReservoirMesh( );

    /**
     * Constructor that receives the grid dimension.
     * @param ni - number of elements on i direction.
     * @param nj - number of elements on j direction.
     * @param nk - number of elements on k direction.
     */
    ReservoirMesh( const int ni, const int nj, const int nk );

    /**
     * Get the average z.
     * @return - average of z-coordinates.
     */
    double getAverageZ( ) const;

    /**
     * Get the the z average on bottom slice k.
     * @param k - slice.
     * @return average z on slice.
     */
    double getAverageZOnBottomSlice( int k ) const;

    /**
     * Get the the z average on top slice k.
     * @param k - slice.
     * @return average z on slice.
     */
    double getAverageZOnTopSlice( int k ) const;

    /**
     * Get the number of cells by meters is on reservoir.
     * @return - number of cells by meters is on reservoir.
     */
    double getCellsByMeters( ) const;
    double getActiveCellsByMeters( ) const;

    double getReservoirThinckness( ) const;
    
    /**
     * Get the grid.
     * @return - grid.
     */
    const DsGrid3D<CellMesh>& getGrid( ) const;
    DsGrid3D<CellMesh>& getGrid( );

    /**
     * Get the reservoid height.
     * @return - the reservoid height.
     */
    double getHeight( ) const;

    /**
     * Get the reservoid lenght.
     * @return - the reservoid lenght.
     */
    double getLengh( ) const;

    /**
     * Get the number of nodes on mesh.
     * @return - the number of nodes on mesh.
     */
    unsigned int getNumberOfNodes( ) const;

    /**
     * Get the number of elements on mesh.
     * @return - the number of elements on mesh.
     */
    int getNumberOfElements( ) const;

    /**
     * Get the vector with reservoir nodes.
     * @return - the vector with reservoir nodes.
     */
    const std::vector<Point3D>& getReservoirNodes( ) const;

    /**
     * Get the rotation angle.
     * @return -rotation angle.
     */
    double getRotationAngle( ) const;

    /**
     * Get the reservoir width.
     * @return - the reservoir width.
     */
    double getWidth( ) const;

    /**
     * Get the maximum value of x.
     * @return - maximum value of x.
     */
    double getXMax( ) const;

    /**
     * Get the minimum value of x.
     * @return - minimum value of x.
     */
    double getXMin( ) const;

    /**
     * Get the maximum value of y.
     * @return - maximum value of y.
     */
    double getYMax( ) const;

    /**
     * Get the minimum value of y.
     * @return - minimum value of y.
     */
    double getYMin( ) const;

    /**
     * Get the maximum value of z.
     * @return - maximum value of z.
     */
    double getZMax( ) const;

    /**
     * Get the minimum value of z.
     * @return - minimum value of z.
     */
    double getZMin( ) const;


    double getActiveXMax( ) const;

    /**
     * Get the minimum value of x.
     * @return - minimum value of x.
     */
    double getActiveXMin( ) const;

    /**
     * Get the maximum value of y.
     * @return - maximum value of y.
     */
    double getActiveYMax( ) const;

    /**
     * Get the minimum value of y.
     * @return - minimum value of y.
     */
    double getActiveYMin( ) const;

    /**
     * Get the maximum value of z.
     * @return - maximum value of z.
     */
    double getActiveZMax( ) const;

    /**
     * Get the minimum value of z.
     * @return - minimum value of z.
     */
    double getActiveZMin( ) const;


    void getTranslateVector( double &tx, double &ty ) const;

    double getXMaxFace( int i, int j, int k );
    double getXMinFace( int i, int j, int k );
    double getYMaxFace( int i, int j, int k );
    double getYMinFace( int i, int j, int k );
    double getZMaxFace( int i, int j, int k );
    double getZMinFace( int i, int j, int k );

    const std::vector< std::vector< Property > >& getProperties( ) const;

    /**
     * Check if the x-direction is inverted or not.
     * @return - true if x-direction is inverted and false otherwise.
     */
    bool invertedX( ) const;

    /**
     * Check if the y-direction is inverted or not.
     * @return - true if y-direction is inverted and false otherwise.
     */
    bool invertedY( ) const;

    /**
     * Check if the z-direction is inverted or not.
     * @return - true if z-direction is inverted and false otherwise.
     */
    bool invertedZ( ) const;

    /**
     * Read the reservoir information.
     * @param path - path to the file that contains a reservoir mesh.
     * @return true if it is everything okay.
     */
    bool readReservoirGeresimFile( const std::string& path );

    /**
     * Read the reservoir information from a eclipse grid file.
     * @param path - path to the file that contains a reservoir mesh.
     * @return true if it is everything okay.
     */
    bool readReservoirEclipseGridFile( const std::string& path );

    /**
     * Rotate the data to turn it align with the xy-axis.
     * @param angle - angle to rotate.
     */
    void rotateData( double angle );

    void untranslateData( );

    /**
     * Destructor.
     */
    virtual ~ReservoirMesh( );
private:

    /**
     * Verify the inverted directions.
     */
    bool checkInvertedDirections( );

    /**
     * Compute the axis alignment bounding box to the rotated data.
     */
    void computeAABB( );
    void computeActiveAABB( );

    /**
     * Compute the average of coordinates Z.
     */
    void computeAverageZ( );

    /**
     * Compute the main direction x and y.
     */
    void computeMainDirections( );

    /**
     * Delete all allocated memory.
     */
    void freeMemory( );

    /**
     * Get face average z.
     * @param (i, j, k) - the cell position.
     * @param f - face to get the central points.
     * @return - the average of z on face.
     */
    double getFaceAverage( int i, int j, int k, int f, int d );

    /**
     * Get face central point. This is will be used to compute the main directions.
     * @param (i, j, k) - the cell position.
     * @param f - face to get the central points.
     * @return - the central point.
     */
    Point3D getFaceCentralPoint( int i, int j, int k, int f );

    /**
     * Preprocess the data: compute main directions, rotate the data, compute
     * the AABB.
     */
    bool preprocessReservoidData( );

    /**
     * Rotate the data to turn it align with the xy-axis.
     */
    void rotateData( );

    /**
     * Rotate a single point.
     * @param p - point to be rotated.
     * @param angle - angle to rotate.
     */
    void rotatePoint( Point3D& p, double angle );


    void translateData( );

private:
    /**
     * Mesh coordinates.
     */
    std::vector<Point3D> _coordinates;

    /**
     * Grid to store the mesh.
     */
    DsGrid3D<CellMesh> _mesh;

    /**
     * Each cell has a set a properties at each time.
     * Indexed by: [time][property][cell]
     */
    std::vector< std::vector< Property > > _props;

    /**
     * Number of active cells.
     */
    unsigned int _numActiveCells;

    /**
     * Number of time steps.
     */
    unsigned int _numSteps;
private:
    /**
     * Rotated data bounding box.
     */
    Point3D _AABB[2];
    Point3D _activeAABB[2];

    double _tx, _ty;

    /**
     * Determine if the directions are inverted or not.
     */
    bool _invX, _invY, _invZ;

    /**
     * Main directions x and y.
     */
    Point3D _mainDirections[2];

    /**
     * Angle that the data was rotated.
     */
    double _rotationAngle;

    /**
     * Average of z.
     */
    double _z;
};

#endif /* RESERVOIRMESH_H */

