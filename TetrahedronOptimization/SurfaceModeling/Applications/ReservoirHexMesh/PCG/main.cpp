/* 
 * File:   main.cpp
 * Author: jcoelho
 *
 * Created on August 3, 2016, 10:26 AM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "../grid3d.h"
#include "../../../../../libs/Timer/Timer.h"
#include "../Point3D.h"
#include "ReservoirCellMeshOptimizationPCG.h"
using namespace std;
using namespace MeshModeling;



void readFile( std::string name, DsGrid3D<Point3D>& grid, std::vector<bool>& activeCells )
{
    //Read and allocate data.
    std::ifstream in( name.c_str( ) );
    if (in.fail( ))
    {
        printf( "Error openning the file %s\n", name.c_str( ) );
        exit( 0 );
        return;
    }

    Timer t;
    printf( "Reading file...\n" );
    unsigned int numLines, numColumns, numDepth;
    unsigned int numCel = 0;

    //Read the header.
    in >> numLines >> numColumns >> numDepth;

    //Allocate memory to grid.
    grid.Resize( numLines, numColumns, numDepth );

    //Compute the number of cells;
    numCel = ( numLines - 1 ) * ( numColumns - 1 ) * ( numDepth - 1 );
    activeCells.resize( numCel, false );

    bool trash = false;
    for (unsigned int k = 0; k < numDepth; k++)
    {
        for (unsigned int j = 0; j < numColumns; j++)
        {
            for (unsigned int i = 0; i < numLines; i++)
            {
                Point3D p;
                in >> p.x[0] >> p.x[1] >> p.x[2] >> trash;
                grid.Set( i, j, k, p );
            }
        }
    }
    for (unsigned int i = 0; i < numCel; i++)
    {
        int ac;
        in >> ac;
        activeCells[i] = ac;
    }

    t.printTime( "    Total time" );
}



void writeFile( std::string name, DsGrid3D<Point3D>& grid, std::vector<bool>& activeCells )
{
    //Read and allocate data.
    std::ofstream out( ( name + ".out" ).c_str( ) );
    if (out.fail( ))
    {
        printf( "Error openning the file %s\n", name.c_str( ) );
        exit( 0 );
        return;
    }

    Timer t;
    printf( "\n\nWriting output file...\n" );

    out.precision( 9 );
    out << std::scientific;

    unsigned int ni = grid.Ni( );
    unsigned int nj = grid.Nj( );
    unsigned int nk = grid.Nk( );

    out << ni << " " << nj << " " << nk << std::endl;

    for (unsigned int k = 0; k < nk; k++)
    {
        for (unsigned int j = 0; j < nj; j++)
        {
            for (unsigned int i = 0; i < ni; i++)
            {
                out << grid.Get( i, j, k ).x[0] << " " << grid.Get( i, j, k ).x[1]
                    << " " << grid.Get( i, j, k ).x[2] << " " << 1 << std::endl;
            }
        }
    }
    for (unsigned int i = 0; i < activeCells.size( ); i++)
    {
        out << activeCells[i] << std::endl;
    }

    t.printTime( "    Total time" );
}



std::vector<std::string> getFilesNames( const std::string name )
{
    //Read and allocate data.
    std::ifstream in( name.c_str( ) );
    if (in.fail( ))
    {
        printf( "Error openning the file %s\n", name.c_str( ) );
        exit( 0 );
    }

    std::vector<std::string> files;
    std::string path;

    while (in >> path)
    {
        files.push_back( path );
    }

    return files;
}



/*
 * 
 */
int main( int argc, char** argv )
{
    if (argc < 2)
    {
        printf( "Usage: ./surfaceMoldeling model.coords" );
        return 0;
    }

    //Get the file names.
//    std::vector<std::string> files = getFilesNames( argv[1] );

//    for (unsigned int i = 0; i < files.size( ); i++)
    {
        std::cout << "\n\n\nProcessing file: " << argv[1] << std::endl;

        DsGrid3D<Point3D> grid;
        std::vector<bool> activeCells;
        Timer t;
        readFile( argv[1], grid, activeCells );

        ReservoirCellMeshOptimizationPCG res;
        res.computeReservoirCellMesh( grid, activeCells, 2 );

        t.printTime( "Total time" );

        writeFile( argv[1], grid, activeCells );
    }
    return 0;
}

