#include "squarematerialcolor.h"
#include "ui_squarematerialcolor.h"



SquareMaterialColor::SquareMaterialColor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SquareMaterialColor)
{
    ui->setupUi(this);
}

SquareMaterialColor::~SquareMaterialColor()
{
    delete ui;
}

QToolButton *SquareMaterialColor::getToolButtonSquareMaterialColor()
{
    return ui->toolButtonSquareMaterialColor;
}



