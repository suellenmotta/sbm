
#ifndef HORIZONSMATERIALS_H
#define HORIZONSMATERIALS_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QMessageBox>

#include <vector>
#include "savematerialsdata.h"

/******************************************************************************************************************
* - O programa faz uma Bezier quadrática quando 3 pontos são clicados
* - Quando 4 pontos são clicados, o programa faz uma Bezier cúbica
* - Os pontos podem ser editados clicando com o botão esquerdo do mouse e arrastando.
* - Com o botão direito, o programa volta e vai apagando o que já fez.
* ******************************************************************************************************************/
class HorizonsMaterials
    : public QOpenGLWidget
    , protected QOpenGLFunctions
{
Q_OBJECT

public:
    explicit HorizonsMaterials(QWidget* parent = 0);
    ~HorizonsMaterials();

    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void buildingHorizons(int numberUp, int numberDown);
    void colorHorizons(int numberUp, int numberDown);
    void setMaterialsVector(std::vector< SaveMaterialsData> materials);
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void setNumberHorizons(int Up,int Down);
    void setMaterialNewColor(QVector3D colorChange,SaveMaterialsData newmaterial);

signals:
 void updateMousePositionText(const QString& message);

private:

    bool Fill_indexes(int size);
    bool Fill_edges(int size);
    int SearchPoint(QVector3D point);
    int HorizonsUp;
    int HorizonsDown;
    bool inicio;

private:
    std::vector<unsigned int> triangles; //vetor com índices dos pontos dos horizontes
    std::vector<unsigned int> _edges; //vetor com índices dos pontos das margens dos horizontes
    std::vector< QVector3D > _points; //vetor de pontos dos horizontes
    std::vector< QVector3D> _color; //vetor de cores para cada horizontes
    std::vector< SaveMaterialsData> _materials; //Lista de todos os materiais existentes
    std::vector< SaveMaterialsData> _rectMaterials; //Materiais para cada Retângulo
    int rectpicked;
    int rectmove;
    QOpenGLShaderProgram* program;
    unsigned int pointsBuffer; //Buffer para os pontos, as retas e a Bezier
    QMatrix4x4 view;
    QMatrix4x4 proj;
    double _zoomaspect;
};

#endif // HORIZONSMATERIALS_H
