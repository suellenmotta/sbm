#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionParameters, SIGNAL(triggered(bool)),
            this, SLOT(onParameterButtonClicked()));
    connect(ui->actionExportar, SIGNAL(triggered(bool)),
            this, SLOT(onExportarButtonClicked()));



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onParameterButtonClicked()
{
    parameterWindow = new ParametersWindow();
    parameterWindow->setFixedSize(260,350);
    parameterWindow->show();
}

void MainWindow::onExportarButtonClicked()
{
    //Temporário para testes
    HorizonsUp=1;
    HorizonsDown=2;

    files = new Files();
    files->setNumberHorizons(HorizonsUp,HorizonsDown);
    //files->setFixedSize(500,500);
    files->show();
}

void MainWindow::on_actionSair_triggered()
{
    this->close();
}

void MainWindow::on_actionCarregar_Reservat_rio_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Reservatório"), "", tr("Arquivos de malha (*.pos)"));
}

void MainWindow::on_actionCarregar_Horizonte_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Carregar Horizontes"), "", tr("Arquivos de malha (*.pos)"));
}
