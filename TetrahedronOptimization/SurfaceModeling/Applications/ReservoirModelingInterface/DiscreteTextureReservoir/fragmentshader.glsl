#version 330 core

uniform vec3 color;
uniform float transparence;
out vec4 finalColor;

void main()
{
    finalColor = vec4(color, transparence);
}

