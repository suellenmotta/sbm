#ifndef SAVEMATERIALSDATA_H
#define SAVEMATERIALSDATA_H

#include <iostream>
#include <string>
#include <iostream>
#include <QVector3D>
#include <QColor>


class SaveMaterialsData
{
public:
    SaveMaterialsData();
    SaveMaterialsData(std::string name,double Poisson,double Young,QVector3D color);

private:
//    std::string comboBoxModel;
    double _Young;
    double _Poisson;
//    double doubleSpinBoxSpecificWeight;
//    double doubleSpinBoxCohesion;
//    double doubleSpinBoxDilationAngle;
//    double doubleSpinBoxFrictionAngle;
    std::string _name;
    QVector3D _colorMaterial;
    QColor _QcolorMaterial;

//    std::string comboBoxHydraulicModel;
    double doubleSpinBoxFluidSpecificWeight;
    double doubleSpinBoxDrySpecificWeight;

public:
    double getYoung() const;
    void setYoung(double value);
    double getPoisson() const;
    void setPoisson(double value);
    std::string getName() const;
    void setName(std::string value);
    QVector3D getColor() const;
    QColor getQColor() const;
    void setColor(QColor value);
//    double getDoubleSpinBoxSpecificWeight() const;
//    void setDoubleSpinBoxSpecificWeight(double value);
//    double getDoubleSpinBoxCohesion() const;
//    void setDoubleSpinBoxCohesion(double value);
//    double getDoubleSpinBoxDilationAngle() const;
//    void setDoubleSpinBoxDilationAngle(double value);
//    double getDoubleSpinBoxFrictionAngle() const;
//    void setDoubleSpinBoxFrictionAngle(double value);

//    double getDoubleSpinBoxFluidSpecificWeight() const;
//    void setDoubleSpinBoxFluidSpecificWeight(double value);
//    double getDoubleSpinBoxDrySpecificWeight() const;
//    void setDoubleSpinBoxDrySpecificWeight(double value);
};

#endif // SAVEMATERIALSDATA_H
