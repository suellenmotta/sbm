#include "horizonsmaterials.h"
#include<iostream>
#include <cfloat>
#include <QMouseEvent>
#include "math.h"
#include <QWheelEvent>
#include <QMouseEvent>

HorizonsMaterials::HorizonsMaterials(QWidget* parent)
    : QOpenGLWidget(parent),
      program(nullptr)
{
    rectpicked=-1;
    rectmove=-1;
    inicio=true;
    _zoomaspect=1;
}

HorizonsMaterials::~HorizonsMaterials()
{
    makeCurrent();
    doneCurrent();
    delete program;
}

void HorizonsMaterials::initializeGL()
{
    buildingHorizons(HorizonsUp,HorizonsDown);
    initializeOpenGLFunctions();

    makeCurrent();

    glViewport(0,0,width(),height());

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //Layout de ponto e linha:
    glEnable(GL_PROGRAM_POINT_SIZE);
    glEnable(GL_POINT_SMOOTH);
    glEnable( GL_LINE_SMOOTH);
    glPointSize(8.0f);

    program = new QOpenGLShaderProgram();
    //Vertex Shader
    if (program->addShaderFromSourceFile(QOpenGLShader::Vertex, "../DiscreteTextureReservoir/vertexShader.glsl"))
        printf("Vertex Shader added successfully\n");
    else
        printf("Problemas ao adicionar o vertex shader\n");

    //Fragment Shader
    if (program->addShaderFromSourceFile(QOpenGLShader::Fragment, "../DiscreteTextureReservoir/fragmentShader.glsl"))
        printf("Fragment Shader added successfully\n");
    else
        printf("Problemas ao adicionar o fragment shader\n");
    program->link();

    if (!program->isLinked())
    {
         printf("O programa não foi linkado");
    }

    glGenBuffers(1, &pointsBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
    glBufferData(GL_ARRAY_BUFFER, _points.size()*sizeof(QVector3D),
                    &_points[0],GL_STATIC_DRAW);
    proj.ortho(-10,10,-10,10,-1,1);
}

void HorizonsMaterials::paintGL()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glBindBuffer(GL_ARRAY_BUFFER, pointsBuffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    program->bind();

   if(!_points.empty() && !triangles.empty())
    {
       program->setUniformValue("transformMatrix", proj*view);
       int indice;
       if(inicio)
       {
           colorHorizons(HorizonsUp,HorizonsDown);
           inicio=false;

       }
       for(unsigned int i=indice=0;i<triangles.size();i=i+6)
       {
           program->setUniformValue("color",QVector3D(_color[indice])); // Vermelho
           if(rectpicked==indice)
           {
               program->setUniformValue("transparence", float(0.7));
           }
           else if (rectmove==indice)
           {
               program->setUniformValue("transparence", float(0.7));
           }
           else
           {
               program->setUniformValue("transparence", float(1));
           }
           glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
           glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, &triangles[i]);
           indice++;
       }


       program->setUniformValue("color", QVector3D(0,0,0)); // Vermelho
       program->setUniformValue("transparence",float(1.0)); // Vermelho
       glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
       glDrawElements(GL_LINE_STRIP, _edges.size(), GL_UNSIGNED_INT, &_edges[0]);

    }
   update();
}

void HorizonsMaterials::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}

bool HorizonsMaterials:: Fill_indexes(int size)
{
    triangles.push_back( size - 4 );
    triangles.push_back( size - 3 );
    triangles.push_back( size - 2 );

    triangles.push_back( size - 4 );
    triangles.push_back( size - 2 );
    triangles.push_back( size - 1 );
    return true;
}

bool HorizonsMaterials:: Fill_edges(int size)
{
    _edges.push_back( size - 4 );
    _edges.push_back( size - 3 );
    _edges.push_back( size - 2 );
    _edges.push_back( size - 1 );
    return true;
}

void Print_Coordinates(std::vector< QVector3D > _points,std::vector<unsigned int> triangles)
{
    printf("Coords:\n");
    for (unsigned int i = 0; i < _points.size(); i++)
    {
        printf("%u: (%.2lf, %.2lf, %.2lf)\n", i, _points[i].x(), _points[i].y(), _points[i].z());
    }

    printf("Coords:\n");
    for (unsigned int i = 0; i < triangles.size() / 3; i++)
    {
        printf("%u: (%d, %d, %d)\n", i, triangles[3 * i + 0], triangles[3 * i + 1], triangles[3 * i + 2]);
    }
}

void HorizonsMaterials::buildingHorizons(int numberUp,int numberDown)
{
   double xMin = -5.0, xMax = +5.0, yMin = -5.0, yMax = +5.0;

   //Slices' number.
   double Ydivisions = numberDown + numberUp + 1 ;
   double yIncrement = (yMax - yMin) / Ydivisions;
   int slice = 0;
   int size;
   //Reservoir up.
   for (int i = 0; i < numberUp; i++)
   {
       double y0 = yMax - yIncrement * slice;
       double y1 = yMax - yIncrement * (slice + 1);
       _points.push_back( QVector3D( xMin, y0, 0 ) ); //-4
       _points.push_back( QVector3D( xMax, y0, 0 ) ); //-3
       _points.push_back( QVector3D( xMax, y1, 0 ) ); //-2
       _points.push_back( QVector3D( xMin, y1, 0 ) ); //-1
        slice++;

        //Preenchendo indices
        size = _points.size();
        Fill_indexes(size);

        //Drawing Edges
        Fill_edges(size);
   }

   //Resevoir.
   double y0 = yMax - yIncrement * slice;
   double y1 = yMax - yIncrement * (slice + 1);
   double x0 = xMin + 0.25 * (xMax - xMin);
   double x1 = xMin + 0.75 * (xMax - xMin);
   _points.push_back( QVector3D( xMin, y0, 0 ) );
   _points.push_back( QVector3D( x0, y0, 0 ) );
   _points.push_back( QVector3D( x0, y1, 0 ) );
   _points.push_back( QVector3D( xMin, y1, 0 ) );

   //Preenchendo indices
   size = _points.size();
   Fill_indexes(size);

   //Drawing Edges
   Fill_edges(size);
   _edges.push_back( size - 4 );

   _points.push_back( QVector3D( x0, y0, 0 ) );
   _points.push_back( QVector3D( x1, y0, 0 ) );
   _points.push_back( QVector3D( x1, y1, 0 ) );
   _points.push_back( QVector3D( x0, y1, 0 ) );

   //Preenchendo indices
   size = _points.size();
   Fill_indexes(size);

   //Drawing Edges
    Fill_edges(size);
    _edges.push_back( size - 4 );

   _points.push_back( QVector3D( x1, y0, 0 ) );
   _points.push_back( QVector3D( xMax, y0, 0 ) );
   _points.push_back( QVector3D( xMax, y1, 0 ) );
   _points.push_back( QVector3D( x1, y1, 0 ) );

   //Preenchendo indices
   size = _points.size();
   Fill_indexes(size);

   //Drawing Edges
   Fill_edges(size);

   slice++;
   //Reservoir down.
   for (int i = 0; i < numberDown; i++)
   {
       double y0 = yMax - yIncrement * slice;
       double y1 = yMax - yIncrement * (slice + 1);
       _points.push_back( QVector3D( xMin, y0, 0 ) );
       _points.push_back( QVector3D( xMax, y0, 0 ) );
       _points.push_back( QVector3D( xMax, y1, 0 ) );
       _points.push_back( QVector3D( xMin, y1, 0 ) );
        slice++;

       //Preenchendo indices
        int size = _points.size();
        Fill_indexes(size);

        //Drawing Edges
        Fill_edges(size);
   }

   Print_Coordinates(_points,triangles);
}

void HorizonsMaterials::colorHorizons(int numberUp,int numberDown)
{
    for(int i = 0; i < numberUp+numberDown+3; i++)
    {
        _rectMaterials.push_back(_materials[0]);
    }

    QVector3D colormaterials=_materials[0].getColor();
    _color.clear();

   //Reservoir up.
   for (int i = 0; i < numberUp; i++)
   {
       _color.push_back(colormaterials);
   }

   //Resevoir.
   _color.push_back(colormaterials);
   _color.push_back(colormaterials);
   _color.push_back(colormaterials);

   //Reservoir down.
   for (int i = 0; i < numberDown; i++)
   {
       _color.push_back(colormaterials);
   }
}

void HorizonsMaterials::mouseMoveEvent(QMouseEvent *event)
{
    QVector3D point( event->x(), height()-event->y(), 0 ); // Pegando o ponto que está na tela
    point = point.unproject( view, proj, QRect(0,0,width(),height()));
    point.setZ(0.f);
    rectmove=SearchPoint(point);
    update();
}

void HorizonsMaterials::mousePressEvent(QMouseEvent *event)
{
    QVector3D point( event->x(), height()-event->y(), 0 ); // Pegando o ponto que está na tela
    point = point.unproject( view, proj, QRect(0,0,width(),height()));
    point.setZ(0.f);
    rectpicked=SearchPoint(point);
    update();  
}

int HorizonsMaterials::SearchPoint(QVector3D point)
{
    int ind1,ind2;
    unsigned int i,ret;
    for(i=0,ret=0;i<triangles.size();i=i+6,ret++)
    {
        ind1=triangles[i+1];
        ind2=triangles[i+5];
        if(point.x()<_points[ind1].x() && point.y()<_points[ind1].y() && point.x()>_points[ind2].x() && point.y()>_points[ind2].y())
        {
            return ret;

        }
    }
    return -1;
}

void HorizonsMaterials:: setMaterialsVector(std::vector<SaveMaterialsData> materials)
{
    _materials=materials;
}

void HorizonsMaterials:: setNumberHorizons(int Up,int Down)
{
    HorizonsUp=Up;
    HorizonsDown=Down;
}

 void HorizonsMaterials:: setMaterialNewColor(QVector3D colorChange,SaveMaterialsData newmaterial)
 {
     std::cout<<"size "<<_rectMaterials.size()<<std::endl;
     std::cout<<"Antes Rect "<<rectpicked<<" era "<<_rectMaterials[rectpicked].getName()<<std::endl;
     _rectMaterials[rectpicked]=newmaterial;
     std::cout<<"Agora Rect "<<rectpicked<<" eh "<<_rectMaterials[rectpicked].getName()<<std::endl;
     _color[rectpicked]=colorChange;
 }
 void HorizonsMaterials::wheelEvent(QWheelEvent *event)
 {
     if(event->delta() > 0)
     {
         _zoomaspect*=0.8;
         proj.setToIdentity();
         proj.ortho(_zoomaspect*(-10),_zoomaspect*10,_zoomaspect*(-10),_zoomaspect*10,-1,1);
     }
     else if(event->delta() < 0)
     {
         _zoomaspect/=0.8;
         proj.setToIdentity();
         proj.ortho(_zoomaspect*(-10),_zoomaspect*10,_zoomaspect*(-10),_zoomaspect*10,-1,1);

     }

     update();
 }
