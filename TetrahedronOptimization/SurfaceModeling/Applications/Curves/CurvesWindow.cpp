/* 
 * File:   IupGLCanvasDummy.cpp
 * Author: jeferson
 * 
 * Created on August 31, 2014, 9:28 AM
 */

//#include <GL/glew.h>
//#include <GL/glew.h>

#include <cstdlib>
#include <cstdio>
#include <iup/iup.h>
#include <iup/iupgl.h>
#include <omp.h>
#include "CurvesWindow.h"
#include <fstream>
#include <cmath>
#include <cstring>
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include "../../ModelManager/OptimizationModel.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientSLUOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/GradientDescentOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"

Ihandle* canvas = 0;
using namespace Solver;
using namespace ModelManager;
using namespace std;



CurvesWindow::CurvesWindow( )
{
    //Allocate _solver[_currentRunIndex] and model.
    _currentRunIndex = 0;
    
    _solver[0] = 0;
    _m[0] = 0;
    _solver[1] = 0;
    _m[1] = 0;
    _solver[2] = 0;
    _m[2] = 0;
    
    _mousePressed = 1;
    _continueSolver = false;
    _isNewCurve = true;


    _pMin.x = -1;
    _pMax.x = +1;
    _pMin.y = -1;
    _pMax.y = +1;

    //Cria janela e define suas configuracoes.
    createWindow( );

    initialize( );
}



Ihandle* CurvesWindow::createMenus( )
{
    //Create an item to open a mesh.
    Ihandle *openData = IupItem( "&Open Points Data", NULL );

    //Create an item to close the window.
    Ihandle *exitWindow = IupItem( "&Exit", NULL );

    //Create an item to save a print screen image.
    Ihandle *savePrint = IupItem( "Salvar &Print", NULL );

    //Create the file menu.
    Ihandle *fileMenu = IupMenu( openData, savePrint, IupSeparator( ), exitWindow, NULL );

    //Create a submenu for menu file.
    Ihandle *fileSubMenu = IupSubmenu( "Arquivo", fileMenu );

    //Create an item to set a color for known points.
    Ihandle *knownPoints = IupItem( "Known Points Color", NULL );

    //Create an item to set a color for border constraints.
    Ihandle *aspectRatio = IupItem( "Border Constraints Color", NULL );

    //Create an edit menu.
    Ihandle *editMenu = IupMenu( knownPoints, aspectRatio, NULL );

    //Create an edit submenu.
    Ihandle *editSubMenu = IupSubmenu( "Edit", editMenu );


    //Cria menu princial.
    Ihandle *mainMenu = IupMenu( fileSubMenu, editSubMenu, NULL );

    //Create the main menu.
    IupSetAttribute( openData, IUP_IMAGE, "IUP_FileOpen" );
    IupSetAttribute( savePrint, IUP_IMAGE, "IUP_FileSave" );
    IupSetAttribute( knownPoints, IUP_IMAGE, "IUP_ToolsColor" );
    IupSetAttribute( aspectRatio, IUP_VALUE, IUP_OFF );

    //Define callbacks do menu.
    IupSetCallback( openData, IUP_ACTION, ( Icallback ) openDataMenuCallback );

    //Define callback do botao sair.
    IupSetCallback( exitWindow, IUP_ACTION, ( Icallback ) exitButtonCallback );

    //Retorna menu principal.
    return mainMenu;
}



void CurvesWindow::createWindow( )
{
    //Cria botao de sair.
    Ihandle *exitButton = IupButton( "Sair", NULL );

    //Cria botao de run.
    Ihandle *runButton = IupButton( "Run", NULL );

    //Cria botao de run x.
    Ihandle *runXButton = IupButton( "Run X", NULL );

    //Cria botao de run y.
    Ihandle *runYButton = IupButton( "Run Y", NULL );

    //Cria botao de stop.
    Ihandle *stopButton = IupButton( "Stop", NULL );

    //Cria botao de stop.
    Ihandle *newButton = IupButton( "New", NULL );

    //Cria output label da iteração corrente.
    Ihandle *curInterationLabel = IupLabel( "Current Iteration:" );

    //Cria gradient norm label.
    Ihandle *gNormLabel = IupLabel( "Gradient Norm:" );

    //Cria eps label .
    Ihandle *epsLabel = IupLabel( "EPS: " );

    //Cria max label .
    Ihandle *maxLabel = IupLabel( "Max Iterations: " );

    //Cria Number of points per interval label .
    Ihandle *nPointsPerIntervalLabel = IupLabel( "Number of points per interval: " );

    //Cria method label .
    Ihandle *methodLabel = IupLabel( "Solver: " );


    //Cria eps norm text field.
    Ihandle *epsText = IupText( "  " );

    //Cria max text field.
    Ihandle *maxText = IupText( "  " );

    //Cria max text field.
    Ihandle *nPointsPerIntevalText = IupText( "  " );


    //Cria method list.
    Ihandle* solverList = IupList( "Select" );

    Ihandle* initList = IupList( "Select" );


    //Cria canvas.
    canvas = IupGLCanvas( NULL );


    //Cria composicao final.
    Ihandle *vboxOutput = IupVbox( curInterationLabel, gNormLabel, NULL );

    //Cria composicao dos botoes de controle (run e stop)
    Ihandle *hboxCtrlButtons = IupHbox( runButton, runXButton, runYButton, stopButton, newButton, NULL );

    //Cria composicao botões.
    Ihandle *vboxButton = IupVbox( exitButton, NULL );

    //Cria composicao parte inferior.
    Ihandle *hboxFotter = IupHbox( vboxOutput, IupFill( ), vboxButton, NULL );

    //Cria composição dos inputs
    Ihandle *hboxInputs = IupHbox( IupVbox( methodLabel, solverList, NULL ),
                                   IupVbox( maxLabel, maxText, NULL ),
                                   IupVbox( epsLabel, epsText, NULL ),
                                   IupVbox( nPointsPerIntervalLabel, nPointsPerIntevalText, NULL ),
                                   IupVbox( IupLabel( "Solucao Inicial:" ), initList, NULL ), NULL );

    //Cria composicao parte superior.
    Ihandle *vboxTop = IupVbox( hboxInputs, hboxCtrlButtons, NULL );

    //Cria composicao final.
    Ihandle *vboxFinal = IupVbox( vboxTop, canvas, hboxFotter, NULL );

    //Cria dialogo.
    _dialog = IupDialog( vboxFinal );

    //Cria conjunto de menus.
    Ihandle *mainMenu = createMenus( );

    //Define handles dos elementos dos menus.
    IupSetHandle( "menuBarWindow", mainMenu );

    //Define os atributos dos botoes
    IupSetAttribute( exitButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( exitButton, IUP_TIP, "Fecha a janela." );
    IupSetAttribute( runButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( runButton, IUP_TIP, "Comeca otimizacao." );
    IupSetAttribute( runXButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( runXButton, IUP_TIP, "Comeca otimizacao para X." );
    IupSetAttribute( runYButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( runYButton, IUP_TIP, "Comeca otimizacao para Y." );
    IupSetAttribute( stopButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( stopButton, IUP_TIP, "Para otmizacao." );

    //Define da list
    IupSetAttributes( solverList, "1=\"LBFGS\", 2=\"CG LIS\", 3=\"CG. SLU\", 4=\"G. Descent\", DROPDOWN=YES" );
    IupSetAttribute( solverList, IUP_VALUE, "1" );
    IupSetAttributes( initList, "1=\"Zero\", 2=\"Corrente\", DROPDOWN=YES" );
    IupSetAttribute( initList, IUP_VALUE, "1" );

    //Define os atributos do canvas.
    IupSetAttribute( canvas, IUP_RASTERSIZE, "600x600" );
    IupSetAttribute( canvas, IUP_BUFFER, IUP_DOUBLE );
    IupSetAttribute( canvas, IUP_EXPAND, IUP_YES );

    IupSetAttribute( hboxInputs, IUP_GAP, "20" );
    IupSetAttribute( hboxCtrlButtons, IUP_GAP, "20" );
    IupSetAttribute( vboxFinal, IUP_MARGIN, "10x5" );

    IupSetAttribute( epsText, IUP_MASK, IUP_MASK_EFLOAT );
    IupSetAttribute( maxText, IUP_MASK, IUP_MASK_UINT );
    IupSetAttribute( epsText, IUP_VALUE, "1e-10" );
    IupSetAttribute( maxText, IUP_VALUE, "10000" );
    IupSetAttribute( nPointsPerIntevalText, IUP_VALUE, "100" );



    //Define propriedades do dialogo.
    IupSetAttribute( _dialog, IUP_TITLE, "Curves Window" );
    IupSetAttribute( _dialog, "THIS", ( char* ) this );
    IupSetAttribute( _dialog, "CANVAS", ( char* ) canvas );
    IupSetAttribute( _dialog, "SOLVER_LIST", ( char* ) solverList );
    IupSetAttribute( _dialog, "INIT_LIST", ( char* ) initList );
    IupSetAttribute( _dialog, "EPS_TEXT", ( char* ) epsText );
    IupSetAttribute( _dialog, "MAX_TEXT", ( char* ) maxText );
    IupSetAttribute( _dialog, "NPOINTS_TEXT", ( char* ) nPointsPerIntevalText );
    IupSetAttribute( _dialog, "STOP_BTN", ( char* ) stopButton );
    IupSetAttribute( _dialog, "RUN_BTN", ( char* ) runButton );
    IupSetAttribute( _dialog, "RUNX_BTN", ( char* ) runXButton );
    IupSetAttribute( _dialog, "RUNY_BTN", ( char* ) runYButton );
    IupSetAttribute( _dialog, "NEW_BTN", ( char* ) newButton );
    IupSetAttribute( _dialog, "GNORM_LABEL", ( char* ) gNormLabel );
    IupSetAttribute( _dialog, "ITERATION_LABEL", ( char* ) curInterationLabel );
    IupSetAttribute( _dialog, IUP_MENU, "menuBarWindow" );

    //Define callbacks do botao.
    IupSetCallback( _dialog, IUP_CLOSE_CB, ( Icallback ) exitButtonCallback );
    IupSetCallback( exitButton, IUP_ACTION, ( Icallback ) exitButtonCallback );
    IupSetCallback( runButton, IUP_ACTION, ( Icallback ) runButtonCallback );
    IupSetCallback( runXButton, IUP_ACTION, ( Icallback ) runXButtonCallback );
    IupSetCallback( runYButton, IUP_ACTION, ( Icallback ) runYButtonCallback );
    IupSetCallback( stopButton, IUP_ACTION, ( Icallback ) stopButtonCallback );
    IupSetCallback( newButton, IUP_ACTION, ( Icallback ) newButtonCallback );

    //Define as callbacks do canvas.
    IupSetCallback( canvas, IUP_ACTION, ( Icallback ) actionCanvasCallback );
    IupSetCallback( initList, IUP_ACTION, ( Icallback ) actionInitListCallback );
    IupSetCallback( solverList, IUP_ACTION, ( Icallback ) actionSolverListCallback );
    IupSetCallback( canvas, IUP_RESIZE_CB, ( Icallback ) resizeCanvasCallback );
    IupSetCallback( canvas, IUP_BUTTON_CB, ( Icallback ) buttonCanvasCallback );
    IupSetCallback( canvas, IUP_MOTION_CB, ( Icallback ) motionCanvasCallback );
    IupSetCallback( canvas, IUP_WHEEL_CB, ( Icallback ) wheelCanvasCallback );

    //Desabilta campos inicialmente
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( nPointsPerIntevalText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_NO );



    //Mapeia o dialogo.
    IupMap( _dialog );

    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );
}



CurvesWindow::~CurvesWindow( )
{
    delete _solver[0];
    delete _m[0];
    delete _solver[1];
    delete _m[1];
    delete _solver[2];
    delete _m[2];
    IupDestroy( _dialog );
}



void CurvesWindow::show( )
{
    IupShow( _dialog );
}



void CurvesWindow::hide( )
{
    IupHide( _dialog );
}



void CurvesWindow::initialize( )
{
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glPointSize( 4.5 );

    //Allocate solver..
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );

    int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
    allocateSolver( solverIndex );

    //initialize fixed points
    _nFixedPoints = 0;

}



unsigned int CurvesWindow::computeVertexIndex( unsigned int i, unsigned int j )
{
    return i * _columns + j;
}



void CurvesWindow::openData( const std::string& path, unsigned int initIndex )
{
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( _dialog, "NEW_BTN" );

    //clear all 
    newButtonCallback( newButton );

    //Read the file.
    readFile( path );
}



void CurvesWindow::readFile( string fileName )
{
    ifstream in( fileName );
    if (in.fail( ))
    {
        IupMessage( "Erro", "Erro ao abrir o arquivo!" );
        return;
    }

    in >> _nFixedPoints;

    //Allocate memory.
    _fixedPoints.clear( );

    _fixedPoints.resize( _nFixedPoints );

    for (int i = 0; i < _nFixedPoints; i++)
    {
        in >> _fixedPoints[i].x >> _fixedPoints[i].y;
    }
    Ihandle* canvas = ( Ihandle* ) IupGetAttribute( _dialog, "CANVAS" );

    renderFixedPoints( canvas );

    IupUpdate( canvas );
}



void CurvesWindow::updateWindowLabes( unsigned int numIter, double gnorm )
{
    Ihandle* g = ( Ihandle* ) IupGetAttribute( _dialog, "GNORM_LABEL" );
    Ihandle* iteration = ( Ihandle* ) IupGetAttribute( _dialog, "ITERATION_LABEL" );
    char gradient[1024], iter[1024];
    sprintf( gradient, "Gradient norm:      %.2e", gnorm );
    sprintf( iter, "Current Iteration:  %u", numIter );

    IupSetAttribute( g, IUP_TITLE, gradient );
    IupSetAttribute( iteration, IUP_TITLE, iter );
}



bool CurvesWindow::updateSolution( const std::vector<double>& v, double gnorm )
{
    _mtx.lock( );
    unsigned int total;
    if(_currentRunIndex == 0)
    {
        total = _coordinates.size( );
    }
    else
    {
        total = _m[_currentRunIndex]->getNumberVariables( );
    }
    //Get the number of threads.
    const int tMax = omp_get_max_threads( );
    const int numThreads = std::max( tMax - 2, 1 );

#pragma omp parallel for num_threads(numThreads)
    for (unsigned int i = 0; i < total; i++)
    {
        if(_currentRunIndex == 0)
        {
            _coordinates[i] = v[i];
        }
        else if(_currentRunIndex == 1)
        {
            _coordinates[ 2 * i + 0] = v[i];
        }
        else if(_currentRunIndex == 2)
        {
            _coordinates[ 2 * i + 1] = v[i];
        }
    }

    //updateWindowLabes( _solver[_currentRunIndex]->getNumberIterations( ), gnorm );
    _mtx.unlock( );
    return _continueSolver;
}



void CurvesWindow::renderScene( )
{
    //define que a cor para limpar a janela eh branca
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );

    //Limpa a janela com a cor pre-determinada
    glClear( GL_COLOR_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    glColor3f( 0, 0, 1 );
    glBegin( GL_LINE_STRIP );

    for (unsigned int i = 0; i < _coordinates.size( ) / 2; i++)
    {
        glVertex2d( _coordinates[2 * i + 0], _coordinates[2 * i + 1] );

    }

    glEnd( );

    glColor3f( 1, 0, 0 );
    glBegin( GL_POINTS );

    for (unsigned int i = 0; i < _fixedPoints.size( ); i++)
    {
        glVertex2d( _fixedPoints[i].x, _fixedPoints[i].y );
    }

    glEnd( );

}



bool CurvesWindow::displayFunction( const std::vector<double>& v, double gnorm )
{
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );
    return window->updateSolution( v, gnorm );
}



void CurvesWindow::buildModel( )
{
    
    delete _m[_currentRunIndex];
    _m[_currentRunIndex] = 0;
    
    //Allocate solver.
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );

    int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
    allocateSolver( solverIndex );


    //create model
    _m[_currentRunIndex] = new OptimizationModel( _solver[_currentRunIndex] );
    //    _m[_currentRunIndex]->showLog( false );



    switch (_currentRunIndex)
    {
        case 0:
        {
            //read and allocate data
            std::vector<Variable> allPointsX( _totalPoints );
            std::vector<Variable> allPointsY( _totalPoints );
            //Create variables 
            for (int i = 0; i < _totalPoints; i++)
            {
                allPointsX[i] = _m[_currentRunIndex]->addVariable( );
                allPointsY[i] = _m[_currentRunIndex]->addVariable( );
            }

            //Build the objective function.
            QuadraticExpression &obj = _m[_currentRunIndex]->getObjectiveFunction( );
            LinearExpression x = allPointsX[0] - allPointsX[1];
            LinearExpression y = allPointsY[0] - allPointsY[1];

            obj += x * x + y * y;
            for (int i = 1; i < ( _totalPoints - 1 ); i++)
            {
                x = allPointsX[i - 1] - 2 * allPointsX[i] + allPointsX[i + 1];
                y = allPointsY[i - 1] - 2 * allPointsY[i] + allPointsY[i + 1];

                obj += x * x + y * y;
            }
            x = allPointsX[_totalPoints - 2] - allPointsX[_totalPoints - 1];
            y = allPointsY[_totalPoints - 2] - allPointsY[_totalPoints - 1];

            obj += x * x + y * y;
            _m[_currentRunIndex]->setObjectiveFunction( obj );


            //Add constraints.
            _m[_currentRunIndex]->addConstraint( allPointsX[0], EQUAL, _fixedPoints[0].x );
            _m[_currentRunIndex]->addConstraint( allPointsY[0], EQUAL, _fixedPoints[0].y );
            int countP = 0;
            for (int i = 1; i < ( _nFixedPoints - 1 ); i++)
            {
                //antes quando era pela distancia
                int nPointsInterval = _totalPoints / ( _nFixedPoints - 1 );
                int index = countP + nPointsInterval;
                _m[_currentRunIndex]->addConstraint( allPointsX[index], EQUAL, _fixedPoints[i].x );
                _m[_currentRunIndex]->addConstraint( allPointsY[index], EQUAL, _fixedPoints[i].y );
                countP += nPointsInterval;
            }
            _m[_currentRunIndex]->addConstraint( allPointsX[_totalPoints - 1], EQUAL, _fixedPoints[_nFixedPoints - 1].x );
            _m[_currentRunIndex]->addConstraint( allPointsY[_totalPoints - 1], EQUAL, _fixedPoints[_nFixedPoints - 1].y );
            break;
        }

        case 1:
        {
            //read and allocate data
            std::vector<Variable> allPointsX( _totalPoints );

            //Create variables 
            for (int i = 0; i < _totalPoints; i++)
            {
                allPointsX[i] = _m[_currentRunIndex]->addVariable( );
            }

            //Build the objective function.
            QuadraticExpression &obj = _m[_currentRunIndex]->getObjectiveFunction( );
            LinearExpression x = allPointsX[0] - allPointsX[1];

            obj += x * x;
            for (int i = 1; i < ( _totalPoints - 1 ); i++)
            {
                x = allPointsX[i - 1] - 2 * allPointsX[i] + allPointsX[i + 1];

                obj += x * x;
            }
            x = allPointsX[_totalPoints - 2] - allPointsX[_totalPoints - 1];

            obj += x * x;
            _m[_currentRunIndex]->setObjectiveFunction( obj );


            //Add constraints.
            _m[_currentRunIndex]->addConstraint( allPointsX[0], EQUAL, _fixedPoints[0].x );
            int countP = 0;
            for (int i = 1; i < ( _nFixedPoints - 1 ); i++)
            {
                //antes quando era pela distancia
                int nPointsInterval = _totalPoints / ( _nFixedPoints - 1 );
                int index = countP + nPointsInterval;
                _m[_currentRunIndex]->addConstraint( allPointsX[index], EQUAL, _fixedPoints[i].x );
                countP += nPointsInterval;
            }
            _m[_currentRunIndex]->addConstraint( allPointsX[_totalPoints - 1], EQUAL, _fixedPoints[_nFixedPoints - 1].x );
            break;
        }

        case 2:
        {
            //read and allocate data
            std::vector<Variable> allPointsY( _totalPoints );

            //Create variables 
            for (int i = 0; i < _totalPoints; i++)
            {
                allPointsY[i] = _m[2]->addVariable( );
            }

            //Build the objective function.
            QuadraticExpression &obj = _m[2]->getObjectiveFunction( );
            LinearExpression y = allPointsY[0] - allPointsY[1];

            obj += y * y;
            for (int i = 1; i < ( _totalPoints - 1 ); i++)
            {
                y = allPointsY[i - 1] - 2 * allPointsY[i] + allPointsY[i + 1];

                obj += y * y;
            }
            y = allPointsY[_totalPoints - 2] - allPointsY[_totalPoints - 1];

            obj += y * y;
            _m[2]->setObjectiveFunction( obj );


            //Add constraints.
            _m[2]->addConstraint( allPointsY[0], EQUAL, _fixedPoints[0].y );
            int countP = 0;
            for (int i = 1; i < ( _nFixedPoints - 1 ); i++)
            {
                //antes quando era pela distancia
                int nPointsInterval = _totalPoints / ( _nFixedPoints - 1 );
                int index = countP + nPointsInterval;
                _m[2]->addConstraint( allPointsY[index], EQUAL, _fixedPoints[i].y );
                countP += nPointsInterval;
            }
            _m[2]->addConstraint( allPointsY[_totalPoints - 1], EQUAL, _fixedPoints[_nFixedPoints - 1].y );
            break;
        }
    }
    
    //set Steps Number
    switch (solverIndex)
    {
        case 1:
            _m[_currentRunIndex]->setStepsNumber( 10 );
            break;
        case 2:
            _m[_currentRunIndex]->setStepsNumber( 1 );
            break;
        case 3:
            _m[_currentRunIndex]->setStepsNumber( 1 );
            break;
        case 4:
            _m[_currentRunIndex]->setStepsNumber( 25 );
            break;
    }
}



void CurvesWindow::resizeCanvas( int width, int height )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    double widthFactor = ( double ) width / viewport[2];
    double heightFactor = ( double ) height / viewport[3];

    //Define que toda a janela sera usada para desenho. Isso eh feito atras da
    //definicao do viewport, que recebe o ponto inferior esquerdo, a largura e
    //a altura da janela.
    glViewport( 0, 0, width, height );

    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( _pMin.x - _pMax.x );
    double heightWorld = fabs( _pMin.y - _pMax.y );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( _pMin.x + _pMax.x ) / 2.0;
    double cy = ( _pMin.y + _pMax.y ) / 2.0;

    //calcula o novo sistema de coordenadas
    _pMin.x = cx - 0.5 * widthWorld;
    _pMax.x = cx + 0.5 * widthWorld;
    _pMin.y = cy - 0.5 * heightWorld;
    _pMax.y = cy + 0.5 * heightWorld;

    //Define sistema de coordenadas.
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluOrtho2D( _pMin.x, _pMax.x, _pMin.y, _pMax.y );
}



int CurvesWindow::exitButtonCallback( Ihandle* button )
{
    //Get the this pointer.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_fut.valid( ))
    {
        window->_continueSolver = false;
        window->_fut.wait( );
    }
    return IUP_CLOSE;
}



void CurvesWindow::optimize( double eps, unsigned numIterations )
{
    //Set to solve continue solving the problem.
    _continueSolver = true;

    //Set the epsilon.
    _m[_currentRunIndex]->setSolverEps( eps );

    //The the max interation number.
    _m[_currentRunIndex]->setMaxIterations( numIterations );

    //Optimize the problem.
    _m[_currentRunIndex]->optimize( );
}



//void CurvesWindow::optimizeX( double eps, unsigned numIterations )
//{
//    //Set to solve continue solving the problem.
//    _continueSolver = true;
//
//    //Set the epsilon.
//    _solver[1]->setEPS( eps );
//
//    //The the max interation number.
//    _solver[1]->setMaxIterations( numIterations );
//
//    //Optimize the problem.
//    _m[1]->optimize( );
//}
//
//
//
//void CurvesWindow::optimizeY( double eps, unsigned numIterations )
//{
//    //Set to solve continue solving the problem.
//    _continueSolver = true;
//
//    //Set the epsilon.
//    _solver[2]->setEPS( eps );
//
//    //The the max interation number.
//    _solver[2]->setMaxIterations( numIterations );
//
//    //Optimize the problem.
//    _m[2]->optimize( );
//}



void CurvesWindow::activateComponents( Ihandle* component )
{
    //Habilita campos
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( component, "SOLVER_LIST" );
    Ihandle* initList = ( Ihandle* ) IupGetAttribute( component, "INIT_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( component, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( component, "MAX_TEXT" );
    Ihandle* nPointsPerIntervalText = ( Ihandle* ) IupGetAttribute( component, "NPOINTS_TEXT" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( component, "STOP_BTN" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( component, "RUN_BTN" );
    Ihandle* runXButton = ( Ihandle* ) IupGetAttribute( component, "RUNX_BTN" );
    Ihandle* runYButton = ( Ihandle* ) IupGetAttribute( component, "RUNY_BTN" );
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( component, "NEW_BTN" );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( nPointsPerIntervalText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( newButton, IUP_ACTIVE, IUP_YES );
}



void CurvesWindow::optimizeCurveCallback( )
{
    //Get the this pointer.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    //The eps and max interations.
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( canvas, "MAX_TEXT" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( canvas, "EPS_TEXT" );
    double eps = atof( IupGetAttribute( epsText, IUP_VALUE ) );
    unsigned int numIterations = atoi( IupGetAttribute( maxText, IUP_VALUE ) );

    //Optimize the problem.
    window->optimize( eps, numIterations );
}



//void CurvesWindow::optimizeCurveXCallback( )
//{
//    //Get the this pointer.
//    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );
//
//    //The eps and max interations.
//    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( canvas, "MAX_TEXT" );
//    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( canvas, "EPS_TEXT" );
//    double eps = atof( IupGetAttribute( epsText, IUP_VALUE ) );
//    unsigned int numIterations = atoi( IupGetAttribute( maxText, IUP_VALUE ) );
//
//    //Optimize the problem.
//    window->optimizeX( eps, numIterations );
//}
//
//
//
//void CurvesWindow::optimizeCurveYCallback( )
//{
//    //Get the this pointer.
//    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );
//
//    //The eps and max interations.
//    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( canvas, "MAX_TEXT" );
//    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( canvas, "EPS_TEXT" );
//    double eps = atof( IupGetAttribute( epsText, IUP_VALUE ) );
//    unsigned int numIterations = atoi( IupGetAttribute( maxText, IUP_VALUE ) );
//
//    //Optimize the problem.
//    window->optimizeY( eps, numIterations );
//}



int CurvesWindow::checkFunction( )
{
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );
    if (window->_fut.wait_for( std::chrono::milliseconds( 16 ) ) == std::future_status::ready)
    {
        //Enable run button. 
        window->activateComponents( window->_dialog );

        window->_fut.get( );
        IupSetFunction( IUP_IDLE_ACTION, NULL );
    }

    IupUpdate( canvas );

    return IUP_DEFAULT;
}



int CurvesWindow::runButtonCallback( Ihandle *button )
{

    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );
    window->_currentRunIndex = 0;
    window->run( );

    return IUP_DEFAULT;
}



int CurvesWindow::runXButtonCallback( Ihandle *button )
{

    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );

    window->_currentRunIndex = 1;
    window->run( );

    return IUP_DEFAULT;
}



int CurvesWindow::runYButtonCallback( Ihandle *button )
{

    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );

    window->_currentRunIndex = 2;
    window->run( );

    return IUP_DEFAULT;
}



int CurvesWindow::stopButtonCallback( Ihandle *button )
{
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );

    window->_continueSolver = false;

    //    for (unsigned int i = 0; i < window->_m[_currentRunIndex]->getNumberVariables( ); i++)
    //    {
    //        if (!window->_m[_currentRunIndex]->getVariable( i ).isKnownValue( ))
    //        {
    //            printf("%.2f %.2f ", window->_m[_currentRunIndex]->getVariable(i).getValue(), window->_coordinates[i]);
    //            window->_m[_currentRunIndex]->getVariable( i ) = window->_coordinates[i];
    //            printf("- %.2f %.2f \n", window->_m[_currentRunIndex]->getVariable(i).getValue(), window->_coordinates[i]);
    //        }
    //    }

    return IUP_DEFAULT;
}



int CurvesWindow::newButtonCallback( Ihandle *button )
{
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );

    window->_isNewCurve = true;

    delete window->_m[0];
    delete window->_m[1];
    delete window->_m[2];
    window->_m[0] = 0;
    window->_m[1] = 0;
    window->_m[2] = 0;
    window->_coordinates.clear( );
    window->_fixedPoints.clear( );

    window->initialize( );

    return IUP_DEFAULT;
}



int CurvesWindow::openDataMenuCallback( Ihandle* button )
{
    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( button, "THIS" );

    Ihandle *dlg = IupFileDlg( );

    IupSetAttribute( dlg, "DIALOGTYPE", "OPEN" );
    IupSetAttribute( dlg, "TITLE", "IupFileDlg Test" );
    IupPopup( dlg, IUP_CURRENT, IUP_CURRENT );

    if (IupGetInt( dlg, "STATUS" ) == -1)
    {
        IupDestroy( dlg );
        return IUP_DEFAULT;
    }

    //set as a new curve(linear)
    window->_isNewCurve = true;

    //Habilita campos necessarios
    window->activateComponents( button );

    string path = IupGetAttribute( dlg, "VALUE" );

    Ihandle* initList = ( Ihandle* ) IupGetAttribute( button, "INIT_LIST" );
    unsigned int initIndex = atoi( IupGetAttribute( initList, "VALUE" ) );
    window->openData( path, initIndex );

    IupDestroy( dlg );
    Ihandle* canvas = ( Ihandle* ) IupGetAttribute( button, "CANVAS" );
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void CurvesWindow::initializeSolution( unsigned int initIndex )
{
    int coordinatesSize;            
    if(_currentRunIndex == 0)
    {
        coordinatesSize = _m[_currentRunIndex]->getNumberVariables( );
    }
    else
    {
        coordinatesSize = 2*_m[_currentRunIndex]->getNumberVariables( );
    }          
    _coordinates.resize(coordinatesSize);
    if (initIndex == 1)
    {
        
        for (unsigned int i = 0; i < _m[_currentRunIndex]->getNumberVariables( ); i++)
        {
            if (!_m[_currentRunIndex]->getVariable( i ).isKnownValue( ))
            {
                _m[_currentRunIndex]->getVariable( i ) = 0.0;
                if(_currentRunIndex == 0)
                {                    
                    _coordinates[i] = 0.0;
                }
                else
                {
                    _coordinates[2 * i + 0] = 0.0;
                    _coordinates[2 * i + 1] = 0.0;
                }
            }
        }
    }
    else if (initIndex == 2 && !_isNewCurve)
    {
        for (unsigned int i = 0; i < _m[_currentRunIndex]->getNumberVariables( ); i++)
        {
            if (!_m[_currentRunIndex]->getVariable( i ).isKnownValue( ))
            {
                if(_currentRunIndex == 0)
                {
                    _m[_currentRunIndex]->getVariable( i ) = _coordinates[i];
                }
                else if(_currentRunIndex == 1)
                {
                    _m[_currentRunIndex]->getVariable( i ) = _coordinates[ 2 * i + 0];
                }
                else if(_currentRunIndex == 2)
                {
                    _m[_currentRunIndex]->getVariable( i ) = _coordinates[ 2 * i + 1];
                }
            }
        }

    }
    else if (initIndex == 2 || _isNewCurve)
    {
        int nPointsInterval = _totalPoints / ( _nFixedPoints - 1 );
        int intervalIndex = 0;
        
        _coordinates[ 0 ] = _fixedPoints[0].x;
        _coordinates[ 1 ] = _fixedPoints[0].y;
        
        for (int i = 1; i < ( _totalPoints - 1 ); i++)
        {
            //points inside interval goes from 0 to nPointsInterval-1
            //indexInsideInterval goes from 1 to nPointsInterval -1 
            double indexInsideInterval = i - ( intervalIndex ) * nPointsInterval;
            double t = indexInsideInterval / ( nPointsInterval );
            if (( intervalIndex + 1 ) * nPointsInterval == i)
            {
                _coordinates[ 2 * i + 1] = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;
                _coordinates[ 2 * i + 0] = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                
                intervalIndex++;
            }
            else
            {
                
                if(_currentRunIndex == 0)
                {
                    //x
                    if (!_m[_currentRunIndex]->getVariable( 2 * i + 0 ).isKnownValue( ))
                    {
                        _m[_currentRunIndex]->getVariable( 2 * i + 0 ) = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                        _coordinates[ 2 * i + 0] = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                    }

                    //y
                    if (!_m[_currentRunIndex]->getVariable( 2 * i + 1 ).isKnownValue( ))
                    {
                        _m[_currentRunIndex]->getVariable( 2 * i + 1 ) = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;
                        _coordinates[ 2 * i + 1] = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;
                    }
                }
                else if(_currentRunIndex == 1)
                {
                    //x
                    if (!_m[1]->getVariable( i ).isKnownValue( ))
                    {
                        _m[1]->getVariable( i ) = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                        _coordinates[ 2 * i + 0] = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                    }

                    //y
                    _coordinates[ 2 * i + 1] = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;               
                   }
                else if(_currentRunIndex == 2)
                {
                    //y
                    if (!_m[2]->getVariable( i ).isKnownValue( ))
                    {
                        _m[2]->getVariable( i ) = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;
                        _coordinates[ 2 * i + 1] = _fixedPoints[intervalIndex].y * ( 1 - t ) + _fixedPoints[intervalIndex + 1].y*t;
                    }

                    //x
                    _coordinates[ 2 * i + 0] = _fixedPoints[intervalIndex].x * ( 1 - t ) + _fixedPoints[intervalIndex + 1].x*t;
                }
            }
        }
        
        _coordinates[ 2 * _totalPoints - 2 ] = _fixedPoints[ _nFixedPoints - 1].x;
        _coordinates[ 2 * _totalPoints - 1 ] = _fixedPoints[ _nFixedPoints - 1].y;
        
    }

    //Set callback to update data.
    _m[_currentRunIndex]->setDisplayInformationCallback( displayFunction );

    //Define the solver.
    _m[_currentRunIndex]->setOptimizationSolver( _solver[_currentRunIndex] );
}





void CurvesWindow::allocateSolver( int index )
{
    delete _solver[_currentRunIndex];

    switch (index)
    {
        case 1:
            _solver[_currentRunIndex] = new LBFGSOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            //_m[_currentRunIndex]->setStepsNumber( 10 );
            break;
        case 2:
            _solver[_currentRunIndex] = new ConjugateGradientLISOptimizationSolver( );


            //Set the frequency that the solver must let the window knows about the
            //current solution.
            //_m[_currentRunIndex]->setStepsNumber( 1 );
            break;
        case 3:
            _solver[_currentRunIndex] = new ConjugateGradientSLUOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            //_m[_currentRunIndex]->setStepsNumber( 1 );
            break;
        case 4:
            _solver[_currentRunIndex] = new GradientDescentOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            //_m[_currentRunIndex]->setStepsNumber( 25 );
            break;
    }
}



//void CurvesWindow::allocateSolverX( int index )
//{
//    delete _solver[1];
//
//    switch (index)
//    {
//        case 1:
//            _solver[1] = new LBFGSOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[1]->setStepsNumber( 10 );
//            break;
//        case 2:
//            _solver[1] = new ConjugateGradientLISOptimizationSolver( );
//
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[1]->setStepsNumber( 1 );
//            break;
//        case 3:
//            _solver[1] = new ConjugateGradientSLUOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[1]->setStepsNumber( 1 );
//            break;
//        case 4:
//            _solver[1] = new GradientDescentOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[1]->setStepsNumber( 25 );
//            break;
//    }
//}
//
//
//
//void CurvesWindow::allocateSolverY( int index )
//{
//    delete _solver[2];
//
//    switch (index)
//    {
//        case 1:
//            _solver[2] = new LBFGSOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[2]->setStepsNumber( 10 );
//            break;
//        case 2:
//            _solver[2] = new ConjugateGradientLISOptimizationSolver( );
//
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[2]->setStepsNumber( 1 );
//            break;
//        case 3:
//            _solver[2] = new ConjugateGradientSLUOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[2]->setStepsNumber( 1 );
//            break;
//        case 4:
//            _solver[2] = new GradientDescentOptimizationSolver( );
//
//            //Set the frequency that the solver must let the window knows about the
//            //current solution.
//            _solver[2]->setStepsNumber( 25 );
//            break;
//    }
//}



void CurvesWindow::run( )
{
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( _dialog, "THIS" );

    Ihandle* nPointsIntervalText = ( Ihandle* ) IupGetAttribute( canvas, "NPOINTS_TEXT" );
    unsigned int nPointsPerInterval = atoi( IupGetAttribute( nPointsIntervalText, IUP_VALUE ) );
    window->_totalPoints = ( int ) ( window->_nFixedPoints - 1 ) * nPointsPerInterval;

    //Construct the model.
    window->buildModel( );

    Ihandle* initList = ( Ihandle* ) IupGetAttribute( _dialog, "INIT_LIST" );
    unsigned int initIndex = atoi( IupGetAttribute( initList, "VALUE" ) );
    //Initialize solution.
    window->initializeSolution( initIndex );

    window->_fut = std::async( std::launch::async, CurvesWindow::optimizeCurveCallback );
    IupSetFunction( IUP_IDLE_ACTION, ( Icallback ) checkFunction );

    //set that it is not linear any more.
    window->_isNewCurve = false;

    //Desabilta campos 
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( _dialog, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( _dialog, "MAX_TEXT" );
    Ihandle* nPointsPerIntervalText = ( Ihandle* ) IupGetAttribute( _dialog, "NPOINTS_TEXT" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUN_BTN" );
    Ihandle* runXButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUNX_BTN" );
    Ihandle* runYButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUNY_BTN" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( _dialog, "STOP_BTN" );
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( _dialog, "NEW_BTN" );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_VALUE, "2" );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( nPointsPerIntervalText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( newButton, IUP_ACTIVE, IUP_NO );
    
}



int CurvesWindow::actionInitListCallback( Ihandle* initList, char *text, int item, int state )
{
    //        if (state)
    //        {
    //            CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( initList, "THIS" );
    //    
    //            int initIndex = atoi( IupGetAttribute( initList, IUP_VALUE ) );
    //            window->initializeSolution( initIndex );
    //        }
    //        IupUpdate( canvas );

    return IUP_DEFAULT;
}



int CurvesWindow::actionSolverListCallback( Ihandle* solverList, char *text, int item, int state )
{
    if (state)
    {
        CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( solverList, "THIS" );

        int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
        window->allocateSolver( solverIndex );
    }
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



int CurvesWindow::actionCanvasCallback( Ihandle* canvas )
{
    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_coordinates.size( ) != 0)
    {

        //Torna o canvas como corrente.
        IupGLMakeCurrent( canvas );

        //Redesenha a janela.
        window->renderScene( );

        //Troca os buffers.
        IupGLSwapBuffers( canvas );
    }
    else
    {
        window->renderFixedPoints( canvas );
    }

    return IUP_DEFAULT;
}



int CurvesWindow::resizeCanvasCallback( Ihandle *canvas, int width, int height )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    //Redesenha a janela.
    window->resizeCanvas( width, height );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void CurvesWindow::zoom( int delta )
{
    //Calcula fator de zoom
    float factor = delta > 0 ? 1.05f : 0.95f;

    // Computa a coordenada do centro da área visível
    double cx = 0.5f * ( _pMin.x + _pMax.x );
    double cy = 0.5f * ( _pMin.y + _pMax.y );

    // Computa metade do altura de largura visível
    double halfWidth = 0.5f * ( _pMax.x - _pMin.x );
    double halfHeight = 0.5f * ( _pMax.y - _pMin.y );

    // Aplica o fator de escala para alterar a largura e altura
    halfWidth *= factor;
    halfHeight *= factor;

    _pMin.x = cx - halfWidth;
    _pMax.x = cx + halfWidth;
    _pMin.y = cy - halfHeight;
    _pMax.y = cy + halfHeight;

    //Define a matriz de projecao como matriz corrente. Essa matriz guarda informacoes
    //de como o desenho deve ser projetado na tela, desta forma qualquer definicao
    //relativa a sistema de coordenadas deve ser feita na matriz de projecao
    glMatrixMode( GL_PROJECTION );

    //Carrega a matriz indentida na matriz corrente. Esta operacao substitui a matriz
    //corrente pela matriz identidade
    glLoadIdentity( );

    //Define como o sistema de coordenadas varia dentro do viewport, ou seja,
    //da esquerda para direita o eixo x varia de x1 ate x2 e o eixo y varia de
    //y1 ate y2. Note que nao necessariamente x1 <= x2 ou y1 <= y2
    gluOrtho2D( _pMin.x, _pMax.x,
                _pMin.y, _pMax.y );
}



void CurvesWindow::convertPixelToWorld( int px, int py, double& x, double& y )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    //Converte ponto em tela para o sistema de coordenadas.
    double t = ( double ) px / ( viewport[2] - 1 );
    x = ( 1 - t ) * _pMin.x + t * _pMax.x;
    t = 1.0 - ( double ) py / ( viewport[3] - 1 );
    y = ( 1 - t ) * _pMin.y + t * _pMax.y;
}



void CurvesWindow::renderFixedPoints( Ihandle* canvas )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    //define que a cor para limpar a janela eh branca
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );

    //Limpa a janela com a cor pre-determinada
    glClear( GL_COLOR_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    glColor3f( 0, 0, 1 );
    glBegin( GL_LINE_STRIP );

    for (unsigned int i = 0; i < window->_fixedPoints.size( ); i++)
    {
        glVertex2d( window->_fixedPoints[i].x, window->_fixedPoints[i].y );
    }

    glEnd( );

    glColor3f( 1, 0, 0 );
    glBegin( GL_POINTS );

    for (unsigned int i = 0; i < window->_fixedPoints.size( ); i++)
    {
        glVertex2d( window->_fixedPoints[i].x, window->_fixedPoints[i].y );
    }

    glEnd( );

    //Troca os buffers.
    IupGLSwapBuffers( canvas );
}



int CurvesWindow::buttonCanvasCallback( Ihandle* canvas, int button, int pressed,
                                        int x, int y, char* status )
{
    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );
    if (button == IUP_BUTTON1)
    {
        if (pressed)
        {
            window->_firstPixelX = x;
            window->_firstPixelY = y;
            window->_mousePressed = IUP_BUTTON1;

            double xi = 0.0, yi = 0.0;
            window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );

            //range around the fixed point for selection
            double selectionRange = 0.05;

            //mark and get index if a fixed point is selected
            for (unsigned int i = 0; i < window->_fixedPoints.size( ); i++)
            {
                if (xi <= window->_fixedPoints[i].x + selectionRange &&
                    xi >= window->_fixedPoints[i].x - selectionRange &&
                    yi <= window->_fixedPoints[i].y + selectionRange &&
                    yi >= window->_fixedPoints[i].y - selectionRange)
                {
                    window->_fixedPointClicked = true;
                    window->_choosenPointIndex = i;
                }
            }
        }
        else
        {
            window->_mousePressed = -1;
            window->_fixedPointClicked = false;
        }
    }
    else if (button == IUP_BUTTON3)
    {
        if (pressed)
        {
            window->_nFixedPoints++;

            Point newPoint;
            window->convertPixelToWorld( x, y, newPoint.x, newPoint.y );
            window->_fixedPoints.push_back( newPoint );

            //only activate components for mor than 1 point
            if (window->_nFixedPoints >= 2)
            {
                window->activateComponents( canvas );

                window->renderFixedPoints( canvas );

                IupUpdate( canvas );
            }
        }
    }
    return IUP_DEFAULT;
}



int CurvesWindow::motionCanvasCallback( Ihandle* canvas, int x, int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_mousePressed == IUP_BUTTON1)
    {
        double xi = 0.0, yi = 0.0, xf = 0.0, yf = 0.0f;

        window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );
        window->convertPixelToWorld( x, y, xf, yf );

        if (window->_fixedPointClicked)
        {
            //change the fixed selected point to mouse position
            window->_fixedPoints[window->_choosenPointIndex].x = xf;
            window->_fixedPoints[window->_choosenPointIndex].y = yf;
            window->renderScene( );
        }
        else if (!window->_fixedPointClicked)
        {


            double dx = xf - xi;
            double dy = yf - yi;

            window->_pMin.x -= dx;
            window->_pMax.x -= dx;
            window->_pMin.y -= dy;
            window->_pMax.y -= dy;

            window->_firstPixelX = x;
            window->_firstPixelY = y;

            glMatrixMode( GL_PROJECTION );
            glLoadIdentity( );

            gluOrtho2D( window->_pMin.x, window->_pMax.x, window->_pMin.y, window->_pMax.y );
        }
    }

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}



int CurvesWindow::wheelCanvasCallback( Ihandle* canvas, float delta, int x,
                                       int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    CurvesWindow *window = ( CurvesWindow* ) IupGetAttribute( canvas, "THIS" );

    delta < 0 ? window->zoom( -1 ) : window->zoom( 1 );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}