/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: ncortez
 *
 * Created on July 18, 2016, 11:12 AM
 */

#include <cstdlib>
#include "CurvesWindow.h"
#include <iup/iup.h>
#include <iup/iupgl.h>

using namespace std;



int main( int argc, char** argv )
{
    //Inicializa a IUP.
    IupOpen( &argc, &argv );

    //Inicializa a OpenGL na IUP.
    IupGLCanvasOpen( );

    //Cria objeto.
    CurvesWindow *window = new CurvesWindow( );

    //Exibe a janela.
    window->show( );

    //Coloca a IUP em loop.
    IupMainLoop( );


    //Deleta o obejto alocado.
    delete window;

    //Fecha a IUP.
    IupClose( );

    return 0;
}

