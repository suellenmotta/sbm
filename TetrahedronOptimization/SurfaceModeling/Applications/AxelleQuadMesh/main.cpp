/* 
 * File:   main.cpp
 * Author: jcoelho
 *
 * Created on August 1, 2016, 9:54 AM
 */
//
//
//#include <cstdlib>
//#include <fstream>
//#include "../../ModelManager/OptimizationModel.h"
//#include "../../ModelManager/Variable.h"
//#include "../../Solver/OptimizationSolvers/ConjugateGradientSLUOptimizationSolver.h"
//#include "../../Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
//#include "../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"
//#include "../../../../libs/Timer/Timer.h"
//#include "Geometry.h"
//
//
//using namespace std;
//using namespace ModelManager;
//using namespace Solver;
//
//struct VarPoint
//{
//    Variable x, y;
//};
//
//std::vector<VarPoint> variables;
//int dMax = -1, dMin = 1e5;
//int pMax = 10, pMin = 1;
//OptimizationModel *getJmetricModel( const std::vector<Point>& input,
//                                    const std::vector< std::vector< int > >& neighbors,
//                                    std::vector< std::vector< int > >& faces, int negPenalty, double& worstArea );
//
//void writeFile( const char* fileName, std::vector<Point>& input,
//                std::vector< std::vector< int > >& neighbors,
//                std::vector< std::vector< int > >& faces,
//                std::vector< int>& distances );
//
//
//
//OptimizationModel *getDiagonalModel( const std::vector<Point>& input,
//                                     const std::vector< std::vector< int > >& neighbors,
//                                     std::vector< std::vector< int > >& faces )
//{
//    //    ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
//    ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );
//
//    OptimizationModel*m = new OptimizationModel( solver );
//
//    std::vector<VarPoint> variables( input.size( ) );
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        variables[i].x = m->addVariable( );
//        variables[i].y = m->addVariable( );
//
//        variables[i].x = input[i].x;
//        variables[i].y = input[i].y;
//    }
//
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//    for (unsigned int i = 0; i < faces.size( ); i++)
//    {
//        LinearExpression x, y;
//        int v0 = faces[i][0];
//        int v1 = faces[i][1];
//        int v2 = faces[i][2];
//        int v3 = faces[i][3];
//
//        x = ( ( variables[v0].x + variables[v2].x ) - ( variables[v1].x + variables[v3].x ) );
//        y = ( ( variables[v0].y + variables[v2].y ) - ( variables[v1].y + variables[v3].y ) );
//
//        obj += x * x / 4 + y * y / 4;
//    }
//
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        if (input[i].known)
//        {
//            m->addConstraint( variables[i].x, EQUAL, input[i].x );
//            m->addConstraint( variables[i].y, EQUAL, input[i].y );
//        }
//    }
//    return m;
//}
//
//
//
//void getJmetricItModel( std::vector<Point>& input,
//                        std::vector< std::vector< int > >& neighbors,
//                        std::vector< std::vector< int > >& faces,
//                        std::vector< int >& distances,
//                        std::string inputName,
//                        OptimizationModel *m )
//{
//    //enquanto existir negativo ou numero maximo de iteracoes
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//
//    bool moreIterations = true;
//    unsigned int iterations = 0;
//    while (moreIterations && iterations++ < 20)
//    {
//        moreIterations = false;
//
//        int contIn = 0, contOut = 0;
//        //printf( "Objective size: %u %.3lf\n\n", obj.size( ), obj.getValue( ) );
//        for (unsigned int i = 0; i < faces.size( ); i++)
//        {
//            QuadraticExpression l1, l2, l3, l4, A;
//            int v0 = faces[i][0];
//            int v1 = faces[i][1];
//            int v2 = faces[i][2];
//            int v3 = faces[i][3];
//
//            Polygon t1, t2, t3, t4;
//            t1.resize( 3 );
//            t2.resize( 3 );
//            t3.resize( 3 );
//            t4.resize( 3 );
//
//            t1[0] = Point( input[v0].x, input[v0].y );
//            t1[1] = Point( input[v1].x, input[v1].y );
//            t1[2] = Point( input[v2].x, input[v2].y );
//
//            t2[0] = Point( input[v1].x, input[v1].y );
//            t2[1] = Point( input[v2].x, input[v2].y );
//            t2[2] = Point( input[v3].x, input[v3].y );
//
//            t3[0] = Point( input[v2].x, input[v2].y );
//            t3[1] = Point( input[v3].x, input[v3].y );
//            t3[2] = Point( input[v0].x, input[v0].y );
//
//            t4[0] = Point( input[v3].x, input[v3].y );
//            t4[1] = Point( input[v0].x, input[v0].y );
//            t4[2] = Point( input[v1].x, input[v1].y );
//
//            double A1 = polygonArea( t1 );
//            double A2 = polygonArea( t2 );
//            double A3 = polygonArea( t3 );
//            double A4 = polygonArea( t4 );
//
//            double eps = 10;
//            //COLOCAR IF: se tem area negativa de algum dos quatro triangulos.
//            if (A1 < eps || A2 < eps || A3 < eps || A4 < eps)
//            {
//                moreIterations = true;
//
//                //calcula o quadrado dos lados
//                l1 = ( ( variables[v0].x - variables[v1].x )*( variables[v0].x - variables[v1].x ) +
//                       ( variables[v0].y - variables[v1].y )*( variables[v0].y - variables[v1].y ) );
//
//                l2 = ( ( variables[v1].x - variables[v2].x )*( variables[v1].x - variables[v2].x ) +
//                       ( variables[v1].y - variables[v2].y )*( variables[v1].y - variables[v2].y ) );
//
//                l3 = ( ( variables[v2].x - variables[v3].x )*( variables[v2].x - variables[v3].x ) +
//                       ( variables[v2].y - variables[v3].y )*( variables[v2].y - variables[v3].y ) );
//
//                l4 = ( ( variables[v3].x - variables[v0].x )*( variables[v3].x - variables[v0].x ) +
//                       ( variables[v3].y - variables[v0].y )*( variables[v3].y - variables[v0].y ) );
//
//                //2 vezes a area do poligono
//                A = variables[v0].x * variables[v1].y - variables[v1].x * variables[v0].y +
//                    variables[v1].x * variables[v2].y - variables[v2].x * variables[v1].y +
//                    variables[v2].x * variables[v3].y - variables[v3].x * variables[v2].y +
//                    variables[v3].x * variables[v0].y - variables[v0].x * variables[v3].y;
//
//                //                printf( "Val: %.3lf\n", ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) ).getValue( ) );
//                obj += pMax * ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//                //printf("Aqui\n");
//
//                contIn++;
//            }
//            else
//            {
//                contOut++;
//            }
//        }
//        //
//        printf( "%d %d\n", contIn, contOut );
//        //        break;
//        //printf( "Objective size: %u %.3lf\n\n", obj.size( ), obj.getValue( ) );
//        if (moreIterations)
//        {
//            m->optimize( );
//
//            for (unsigned int i = 0; i < input.size( ); i++)
//            {
//                input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//                input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//                //                printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//            }
//        }
//    }
//
//    //Get solution.
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//        input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//    }
//
//    //std::string outName(argv[1]);
//    std::string outName;
//    outName += inputName;
//    outName += "JmetricIt.out";
//
//    //Write file.
//    writeFile( outName.c_str( ), input, neighbors, faces, distances );
//
//}
//
//
//
//OptimizationModel *getJmetricModel( const std::vector<Point>& input,
//                                    const std::vector< std::vector< int > >& neighbors,
//                                    std::vector< std::vector< int > >& faces,
//                                    std::vector<int>& distances )
//{
//    //            ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
//    ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );
//    //            LBFGSOptimizationSolver* solver = new LBFGSOptimizationSolver( );
//
//    OptimizationModel*m = new OptimizationModel( solver );
//    solver->setEPS( 1e-2 );
//    m->showLog( false );
//
//    variables.resize( input.size( ) );
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        variables[i].x = m->addVariable( );
//        variables[i].y = m->addVariable( );
//
//        variables[i].x = input[i].x;
//        variables[i].y = input[i].y;
//    }
//
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//
//    for (unsigned int i = 0; i < faces.size( ); i++)
//    {
//        QuadraticExpression l1, l2, l3, l4, A2;
//        int v0 = faces[i][0];
//        int v1 = faces[i][1];
//        int v2 = faces[i][2];
//        int v3 = faces[i][3];
//
//        //calcula o quadrado dos lados
//        l1 = ( ( variables[v0].x - variables[v1].x )*( variables[v0].x - variables[v1].x ) +
//               ( variables[v0].y - variables[v1].y )*( variables[v0].y - variables[v1].y ) );
//
//        l2 = ( ( variables[v1].x - variables[v2].x )*( variables[v1].x - variables[v2].x ) +
//               ( variables[v1].y - variables[v2].y )*( variables[v1].y - variables[v2].y ) );
//
//        l3 = ( ( variables[v2].x - variables[v3].x )*( variables[v2].x - variables[v3].x ) +
//               ( variables[v2].y - variables[v3].y )*( variables[v2].y - variables[v3].y ) );
//
//        l4 = ( ( variables[v3].x - variables[v0].x )*( variables[v3].x - variables[v0].x ) +
//               ( variables[v3].y - variables[v0].y )*( variables[v3].y - variables[v0].y ) );
//
//        //2 vezes a area do poligono
//        A2 = variables[v0].x * variables[v1].y - variables[v1].x * variables[v0].y +
//            variables[v1].x * variables[v2].y - variables[v2].x * variables[v1].y +
//            variables[v2].x * variables[v3].y - variables[v3].x * variables[v2].y +
//            variables[v3].x * variables[v0].y - variables[v0].x * variables[v3].y;
//
//        double t = ( ( double ) distances[i] - dMin ) / ( dMax - dMin );
//        double w = ( 1 - t ) * pMax + t * pMin;
//        if (distances[i] == 0)
//            w = 2 * pMax;
//
//        obj += w * w * ( l1 + l2 + l3 + l4 - 2 * A2 );
//    }
//
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        if (input[i].known)
//        {
//            m->addConstraint( variables[i].x, EQUAL, input[i].x );
//            m->addConstraint( variables[i].y, EQUAL, input[i].y );
//        }
//    }
//    return m;
//}
//
//
//
//void getJmetricItSmallMattersNoNegModel( std::vector<Point>& input,
//                                         std::vector< std::vector< int > >& neighbors,
//                                         std::vector< std::vector< int > >& faces,
//                                         std::vector< int >& distances,
//                                         std::string inputName,
//                                         OptimizationModel *m )
//{
//    //enquanto existir negativo ou numero maximo de iteracoes
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//
//    unsigned int iterations = 0;
//    while (iterations++ < 3)
//    {
//        std::vector<double> jMetrics;
//        jMetrics.resize( faces.size( ) );
//        double maxJmetric = -1e10;
//
//        omp_lock_t writelock;
//        omp_init_lock( &writelock );
//
//        #pragma omp parallel for
//        for (unsigned int i = 0; i < faces.size( ); i++)
//        {
//            double l1, l2, l3, l4, A;
//            int v0 = faces[i][0];
//            int v1 = faces[i][1];
//            int v2 = faces[i][2];
//            int v3 = faces[i][3];
//
//
//            //calcula o quadrado dos lados
//            l1 = ( ( input[v0].x - input[v1].x )*( input[v0].x - input[v1].x ) +
//                   ( input[v0].y - input[v1].y )*( input[v0].y - input[v1].y ) );
//
//            l2 = ( ( input[v1].x - input[v2].x )*( input[v1].x - input[v2].x ) +
//                   ( input[v1].y - input[v2].y )*( input[v1].y - input[v2].y ) );
//
//            l3 = ( ( input[v2].x - input[v3].x )*( input[v2].x - input[v3].x ) +
//                   ( input[v2].y - input[v3].y )*( input[v2].y - input[v3].y ) );
//
//            l4 = ( ( input[v3].x - input[v0].x )*( input[v3].x - input[v0].x ) +
//                   ( input[v3].y - input[v0].y )*( input[v3].y - input[v0].y ) );
//
//            //2 vezes a area do poligono
//            A = input[v0].x * input[v1].y - input[v1].x * input[v0].y +
//                input[v1].x * input[v2].y - input[v2].x * input[v1].y +
//                input[v2].x * input[v3].y - input[v3].x * input[v2].y +
//                input[v3].x * input[v0].y - input[v0].x * input[v3].y;
//
//            jMetrics[i] = ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//
//            if (jMetrics[i] > maxJmetric)
//            {
//                omp_set_lock( &writelock );
//                maxJmetric = jMetrics[i];
//                omp_unset_lock( &writelock );
//            }
//        }
//
//        omp_destroy_lock( &writelock );
//
//        obj.clear( );
//
//        printf( "Iteracao: %2d %e\n", iterations, maxJmetric );
//
//        double maxDivision = 0.0;
//
//        for (unsigned int i = 0; i < faces.size( ); i++)
//        {
//
//            QuadraticExpression l1, l2, l3, l4, A;
//            int v0 = faces[i][0];
//            int v1 = faces[i][1];
//            int v2 = faces[i][2];
//            int v3 = faces[i][3];
//
//            Polygon t1, t2, t3, t4;
//            t1.resize( 3 );
//            t2.resize( 3 );
//            t3.resize( 3 );
//            t4.resize( 3 );
//
//            t1[0] = Point( input[v0].x, input[v0].y );
//            t1[1] = Point( input[v1].x, input[v1].y );
//            t1[2] = Point( input[v2].x, input[v2].y );
//
//            t2[0] = Point( input[v1].x, input[v1].y );
//            t2[1] = Point( input[v2].x, input[v2].y );
//            t2[2] = Point( input[v3].x, input[v3].y );
//
//            t3[0] = Point( input[v2].x, input[v2].y );
//            t3[1] = Point( input[v3].x, input[v3].y );
//            t3[2] = Point( input[v0].x, input[v0].y );
//
//            t4[0] = Point( input[v3].x, input[v3].y );
//            t4[1] = Point( input[v0].x, input[v0].y );
//            t4[2] = Point( input[v1].x, input[v1].y );
//
//            double A1 = polygonArea( t1 );
//            double A2 = polygonArea( t2 );
//            double A3 = polygonArea( t3 );
//            double A4 = polygonArea( t4 );
//
//            double eps1 = 10, eps2 = 1e0;
//
//
//            //calcula o quadrado dos lados
//            l1 = ( ( variables[v0].x - variables[v1].x )*( variables[v0].x - variables[v1].x ) +
//                   ( variables[v0].y - variables[v1].y )*( variables[v0].y - variables[v1].y ) );
//
//            l2 = ( ( variables[v1].x - variables[v2].x )*( variables[v1].x - variables[v2].x ) +
//                   ( variables[v1].y - variables[v2].y )*( variables[v1].y - variables[v2].y ) );
//
//            l3 = ( ( variables[v2].x - variables[v3].x )*( variables[v2].x - variables[v3].x ) +
//                   ( variables[v2].y - variables[v3].y )*( variables[v2].y - variables[v3].y ) );
//
//            l4 = ( ( variables[v3].x - variables[v0].x )*( variables[v3].x - variables[v0].x ) +
//                   ( variables[v3].y - variables[v0].y )*( variables[v3].y - variables[v0].y ) );
//
//            //2 vezes a area do poligono
//            A = variables[v0].x * variables[v1].y - variables[v1].x * variables[v0].y +
//                variables[v1].x * variables[v2].y - variables[v2].x * variables[v1].y +
//                variables[v2].x * variables[v3].y - variables[v3].x * variables[v2].y +
//                variables[v3].x * variables[v0].y - variables[v0].x * variables[v3].y;
//
//            //COLOCAR IF: se tem area negativa de algum dos quatro triangulos.
//            if (A1 < eps1 || A2 < eps1 || A3 < eps1 || A4 < eps1)
//            {
//                obj +=  pMax * pMax *  ( maxJmetric / jMetrics[i] )* ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//
//            }
//            else
//            {
//                if (fabs( jMetrics[i] ) < eps2)
//                {
//                    obj += pMax * pMax * ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//                }
//                else
//                {
//                    obj += ( maxJmetric / jMetrics[i] )* ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//                    
//                    maxDivision = std::max( maxDivision, maxJmetric / jMetrics[i] );
//                }
//            }
//        }
//        printf( "      Maxima divisao: %e\n", maxDivision );
//
//        m->optimize( );
//
//        for (unsigned int i = 0; i < input.size( ); i++)
//        {
//            input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//            input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//            //                printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//        }
//    }
//
//    //Get solution.
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//        input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//    }
//
//    //std::string outName(argv[1]);
//    std::string outName;
//    outName += inputName;
//    outName += "JmetricItSmallMattersNoNeg.out";
//
//    //Write file.
//    writeFile( outName.c_str( ), input, neighbors, faces, distances );
//
//}
//
//
//
//void getJmetricItSmallMattersModel( std::vector<Point>& input,
//                                    std::vector< std::vector< int > >& neighbors,
//                                    std::vector< std::vector< int > >& faces,
//                                    std::vector< int >& distances,
//                                    std::string inputName,
//                                    OptimizationModel *m )
//{
//    //enquanto existir negativo ou numero maximo de iteracoes
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//
//    unsigned int iterations = 0;
//    while (iterations++ < 4)
//    {
//        std::vector<double> jMetrics;
//        jMetrics.resize( faces.size( ) );
//        double maxJmetric = -1e10;
//        for (unsigned int i = 0; i < faces.size( ); i++)
//        {
//            double l1, l2, l3, l4, A;
//            int v0 = faces[i][0];
//            int v1 = faces[i][1];
//            int v2 = faces[i][2];
//            int v3 = faces[i][3];
//
//
//            //calcula o quadrado dos lados
//            l1 = ( ( input[v0].x - input[v1].x )*( input[v0].x - input[v1].x ) +
//                   ( input[v0].y - input[v1].y )*( input[v0].y - input[v1].y ) );
//
//            l2 = ( ( input[v1].x - input[v2].x )*( input[v1].x - input[v2].x ) +
//                   ( input[v1].y - input[v2].y )*( input[v1].y - input[v2].y ) );
//
//            l3 = ( ( input[v2].x - input[v3].x )*( input[v2].x - input[v3].x ) +
//                   ( input[v2].y - input[v3].y )*( input[v2].y - input[v3].y ) );
//
//            l4 = ( ( input[v3].x - input[v0].x )*( input[v3].x - input[v0].x ) +
//                   ( input[v3].y - input[v0].y )*( input[v3].y - input[v0].y ) );
//
//            //2 vezes a area do poligono
//            A = input[v0].x * input[v1].y - input[v1].x * input[v0].y +
//                input[v1].x * input[v2].y - input[v2].x * input[v1].y +
//                input[v2].x * input[v3].y - input[v3].x * input[v2].y +
//                input[v3].x * input[v0].y - input[v0].x * input[v3].y;
//
//            jMetrics[i] = ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//
//            if (jMetrics[i] > maxJmetric)
//            {
//                maxJmetric = jMetrics[i];
//            }
//
//            if (jMetrics[i] < 0)
//            {
//                printf( "Negativo\n" );
//            }
//        }
//
//        obj.clear( );
//
//        for (unsigned int i = 0; i < faces.size( ); i++)
//        {
//
//            QuadraticExpression l1, l2, l3, l4, A;
//            int v0 = faces[i][0];
//            int v1 = faces[i][1];
//            int v2 = faces[i][2];
//            int v3 = faces[i][3];
//
//
//            //calcula o quadrado dos lados
//            l1 = ( ( variables[v0].x - variables[v1].x )*( variables[v0].x - variables[v1].x ) +
//                   ( variables[v0].y - variables[v1].y )*( variables[v0].y - variables[v1].y ) );
//
//            l2 = ( ( variables[v1].x - variables[v2].x )*( variables[v1].x - variables[v2].x ) +
//                   ( variables[v1].y - variables[v2].y )*( variables[v1].y - variables[v2].y ) );
//
//            l3 = ( ( variables[v2].x - variables[v3].x )*( variables[v2].x - variables[v3].x ) +
//                   ( variables[v2].y - variables[v3].y )*( variables[v2].y - variables[v3].y ) );
//
//            l4 = ( ( variables[v3].x - variables[v0].x )*( variables[v3].x - variables[v0].x ) +
//                   ( variables[v3].y - variables[v0].y )*( variables[v3].y - variables[v0].y ) );
//
//            //2 vezes a area do poligono
//            A = variables[v0].x * variables[v1].y - variables[v1].x * variables[v0].y +
//                variables[v1].x * variables[v2].y - variables[v2].x * variables[v1].y +
//                variables[v2].x * variables[v3].y - variables[v3].x * variables[v2].y +
//                variables[v3].x * variables[v0].y - variables[v0].x * variables[v3].y;
//
//            if (jMetrics[i] == 0)
//            {
//                obj += ( maxJmetric / 1 )* ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//            }
//            else
//            {
//                obj += ( maxJmetric / jMetrics[i] )* ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
//            }
//
//        }
//
//        m->optimize( );
//
//        for (unsigned int i = 0; i < input.size( ); i++)
//        {
//            input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//            input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//            //                printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//        }
//    }
//
//    //Get solution.
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//        input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//    }
//
//    //std::string outName(argv[1]);
//    std::string outName;
//    outName += inputName;
//    outName += "JmetricItSmallMatters.out";
//
//    //Write file.
//    writeFile( outName.c_str( ), input, neighbors, faces, distances );
//
//}
//
//
//
//OptimizationModel *getUniformLaplacianAverageModel( const std::vector<Point>& input,
//                                                    const std::vector< std::vector< int > >& neighbors,
//                                                    std::vector< std::vector< int > >& faces )
//{
//    ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );
//    OptimizationModel*m = new OptimizationModel( solver );
//
//    std::vector<VarPoint> variables( input.size( ) );
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        variables[i].x = m->addVariable( );
//        variables[i].y = m->addVariable( );
//
//        variables[i].x = input[i].x;
//        variables[i].y = input[i].y;
//    }
//
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//    for (unsigned int i = 0; i < neighbors.size( ); i++)
//    {
//        LinearExpression x, y;
//        for (unsigned int j = 0; j < neighbors[i].size( ); j++)
//        {
//            unsigned int idx = neighbors[i][j];
//            x += variables[idx].x;
//            y += variables[idx].y;
//        }
//        x = variables[i].x - x * ( 1.0 / neighbors[i].size( ) );
//        y = variables[i].y - y * ( 1.0 / neighbors[i].size( ) );
//
//        obj += x * x + y * y;
//    }
//
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        if (input[i].known)
//        {
//            m->addConstraint( variables[i].x, EQUAL, input[i].x );
//            m->addConstraint( variables[i].y, EQUAL, input[i].y );
//        }
//    }
//
//    return m;
//}
//
//
//
//OptimizationModel *getUniformLaplacianModel( const std::vector<Point>& input,
//                                             const std::vector< std::vector< int > >& neighbors,
//                                             std::vector< std::vector< int > >& faces )
//{
//    ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
//    OptimizationModel*m = new OptimizationModel( solver );
//
//    std::vector<VarPoint> variables( input.size( ) );
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        variables[i].x = m->addVariable( );
//        variables[i].y = m->addVariable( );
//
//        variables[i].x = input[i].x;
//        variables[i].y = input[i].y;
//    }
//
//    QuadraticExpression& obj = m->getObjectiveFunction( );
//    for (unsigned int i = 0; i < neighbors.size( ); i++)
//    {
//        LinearExpression x, y;
//        for (unsigned int j = 0; j < neighbors[i].size( ); j++)
//        {
//            unsigned int idx = neighbors[i][j];
//            x += variables[idx].x;
//            y += variables[idx].y;
//        }
//        x -= x.size( ) * variables[i].x;
//        y -= y.size( ) * variables[i].y;
//
//        obj += x * x + y * y;
//    }
//
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        if (input[i].known)
//        {
//            m->addConstraint( variables[i].x, EQUAL, input[i].x );
//            m->addConstraint( variables[i].y, EQUAL, input[i].y );
//        }
//    }
//
//    return m;
//}
//
//
//
//void readFile( const char* fileName, std::vector<Point>& input,
//               std::vector< std::vector< int > >& neighbors,
//               std::vector< std::vector< int > >& faces,
//               std::vector< int >& distances )
//{
//    std::ifstream in( fileName );
//    if (in.fail( ))
//    {
//        printf( "Fail to open the file %s\n", fileName );
//        return;
//    }
//    std::string aux;
//    std::getline( in, aux );
//
//    //Read the number of vertices.
//    unsigned int numVertices;
//    in >> numVertices;
//
//    unsigned int n;
//    //Allocate storage.
//    input.resize( numVertices );
//    neighbors.resize( numVertices );
//    for (unsigned int i = 0; i < numVertices; i++)
//    {
//        in >> n >> input[i].x >> input[i].y >> input[i].known;
//    }
//
//    in >> aux >> aux;
//
//    for (unsigned int i = 0; i < numVertices; i++)
//    {
//        in >> n;
//        neighbors[i].resize( n );
//        for (unsigned int j = 0; j < n; j++)
//        {
//            in >> neighbors[i][j];
//        }
//    }
//
//    in >> aux;
//
//    unsigned int numberFaces;
//    in >> numberFaces;
//    faces.resize( numberFaces );
//    distances.resize( numberFaces );
//    for (unsigned int i = 0; i < numberFaces; i++)
//    {
//        unsigned int n;
//        in >> n;
//        faces[i].resize( 4 );
//        for (unsigned int j = 0; j < 4; j++)
//        {
//            in >> faces[i][j];
//        }
//        in >> distances[i];
//        dMin = std::min( distances[i], dMin );
//        dMax = std::max( distances[i], dMax );
//    }
//}
//
//
//
//void writeFile( const char* fileName, std::vector<Point>& input,
//                std::vector< std::vector< int > >& neighbors,
//                std::vector< std::vector< int > >& faces,
//                std::vector<int>& distances )
//{
//    std::ofstream out( fileName );
//    if (out.fail( ))
//    {
//        printf( "Fail to open the file %s\n", fileName );
//        return;
//    }
//    out << "*Vertices x y fix" << endl;
//    out << input.size( ) << endl;
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        out << i << " " << input[i].x << " " << input[i].y << " " << ( int ) input[i].known << endl;
//    }
//
//    out << "*Vertices star" << endl;
//
//    for (unsigned int i = 0; i < neighbors.size( ); i++)
//    {
//        out << neighbors[i].size( ) << " ";
//        for (unsigned int j = 0; j < neighbors[i].size( ); j++)
//        {
//            out << neighbors[i][j] << " ";
//        }
//        out << endl;
//    }
//
//    out << "*Faces" << endl;
//    out << faces.size( ) << endl;
//
//    for (unsigned int i = 0; i < faces.size( ); i++)
//    {
//        out << i << " ";
//        for (unsigned int j = 0; j < 4; j++)
//        {
//            out << faces[i][j] << " ";
//        }
//        out << distances[i] << endl;
//    }
//    out << "*end" << endl;
//}
//
//
//
///*
// * 
// */
//int main( int argc, char** argv )
//{
//    if (argc < 2)
//    {
//        printf( "Usage: ./quad file\n" );
//        return 0;
//    }
//
//    //Read input data.
//    std::vector<Point> input;
//    std::vector< std::vector< int > > neighbors;
//    std::vector< std::vector< int > > faces;
//    std::vector< int > distances;
//    int option = atoi( argv[2] );
//
//    //Read file.
//    readFile( argv[1], input, neighbors, faces, distances );
//
//    //Build model.
//    OptimizationModel *m;
//    if (option == 1)
//    {
//        m = getUniformLaplacianModel( input, neighbors, faces );
//    }
//    else if (option == 2)
//    {
//        m = getUniformLaplacianAverageModel( input, neighbors, faces );
//    }
//    else if (option == 3)
//    {
//        m = getDiagonalModel( input, neighbors, faces );
//    }
//    else if (option == 4)
//    {
//        m = getJmetricModel( input, neighbors, faces, distances );
//    }
//    else if (option == 5)
//    {
//        m = getJmetricModel( input, neighbors, faces, distances );
//        m->optimize( );
//
//        for (unsigned int i = 0; i < input.size( ); i++)
//        {
//            input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//            input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//            //            printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//        }
//
//        Timer t;
//        getJmetricItModel( input, neighbors, faces, distances, argv[1], m );
//        t.printTime( "Total time" );
//        return 0;
//
//    }
//    else if (option == 6)
//    {
//        m = getJmetricModel( input, neighbors, faces, distances );
//        m->optimize( );
//
//        for (unsigned int i = 0; i < input.size( ); i++)
//        {
//            input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//            input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//            //            printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//        }
//
//        Timer t;
//        getJmetricItSmallMattersModel( input, neighbors, faces, distances, argv[1], m );
//        t.printTime( "Total time" );
//        return 0;
//
//    }
//    else if (option == 7)
//    {
//        m = getJmetricModel( input, neighbors, faces, distances );
//        m->optimize( );
//
//        for (unsigned int i = 0; i < input.size( ); i++)
//        {
//            input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//            input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//            //            printf( "%u: (%.10lf, %.10lf)\n", i + 1, input[i].x, input[i].y );
//        }
//
//        Timer t;
//        getJmetricItSmallMattersNoNegModel( input, neighbors, faces, distances, argv[1], m );
//        t.printTime( "Total time" );
//        return 0;
//
//    }
//    else
//    {
//        m = getUniformLaplacianModel( input, neighbors, faces );
//    }
//
//    Timer t;
//    //Optimize.
//    m->optimize( );
//
//    t.printTime( "Total time" );
//    std::cout << "Objective function: " << m->getObjectiveFunction( ).getValue( ) << std::endl;
//
//
//    //Get solution.
//    for (unsigned int i = 0; i < input.size( ); i++)
//    {
//        input[i].x = m->getVariable( 2 * i + 0 ).getValue( );
//        input[i].y = m->getVariable( 2 * i + 1 ).getValue( );
//    }
//
//
//    //std::string outName(argv[1]);
//    std::string outName;
//    outName += argv[1];
//    if (option == 1)
//    {
//        outName += "UniformLaplacian.out";
//    }
//    else if (option == 2)
//    {
//        outName += "UniformLaplacianAverage.out";
//    }
//    else if (option == 3)
//    {
//        outName += "Diagonal.out";
//    }
//    else if (option == 4)
//    {
//        outName += "Jmetric.out";
//    }
//    else
//    {
//        outName += "UniformLaplacian.out";
//    }
//
//    //Read file.
//    writeFile( outName.c_str( ), input, neighbors, faces, distances );
//    return 0;
//}


#include <cstdlib>
#include "MeshWindow.h"
#include <iup/iup.h>
#include <iup/iupgl.h>

using namespace std;



int main( int argc, char** argv )
{
    //Inicializa a IUP.
    IupOpen( &argc, &argv );

    //Inicializa a OpenGL na IUP.
    IupGLCanvasOpen( );

    //Cria objeto.
    MeshWindow *window = new MeshWindow( );

    //Exibe a janela.
    window->show( );

    //Coloca a IUP em loop.
    IupMainLoop( );


    //Deleta o obejto alocado.
    delete window;

    //Fecha a IUP.
    IupClose( );

    return 0;
}