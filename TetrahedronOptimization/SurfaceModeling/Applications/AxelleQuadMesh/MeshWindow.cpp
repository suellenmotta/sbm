/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MeshWindow.cpp
 * Author: ncortez
 * 
 * Created on September 20, 2016, 5:52 PM
 */


#include "MeshWindow.h"
#include <cstdlib>
#include <cstdio>
#include <iup/iup.h>
#include <iup/iupgl.h>
#include <cstring>
#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <fstream>
#include <cmath>

#include "../../ModelManager/OptimizationModel.h"
#include "../../ModelManager/Variable.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientSLUOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/LBFGSOptimizationSolver.h"
#include "../../Solver/OptimizationSolvers/GradientDescentOptimizationSolver.h"
#include "../../../../libs/Timer/Timer.h"

Ihandle* canvas = 0;
using namespace Solver;
using namespace ModelManager;
using namespace std;



MeshWindow::MeshWindow( )
{
    //Allocate _solver[_currentRunIndex] and model.
    //    _currentRunIndex = 0;
    //    
    //    _solver[0] = 0;
    //    _m[0] = 0;
    //    _solver[1] = 0;
    //    _m[1] = 0;
    //    _solver[2] = 0;
    //    _m[2] = 0;
    //    
    //    _mousePressed = 1;
    //    _continueSolver = false;
    //    _isNewCurve = true;
    //
    //
    _pMin.x = -1;
    _pMax.x = +1;
    _pMin.y = -1;
    _pMax.y = +1;

    //Cria janela e define suas configuracoes.
    createWindow( );

    initialize( );
}



MeshWindow::~MeshWindow( )
{
    //    delete _solver[0];
    //    delete _m[0];
    //    delete _solver[1];
    //    delete _m[1];
    //    delete _solver[2];
    //    delete _m[2];
    IupDestroy( _dialog );
}



void MeshWindow::show( )
{
    IupShow( _dialog );
}



void MeshWindow::hide( )
{
    IupHide( _dialog );
}



void MeshWindow::createWindow( )
{
    //Cria botao de sair.
    Ihandle *exitButton = IupButton( "Sair", NULL );

    //Cria botao de run.
    Ihandle *runButton = IupButton( "Run", NULL );

    //    //Cria botao de run x.
    //    Ihandle *runXButton = IupButton( "Run X", NULL );
    //
    //    //Cria botao de run y.
    //    Ihandle *runYButton = IupButton( "Run Y", NULL );

    //Cria botao de stop.
    Ihandle *stopButton = IupButton( "Stop", NULL );

    //Cria botao de stop.
    Ihandle *newButton = IupButton( "New", NULL );

    //Cria output label da iteração corrente.
    Ihandle *curInterationLabel = IupLabel( "Current Iteration:" );

    //Cria gradient norm label.
    Ihandle *gNormLabel = IupLabel( "Gradient Norm:" );

    //Cria eps label .
    Ihandle *epsLabel = IupLabel( "EPS: " );

    //Cria max label .
    Ihandle *maxLabel = IupLabel( "Max Iterations: " );

    //Cria method label .
    Ihandle *methodLabel = IupLabel( "Solver: " );

    //Model label
    Ihandle *modelLabel = IupLabel( "Model: " );

    //Cria eps norm text field.
    Ihandle *epsText = IupText( "  " );

    //Cria max text field.
    Ihandle *maxText = IupText( "  " );


    //Cria method list.
    Ihandle* solverList = IupList( "Select" );

    Ihandle* modelList = IupList( "Select" );

    Ihandle* initList = IupList( "Select" );


    //Cria canvas.
    canvas = IupGLCanvas( NULL );


    //Cria composicao final.
    Ihandle *vboxOutput = IupVbox( curInterationLabel, gNormLabel, NULL );

    //Cria composicao dos botoes de controle (run e stop)
    Ihandle *hboxCtrlButtons = IupHbox( runButton, /* runXButton, runYButton,*/ stopButton, newButton, NULL );

    //Cria composicao botões.
    Ihandle *vboxButton = IupVbox( exitButton, NULL );

    //Cria composicao parte inferior.
    Ihandle *hboxFotter = IupHbox( vboxOutput, IupFill( ), vboxButton, NULL );

    //Cria composição dos inputs
    Ihandle *hboxInputs = IupHbox( IupVbox( methodLabel, solverList, NULL ),
                                   IupVbox( modelLabel, modelList, NULL ),
                                   IupVbox( maxLabel, maxText, NULL ),
                                   IupVbox( epsLabel, epsText, NULL ),
                                   IupVbox( IupLabel( "Solucao Inicial:" ), initList, NULL ), NULL );

    //Cria composicao parte superior.
    Ihandle *vboxTop = IupVbox( hboxInputs, hboxCtrlButtons, NULL );

    //Cria composicao final.
    Ihandle *vboxFinal = IupVbox( vboxTop, canvas, hboxFotter, NULL );

    //Cria dialogo.
    _dialog = IupDialog( vboxFinal );

    //Cria conjunto de menus.
    Ihandle *mainMenu = createMenus( );

    //Define handles dos elementos dos menus.
    IupSetHandle( "menuBarWindow", mainMenu );

    //Define os atributos dos botoes
    IupSetAttribute( exitButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( exitButton, IUP_TIP, "Fecha a janela." );
    IupSetAttribute( runButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( runButton, IUP_TIP, "Comeca otimizacao." );
    //    IupSetAttribute( runXButton, IUP_RASTERSIZE, "80x32" );
    //    IupSetAttribute( runXButton, IUP_TIP, "Comeca otimizacao para X." );
    //    IupSetAttribute( runYButton, IUP_RASTERSIZE, "80x32" );
    //    IupSetAttribute( runYButton, IUP_TIP, "Comeca otimizacao para Y." );
    IupSetAttribute( stopButton, IUP_RASTERSIZE, "80x32" );
    IupSetAttribute( stopButton, IUP_TIP, "Para otmizacao." );

    //Define da list
    IupSetAttributes( solverList, "1=\"LBFGS\", 2=\"CG LIS\", 3=\"CG. SLU\", 4=\"G. Descent\", DROPDOWN=YES" );
    IupSetAttribute( solverList, IUP_VALUE, "1" );
    IupSetAttributes( modelList, "1=\"UnLap\", 2=\"UnLalAvg\", 3=\"Diagonal\", 4=\"Jmetric\", 5=\"JmetricIt\", DROPDOWN=YES" );
    IupSetAttribute( modelList, IUP_VALUE, "1" );
    IupSetAttributes( initList, "1=\"Zero\", 2=\"Corrente\", DROPDOWN=YES" );
    IupSetAttribute( initList, IUP_VALUE, "1" );

    //Define os atributos do canvas.
    IupSetAttribute( canvas, IUP_RASTERSIZE, "600x600" );
    IupSetAttribute( canvas, IUP_BUFFER, IUP_DOUBLE );
    IupSetAttribute( canvas, IUP_EXPAND, IUP_YES );

    IupSetAttribute( hboxInputs, IUP_GAP, "20" );
    IupSetAttribute( hboxCtrlButtons, IUP_GAP, "20" );
    IupSetAttribute( vboxFinal, IUP_MARGIN, "10x5" );

    IupSetAttribute( epsText, IUP_MASK, IUP_MASK_EFLOAT );
    IupSetAttribute( maxText, IUP_MASK, IUP_MASK_UINT );
    IupSetAttribute( epsText, IUP_VALUE, "1e-3" );
    IupSetAttribute( maxText, IUP_VALUE, "1000" );
    //    IupSetAttribute( nPointsPerIntevalText, IUP_VALUE, "100" );



    //Define propriedades do dialogo.
    IupSetAttribute( _dialog, IUP_TITLE, "Curves Window" );
    IupSetAttribute( _dialog, "THIS", ( char* ) this );
    IupSetAttribute( _dialog, "CANVAS", ( char* ) canvas );
    IupSetAttribute( _dialog, "SOLVER_LIST", ( char* ) solverList );
    IupSetAttribute( _dialog, "MODEL_LIST", ( char* ) modelList );
    IupSetAttribute( _dialog, "INIT_LIST", ( char* ) initList );
    IupSetAttribute( _dialog, "EPS_TEXT", ( char* ) epsText );
    IupSetAttribute( _dialog, "MAX_TEXT", ( char* ) maxText );
    //    IupSetAttribute( _dialog, "NPOINTS_TEXT", ( char* ) nPointsPerIntevalText );
    IupSetAttribute( _dialog, "STOP_BTN", ( char* ) stopButton );
    IupSetAttribute( _dialog, "RUN_BTN", ( char* ) runButton );
    //    IupSetAttribute( _dialog, "RUNX_BTN", ( char* ) runXButton );
    //    IupSetAttribute( _dialog, "RUNY_BTN", ( char* ) runYButton );
    IupSetAttribute( _dialog, "NEW_BTN", ( char* ) newButton );
    IupSetAttribute( _dialog, "GNORM_LABEL", ( char* ) gNormLabel );
    IupSetAttribute( _dialog, "ITERATION_LABEL", ( char* ) curInterationLabel );
    IupSetAttribute( _dialog, IUP_MENU, "menuBarWindow" );

    //Define callbacks do botao.
    IupSetCallback( _dialog, IUP_CLOSE_CB, ( Icallback ) exitButtonCallback );
    IupSetCallback( exitButton, IUP_ACTION, ( Icallback ) exitButtonCallback );
    IupSetCallback( runButton, IUP_ACTION, ( Icallback ) runButtonCallback );
    //    IupSetCallback( runXButton, IUP_ACTION, ( Icallback ) runXButtonCallback );
    //    IupSetCallback( runYButton, IUP_ACTION, ( Icallback ) runYButtonCallback );
    IupSetCallback( stopButton, IUP_ACTION, ( Icallback ) stopButtonCallback );
    IupSetCallback( newButton, IUP_ACTION, ( Icallback ) newButtonCallback );
    //
    //    //Define as callbacks do canvas.
    IupSetCallback( canvas, IUP_ACTION, ( Icallback ) actionCanvasCallback );
    //    IupSetCallback( initList, IUP_ACTION, ( Icallback ) actionInitListCallback );
    //    IupSetCallback( solverList, IUP_ACTION, ( Icallback ) actionSolverListCallback );
    IupSetCallback( canvas, IUP_RESIZE_CB, ( Icallback ) resizeCanvasCallback );
    IupSetCallback( canvas, IUP_BUTTON_CB, ( Icallback ) buttonCanvasCallback );
    IupSetCallback( canvas, IUP_MOTION_CB, ( Icallback ) motionCanvasCallback );
    IupSetCallback( canvas, IUP_WHEEL_CB, ( Icallback ) wheelCanvasCallback );

    //Desabilta campos inicialmente
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    //    IupSetAttribute( nPointsPerIntevalText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );
    //    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_NO );
    //    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_NO );



    //Mapeia o dialogo.
    IupMap( _dialog );

    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );
}



Ihandle* MeshWindow::createMenus( )
{
    //Create an item to open a mesh.
    Ihandle *openData = IupItem( "&Open Points Data", NULL );

    //Create an item to close the window.
    Ihandle *exitWindow = IupItem( "&Exit", NULL );

    //Create an item to save a print screen image.
    Ihandle *savePrint = IupItem( "Salvar &Print", NULL );

    //Create the file menu.
    Ihandle *fileMenu = IupMenu( openData, savePrint, IupSeparator( ), exitWindow, NULL );

    //Create a submenu for menu file.
    Ihandle *fileSubMenu = IupSubmenu( "Arquivo", fileMenu );

    //Create an item to set a color for known points.
    Ihandle *knownPoints = IupItem( "Known Points Color", NULL );

    //Create an item to set a color for border constraints.
    Ihandle *aspectRatio = IupItem( "Border Constraints Color", NULL );

    //Create an edit menu.
    Ihandle *editMenu = IupMenu( knownPoints, aspectRatio, NULL );

    //Create an edit submenu.
    Ihandle *editSubMenu = IupSubmenu( "Edit", editMenu );


    //Cria menu princial.
    Ihandle *mainMenu = IupMenu( fileSubMenu, editSubMenu, NULL );

    //Create the main menu.
    IupSetAttribute( openData, IUP_IMAGE, "IUP_FileOpen" );
    IupSetAttribute( savePrint, IUP_IMAGE, "IUP_FileSave" );
    IupSetAttribute( knownPoints, IUP_IMAGE, "IUP_ToolsColor" );
    IupSetAttribute( aspectRatio, IUP_VALUE, IUP_OFF );

    //Define callbacks do menu.
    IupSetCallback( openData, IUP_ACTION, ( Icallback ) openDataMenuCallback );

    //Define callback do botao sair.
    //IupSetCallback( exitWindow, IUP_ACTION, ( Icallback ) exitButtonCallback );

    //Retorna menu principal.
    return mainMenu;
}



void MeshWindow::initialize( )
{
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
    glEnable( GL_LINE_SMOOTH );
    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glPointSize( 4.5 );

    //Allocate solver..
    //Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );

    //int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
    //allocateSolver( solverIndex );
    //    allocateSolverX( solverIndex );
    //    allocateSolverY( solverIndex );

    //Set the frequency that the solver must let the window knows about the
    //current solution.
    //_solver[_currentRunIndex]->setStepsNumber( 10 );
    //_solver[1]->setStepsNumber( 10 );
    //_solver[2]->setStepsNumber( 10 );

    //initialize fixed points
    //_nFixedPoints = 0;
}



int MeshWindow::openDataMenuCallback( Ihandle* button )
{
    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( button, "THIS" );

    Ihandle *dlg = IupFileDlg( );

    IupSetAttribute( dlg, "DIALOGTYPE", "OPEN" );
    IupSetAttribute( dlg, "TITLE", "IupFileDlg Test" );
    IupPopup( dlg, IUP_CURRENT, IUP_CURRENT );

    if (IupGetInt( dlg, "STATUS" ) == -1)
    {
        IupDestroy( dlg );
        return IUP_DEFAULT;
    }

    //set as a new curve(linear)
    //window->_isNewCurve = true;

    //Habilita campos necessarios
    window->activateComponents( button );

    string path = IupGetAttribute( dlg, "VALUE" );

    Ihandle* initList = ( Ihandle* ) IupGetAttribute( button, "INIT_LIST" );
    unsigned int initIndex = atoi( IupGetAttribute( initList, "VALUE" ) );
    window->openData( path, initIndex );

    IupDestroy( dlg );
    Ihandle* canvas = ( Ihandle* ) IupGetAttribute( button, "CANVAS" );
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void MeshWindow::openData( const std::string& path, unsigned int initIndex )
{
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( _dialog, "NEW_BTN" );

    //clear all 
    newButtonCallback( newButton );

    //Read the file.
    readFile( path.c_str( ) );
}



int MeshWindow::newButtonCallback( Ihandle *button )
{
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( button, "THIS" );

    //    window->_isNewCurve = true;
    //
    delete window->_m;
    //    delete window->_m[1];
    //    delete window->_m[2];
    //    window->_m[0] = 0;
    //    window->_m[1] = 0;
    //    window->_m[2] = 0;
    window->_input.clear( );
    window->_neighbors.clear( );
    window->_faces.clear( );
    window->_distances.clear( );

    window->initialize( );

    return IUP_DEFAULT;
}



int MeshWindow::findNewIndex( int oldIndex, std::vector< int > indexesVector )
{
    unsigned int i = 0;
    while (indexesVector[i] != oldIndex)
    {
        i++;
        if (i > indexesVector.size( ))
        {
            printf( "Algo errado com o vetor auxilhar de indice\n" );
        }
    }

    return i;
}



void MeshWindow::readFile( const char* fileName )
{
    std::ifstream in( fileName );
    if (in.fail( ))
    {
        printf( "Fail to open the file %s\n", fileName );
        return;
    }
    std::string aux;
    std::getline( in, aux );

    //Read the number of vertices.
    unsigned int numVertices;
    in >> numVertices;

    std::vector<int> indexesVector;
    indexesVector.resize( numVertices );

    unsigned int n;
    //Allocate storage.
    _input.resize( numVertices );
    _neighbors.resize( numVertices );
    for (unsigned int i = 0; i < numVertices; i++)
    {
        in >> indexesVector[i] >> _input[i].x >> _input[i].y >> _input[i].known;
    }

    in >> aux >> aux;

    for (unsigned int i = 0; i < numVertices; i++)
    {

        in >> n;
        _neighbors[i].resize( n );
        for (unsigned int j = 0; j < n; j++)
        {
            int oldIndex;
            in >> oldIndex;
            _neighbors[i][j] = findNewIndex( oldIndex, indexesVector );
        }
    }

    in >> aux;

    unsigned int numberFaces;
    in >> numberFaces;
    _faces.resize( numberFaces );
    _distances.resize( numberFaces );

    for (unsigned int i = 0; i < numberFaces; i++)
    {
        unsigned int n;
        in >> n;
        _faces[i].resize( 4 );
        for (unsigned int j = 0; j < 4; j++)
        {
            int oldIndex;
            in >> oldIndex;
            _faces[i][j] = findNewIndex( oldIndex, indexesVector );
        }
        in >> _distances[i];
    }

    //    //antessssss
    //    
    //    //maxDist = 0;
    //    //minDist = 10000000000;
    //    std::ifstream in( fileName );
    //    if (in.fail( ))
    //    {
    //        printf( "Fail to open the file %s\n", fileName );
    //        return;
    //    }
    //    std::string aux;
    //    std::getline( in, aux );
    //
    //    //Read the number of vertices.
    //    unsigned int numVertices;
    //    in >> numVertices;
    //
    //    unsigned int n;
    //    //Allocate storage.
    //    _input.resize( numVertices );
    //    _neighbors.resize( numVertices );
    //    for (unsigned int i = 0; i < numVertices; i++)
    //    {
    //        in >> n >> _input[i].x >> _input[i].y >> _input[i].known;
    //    }
    //
    //    in >> aux >> aux;
    //
    //    for (unsigned int i = 0; i < numVertices; i++)
    //    {
    //        in >> n;
    //        _neighbors[i].resize( n );
    //        for (unsigned int j = 0; j < n; j++)
    //        {
    //            in >> _neighbors[i][j];
    //        }
    //    }
    //
    //    in >> aux;
    //
    //    unsigned int numberFaces;
    //    in >> numberFaces;
    //    _faces.resize( numberFaces );
    //    _distances.resize( numberFaces );
    //    
    //    for (unsigned int i = 0; i < numberFaces; i++)
    //    {
    //        unsigned int n;
    //        in >> n;
    //        _faces[i].resize( 4 );
    //        for (unsigned int j = 0; j < 4; j++)
    //        {
    //            in >> _faces[i][j];
    //        }
    //        in >> _distances[i];
    //    }
    //    
    //    //fim de antes

    Ihandle* canvas = ( Ihandle* ) IupGetAttribute( _dialog, "CANVAS" );

    _pMin.x = 1E10;
    _pMin.y = 1E10;
    _pMax.x = -1E10;
    _pMax.y = -1E10;

    double minX = 0.0;
    double minY = 0.0;
    double maxX = 0.0;
    double maxY = 0.0;
    for (unsigned int i = 0; i < _input.size( ); i++)
    {
        minX = std::min( _pMin.x, _input[i].x );
        minY = std::min( _pMin.y, _input[i].y );
        maxX = std::max( _pMax.x, _input[i].x );
        maxY = std::max( _pMax.y, _input[i].y );

    }
    double aux1 = std::min( maxX, minX );
    double aux2 = std::min( maxY, minY );
    _pMin.x = std::min( aux1, aux2 );
    _pMin.y = std::min( aux1, aux2 );
    aux1 = std::max( maxX, minX );
    aux2 = std::max( maxY, minY );
    _pMax.x = std::max( aux1, aux2 );
    _pMax.y = std::max( aux1, aux2 );

    _scale = fabs( _pMin.x - _pMax.x ) / fabs( _pMin.y - _pMax.y );

    renderFirst( canvas );

    IupUpdate( canvas );



}



void MeshWindow::renderFirst( Ihandle* canvas )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //define que a cor para limpar a janela eh branca
    glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );

    //Limpa a janela com a cor pre-determinada
    glClear( GL_COLOR_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );
    glScaled( window->_scale, 1, 1 );
    glLineWidth( 1.0 );
    glColor3d( 0.0, 0.0, 0.0 );
    glBegin( GL_QUADS );
    for (unsigned int i = 0; i < window->_faces.size( ); i++)
    {
        for (unsigned int j = 0; j < window->_faces[i].size( ); j++)
        {
            glVertex2d( window->_input[window->_faces[i][j]].x, window->_input[window->_faces[i][j]].y );
        }
    }
    glEnd( );


    glBegin( GL_POINTS );
    glColor3d( 0.0, 1.0, 0.0 );
    for (unsigned int i = 0; i < window->_input.size( ); i++)
    {
        if (window->_input[i].known)
        {
            glVertex2d( window->_input[i].x, window->_input[i].y );
        }
    }

    glEnd( );
    //glutSwapBuffers( );
    glEnd( );

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );

    gluOrtho2D( window->_pMin.x, window->_pMax.x, window->_pMin.y, window->_pMax.y );


    //Troca os buffers.
    IupGLSwapBuffers( canvas );


}



int MeshWindow::actionCanvasCallback( Ihandle* canvas )
{
    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //if (window->_input.size( ) != 0)
    //{

    //Torna o canvas como corrente.
    //IupGLMakeCurrent( canvas );

    //Redesenha a janela.
    //window->renderFirst( );

    //Troca os buffers.
    //IupGLSwapBuffers( canvas );
    //}
    //else
    //{
    window->renderFirst( canvas );
    //}

    return IUP_DEFAULT;
}



int MeshWindow::wheelCanvasCallback( Ihandle* canvas, float delta, int x,
                                     int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    delta < 0 ? window->zoom( -1 ) : window->zoom( 1 );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}



void MeshWindow::zoom( int delta )
{
    //Calcula fator de zoom
    float factor = delta > 0 ? 1.05f : 0.95f;

    // Computa a coordenada do centro da área visível
    double cx = 0.5f * ( _pMin.x + _pMax.x );
    double cy = 0.5f * ( _pMin.y + _pMax.y );

    // Computa metade do altura de largura visível
    double halfWidth = 0.5f * ( _pMax.x - _pMin.x );
    double halfHeight = 0.5f * ( _pMax.y - _pMin.y );

    // Aplica o fator de escala para alterar a largura e altura
    halfWidth *= factor;
    halfHeight *= factor;

    _pMin.x = cx - halfWidth;
    _pMax.x = cx + halfWidth;
    _pMin.y = cy - halfHeight;
    _pMax.y = cy + halfHeight;

    //Define a matriz de projecao como matriz corrente. Essa matriz guarda informacoes
    //de como o desenho deve ser projetado na tela, desta forma qualquer definicao
    //relativa a sistema de coordenadas deve ser feita na matriz de projecao
    glMatrixMode( GL_PROJECTION );

    //Carrega a matriz indentida na matriz corrente. Esta operacao substitui a matriz
    //corrente pela matriz identidade
    glLoadIdentity( );

    //Define como o sistema de coordenadas varia dentro do viewport, ou seja,
    //da esquerda para direita o eixo x varia de x1 ate x2 e o eixo y varia de
    //y1 ate y2. Note que nao necessariamente x1 <= x2 ou y1 <= y2
    gluOrtho2D( _pMin.x, _pMax.x,
                _pMin.y, _pMax.y );
}



int MeshWindow::motionCanvasCallback( Ihandle* canvas, int x, int y, char* status )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_mousePressed == IUP_BUTTON1)
    {
        double xi = 0.0, yi = 0.0, xf = 0.0, yf = 0.0f;

        window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );
        window->convertPixelToWorld( x, y, xf, yf );

        if (window->_pointClicked)
        {
            //change the fixed selected point to mouse position
            window->_input[window->_choosenPointIndex].x = xf;
            window->_input[window->_choosenPointIndex].y = yf;
            window->renderFirst( canvas );
        }
        //        else if (!window->_pointClicked)
        //        {
        //
        //
        //            double dx = xf - xi;
        //            double dy = yf - yi;
        //
        //            window->_pMin.x -= dx;
        //            window->_pMax.x -= dx;
        //            window->_pMin.y -= dy;
        //            window->_pMax.y -= dy;
        //
        //            window->_firstPixelX = x;
        //            window->_firstPixelY = y;
        //
        //            glMatrixMode( GL_PROJECTION );
        //            glLoadIdentity( );
        //
        //            gluOrtho2D( window->_pMin.x, window->_pMax.x, window->_pMin.y, window->_pMax.y );
        //        }
    }
    else if (window->_mousePressed == IUP_BUTTON2)
    {
        double xi = 0.0, yi = 0.0, xf = 0.0, yf = 0.0f;

        window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );
        window->convertPixelToWorld( x, y, xf, yf );

        double dx = xf - xi;
        double dy = yf - yi;

        window->_pMin.x -= dx;
        window->_pMax.x -= dx;
        window->_pMin.y -= dy;
        window->_pMax.y -= dy;

        window->_firstPixelX = x;
        window->_firstPixelY = y;

        glMatrixMode( GL_PROJECTION );
        glLoadIdentity( );

        gluOrtho2D( window->_pMin.x, window->_pMax.x, window->_pMin.y, window->_pMax.y );
    }

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );
    return IUP_DEFAULT;
}



int MeshWindow::buttonCanvasCallback( Ihandle* canvas, int button, int pressed,
                                      int x, int y, char* status )
{
    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );
    if (button == IUP_BUTTON1)
    {
        if (pressed)
        {
            window->_firstPixelX = x;
            window->_firstPixelY = y;
            window->_mousePressed = IUP_BUTTON1;

            double xi = 0.0, yi = 0.0;

            window->convertPixelToWorld( window->_firstPixelX, window->_firstPixelY, xi, yi );


            //diagonal do pixel em mundo
            double xi0 = 0.0, yi0 = 0.0;
            double xi1 = 0.0, yi1 = 0.0;
            window->convertPixelToWorld( 0, 0, xi0, yi0 );
            window->convertPixelToWorld( 1, 1, xi1, yi1 );
            double pixelDiagonal2 = ( xi0 - xi1 )*( xi0 - xi1 ) + ( yi0 - yi1 )*( yi0 - yi1 );


            double minDistance2 = -1;
            int currentChosenIndex = 0;
            for (unsigned int i = 0; i < window->_input.size( ); i++)
            {
                double dist2 = ( window->_input[i].x - xi )*( window->_input[i].x - xi ) + ( window->_input[i].y - yi )*( window->_input[i].y - yi );
                if (minDistance2 == -1 || sqrt( dist2 ) < sqrt( minDistance2 ))
                {
                    minDistance2 = dist2;
                    currentChosenIndex = i;
                    printf( "min distance = %.2f\n", minDistance2 );
                }
            }

            printf( "click: %.2f %.2f\n", xi, yi );
            printf( "point: %.2f %.2f\n", window->_input[currentChosenIndex].x, window->_input[currentChosenIndex].y );
            if (sqrt( minDistance2 ) < 3 * sqrt( pixelDiagonal2 ) && window->_input.size( ) > 0)
            {
                printf( "Selected!\n" );
                //printf("click: %.2f %.2f",xi,yi);
                //printf("point: %.2f %.2f",window->_input[currentChosenIndex].x, window->_input[currentChosenIndex].y);
                window->_pointClicked = true;
                window->_choosenPointIndex = currentChosenIndex;
            }
            printf( "diagonal = %.2f\n", pixelDiagonal2 );



            //            //range around the fixed point for selection
            //            double selectionRange = 0.05;
            //            //mark and get index if a fixed point is selected
            //            for (unsigned int i = 0; i < window->_input.size( ); i++)
            //            {
            //                double dx = (window->_input[ i ].x - xi)*(window->_input[ i ].x - xi);
            //                double dy = (window->_input[ i ].y - yi)*(window->_input[ i ].y - yi);
            //                
            //                if (dx < selectionRange*selectionRange &&
            //                    dy < selectionRange*selectionRange)
            //                {
            //                    printf("AquiAchou\n");
            //                    window->_pointClicked = true;
            //                    window->_choosenPointIndex = i;
            //                }
            //            }
        }
        else
        {
            window->_mousePressed = -1;
            window->_pointClicked = false;
        }
    }
    else if (button == IUP_BUTTON2)
    {
        if (pressed)
        {
            window->_firstPixelX = x;
            window->_firstPixelY = y;
            window->_mousePressed = IUP_BUTTON2;
        }
        else
        {
            window->_mousePressed = -1;
            window->_pointClicked = false;
        }
    }

    //    else if (button == IUP_BUTTON3)
    //    {
    //        if (pressed)
    //        {
    //            window->_nFixedPoints++;
    //
    //            Point newPoint;
    //            window->convertPixelToWorld( x, y, newPoint.x, newPoint.y );
    //            window->_fixedPoints.push_back( newPoint );
    //
    //            //only activate components for mor than 1 point
    //            if (window->_nFixedPoints >= 2)
    //            {
    //                //window->activateComponents( canvas );
    //
    //                window->renderFirst( canvas );
    //
    //                IupUpdate( canvas );
    //            }
    //        }
    //    }
    return IUP_DEFAULT;
}



void MeshWindow::convertPixelToWorld( int px, int py, double& x, double& y )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    //Converte ponto em tela para o sistema de coordenadas.
    double t = ( double ) px / ( viewport[2] - 1 );
    x = ( 1 - t ) * _pMin.x + t * _pMax.x;
    t = 1.0 - ( double ) py / ( viewport[3] - 1 );
    y = ( 1 - t ) * _pMin.y + t * _pMax.y;
}



int MeshWindow::runButtonCallback( Ihandle *button )
{

    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( button, "THIS" );
    //    window->_currentRunIndex = 0;
    window->run( );

    return IUP_DEFAULT;
}



void MeshWindow::optimize( double eps, unsigned numIterations )
{
    //Set to solve continue solving the problem.
    _continueSolver = true;

    //Set the epsilon.
    _m->setSolverEps( eps );

    //The the max interation number.
    _m->setMaxIterations( numIterations );

    //Optimize the problem.
    _m->optimize( );
}



void MeshWindow::optimizeCurveCallback( )
{
    //Get the this pointer.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //The eps and max interations.
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( canvas, "MAX_TEXT" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( canvas, "EPS_TEXT" );
    double eps = atof( IupGetAttribute( epsText, IUP_VALUE ) );
    unsigned int numIterations = atoi( IupGetAttribute( maxText, IUP_VALUE ) );

    //Optimize the problem.
    window->optimize( eps, numIterations );
}



int MeshWindow::checkFunction( )
{
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );
    if (window->_fut.wait_for( std::chrono::milliseconds( 16 ) ) == std::future_status::ready)
    {
        //Enable run button. 
        window->activateComponents( window->_dialog );

        window->_fut.get( );
        IupSetFunction( IUP_IDLE_ACTION, NULL );
    }

    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void MeshWindow::run( )
{
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( _dialog, "THIS" );

    //Ihandle* nPointsIntervalText = ( Ihandle* ) IupGetAttribute( canvas, "NPOINTS_TEXT" );
    //unsigned int nPointsPerInterval = atoi( IupGetAttribute( nPointsIntervalText, IUP_VALUE ) );
    //window->_totalPoints = ( int ) ( window->_nFixedPoints - 1 ) * nPointsPerInterval;

    //Construct the model.
    window->buildModel( );

    Ihandle* initList = ( Ihandle* ) IupGetAttribute( _dialog, "INIT_LIST" );
    unsigned int initIndex = atoi( IupGetAttribute( initList, "VALUE" ) );

    Ihandle* modelList = ( Ihandle* ) IupGetAttribute( _dialog, "MODEL_LIST" );
    int modelIndex = atoi( IupGetAttribute( modelList, IUP_VALUE ) );
    if (modelIndex != 5)
    {

        //Initialize solution.
        window->initializeSolution( initIndex );

        window->_fut = std::async( std::launch::async, MeshWindow::optimizeCurveCallback );

    }

    IupSetFunction( IUP_IDLE_ACTION, ( Icallback ) checkFunction );
    //
    //    //set that it is not linear any more.
    //    window->_isNewCurve = false;

    //Desabilta campos 
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( _dialog, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( _dialog, "MAX_TEXT" );
    Ihandle* nPointsPerIntervalText = ( Ihandle* ) IupGetAttribute( _dialog, "NPOINTS_TEXT" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUN_BTN" );
    Ihandle* runXButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUNX_BTN" );
    Ihandle* runYButton = ( Ihandle* ) IupGetAttribute( _dialog, "RUNY_BTN" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( _dialog, "STOP_BTN" );
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( _dialog, "NEW_BTN" );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( initList, IUP_VALUE, "2" );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( nPointsPerIntervalText, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( newButton, IUP_ACTIVE, IUP_NO );

}



void MeshWindow::allocateSolver( int index )
{
    delete _solver;

    switch (index)
    {
        case 1:
            _solver = new LBFGSOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 10 );
            break;
        case 2:
            _solver = new ConjugateGradientLISOptimizationSolver( );


            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 1 );
            break;
        case 3:
            _solver = new ConjugateGradientSLUOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 1 );
            break;
        case 4:
            _solver = new GradientDescentOptimizationSolver( );

            //Set the frequency that the solver must let the window knows about the
            //current solution.
            _m->setStepsNumber( 25 );
            break;
    }
}



void MeshWindow::buildModel( )
{
    delete _m;
    _m = 0;

    //Allocate solver.
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( _dialog, "SOLVER_LIST" );
    int solverIndex = atoi( IupGetAttribute( solverList, IUP_VALUE ) );
    allocateSolver( solverIndex );

    Ihandle* modelList = ( Ihandle* ) IupGetAttribute( _dialog, "MODEL_LIST" );
    int modelIndex = atoi( IupGetAttribute( modelList, IUP_VALUE ) );
    switch (modelIndex)
    {
        case 1:
            _m = getUniformLaplacianModel( _input, _neighbors, _faces );
            break;
        case 2:
            _m = getUniformLaplacianAverageModel( _input, _neighbors, _faces );
            break;
        case 3:
            _m = getDiagonalModel( _input, _neighbors, _faces );
            break;
        case 4:
            _m = getJmetricModel( _input, _neighbors, _faces, _distances );
            break;
        case 5:


            //            _m = getJmetricModel( _input, _neighbors, _faces, _distances );
            //            _m->optimize();           
            //            
            //            for (unsigned int i = 0; i < _input.size( ); i++)
            //            {
            //                _input[i].x = _m->getVariable( 2 * i + 0 ).getValue( );
            //                _input[i].y = _m->getVariable( 2 * i + 1 ).getValue( );
            //                //            printf( "%u: (%.10lf, %.10lf)\n", i + 1, _input[i].x, _input[i].y );
            //            }
            //
            //            Timer t;
            _fut = std::async( std::launch::async, MeshWindow::getJmetricItModel );
            //getJmetricItModel( _input, _neighbors, _faces, _distances, _m );
            //            t.printTime( "Total time" );
            break;
    }

}



OptimizationModel *MeshWindow::getUniformLaplacianModel( const std::vector<Point>& input,
                                                         const std::vector< std::vector< int > >& neighbors,
                                                         std::vector< std::vector< int > >& faces )
{
    //ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
    OptimizationModel*m = new OptimizationModel( _solver );

    _variables.resize( input.size( ) );
    for (unsigned int i = 0; i < input.size( ); i++)
    {
        _variables[i].x = m->addVariable( );
        _variables[i].y = m->addVariable( );

        //_variables[i].x = input[i].x;
        // _variables[i].y = input[i].y;
    }

    QuadraticExpression& obj = m->getObjectiveFunction( );
    for (unsigned int i = 0; i < neighbors.size( ); i++)
    {
        LinearExpression x, y;
        for (unsigned int j = 0; j < neighbors[i].size( ); j++)
        {
            unsigned int idx = neighbors[i][j];
            x += _variables[idx].x;
            y += _variables[idx].y;
        }
        x -= x.size( ) * _variables[i].x;
        y -= y.size( ) * _variables[i].y;

        obj += x * x + y * y;
    }

    for (unsigned int i = 0; i < input.size( ); i++)
    {
        if (input[i].known)
        {
            m->addConstraint( _variables[i].x, EQUAL, input[i].x );
            m->addConstraint( _variables[i].y, EQUAL, input[i].y );
        }
    }

    return m;
}



OptimizationModel *MeshWindow::getUniformLaplacianAverageModel( const std::vector<Point>& input,
                                                                const std::vector< std::vector< int > >& neighbors,
                                                                std::vector< std::vector< int > >& faces )
{
    //ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );
    OptimizationModel*m = new OptimizationModel( _solver );

    _variables.resize( input.size( ) );
    for (unsigned int i = 0; i < input.size( ); i++)
    {
        _variables[i].x = m->addVariable( );
        _variables[i].y = m->addVariable( );

        _variables[i].x = input[i].x;
        _variables[i].y = input[i].y;
    }

    QuadraticExpression& obj = m->getObjectiveFunction( );
    for (unsigned int i = 0; i < neighbors.size( ); i++)
    {
        LinearExpression x, y;
        for (unsigned int j = 0; j < neighbors[i].size( ); j++)
        {
            unsigned int idx = neighbors[i][j];
            x += _variables[idx].x;
            y += _variables[idx].y;
        }
        x = _variables[i].x - x * ( 1.0 / neighbors[i].size( ) );
        y = _variables[i].y - y * ( 1.0 / neighbors[i].size( ) );

        obj += x * x + y * y;
    }

    for (unsigned int i = 0; i < input.size( ); i++)
    {
        if (input[i].known)
        {
            m->addConstraint( _variables[i].x, EQUAL, input[i].x );
            m->addConstraint( _variables[i].y, EQUAL, input[i].y );
        }
    }

    return m;
}



void MeshWindow::getJmetricItModel( )
{
    //Get the this pointer.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    window->_m = window->getJmetricModel( window->_input, window->_neighbors, window->_faces, window->_distances );
    //window->initializeSolution( initIndex );
    window->_m->optimize( );

    for (unsigned int i = 0; i < window->_input.size( ); i++)
    {
        window->_input[i].x = window->_m->getVariable( 2 * i + 0 ).getValue( );
        window->_input[i].y = window->_m->getVariable( 2 * i + 1 ).getValue( );
        //            printf( "%u: (%.10lf, %.10lf)\n", i + 1, _input[i].x, _input[i].y );
    }

    Timer t;


    //enquanto existir negativo ou numero maximo de iteracoes
    QuadraticExpression& obj = window->_m->getObjectiveFunction( );

    bool moreIterations = true;
    unsigned int iterations = 0;
    while (moreIterations && iterations++ < 20)
    {
        moreIterations = false;

        int contIn = 0, contOut = 0;
        //printf( "Objective size: %u %.3lf\n\n", obj.size( ), obj.getValue( ) );
        for (unsigned int i = 0; i < window->_faces.size( ); i++)
        {
            QuadraticExpression l1, l2, l3, l4, A;
            int v0 = window->_faces[i][0];
            int v1 = window->_faces[i][1];
            int v2 = window->_faces[i][2];
            int v3 = window->_faces[i][3];

            Polygon t1, t2, t3, t4;
            t1.resize( 3 );
            t2.resize( 3 );
            t3.resize( 3 );
            t4.resize( 3 );

            t1[0] = Point( window->_input[v0].x, window->_input[v0].y );
            t1[1] = Point( window->_input[v1].x, window->_input[v1].y );
            t1[2] = Point( window->_input[v2].x, window->_input[v2].y );

            t2[0] = Point( window->_input[v1].x, window->_input[v1].y );
            t2[1] = Point( window->_input[v2].x, window->_input[v2].y );
            t2[2] = Point( window->_input[v3].x, window->_input[v3].y );

            t3[0] = Point( window->_input[v2].x, window->_input[v2].y );
            t3[1] = Point( window->_input[v3].x, window->_input[v3].y );
            t3[2] = Point( window->_input[v0].x, window->_input[v0].y );

            t4[0] = Point( window->_input[v3].x, window->_input[v3].y );
            t4[1] = Point( window->_input[v0].x, window->_input[v0].y );
            t4[2] = Point( window->_input[v1].x, window->_input[v1].y );

            double A1 = polygonArea( t1 );
            double A2 = polygonArea( t2 );
            double A3 = polygonArea( t3 );
            double A4 = polygonArea( t4 );

            double eps = 10;
            //COLOCAR IF: se tem area negativa de algum dos quatro triangulos.
            if (A1 < eps || A2 < eps || A3 < eps || A4 < eps)
            {
                moreIterations = true;

                //calcula o quadrado dos lados
                l1 = ( ( window->_variables[v0].x - window->_variables[v1].x )*( window->_variables[v0].x - window->_variables[v1].x ) +
                       ( window->_variables[v0].y - window->_variables[v1].y )*( window->_variables[v0].y - window->_variables[v1].y ) );

                l2 = ( ( window->_variables[v1].x - window->_variables[v2].x )*( window->_variables[v1].x - window->_variables[v2].x ) +
                       ( window->_variables[v1].y - window->_variables[v2].y )*( window->_variables[v1].y - window->_variables[v2].y ) );

                l3 = ( ( window->_variables[v2].x - window->_variables[v3].x )*( window->_variables[v2].x - window->_variables[v3].x ) +
                       ( window->_variables[v2].y - window->_variables[v3].y )*( window->_variables[v2].y - window->_variables[v3].y ) );

                l4 = ( ( window->_variables[v3].x - window->_variables[v0].x )*( window->_variables[v3].x - window->_variables[v0].x ) +
                       ( window->_variables[v3].y - window->_variables[v0].y )*( window->_variables[v3].y - window->_variables[v0].y ) );

                //2 vezes a area do poligono
                A = window->_variables[v0].x * window->_variables[v1].y - window->_variables[v1].x * window->_variables[v0].y +
                    window->_variables[v1].x * window->_variables[v2].y - window->_variables[v2].x * window->_variables[v1].y +
                    window->_variables[v2].x * window->_variables[v3].y - window->_variables[v3].x * window->_variables[v2].y +
                    window->_variables[v3].x * window->_variables[v0].y - window->_variables[v0].x * window->_variables[v3].y;

                //                printf( "Val: %.3lf\n", ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) ).getValue( ) );
                obj += ( 100 ) * ( ( l1 + l2 + l3 + l4 ) - 2 * ( A ) );
                //printf("Aqui\n");

                contIn++;
            }
            else
            {
                contOut++;
            }
        }
        //
        printf( "%d %d\n", contIn, contOut );
        //        break;
        //printf( "Objective size: %u %.3lf\n\n", obj.size( ), obj.getValue( ) );
        if (moreIterations)
        {
            //Initialize solution.
            window->_m->optimize( );
            IupSetFunction( IUP_IDLE_ACTION, ( Icallback ) checkFunction );


            for (unsigned int i = 0; i < window->_input.size( ); i++)
            {
                window->_input[i].x = window->_m->getVariable( 2 * i + 0 ).getValue( );
                window->_input[i].y = window->_m->getVariable( 2 * i + 1 ).getValue( );
                //                printf( "%u: (%.10lf, %.10lf)\n", i + 1, window->_input[i].x, window->_input[i].y );
            }
        }
    }

    //Get solution.
    for (unsigned int i = 0; i < window->_input.size( ); i++)
    {
        window->_input[i].x = window->_m->getVariable( 2 * i + 0 ).getValue( );
        window->_input[i].y = window->_m->getVariable( 2 * i + 1 ).getValue( );
    }

    t.printTime( "Total time" );
    //std::string outName(argv[1]);
    //    std::string outName;
    //    outName += inputName;
    //    outName += "JmetricIt.out";

    //Write file.
    //writeFile( outName.c_str( ), input, neighbors, _faces, _distances );

}



OptimizationModel *MeshWindow::getJmetricModel( const std::vector<Point>& input,
                                                const std::vector< std::vector< int > >& neighbors,
                                                std::vector< std::vector< int > >& faces,
                                                std::vector<int>& distances )
{
    //            ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
    //ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );
    //            LBFGSOptimizationSolver* solver = new LBFGSOptimizationSolver( );

    OptimizationModel*m = new OptimizationModel( _solver );
    m->setSolverEps( 1e-2 );
    m->showLog( false );

    _variables.resize( input.size( ) );
    for (unsigned int i = 0; i < input.size( ); i++)
    {
        _variables[i].x = m->addVariable( );
        _variables[i].y = m->addVariable( );

        _variables[i].x = input[i].x;
        _variables[i].y = input[i].y;
    }

    QuadraticExpression& obj = m->getObjectiveFunction( );

    for (unsigned int i = 0; i < faces.size( ); i++)
    {
        QuadraticExpression l1, l2, l3, l4, A2;
        int v0 = faces[i][0];
        int v1 = faces[i][1];
        int v2 = faces[i][2];
        int v3 = faces[i][3];

        //calcula o quadrado dos lados
        l1 = ( ( _variables[v0].x - _variables[v1].x )*( _variables[v0].x - _variables[v1].x ) +
               ( _variables[v0].y - _variables[v1].y )*( _variables[v0].y - _variables[v1].y ) );

        l2 = ( ( _variables[v1].x - _variables[v2].x )*( _variables[v1].x - _variables[v2].x ) +
               ( _variables[v1].y - _variables[v2].y )*( _variables[v1].y - _variables[v2].y ) );

        l3 = ( ( _variables[v2].x - _variables[v3].x )*( _variables[v2].x - _variables[v3].x ) +
               ( _variables[v2].y - _variables[v3].y )*( _variables[v2].y - _variables[v3].y ) );

        l4 = ( ( _variables[v3].x - _variables[v0].x )*( _variables[v3].x - _variables[v0].x ) +
               ( _variables[v3].y - _variables[v0].y )*( _variables[v3].y - _variables[v0].y ) );

        //2 vezes a area do poligono
        A2 = _variables[v0].x * _variables[v1].y - _variables[v1].x * _variables[v0].y +
            _variables[v1].x * _variables[v2].y - _variables[v2].x * _variables[v1].y +
            _variables[v2].x * _variables[v3].y - _variables[v3].x * _variables[v2].y +
            _variables[v3].x * _variables[v0].y - _variables[v0].x * _variables[v3].y;

        //        double t = ( ( double ) distances[i] - dMin ) / ( dMax - dMin );
        //        double w = ( 1 - t ) * pMax + t * pMin;
        //        if (distances[i] == 0)
        double w = 2 * ( 100 ); //pMax;

        obj += w * w * ( l1 + l2 + l3 + l4 - 2 * A2 );
    }

    for (unsigned int i = 0; i < input.size( ); i++)
    {
        if (input[i].known)
        {
            m->addConstraint( _variables[i].x, EQUAL, input[i].x );
            m->addConstraint( _variables[i].y, EQUAL, input[i].y );
        }
    }
    return m;
}



OptimizationModel *MeshWindow::getDiagonalModel( const std::vector<Point>& input,
                                                 const std::vector< std::vector< int > >& neighbors,
                                                 std::vector< std::vector< int > >& faces )
{
    //    ConjugateGradientSLUOptimizationSolver* solver = new ConjugateGradientSLUOptimizationSolver( );
    //ConjugateGradientLISOptimizationSolver* solver = new ConjugateGradientLISOptimizationSolver( );

    OptimizationModel*m = new OptimizationModel( _solver );

    _variables.resize( input.size( ) );
    for (unsigned int i = 0; i < input.size( ); i++)
    {
        _variables[i].x = m->addVariable( );
        _variables[i].y = m->addVariable( );

        _variables[i].x = input[i].x;
        _variables[i].y = input[i].y;
    }

    QuadraticExpression& obj = m->getObjectiveFunction( );
    for (unsigned int i = 0; i < faces.size( ); i++)
    {
        LinearExpression x, y;
        int v0 = faces[i][0];
        int v1 = faces[i][1];
        int v2 = faces[i][2];
        int v3 = faces[i][3];

        x = ( ( _variables[v0].x + _variables[v2].x ) - ( _variables[v1].x + _variables[v3].x ) );
        y = ( ( _variables[v0].y + _variables[v2].y ) - ( _variables[v1].y + _variables[v3].y ) );

        obj += x * x / 4 + y * y / 4;
    }

    for (unsigned int i = 0; i < input.size( ); i++)
    {
        if (input[i].known)
        {
            m->addConstraint( _variables[i].x, EQUAL, input[i].x );
            m->addConstraint( _variables[i].y, EQUAL, input[i].y );
        }
    }
    return m;
}



void MeshWindow::initializeSolution( unsigned int initIndex )
{
    //Get solution.

    if (initIndex == 1)
    {
        for (unsigned int i = 0; i < _input.size( ); i++)
        {
            if (!_input[i].known)
            {
                _input[i].x = 0.0;
                _input[i].y = 0.0;
                _m->getVariable( 2 * i + 0 ) = 0.0;
                _m->getVariable( 2 * i + 1 ) = 0.0;
            }
        }
    }
    else if (initIndex == 2 /*&& !_isNewCurve*/)
    {
        for (unsigned int i = 0; i < _input.size( ); i++)
        {
            if (!_input[i].known)
            {
                _m->getVariable( 2 * i + 0 ) = _input[i].x;
                _m->getVariable( 2 * i + 1 ) = _input[i].y;
            }
        }

    }

    //Set callback to update data.
    _m->setDisplayInformationCallback( displayFunction );

    //Define the solver.
    _m->setOptimizationSolver( _solver );
}



bool MeshWindow::displayFunction( const std::vector<double>& v, double gnorm )
{
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );
    return window->updateSolution( v, gnorm );
}



bool MeshWindow::updateSolution( const std::vector<double>& v, double gnorm )
{
    _mtx.lock( );
    unsigned int total;

    total = _input.size( );

    //Get the number of threads.
    const int tMax = omp_get_max_threads( );
    const int numThreads = std::max( tMax - 2, 1 );

    #pragma omp parallel for num_threads(numThreads)
    for (unsigned int i = 0; i < total; i++)
    {

        _input[i].x = v[ 2 * i + 0 ];
        _input[i].y = v[ 2 * i + 1 ];
    }

    //updateWindowLabes( _solver[_currentRunIndex]->getNumberIterations( ), gnorm );
    _mtx.unlock( );
    return _continueSolver;
}



void MeshWindow::activateComponents( Ihandle* component )
{
    //Habilita campos
    Ihandle* solverList = ( Ihandle* ) IupGetAttribute( component, "SOLVER_LIST" );
    Ihandle* initList = ( Ihandle* ) IupGetAttribute( component, "INIT_LIST" );
    Ihandle* epsText = ( Ihandle* ) IupGetAttribute( component, "EPS_TEXT" );
    Ihandle* maxText = ( Ihandle* ) IupGetAttribute( component, "MAX_TEXT" );
    Ihandle* nPointsPerIntervalText = ( Ihandle* ) IupGetAttribute( component, "NPOINTS_TEXT" );
    Ihandle* stopButton = ( Ihandle* ) IupGetAttribute( component, "STOP_BTN" );
    Ihandle* runButton = ( Ihandle* ) IupGetAttribute( component, "RUN_BTN" );
    Ihandle* runXButton = ( Ihandle* ) IupGetAttribute( component, "RUNX_BTN" );
    Ihandle* runYButton = ( Ihandle* ) IupGetAttribute( component, "RUNY_BTN" );
    Ihandle* newButton = ( Ihandle* ) IupGetAttribute( component, "NEW_BTN" );
    IupSetAttribute( solverList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( initList, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( epsText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( maxText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( nPointsPerIntervalText, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( stopButton, IUP_ACTIVE, IUP_NO );
    IupSetAttribute( runButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( runXButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( runYButton, IUP_ACTIVE, IUP_YES );
    IupSetAttribute( newButton, IUP_ACTIVE, IUP_YES );
}



int MeshWindow::resizeCanvasCallback( Ihandle *canvas, int width, int height )
{
    //Torna o canvas como corrente.
    IupGLMakeCurrent( canvas );

    //Obtem ponteiro para o this.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    //Redesenha a janela.
    window->resizeCanvas( width, height );

    //Marca o canvas para ser redesenhado.
    IupUpdate( canvas );

    return IUP_DEFAULT;
}



void MeshWindow::resizeCanvas( int width, int height )
{
    //Recupera o viewport para obter as dimensoes da janela.
    GLint viewport[4];
    glGetIntegerv( GL_VIEWPORT, viewport );

    double widthFactor = ( double ) width / viewport[2];
    double heightFactor = ( double ) height / viewport[3];

    //Define que toda a janela sera usada para desenho. Isso eh feito atras da
    //definicao do viewport, que recebe o ponto inferior esquerdo, a largura e
    //a altura da janela.
    glViewport( 0, 0, width, height );

    //calcula a largura e altura em mundo da janela
    double widthWorld = fabs( _pMin.x - _pMax.x );
    double heightWorld = fabs( _pMin.y - _pMax.y );

    widthWorld *= widthFactor;
    heightWorld *= heightFactor;

    //calcula o centro da imagem em mundo
    double cx = ( _pMin.x + _pMax.x ) / 2.0;
    double cy = ( _pMin.y + _pMax.y ) / 2.0;

    //calcula o novo sistema de coordenadas
    _pMin.x = cx - 0.5 * widthWorld;
    _pMax.x = cx + 0.5 * widthWorld;
    _pMin.y = cy - 0.5 * heightWorld;
    _pMax.y = cy + 0.5 * heightWorld;

    //Define sistema de coordenadas.
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    gluOrtho2D( _pMin.x, _pMax.x, _pMin.y, _pMax.y );
}



int MeshWindow::stopButtonCallback( Ihandle *button )
{
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( button, "THIS" );

    window->_continueSolver = false;

    //    for (unsigned int i = 0; i < window->_m[_currentRunIndex]->getNumberVariables( ); i++)
    //    {
    //        if (!window->_m[_currentRunIndex]->getVariable( i ).isKnownValue( ))
    //        {
    //            printf("%.2f %.2f ", window->_m[_currentRunIndex]->getVariable(i).getValue(), window->_coordinates[i]);
    //            window->_m[_currentRunIndex]->getVariable( i ) = window->_coordinates[i];
    //            printf("- %.2f %.2f \n", window->_m[_currentRunIndex]->getVariable(i).getValue(), window->_coordinates[i]);
    //        }
    //    }

    return IUP_DEFAULT;
}



int MeshWindow::exitButtonCallback( Ihandle* button )
{
    //Get the this pointer.
    MeshWindow *window = ( MeshWindow* ) IupGetAttribute( canvas, "THIS" );

    if (window->_fut.valid( ))
    {
        window->_continueSolver = false;
        window->_fut.wait( );
    }
    return IUP_CLOSE;
}