/* 
 * File:   Geometry.h
 * Author: jcoelho
 *
 * Created on October 22, 2015, 6:29 PM
 */

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <vector>
#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
namespace geom
{
    const double EPS = 1e-10;

    inline int cmp( double x, double y = 0, double tol = EPS )
    {
        return (x <= y + tol) ? (x + tol < y) ? -1 : 0 : 1;
    }

    struct Point
    {
        double x, y;
        bool known;

        Point( double x = 0, double y = 0 ) : x( x ), y( y )
        {
        }

        Point operator+(Point q)const
        {
            return Point( x + q.x, y + q.y );
        }

        Point operator-(Point q)const
        {
            return Point( x - q.x, y - q.y );
        }

        Point operator*( double t ) const
        {
            return Point( x * t, y * t );
        }

        Point operator/( double t ) const
        {
            return Point( x / t, y / t );
        }

        double operator*(Point q)const
        {
            return x * q.x + y * q.y;
        }

        double operator^(Point q)const
        {
            return x * q.y - y * q.x;
        }

        int cmp( Point q ) const
        {
            if ( int t = ::geom::cmp( x, q.x ) ) return t;
            return ::geom::cmp( y, q.y );
        }

        bool operator==(Point q) const
        {
            return cmp( q ) == 0;
        }

        bool operator!=(Point q) const
        {
            return cmp( q ) != 0;
        }

        bool operator<(Point q) const
        {
            return cmp( q ) < 0;
        }

        friend ostream& operator<<(ostream& o, Point p)
        {
            return o << "(" << p.x << ", " << p.y << ")";
        }
        static Point pivot;
    };

    //Point Point::pivot;

    typedef vector<Point> Polygon;

    inline double abs( Point& p );

    inline double arg( Point& p );

    //Verifica o sinal do produto vetorial entre os vetores (p-r) e (q - r)
    inline int ccw( Point& p, Point& q, Point& r );



    //calcula o angulo orientado entre os vetores (p-q) e (r - q)
    inline double angle( Point& p, Point &q, Point& r );



    //Decide se o ponto p esta sobre a reta que passa por p1p2.
    bool pointoSobreReta( Point& p1, Point &p, Point& p2 );



    //Decide se dois segmentos de reta se interceptam ou nao.
    bool segIntersect( Point& p1, Point& p2, Point& p3, Point& p4 );



    //Decide de p esta sobre o segmento p1p2
    bool between( Point& p1, Point &p, Point& p2 );



    //Calcula a distancia do ponto p a reta que passa por p1p2
    double retaDistance( Point& p1, Point& p2, Point &p );



    //Calcula a distancia do ponto p ao segmento de reta que passa por p1p2
    double segDistance( Point& p1, Point& p2, Point &p );



    //Calcula a area orientada do poligono T.
    double polygonArea( Polygon& T );



    //Classifica o ponto p em relacao ao poligono T dependendo se ele está
    //na fronteira (-1) no exterior (0) ou no interior (1).
    int inpoly( Point& p, Polygon& T );


    //Ordenacao radial.
    bool radialSort( Point p, Point q );


    //Determina o convex hull de T. ATENCAO. A lista de pontos T e destruida.
    Polygon convexHull( vector<Point>& T );


    //Calcula a intersecao de duas reta.
    //-1 a mesma reta, 1, retas paralelas, 0 retas com intersecao.
    int computeLineIntersection( Point& p1, Point& p2, Point& p3, Point& p4, Point& out );
};
#endif /* GEOMETRY_H */

