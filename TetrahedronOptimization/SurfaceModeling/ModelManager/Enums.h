/*
 * File:   Enums.h
 * Author: jcoelho
 *
 * Created on May 9, 2016, 3:09 PM
 */

#pragma once

#ifdef	__cplusplus
extern "C"
{
#endif

    enum ContraintType
    {
        EQUAL = '=',
        LESS_EQUAL = '<',
        GREATER_EQUAL = '>',
        SOFT = '~'
    };



#ifdef	__cplusplus
}
#endif


