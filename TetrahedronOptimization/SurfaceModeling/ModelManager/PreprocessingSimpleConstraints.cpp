#include <vector>
#include <cmath>
#include "PreprocessingSimpleConstraints.h"
#include "LinearExpression.h"
#include "QuadraticExpression.h"
#include "Constraint.h"

namespace ModelManager
{



    bool PreprocessingSimpleConstraints::computeValues( std::vector<Constraint>& c )
    {
        bool infeasible = false;
        #pragma omp parallel for num_threads(_numberOfThreads)
        for (unsigned int i = 0; i < c.size( ); i++)
        {
            if (infeasible)
                continue;

            const LinearExpression& exp = c[i].getLine( );

            if (exp.size( ) == 0)
                continue;

            Variable x = exp.getVariable( 0 );
            double a = exp.getCoefficient( 0 );
            double b = exp.getConstant( );

            //The possibility to a be zero must be checked by the model.

            double value = ( -b ) / a;

            //Verify if the variable has already a known value to check the
            //feasibility.
            if (x.isKnownValue( ))
            {
                if (fabs( x.getValue( ) - value ) >= 1e8)
                {
                    infeasible = true;
                }
            }
            x.setValue( value );
            x.knownValue( true );
        }

        return !infeasible;
    }



    void PreprocessingSimpleConstraints::simplifyObjectiveFunction( QuadraticExpression* obj )
    {
        unsigned int size = obj->size( );
        for (unsigned int i = 0; i < size; i++)
        {
            Variable vi = obj->getVar1( i );
            Variable vj = obj->getVar2( i );
            double c = obj->getCoefficient( i );
            bool xi = vi.isKnownValue( );
            bool xj = vj.isKnownValue( );

            if (!xi && xj)
            {
                obj->setCoefficient( i, 0.0 );
                ( *obj ) += c * vi * vj.getValue( );
            }
            else if (xi && !xj)
            {
                obj->setCoefficient( i, 0.0 );
                ( *obj ) += c * vj * vi.getValue( );
            }
            else if (xi && xj)
            {
                obj->setCoefficient( i, 0.0 );
                ( *obj ) += c * vi.getValue( ) * vj.getValue( );
            }
        }

        LinearExpression& l = obj->getLinearExpression( );
        for (unsigned int f = 0; f < l.size( ); f++)
        {
            Variable vi = l.getVariable( f );
            double c = l.getCoefficient( f );
            if (vi.isKnownValue( ))
            {
                l.setCoefficient( f, 0.0 );
                l += c * vi.getValue( );
            }
        }
    }



    bool PreprocessingSimpleConstraints::preprocessingConstrainst( std::vector<Constraint>& c, QuadraticExpression* obj,
                                                                   unsigned int numberOfThreads )
    {
        _numberOfThreads = numberOfThreads;
        
        //Compute the values for the variables.
        bool feasible = computeValues( c );

        //Simplify the objective function.
        if (!feasible)
            return false;

        //Use the values to simplify the objective function.
        simplifyObjectiveFunction( obj );

        return true;
    }

} // namespace ModelManager
