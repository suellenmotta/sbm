#include <cstdio>

#include "Variable.h"
#include "VariableRepresentation.h"

namespace ModelManager
{



    Variable::Variable( unsigned int index, std::string name, double lb, double up )
    {
        _var = new VariableRepresentation( index, name, lb, up );
    }



    void Variable::destroy( )
    {
        delete _var;
    }



    void Variable::knownValue( const bool known )
    {
        if (_var)
        {
            _var->knownValue( known );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    Variable::Variable( )
    {
        _var = 0;
    }



    Variable::Variable( const Variable& v )
    {
        _var = v._var;
    }



    Variable::~Variable( )
    {
    }



    unsigned int Variable::getIndex( ) const
    {
        if (_var)
        {
            return _var->getIndex( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return -1;
    }



    double Variable::getLowerBound( ) const
    {
        if (_var)
        {
            return _var->getLowerBound( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return -VAR_INFINITY;
    }



    const std::string Variable::getName( ) const
    {
        if (_var)
        {
            return _var->getName( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return "";
    }



    double Variable::getUpperBound( ) const
    {
        if (_var)
        {
            return _var->getUpperBound( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return -VAR_INFINITY;
    }



    double Variable::getValue( ) const
    {
        if (_var)
        {
            return _var->getValue( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return 0.0;
    }



    void Variable::setLowerBound( double lb )
    {
        if (_var)
        {
            _var->setLowerBound( lb );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    bool Variable::isKnownValue( ) const
    {
        if (_var)
        {
            return _var->isKnownValue( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return false;
    }



    Variable Variable::operator=( const Variable& v )
    {
        _var = v._var;
        return *this;
    }



    void Variable::operator=( const double value )
    {
        if (_var)
        {
            return _var->setValue( value );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    void Variable::operator=( const std::string& name )
    {
        if (_var)
        {
            return _var->setName( name );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    bool Variable::operator==( const Variable& v ) const
    {
        return _var == v._var;
    }



    bool Variable::operator!=( const Variable& v ) const
    {
        return _var != v._var;
    }



    bool Variable::operator<( const Variable& v ) const
    {
        if (_var)
        {
            return _var->getIndex( ) < v.getIndex( );
        }
        return false;
    }



    void Variable::setName( const std::string &name )
    {
        if (_var)
        {
            _var->setName( name );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    void Variable::setUpperBound( double up )
    {
        if (_var)
        {
            _var->setUpperBound( up );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }



    void Variable::setValue( double value )
    {
        if (_var)
        {
            _var->setValue( value );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
    }
} // namespace ModelManager
