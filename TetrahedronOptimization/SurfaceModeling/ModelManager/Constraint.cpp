#include "Constraint.h"
#include "ConstraintRepresentation.h"
#include "Enums.h"

namespace ModelManager
{

    Constraint::Constraint( const LinearExpression& line, char sense, double wc, std::string name )
    {
        _constraint = new ConstraintRepresentation( line, sense, wc, name );
    }



    Constraint::Constraint( const LinearExpression &line, char sense, double constant, double wc, std::string name )
    {
        _constraint = new ConstraintRepresentation( line, sense, constant, wc, name );
    }



    Constraint::Constraint( const LinearExpression &line, double lb, double up, double wc, std::string name )
    {
        _constraint = new ConstraintRepresentation( line, lb, up, wc, name );
    }



    void Constraint::destroy( )
    {
        delete _constraint;
    }



    void Constraint::setIsKnown( const bool known )
    {
        if (_constraint)
        {
            _constraint->setIsKnown( known );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
    }



    Constraint::Constraint( )
    {
        _constraint = nullptr;
    }



    Constraint::~Constraint( )
    {
    }



    LinearExpression& Constraint::getLine( )
    {
        if (_constraint)
        {
            return _constraint->getLine( );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
        return *( new LinearExpression( ) );
    }



    const LinearExpression& Constraint::getLine( ) const
    {
        if (_constraint)
        {
            return _constraint->getLine( );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
        return *( new LinearExpression( ) );
    }



    const std::string Constraint::getName( ) const
    {
        if (_constraint)
        {
            return _constraint->getName( );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
        return "";
    }
    
    const double Constraint::getWc() const
    {
        if (_constraint)
        {
            return _constraint->getWc();
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
        return 1; 
    }

    char Constraint::getSense( ) const
    {
        if (_constraint)
        {
            return _constraint->getSense( );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
        return 0;
    }



    bool Constraint::operator==( const Constraint& c ) const
    {
        return _constraint == c._constraint;
    }



    void Constraint::setName( const std::string &name )
    {
        if (_constraint)
        {
            _constraint->setName( name );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
    }
    
    void Constraint::setWc( double wc)
    {
         if (_constraint)
        {
            _constraint->setWc(wc);
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
    }



    void Constraint::setSense( char sense )
    {
        if (_constraint)
        {
            _constraint->setSense( sense );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
    }



    void Constraint::update( )
    {
        if (_constraint)
        {
            _constraint->update( );
        }
        else
        {
            printf( "Constraint is not on Model.\n" );
        }
    }



    bool Constraint::getIsKnown( ) const
    {
        if (_constraint)
        {
            return _constraint->getIsKnown( );
        }
        else
        {
            printf( "Variable is not on Model.\n" );
        }
        return false;
    }



    std::ostream& operator<<( std::ostream &stream, const Constraint &c )
    {
        const LinearExpression& exp = c.getLine( );
        for (unsigned int i = 0; i < exp.size( ); i++)
        {
            double c = exp.getCoefficient( i );
            if (c < 0.0)
            {
                stream << "- " << -c << " * " << exp.getVariable( i ).getName( ) << " ";
            }
            else
            {
                stream << "+ " << c << " * " << exp.getVariable( i ).getName( ) << " ";
            }
        }

        double constant = -exp.getConstant( );
        switch (c.getSense( ))
        {
            case EQUAL:
                stream <<"= ";
                break;
            case LESS_EQUAL:
                stream << "<= ";
                break;
            case GREATER_EQUAL:
                stream << ">= ";
                break;
            case SOFT:
                stream << "~= ";
                break;

        }

        if (constant != 0.0)
        {
            if (constant < 0.0)
            {
                stream << "- " << -constant;
            }
            else
            {
                stream << "+ " << constant;
            }
        }
        else
        {
            stream << "+ 0";
        }
        return stream;
    }
} // namespace ModelManager
