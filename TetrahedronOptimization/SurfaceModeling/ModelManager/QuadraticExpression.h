#pragma once

#include "LinearExpression.h"
#include "Variable.h"

namespace ModelManager
{
    class QuadraticExpression;

    std::ostream& operator<<(std::ostream &stream, const QuadraticExpression &exp);
    QuadraticExpression operator+( const QuadraticExpression &x, const QuadraticExpression &y);
    QuadraticExpression operator-( const QuadraticExpression &x, const QuadraticExpression &y);
    QuadraticExpression operator+( const QuadraticExpression &x);
    QuadraticExpression operator-( const QuadraticExpression &x);
    QuadraticExpression operator*( const QuadraticExpression &x, double a );
    QuadraticExpression operator*( double a, const QuadraticExpression &x);
    QuadraticExpression operator*(Variable x, Variable y);
    QuadraticExpression operator*(Variable x, const LinearExpression &y);
    QuadraticExpression operator*( const LinearExpression &y, Variable x);
    QuadraticExpression operator*( const LinearExpression &x, const LinearExpression &y);
    QuadraticExpression operator/( const QuadraticExpression &x, double a );

    class QuadraticExpression
    {
    private:

        struct QuadraticTerm
        {
            /**
             * The coefficient
             */
            double _coefficient;

            /**
             * The first variable.
             */
            Variable _var1;

            /**
             * The second variable.
             */
            Variable _var2;

            /**
             * Default constructor. Just to resize. 
             */
            QuadraticTerm( )
            {

            }

            /**
             * The constructor of the term cv1v2.
             * @param c - the coefficient.
             * @param v1 - the first variable.
             * @param v2 - the second variable.
             */
            QuadraticTerm( double c, Variable v1, Variable v2 )
            {
                _var1 = v1;
                _var2 = v2;
                _coefficient = c;
            }

            /**
             * Operator to sort two quadratic terms.
             * @param q - another term to be compared with the current term.
             * @return - true if the order is correct and false otherwise.
             */
            bool operator<( const QuadraticTerm& q) const
            {
                if ( _var1.getIndex( ) < q._var1.getIndex( ) )
                {
                    return true;
                }
                else if ( _var1.getIndex( ) == q._var1.getIndex( ) )
                {
                    return _var2.getIndex( ) < q._var2.getIndex( );
                }
                return false;
            }
        };

        /**
         * The linear part of the quadratic expression.
         */
        LinearExpression _linearExpression;

        /**
         * The quadratic term to represent c_ij Xi Xj
         */
        std::vector<QuadraticTerm*> _quadraticTerms;

        /**
         * Define if the expression is updated or not.
         */
        bool _isUpdated;

    private:
        /**
         * Compare two terms in sort method.
         * @param t1 - first terms
         * @param t2 - second term.
         * @return - true if the order is correct and false otherwise.
         */
        static bool compareTerms( const QuadraticTerm* t1, const QuadraticTerm* t2 );

        /**
         * Sort the quadratic expression using  bucket sort.
         * @param minIndex - min variable index.
         * @param maxIndex - max variable index.
         */
        void bucketSort( unsigned int minIndex, unsigned int maxIndex );
    public:

        /**
         * The quadratic expression constructor that receives a constant.
         * @param constant - the constant of the quadratic expression.
         */
        QuadraticExpression( double constant = 0.0 );

        /**
         * The quadratic expression constructor that receives a variable and
         * its coefficient.
         * @param var - the initial variable on quadratic expression.
         * @param coefficient - the variable coefficient.
         */
        QuadraticExpression( const Variable var, double coefficient = 1.0 );

        /**
         * The quadratic expression constructor that receives a linear expression.
         * @param le - the initial expression to initialize the quadratic
         * expression.
         */
        QuadraticExpression( const LinearExpression &le );

        /**
         * Copy constructor.
         * @param q - new quadratic expression
         */
        QuadraticExpression( const QuadraticExpression &q );

        /**
         * Destructor.
         */
        ~QuadraticExpression( );

        /**
         * Add a linear expression to quadratic expression.
         * @param le - the linear expression to be added quadratic expression.
         */
        void add( const LinearExpression &le );

        /**
         * Add a constant to quadratic expression.
         * @param c - constant to be added to quadratic expression.
         */
        void addConstant( double c );

        /**
         * Add a new linear term to quadratic expression.
         * @param coefficient - the coefficient of the variable.
         * @param var - variable to be summed to quadratic expression.
         */
        void addTerm( double coefficient, Variable var );

        /**
         * Add a quadratic term to quadratic expression.
         * @param coefficient - the coefficient of the quadratic term.
         * @param var1 - the first variable of the product.
         * @param var2 - the second variable of the product.
         */
        void addTerm( double coefficient, Variable var1, Variable var2 );

        /**
         * Clear the quadratic expression.
         */
        void clear( );

        /**
         * Compute the gradient of the function.
         * @return - the derivative expression.
         */
        std::vector<LinearExpression> computeGradient( unsigned int numberVariables ) const;

        /**
         * Verify if a variable v is on quadratic expression.
         * @param v - variable
         * @return - true if v is on quadratic expression, false otherwise.
         */
        bool exist( const Variable v ) const;
        
        /**
         * Get the coefficient of the term i.
         * @param i - index of the term to get the coefficient.
         * @return - the coefficient of the term i.
         */
        double getCoefficient( unsigned int i ) const;

        /**
         * Get the linear part of the quadratic term.
         * @return - the linear part of the quadratic term.
         */
        const LinearExpression& getLinearExpression( ) const;

        /**
         * Get the linear part of the quadratic term.
         * @return - the linear part of the quadratic term.
         */
        LinearExpression& getLinearExpression( );

        /**
         * Get the value of the quadratic expression.
         * @return - the value of the quadratic expression.
         */
        double getValue( ) const;

        /**
         * Get the first variable of the term of index i.
         * @param i - index of the term.
         * @return - the first variable of the term of index i.
         */
        Variable getVar1( unsigned int i ) const;

        /**
         * Get the second variable of the term of index i.
         * @param i - index of the term.
         * @return - the second variable of the term of index i.
         */
        Variable getVar2( unsigned int i ) const;

        /**
         * Return if the objective functions is updated or not.
         * @return - if the objective functions is updated or not.
         */
        bool isUpdated( ) const;

        /**
         * Overload of the operator << to print the quadratic expression.
         * @param stream - the stream that will be used to print the linear
         * expression.
         * @param exp - quadratic expression that needs to be printed.
         * @return - the strem with the linear expression printed.
         */
        friend std::ostream& operator<<(std::ostream &stream, const QuadraticExpression &exp);

        /**
         * The overload of operator + to sum two quadratic expression.
         * @param x - the left hand side of the quadratic expression.
         * @param y - the right hand side of the quadratic expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator+( const QuadraticExpression &x, const QuadraticExpression &y);

        /**
         * The overload of operator - to subtract two quadratic expression.
         * @param x - the left hand side of the quadratic expression.
         * @param y - the right hand side of the quadratic expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator-( const QuadraticExpression &x, const QuadraticExpression &y);

        /**
         * The overload of operator + to set the sign of the quadratic expression.
         * @param x - the right hand side of the quadratic expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator+( const QuadraticExpression &x);

        /**
         * The overload of operator - to set the sign of the quadratic expression.
         * @param x - the right hand side of the quadratic expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator-( const QuadraticExpression &x);

        /**
         * The overload of operator * multiply a quadratic expression by a
         * constant.
         * @param x - the quadratic expression.
         * @param a - the constant.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( const QuadraticExpression &x, double a );

        /**
         * The overload of operator * multiply a constant by a quadratic
         * expression.
         * @param x - the quadratic expression.
         * @param a - the constant.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( double a, const QuadraticExpression &x);

        /**
         * The overload of operator * to multiply two variables and return a
         * quadratic expression.
         * @param x - the left hand side variable.
         * @param y - the right hand side variable.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( const Variable x, const Variable y );

        /**
         * The overload of operator * to multiply a variable to a linear 
         * expression.
         * @param x - the left hand side variable.
         * @param y - the right hand side linear expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( const Variable x, const LinearExpression &y);

        /**
         * The overload of operator * to multiply a linear expression to 
         * a variable.
         * @param x - the left hand side linear expression.
         * @param y - the right hand side variable.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( const LinearExpression &y, const Variable x );

        /**
         * The overload of operator * to multiply two linear expressions
         * @param x - the left hand side linear expression.
         * @param y - the right hand side linear expression.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator*( const LinearExpression &x, const LinearExpression &y);

        /**
         * The overload of operator / to divide a quadratic expression to a
         * constant.
         * @param x - the quadratic expression.
         * @param a - the constant.
         * @return - the resultant quadratic expression.
         */
        friend QuadraticExpression operator/( const QuadraticExpression &x, double a );

        /**
         * The overload of the operator = to copy a quadratic expression to the
         * current quadratic expression.
         * @param rhs - the quadratic expression that must be copied to the
         * current quadratic expression.
         * @return - the resultant quadratic expression.
         */
        QuadraticExpression operator=( const QuadraticExpression &rhs);

        /**
         * The overload of the operator += to sum a quadratic expression to the
         * current quadratic expression.
         * @param exp - the quadratic expression that must be summed with the
         * current quadratic expression.
         */
        void operator+=( const QuadraticExpression &exp);

        /**
         * The overload of the operator -= to subtract a quadratic expression to the
         * current quadratic expression.
         * @param exp - the quadratic expression that must be subtracted with the
         * current quadratic expression.
         */
        void operator-=( const QuadraticExpression &exp);

        /**
         * The overload of the operator *= to multiply the quadratic expression
         * by a constant.
         * @param mult - constant that must be multiplied by the current quadratic
         * expression.
         */
        void operator*=( double mult );

        /**
         * The overload of the operator /= to divide a constant by the current
         * quadratic expression.
         * @param a - constant that must be multiplied by the current quadratic
         * expression. If a = 0.0, nothing will be made.
         */
        void operator/=( double a );

        /**
         * The overload of the operator + sum a quadratic expression with to the
         * current expression. 
         * @param rhs - the quadratic expression that must be summed with the
         * current quadratic expression.
         * @return - the resultant quadratic expression.
         */
        QuadraticExpression operator+( const QuadraticExpression &rhs);

        /**
         * The overload of the operator - subtract a quadratic expression with to the
         * current expression. 
         * @param rhs - the quadratic expression that must be subtracted with the
         * current quadratic expression.
         * @return - the resultant quadratic expression.
         */
        QuadraticExpression operator-( const QuadraticExpression &rhs);

        /**
         * Remove the term with index i in the quadratic expression.
         * @param i - index of the quadratic expression that must be removed.
         */
        void remove( unsigned int i );

        /**
         * Remove the variable v.
         * @param v - variable that must be removed.
         * @return - true if the variable v was found and removed and false
         * otherwise.
         */
        bool remove( const Variable v );

        /**
         * Reserve space on vectors.
         * @param n - size to reserve.
         */
        void reserve( unsigned int n );

        /**
         * Define a new coefficient to i-th term.
         * @param u - term index.
         * @param c - new coefficient.
         */
        void setCoefficient( unsigned int i, double c );

        /**
         * Get the number of terms in the quadratic expression.
         * @return - the number of variable in the quadratic expression.
         */
        unsigned int size( ) const;

        /**
         * Group the expression and sort it.
         * @log - flag to indicates if the log must be showed or not.
         */
        void update( bool log = false );
    };
} // namespace ModelManager
