#pragma once

#include "Model.h"

namespace Graph
{
    class DSIGraph;
}

namespace Solver
{
    class DSIBasedSolver;
}

namespace ModelManager
{

    class DSIModel : public Model
    {
    private:
        /**
         * Graph to get topological requests.
         */
        Graph::DSIGraph *_graph;

        /**
         * A DSI based solver that will be used to solve the model.
         */
        Solver::DSIBasedSolver *_solver;
    public:
        /**
         * Constructor that receives a solver that will be used to solve the
         * model.
         * @param solver - a DSI based solver to solve the model.
         */
        DSIModel( Solver::DSIBasedSolver *solver );

        /**
         * Optimize the model.
         */
        bool optimize( );

        /**
         * Define a graph to be used to topological requests about the relationship
         * of the vertices.
         * @param graph - graph to be used by the solver.
         */
        void setGraph( Graph::DSIGraph *graph );
        
        /**
         * Get the number of iterations used to solve the last model.
         * @return - number of iterations used to solve the last model.
         */
        unsigned int getNumberIterations( ) const;

        /**
         * Get the current maximum iterations.
         * @return - the current maximum iterations.
         */
        unsigned int getMaxIterations( ) const;

        /**
         * Get the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @return - steps number.
         */
        unsigned int getStepsNumber( ) const;


        /**
         * Set a callback function to be called after stepsNumber iterations, i.e,
         * after each stepsNumber iterations this function is called with the current
         * variable values. If stepsNumber have no set, this function will be called
         * after each iteration. For direct methods, this function will be called just
         * two times: the former with the initial solution, and the latter with
         * the final solution.
         * @param displayInformation - pointer to a function that must be called
         * with the current variables values. This function must return false to
         * interrupt the solve or true to continue.
         */
        void setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm ) );


        /**
         * Define a new upper bound to the number of iterations.
         * @param maxIter - new upper bound to the number of iterations.
         */
        void setMaxIterations( const unsigned int maxIter );


        /**
         * Set the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @param sn - steps number.
         */
        void setStepsNumber( unsigned int sn );
    };

} // namespace ModelManager
