#pragma once

#include "Enums.h"
#include "Constraint.h"
#include "QuadraticExpression.h"
#include "Variable.h"

namespace ModelManager
{
    class LinearExpression;
    class PreprocessingEqualityConstraints;
    class Model;
    
    std::ostream& operator<<( std::ostream &stream, const Model &m );

    class Model
    {
    public:
        /**
         * Overload the operator << to print the model.
         * @param stream - where the strem must be write.
         * @param m - the model to be printed.
         * @return - the final stream.
         */
        friend std::ostream& operator<<( std::ostream &stream, const Model &m );
    protected:
        /**
         * The set of model's constraints.
         */
        std::vector<Constraint> _constraints;

        /**
         * Simple constraints that are trivially solved, but as they are returned
         * to the user, it's necessary to store them to delete.
         */
        std::vector<Constraint> _simpleConstraints;

        /**
         * The set of model's soft constraints.
         */
        std::vector<Constraint> _softConstraints;

        /**
         * The set of model's variables.
         */
        std::vector<Variable> _variables;

        /**
         * Objective function to be minimized by the solver.
         */
        QuadraticExpression _objectiveFunction;

        /**
         * Object to preprocessing constraints.
         */
        PreprocessingEqualityConstraints* _processEqualityConstrs;

        /**
         * Flag to know if the model is already preprocessed or not.
         */
        bool _preprocessedModel;

        /**
         * Flag that determines if the solver must show messages or not.
         */
        bool _showLog;

        /**
         * Store if the model is feasible or not.
         */
        //        bool _feasible;

        /**
         * Used to specify how important is to honor the soft constraints. In 
         * practice: _softConstraintsWeight \< 1 means that is less important to
         * respect the soft constraints compared to objective function. 
         * _softConstraintsWeight == 1 means that both have the same importance 
         * and _softConstraintsWeight \> 1 means that is more important to honor
         * the soft constraints.
         */
        double _softConstraintsWeight;

        /**
         * The number of threads 
         */
        unsigned int _numberOfThreads;

        /**
         * Numerical toleration from error at float point operations
         */
        double _truncationEps;

        /**
         * Stop condition for current solver
         */
        double _solverEps;

    private:
        /**
         * Define all variable as unknown and simplify constraints.
         * @return - true if the model is feasible and false otherwise.
         */
        bool initialize( );

        /**
         * Process the equality constrains and fix some freedom degrees.
         * @return - return false if it is impossible to solve the constraints,
         * and true otherwise.
         */
        bool processEqualityConstraints( );

        /**
         * Process simple constraints.
         * @return - true if the model is feasible and false otherwise.
         */
        bool processSimpleConstraints( );

        /**
         * Process the soft constraints in the model adding the expression to
         * be minimized with the objective function.
         */
        void processSoftConstraints( );
    public:

        /**
         * Default constructor.
         */
        Model( );

        /**
         * Destructor.
         */
        virtual ~Model( );

        /**
         * Add a new constraint to current model. ATTENTION: if it is possible to
         * simplify the model by removing the constraint, it will be made and this
         * constraints will not be added to constraints vector. Don't worry, the
         * constraint will be honored.
         * @param exp - a linear expression that defines the left hand side of 
         * the constraint.
         * @param sense - EQUAL('='), LESS_EQUAL('\<'), GREATER_EQUAL('>').
         * @param rhs - the right hand side of the constraint.
         * @param wc - the constraint weight.
         * @param name - the constraint's name.
         * @return - the new constraint.
         */
        Constraint addConstraint( const LinearExpression &exp, char sense, double rhs, double wc=1, std::string name = "" ); 

        /**
         * Add a new range constraint to the current model. A range constraint is
         * in the form: lb < exp < up.
         * @param exp - a linear expression that defines constraint.
         * @param lb - the constraint's lower bound.
         * @param up - the constraint's upper bound.
         * @param name - the constraint's name.
         * @return - the new constraint.
         */
        Constraint addRange( const LinearExpression &exp, double lb, double up, std::string name = "" );

        /**
         * Add a new continuous decision variable to a model. 
         * @param name - the variable's name.
         * @param lb - the variable's lower bound.
         * @param up - the variable's upper bound.
         * @return - the new variable.
         */
        Variable addVariable( std::string name = "",double lb = -VAR_INFINITY, double up = VAR_INFINITY );

        /**
         * Add new continuous decision variables to a model. 
         * @param lb - the variables' lower bound.
         * @param up - the variables' upper bound.
         * @param name - the variables' name.
         * @param len - the number of variables.
         * @return - the new variables.
         */
        Variable* addVariables( double *lb, double *up, std::string *names, unsigned int len );

        /**
         * Add new continuous decision variables to a model using default
         * parameters, i.e, lower bound equal -INF and upper bound equal + INF. 
         * @param count- the number of variables.
         * @return - the new variables. 
         */
        Variable* addVariables( unsigned int count );

        /**
         * Get the coefficient of the variable v in the constraint c.
         * @param c - constraint index.
         * @param v - variable object.
         * @return - the coefficient of the variable v in the constraint c.
         */
        double getCoefficient( const Constraint c, const Variable v ) const;

        /**
         * Get the constraints vector.
         * @return - a pointer to constraints.
         */
        Constraint* getConstraints( ) const;

        /**
         * Get the constraint of index i.
         * @param i - constraint index.
         * @return - the constraint of index i.
         */
        Constraint getConstraint( unsigned int i ) const;

        /**
         * Get the number of constraints.
         * @return - the number of constraints.
         */
        unsigned int getNumberConstraints( ) const;

        /**
         * Get the number of soft constraints.
         * @return - the number of soft constraints.
         */
        unsigned int getNumberSoftConstraints( ) const;

        /**
         * Get the number of variables.
         * @return - the number of variables.
         */
        unsigned int getNumberVariables( ) const;

        /**
         * Get a reference to model's objective function. By setting the objective
         * function using this references, double representation are avoided.
         * @return - a reference to model's objective function.
         */
        QuadraticExpression& getObjectiveFunction( );

        /**
         * Get a reference to model's objective function. By setting the objective
         * function using this references, double representation are avoided.
         * @return - a cnst reference to model's objective function.
         */
        const QuadraticExpression& getObjectiveFunction( ) const;

        /**
         * Get the objective function value using the current variables' value.
         * @return - the objective function value.
         */
        double getObjectiveFunctionValue( ) const;

        /**
         * Get the soft constraint of index i.
         * @param i - soft constraint index.
         * @return - soft the constraint of index i.
         */
        Constraint getSofConstraint( unsigned int i ) const;

        /**
         * Get the soft constraints vector.
         * @return - a pointer to constraints.
         */
        Constraint* getSoftConstraints( ) const;

        /**
         * Get the SoftConstraintWeight value.
         * @return - the SoftConstraintWeight value.
         */
        double getSoftConstraintWeight( ) const;

        /**
         * Get the variable of index i.
         * @param i - variable index.
         * @return - the variable of index i.
         */
        Variable getVariable( unsigned int i ) const;

        /**
         * Get the variables vector.
         * @return - the variables vector.
         */
        Variable* getVariables( ) const;


        /**
         * Get TruncationEps
         * @return - double for Numerical toleration 
         * from error at float point operations
         */
        double getTruncationEps( ) const;

        /**
         * Get SolverEps
         * @return - double for Stop condition for current solver 
         */
        double getSolverEps( ) const;

        /**
         * Get number o threads
         * @return - unsigned int for the number of threads 
         */
        unsigned int getNumberOfThreads( ) const;

        /**
         * Set the variable _numberOfThreads
         * @param - numberOfThreads
         */
        void setNumberOfThreads( const unsigned int numberOfThreads );


        /**
         * Set the variable _truncationEps
         * @param - eps
         */
        void setTruncationEps( const double eps );


        /**
         * Set the variable _solverEps
         * @param - eps
         */
        void setSolverEps( const double eps );

        /**
         * Optimize the model.
         */
        virtual bool optimize( ) = 0;

        /**
         * Remove the constraint of index x.
         * @param c - index of constraint to be removed.
         */
        void removeConstraint( const Constraint c );

        /**
         * Remove the soft constraint of index x.
         * @param c - soft constraint index to be removed.
         */
        void removeSoftConstraint( const Constraint c );

        /**
         * Define a new value for phi. The phi value is used to specify how
         * important is to honor the soft constraints. In practice: scw \< 1 means
         * that is less important to respect the soft constraints compared to
         * objective function. scw == 1 means that both have the same importance
         * and scw \> 1 means that is more important to honor the soft constraints.
         * @param phi - the new scw value.
         */
        void setSoftConstraintsWeight( double scw );

        /**
         * Set if the solver must show the log or not.
         * @param log - true if the solve need to show the log and false otherwise.
         */
        void showLog( bool log );

        /**
         * Group the expressions and sort them. This function is automatically
         * called by the optimize function.
         * @return - false in case the problem is infeasible, and true otherwise.
         */
        bool update( );
        
        

        /**
         * Get the number of iterations used to solve the last model.
         * @return - number of iterations used to solve the last model.
         */
        virtual unsigned int getNumberIterations( ) const = 0;

        /**
         * Get the current maximum iterations.
         * @return - the current maximum iterations.
         */
        virtual unsigned int getMaxIterations( ) const = 0;

        /**
         * Get the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @return - steps number.
         */
        virtual unsigned int getStepsNumber( ) const = 0;


        /**
         * Set a callback function to be called after stepsNumber iterations, i.e,
         * after each stepsNumber iterations this function is called with the current
         * variable values. If stepsNumber have no set, this function will be called
         * after each iteration. For direct methods, this function will be called just
         * two times: the former with the initial solution, and the latter with
         * the final solution.
         * @param displayInformation - pointer to a function that must be called
         * with the current variables values. This function must return false to
         * interrupt the solve or true to continue.
         * @param object - object passed to be used in static function.
         */
        virtual void setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm, void *obj ), void *object ) = 0;

        /**
         * Define a new upper bound to the number of iterations.
         * @param maxIter - new upper bound to the number of iterations.
         */
        virtual void setMaxIterations( const unsigned int maxIter ) = 0;


        /**
         * Set the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @param sn - steps number.
         */
        virtual void setStepsNumber( unsigned int sn ) = 0;
    };

} // namespace ModelManager
