#pragma once

#include <string>
#include <vector>
#include "Model.h"

namespace Solver
{
    class OptimizationSolver;
}

namespace ModelManager
{

    class OptimizationModel : public Model
    {
    private:

        /**
         * Unknown variable indexes.
         */
        std::vector<unsigned int> _newVariableIndex;

        /**
         * Number of unknown variables.
         */
        unsigned int _numberUnknowVariables;

        /**
         * Vector to scale the objective function.
         */
        std::vector<double> _scale;

        /**
         * An optimization solver that will be used to solver the current model.
         */
        Solver::OptimizationSolver *_solver;
    private:

        /**
         * Compute the indexes of the new objective function to input at the
         * matrix.
         * @return - number of variables in the new objective function. 
         */
        unsigned int computeNewIndex( );

        /**
         * Scale the objective function.
         * @param n - number unknown variables.
         */
        void scaleObjectiveFunction( unsigned int n );

        /**
         * uncale the objective function.
         * @param n - number unknown variables.
         */
        void unscaleObjectiveFunction( unsigned int n );

    public:
        /**
         * Construct the optimization model with an optimization solver.
         * @param solver - an optimization solver.
         */
        OptimizationModel( Solver::OptimizationSolver *solver );

        /**
         * Get the current optimization solver.
         * @return - the current optimization solver.
         */
        const Solver::OptimizationSolver *getOptimizationSolver( ) const;

        /**
         * Optimize the model.
         */
        bool optimize( );

        /**
         * Define an objective function to be minimized.
         * @param obj - objective function to be minimized.
         */
        void setObjectiveFunction( const QuadraticExpression& obj );

        /**
         * Set a new optimization solver.
         * @param solver - a new optimization solver.
         */
        void setOptimizationSolver( Solver::OptimizationSolver *solver );
                
        /**
         * Get the number of iterations used to solve the last model.
         * @return - number of iterations used to solve the last model.
         */
        unsigned int getNumberIterations( ) const;

        /**
         * Get the current maximum iterations.
         * @return - the current maximum iterations.
         */
        unsigned int getMaxIterations( ) const;

        /**
         * Get the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @return - steps number.
         */
        unsigned int getStepsNumber( ) const;


        /**
         * Set a callback function to be called after stepsNumber iterations, i.e,
         * after each stepsNumber iterations this function is called with the current
         * variable values. If stepsNumber have no set, this function will be called
         * after each iteration. For direct methods, this function will be called just
         * two times: the former with the initial solution, and the latter with
         * the final solution.
         * @param displayInformation - pointer to a function that must be called
         * with the current variables values. This function must return false to
         * interrupt the solve or true to continue.
         * @param object - object passed to be used in static function.
         */
        void setDisplayInformationCallback( bool (*displayInformation )( const std::vector<double>& x, double gnorm, void *obj ), void *object );


        /**
         * Define a new upper bound to the number of iterations.
         * @param maxIter - new upper bound to the number of iterations.
         */
        void setMaxIterations( const unsigned int maxIter );


        /**
         * Set the number of iterations that must be performed before to display
         * the log information and call (if set) the display callback information.
         * @param sn - steps number.
         */
        void setStepsNumber( unsigned int sn );
    };

} // namespace ModelManager
