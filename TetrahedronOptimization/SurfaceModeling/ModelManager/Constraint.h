#pragma once

#include <iostream>

namespace ModelManager
{
    class ConstraintRepresentation;
    class LinearExpression;
    class Constraint;

    std::ostream& operator<<( std::ostream &stream, const Constraint &c );

    class Constraint
    {
    private:
        /**
         * Object to represent the allocated constraint.
         */
        ConstraintRepresentation* _constraint;

    private:
        /**
         * The constraint constructor that receives a linear expression, the sense
         * and the constraint name.
         * @param line - a linear expression with the constraint.
         * @param sense - the constraint sense.
         * @param wc - the constraint weight.
         * @param name - the constraint name.
         */
        Constraint( const LinearExpression &line, char sense, double wc = 1, std::string name = "" );

        /**
         * The constraint constructor that receives a linear expression, the sense
         * a constant, and the constraint name.
         * @param line - a linear expression with the constraint.
         * @param sense - the constraint sense.
         * @param constant - the constraint right hand side.
         * @param wc - the constraint weight.
         * @param name - the constraint name.
         */
        Constraint( const LinearExpression &line, char sense, double constant, double wc = 1, std::string name = "" );

        /**
         * The constraint constructor that receives a linear expression and set
         * a range constraint.
         * @param line - a linear expression with the constraint.
         * @param lb - lower bound.
         * @param up - upper bound.
         * @param wc - the constraint weight.
         * @param name - the constraint name.
         */
        Constraint( const LinearExpression &line, double lb, double up, double wc = 1, std::string name = "" );

        /**
         * Destroy the object.
         */
        void destroy( );

        /**
         * Define if the constraint value is known or not.
         * @return - true if the constraint is known and false otherwise.
         */
        void setIsKnown( const bool known );

    public:

        friend class Model;

        /**
         * Default constructor.
         */
        Constraint( );

        /**
         * Destructor.
         */
        ~Constraint( );

        /**
         * Get the linear expression that defines the constraint.
         * @return - the linear expression that defines the constraint.
         */
        LinearExpression& getLine( );

        /**
         * Get the linear expression that defines the constraint.
         * @return - the linear expression that defines the constraint.
         */
        
        const LinearExpression& getLine( ) const;

        /**
         * Get the constraint name.
         * @return - the constraint name.
         */
        const std::string getName( ) const;
        
        /**
         * Get the constraint weight.
         * @return - the constraint weight.
         */
        const double getWc() const;

        /**
         * Get the constraint sense.
         * @return - the constraint sense.
         */
        char getSense( ) const;

        /**
         * Compare to constraints.
         * @param c - constraint to be compared with the current constraint.
         * @return - true if both are equal and false otherwise.
         */
        bool operator==( const Constraint& c ) const;

        /**
         * Overload of the operator << to print the constraint.
         * @param stream - the stream that will be used to print the linear
         * expression.
         * @param c - the constraint the needs to be printed.
         * @return - the strem with the constraint printed.
         */
        friend std::ostream& operator<<( std::ostream &stream, const Constraint &c );

        /**
         * Set a new name to constraint.
         * @param name - new name.
         */
        void setName( const std::string &name );
        
        /**
         * Set a new weight to constraint.
         * @param weight constraint - new constraint weight.
         */
        void setWc( double wc);

        /**
         * Set a new constraint sense.
         * @param sense - new constraint sense.
         */
        void setSense( char sense );

        /**
         * Group the expression and sort it.
         */
        void update( );

        /**
         * Return if the constraint has a known value.
         * @return - true if the constraint value has a known value and false
         * otherwise.
         */
        bool getIsKnown( ) const;
    };

} // namespace ModelManager
