#include <algorithm>
#include <omp.h>
#include "../Timer/Timer.h"
#include "QuadraticExpression.h"
#include "LinearExpression.h"
#include "Variable.h"

namespace ModelManager
{



    QuadraticExpression::QuadraticExpression( double constant )
    {
        _linearExpression += constant;
        _isUpdated = false;
    }



    QuadraticExpression::QuadraticExpression( const Variable var, double coefficient )
    {
        _linearExpression += coefficient * var;
        _isUpdated = false;
    }



    QuadraticExpression::QuadraticExpression( const LinearExpression &le )
    {
        _linearExpression = le;
        _isUpdated = false;
    }



    QuadraticExpression::QuadraticExpression( const QuadraticExpression& q )
    {
        _quadraticTerms.reserve( q.size( ) );
        for ( unsigned int i = 0; i < q.size( ); i++ )
        {
            _quadraticTerms.push_back( new QuadraticTerm( q.getCoefficient( i ),
                                                          q.getVar1( i ),
                                                          q.getVar2( i ) ) );
        }
        _linearExpression = q._linearExpression;
        _isUpdated = q._isUpdated;
    }



    QuadraticExpression::~QuadraticExpression( )
    {
        for ( unsigned int i = 0; i < _quadraticTerms.size( ); i++ )
        {
            delete _quadraticTerms[i];
        }
    }



    void QuadraticExpression::add( const LinearExpression &le )
    {
        _linearExpression += le;
        _isUpdated = false;
    }



    void QuadraticExpression::addConstant( double c )
    {
        _linearExpression += c;
        _isUpdated = false;
    }



    void QuadraticExpression::addTerm( double coefficient, const Variable var )
    {
        _linearExpression += coefficient * var;
        _isUpdated = false;
    }



    void QuadraticExpression::addTerm( double coefficient, const Variable var1, const Variable var2 )
    {
        _quadraticTerms.push_back( new QuadraticTerm( coefficient, var1, var2 ) );
        _isUpdated = false;
    }



    void QuadraticExpression::clear( )
    {
        for ( unsigned int i = 0; i < _quadraticTerms.size( ); i++ )
        {
            delete _quadraticTerms[i];
        }

        _quadraticTerms.clear( );
        _linearExpression.clear( );

        _quadraticTerms.shrink_to_fit( );
        _isUpdated = false;
    }



    std::vector<LinearExpression> QuadraticExpression::computeGradient( unsigned int numberVariables ) const
    {
        std::vector<LinearExpression> g( numberVariables );

        for ( unsigned int i = 0; i < size( ); i++ )
        {
            Variable v1 = _quadraticTerms[i]->_var1;
            Variable v2 = _quadraticTerms[i]->_var2;
            double c = _quadraticTerms[i]->_coefficient;

            if ( v1 == v2 )
            {
                g[v1.getIndex( )] += 2 * c * v1;
            }
            else
            {
                g[v1.getIndex( )] += c * v2;
                g[v2.getIndex( )] += c * v1;
            }
        }
        for ( unsigned int i = 0; i < _linearExpression.size( ); i++ )
        {
            Variable v = _linearExpression.getVariable( i );
            g[v.getIndex( )] += _linearExpression.getCoefficient( i );
        }
        return g;
    }



    bool QuadraticExpression::exist( const Variable v ) const
    {
        if ( size( ) == 0 )return false;

        for ( int i = ( int ) size( ) - 1; i >= 0; i-- )
        {
            if ( v == _quadraticTerms[i]->_var1 || v == _quadraticTerms[i]->_var2 )
            {
                return true;
            }
        }
        return false;
    }



    double QuadraticExpression::getCoefficient( unsigned int i ) const
    {
        return _quadraticTerms[i]->_coefficient;
    }



    const LinearExpression& QuadraticExpression::getLinearExpression( ) const
    {
        return _linearExpression;
    }



    LinearExpression& QuadraticExpression::getLinearExpression( )
    {
        return _linearExpression;
    }



    double QuadraticExpression::getValue( ) const
    {
        double value = _linearExpression.getValue( );
        for ( unsigned int i = 0; i < size( ); i++ )
        {
            value += _quadraticTerms[i]->_coefficient * _quadraticTerms[i]->_var1.getValue( ) *
                _quadraticTerms[i]->_var2.getValue( );
        }
        return value;
    }



    Variable QuadraticExpression::getVar1( unsigned int i ) const
    {
        if ( i >= size( ) ) return Variable( );
        return _quadraticTerms[i]->_var1;
    }



    Variable QuadraticExpression::getVar2( unsigned int i ) const
    {
        if ( i >= size( ) ) return Variable( );
        return _quadraticTerms[i]->_var2;
    }



    bool QuadraticExpression::isUpdated( ) const
    {
        return _isUpdated;
    }



    QuadraticExpression QuadraticExpression::operator=( const QuadraticExpression &rhs )
    {
        _quadraticTerms.clear( );
        _quadraticTerms.reserve( rhs.size( ) );
        for ( unsigned int i = 0; i < rhs.size( ); i++ )
        {
            _quadraticTerms.push_back( new QuadraticTerm( rhs.getCoefficient( i ),
                                                          rhs.getVar1( i ),
                                                          rhs.getVar2( i ) ) );
        }
        _linearExpression = rhs._linearExpression;
        _isUpdated = rhs._isUpdated;
        return *this;
    }



    void QuadraticExpression::operator+=( const QuadraticExpression &exp )
    {
        _linearExpression += exp._linearExpression;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            _quadraticTerms.push_back( new QuadraticTerm( exp.getCoefficient( i ),
                                                          exp.getVar1( i ),
                                                          exp.getVar2( i ) ) );
        }
        _isUpdated = false;
    }



    void QuadraticExpression::operator-=( const QuadraticExpression &exp )
    {
        _linearExpression -= exp._linearExpression;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            _quadraticTerms.push_back( new QuadraticTerm( -exp.getCoefficient( i ),
                                                          exp.getVar1( i ),
                                                          exp.getVar2( i ) ) );
        }
        _isUpdated = false;
    }



    void QuadraticExpression::operator*=( double mult )
    {
        if ( mult == 0.0 )
        {
            clear( );
            return;
        }
        _linearExpression *= mult;
        for ( unsigned int i = 0; i < size( ); i++ )
        {
            _quadraticTerms[i]->_coefficient *= mult;
        }
    }



    void QuadraticExpression::operator/=( double a )
    {
        if ( a == 0.0 )
        {
            return;
        }
        _linearExpression /= a;
        for ( unsigned int i = 0; i < size( ); i++ )
        {
            _quadraticTerms[i]->_coefficient /= a;
        }
    }



    QuadraticExpression QuadraticExpression::operator+( const QuadraticExpression &rhs )
    {
        QuadraticExpression exp = *this;
        exp._linearExpression += rhs._linearExpression;
        for ( unsigned int i = 0; i < rhs.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticTerm( rhs.getCoefficient( i ),
                                                              rhs.getVar1( i ),
                                                              rhs.getVar2( i ) ) );
        }
        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression QuadraticExpression::operator-( const QuadraticExpression &rhs )
    {
        QuadraticExpression exp = *this;
        exp._linearExpression -= rhs._linearExpression;
        for ( unsigned int i = 0; i < rhs.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticTerm( -rhs.getCoefficient( i ),
                                                              rhs.getVar1( i ),
                                                              rhs.getVar2( i ) ) );
        }
        exp._isUpdated = false;
        return exp;
    }



    void QuadraticExpression::remove( unsigned int i )
    {
        if ( i >= size( ) )return;
        delete _quadraticTerms[i];
        _quadraticTerms.erase( _quadraticTerms.begin( ) + i );
    }



    bool QuadraticExpression::remove( const Variable v )
    {
        if ( size( ) == 0 )return false;

        bool found = false;
        for ( int i = ( int ) size( ) - 1; i >= 0; i-- )
        {
            if ( v == _quadraticTerms[i]->_var1 || v == _quadraticTerms[i]->_var2 )
            {
                delete _quadraticTerms[i];
                _quadraticTerms.erase( _quadraticTerms.begin( ) + i );
                found = true;
            }
        }
        return found || _linearExpression.remove( v );
    }



    void QuadraticExpression::reserve( unsigned int n )
    {
        _quadraticTerms.reserve( n );
    }



    void QuadraticExpression::setCoefficient( unsigned int i, double c )
    {
        if ( i < size( ) )
            _quadraticTerms[i]->_coefficient = c;
    }



    unsigned int QuadraticExpression::size( ) const
    {
        return _quadraticTerms.size( );
    }



    bool QuadraticExpression::compareTerms( const QuadraticTerm* t1, const QuadraticTerm* t2 )
    {
        if ( t1->_var1.getIndex( ) < t2->_var1.getIndex( ) )
        {
            return true;
        }
        else if ( t1->_var1.getIndex( ) == t2->_var1.getIndex( ) )
        {
            return t1->_var2.getIndex( ) < t2->_var2.getIndex( );
        }
        return false;
    }



    void QuadraticExpression::bucketSort( unsigned int minIndex, unsigned int maxIndex )
    {
        unsigned int k = maxIndex - minIndex + 1;
        std::vector<unsigned int> count( k + 1 ), availablePosition( k );
        unsigned int lenght = _quadraticTerms.size( );

        //Compute the number of elements by bucket.
        unsigned int M = ( maxIndex - minIndex + 1 ) / k + 1;

        for ( unsigned int i = 0; i < lenght; i++ )
        {
            unsigned int index = ( _quadraticTerms[i]->_var1.getIndex( ) - minIndex ) / M;
            count[index]++;
        }

        //Compute the available positions.
        count[k - 1] = lenght - count[k - 1];
        availablePosition[k - 1] = count[k - 1];
        for ( unsigned int i = k - 2; i >= 1; i-- )
        {
            count[i] = count[i + 1] - count[i];
            availablePosition[i] = count[i];
        }
        count[0] = 0;
        count[k] = lenght;

        //Get next available positions.
        const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );
#pragma omp parallel for num_threads(numThreads)
        for ( unsigned int g = 0; g < k; g++ )
        {
            //Increment the sentinel to next available position.
            while ( availablePosition[g] < count[g + 1] && ( _quadraticTerms[availablePosition[g]]->_var1.getIndex( ) - minIndex ) / M == g )
                availablePosition[g]++;
        }

        //Put each element on correct group.
        for ( unsigned int g = 0; g < k - 1; g++ )
        {
            for ( unsigned int i = availablePosition[g]; i < count[g + 1]; )
            {
                //Compute the number group.
                unsigned int group = ( _quadraticTerms[i]->_var1.getIndex( ) - minIndex ) / M;
                if ( group != g )
                {
                    std::swap( _quadraticTerms[i], _quadraticTerms[availablePosition[group]] );
                    while ( availablePosition[group] < count[group + 1] && ( _quadraticTerms[availablePosition[group]]->_var1.getIndex( ) - minIndex ) / M == group )
                        availablePosition[group]++;
                }
                else
                {
                    while ( availablePosition[g] < count[g + 1] && ( _quadraticTerms[availablePosition[g]]->_var1.getIndex( ) - minIndex ) / M == g )
                        availablePosition[g]++;
                    i++;
                }
            }
        }

        //Sort the groups.
#pragma omp parallel for num_threads(numThreads)
        for ( unsigned int i = 0; i < k; i++ )
        {
            std::sort( _quadraticTerms.begin( ) + count[i], _quadraticTerms.begin( ) + count[i + 1], compareTerms );
        }
    }



    void QuadraticExpression::update( bool log )
    {
        _linearExpression.update( );

        if ( !size( ) ) return;

        //Sort all individual quadratic terms in order to put the variable with small index
        //first.
        if ( log )
            printf( "        Sorting individual terms...\n" );

        unsigned int minIndex = -1, maxIndex = 0;

        for ( unsigned int t = 0; t < size( ); t++ )
        {
            if ( _quadraticTerms[t]->_var1.getIndex( ) > _quadraticTerms[t]->_var2.getIndex( ) )
            {
                std::swap( _quadraticTerms[t]->_var1, _quadraticTerms[t]->_var2 );
            }
            unsigned int id = _quadraticTerms[t]->_var1.getIndex( );
            minIndex = id < minIndex ? id : minIndex;
            maxIndex = id > maxIndex ? id : maxIndex;
        }

        if ( log )
            printf( "        Sorting quadratic expression..\n" );

        //Sort the elements.
        if ( _quadraticTerms.size( ) < 10000 )
        {
            Timer t;
            std::sort( _quadraticTerms.begin( ), _quadraticTerms.end( ), compareTerms );
            if ( log )
                t.printTime( "            Quick sort" );
        }
        else
        {
            Timer t;
            bucketSort( minIndex, maxIndex );
            if ( log )
                t.printTime( "            Bucket sort" );
        }

        if ( log )
            printf( "        Compressing quadratic expression...\n" );

        //Number of different terms.
        unsigned int term = 0;
        Variable v1 = _quadraticTerms[term]->_var1;
        Variable v2 = _quadraticTerms[term]->_var2;

        //Compress the expression.
        for ( unsigned int t = 1; t < size( ); t++ )
        {
            if ( v1 == _quadraticTerms[t]->_var1 && v2 == _quadraticTerms[t]->_var2 )
            {
                _quadraticTerms[term]->_coefficient += _quadraticTerms[t]->_coefficient;
            }
            else
            {
                double c = _quadraticTerms[term]->_coefficient;
                if ( c != 0.0 )
                {
                    term++;
                }
                std::swap( _quadraticTerms[term], _quadraticTerms[t] );
                v1 = _quadraticTerms[term]->_var1;
                v2 = _quadraticTerms[term]->_var2;
            }
        }

        //TODO: TEST THIS TYPES AND VERIFY THAT IT IS UNIQUE.
        if ( _quadraticTerms[term]->_coefficient == 0.0 )
            term--;

        if ( log )
            printf( "        Resize vector...\n" );

        const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );
#pragma omp parallel for num_threads(numThreads)
        for ( unsigned int i = term + 1; i < _quadraticTerms.size( ); i++ )
        {
            delete _quadraticTerms[i];
            _quadraticTerms[i] = 0;
        }
        _quadraticTerms.resize( term + 1 );
        _quadraticTerms.shrink_to_fit( );
        if ( log )
            printf( "        New quadratic expression has %lu quadratic terms and %u linear terms...\n",
                    _quadraticTerms.size( ),
                    _linearExpression.size( ) );

        _isUpdated = true;
    }



    std::ostream& operator<<( std::ostream &stream, const QuadraticExpression &exp )
    {
        stream << exp.getLinearExpression( ) << " + [";
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            double c = exp.getCoefficient( i );
            if ( c < 0.0 )
            {
                stream << " - " << -c * 2.0 << " " << exp.getVar1( i ).getName( ) << " * " << exp.getVar2( i ).getName( ) << " ";
            }
            else
            {
                stream << " + " << c * 2.0 << " " << exp.getVar1( i ).getName( ) << " * " << exp.getVar2( i ).getName( ) << " ";
            }
        }
        stream << "] / 2";

        return stream;
    }



    QuadraticExpression operator+( const QuadraticExpression &x, const QuadraticExpression &y )
    {
        QuadraticExpression exp = x;

        for ( unsigned int i = 0; i < y.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( y.getCoefficient( i ), y.getVar1( i ), y.getVar2( i ) ) );
        }
        exp._linearExpression += y.getLinearExpression( );

        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator-( const QuadraticExpression &x, const QuadraticExpression &y )
    {
        QuadraticExpression exp = x;

        for ( unsigned int i = 0; i < y.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( -y.getCoefficient( i ), y.getVar1( i ), y.getVar2( i ) ) );
        }
        exp._linearExpression -= y.getLinearExpression( );

        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator+( const QuadraticExpression &x )
    {
        return x;
    }



    QuadraticExpression operator-( const QuadraticExpression &x )
    {
        QuadraticExpression exp = x;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            exp._quadraticTerms[i]->_coefficient *= -1;
        }
        exp._linearExpression *= -1;

        return exp;
    }



    QuadraticExpression operator*( const QuadraticExpression &x, double a )
    {
        if ( a == 0.0 )
        {
            return QuadraticExpression( );
        }

        QuadraticExpression exp = x;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            exp._quadraticTerms[i]->_coefficient *= a;
        }
        exp._linearExpression *= a;

        return exp;
    }



    QuadraticExpression operator*( double a, const QuadraticExpression &x )
    {
        if ( a == 0.0 )
        {
            return QuadraticExpression( );
        }

        QuadraticExpression exp = x;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            exp._quadraticTerms[i]->_coefficient *= a;
        }
        exp._linearExpression *= a;
        return exp;
    }



    QuadraticExpression operator*( const Variable x, const Variable y )
    {
        QuadraticExpression exp;
        exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( 1.0, x, y ) );
        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator*( const Variable x, const LinearExpression &y )
    {
        QuadraticExpression exp;
        for ( unsigned int i = 0; i < y.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( y.getCoefficient( i ), x, y.getVariable( i ) ) );
        }
        if ( y.getConstant( ) != 0.0 )
        {
            exp._linearExpression += y.getConstant( ) * x;
        }
        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator*( const LinearExpression &y, const Variable x )
    {
        QuadraticExpression exp;
        for ( unsigned int i = 0; i < y.size( ); i++ )
        {
            exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( y.getCoefficient( i ), y.getVariable( i ), x ) );
        }
        if ( y.getConstant( ) != 0.0 )
        {
            exp._linearExpression += y.getConstant( ) * x;
        }
        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator*( const LinearExpression &x, const LinearExpression &y )
    {
        QuadraticExpression exp;
        if ( &x == &y )
        {
            unsigned int size = x.size( );
            for ( unsigned int i = 0; i < size; i++ )
            {
                double c1 = x.getCoefficient( i );
                Variable v1 = x.getVariable( i );
                exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( c1 * c1, v1, v1 ) );
                for ( unsigned int j = i + 1; j < size; j++ )
                {
                    double c2 = x.getCoefficient( j );
                    Variable v2 = x.getVariable( j );
                    exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( 2 * c1 * c2, v1, v2 ) );
                }
            }
        }
        else
        {
            for ( unsigned int i = 0; i < x.size( ); i++ )
            {
                double c1 = x.getCoefficient( i );
                for ( unsigned int j = 0; j < y.size( ); j++ )
                {
                    double c2 = y.getCoefficient( j );
                    if ( c1 != 0.0 && c2 != 0.0 )
                    {
                        exp._quadraticTerms.push_back( new QuadraticExpression::QuadraticTerm( c1 * c2, x.getVariable( i ), y.getVariable( j ) ) );
                    }
                }
            }
        }
        if ( y.getConstant( ) != 0.0 )
        {
            for ( unsigned int i = 0; i < x.size( ); i++ )
            {
                exp._linearExpression += y.getConstant( ) * x.getVariable( i ) * x.getCoefficient( i );
            }
        }

        if ( x.getConstant( ) != 0.0 )
        {
            for ( unsigned int i = 0; i < y.size( ); i++ )
            {
                exp._linearExpression += x.getConstant( ) * y.getVariable( i ) * y.getCoefficient( i );
            }
        }
        exp._linearExpression += x.getConstant( ) * y.getConstant( );

        exp._isUpdated = false;
        return exp;
    }



    QuadraticExpression operator/( const QuadraticExpression &x, double a )
    {
        if ( a == 0.0 )
        {
            return QuadraticExpression( );
        }

        QuadraticExpression exp = x;
        for ( unsigned int i = 0; i < exp.size( ); i++ )
        {
            exp._quadraticTerms[i]->_coefficient /= a;
        }
        exp._linearExpression /= a;
        return exp;
    }
} // namespace ModelManager
