#include "ConstraintRepresentation.h"
#include "Enums.h"
#include <cmath>
#define CONSTR_INFINITY 1e100

namespace ModelManager
{



    ConstraintRepresentation::ConstraintRepresentation( const LinearExpression &line, char sense, double wc, std::string name ) 
    {
        _line = line;
        _sense = sense;
        _name = name;
        _wc = wc;
        _lowerBound = -CONSTR_INFINITY;
        _upperBound = CONSTR_INFINITY;
        if (sense == GREATER_EQUAL) _line = -line;

        unsigned int size = line.size( );
        if (!size) return;

        double lb = 0.0, ub = 0.0;
        for (unsigned int i = 0; i < size; i++)
        {
            lb += line.getCoefficient( i ) * line.getVariable( i ).getLowerBound( );
            ub += line.getCoefficient( i ) * line.getVariable( i ).getUpperBound( );
        }
        _lowerBound = std::max( _lowerBound, lb + line.getConstant( ) );
        _upperBound = std::min( _upperBound, ub + line.getConstant( ) );
        if(sense == SOFT)
        {
            _line.update();
            double norm = 0;
            for(unsigned int i = 0; i < size; i++)
            {
                norm += _line.getCoefficient(i) * _line.getCoefficient(i);
            }
            norm = sqrt(norm);
            if(norm != 0)
            {
                _line /=norm; 
            }
            
            _line *= wc;
        }
    }



    ConstraintRepresentation::ConstraintRepresentation( const LinearExpression &line, char sense, double constant,double wc, std::string name )
    {
        _line = line;
        _sense = sense;
        _line -= constant;
        if (sense == GREATER_EQUAL)
        {
            _line = -_line;
            _sense = LESS_EQUAL;
        }

        _name = name;
        _wc = wc;
        _lowerBound = -CONSTR_INFINITY;
        _upperBound = CONSTR_INFINITY;

        unsigned int size = line.size( );
        if (!size) return;

        double lb = 0.0, ub = 0.0;
        for (unsigned int i = 0; i < size; i++)
        {
            lb += line.getCoefficient( i ) * line.getVariable( i ).getLowerBound( );
            ub += line.getCoefficient( i ) * line.getVariable( i ).getUpperBound( );
        }
        _lowerBound = std::max( _lowerBound, lb + line.getConstant( ) );
        _upperBound = std::min( _upperBound, ub + line.getConstant( ) );
        
        if(sense == SOFT)
        {
            _line.update();
            double norm = 0;
            for(unsigned int i = 0; i < size; i++)
            {
                norm += _line.getCoefficient(i) * _line.getCoefficient(i);
            }
            norm = sqrt(norm);
            if(norm != 0)
            {
                _line /=norm; 
            }
            
            _line *= wc;
        }
    }



    ConstraintRepresentation::ConstraintRepresentation( const LinearExpression &line, double lb, double up, std::string name )
    {
        _line = line;
        _lowerBound = lb;
        _upperBound = up;
        _name = name;
    }



    LinearExpression& ConstraintRepresentation::getLine( )
    {
        return _line;
    }



    const LinearExpression& ConstraintRepresentation::getLine( ) const
    {
        return _line;
    }



    const std::string& ConstraintRepresentation::getName( ) const
    {
        return _name;
    }
    
    const double ConstraintRepresentation::getWc( ) const
    {
        return _wc;
    }



    char ConstraintRepresentation::getSense( ) const
    {
        return _sense;
    }



    bool ConstraintRepresentation::getIsKnown( ) const
    {
        return _isKnown;
    }



    void ConstraintRepresentation::setIsKnown( const bool known )
    {
        _isKnown = known;
    }



    void ConstraintRepresentation::setName( const std::string &name )
    {
        _name = name;
    }
    
    
    void ConstraintRepresentation::setWc( const double wc )
    {
        _wc = wc;
    }



    void ConstraintRepresentation::setSense( char sense )
    {
        _sense = sense;
    }



    void ConstraintRepresentation::update( )
    {
        _line.update( );
    }
} // namespace ModelManager
