#include "io.h"


void writeXYZ( const std::string& path, const std::vector< glm::vec3 >& points )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.x << " " << p.y << " " << p.z << "\n";
        }

        output.close( );
    }
}

void writeXYZ(const std::string &path, const std::vector<glm::vec2> &points)
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.x << " " << p.y << " 0" << "\n";
        }

        output.close( );
    }
}

void writeXYZ( const std::string& path, const std::vector< SamplePoint >& points )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.coords.x << " " << p.coords.y << " " << p.coords.z << "\n";
        }

        output.close( );
    }
}

void writeXYZ( const std::string& path, const std::vector< SamplePoint2D >& points )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.coords.x << " " << p.coords.y << " 0" << "\n";
        }

        output.close( );
    }
}

void writeOFF(const std::string& path, const RegularGrid& grid)
{
    std::ofstream out( path );
    if (!out)
    {
        printf( "Error writing the file %s\n", path.c_str( ) );
        return;
    }

    out << "OFF\n";

    auto dx = grid.nx;
    auto dy = grid.ny;
    auto dz = grid.nz;

    auto numVertices = dx*dy*dz;
    auto numElements = (dx-1)*(dy-1)*dz +
            (dz-1)*(dy-1)*dx + (dx-1)*(dz-1)*dy;

    out << numVertices << " " << numElements << " 0\n";

    glm::vec3 first;
    first.x = -(dx-1.0f)*grid.sx / 2.0f;
    first.y = -(dy-1.0f)*grid.sy / 2.0f;
    first.z = -(dz-1.0f)*grid.sz / 2.0f;

    for(unsigned int k = 0; k < grid.nz; ++k)
    {
        auto z = first.z + k*grid.sz;

        for(unsigned int j = 0; j < grid.ny; ++j)
        {
            auto y = first.y + j*grid.sy;

            for(unsigned int i = 0; i < grid.nx; ++i)
            {
                auto x = first.x + i*grid.sx;

                out << x << " " << y << " " << z << "\n";
            }
        }
    }

    auto index = [&](unsigned int x, unsigned int y, unsigned int z)
    {
        return z*grid.ny*grid.nx +
               y*grid.nx + x;
    };

    for(unsigned int k = 0; k < grid.nz; ++k)
    {
        for(unsigned int j = 0; j < grid.ny-1; ++j)
        {
            for(unsigned int i = 0; i < grid.nx-1; ++i)
            {
                out << "4 ";
                out << index(i,j,k)     << " "
                    << index(i+1,j,k)   << " "
                    << index(i+1,j+1,k) << " "
                    << index(i,j+1,k)   <<"\n";
            }
        }
    }

    for(unsigned int i = 0; i < grid.nx; ++i)
    {
        for(unsigned int j = 0; j < grid.ny-1; ++j)
        {
            for(unsigned int k = 0; k < grid.nz-1; ++k)
            {
                out << "4 ";
                out << index(i,j,k)     << " "
                    << index(i,j,k+1)   << " "
                    << index(i,j+1,k+1) << " "
                    << index(i,j+1,k)   <<"\n";
            }
        }
    }

    for(unsigned int j = 0; j < grid.ny; ++j)
    {
        for(unsigned int k = 0; k < grid.nz-1; ++k)
        {
            for(unsigned int i = 0; i < grid.nx-1; ++i)
            {
                out << "4 ";
                out << index(i,j,k)     << " "
                    << index(i+1,j,k)   << " "
                    << index(i+1,j,k+1) << " "
                    << index(i,j,k+1)   <<"\n";
            }
        }
    }

    out.close( );
}

void writeOFF( const std::string& path, const std::vector<glm::vec3>& vertices,
                          const std::vector<unsigned int>& indices)
{
    std::ofstream out( path );
    if (!out)
    {
        printf( "Error writing the file %s\n", path.c_str( ) );
        return;
    }

    out << "OFF\n";

    auto numElements = indices.size( ) / 3;
    out << vertices.size( ) << " " << numElements << " 0\n";

    for (const auto& p : vertices)
    {
        out << p.x << " " << p.y << " " << p.z << "\n";
    }

    for (unsigned int i = 0, pos = 0; i < numElements; i++, pos += 3)
    {
        out << "3 ";
        out << indices[pos + 0] << " "
                                << indices[pos + 1] << " "
                                << indices[pos + 2] << "\n";
    }

    out.close( );
}

void writeNeutralFile(const std::string& path, const RegularGrid& grid, const std::vector<glm::vec3> &vertices, const std::vector<unsigned int> &indices)
{
    printf( "Writing file %s...\n", path.c_str( ) );

    auto dx = grid.nx;
    auto dy = grid.ny;
    auto dz = grid.nz;

    //Get the number of nodes on output.
    unsigned int numGridVertices = dx*dy*dz;
    unsigned int numMeshVertices = vertices.size();

    //Get the number of elements on output.
    unsigned int numGridElements = (dx - 1) * (dy - 1) * (dz - 1);
    unsigned int numMeshElements = indices.size()/3;

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return;
    }

    out << "%HEADER\n"
        "Neutral file created by Suellen\n\n"

        "%HEADER.VERSION\n"
        "'Apr/18'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Implicit function'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numGridVertices+numMeshVertices << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numGridVertices+numMeshVertices << std::endl << std::endl;

    int idx = 1;

    glm::vec3 first;
    first.x = -(dx-1.0f)*grid.sx / 2.0f;
    first.y = -(dy-1.0f)*grid.sy / 2.0f;
    first.z = -(dz-1.0f)*grid.sz / 2.0f;

    for(unsigned int k = 0; k < dz; ++k)
    {
        auto z = first.z + k*grid.sz;

        for(unsigned int j = 0; j < dy; ++j)
        {
            auto y = first.y + j*grid.sy;

            for(unsigned int i = 0; i < dx; ++i)
            {
                auto x = first.x + i*grid.sx;
                out << idx << " " << x << " " << y << " " << z << "\n";

                idx++;
            }
        }
    }

    for(const auto p : vertices)
    {
        out << idx++ << " " << p.x << " " << p.y << " " << p.z << "\n";
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << numGridElements+numMeshElements << std::endl << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << numGridElements << std::endl << std::endl;

    auto index = [&](unsigned int x, unsigned int y, unsigned int z)
    {
        return z*grid.ny*grid.nx +
               y*grid.nx + x;
    };

    idx = 1;
    int inc = 1;

    for (int k = 0; k < dz - 1; k++)
    {
        for (int j = 0; j < dy - 1; j++)
        {
            for (int i = 0; i < dx - 1; i++)
            {
                out << idx << " 0 1 ";

                out << index( i + 1, j + 0, k + 0 ) + inc << " ";
                out << index( i + 1, j + 1, k + 0 ) + inc << " ";
                out << index( i + 1, j + 1, k + 1 ) + inc << " ";
                out << index( i + 1, j + 0, k + 1 ) + inc << " ";

                out << index( i + 0, j + 0, k + 0 ) + inc << " ";
                out << index( i + 0, j + 1, k + 0 ) + inc << " ";
                out << index( i + 0, j + 1, k + 1 ) + inc << " ";
                out << index( i + 0, j + 0, k + 1 ) + inc << " ";

                out << std::endl;

                idx++;
            }
        }
    }

    out << "%ELEMENT.T3" << std::endl;
    out << numMeshElements << std::endl << std::endl;


    int pos = 0;
    inc += numGridVertices;
    for (unsigned int i = 0; i < numMeshElements; i++)
    {

        out << idx << " 0 0 0 ";

        out << indices[pos + 0] + inc << " "
            << indices[pos + 1] + inc << " "
            << indices[pos + 2] + inc << "\n";

        pos+=3;
        idx++;
    }


    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


       out << "%RESULT" << std::endl;
       out << 1 << std::endl;
       out << 1 << " " << "'Mesh'" << std::endl << std::endl;

       out << "%RESULT.CASE" << std::endl;
       out << 1 << " " << 1 << std::endl;
       out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

       out << "%RESULT.CASE.STEP" << std::endl;
       out << 1 << std::endl << std::endl;

       out << "%RESULT.CASE.STEP.TIME" << std::endl;
        out << "0.000000" << std::endl << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
        out << 1 << std::endl;
        out << "'FUNCTION'" << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
        out << numGridElements+numMeshElements << std::endl;

    idx = 1;

    for (int k = 0; k < dz - 1; k++)
    {
        for (int j = 0; j < dy - 1; j++)
        {
            for (int i = 0; i < dx - 1; i++)
            {
                out << idx << std::endl;

                out << grid.functionValue[index( i + 1, j + 0, k + 0 )] << " ";
                out << grid.functionValue[index( i + 1, j + 1, k + 0 )] << " ";
                out << grid.functionValue[index( i + 1, j + 1, k + 1 )] << " ";
                out << grid.functionValue[index( i + 1, j + 0, k + 1 )] << " ";

                out << grid.functionValue[index( i + 0, j + 0, k + 0 )] << " ";
                out << grid.functionValue[index( i + 0, j + 1, k + 0 )] << " ";
                out << grid.functionValue[index( i + 0, j + 1, k + 1 )] << " ";
                out << grid.functionValue[index( i + 0, j + 0, k + 1 )] << " ";

                out << std::endl;

                idx++;
            }
        }
    }

    for (unsigned int i = 0; i < numMeshElements; i++)
    {
        out << idx << std::endl;

        out << 20 << " " << 20 << " " << 20 << std::endl;

        idx++;
    }


    out << "%END" << std::endl;
}

void writeNeutralFile(const std::string& path, const RegularGrid& grid, const std::vector<glm::vec2> &vertices, const std::vector<unsigned int> &indices)
{
    printf( "Writing file %s...\n", path.c_str( ) );

    auto dx = grid.nx;
    auto dy = grid.ny;

    //Get the number of nodes on output.
    unsigned int numGridVertices = dx*dy;
    unsigned int numMeshVertices = vertices.size();

    //Get the number of elements on output.
    unsigned int numGridElements = (dx - 1) * (dy - 1);
    unsigned int numMeshElements = indices.size()/3;

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return;
    }

    out << "%HEADER\n"
        "Neutral file created by Suellen\n\n"

        "%HEADER.VERSION\n"
        "'Apr/18'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Implicit function'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numGridVertices+numMeshVertices << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numGridVertices+numMeshVertices << std::endl << std::endl;

    int idx = 1;

    glm::vec3 first;
    first.x = -(dx-1.0f)*grid.sx / 2.0f;
    first.y = -(dy-1.0f)*grid.sy / 2.0f;

    for(unsigned int j = 0; j < dy; ++j)
    {
        auto y = first.y + j*grid.sy;

        for(unsigned int i = 0; i < dx; ++i)
        {
            auto x = first.x + i*grid.sx;
            out << idx << " " << x << " " << y << " 0" << "\n";

            idx++;
        }
    }

    for(const auto p : vertices)
    {
        out << idx++ << " " << p.x << " " << p.y << " 0" << "\n";
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << numGridElements+numMeshElements << std::endl << std::endl;

    out << "%ELEMENT.Q4" << std::endl;
    out << numGridElements << std::endl << std::endl;

    auto index = [&](unsigned int x, unsigned int y)
    {
        return y*grid.nx + x;
    };

    idx = 1;
    int inc = 1;

    for (int j = 0; j < dy - 1; j++)
    {
        for (int i = 0; i < dx - 1; i++)
        {
            out << idx << " 0 0 1 ";

            out << index( i + 0, j + 0 ) + inc << " ";
            out << index( i + 1, j + 0 ) + inc << " ";
            out << index( i + 1, j + 1 ) + inc << " ";
            out << index( i + 0, j + 1 ) + inc << " ";

            out << std::endl;

            idx++;
        }
    }

    if(numMeshElements>0)
    {
        out << "%ELEMENT.T3" << std::endl;
        out << numMeshElements << std::endl << std::endl;


        int pos = 0;
        inc += numGridVertices;
        for (unsigned int i = 0; i < numMeshElements; i++)
        {

            out << idx << " 0 0 0 ";

            out << indices[pos + 0] + inc << " "
                << indices[pos + 1] + inc << " "
                << indices[pos + 2] + inc << "\n";

            pos+=3;
            idx++;
        }
    }


    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


       out << "%RESULT" << std::endl;
       out << 1 << std::endl;
       out << 1 << " " << "'Mesh'" << std::endl << std::endl;

       out << "%RESULT.CASE" << std::endl;
       out << 1 << " " << 1 << std::endl;
       out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

       out << "%RESULT.CASE.STEP" << std::endl;
       out << 1 << std::endl << std::endl;

       out << "%RESULT.CASE.STEP.TIME" << std::endl;
        out << "0.000000" << std::endl << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
        out << 1 << std::endl;
        out << "'FUNCTION'" << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
        out << numGridElements+numMeshElements << std::endl;

    idx = 1;

    for (int j = 0; j < dy - 1; j++)
    {
        for (int i = 0; i < dx - 1; i++)
        {
            out << idx << std::endl;

            out << grid.functionValue[index( i + 0, j + 0 )] << " ";
            out << grid.functionValue[index( i + 1, j + 0 )] << " ";
            out << grid.functionValue[index( i + 1, j + 1 )] << " ";
            out << grid.functionValue[index( i + 0, j + 1 )] << " ";

            out << std::endl;

            idx++;
        }
    }

    for (unsigned int i = 0; i < numMeshElements; i++)
    {
        out << idx << std::endl;

        out << 20 << " " << 20 << std::endl;

        idx++;
    }


    out << "%END" << std::endl;
}


void writeNeutralFile(const std::string &path, const Octree<float> &oct, const std::vector<glm::vec3> &vertices, const std::vector<unsigned int> &indices)
{
    printf( "Writing file %s...\n", path.c_str( ) );

    const auto& octVertices = oct.getVertices();

    //Get the number of nodes on output.
    unsigned int numOctreeVertices = octVertices.size();
    unsigned int numMeshVertices = vertices.size();

    //Get the number of elements on output.
    unsigned int numOctreeElements = oct.getNumLeafNodes();
    unsigned int numMeshElements = indices.size()/3;

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return;
    }

    out << "%HEADER\n"
        "Neutral file created by Suellen\n\n"

        "%HEADER.VERSION\n"
        "'Apr/18'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Implicit function'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numOctreeVertices+numMeshVertices << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numOctreeVertices+numMeshVertices << std::endl << std::endl;

    int idx = 1;
    for(const auto v : octVertices)
    {
        out << idx++ << " " << v.x << " " << v.y << " " << v.z << "\n";
    }

    for(const auto v : vertices)
    {
        out << idx++ << " " << v.x << " " << v.y << " " << v.z << "\n";
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << numOctreeElements+numMeshElements << std::endl << std::endl;

    out << "%ELEMENT.BRICK8" << std::endl;
    out << numOctreeElements << std::endl << std::endl;

    idx = 1;
    int inc = 1;

    for(const auto& node : oct)
    {
        out << idx << " 0 1 ";

        out << node.indices[1] + inc << " ";
        out << node.indices[3] + inc << " ";
        out << node.indices[7] + inc << " ";
        out << node.indices[5] + inc << " ";

        out << node.indices[0] + inc << " ";
        out << node.indices[2] + inc << " ";
        out << node.indices[6] + inc << " ";
        out << node.indices[4] + inc << " ";

        out << std::endl;

        idx++;
    }


    if(numMeshElements>0)
    {
        out << "%ELEMENT.T3" << std::endl;
        out << numMeshElements << std::endl << std::endl;


        int pos = 0;
        inc += numOctreeVertices;
        for (unsigned int i = 0; i < numMeshElements; i++)
        {

            out << idx << " 0 0 0 ";

            out << indices[pos + 0] + inc << " "
                << indices[pos + 1] + inc << " "
                << indices[pos + 2] + inc << "\n";

            pos+=3;
            idx++;
        }
    }


    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


       out << "%RESULT" << std::endl;
       out << 1 << std::endl;
       out << 1 << " " << "'Mesh'" << std::endl << std::endl;

       out << "%RESULT.CASE" << std::endl;
       out << 1 << " " << 1 << std::endl;
       out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

       out << "%RESULT.CASE.STEP" << std::endl;
       out << 1 << std::endl << std::endl;

       out << "%RESULT.CASE.STEP.TIME" << std::endl;
        out << "0.000000" << std::endl << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
        out << 1 << std::endl;
        out << "'FUNCTION'" << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
        out << numOctreeElements+numMeshElements << std::endl;

    idx = 1;

    const auto& data = oct.getData();

    for(const auto& node : oct)
    {
        out << idx << std::endl;

        out << data[node.indices[1]] << " ";
        out << data[node.indices[3]] << " ";
        out << data[node.indices[7]] << " ";
        out << data[node.indices[5]] << " ";

        out << data[node.indices[0]] << " ";
        out << data[node.indices[2]] << " ";
        out << data[node.indices[6]] << " ";
        out << data[node.indices[4]] << " ";

        out << std::endl;

        idx++;
    }

    for (unsigned int i = 0; i < numMeshElements; i++)
    {
        out << idx << std::endl;

        out << 20 << " " << 20 << " " << 20 << std::endl;

        idx++;
    }


    out << "%END" << std::endl;
}


void writeNeutralFile(const std::string &path, const Quadtree<float> &qdt, const std::vector<glm::vec2> &vertices, const std::vector<unsigned int> &indices)
{
    printf( "Writing file %s...\n", path.c_str( ) );

    const auto& octVertices = qdt.getVertices();

    //Get the number of nodes on output.
    unsigned int numOctreeVertices = octVertices.size();
    unsigned int numMeshVertices = vertices.size();

    //Get the number of elements on output.
    unsigned int numQuadElements = qdt.getNumLeafNodes();
    unsigned int numMeshElements = indices.size()/3;

    std::ofstream out( path.c_str( ) );
    if (!out)
    {
        std::cout << "Falha ao abrir o arquivo " << path << std::endl;
        return;
    }

    out << "%HEADER\n"
        "Neutral file created by Suellen\n\n"

        "%HEADER.VERSION\n"
        "'Apr/18'\n\n"

        "%HEADER.VERSION\n"
        "1.00\n\n"

        "%HEADER.TITLE\n"
        "'Implicit function'\n" << std::endl;

    out << "%NODE" << std::endl;
    out << numOctreeVertices+numMeshVertices << std::endl << std::endl;

    out << "%NODE.COORD\n";
    out << numOctreeVertices+numMeshVertices << std::endl << std::endl;

    int idx = 1;
    for(const auto v : octVertices)
    {
        out << idx++ << " " << v.x << " " << v.y << " 0" << "\n";
    }

    for(const auto v : vertices)
    {
        out << idx++ << " " << v.x << " " << v.y << " 0" << "\n";
    }

    out << std::endl;

    out << "%ELEMENT" << std::endl;
    out << numQuadElements+numMeshElements << std::endl << std::endl;

    out << "%ELEMENT.Q4" << std::endl;
    out << numQuadElements << std::endl << std::endl;

    idx = 1;
    int inc = 1;

    for(const auto& node : qdt)
    {
        out << idx << " 0 0 1 ";

        out << node.indices[0] + inc << " ";
        out << node.indices[1] + inc << " ";
        out << node.indices[3] + inc << " ";
        out << node.indices[2] + inc << " ";

        out << std::endl;

        idx++;
    }


    if(numMeshElements>0)
    {
        out << "%ELEMENT.T3" << std::endl;
        out << numMeshElements << std::endl << std::endl;


        int pos = 0;
        inc += numOctreeVertices;
        for (unsigned int i = 0; i < numMeshElements; i++)
        {

            out << idx << " 0 0 0 ";

            out << indices[pos + 0] + inc << " "
                << indices[pos + 1] + inc << " "
                << indices[pos + 2] + inc << "\n";

            pos+=3;
            idx++;
        }
    }


    out << "%QUADRATURE\n"
        "1\n"
        "%QUADRATURE.GAUSS.CUBE\n"
        "1\n"
        "1  1  1  1  0\n" << std::endl;


       out << "%RESULT" << std::endl;
       out << 1 << std::endl;
       out << 1 << " " << "'Mesh'" << std::endl << std::endl;

       out << "%RESULT.CASE" << std::endl;
       out << 1 << " " << 1 << std::endl;
       out << 1 << " " << "'Mesh1'" << std::endl << std::endl;

       out << "%RESULT.CASE.STEP" << std::endl;
       out << 1 << std::endl << std::endl;

       out << "%RESULT.CASE.STEP.TIME" << std::endl;
        out << "0.000000" << std::endl << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR" << std::endl;
        out << 1 << std::endl;
        out << "'FUNCTION'" << std::endl;
        out << "%RESULT.CASE.STEP.ELEMENT.NODAL.SCALAR.DATA" << std::endl;
        out << numQuadElements+numMeshElements << std::endl;

    idx = 1;

    const auto& data = qdt.getData();

    for(const auto& node : qdt)
    {
        out << idx << std::endl;

        out << data[node.indices[0]] << " ";
        out << data[node.indices[1]] << " ";
        out << data[node.indices[3]] << " ";
        out << data[node.indices[2]] << " ";

        out << std::endl;

        idx++;
    }

    for (unsigned int i = 0; i < numMeshElements; i++)
    {
        out << idx << std::endl;

        out << 20 << " " << 20 << std::endl;

        idx++;
    }


    out << "%END" << std::endl;
}

void writeDCFNode(std::ofstream& out, Octree<float>::Node* node,
                  const glm::vec3& corner, float size)
{
    int type;

    //type = 0: internal node
    //type = 1: empty node
    //type = 2: leaf node

    if(node->sons.empty())
    {
        if(node->numPoints>0)
        {

        }
        else //empty node
        {
            type = 1;

            short value = 1; //0 for inside, 1 for outside

            out << value;
        }
    }
    else
    {
        type = 0;

        out << type;

        for(auto son : node->sons)
        {
            writeDCFNode(out,son);
        }

    }
}

void writeDCFFile(const std::string &path, const Octree<float> &oct)
{
    std::ofstream out( path.c_str( ), std::ios::binary );
    if (!out)
    {
        std::cout << "Falha ao escrever o arquivo " << path << std::endl;
        return;
    }

    out << "multisign";

    int w,h,d;
    out << w << h << d;
}

void readXYZ(const std::__cxx11::string &path, std::vector<glm::vec3> &points)
{
    std::ifstream input;
    input.open( path );

    if (input.is_open( ))
    {
        unsigned int numPoints;
        input >> numPoints;

        points.reserve(numPoints);

        for(unsigned int i = 0; i < numPoints; ++i)
        {
            glm::vec3 p;
//            int idx;
            input >> /*idx >> */p.x >> p.y >> p.z;

//            p.z*=4;

            points.push_back(p);
        }

        input.close( );
    }
}

