QT += core
QT -= gui

CONFIG += c++11

TARGET = TetrahedronOptimization
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    ./SurfaceModeling/Graph/DSIGraph.cpp \
    ./SurfaceModeling/ModelManager/Constraint.cpp \
    ./SurfaceModeling/ModelManager/ConstraintRepresentation.cpp \
    ./SurfaceModeling/ModelManager/DSIModel.cpp \
    ./SurfaceModeling/ModelManager/LinearExpression.cpp \
    ./SurfaceModeling/ModelManager/Model.cpp \
    ./SurfaceModeling/ModelManager/OptimizationModel.cpp \
    ./SurfaceModeling/ModelManager/PreprocessingEqualityConstraints.cpp \
    ./SurfaceModeling/ModelManager/PreprocessingSimpleConstraints.cpp \
    ./SurfaceModeling/ModelManager/QuadraticExpression.cpp \
    ./SurfaceModeling/ModelManager/Variable.cpp \
    ./SurfaceModeling/ModelManager/VariableRepresentation.cpp \
    ./SurfaceModeling/Solver/DSISolvers/ConjugateGradientDSIBasedSolver.cpp \
    ./SurfaceModeling/Solver/DSISolvers/DSIBasedSolver.cpp \
    ./SurfaceModeling/Solver/DSISolvers/GlobalDSIBasedSolver.cpp \
    ./SurfaceModeling/Solver/DSISolvers/LocalDSIBasedSolver.cpp \
    ./SurfaceModeling/Solver/DSISolvers/PseudoConjugateGradientDSIBasedSolver.cpp \
    ./SurfaceModeling/Solver/OptimizationSolvers/LBFGSOptimizationSolver.cpp \
    ./SurfaceModeling/Solver/OptimizationSolvers/OptimizationSolver.cpp \
    ./SurfaceModeling/Solver/Solver.cpp \
    ./SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.cpp \
    graph.cpp \
    MarchingCubes/MarchingCubes.cpp \
    MarchingCubes/ply.c \
    io.cpp \
    octree.cpp \
    octreemc.cpp \
    main.cpp \
    surfacereconstruction.cpp \
    defs.cpp \
    adaptivesurfacereconstruction.cpp \
    quadtree.cpp \
    adaptivesurfacereconstruction2d.cpp \
    gridsurfacereconstruction.cpp \
    gridsurfacereconstruction2d.cpp

HEADERS += \
    ./SurfaceModeling/Graph/DSIGraph.h \
    ./SurfaceModeling/ModelManager/Constraint.h \
    ./SurfaceModeling/ModelManager/ConstraintRepresentation.h \
    ./SurfaceModeling/ModelManager/DSIModel.h \
    ./SurfaceModeling/ModelManager/Enums.h \
    ./SurfaceModeling/ModelManager/LinearExpression.h \
    ./SurfaceModeling/ModelManager/Model.h \
    ./SurfaceModeling/ModelManager/OptimizationModel.h \
    ./SurfaceModeling/ModelManager/PreprocessingEqualityConstraints.h \
    ./SurfaceModeling/ModelManager/PreprocessingSimpleConstraints.h \
    ./SurfaceModeling/ModelManager/QuadraticExpression.h \
    ./SurfaceModeling/ModelManager/Variable.h \
    ./SurfaceModeling/ModelManager/VariableRepresentation.h \
    ./SurfaceModeling/Solver/DSISolvers/ConjugateGradientDSIBasedSolver.h \
    ./SurfaceModeling/Solver/DSISolvers/DSIBasedSolver.h \
    ./SurfaceModeling/Solver/DSISolvers/GlobalDSIBasedSolver.h \
    ./SurfaceModeling/Solver/DSISolvers/LocalDSIBasedSolver.h \
    ./SurfaceModeling/Solver/DSISolvers/PseudoConjugateGradientDSIBasedSolver.h \
    ./SurfaceModeling/Solver/OptimizationSolvers/LBFGSOptimizationSolver.h \
    ./SurfaceModeling/Solver/OptimizationSolvers/OptimizationSolver.h \
    ./SurfaceModeling/Solver/Solver.h \
    ./SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h \
    graph.h \
    MarchingCubes/ply.h \
    MarchingCubes/MarchingCubes.h \
    MarchingCubes/LookUpTable.h \
    octree.h \
    io.h \
    defs.h \
    octreemc.h \
    surfacereconstruction.h \
    adaptivesurfacereconstruction.h \
    quadtree.h \
    adaptivesurfacereconstruction2d.h \
    gridsurfacereconstruction.h \
    gridsurfacereconstruction2d.h

QMAKE_CXXFLAGS+= -fopenmp -D_LONG__LONG
QMAKE_LFLAGS +=  -fopenmp -D_LONG__LONG


unix:!macx: LIBS += -L$$PWD/SurfaceModeling/libs/lis/lib/ -llis

INCLUDEPATH += $$PWD/SurfaceModeling/libs/lis/include
DEPENDPATH += $$PWD/SurfaceModeling/libs/lis/include

unix:!macx: PRE_TARGETDEPS += $$PWD/SurfaceModeling/libs/lis/lib/liblis.a

unix:!macx: LIBS += -L$$PWD/../../../libraries/gurobi752/linux64/lib/ -lgurobi_c++

INCLUDEPATH += $$PWD/../../../libraries/gurobi752/linux64/include
DEPENDPATH += $$PWD/../../../libraries/gurobi752/linux64/include

unix:!macx: PRE_TARGETDEPS += $$PWD/../../../libraries/gurobi752/linux64/lib/libgurobi_c++.a

unix:!macx: LIBS += -L$$PWD/../../../libraries/gurobi752/linux64/lib/ -lgurobi75

INCLUDEPATH += $$PWD/../../../libraries/gurobi752/linux64/include
DEPENDPATH += $$PWD/../../../libraries/gurobi752/linux64/include
