#include "adaptivesurfacereconstruction.h"

#include "io.h"

#include "SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "SurfaceModeling/ModelManager/QuadraticExpression.h"
#include "SurfaceModeling/ModelManager/OptimizationModel.h"

#include <sstream>


AdaptiveSurfaceReconstructor::AdaptiveSurfaceReconstructor(const std::vector<glm::vec3>& pointCloud, bool closed)
    : SurfaceReconstructor(pointCloud,closed)
    , oct({-2.0f,-2.0f,-2.0f},{2.0f,2.0f,2.0f})
{
}

void AdaptiveSurfaceReconstructor::setMinimumOctreeLeafSize(float size)
{
    oct.setMinimumSize(size);
}

void AdaptiveSurfaceReconstructor::exportNeutralFile(const std::string &path)
{
    writeNeutralFile(path,oct,meshVertices,meshIndices);
}

void AdaptiveSurfaceReconstructor::optimize()
{
    oct.setPoints(pts);
    oct.create();
    writeOFF("octree.off",oct);

    Solver::ConjugateGradientLISOptimizationSolver solver;
    ModelManager::OptimizationModel model(&solver);

    setupVariables(model);
    setupObjectiveFunction(model);
    setupInputPointsConstraints(model);
    setupBoundaryConstraints(model);

    model.optimize( );

    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();
    for(unsigned int i = 0; i < numVariables; ++i)
    {
        oct.setData(i,variables[i].getValue());
    }
}

void AdaptiveSurfaceReconstructor::reconstruct()
{

}

void AdaptiveSurfaceReconstructor::setupVariables(OptimizationModel &model)
{
    auto numVariables = oct.getVertices().size();

    std::vector<ModelManager::Variable> variables(numVariables);
    for(unsigned int i = 0; i < numVariables; ++i)
    {
        std::stringstream name;
        name << "x" << i;
        variables[i] = model.addVariable( name.str(), -1000, 1000 );
    }

    model.update();
}

void AdaptiveSurfaceReconstructor::setupObjectiveFunction(OptimizationModel &model)
{
    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();

    ModelManager::QuadraticExpression& obj = model.getObjectiveFunction();
    auto G = octreeToGraph(oct);
    for(unsigned int u = 0; u < numVariables; ++u)
    {
        auto neighbors = G.getNeighbors(u);

        ModelManager::LinearExpression exp;

        for(const auto& v : neighbors)
            exp += variables[v.v];

        exp -= exp.size()*variables[u];

        obj += exp*exp;
    }
}

void AdaptiveSurfaceReconstructor::setupInputPointsConstraints(OptimizationModel &model)
{
    auto sense = isSoft ? SOFT : EQUAL;

    auto variables = model.getVariables();

    auto calcVolume = [](const glm::vec3& p0, const glm::vec3& p1)
    {
        glm::vec3 dist = glm::abs(p1-p0);
        return dist.x*dist.y*dist.z;
    };

    auto vertices = oct.getVertices();

    for(const auto leaf : oct)
    {
        if(leaf.numPoints>0)
        {
            auto minPoint = vertices[leaf.indices[0]];
            auto maxPoint = vertices[leaf.indices[7]];

            float volume = calcVolume(minPoint,maxPoint);

            ModelManager::LinearExpression exp;
            for(unsigned int i = 0; i < 8; ++i)
            {
                auto iVolume = calcVolume(leaf.point,vertices[leaf.indices[7-i]]);
                auto bCoord  = iVolume / volume;

                exp += bCoord * variables[leaf.indices[i]];
            }

            model.addConstraint(exp,sense,0);
        }
    }

    model.setSoftConstraintsWeight(1000.0);
}

void AdaptiveSurfaceReconstructor::setupBoundaryConstraints(OptimizationModel &model)
{
    auto variables = model.getVariables();
    auto numVariables = model.getNumberVariables();
    auto vertices = oct.getVertices();

    float value = 10.0f;

    if(isClosed)
    {
        for(unsigned int u  = 0; u < numVariables; ++u)
        {
            if(vertices[u].x > 1.999f || vertices[u].x < -1.9999f ||
               vertices[u].y > 1.999f || vertices[u].y < -1.9999f ||
               vertices[u].z > 1.999f || vertices[u].z < -1.9999f)
            {
                model.addConstraint(variables[u], EQUAL, value);
            }
        }
        //Assuming the center index is inside the object
        auto centerIdx = 26;
        model.addConstraint(variables[centerIdx],LESS_EQUAL,-value);
    }

    else
    {
//        //Bottom indices
//        model.addConstraint(variables[0], EQUAL,  -value);
//        model.addConstraint(variables[1], EQUAL,  -value);
//        model.addConstraint(variables[4], EQUAL,  -value);
//        model.addConstraint(variables[5], EQUAL,  -value);
//        model.addConstraint(variables[8], EQUAL,  -value);
//        model.addConstraint(variables[12], EQUAL, -value);
//        model.addConstraint(variables[13], EQUAL, -value);
//        model.addConstraint(variables[16], EQUAL, -value);
//        model.addConstraint(variables[21], EQUAL, -value);

//        //Top indices
//        model.addConstraint(variables[2], EQUAL, value);
//        model.addConstraint(variables[3], EQUAL, value);
//        model.addConstraint(variables[6], EQUAL, value);
//        model.addConstraint(variables[7], EQUAL, value);
//        model.addConstraint(variables[11], EQUAL, value);
//        model.addConstraint(variables[14], EQUAL, value);
//        model.addConstraint(variables[15], EQUAL, value);
//        model.addConstraint(variables[19], EQUAL, value);
//        model.addConstraint(variables[24], EQUAL, value);

        //Front indices
        model.addConstraint(variables[0], EQUAL,  -value);
        model.addConstraint(variables[1], EQUAL,  -value);
        model.addConstraint(variables[2], EQUAL,  -value);
        model.addConstraint(variables[3], EQUAL,  -value);
        model.addConstraint(variables[8], EQUAL,  -value);
        model.addConstraint(variables[9], EQUAL,  -value);
        model.addConstraint(variables[10], EQUAL, -value);
        model.addConstraint(variables[11], EQUAL, -value);
        model.addConstraint(variables[20], EQUAL, -value);

        //Back indices
        model.addConstraint(variables[4], EQUAL, value);
        model.addConstraint(variables[5], EQUAL, value);
        model.addConstraint(variables[6], EQUAL, value);
        model.addConstraint(variables[7], EQUAL, value);
        model.addConstraint(variables[16], EQUAL, value);
        model.addConstraint(variables[17], EQUAL, value);
        model.addConstraint(variables[18], EQUAL, value);
        model.addConstraint(variables[19], EQUAL, value);
        model.addConstraint(variables[25], EQUAL, value);
    }

}
