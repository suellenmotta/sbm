#ifndef ADAPTIVESURFACERECONSTRUCTION2D_H
#define ADAPTIVESURFACERECONSTRUCTION2D_H

#include "surfacereconstruction.h"
#include "quadtree.h"

#include "SurfaceModeling/ModelManager/OptimizationModel.h"
using namespace ModelManager;

class AdaptiveSurfaceReconstructor2D
        : public SurfaceReconstructor2D
{
public:
    AdaptiveSurfaceReconstructor2D(const std::vector<glm::vec2> &pointCloud, bool closed = false);

    void setMinimumQuadtreeLeafSize(float size);
    void exportNeutralFile(const std::string& path);

private:
    void optimize();

    void setupVariables(OptimizationModel& model);
    void setupObjectiveFunction(OptimizationModel& model);
    void setupInputPointsConstraints(OptimizationModel& model);
    void setupBoundaryConstraints(OptimizationModel& model);

    Quadtree<float> qdt;
};

#endif // ADAPTIVESURFACERECONSTRUCTION2D_H
