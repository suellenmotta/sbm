#ifndef ADAPTIVESURFACERECONSTRUCTION_H
#define ADAPTIVESURFACERECONSTRUCTION_H

#include "surfacereconstruction.h"
#include "octree.h"

#include "SurfaceModeling/ModelManager/OptimizationModel.h"
using namespace ModelManager;

class AdaptiveSurfaceReconstructor
        : public SurfaceReconstructor3D
{
public:
    AdaptiveSurfaceReconstructor(const std::vector<glm::vec3>& pointCloud, bool closed = false);

    void setMinimumOctreeLeafSize(float size);
    void exportNeutralFile(const std::string& path);

private:
    void optimize() override;
    void reconstruct() override;

    void setupVariables(OptimizationModel& model);
    void setupObjectiveFunction(OptimizationModel& model);
    void setupInputPointsConstraints(OptimizationModel& model);
    void setupBoundaryConstraints(OptimizationModel& model);

    Octree<float> oct;
};

#endif // ADAPTIVESURFACERECONSTRUCTION_H
