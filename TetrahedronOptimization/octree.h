#ifndef OCTREE_H
#define OCTREE_H

#include <vector> //list would be probably better
#include <array>
#include <stack>
#include <glm/glm.hpp>

#include "graph.h"

template< typename T >
class Octree
{
public:
    Octree(const glm::vec3 &minPoint, const glm::vec3 &maxPoint, const std::vector<glm::vec3>& points);
    Octree(const glm::vec3 &minPoint, const glm::vec3 &maxPoint);

    ~Octree();

    void create();

    const std::vector<glm::vec3>& getVertices() const;
    const std::vector<T>& getData() const;
    std::vector<unsigned int> getBoundaryIndices() const;

    unsigned int getNumNodes() const;
    unsigned int getNumLeafNodes() const;

    void setPoints(const std::vector<glm::vec3>& points);
    void setData(unsigned int index, const T& element);
    void setData(const std::vector<T>& data);

    void setMinimumSize(float size);

    struct Node
    {
        //Level of the node in the tree
        unsigned int level;

        //Number of points inside this node
        unsigned int numPoints;

        //Pointers to the sons. It contains eight or zero elements.
        std::vector<Node*> sons;

        /* Indices order:
         *                          y  z
         *        6-----------7     | /
         *       /|          /|     |/
         *      / |         / |     ----x
         *     2-----------3  |
         *     |  4--------|--5
         *     | /         | /
         *     |/          |/
         *     0-----------1
         *
         */
        std::array<unsigned int, 8> indices;

        //The one point inside the node, when numPoints is equal to 1
        glm::vec3 point;
    };

private:

    Node* root;

    std::vector<glm::vec3> initPoints;

    std::vector< glm::vec3 > vertices;

    std::vector< T > data;

    unsigned int numNodes;
    unsigned int numLNodes;

    bool useMinimumSize;
    float minimumSize;

    bool isInside(const glm::vec3& point, const glm::vec3& minPoint,
                  const glm::vec3& maxPoint, float eps = 0.0f);

    //void subdivide(Bin* bin, const std::vector<glm::vec3>& points, std::map<std::pair<unsigned int, unsigned int>, unsigned int> &edges);

    void destructor(Node* node);



public:


    //Iterating functionality
    class iterator
    {
    public:
        iterator operator++(int){ nextLeaf(); return *this; }
        iterator operator++(){ auto it = *this; nextLeaf(); return it; }
        bool operator==(const iterator& rhs) { return cur == rhs.cur; }
        bool operator!=(const iterator& rhs) { return cur != rhs.cur; }
        Octree::Node* operator->() { return cur; }
        Octree::Node& operator*() { return *cur; }


        friend class Octree;

        Octree::Node* curParent;
        Octree::Node* cur;

    private:
        iterator(Octree::Node* root)
            : cur(nullptr)
        {
            if(root)
            {
                stack.push(root);
                nextLeaf();
            }
        }

        void nextLeaf()
        {
            if(stack.empty())
                cur = nullptr;

            else
            {
                while(!stack.empty())
                {
                    Node* n = stack.top();
                    stack.pop();

                    if(!n->sons.empty())
                    {
                        for(auto it = n->sons.rbegin(); it != n->sons.rend(); ++it)
                        {
                            stack.push(*it);
                        }
                    }

                    else
                    {
                        cur = n;
                        break;
                    }
                }
            }
        }

        //For recursive call simulation
        std::stack<Node*> stack;

    };


    iterator begin() const
    {
        return iterator(root);
    }

    iterator end() const
    {
        return iterator(nullptr);
    }


};

template class Octree<float>;

template<typename T>
Graph octreeToGraph(const Octree<T> &octree)
{
    Graph G(octree.getVertices().size());

    for(const auto& node : octree)
    {
        G.insertEdge(node.indices[0],node.indices[1]);
        G.insertEdge(node.indices[0],node.indices[2]);
        G.insertEdge(node.indices[0],node.indices[4]);
        G.insertEdge(node.indices[1],node.indices[3]);
        G.insertEdge(node.indices[1],node.indices[5]);
        G.insertEdge(node.indices[2],node.indices[3]);
        G.insertEdge(node.indices[2],node.indices[6]);
        G.insertEdge(node.indices[3],node.indices[7]);
        G.insertEdge(node.indices[4],node.indices[5]);
        G.insertEdge(node.indices[4],node.indices[6]);
        G.insertEdge(node.indices[5],node.indices[7]);
        G.insertEdge(node.indices[6],node.indices[7]);
    }

    return G;

}

void octreeMarchingCubes(const Octree<float>& octree, std::vector<unsigned int>& indices, std::vector<glm::vec3>& vertices);


#endif // OCTREE_H
