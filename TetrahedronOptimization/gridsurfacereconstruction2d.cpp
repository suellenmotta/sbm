#include "gridsurfacereconstruction2d.h"

#include "SurfaceModeling/Solver/OptimizationSolvers/ConjugateGradientLISOptimizationSolver.h"
#include "SurfaceModeling/ModelManager/QuadraticExpression.h"
#include "SurfaceModeling/ModelManager/OptimizationModel.h"

#include "io.h"

#include <sstream>

GridSurfaceReconstructor2D::GridSurfaceReconstructor2D(const std::vector<glm::vec2> &pointCloud, bool closed)
    : SurfaceReconstructor2D(pointCloud,closed)
{

}

void GridSurfaceReconstructor2D::setGridResolution(unsigned int width, unsigned int height)
{
    if(width%2 > 0)  width++;
    if(height%2 > 0) height++;

    grid.nx = 2*width+1;
    grid.ny = 2*height+1;
    grid.nz = 1;

    grid.sx = 4.0f / (grid.nx-1);
    grid.sy = 4.0f / (grid.ny-1);
    grid.sz = 0.0f;

    writeOFF("grid.off",grid);
}

void GridSurfaceReconstructor2D::exportNeutralFile(const std::string &path)
{
    writeNeutralFile(path,grid,meshVertices,meshIndices);
}

void GridSurfaceReconstructor2D::optimize()
{
    Solver::ConjugateGradientLISOptimizationSolver solver;
    ModelManager::OptimizationModel model(&solver);

    std::vector<SamplePoint2D> sampledPoints;
    samplePoints(sampledPoints);

    writeXYZ("sampled_points.xyz", sampledPoints);

    setupVariables(model);
    setupObjectiveFunction(model);
    setupInputPointsConstraints(model,sampledPoints);
    setupBoundaryConstraints(model);

    model.optimize();

    auto numVariables = model.getNumberVariables();
    grid.functionValue.resize(numVariables,0.0f);

    auto variables = model.getVariables();

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        double x = variables[i].getValue();
        grid.functionValue[i] = x;
    }
}


void GridSurfaceReconstructor2D::reconstruct()
{
}

void GridSurfaceReconstructor2D::setupVariables(OptimizationModel& model)
{
    auto numVariables = grid.nx*grid.ny*grid.nz;

    for(unsigned int i = 0; i < numVariables; ++i)
    {
        std::stringstream name;
        name << "x" << i;
        model.addVariable( name.str(), -1000, 1000 );
    }

    model.update();
}

void GridSurfaceReconstructor2D::setupObjectiveFunction(OptimizationModel& model)
{
    auto variables = model.getVariables();
    ModelManager::QuadraticExpression& obj = model.getObjectiveFunction();

    int nx = (int)grid.nx;
    int ny = (int)grid.ny;
    int nz = (int)grid.nz;

    const int numThreads = std::max( omp_get_max_threads( ) - 2, 1 );
    omp_lock_t writelock;
    omp_init_lock( &writelock );

    for(int z = 0; z < nz; ++z)
    {
        printf( "\r%6.2lf%% complete...", 100.0 * ( double ) z / grid.nz );
        fflush( stdout );

        #pragma omp parallel for num_threads(numThreads)
        for(int y = 0; y < ny; ++y)
        {
            ModelManager::QuadraticExpression aux;

            for(int x = 0; x < nx; ++x)
            {
                ModelManager::QuadraticExpression aux2;
                ModelManager::LinearExpression exp;

                for(int k = -1; k < 2; ++k)
                {
                    auto zk = z+k;

                    if(zk >= 0 && zk < nz)
                    {
                        for(int j = -1; j < 2; ++j)
                        {
                            auto yj = y+j;

                            if(yj >= 0 && yj < ny)
                            {
                                for(int i = -1; i < 2; ++i)
                                {
                                    auto xi = x+i;

                                    if(xi >= 0 && xi < nx)
                                    {
                                        auto idx = grid.index(xi,yj,zk);
                                        exp += variables[idx];
                                    }
                                }
                            }
                        }
                    }
                }

                auto idx = grid.index(x,y,z);
                exp -= exp.size()*variables[idx];

                aux2 = exp * exp;
                aux2.update( );
                aux += aux2;
            }

            aux.update( );

            omp_set_lock( &writelock );
            obj += aux;
            omp_unset_lock( &writelock );
        }
    }
}

void GridSurfaceReconstructor2D::setupInputPointsConstraints(OptimizationModel& model,
                                                       const std::vector<SamplePoint2D> &sampledPoints)
{
    auto sense = isSoft ? SOFT : EQUAL;

    auto variables = model.getVariables();

    for (unsigned int i = 0; i < sampledPoints.size( ); i++)
    {
        const auto& p = sampledPoints[i];

        auto gPos = p.gridPos;

        ModelManager::LinearExpression exp;

        unsigned int idx = 0;
        for(unsigned int j = 0; j < 2; j++)
            {
                for(unsigned int i = 0; i < 2; i++)
                {
                    exp +=
                        p.bCoords[idx] * variables[grid.index(gPos.x+i, gPos.y+j, 0)];

                    ++idx;
                }
            }

        model.addConstraint(exp,sense,0);
    }

    if(isSoft)
        model.setSoftConstraintsWeight(1000.0);
}

void GridSurfaceReconstructor2D::setupBoundaryConstraints(OptimizationModel& model)
{
    auto nx = grid.nx;
    auto ny = grid.ny;

    auto variables = model.getVariables();

    float value = 10.0f;

    if(isClosed)
    {
        for(unsigned int y = 0; y < ny; ++y)
            {
                for(unsigned int x = 0; x < nx; ++x)
                {
                    if( (nx > 1 && (x==0 || x==nx-1)) ||
                        (ny > 1 && (y==0 || y==ny-1)) )
                    {
                        model.addConstraint(variables[grid.index(x,y,0)], EQUAL, value);
                    }
                }
            }

        //Assuming the center index is inside the object
        auto centerIdx = grid.index(nx*0.5,ny*0.5,0);
        model.addConstraint(variables[centerIdx],LESS_EQUAL,-value);
    }

    else
    {
        for(unsigned int x = 0; x < nx; ++x)
            {
                auto bottomIdx = grid.index(x,0,0);
                auto topIdx    = grid.index(x,ny-1,0);

                model.addConstraint(variables[bottomIdx], EQUAL, value);
                model.addConstraint(variables[topIdx],    EQUAL, -value);
            }
    }
}

void GridSurfaceReconstructor2D::samplePoints(std::vector<SamplePoint2D>& sampledPoints)
{
    auto area = grid.sx*grid.sy;

    auto calcArea = [](const glm::vec2& p0, const glm::vec2& p1)
    {
        auto dist = glm::abs(p1-p0);
        return dist.x*dist.y;
    };

    auto computeCoords2d = [&](SamplePoint2D& p,
            const glm::vec2& pMin)
    {
        p.bCoords.resize(4);
        p.bCoords[0] = calcArea(p.coords,pMin+glm::vec2(grid.sx,grid.sy)) / area;
        p.bCoords[1] = calcArea(p.coords,pMin+glm::vec2(0,grid.sy)) / area;
        p.bCoords[2] = calcArea(p.coords,pMin+glm::vec2(grid.sx,0)) / area;
        p.bCoords[3] = calcArea(p.coords,pMin) / area;
    };

    glm::vec2 first = {-2.0f,-2.0f};

    for(unsigned int j = 0; j < grid.ny-1; ++j)
        {
            auto yMin = first.y + j*grid.sy;
            auto yMax = yMin+grid.sy;

            for(unsigned int i = 0; i < grid.nx-1; ++i)
            {
                auto xMin = first.x + i*grid.sx;
                auto xMax = xMin+grid.sx;

                for(const auto& p : pts)
                {
                    auto eps = 0.01f;

                    if(p.x >= xMin+eps && p.x <= xMax-eps &&
                       p.y >= yMin+eps && p.y <= yMax-eps)
                    {
                        SamplePoint2D point;
                        point.coords = p;
                        point.gridPos = {i,j};

                        computeCoords2d(point,{xMin,yMin});

                        sampledPoints.push_back(point);
                        break;
                    }
                }
            }
        }
}
