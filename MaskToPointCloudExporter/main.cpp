#include <QImageReader>
#include <QVector3D>

#include <unordered_map>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>


using PointCloud = std::vector<QVector3D>;
std::unordered_map<QRgb,PointCloud> pointClouds;


void savePointCloud(const std::string& path, const PointCloud& pc)
{
    std::ofstream output;
    output.open( path );

    if (output.is_open())
    {
        output << pc.size() << std::endl;

        for(const auto& p : pc)
        {
            output << p.x() << " " << p.y() << " " << p.z() << std::endl;
        }

        output.close();
    }
}

int xTransf(int x) //crossline
{
    return x-410;
}

int yTransf(int y) //depth
{
    return y-(532.f/4);
}

int zTransf(int z) //inline
{
    return z-10;
}


void processImage(const std::string& path,
                  unsigned int z)
{
    QImage image(QString::fromStdString(path));
    if(!image.isNull() && image.isGrayscale())
    {
        auto w = image.size().width();
        auto h = image.size().height();

        for(int y = 1; y < h-1; ++y)
        {
            for(int x = 1; x < w-1; ++x)
            {
                auto pixel = image.pixel(x,y);

                if( image.pixel(x-1,y-1) != pixel
                 || image.pixel(x-1,y+0) != pixel
                 || image.pixel(x-1,y+1) != pixel
                 || image.pixel(x+0,y-1) != pixel
                 || image.pixel(x+0,y+1) != pixel
                 || image.pixel(x+1,y-1) != pixel
                 || image.pixel(x+1,y+0) != pixel
                 || image.pixel(x+1,y+1) != pixel ) //If it is a border pixel
                {
                    auto it = pointClouds.find(pixel);
                    if(it==pointClouds.end())
                    {
                        pointClouds[pixel] = PointCloud();
                    }
                    pointClouds[pixel].push_back(QVector3D(zTransf(z),xTransf(x),yTransf(y)));
                }
            }
        }
    }
}


void processVolume(const std::string& folder,
                   const std::string& prefix,
                   const std::string& suffix, //suffix with .extension
                   int firstIdx, int lastIdx, int stepIdx,
                   const std::string& outputFolder)
{
    int count = 0;

    for(auto cur = firstIdx; cur <= lastIdx; cur += stepIdx)
    {
        std::stringstream path;
        path << folder << prefix << cur << suffix;

        processImage(path.str(),count++);
    }

    std::cout << "Point clouds found: " << pointClouds.size() << std::endl;

    count = 0;
    for(const auto& pc : pointClouds)
    {
        std::stringstream path;
        path << outputFolder << "pointcloud_" << count++ << ".xyz";

        savePointCloud(path.str(),pc.second);
    }

    std::cout << "Done." << std::endl;
}


int main(int, char**)
{
    std::string folder = "C:/Users/suellen/Google Drive/Doutorado/Tese/Dados/F3/masks/";
    std::string prefix = "inline_";
    std::string suffix = "_mask.png";
    int firstIdx = 100;
    int lastIdx = 750;
    int stepIdx = 1;
    std::string outputFolder = "./";

    processVolume(folder,prefix,suffix,firstIdx,lastIdx,stepIdx,outputFolder);
}
