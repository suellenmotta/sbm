/****************************************************************************
Copyright 2005, Colorado School of Mines and others.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

#ifndef RECURSIVEGAUSSIANFILTER_H
#define RECURSIVEGAUSSIANFILTER_H

#include "filter.h"

/**
 * Recursive implementation of a Gaussian filter and derivatives. Filters
 * include the 0th, 1st, and 2nd derivatives. The impulse response of the
 * 0th-derivative smoothing filter is infinitely long, and is approximately
 * h[n] = 1.0/(sqrt(2*PI)*sigma)*exp(-0.5*(n*n)/(sigma*sigma)). Here,
 * sigma denotes the standard width of the Gaussian.
 * <p>
 * For large filter widths sigma, this recursive implementation can be
 * much more efficient than convolution with a truncated Gaussian.
 * Specifically, if the Gaussian is truncated for |n| &gt; 4*sigma, then
 * this recursive implementation requires 2/sigma of the multiplications
 * required by convolution. In other words, for sigma &gt; 2, this
 * recursive implementation should be more efficient than convolution.
 * <p>
 * For any application of this filter, input and output arrays may be the
 * same array. When the filter cannot be applied in-place, intermediate
 * arrays are constructed internally.
 * <p>
 * This filter implements two different methods for approximating
 * with difference equations a Gaussian filter and its derivatives.
 * <p>
 * The first method is that of Deriche, R., 1993, Recursively implementing
 * the Gaussian and its derivatives: INRIA Research Report, number 1893.
 * Deriche's method is used for small widths sigma, for which it is most
 * accurate.
 * <p>
 * The second method is that of van Vliet, L.J., Young, I.T., and Verbeek,
 * P.W., 1998, Recursive Gaussian derivative filters, Proceedings of the
 * 14th International Conference on Pattern Recognition, IEEE Computer
 * Society Press. The parallel implementation used here yields zero-phase
 * impulse responses without the end effects caused by the serial (cascade)
 * poles-only implementation recommended by van Vliet, et al. This
 * second method is used for large widths sigma.
 * @author Dave Hale, Colorado School of Mines
 * @version 2006.02.12
 *
 *
 * Conversion from Java to C++ by Suellen Motta, PUC-Rio in 2018.09.10
 * Only the second method (van Vliet) was converted.
 *
 */
class RecursiveGaussianFilter
{
public:
    /**
     * Construct a Gaussian filter with specified width.
     * @param sigma the width; must not be less than 1.
     */
    RecursiveGaussianFilter(double sigma);

    ~RecursiveGaussianFilter();

    /**
    * Applies the 0th-derivative filter.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply0(const Array1D& x, Array1D& y);

    /**
    * Applies the 1st-derivative filter.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply1(const Array1D& x, Array1D& y);

    /**
    * Applies the 2nd-derivative filter.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply2(const Array1D& x, Array1D& y);

    /**
    * Applies the 0th-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply0X(const Array2D& x, Array2D& y);

    /**
    * Applies the 1st-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply1X(const Array2D& x, Array2D& y);

    /**
    * Applies the 2nd-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply2X(const Array2D& x, Array2D& y);

    /**
    * Applies the 0th-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX0(const Array2D& x, Array2D& y);

    /**
    * Applies the 1st-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX1(const Array2D& x, Array2D& y);

    /**
    * Applies the 2nd-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX2(const Array2D& x, Array2D& y);

    /**
    * Applies the 0th-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply0XX(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply1XX(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 1st dimension.
    * Applies no filter along the 2nd or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply2XX(const Array3D& x, Array3D& y);

    /**
    * Applies the 0th-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX0X(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX1X(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 2nd dimension.
    * Applies no filter along the 1st or 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyX2X(const Array3D& x, Array3D& y);

    /**
    * Applies the 0th-derivative filter along the 3rd dimension.
    * Applies no filter along the 1st or 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyXX0(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 3rd dimension.
    * Applies no filter along the 1st or 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyXX1(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 3rd dimension.
    * Applies no filter along the 1st or 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void applyXX2(const Array3D& x, Array3D& y);

    /**
    * Applies the 0th-derivative filter along the 1st and 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply00(const Array2D& x, Array2D& y);

    /**
    * Applies the 1st-derivative filter along the 1st dimension
    * and the 0th-derivative filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply10(const Array2D& x, Array2D& y);

    /**
    * Applies the 0th-derivative filter along the 1st dimension
    * and the 1st-derivative filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply01(const Array2D& x, Array2D& y);

    /**
    * Applies the 1st-derivative filter along the 1st and 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply11(const Array2D& x, Array2D& y);

    /**
    * Applies the 2nd-derivative filter along the 1st dimension
    * and the 0th-derivative filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply20(const Array2D& x, Array2D& y);

    /**
    * Applies the 0th-derivative filter along the 1st dimension
    * and the 2nd-derivative filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply02(const Array2D& x, Array2D& y);

    /**
    * Applies the 0th-derivative filter along the 1st, 2nd and 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply000(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 1st dimension
    * and the 0th-derivative filter along the 2nd and 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply100(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 2nd dimension
    * and the 0th-derivative filter along the 1st and 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply010(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 3rd dimension
    * and the 0th-derivative filter along the 1st and 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply001(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 1st and 2nd dimensions
    * and the 0th-derivative filter along the 3rd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply110(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 1st and 3rd dimensions
    * and the 0th-derivative filter along the 2nd dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply101(const Array3D& x, Array3D& y);

    /**
    * Applies the 1st-derivative filter along the 2nd and 3rd dimensions
    * and the 0th-derivative filter along the 1st dimension.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply011(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 1st dimension
    * and the 0th-derivative filter along the 2nd and 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply200(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 2nd dimension
    * and the 0th-derivative filter along the 1st and 3rd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply020(const Array3D& x, Array3D& y);

    /**
    * Applies the 2nd-derivative filter along the 3rd dimension
    * and the 0th-derivative filter along the 1st and 2nd dimensions.
    * @param x the filter input.
    * @param y the filter output.
    */
    void apply002(const Array3D& x, Array3D& y);

private:
     /**
       * Copies elements from one specified array to another.
       * @param n1 number of elements to copy in 1st dimension.
       * @param j1x offset in 1st dimension of rx.
       * @param rx source array.
       * @param j1y offset in 1st dimension of ry.
       * @param ry destination array.
       */
      void copy(
        int n1,
        int j1x, const Array1D& rx,
        int j1y, Array1D& ry);
      /**
        * Copies elements from one specified array to another.
        * @param n1 number of elements to copy in 1st dimension.
        * @param n2 number of elements to copy in 2nd dimension.
        * @param j1x offset in 1st dimension of rx.
        * @param j2x offset in 2nd dimension of rx.
        * @param rx source array.
        * @param j1y offset in 1st dimension of ry.
        * @param j2y offset in 2nd dimension of ry.
        * @param ry destination array.
        */
        void copy(
            int n1, int n2,
            int j1x, int j2x, const Array2D& rx,
            int j1y, int j2y, Array2D& ry);
      /**
        * Copies elements from one specified array to another.
        * @param n1 number of elements to copy in 1st dimension.
        * @param n2 number of elements to copy in 2nd dimension.
        * @param n3 number of elements to copy in 3rd dimension.
        * @param j1x offset in 1st dimension of rx.
        * @param j2x offset in 2nd dimension of rx.
        * @param j3x offset in 3rd dimension of rx.
        * @param rx source array.
        * @param j1y offset in 1st dimension of ry.
        * @param j2y offset in 2nd dimension of ry.
        * @param j3y offset in 3rd dimension of ry.
        * @param ry destination array.
        */
        void copy(
          int n1, int n2, int n3,
          int j1x, int j2x, int j3x, const Array3D& rx,
          int j1y, int j2y, int j3y, Array3D& ry);

    Array1D pad(int d, const Array1D& x);
    Array2D pad(int d, const Array2D& x);
    Array3D pad(int d, const Array3D& x);


    int _d;
    Filter* _filter;
};

#endif // RECURSIVEGAUSSIANFILTER_H
