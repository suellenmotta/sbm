#include "recursivegaussianfilter.h"

#include "DericheFilter.h"
#include "vanvlietfilter.h"

RecursiveGaussianFilter::RecursiveGaussianFilter(double sigma)
    : _d((int)(sigma*5))
{
    if(sigma >= 1)
    {
        _filter =  new DericheFilter(sigma);
    }
    else
    {
        _filter = new VanVlietFilter(sigma);
    }
}

RecursiveGaussianFilter::~RecursiveGaussianFilter()
{
    delete _filter;
}

void RecursiveGaussianFilter::apply0(const Array1D& x, Array1D& y) {
  int n = (int)x.size();
  Array1D xt = pad(_d,x);
  Array1D yt(xt.size());
  _filter->applyN(0,xt,yt);
  copy(n,_d,yt,0,y);
}

void RecursiveGaussianFilter::apply1(const Array1D& x, Array1D& y) {
  int n = (int)x.size();
  Array1D xt = pad(_d,x);
  Array1D yt(xt.size());
  _filter->applyN(1,xt,yt);
  copy(n,_d,yt,0,y);
}

void RecursiveGaussianFilter::apply2(const Array1D& x, Array1D& y) {
  int n = (int)x.size();
  Array1D xt = pad(_d,x);
  Array1D yt(xt.size());
  _filter->applyN(2,xt,yt);
  copy(n,_d,yt,0,y);
}

void RecursiveGaussianFilter::apply0X(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyNX(0,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply1X(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyNX(1,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply2X(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyNX(2,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::applyX0(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(0,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::applyX1(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(1,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::applyX2(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(2,xt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply0XX(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyNXX(0,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply1XX(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyNXX(1,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply2XX(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyNXX(2,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyX0X(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXNX(0,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyX1X(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXNX(1,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyX2X(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXNX(2,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyXX0(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyXX1(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(1,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::applyXX2(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(2,xt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply00(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(0,xt,yt);
  _filter->applyNX(0,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply10(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(0,xt,yt);
  _filter->applyNX(1,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply01(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(1,xt,yt);
  _filter->applyNX(0,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply11(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(1,xt,yt);
  _filter->applyNX(1,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply20(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(0,xt,yt);
  _filter->applyNX(2,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply02(const Array2D& x, Array2D& y) {
  int n2 = (int)x.size();
  int n1 = (int)x[0].size();
  Array2D xt = pad(_d,x);
  Array2D yt(xt.size(),Array1D(xt[0].size()));
  _filter->applyXN(2,xt,yt);
  _filter->applyNX(0,yt,yt);
  copy(n1,n2,_d,_d,yt,0,0,y);
}

void RecursiveGaussianFilter::apply000(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply100(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(1,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply010(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(1,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply001(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(1,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply110(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(1,yt,yt);
  _filter->applyNXX(1,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply101(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(1,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(1,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply011(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(1,xt,yt);
  _filter->applyXNX(1,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply200(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(2,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply020(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(0,xt,yt);
  _filter->applyXNX(2,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::apply002(const Array3D& x, Array3D& y) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  Array3D xt = pad(_d,x);
  Array3D yt(xt.size(),Array2D(xt[0].size(),Array1D(xt[0][0].size())));
  _filter->applyXXN(2,xt,yt);
  _filter->applyXNX(0,yt,yt);
  _filter->applyNXX(0,yt,yt);
  copy(n1,n2,n3,_d,_d,_d,yt,0,0,0,y);
}

void RecursiveGaussianFilter::copy(
   int n1,
   int j1x, const Array1D& rx,
   int j1y, Array1D& ry)
 {
   for (int i1=0,ix=j1x,iy=j1y; i1<n1; ++i1)
     ry[iy++] = rx[ix++];
 }

void RecursiveGaussianFilter::copy(
       int n1, int n2,
       int j1x, int j2x, const Array2D& rx,
       int j1y, int j2y, Array2D& ry)
   {
     for (int i2=0; i2<n2; ++i2)
       copy(n1,j1x,rx[j2x+i2],j1y,ry[j2y+i2]);
   }

   void RecursiveGaussianFilter::copy(
     int n1, int n2, int n3,
     int j1x, int j2x, int j3x, const Array3D& rx,
     int j1y, int j2y, int j3y, Array3D& ry)
   {
     for (int i3=0; i3<n3; ++i3)
       copy(n1,n2,j1x,j2x,rx[j3x+i3],j1y,j2y,ry[j3y+i3]);
   }


Array1D RecursiveGaussianFilter::pad(int d, const Array1D& x)
{
 int n = (int)x.size();
 Array1D y(n+2*d);
 copy(n,0,x,d,y);
 for (int k=0; k<d; k++) {
   y[k    ] = x[0  ];
   y[n+d+k] = x[n-1];
 }
 return y;
}

Array2D RecursiveGaussianFilter::pad(int d, const Array2D& x)
{
 int n2 = (int)x.size();
 int n1 = (int)x[0].size();
 int m1 = n1+d*2;
 int m2 = n2+d*2;
 Array2D y(m2,Array1D(m1));
 copy(n1,n2,0,0,x,d,d,y);
 for (int i2=0; i2<m2; i2++) {
 for (int k=0; k<d; k++) {
   y[i2][k     ] = y[i2][d     ];
   y[i2][n1+d+k] = y[i2][n1+d-1];
 }}
 for (int i1=0; i1<m1; i1++) {
 for (int k=0; k<d; k++) {
   y[k     ][i1] = y[d     ][i1];
   y[n2+d+k][i1] = y[n2+d-1][i1];
 }}
 return y;
}

Array3D RecursiveGaussianFilter::pad(int d, const Array3D& x) {
  int n3 = (int)x.size();
  int n2 = (int)x[0].size();
  int n1 = (int)x[0][0].size();
  int m1 = n1+d*2;
  int m2 = n2+d*2;
  int m3 = n3+d*2;
  Array3D y(m3, Array2D(m2, Array1D(m1)));
  copy(n1,n2,n3,0,0,0,x,d,d,d,y);
  for (int i3=0; i3<m3; i3++) {
  for (int i2=0; i2<m2; i2++) {
  for (int k=0; k<d; k++) {
    y[i3][i2][k     ] = y[i3][i2][d     ];
    y[i3][i2][n1+d+k] = y[i3][i2][n1+d-1];
  }}}
  for (int i3=0; i3<m3; i3++) {
  for (int i1=0; i1<m1; i1++) {
  for (int k=0; k<d; k++) {
    y[i3][k     ][i1] = y[i3][d     ][i1];
    y[i3][n2+d+k][i1] = y[i3][n2+d-1][i1];
  }}}
  for (int i2=0; i2<m2; i2++) {
  for (int i1=0; i1<m1; i1++) {
  for (int k=0; k<d; k++) {
    y[k     ][i2][i1] = y[d     ][i2][i1];
    y[n3+d+k][i2][i1] = y[n3+d-1][i2][i1];
  }}}
  return y;
}
