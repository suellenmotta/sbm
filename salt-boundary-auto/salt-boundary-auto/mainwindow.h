#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "Canvas2D.h"

#include "ViewOptionsDialog.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void createToolBar();

    QToolBar* toolBar;
    Canvas2D* canvas;

    Image* seismicImage;
    Image* attributeImage;
    Image* gradXImage;
    Image* gradYImage;

    ViewOptionsDialog* dlg;

private slots:
    void onLoadAmplitudeImageTriggered();
    void onLoadAttributeImageTriggered();
    void onViewAttributeImageToggled(bool viewAttr);
    void onViewOptionsTriggered();
    void onPickPointsToggled(bool pickPts);
    void onExecuteTriggered();
    void onRestartTriggered();

    void setupMinMaxValues();
    void onMinValueChanged(double value);
    void onMaxValueChanged(double value);
};

#endif //MAINWINDOW_H
