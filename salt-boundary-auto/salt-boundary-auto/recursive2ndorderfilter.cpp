#include "recursive2ndorderfilter.h"

Recursive2ndOrderFilter::Recursive2ndOrderFilter()
    : Recursive2ndOrderFilter(0.f,0.f,0.f,0.f,0.f)
{

}

Recursive2ndOrderFilter::Recursive2ndOrderFilter(float b0, float b1, float b2, float a1, float a2)
{
    _b0 = b0;
    _b1 = b1;
    _b2 = b2;
    _a1 = a1;
    _a2 = a2;
}

Recursive2ndOrderFilter::Recursive2ndOrderFilter(double pole, double zero, double gain)
{
  _b0 = (float)(gain);
  _b1 = (float)(-gain*zero);
  _a1 = (float)(-pole);
}

Recursive2ndOrderFilter::Recursive2ndOrderFilter(
  std::complex<float> pole1, std::complex<float> pole2,
  std::complex<float> zero1, std::complex<float> zero2,
  double gain)
{
//      Check.argument(pole1.i==0.0     &&  pole2.i==0.0 ||
//                     pole2.real()==pole1.real() && -pole2.i==pole1.i,
//                     "poles are real or conjugate pair");
//      Check.argument(zero1.i==0.0     &&  zero2.i==0.0 ||
//                     zero2.real()==zero1.real() && -zero2.i==zero1.i,
//                     "zeros are real or conjugate pair");
  _b0 = (float)(gain);
  _b1 = (float)(-(zero1.real()+zero2.real())*gain);
  _b2 = (float)((zero1*zero2).real()*gain);
  _a1 = (float)(-(pole1.real()+pole2.real()));
  _a2 = (float)((pole1*pole2).real());
}

void Recursive2ndOrderFilter::applyForward(Array1D x, Array1D& y)
{
  checkArrays(x,y);
  int n = (int)y.size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f)
//  {
//    float yim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b0*xi-_a1*yim1;
//      y[i] = yi;
//      yim1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f)
//  {
//    float yim1 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i)
//    {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xim1-_a1*yim1;
//      y[i] = yi;
//      yim1 = yi;
//      xim1 = xi;
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    float yim2 = 0.0f;
//    float yim1 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xim1-_a1*yim1-_a2*yim2;
//      y[i] = yi;
//      yim2 = yim1;
//      yim1 = yi;
//      xim1 = xi;
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    float yim2 = 0.0f;
//    float yim1 = 0.0f;
//    float xim2 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b1*xim1+_b2*xim2-_a1*yim1-_a2*yim2;
//      y[i] = yi;
//      yim2 = yim1;
//      yim1 = yi;
//      xim2 = xim1;
//      xim1 = xi;
//    }
//  }

//  // General case.
//  else {
    float yim2 = 0.0f;
    float yim1 = 0.0f;
    float xim2 = 0.0f;
    float xim1 = 0.0f;
    for (int i=0; i<n; ++i) {
      float xi = x[i];
      float yi = _b0 * xi + _b1 * xim1 + _b2 * xim2 - _a1 * yim1 - _a2 * yim2;
      y[i] = yi;
      yim2 = yim1;
      yim1 = yi;
      xim2 = xim1;
      xim1 = xi;
    }
//  }
}

void Recursive2ndOrderFilter::applyReverse(Array1D x, Array1D& y) {
  checkArrays(x,y);
  int n = (int)y.size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    float yip1 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi-_a1*yip1;
//      y[i] = yi;
//      yip1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    float xip1 = 0.0f;
//    float yip1 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xip1-_a1*yip1;
//      y[i] = yi;
//      yip1 = yi;
//      xip1 = xi;
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    float xip1 = 0.0f;
//    float yip1 = 0.0f;
//    float yip2 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xip1-_a1*yip1-_a2*yip2;
//      y[i] = yi;
//      yip2 = yip1;
//      yip1 = yi;
//      xip1 = xi;
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    float xip1 = 0.0f;
//    float xip2 = 0.0f;
//    float yip1 = 0.0f;
//    float yip2 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b1*xip1+_b2*xip2-_a1*yip1-_a2*yip2;
//      y[i] = yi;
//      yip2 = yip1;
//      yip1 = yi;
//      xip2 = xip1;
//      xip1 = xi;
//    }
//  }

//  // General case.
//  else {
    float xip1 = 0.0f;
    float xip2 = 0.0f;
    float yip1 = 0.0f;
    float yip2 = 0.0f;
    for (int i=n-1; i>=0; --i)
    {
      float xi = x[i];
      float yi = _b0 * xi + _b1 * xip1 + _b2 * xip2 - _a1 * yip1 - _a2 * yip2;
      y[i] = yi;
      yip2 = yip1;
      yip1 = yi;
      xip2 = xip1;
      xip1 = xi;
    }
//  }
}

void Recursive2ndOrderFilter::accumulateForward(Array1D x, Array1D& y) {
  checkArrays(x,y);
  int n = (int)y.size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    float yim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b0*xi-_a1*yim1;
//      y[i] += yi;
//      yim1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    float yim1 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xim1-_a1*yim1;
//      y[i] += yi;
//      yim1 = yi;
//      xim1 = xi;
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    float yim2 = 0.0f;
//    float yim1 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xim1-_a1*yim1-_a2*yim2;
//      y[i] += yi;
//      yim2 = yim1;
//      yim1 = yi;
//      xim1 = xi;
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    float yim2 = 0.0f;
//    float yim1 = 0.0f;
//    float xim2 = 0.0f;
//    float xim1 = 0.0f;
//    for (int i=0; i<n; ++i) {
//      float xi = x[i];
//      float yi = _b1*xim1+_b2*xim2-_a1*yim1-_a2*yim2;
//      y[i] += yi;
//      yim2 = yim1;
//      yim1 = yi;
//      xim2 = xim1;
//      xim1 = xi;
//    }
//  }

//  // General case.
//  else {
    float yim2 = 0.0f;
    float yim1 = 0.0f;
    float xim2 = 0.0f;
    float xim1 = 0.0f;
    for (int i=0; i<n; ++i) {
      float xi = x[i];
      float yi = _b0 * xi + _b1 * xim1 + _b2 * xim2 - _a1 * yim1 - _a2 * yim2;
      y[i] += yi;
      yim2 = yim1;
      yim1 = yi;
      xim2 = xim1;
      xim1 = xi;
    }
//  }
}

void Recursive2ndOrderFilter::accumulateReverse(Array1D x, Array1D& y) {
  checkArrays(x,y);
  int n = (int)y.size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    float yip1 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi-_a1*yip1;
//      y[i] += yi;
//      yip1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    float xip1 = 0.0f;
//    float yip1 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xip1-_a1*yip1;
//      y[i] += yi;
//      yip1 = yi;
//      xip1 = xi;
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    float xip1 = 0.0f;
//    float yip1 = 0.0f;
//    float yip2 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b0*xi+_b1*xip1-_a1*yip1-_a2*yip2;
//      y[i] += yi;
//      yip2 = yip1;
//      yip1 = yi;
//      xip1 = xi;
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    float xip1 = 0.0f;
//    float xip2 = 0.0f;
//    float yip1 = 0.0f;
//    float yip2 = 0.0f;
//    for (int i=n-1; i>=0; --i) {
//      float xi = x[i];
//      float yi = _b1*xip1+_b2*xip2-_a1*yip1-_a2*yip2;
//      y[i] += yi;
//      yip2 = yip1;
//      yip1 = yi;
//      xip2 = xip1;
//      xip1 = xi;
//    }
//  }

//  // General case.
//  else {
    float xip1 = 0.0f;
    float xip2 = 0.0f;
    float yip1 = 0.0f;
    float yip2 = 0.0f;
    for (int i=n-1; i>=0; --i) {
      float xi = x[i];
      float yi = _b0 * xi + _b1 * xip1 + _b2 * xip2 - _a1 * yip1 - _a2 * yip2;
      y[i] += yi;
      yip2 = yip1;
      yip1 = yi;
      xip2 = xip1;
      xip1 = xi;
    }
//  }
}

///////////////////////////////////////////////////////////////////////////
// 2-D
void Recursive2ndOrderFilter::apply1Forward(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  for (int i2=0; i2<n2; ++i2) {
    applyForward(x[i2],y[i2]);
  }
}

void Recursive2ndOrderFilter::apply1Reverse(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  for (int i2=0; i2<n2; ++i2) {
    applyReverse(x[i2],y[i2]);
  }
}

void Recursive2ndOrderFilter::apply2Forward(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  int n1 = (int)y[0].size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    std::vector<float> yim1(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& xi = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        yi[i1] = _b0*xi[i1]-_a1*yim1[i1];
//      }
//      yim1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]-
//                            _a1*yim1[i1];
//      }
//      yim1 = yi;
//      std::swap(xim1,xi);
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    std::vector<float> yim2(n1,0.f);
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]-
//                            _a1*yim1[i1]-_a2*yim2[i1];
//      }
//      yim2 = yim1;
//      yim1 = yi;

//      std::swap(xim1,xi);
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    std::vector<float> yim2(n1,0.f);
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> xim2(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b1*xim1[i1]+_b2*xim2[i1]-
//                 _a1*yim1[i1]-_a2*yim2[i1];
//      }
//      yim2 = yim1;
//      yim1 = yi;

//      std::swap(xim2,xim1);
//      std::swap(xim1,xi);
//    }
//  }

//  // General case.
//  else {
    std::vector<float> yim2(n1,0.f);
    std::vector<float> yim1(n1,0.f);
    std::vector<float> xim2(n1,0.f);
    std::vector<float> xim1(n1,0.f);
    std::vector<float> xi(n1,0.f);
    for (int i2=0; i2<n2; ++i2) {
      auto& x2 = x[i2];
      auto& yi = y[i2];
      for (int i1=0; i1<n1; ++i1) {
        xi[i1] = x2[i1];
        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]+_b2*xim2[i1]-
                            _a1*yim1[i1]-_a2*yim2[i1];
      }
      yim2 = yim1;
      yim1 = yi;

      std::swap(xim2,xim1);
      std::swap(xim1,xi);
    }
//  }
}

void Recursive2ndOrderFilter::apply2Reverse(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  int n1 = (int)y[0].size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    std::vector<float> yip1(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& xi = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        yi[i1] = _b0*xi[i1]-_a1*yip1[i1];
//      }
//      yip1 = yi;
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]-
//                            _a1*yip1[i1];
//      }
//      yip1 = yi;

//      std::swap(xip1,xi);
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    std::vector<float> yip2(n1,0.f);
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]-
//                            _a1*yip1[i1]-_a2*yip2[i1];
//      }
//      yip2 = yip1;
//      yip1 = yi;

//      std::swap(xip1,xi);
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    std::vector<float> yip2(n1,0.f);
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> xip2(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& yi = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b1*xip1[i1]+_b2*xip2[i1]-
//                 _a1*yip1[i1]-_a2*yip2[i1];
//      }
//      yip2 = yip1;
//      yip1 = yi;

//      std::swap(xip2,xip1);
//      std::swap(xip1,xi);
//    }
//  }

//  // General case.
//  else {
    std::vector<float> yip2(n1,0.f);
    std::vector<float> yip1(n1,0.f);
    std::vector<float> xip2(n1,0.f);
    std::vector<float> xip1(n1,0.f);
    std::vector<float> xi(n1,0.f);
    for (int i2=n2-1; i2>=0; --i2) {
      auto& x2 = x[i2];
      auto& yi = y[i2];
      for (int i1=0; i1<n1; ++i1) {
        xi[i1] = x2[i1];
        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]+_b2*xip2[i1]-
                            _a1*yip1[i1]-_a2*yip2[i1];
      }
      yip2 = yip1;
      yip1 = yi;

      std::swap(xip2,xip1);
      std::swap(xip1,xi);
    }
//  }
}

void Recursive2ndOrderFilter::accumulate1Forward(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  for (int i2=0; i2<n2; ++i2) {
    accumulateForward(x[i2],y[i2]);
  }
}

void Recursive2ndOrderFilter::accumulate1Reverse(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  for (int i2=0; i2<n2; ++i2) {
    accumulateReverse(x[i2],y[i2]);
  }
}

void Recursive2ndOrderFilter::accumulate2Forward(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  int n1 = (int)y[0].size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& xi = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        yi[i1] = _b0*xi[i1]-_a1*yim1[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yim1,yi);
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]-
//                            _a1*yim1[i1];
//        y2[i1] += yi[i1];
//      }
//      std::swap(yim1,yi);
//      std::swap(xim1,xi);
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    std::vector<float> yim2(n1,0.f);
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]-
//                            _a1*yim1[i1]-_a2*yim2[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yim2,yi);
//      std::swap(xim1,xi);
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    std::vector<float> yim2(n1,0.f);
//    std::vector<float> yim1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xim2(n1,0.f);
//    std::vector<float> xim1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=0; i2<n2; ++i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b1*xim1[i1]+_b2*xim2[i1]-
//                 _a1*yim1[i1]-_a2*yim2[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yim2,yim1);
//      std::swap(yim1,yi);

//      std::swap(xim2,xim1);
//      std::swap(xim1,xi);
//    }
//  }

//  // General case.
//  else {
    std::vector<float> yim2(n1,0.f);
    std::vector<float> yim1(n1,0.f);
    std::vector<float> yi(n1,0.f);
    std::vector<float> xim2(n1,0.f);
    std::vector<float> xim1(n1,0.f);
    std::vector<float> xi(n1,0.f);
    for (int i2=0; i2<n2; ++i2) {
      auto& x2 = x[i2];
      auto& y2 = y[i2];
      for (int i1=0; i1<n1; ++i1) {
        xi[i1] = x2[i1];
        yi[i1] = _b0*xi[i1]+_b1*xim1[i1]+_b2*xim2[i1]-
                            _a1*yim1[i1]-_a2*yim2[i1];
        y2[i1] += yi[i1];
      }

      std::swap(yim2,yim1);
      std::swap(yim1,yi);

      std::swap(xim2,xim1);
      std::swap(xim1,xi);
    }
//  }
}

void Recursive2ndOrderFilter::accumulate2Reverse(Array2D x, Array2D& y) {
  checkArrays(x,y);
  int n2 = (int)y.size();
  int n1 = (int)y[0].size();

//  // Special case b1 = b2 = a2 = 0.
//  if (_b1==0.0f && _b2==0.0f && _a2==0.0f) {
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& xi = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        yi[i1] = _b0*xi[i1]-
//                            _a1*yip1[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yip1,yi);
//    }
//  }

//  // Special case b2 = a2 = 0.
//  else if (_b2==0.0f && _a2==0.0f) {
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]-
//                            _a1*yip1[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yip1,yi);
//      std::swap(xip1,xi);
//    }
//  }

//  // Special case b2 = 0.
//  else if (_b2==0.0f) {
//    std::vector<float> yip2(n1,0.f);
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]-
//                            _a1*yip1[i1]-_a2*yip2[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yip2,yip1);
//      std::swap(yip1,yi);

//      std::swap(xip1,xi);
//    }
//  }

//  // Special case b0 = 0.
//  else if (_b0==0.0f) {
//    std::vector<float> yip2(n1,0.f);
//    std::vector<float> yip1(n1,0.f);
//    std::vector<float> yi(n1,0.f);
//    std::vector<float> xip2(n1,0.f);
//    std::vector<float> xip1(n1,0.f);
//    std::vector<float> xi(n1,0.f);
//    for (int i2=n2-1; i2>=0; --i2) {
//      auto& x2 = x[i2];
//      auto& y2 = y[i2];
//      for (int i1=0; i1<n1; ++i1) {
//        xi[i1] = x2[i1];
//        yi[i1] = _b1*xip1[i1]+_b2*xip2[i1]-
//                 _a1*yip1[i1]-_a2*yip2[i1];
//        y2[i1] += yi[i1];
//      }

//      std::swap(yip2,yip1);
//      std::swap(yip1,yi);

//      std::swap(xip2,xip1);
//      std::swap(xip1,xi);
//    }
//  }

//  // General case.
//  else {
    std::vector<float> yip2(n1,0.f);
    std::vector<float> yip1(n1,0.f);
    std::vector<float> yi(n1,0.f);
    std::vector<float> xip2(n1,0.f);
    std::vector<float> xip1(n1,0.f);
    std::vector<float> xi(n1,0.f);
    for (int i2=n2-1; i2>=0; --i2) {
      auto& x2 = x[i2];
      auto& y2 = y[i2];
      for (int i1=0; i1<n1; ++i1) {
        xi[i1] = x2[i1];
        yi[i1] = _b0*xi[i1]+_b1*xip1[i1]+_b2*xip2[i1]-
                            _a1*yip1[i1]-_a2*yip2[i1];
        y2[i1] += yi[i1];
      }

      std::swap(yip2,yip1);
      std::swap(yip1,yi);

      std::swap(xip2,xip1);
      std::swap(xip1,xi);
    }
//  }
}

///////////////////////////////////////////////////////////////////////////
// 3-D
void Recursive2ndOrderFilter::apply1Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    apply1Forward(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::apply1Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    apply1Reverse(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::apply2Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    apply2Forward(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::apply2Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    apply2Reverse(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::apply3Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  int n2 = (int)y[0].size();
  int n1 = (int)y[0][0].size();
  Array2D xy(n3,Array1D(n1,0.f));
  for (int i2=0; i2<n2; ++i2) {
    get2(i2,x,xy);
    apply2Forward(xy,xy);
    set2(i2,xy,y);
  }
}

void Recursive2ndOrderFilter::apply3Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  int n2 = (int)y[0].size();
  int n1 = (int)y[0][0].size();
  Array2D xy(n3,Array1D(n1,0.f));
  for (int i2=0; i2<n2; ++i2) {
    get2(i2,x,xy);
    apply2Reverse(xy,xy);
    set2(i2,xy,y);
  }
}

void Recursive2ndOrderFilter::accumulate1Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    accumulate1Forward(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::accumulate1Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    accumulate1Reverse(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::accumulate2Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    accumulate2Forward(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::accumulate2Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  for (int i3=0; i3<n3; ++i3) {
    accumulate2Reverse(x[i3],y[i3]);
  }
}

void Recursive2ndOrderFilter::accumulate3Forward(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  int n2 = (int)y[0].size();
  int n1 = (int)y[0][0].size();
  Array2D xy(n3,Array1D(n1,0.f));
  for (int i2=0; i2<n2; ++i2) {
    get2(i2,x,xy);
    apply2Forward(xy,xy);
    acc2(i2,xy,y);
  }
}

void Recursive2ndOrderFilter::accumulate3Reverse(const Array3D& x, Array3D& y) {
  checkArrays(x,y);
  int n3 = (int)y.size();
  int n2 = (int)y[0].size();
  int n1 = (int)y[0][0].size();
  Array2D xy(n3,Array1D(n1,0.f));
  for (int i2=0; i2<n2; ++i2) {
    get2(i2,x,xy);
    apply2Reverse(xy,xy);
    acc2(i2,xy,y);
  }
}

void Recursive2ndOrderFilter::checkArrays(const Array1D& x, const Array1D& y)
{
//    Check.argument((int)x.size()==(int)y.size(),"(int)x.size()==(int)y.size()");
}

void Recursive2ndOrderFilter::checkArrays(const Array2D& x, const Array2D& y)
{
//    Check.argument((int)x.size()==(int)y.size(),"(int)x.size()==(int)y.size()");
//    Check.argument(x[0].size()==(int)y[0].size(),"x[0].size()==(int)y[0].size()");
//    Check.argument(isRegular(x),"x is regular");
//    Check.argument(isRegular(y),"y is regular");
}

void Recursive2ndOrderFilter::checkArrays(const Array3D& x, const Array3D& y)
{
//    Check.argument((int)x.size()==(int)y.size(),"(int)x.size()==(int)y.size()");
//    Check.argument(x[0].size()==(int)y[0].size(),"x[0].size()==(int)y[0].size()");
//    Check.argument(x[0][0].size()==(int)y[0][0].size(),
//      "x[0][0].size()==(int)y[0][0].size()");
//    Check.argument(isRegular(x),"x is regular");
//    Check.argument(isRegular(y),"y is regular");
}

void Recursive2ndOrderFilter::get2(int i2, const Array3D& x, Array2D& x2)
{
    int n3 = (int)x2.size();
    int n1 = (int)x2[0].size();
    for (int i3=0; i3<n3; ++i3) {
      auto& x32 = x[i3][i2];
      auto& x23 = x2[i3];
      for (int i1=0; i1<n1; ++i1) {
        x23[i1] = x32[i1];
      }
    }
}

void Recursive2ndOrderFilter::set2(int i2, const Array2D& x2, Array3D& x)
{
    int n3 = (int)x2.size();
    int n1 = (int)x2[0].size();
    for (int i3=0; i3<n3; ++i3) {
      auto& x32 = x[i3][i2];
      auto& x23 = x2[i3];
      for (int i1=0; i1<n1; ++i1) {
        x32[i1] = x23[i1];
      }
    }
}

void Recursive2ndOrderFilter::acc2(int i2, const Array2D& x2, Array3D& x)
{
    int n3 = (int)x2.size();
    int n1 = (int)x2[0].size();
    for (int i3=0; i3<n3; ++i3) {
      auto& x32 = x[i3][i2];
      auto& x23 = x2[i3];
      for (int i1=0; i1<n1; ++i1) {
        x32[i1] += x23[i1];
      }
    }
}
