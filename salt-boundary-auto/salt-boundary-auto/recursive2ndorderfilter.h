/****************************************************************************
Copyright 2005, Colorado School of Mines and others.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
****************************************************************************/

#ifndef RECURSIVE2NDORDERFILTER_H
#define RECURSIVE2NDORDERFILTER_H

/**
 * Recursive 2nd-order filter. This filter solves a linear, 2nd-order,
 * constant-coefficient difference equation in either forward or reverse
 * directions along any dimension of a 1-D, 2-D, or 3-D array.
 * <p>
 * Application of the filter in the forward direction computes
 * <pre>
 * y[i] = b0*x[i]+b1*x[i-1]+b2*x[i-2]-a1*y[i-1]-a2*y[i-2],
 * </pre>
 * for i = 0, 1, 2, ..., n-1, where x[i] = y[i] = 0 for i&lt;0.
 * Application of the filter in the reverse direction computes
 * <pre>
 * y[i] = b0*x[i]+b1*x[i+1]+b2*x[i+2]-a1*y[i+1]-a2*y[i+2],
 * </pre>
 * for i = n-1, n-2, ..., 0, where x[i] = y[i] = 0 for i&gt;=n.
 * @author Dave Hale, Colorado School of Mines
 * @version 2005.11.22
 */

/**
 * Conversion from Java to C++ by Suellen Motta, PUC-Rio in 2018.09.10
 */

#include <vector>
#include <complex>

using Array1D = std::vector<float>;
using Array2D = std::vector< std::vector<float> >;
using Array3D = std::vector< std::vector< std::vector<float> > >;

class Recursive2ndOrderFilter
{
public:
    /**
     * Default empty constructor. All coefficients equals zero.
     */
    Recursive2ndOrderFilter();

    /**
    * Constructs a recursive 2nd-order filter with specified coefficients.
    * If some of the coefficients are zero, the filter may be of only 1st
    * or even 0th order.
    * @param b0 a filter coefficient.
    * @param b1 a filter coefficient.
    * @param b2 a filter coefficient.
    * @param a1 a filter coefficient.
    * @param a2 a filter coefficient.
    */
    Recursive2ndOrderFilter(float b0, float b1, float b2, float a1, float a2);

    /**
     * Constructs a recursive 2nd-order filter from pole, zero, and gain.
     * This filter is actually a 1st-order filter, because it has only
     * one (real) pole and zero.
     * @param pole the pole.
     * @param zero the zero.
     * @param gain the filter gain.
     */
    Recursive2ndOrderFilter(double pole, double zero, double gain);

    /**
     * Constructs a recursive 2nd-order filter from poles, zeros, and gain.
     * The poles must be real or conjugate pairs; likewise for the zeros.
     * @param pole1 the 1st pole.
     * @param pole2 the 2nd pole.
     * @param zero1 the 1st zero.
     * @param zero2 the 2nd zero.
     * @param gain the filter gain.
     */
    Recursive2ndOrderFilter(
      std::complex<float> pole1, std::complex<float> pole2,
      std::complex<float> zero1, std::complex<float> zero2,
      double gain);

    /**
     * Applies this filter in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must have equal
     * lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void applyForward(Array1D x, Array1D& y);

    /**
     * Applies this filter in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must have equal
     * lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void applyReverse(Array1D x, Array1D& y);

    /**
     * Applies this filter in the forward direction, accumulating the output.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must have equal
     * lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulateForward(Array1D x, Array1D& y);

    /**
     * Applies this filter in the reverse direction, accumulating the output.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must have equal
     * lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulateReverse(Array1D x, Array1D& y);

    ///////////////////////////////////////////////////////////////////////////
    // 2-D

    /**
     * Applies this filter in 1st dimension in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply1Forward(Array2D x, Array2D& y);

    /**
     * Applies this filter in 1st dimension in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply1Reverse(Array2D x, Array2D& y);

    /**
     * Applies this filter in 2nd dimension in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply2Forward(Array2D x, Array2D& y);

    /**
     * Applies this filter in 2nd dimension in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply2Reverse(Array2D x, Array2D& y);

    /**
     * Accumulates output in 1st dimension in the forward direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate1Forward(Array2D x, Array2D& y);

    /**
     * Accumulates output in 1st dimension in the reverse direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate1Reverse(Array2D x, Array2D& y);

    /**
     * Accumulates output in 2nd dimension in the forward direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate2Forward(Array2D x, Array2D& y);

    /**
    /**
     * Accumulates output in 2nd dimension in the reverse direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate2Reverse(Array2D x, Array2D& y);

    ///////////////////////////////////////////////////////////////////////////
    // 3-D

    /**
     * Applies this filter in 1st dimension in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply1Forward(const Array3D& x, Array3D& y);

    /**
     * Applies this filter in 1st dimension in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply1Reverse(const Array3D& x, Array3D& y);

    /**
     * Applies this filter in 2nd dimension in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply2Forward(const Array3D& x, Array3D& y);

    /**
     * Applies this filter in 2nd dimension in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply2Reverse(const Array3D& x, Array3D& y);

    /**
     * Applies this filter in 3rd dimension in the forward direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply3Forward(const Array3D& x, Array3D& y);

    /**
     * Applies this filter in 3rd dimension in the reverse direction.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void apply3Reverse(const Array3D& x, Array3D& y);

    /**
     * Accumulates output in 1st dimension in the forward direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate1Forward(const Array3D& x, Array3D& y);

    /**
     * Accumulates output in 1st dimension in the reverse direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate1Reverse(const Array3D& x, Array3D& y);

    /**
     * Accumulates output in 2nd dimension in the forward direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate2Forward(const Array3D& x, Array3D& y);

    /**
     * Accumulates output in 2nd dimension in the reverse direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate2Reverse(const Array3D& x, Array3D& y);

    /**
     * Accumulates output in 3rd dimension in the forward direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate3Forward(const Array3D& x, Array3D& y);

    /**
    /**
     * Accumulates output in 3rd dimension in the reverse direction.
     * This method filters the input, and adds the result to the output; it
     * is most useful when implementing parallel forms of recursive filters.
     * <p>
     * Input and output arrays may be the same array, but must be
     * regular and have equal lengths.
     * @param x the input array.
     * @param y the output array.
     */
    void accumulate3Reverse(const Array3D& x, Array3D& y);

private:
    float _b0,_b1,_b2,_a1,_a2; // filter coefficients

    void checkArrays(const Array1D& x, const Array1D& y);

    void checkArrays(const Array2D& x, const Array2D& y);

    void checkArrays(const Array3D& x, const Array3D& y);

    void get2(int i2, const Array3D& x, Array2D& x2);

    void set2(int i2, const Array2D& x2, Array3D& x);

    void acc2(int i2, const Array2D& x2, Array3D& x);
};

#endif // RECURSIVE2NDORDERFILTER_H


