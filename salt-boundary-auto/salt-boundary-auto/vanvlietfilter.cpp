#include "vanvlietfilter.h"

std::vector< std::vector< std::complex<double> > > VanVlietFilter::POLES = {
{{ 1.12075, 1.27788},
 { 1.12075,-1.27788},
 { 1.76952, 0.46611},
 { 1.76952,-0.46611}},
{{ 1.04185, 1.24034},
 { 1.04185,-1.24034},
 { 1.69747, 0.44790},
 { 1.69747,-0.44790}},
{{ 0.94570, 1.21064},
 { 0.94570,-1.21064},
 { 1.60161, 0.42647},
 { 1.60161,-0.42647}}
};

std::complex<double> inverse(std::complex<double> x)
{
    return std::conj(x)/std::norm(x);
}

VanVlietFilter::VanVlietFilter(double sigma)
{
    makeG(sigma);
}


void VanVlietFilter::applyN(int nd, Array1D x, Array1D& y)
{
//    checkArrays(x,y);
//    if (sameArrays(x,y))
//      x = copy(x);
//   if(x.data()==y.data())
//   {
//       Array1D otherX = x;

//       _g[nd][0][0].applyForward(otherX,y);
//       _g[nd][0][1].accumulateReverse(otherX,y);
//       _g[nd][1][0].accumulateForward(otherX,y);
//       _g[nd][1][1].accumulateReverse(otherX,y);
//   }

//   else
//   {
       _g[nd][0][0].applyForward(x,y);
       _g[nd][0][1].accumulateReverse(x,y);
       _g[nd][1][0].accumulateForward(x,y);
       _g[nd][1][1].accumulateReverse(x,y);
//   }

}

void VanVlietFilter::applyXN(int nd, Array2D x, Array2D& y)
{
//    checkArrays(x,y);
//    if (sameArrays(x,y))
//      x = copy(x);
//    if(x.data()==y.data())
//    {
//        Array2D otherX = x;

//        _g[nd][0][0].apply2Forward(otherX,y);
//        _g[nd][0][1].accumulate2Reverse(otherX,y);
//        _g[nd][1][0].accumulate2Forward(otherX,y);
//        _g[nd][1][1].accumulate2Reverse(otherX,y);
//    }
//    else
//    {
        _g[nd][0][0].apply2Forward(x,y);
        _g[nd][0][1].accumulate2Reverse(x,y);
        _g[nd][1][0].accumulate2Forward(x,y);
        _g[nd][1][1].accumulate2Reverse(x,y);
//    }

}


void VanVlietFilter::makeG(double sigma) {

  // Loop over filters for 0th, 1st, and 2nd derivatives.
  for (int nd=0; nd<3; ++nd) {

    // Adjust the poles for the scale factor q.
    auto poles = adjustPoles(sigma,POLES[nd]);

    // Filter gain.
    double gain = computeGain(poles);
    double gg = gain*gain;

    // Residues.
    std::complex<double> d0(poles[0]);
    std::complex<double> d1(poles[2]);

    auto e0 = inverse(d0);
    auto e1 = inverse(d1);

    auto g0 = gr(nd,d0,poles,gg);
    auto g1 = gr(nd,d1,poles,gg);

    // Coefficients for 2nd-order recursive filters.
    auto a10 = d0.real()*(-2.0);
    auto a11 = d1.real()*(-2.0);
    auto a20 = std::norm(d0);
    auto a21 = std::norm(d1);

    double b00,b01,b10,b11,b20,b21;

    // 0th- and 2nd-derivative filters are symmetric.
    if (nd==0 || nd==2) {
      b10 = g0.imag()/e0.imag();
      b11 = g1.imag()/e1.imag();
      b00 = g0.real()-b10*e0.real();
      b01 = g1.real()-b11*e1.real();
      b20 = 0.0;
      b21 = 0.0;
      _g[nd][0][0] = makeFilter(b00,b10,b20,a10,a20);
      _g[nd][1][0] = makeFilter(b01,b11,b21,a11,a21);
      b20 -= b00*a20;
      b21 -= b01*a21;
      b10 -= b00*a10;
      b11 -= b01*a11;
      b00 = 0.0;
      b01 = 0.0;
      _g[nd][0][1] = makeFilter(b00,b10,b20,a10,a20);
      _g[nd][1][1] = makeFilter(b01,b11,b21,a11,a21);

    // 1st-derivative filter is anti-symmetric.
    } else if (nd==1) {
      b20 = g0.imag()/e0.imag();
      b21 = g1.imag()/e1.imag();
      b10 = g0.real()-b20*e0.real();
      b11 = g1.real()-b21*e1.real();
      b00 = 0.0;
      b01 = 0.0;
      _g[nd][0][0] = makeFilter(b00,b10,b20,a10,a20);
      _g[nd][1][0] = makeFilter(b01,b11,b21,a11,a21);
      b20 = -b20;
      b21 = -b21;
      b10 = -b10;
      b11 = -b11;
      b00 = 0.0;
      b01 = 0.0;
      _g[nd][0][1] = makeFilter(b00,b10,b20,a10,a20);
      _g[nd][1][1] = makeFilter(b01,b11,b21,a11,a21);
    }
  }
}

Recursive2ndOrderFilter VanVlietFilter::makeFilter(
  double b0, double b1, double b2, double a1, double a2)
{
  return Recursive2ndOrderFilter(
    (float)b0,(float)b1,(float)b2,(float)a1,(float)a2);
}

/**
 * Evaluates residue of G(z) for the n'th derivative and j'th pole.
 */
std::complex<double> VanVlietFilter::gr(int nd, std::complex<double> polej,
                        const std::vector< std::complex<double> >& poles, double gain)
{
  auto pj = polej;
  auto qj = inverse(pj);
  std::complex<double> c1(1,0);
  std::complex<double> gz(c1);
  if (nd==1) {
    gz *= (c1 - qj);
    gz *= (c1 + pj);
    gz *= pj;
    gz *= 0.5;
  } else if (nd==2) {
    gz *= (c1 - qj);
    gz *= (c1 - pj);
    gz *= (-1.0);
  }

  std::complex<double> gp(c1);

  int np = (int)poles.size();
  for (int ip=0; ip<np; ++ip) {
    auto& pi = poles[ip];
    if (pi != pj && pi != std::conj(pj))
      gp *= (c1 - (pi * qj));
    gp *= (c1 - (pi * pj));
  }
  return (gz / gp) * gain;
}

std::vector< std::complex<double> > VanVlietFilter::adjustPoles(double sigma,
                                                const std::vector< std::complex<double> > poles)
{
  // Simple search for scale factor q that yields the desired sigma.
  double q = sigma;
  double s = computeSigma(q,poles);
  for (int iter=0; std::abs(sigma-s)>sigma*1.0e-8; ++iter) {
    //System.out.println("sigma="+sigma+" s="+s+" q="+q);
//      Check.state(iter<100,"number of iterations less than 100");
    s = computeSigma(q,poles);
    q *= sigma/s;
  }

  // Adjust poles.
  int npole = (int)poles.size();
  std::vector< std::complex<double> > apoles(npole);
  for (int ipole=0; ipole<npole; ++ipole) {
    auto& pi = poles[ipole];
    double a = std::pow(std::abs(pi),2.0/q);
    double t = std::atan2(pi.imag(),pi.real())*2.0/q;
    apoles[ipole] = inverse(std::polar(a,t));
  }
  return apoles;
}

double VanVlietFilter::computeGain(const std::vector< std::complex<double> >& poles)
{
  int npole = (int)poles.size();

  std::complex<double> c1(1,0);
  std::complex<double> cg(c1);

  for (int ipole=0; ipole<npole; ++ipole) {
    cg *= (c1 - poles[ipole]);
  }
  return cg.real();
}

double VanVlietFilter::computeSigma(double sigma, const std::vector< std::complex<double> >& poles)
{
  int npole = (int)poles.size();
  double q = sigma/2.0;
  std::complex<double> c1(1,0);
  std::complex<double> cs(0,0);

  for (int ipole=0; ipole<npole; ++ipole) {
    auto& pi = poles[ipole];
    double a = std::pow(std::abs(pi),-1.0/q);
    double t = atan2(pi.imag(),pi.real())/q;
    auto b = std::polar(a,t);
    auto c = c1 - b;
    auto d = c * c;
    cs += ((b * 2.0)/d);
  }

  return std::sqrt(cs.real());
}
