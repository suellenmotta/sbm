#include "saltboundaryautoextractor.h"

#include <complex>


SaltBoundaryAutoExtractor::SaltBoundaryAutoExtractor(Image *amplitudeImage)
    : ampImage(amplitudeImage)
{

}

SaltBoundaryAutoExtractor::~SaltBoundaryAutoExtractor()
{
//    delete imgG11;
//    delete imgG12;
//    delete imgG22;
}

void SaltBoundaryAutoExtractor::run()
{
    computeGradient();
}

Image *SaltBoundaryAutoExtractor::getPlanarityImage()
{
    return planarityImage;
}

void SaltBoundaryAutoExtractor::computeGradient()
{
    double sigma = 60;

    RecursiveGaussianFilter _rgfGradient1(1.0);
    RecursiveGaussianFilter& _rgfGradient2 = _rgfGradient1;

    //Input image
    Array2D x = imageToArray2D(ampImage);

    // Gradient.
    int n1 = x[0].size();
    int n2 = x.size();

    Array2D g1(n2,Array1D(n1));
    Array2D g2(n2,Array1D(n1));
    _rgfGradient1.apply10(x,g1);
    _rgfGradient2.apply01(x,g2);

    xGradImage = array2DToImage(g1);
    yGradImage = array2DToImage(g2);

    // Gradient products.
    auto g11 = g1;
    auto g22 = g2;
    Array2D g12(n2,Array1D(n1));
    for (int i2=0; i2<n2; ++i2) {
      for (int i1=0; i1<n1; ++i1) {
        float g1i = g1[i2][i1];
        float g2i = g2[i2][i1];
        g11[i2][i1] = g1i*g1i;
        g22[i2][i1] = g2i*g2i;
        g12[i2][i1] = g1i*g2i;
      }
    }

    RecursiveGaussianFilter _rgfSmoother1(sigma);
    RecursiveGaussianFilter& _rgfSmoother2 = _rgfGradient1;

    // Smoothed gradient products comprise the structure tensor.
    Array2D h(n2,Array1D(n1));

    _rgfSmoother1.apply0X(g11,h);
    _rgfSmoother2.applyX0(h,g11);

    _rgfSmoother1.apply0X(g22,h);
    _rgfSmoother2.applyX0(h,g22);

    _rgfSmoother1.apply0X(g12,h);
    _rgfSmoother2.applyX0(h,g12);

//    imgG11 = array2DToImage(g11);
//    imgG22 = array2DToImage(g22);
//    imgG12 = array2DToImage(g12);

//    // Compute eigenvectors, eigenvalues, and outputs that depend on them.
    float a[2][2];
    float z[2][2];
    float e[2];

    planarityImage = new Image(ampImage->width,ampImage->height);

    for (int i2=0; i2<n2; ++i2) {
      for (int i1=0; i1<n1; ++i1) {
        a[0][0] = g11[i2][i1];
        a[0][1] = g12[i2][i1];
        a[1][0] = g12[i2][i1];
        a[1][1] = g22[i2][i1];

        solveSymmetric22(a,z,e);

        float u1i = z[0][0];
        float u2i = z[0][1];
        if (u1i<0.0f) {
          u1i = -u1i;
          u2i = -u2i;
        }
        float v1i = -u2i;
        float v2i = u1i;
        float eui = e[0];
        float evi = e[1];

        planarityImage->data[planarityImage->index(i1,i2)] = (eui-evi)/eui;

        //How to save the results
        //Last one is linearity
//        if (evi<0.0f) evi = 0.0f;
//        if (eui<evi) eui = evi;
//        if (theta!=null) theta[i2][i1] = asin(u2i);
//        if (u1!=null) u1[i2][i1] = u1i;
//        if (u2!=null) u2[i2][i1] = u2i;
//        if (v1!=null) v1[i2][i1] = v1i;
//        if (v2!=null) v2[i2][i1] = v2i;
//        if (eu!=null) eu[i2][i1] = eui;
//        if (ev!=null) ev[i2][i1] = evi;
//        if (el!=null) el[i2][i1] =
      }
    }
}

Image *SaltBoundaryAutoExtractor::array2DToImage(const Array2D &array)
{
    Image* image = new Image(array[0].size(),array.size());

    int pos = 0;
    for(int i = 0; i < image->height; ++i)
    {
        for(int j = 0; j < image->width; ++j)
        {
            image->data[pos++] = array[i][j];
        }
    }

    return image;
}

Array2D SaltBoundaryAutoExtractor::imageToArray2D(Image *image)
{
    Array2D array(image->height,Array1D(image->width));
    int pos = 0;
    for(int i = 0; i < image->height; ++i)
    {
        for(int j = 0; j < image->width; ++j)
        {
            array[i][j] = image->data[pos++];
        }
    }
    return array;
}

void SaltBoundaryAutoExtractor::solveSymmetric22(float a[2][2], float v[2][2], float d[2])
{
    // Copy matrix to local variables.
    float a00 = a[0][0];
    float a01 = a[0][1],  a11 = a[1][1];

    // Initial eigenvectors.
    float v00 = 1.0f,     v01 = 0.0f;
    float v10 = 0.0f,     v11 = 1.0f;

    // If off-diagonal element is non-zero, zero it with a Jacobi rotation.
    if (a01!=0.0f) {
      float tiny = 0.1f*std::sqrt(FLT_EPSILON); // avoid overflow in r*r below
      float c,r,s,t,u,vpr,vqr;
      u = a11-a00;
      if (abs(a01)<tiny*abs(u)) {
        t = a01/u;
      } else {
        r = 0.5f*u/a01;
        t = (r>=0.0f)?1.0f/(r+std::sqrt(1.0f+r*r)):1.0f/(r-std::sqrt(1.0f+r*r));
      }
      c = 1.0f/std::sqrt(1.0f+t*t);
      s = t*c;
      u = s/(1.0f+c);
      r = t*a01;
      a00 -= r;
      a11 += r;
      //a01 = 0.0f;
      vpr = v00;
      vqr = v10;
      v00 = vpr-s*(vqr+vpr*u);
      v10 = vqr+s*(vpr-vqr*u);
      vpr = v01;
      vqr = v11;
      v01 = vpr-s*(vqr+vpr*u);
      v11 = vqr+s*(vpr-vqr*u);
    }

    // Copy eigenvalues and eigenvectors to output arrays.
    d[0] = a00;
    d[1] = a11;
    v[0][0] = v00;  v[0][1] = v01;
    v[1][0] = v10;  v[1][1] = v11;

    // Sort eigenvalues (and eigenvectors) in descending order.
    if (d[0]<d[1]) {
      std::swap(d[1],d[0]);
      std::swap(v[1][0],v[0][0]);
      std::swap(v[1][1],v[0][1]);
    }
  }
