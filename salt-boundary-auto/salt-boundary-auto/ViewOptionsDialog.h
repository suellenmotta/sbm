#ifndef VIEWOPTIONSDIALOG_H
#define VIEWOPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class ViewOptionsDialog;
}

class ViewOptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ViewOptionsDialog(QWidget *parent = 0);
    ~ViewOptionsDialog();

    void setupMinMaxPossibleValues(double minValue, double maxValue);
    void setupMinMaxValues(double minValue, double maxValue);

signals:
    void minValueChanged(double value);
    void maxValueChanged(double value);

private slots:
    void on_minValueSpinBox_valueChanged(double value);
    void on_maxValueSpinBox_valueChanged(double value);

private:
    Ui::ViewOptionsDialog *ui;
};

#endif // VIEWOPTIONSDIALOG_H
