#ifndef SALTBOUNDARYAUTOEXTRACTOR_H
#define SALTBOUNDARYAUTOEXTRACTOR_H

#include "Image.h"
#include "recursivegaussianfilter.h"

class SaltBoundaryAutoExtractor
{
public:
    SaltBoundaryAutoExtractor(Image* amplitudeImage);

    ~SaltBoundaryAutoExtractor();

    void run();

    Image* getPlanarityImage();

    Image* getGradientXImage() { return xGradImage; }
    Image* getGradientYImage() { return yGradImage; }

private:
    void computeGradient();
//    void computeTensor();
//    void decomposeTensor();
//    void computeLinearityImage();
//    void thinSaltLikelihood();
//    void indicatorFunction();
//    void marchingCubes();

    Image* array2DToImage(const Array2D& array);
    Array2D imageToArray2D(Image* image);

    /**
     * Computes eigenvalues and eigenvectors for a symmetric 2x2 matrix A.
     * If the eigenvectors are placed in columns in a matrix V, and the
     * eigenvalues are placed in corresponding columns of a diagonal
     * matrix D, then AV = VD.
     * @param a the symmetric matrix A.
     * @param v the array of eigenvectors v[0] and v[1].
     * @param d the array of eigenvalues d[0] and d[1].
     */
    void solveSymmetric22(float a[2][2], float v[2][2], float d[2]);

    Image* ampImage;
    Image* planarityImage;

    Image* xGradImage;
    Image* yGradImage;

    Image* imgG11;
    Image* imgG22;
    Image* imgG12;
};

#endif // SALTBOUNDARYAUTOEXTRACTOR_H
