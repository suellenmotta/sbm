#include "Image.h"

#include <fstream>
#include <iostream>


Image::Image()
    : width(0)
    , height(0)
    , data(nullptr)
{

}

Image::Image(int width, int height)
    : width(width)
    , height(height)
    , data(new float[width*height])
{

}

Image::Image(int width, int height, float *data)
    : width(width)
    , height(height)
    , data(data)
{

}

Image::~Image()
{
    delete[] data;
}

int Image::index(int x, int y)
{
    return width*y + x;
}

Image* readImage(const std::string &filePath, int width, int height, bool rotate)
{
    std::ifstream input;
    input.open(filePath,std::ios::binary);

    if(input.is_open())
    {
        auto size = width*height;

        float* buffer = nullptr;
        try
        {
            buffer = new float[size];
        }
        catch (const std::bad_alloc& e)
        {
            std::cerr << "Allocation failed: " << e.what() << std::endl;
            return nullptr;
        }

        try
        {
            input.read(reinterpret_cast<char*>(&buffer[0]),size*sizeof(float));
            input.close();
            std::cerr << "Image read successfully.";
        }
        catch( const std::ios_base::failure& e)
        {
            std::cerr << "Read file failed: " << e.what() << std::endl;
            delete[] buffer;
            input.close();
            return nullptr;
        }

        if(rotate)
        {
            Image* newImage = nullptr;

            try
            {
                newImage = new Image(height,width);
            }
            catch (const std::bad_alloc& e)
            {
                delete[] buffer;
                std::cerr << "Allocation failed: " << e.what() << std::endl;
                return nullptr;
            }

            Image oldImage(width,height,buffer);

            int pos = 0;
            for(int y = 0; y < width; ++y)
            {
                for(int x = 0; x < height; ++x)
                {
                    newImage->data[pos++] = oldImage.data[oldImage.index(y,x)];
                }
            }

            return newImage;
        }

        else
        {
            return new Image(width,height,buffer);
        }

    }

    return nullptr;
}


