#ifndef IMAGE_H
#define IMAGE_H

#include <string>

class Image
{
public:
    Image();
    Image(int width, int height);
    Image(int width, int height, float* data);
    ~Image();

    int index(int x, int y);

    int width, height;
    float* data;
};


Image* readImage(const std::string& filePath, int width, int height, bool rotate = true);

#endif
