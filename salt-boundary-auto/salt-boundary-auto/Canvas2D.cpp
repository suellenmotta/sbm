#include "Canvas2D.h"

#include <QMessageBox>
#include <QMouseEvent>

#include <algorithm>

struct vertex
{
    glm::vec3 pos;
    glm::vec2 texCoord;
};

const char* vertexShaderSource = R"(
    #version 400 core

    layout( location = 0 ) in vec3 vertexPos;
    uniform mat4 transformMatrix;

    void main()
    {
        gl_Position = transformMatrix *vec4( vertexPos, 1 );
    }
)";


const char* fragmentShaderSource = R"(
    #version 400 core

    uniform vec3 color;
    out vec3 finalColor;

    void main()
    {
        finalColor = color;
    }
)";

const char* textVertShaderSource = R"(
    #version 400 core

    layout( location = 0 ) in vec3 vertexPos;
    layout( location = 1 ) in vec2 vertexTex;

    uniform mat4 mvp;

    out vec2 uv;

    void main()
    {
        gl_Position = mvp * vec4( vertexPos, 1 );

        uv = vertexTex;
    }
)";

const char* textFragShaderSource = R"(
    #version 400 core

    in vec2 uv;
    uniform sampler2D valueSampler;
    uniform sampler1D lutSampler;
    out vec3 color;

    uniform uint numColors;
    uniform float colorMarkers[6];

    uniform float minValue;
    uniform float maxValue;

    void main()
    {
       float value = texture( valueSampler, uv ).r;
       float u = clamp((value-minValue)*(1/(maxValue-minValue)),0,1);
       color = vec3(u,u,u); return;
       /*if( u > colorMarkers[5] ) u = colorMarkers[5];

       uint maxIdx=0;
       while(u>colorMarkers[maxIdx]) maxIdx++;

       float p = 1.0f / 5.0f;
       if(maxIdx < 1) u = 0.0f;
       else
       {
           uint minIdx = maxIdx-1;

           float f = p / (colorMarkers[maxIdx]-colorMarkers[minIdx]);

           u = ((u-colorMarkers[minIdx])*f) + (minIdx*p);
       }*/

       color = vec3(1,0,0);//texture(lutSampler,u).rgb;
    }
)";

Canvas2D::Canvas2D(QWidget* parent)
    : QOpenGLWidget(parent)
    , image(nullptr)
    , program(0)
    , previewing(false)
    , dragIdx(-1)
{
    setMouseTracking(true);
}

Canvas2D::~Canvas2D()
{
}

void Canvas2D::setImage(Image *image)
{
    this->image = image;

    if(image != nullptr)
    {
        float minX, minY, maxX, maxY;

        if(image->width > image->height)
        {
            minX = 0.0f;
            maxX = width();

            auto f = (float) image->height / image->width;
            auto h = height() * 0.5f;
            auto y = (f * width() * 0.5);

            minY = h - y;
            maxY = h + y;
        }
        else
        {
            minY = 0.0f;
            maxY = height();

            auto f = (float) image->width / image->height;
            auto w = width() * 0.5f;
            auto x = (f * height() * 0.5);

            minX = w - x;
            maxX = w + x;
        }

        slicePoints =
        { {minX,minY,0}, {maxX,minY,0}, {minX,maxY,0}, {maxX,maxY,0} };

        for(auto& p : slicePoints)
        {
            p = p.unproject(view, proj, QRect(0,0,width(),height()));
            p.setZ(0.f);
        }

        std::vector< vertex > vbo;
        vbo.reserve( 4 );

        vbo.push_back({{slicePoints[0].x(),slicePoints[0].y(),0}, {0.0f,1.0f}});
        vbo.push_back({{slicePoints[1].x(),slicePoints[1].y(),0}, {1.0f,1.0f}});
        vbo.push_back({{slicePoints[2].x(),slicePoints[2].y(),0}, {0.0f,0.0f}});
        vbo.push_back({{slicePoints[3].x(),slicePoints[3].y(),0}, {1.0f,0.0f}});

        glBindBuffer(GL_ARRAY_BUFFER, sliceVBO);
        glBufferData(GL_ARRAY_BUFFER, vbo.size()*sizeof(vertex), &vbo[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, sliceTexture);
        glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, image->width, image->height,
                      0, GL_RED, GL_FLOAT, image->data );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glGenerateMipmap(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,0);
    }
}

Image *Canvas2D::getImage()
{
    return image;
}

void Canvas2D::startPicking()
{
    points.clear();

    QVector3D firstPoint( 0, 0, 0 );
    firstPoint = firstPoint.unproject( view, proj, QRect(0,0,width(),height()));
    firstPoint.setZ(0.f);
    points.push_back( firstPoint );
    previewing = true;

    update();
}

void Canvas2D::stopPicking()
{
    previewing = false;
    points.back() = points[0];
    update();
}

void Canvas2D::setMinValue(float value)
{
    minValue = value;
}

void Canvas2D::setMaxValue(float value)
{
    maxValue = value;
}

float Canvas2D::getMinValue()
{
    return minValue;
}

float Canvas2D::getMaxValue()
{
    return maxValue;
}

float Canvas2D::getMinPossibleValue()
{
    return *(std::min_element(image->data,image->data+image->width*image->height));
}

float Canvas2D::getMaxPossibleValue()
{
    return *(std::max_element(image->data,image->data+image->width*image->height));
}

void Canvas2D::initializeGL()
{
    initializeOpenGLFunctions();

    makeCurrent();

    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_LINE_SMOOTH);

    glViewport(0,0,width(),height());

    glLineWidth(1.0f);
    glPointSize(8.0f);

    sliceProgram = new QOpenGLShaderProgram;
    sliceProgram->addShaderFromSourceCode(QOpenGLShader::Vertex, textVertShaderSource);
    sliceProgram->addShaderFromSourceCode(QOpenGLShader::Fragment, textFragShaderSource);
    sliceProgram->link();
    if (!sliceProgram->isLinked())
    {
        QMessageBox::critical(nullptr,"Erro","Não foi possível compilar/linkar os shaders.");
        exit(0);
    }

    createVBOs();

    sliceProgram->bind();
    slicePointsBuffer.create();

    program = new QOpenGLShaderProgram;
    program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    program->link();
    if (!program->isLinked())
    {
        QMessageBox::critical(nullptr,"Erro","Não foi possível compilar/linkar os shaders.");
        exit(0);
    }

    program->bind();

    pointsBuffer.create();
    curveBuffer.create();

    proj.ortho(0.f,width(),0.f,height(),-1.f,1.0f);
    program->setUniformValue("transformMatrix", proj);
}

void Canvas2D::paintGL()
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    if(image != nullptr)
    {
        glDisable(GL_CULL_FACE);

        sliceProgram->bind();
        sliceProgram->setUniformValue("mvp", proj);

        glBindVertexArray(VAO);
        glBindVertexBuffer(0,sliceVBO,0,sizeof(vertex));
        glBindTexture(GL_TEXTURE_2D, sliceTexture);
        sliceProgram->setUniformValue("minValue",minValue);
        sliceProgram->setUniformValue("maxValue",maxValue);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
        glBindVertexArray(0);
    }

    if(!points.empty() && pointsBuffer.isCreated())
    {
        glDisable(GL_DEPTH_TEST);
        program->bind();
        program->setUniformValue("transformMatrix", proj);

        //Aloca e desenha o polígono de controle
        pointsBuffer.bind();
        pointsBuffer.allocate( &points[0], (int)points.size()*sizeof(QVector3D) );
        program->enableAttributeArray(0);
        program->setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(QVector3D));

        program->setUniformValue("color", QVector3D(1,0,0));

        glDrawArrays(GL_LINE_STRIP, 0, (int)points.size());

        //Aloca e desenha a curva de bezier
        if( !curvePoints.empty() && curveBuffer.isCreated() )
        {
            curveBuffer.bind();
            curveBuffer.allocate( &curvePoints[0], (int)curvePoints.size()*sizeof(QVector3D) );
            program->enableAttributeArray(0);
            program->setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(QVector3D));
            program->setUniformValue("color", QVector3D(1,1,0));
            glDrawArrays(GL_LINE_STRIP, 0, (int)curvePoints.size());
        }

        //Desenha os pontos de controle, com highlight no selecionado, se houver
        pointsBuffer.bind();
        program->enableAttributeArray(0);
        program->setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(QVector3D));
        program->setUniformValue("color", QVector3D(0,0,0.6f));
        glDrawArrays(GL_POINTS, 0, (int)points.size());
        if( dragIdx > -1 )
        {
            program->setUniformValue("color", QVector3D(0,1,1));
            glDrawArrays(GL_POINTS, dragIdx, 1);
        }
    }
}

void Canvas2D::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
}

void Canvas2D::mouseDoubleClickEvent(QMouseEvent *)
{
    stopPicking();
}

void Canvas2D::mouseMoveEvent(QMouseEvent *event)
{
    QVector3D point( event->x(), height()-event->y(), 0 );

    point = point.unproject( view, proj, QRect(0,0,width(),height()));
    point.setZ(0.f);

    if( dragIdx > -1 && event->buttons() & Qt::LeftButton )
    {
        points[dragIdx] = point;
        points.back() = points.front();
    }

    else if( previewing )
    {
        points.back() = point;
    }

    else
    {
        dragIdx = -1;
        setCursor(QCursor(Qt::ArrowCursor));
        float tolerance = 0.5f;
        for( int i = 0; i < (int)points.size(); ++i )
        {
            if( point.distanceToPoint(points[i]) < tolerance )
            {
                setCursor(QCursor(Qt::CrossCursor));
                dragIdx = i;
                break;
            }
        }
    }

    update();
}

void Canvas2D::mouseReleaseEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton )
    {
        if( dragIdx > -1 )
        {
            dragIdx = -1;

        }
        else if (previewing)
        {
            QVector3D point( event->x(), height()-event->y(), 0 );
            point = point.unproject( view, proj, QRect(0,0,width(),height()));
            point.setZ(0.f);

            points.push_back( point );
        }
//        else
//        {
//            points.clear();
//            QVector3D point( event->x(), height()-event->y(), 0 );
//            point = point.unproject( view, proj, QRect(0,0,width(),height()));
//            point.setZ(0.f);
//            points.push_back( point );
//            previewing = true;
//        }
    }

    update();
}

void Canvas2D::createVBOs()
{
    glGenBuffers(1, &sliceVBO);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
    glVertexAttribBinding(0,0);
    glVertexAttribBinding(1,0);

    glGenTextures(1, &sliceTexture);
}
