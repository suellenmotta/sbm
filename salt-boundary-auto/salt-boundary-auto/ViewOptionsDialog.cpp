#include "ViewOptionsDialog.h"
#include "ui_ViewOptionsDialog.h"

ViewOptionsDialog::ViewOptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewOptionsDialog)
{
    ui->setupUi(this);
}

ViewOptionsDialog::~ViewOptionsDialog()
{
    delete ui;
}

void ViewOptionsDialog::setupMinMaxPossibleValues(double minValue, double maxValue)
{
    ui->minValueSpinBox->setRange(minValue,maxValue);
    ui->maxValueSpinBox->setRange(minValue,maxValue);
}

void ViewOptionsDialog::setupMinMaxValues(double minValue, double maxValue)
{
    ui->minValueSpinBox->setValue(minValue);
    ui->maxValueSpinBox->setValue(maxValue);
}

void ViewOptionsDialog::on_minValueSpinBox_valueChanged(double value)
{
    emit minValueChanged(value);
}

void ViewOptionsDialog::on_maxValueSpinBox_valueChanged(double value)
{
    emit maxValueChanged(value);
}

