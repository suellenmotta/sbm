#ifndef FILTER_H
#define FILTER_H

#include <vector>

using Array1D = std::vector<float>;
using Array2D = std::vector< std::vector<float> >;
using Array3D = std::vector< std::vector< std::vector<float> > >;

class Filter
{
public:
    virtual void applyN(int nd, Array1D x, Array1D& y) = 0;

    virtual void applyXN(int nd, Array2D x, Array2D& y) = 0;

    void applyNX(int nd, Array2D x, Array2D& y)
    {
      int m2 = y.size();
      for (int i2=0; i2<m2; ++i2)
        applyN(nd,x[i2],y[i2]);
    }

    void applyNXX(int nd, const Array3D& x, Array3D& y) {
      int m3 = y.size();

      for (int i3=0; i3<m3; ++i3)
          applyNX(nd,x[i3],y[i3]);
    }

    void applyXNX(int nd, const Array3D& x, Array3D& y) {
      int m3 = y.size();

      for (int i3=0; i3<m3; ++i3)
         applyXN(nd,x[i3],y[i3]);
    }

    void applyXXN(int nd, const Array3D& x, Array3D& y) {
      //checkArrays(x,y);
      int m3 = y.size();
      int m2 = y[0].size();
      Array3D tx(m2,Array2D(m3));
      Array3D ty(m2,Array2D(m3));
      for (int i3=0; i3<m3; ++i3) {
        for (int i2=0; i2<m2; ++i2) {
          tx[i2][i3] = x[i3][i2];
          ty[i2][i3] = y[i3][i2];
        }
      }

      for (int i2=0; i2<m2; ++i2)
          applyXN(nd,tx[i2],ty[i2]);
    }
    /* Should be equivalent to the parallel version above.
    void applyXXN(int nd, const Array3D& x, Array3D& y) {
      checkArrays(x,y);
      int m3 = y.size();
      int m2 = y[0].size();
      const Array2D& x2 = new float[m3][];
      Array2D& y2 = new float[m3][];
      for (int i2=0; i2<m2; ++i2) {
        for (int i3=0; i3<m3; ++i3) {
          x2[i3] = x[i3][i2];
          y2[i3] = y[i3][i2];
        }
        applyXN(nd,x2,y2);
      }
    }
    */
};





#endif // FILTER_H
