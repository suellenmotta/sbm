#include "DericheFilter.h"


#include <math.h>
using namespace std;

double DericheFilter::a00 =  1.6797292232361107;
double DericheFilter::a10 =  3.7348298269103580;
double DericheFilter::b00 =  1.7831906544515104;
double DericheFilter::b10 =  1.7228297663338028;
double DericheFilter::c00 = -0.6802783501806897;
double DericheFilter::c10 = -0.2598300478959625;
double DericheFilter::w00 =  0.6318113174569493;
double DericheFilter::w10 =  1.9969276832487770;
// 1st derivative.
double DericheFilter::a01 =  0.6494024008440620;
double DericheFilter::a11 =  0.9557370760729773;
double DericheFilter::b01 =  1.5159726670750566;
double DericheFilter::b11 =  1.5267608734791140;
double DericheFilter::c01 = -0.6472105276644291;
double DericheFilter::c11 = -4.5306923044570760;
double DericheFilter::w01 =  2.0718953658782650;
double DericheFilter::w11 =  0.6719055957689513;
// 2nd derivative.
double DericheFilter::a02 =  0.3224570510072559;
double DericheFilter::a12 = -1.7382843963561239;
double DericheFilter::b02 =  1.3138054926516880;
double DericheFilter::b12 =  1.2402181393295362;
double DericheFilter::c02 = -1.3312275593739595;
double DericheFilter::c12 =  3.6607035671974897;
double DericheFilter::w02 =  2.1656041357418863;
double DericheFilter::w12 =  0.7479888745408682;
//
double DericheFilter::a0[3] = {a00,a01,a02};
double DericheFilter::a1[3] = {a10,a11,a12};
double DericheFilter::b0[3] = {b00,b01,b02};
double DericheFilter::b1[3] = {b10,b11,b12};
double DericheFilter::c0[3] = {c00,c01,c02};
double DericheFilter::c1[3] = {c10,c11,c12};
double DericheFilter::w0[3] = {w00,w01,w02};
double DericheFilter::w1[3] = {w10,w11,w12};

DericheFilter::DericheFilter(double sigma)
{
    makeND(sigma);
}

void DericheFilter::applyN(int nd, Array1D x, Array1D& y)
{
//  checkArrays(x,y);
//  if (sameArrays(x,y))
//    x = copy(x);
  int m = y.size();
  float n0 = _n0[nd],  n1 = _n1[nd],  n2 = _n2[nd],  n3 = _n3[nd];
  float d1 = _d1[nd],  d2 = _d2[nd],  d3 = _d3[nd],  d4 = _d4[nd];
  float yim4 = 0.0f,  yim3 = 0.0f,  yim2 = 0.0f,  yim1 = 0.0f;
  float               xim3 = 0.0f,  xim2 = 0.0f,  xim1 = 0.0f;
  for (int i=0; i<m; ++i) {
    float xi = x[i];
    float yi = n0*xi+n1*xim1+n2*xim2+n3*xim3 -
                     d1*yim1-d2*yim2-d3*yim3-d4*yim4;
    y[i] = yi;
    yim4 = yim3;  yim3 = yim2;  yim2 = yim1;  yim1 = yi;
                  xim3 = xim2;  xim2 = xim1;  xim1 = xi;
  }
  n1 = n1-d1*n0;
  n2 = n2-d2*n0;
  n3 = n3-d3*n0;
  float n4 = -d4*n0;
  if (nd%2!=0) {
    n1 = -n1;  n2 = -n2;  n3 = -n3;  n4 = -n4;
  }
  float yip4 = 0.0f,  yip3 = 0.0f,  yip2 = 0.0f,  yip1 = 0.0f;
  float xip4 = 0.0f,  xip3 = 0.0f,  xip2 = 0.0f,  xip1 = 0.0f;
  for (int i=m-1; i>=0; --i) {
    float xi = x[i];
    float yi = n1*xip1+n2*xip2+n3*xip3+n4*xip4 -
               d1*yip1-d2*yip2-d3*yip3-d4*yip4;
    y[i] += yi;
    yip4 = yip3;  yip3 = yip2;  yip2 = yip1;  yip1 = yi;
    xip4 = xip3;  xip3 = xip2;  xip2 = xip1;  xip1 = xi;
  }
}

void DericheFilter::applyXN(int nd, Array2D x, Array2D& y)
{
//  checkArrays(x,y);
//  if (sameArrays(x,y))
//    x = copy(x);
  int m2 = y.size();
  int m1 = y[0].size();
  float n0 = _n0[nd],  n1 = _n1[nd],  n2 = _n2[nd],  n3 = _n3[nd];
  float d1 = _d1[nd],  d2 = _d2[nd],  d3 = _d3[nd],  d4 = _d4[nd];
  Array1D yim4(m1);
  Array1D yim3(m1);
  Array1D yim2(m1);
  Array1D yim1(m1);
  Array1D xim4(m1);
  Array1D xim3(m1);
  Array1D xim2(m1);
  Array1D xim1(m1);
  Array1D yi(m1);
  Array1D xi(m1);
  for (int i2=0; i2<m2; ++i2) {
    auto& x2 = x[i2];
    auto& y2 = y[i2];
    for (int i1=0; i1<m1; ++i1) {
      xi[i1] = x2[i1];
      yi[i1] = n0*xi[i1]+n1*xim1[i1]+n2*xim2[i1]+n3*xim3[i1]
                        -d1*yim1[i1]-d2*yim2[i1]-d3*yim3[i1]-d4*yim4[i1];
      y2[i1] = yi[i1];
    }
    auto yt = yim4;
    yim4 = yim3;
    yim3 = yim2;
    yim2 = yim1;
    yim1 = yi;
    yi = yt;
    auto xt = xim3;
    xim3 = xim2;
    xim2 = xim1;
    xim1 = xi;
    xi = xt;
  }
  n1 = n1-d1*n0;
  n2 = n2-d2*n0;
  n3 = n3-d3*n0;
  float n4 = -d4*n0;
  if (nd%2!=0) {
    n1 = -n1;  n2 = -n2;  n3 = -n3;  n4 = -n4;
  }
  auto& yip4 = yim4;
  auto& yip3 = yim3;
  auto& yip2 = yim2;
  auto& yip1 = yim1;
  auto& xip4 = xim4;
  auto& xip3 = xim3;
  auto& xip2 = xim2;
  auto& xip1 = xim1;
  for (int i1=0; i1<m1; ++i1) {
    yip4[i1] = 0.0f;
    yip3[i1] = 0.0f;
    yip2[i1] = 0.0f;
    yip1[i1] = 0.0f;
    xip4[i1] = 0.0f;
    xip3[i1] = 0.0f;
    xip2[i1] = 0.0f;
    xip1[i1] = 0.0f;
  }
  for (int i2=m2-1; i2>=0; --i2) {
    auto& x2 = x[i2];
    auto& y2 = y[i2];
    for (int i1=0; i1<m1; ++i1) {
      xi[i1] = x2[i1];
      yi[i1] = n1*xip1[i1]+n2*xip2[i1]+n3*xip3[i1]+n4*xip4[i1] -
               d1*yip1[i1]-d2*yip2[i1]-d3*yip3[i1]-d4*yip4[i1];
      y2[i1] += yi[i1];
    }
    auto yt = yip4;
    yip4 = yip3;
    yip3 = yip2;
    yip2 = yip1;
    yip1 = yi;
    yi = yt;
    auto xt = xip4;
    xip4 = xip3;
    xip3 = xip2;
    xip2 = xip1;
    xip1 = xi;
    xi = xt;
  }
}

void DericheFilter::makeND(double sigma)
{
    // For 0th, 1st, and 2nd derivatives, ...
  for (int i=0; i<3; ++i) {
    double n0 = (i%2==0)?a0[i]+c0[i]:0.0;
    double n1 = exp(-b1[i]/sigma) * (
                  c1[i]*sin(w1[i]/sigma) -
                  (c0[i]+2.0*a0[i])*cos(w1[i]/sigma)) +
                exp(-b0[i]/sigma) * (
                  a1[i]*sin(w0[i]/sigma) -
                  (2.0*c0[i]+a0[i])*cos(w0[i]/sigma));
    double n2 = 2.0*exp(-(b0[i]+b1[i])/sigma) * (
                  (a0[i]+c0[i])*cos(w1[i]/sigma)*cos(w0[i]/sigma) -
                  a1[i]*cos(w1[i]/sigma)*sin(w0[i]/sigma) -
                  c1[i]*cos(w0[i]/sigma)*sin(w1[i]/sigma)) +
                c0[i]*exp(-2.0*b0[i]/sigma) +
                a0[i]*exp(-2.0*b1[i]/sigma);
    double n3 = exp(-(b1[i]+2.0*b0[i])/sigma) * (
                  c1[i]*sin(w1[i]/sigma) -
                  c0[i]*cos(w1[i]/sigma)) +
                exp(-(b0[i]+2.0*b1[i])/sigma) * (
                  a1[i]*sin(w0[i]/sigma) -
                  a0[i]*cos(w0[i]/sigma));
    double d1 = -2.0*exp(-b0[i]/sigma)*cos(w0[i]/sigma) -
                 2.0*exp(-b1[i]/sigma)*cos(w1[i]/sigma);
    double d2 = 4.0*exp(-(b0[i]+b1[i])/sigma) *
                  cos(w0[i]/sigma)*cos(w1[i]/sigma) +
                exp(-2.0*b0[i]/sigma) +
                exp(-2.0*b1[i]/sigma);
    double d3 = -2.0*exp(-(b0[i]+2.0*b1[i])/sigma)*cos(w0[i]/sigma) -
                 2.0*exp(-(b1[i]+2.0*b0[i])/sigma)*cos(w1[i]/sigma);
    double d4 = exp(-2.0*(b0[i]+b1[i])/sigma);
    _n0[i] = (float)n0;
    _n1[i] = (float)n1;
    _n2[i] = (float)n2;
    _n3[i] = (float)n3;
    _d1[i] = (float)d1;
    _d2[i] = (float)d2;
    _d3[i] = (float)d3;
    _d4[i] = (float)d4;
  }
  scaleN(sigma);
}

void DericheFilter::scaleN(double sigma)
{
    int n = 1+2*(int)(10.0*sigma);
    std::vector<float> x(n);
    std::vector<float> y0(n);
    std::vector<float> y1(n);
    std::vector<float> y2(n);
    int m = (n-1)/2;
    x[m] = 1.0f;
    applyN(0,x,y0);
    applyN(1,x,y1);
    applyN(2,x,y2);
    double s[3];
    for (int i=0,j=n-1; i<j; ++i,--j) {
      double t = i-m;
      s[0] += y0[j]+y0[i];
      s[1] += sin(t/sigma)*(y1[j]-y1[i]);
      s[2] += cos(t*sqrt(2.0)/sigma)*(y2[j]+y2[i]);
    }
    s[0] += y0[m];
    s[2] += y2[m];
    s[1] *= sigma*exp(0.5);
    s[2] *= -(sigma*sigma)/2.0*exp(1.0);
    for (int i=0; i<3; ++i) {
      _n0[i] /= s[i];
      _n1[i] /= s[i];
      _n2[i] /= s[i];
      _n3[i] /= s[i];
    }
}
