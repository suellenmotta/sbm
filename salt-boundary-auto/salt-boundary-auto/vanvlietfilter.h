#ifndef VANVLIETFILTER_H
#define VANVLIETFILTER_H

#include "filter.h"
#include "recursive2ndorderfilter.h"

class VanVlietFilter : public Filter
{
public:
    VanVlietFilter(double sigma);

    void applyN(int nd, Array1D x, Array1D& y) override;
    void applyXN(int nd, Array2D x, Array2D& y) override;

    // Poles (inverses) for 4th-order filters published by van Vliet, et al.
    static std::vector< std::vector< std::complex<double> > > POLES;//[3][4];

private:

    void makeG(double sigma);

    Recursive2ndOrderFilter makeFilter(double b0, double b1,
                                       double b2, double a1, double a2);

    /**
     * Evaluates residue of G(z) for the n'th derivative and j'th pole.
     */
    std::complex<double> gr(int nd, std::complex<double> polej,
                            const std::vector<std::complex<double> > &poles, double gain);

    std::vector< std::complex<double> > adjustPoles(double sigma,
                                                    const std::vector<std::complex<double> > poles);

    double computeGain(const std::vector<std::complex<double> > &poles);

    double computeSigma(double sigma, const std::vector< std::complex<double> >& poles);

    Recursive2ndOrderFilter _g[3][2][2];
};


#endif // VANVLIETFILTER_H
