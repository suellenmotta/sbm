#ifndef DERICHEFILTER_H
#define DERICHEFILTER_H

#include "filter.h"


class DericheFilter : public Filter
{
public:
    DericheFilter(double sigma);

    void applyN(int nd, Array1D x, Array1D& y) override;
    void applyXN(int nd, Array2D x, Array2D& y) override;

private:

      // Coefficients computed using Deriche's method. These coefficients
      // were computed for sigma = 100 and 0 <= x <= 10*sigma = 1000,
      // using the Mathematica function FindFit. The coefficients have
      // roughly 10 digits of precision.
      // 0th derivative.
      static double a00;
      static double a10;
      static double b00;
      static double b10;
      static double c00;
      static double c10;
      static double w00;
      static double w10;
      // 1st derivative
      static double a01;
      static double a11;
      static double b01;
      static double b11;
      static double c01;
      static double c11;
      static double w01;
      static double w11;
      // 2nd derivative
      static double a02;
      static double a12;
      static double b02;
      static double b12;
      static double c02;
      static double c12;
      static double w02;
      static double w12;
      //
      static double a0[3];
      static double a1[3];
      static double b0[3];
      static double b1[3];
      static double c0[3];
      static double c1[3];
      static double w0[3];
      static double w1[3];

      /*
      // Deriche's published coefficients.
      private static double[] a0 = {  1.6800, -0.6472, -1.3310};
      private static double[] a1 = {  3.7350, -4.5310,  3.6610};
      private static double[] b0 = {  1.7830,  1.5270,  1.2400};
      private static double[] b1 = {  1.7230,  1.5160,  1.3140};
      private static double[] c0 = { -0.6803,  0.6494,  0.3225};
      private static double[] c1 = { -0.2598,  0.9557, -1.7380};
      private static double[] w0 = {  0.6318,  0.6719,  0.7480};
      private static double[] w1 = {  1.9970,  2.0720,  2.1660};
      */
      float _n0[3],_n1[3],_n2[3],_n3[3]; // numerator coefficients
      float _d1[3],_d2[3],_d3[3],_d4[3]; // denominator coefficients

      /**
       * Makes Deriche's numerator and denominator coefficients.
       */
      void makeND(double sigma);

      /**
       * Scales numerator filter coefficients to normalize the filters.
       * For example, the sum of the 0th-derivative filter coefficients
       * should be 1.0. The scale factors are computed from finite-length
       * approximations to the impulse responses of the three filters.
       */
      void scaleN(double sigma);

};

#endif // DERICHEFILTER_H

