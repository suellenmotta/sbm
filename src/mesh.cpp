#include "mesh.h"

#include <iostream>
#include <typeinfo>
#include <QDebug>
namespace opengl
{

template< typename VertexType, int PrimitiveType >
Mesh<VertexType,PrimitiveType>::Mesh()
    : VBO(0)
    , EBO(0)
    , created(false)
{
    initializeOpenGLFunctions();
}

template< typename VertexType, int PrimitiveType >
Mesh<VertexType,PrimitiveType>::~Mesh()
{
    if(created)
    {
        glDeleteBuffers(1, &VBO);
        glDeleteBuffers(1, &EBO);
    }
}

template<typename VertexType, int PrimitiveType>
Mesh<VertexType,PrimitiveType>::Mesh(Mesh<VertexType,PrimitiveType> &&rhs) noexcept
{
    vertexBuffer = std::move(rhs.vertexBuffer);
    indices = std::move(rhs.indices);
    name = std::move(rhs.name);
    material = std::move(rhs.material);
    modelMatrix = std::move(rhs.modelMatrix);
    VBO = std::move(rhs.VBO);
    EBO = std::move(rhs.EBO);
    created = std::move(rhs.created);

    initializeOpenGLFunctions();

    rhs.created = false;
}


template< typename VertexType, int PrimitiveType >
void Mesh<VertexType,PrimitiveType>::create()
{
    if(created)
        return;

    if( vertexBuffer.empty() )
    {
        std::cerr << "[Mesh::create] Error: vertex buffer is empty." << std::endl;
        return;
    }

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size()*sizeof(VertexType), &vertexBuffer[0], GL_DYNAMIC_DRAW);

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    created = true;
}

template< typename VertexType, int PrimitiveType >
void Mesh<VertexType,PrimitiveType>::update()
{
    if( vertexBuffer.empty() )
    {
        std::cerr << "[Mesh::update] Error: vertex buffer is empty." << std::endl;
        return;
    }

    if( VBO == 0 )
    {
        std::cerr << "[Mesh::update] Error: VBO was never created." << std::endl;
        return;
    }

    if( EBO == 0 )
    {
        std::cerr << "[Mesh::update] Error: EBO was never created." << std::endl;
        return;
    }

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertexBuffer.size()*sizeof(VertexType), &vertexBuffer[0], GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_DYNAMIC_DRAW);
}

template< typename VertexType, int PrimitiveType >
void Mesh<VertexType,PrimitiveType>::draw()
{
    bindVBO();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(PrimitiveType, (GLsizei)indices.size(), GL_UNSIGNED_INT,0);
}

template< typename VertexType, int PrimitiveType >
void Mesh<VertexType,PrimitiveType>::bindVBO()
{
    glBindVertexBuffer(0,VBO,0,sizeof(VertexType));
}

template<typename VertexType, int PrimitiveType>
void Mesh<VertexType,PrimitiveType>::createCopy(Mesh<VertexType,PrimitiveType> &copy, const std::string& name)
{
    copy.vertexBuffer = vertexBuffer;
    copy.indices = indices;
    copy.name = name;
    copy.material = material;
    copy.modelMatrix = modelMatrix;
    copy.create();
}

template class Mesh< Vertex, GL_TRIANGLES >;
template class Mesh< glm::vec3, GL_LINE_STRIP >;

}

std::ostream& operator << (std::ostream& output, const glm::vec3& point)
{
    output << std::fixed << std::setprecision(6) << "("
           << point.x << ", "
           << point.y << ", "
           << point.z <<
              ")";
    return output;
}
