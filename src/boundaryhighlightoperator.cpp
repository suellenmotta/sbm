#include "boundaryhighlightoperator.h"

#include "meshalgorithms.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>



BoundaryHighlightOperator::BoundaryHighlightOperator(Scene& scene)
    : Operator(scene)
{
    initializeOpenGLFunctions();

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/solid-color-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/solid-color-frag");
    program.link();

    if( !program.isLinked() )
        qCritical() << "[ROISelecitionOperator] Shader program not linked";

    findBorderVertices(scene);

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glEnableVertexAttribArray( 0 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0,0);
    glBindVertexArray(0);

    for(auto& mesh : meshes)
        mesh.create();
}

void BoundaryHighlightOperator::draw()
{
    glPointSize(3.0f);
    glLineWidth(3.0f);
    glEnable(GL_POINT_SMOOTH);

    program.bind();

    QMatrix4x4 m(glm::value_ptr(glm::transpose(scene.camera.modelMatrix())));
    QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

    auto mvp = p*v*m;
    program.setUniformValue("MVP", mvp);

    glBindVertexArray(VAO);

    for(auto& mesh : meshes)
    {
        auto color = mesh.material.diffuse;
        program.setUniformValue("color", color.r, color.g, color.b);

        mesh.draw();
    }

    glBindVertexArray(0);

    program.release();
}

void BoundaryHighlightOperator::findBorderVertices(const Scene& scene)
{
    for(const auto& obj : scene.objects)
    {
        std::vector< std::vector< unsigned int > > borders;
        findBorders(obj,borders);

        //TODO: Improve render: do not upload vertices again, only indices
        for( const auto& border : borders)
        {
            meshes.emplace_back();
            auto& mesh = meshes.back();

            int i = 0;
            for(auto v : border)
            {
                mesh.vertexBuffer.push_back(obj.vertexBuffer[v].position);
                mesh.indices.push_back(i++); //not used
            }

            mesh.material = obj.material;
            mesh.modelMatrix = obj.modelMatrix;
        }
    }
}
