#include "slicesketchingoperator.h"

#include <glm/gtc/type_ptr.hpp>
#include <sstream>

#include "raycasting.h"

SliceSketchingOperator::SliceSketchingOperator(Scene &scene)
    : Operator(scene)
    , volumeRenderer(scene.getVolumeRenderer())
    , onlyVisible(false)
    , drawMode(GL_LINE_STRIP)
    , curAxis(NewVolume::X)
    , preview(true)
    , curveClosed(false)
{
    initializeOpenGLFunctions();

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/solid-color-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/solid-color-frag");
    program.link();

    if( !program.isLinked() )
        qCritical() << "[SliceSketchingOperator] Shader program not linked";

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glEnableVertexAttribArray( 0 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0,0);
    glBindVertexArray(0);

    glGenBuffers(1, &previewVBO);
}

std::vector<std::pair<NewVolume::Axis, unsigned int> > SliceSketchingOperator::getSketchedSlices()
{
    std::vector<std::pair<NewVolume::Axis, unsigned int> > sketchedSlices;

    for(const auto& sketchIdx : xSketches)
        sketchedSlices.push_back({NewVolume::X,sketchIdx.first});

    for(const auto& sketchIdx : ySketches)
        sketchedSlices.push_back({NewVolume::Y,sketchIdx.first});

    for(const auto& sketchIdx : zSketches)
        sketchedSlices.push_back({NewVolume::Z,sketchIdx.first});

    return sketchedSlices;
}

const std::map<unsigned int, std::vector<glm::vec3> > &SliceSketchingOperator::getSketches(NewVolume::Axis axis)
{
    switch(axis)
    {
        case NewVolume::X:
            return xSketches;
        case NewVolume::Y:
            return ySketches;
        default:
            return zSketches;
    }
}

std::vector<std::string> SliceSketchingOperator::getSketchNames()
{
    std::vector< std::string > sketchNames;

    std::string prefix = "sketch_" + std::string("X") + "_";
    for(const auto& sketchIdx : xSketches)
    {
        std::stringstream str;
        str << prefix << sketchIdx.first;
        sketchNames.push_back(str.str());
    }

    prefix = "sketch_" + std::string("Y") + "_";
    for(const auto& sketchIdx : ySketches)
    {
        std::stringstream str;
        str << prefix << sketchIdx.first;
        sketchNames.push_back(str.str());
    }

    prefix = "sketch_" + std::string("Z") + "_";
    for(const auto& sketchIdx : zSketches)
    {
        std::stringstream str;
        str << prefix << sketchIdx.first;
        sketchNames.push_back(str.str());
    }

    return sketchNames;
}

std::vector<glm::vec3> SliceSketchingOperator::getSketchesPointCloud()
{
    std::vector< glm::vec3 > pointCloud;

    for(auto sketch : xSketches)
        pointCloud.insert(pointCloud.end(),sketch.second.begin(),sketch.second.end());

    for(auto sketch : ySketches)
        pointCloud.insert(pointCloud.end(),sketch.second.begin(),sketch.second.end());

    for(auto sketch : zSketches)
        pointCloud.insert(pointCloud.end(),sketch.second.begin(),sketch.second.end());

    return pointCloud;
}

void SliceSketchingOperator::deleteSketch(NewVolume::Axis axis, unsigned int index)
{
    switch(axis)
    {
        case NewVolume::X:
            xSketches.erase(index);
            if(xVBOs.find(index)!=xVBOs.end())
            {
                glDeleteBuffers(1,&xVBOs[index]);
                xVBOs.erase(index);
            }
            break;
        case NewVolume::Y:
            ySketches.erase(index);
            if(yVBOs.find(index)!=yVBOs.end())
            {
                glDeleteBuffers(1,&yVBOs[index]);
                yVBOs.erase(index);
            }
            break;
        case NewVolume::Z:
            zSketches.erase(index);
            if(zVBOs.find(index)!=zVBOs.end())
            {
                glDeleteBuffers(1,&zVBOs[index]);
                zVBOs.erase(index);
            }
            break;
        default:
            break;
    }
}

void SliceSketchingOperator::deleteSketches()
{
    xSketches.clear();
    for(auto vbo : xVBOs)
        glDeleteBuffers(1,&vbo.second);
    xVBOs.clear();

    ySketches.clear();
    for(auto vbo : yVBOs)
        glDeleteBuffers(1,&vbo.second);
    yVBOs.clear();

    zSketches.clear();
    for(auto vbo : zVBOs)
        glDeleteBuffers(1,&vbo.second);
    zVBOs.clear();
}

void SliceSketchingOperator::draw()
{
    program.bind();

    glLineWidth(3.0f);
    glEnable(GL_LINE_SMOOTH);

    glPointSize(3.0f);
    glEnable(GL_POINT_SMOOTH);

    glEnable(GL_DEPTH_TEST);

    QMatrix4x4 m(glm::value_ptr(glm::transpose(scene.camera.modelMatrix())));
    QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

    auto mvp = p*v*m;
    program.setUniformValue("MVP", mvp);

    glBindVertexArray(VAO);

    if(onlyVisible)
    {
        if(scene.isXSliceShown())
        {
            auto slice = scene.getCurrentXSlice();
            auto it = xVBOs.find(slice);

            if(it!=xVBOs.end())
            {
                glBindVertexBuffer(0,(*it).second,0,sizeof(glm::vec3));
                glDrawArrays(drawMode,0,xSketches[slice].size());
            }
        }
        if(scene.isYSliceShown())
        {
            auto slice = scene.getCurrentYSlice();
            auto it = yVBOs.find(slice);

            if(it!=yVBOs.end())
            {
                glBindVertexBuffer(0,(*it).second,0,sizeof(glm::vec3));
                glDrawArrays(drawMode,0,ySketches[slice].size());
            }
        }
        if(scene.isZSliceShown())
        {
            auto slice = scene.getCurrentZSlice();
            auto it = zVBOs.find(slice);

            if(it!=zVBOs.end())
            {
                glBindVertexBuffer(0,(*it).second,0,sizeof(glm::vec3));
                glDrawArrays(drawMode,0,zSketches[slice].size());
            }
        }
    }
    else
    {
        for(const auto& vbo : xVBOs)
        {
            glBindVertexBuffer(0,vbo.second,0,sizeof(glm::vec3));
            glDrawArrays(drawMode,0,xSketches[vbo.first].size());
        }
        for(const auto& vbo : yVBOs)
        {
            glBindVertexBuffer(0,vbo.second,0,sizeof(glm::vec3));
            glDrawArrays(drawMode,0,ySketches[vbo.first].size());
        }
        for(const auto& vbo : zVBOs)
        {
            glBindVertexBuffer(0,vbo.second,0,sizeof(glm::vec3));
            glDrawArrays(drawMode,0,zSketches[vbo.first].size());
        }
    }

    if(preview)
    {
        program.setUniformValue("color",1.0f,1.0f,1.0f);
        glBindVertexBuffer(0,previewVBO,0,sizeof(glm::vec3));
        glDrawArrays(drawMode,0,sketchPoints.size());
    }

    glBindVertexArray(0);
    program.release();
}

bool SliceSketchingOperator::mousePress(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton)
    {
        glm::ivec2 screenPos(event->x(), event->y());
        glm::vec3 point;
        if(volumeRenderer.intercepts(screenPos,curAxis,point))
        {
            sketchPoints2d.clear();
            sketchPoints.clear();

            switch(curAxis)
            {
                case NewVolume::X:
                {
                    auto slice = volumeRenderer.getCurrentXSlice();
                    auto it = xVBOs.find(slice);
                    if(it==xVBOs.end())
                    {
                        GLuint vbo;
                        glGenBuffers(1, &vbo);

                        xVBOs[slice]=vbo;
                    }
                    else
                        xSketches[slice].clear();

                    break;
                }
                case NewVolume::Y:
                {
                    auto slice = volumeRenderer.getCurrentYSlice();
                    auto it = yVBOs.find(slice);
                    if(it==yVBOs.end())
                    {
                        GLuint vbo;
                        glGenBuffers(1, &vbo);

                        yVBOs[slice]=vbo;
                    }
                    else
                        ySketches[slice].clear();

                    break;
                }

                case NewVolume::Z:
                {
                    auto slice = volumeRenderer.getCurrentZSlice();
                    auto it = zVBOs.find(slice);
                    if(it==zVBOs.end())
                    {
                        GLuint vbo;
                        glGenBuffers(1, &vbo);

                        zVBOs[slice]=vbo;
                    }
                    else
                        zSketches[slice].clear();

                    break;
                }
            }

            sketchPoints2d.push_back(screenPos);
            sketchPoints.push_back(point);

            glBindBuffer(GL_ARRAY_BUFFER, previewVBO);
            glBufferData(GL_ARRAY_BUFFER, sketchPoints.size()*sizeof(glm::vec3),
                         sketchPoints.data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            preview = true;
            curveClosed = false;
            return true;
        }
    }

    return false;
}

bool SliceSketchingOperator::mouseMove(QMouseEvent *event)
{
    if(!curveClosed && preview && event->buttons() & Qt::LeftButton)
    {
        glm::ivec2 screenPos(event->x(), event->y());
        auto dir = rayDirection(screenPos,scene.camera);

        bool intercepts = false;
        float dist = 0.0f;
        switch(curAxis)
        {
            case NewVolume::X:
                intercepts = volumeRenderer.interceptsXSlice(scene.camera.eye,dir,dist);
                break;
            case NewVolume::Y:
                intercepts = volumeRenderer.interceptsYSlice(scene.camera.eye,dir,dist);
                break;
            case NewVolume::Z:
                intercepts = volumeRenderer.interceptsZSlice(scene.camera.eye,dir,dist);
                break;
            default:
                break;
        }

        if(intercepts)
        {
            auto point = glm::vec3(
                        glm::inverse(scene.camera.modelMatrix())
                        *glm::vec4(scene.camera.eye + dist * dir,1));

            sketchPoints2d.push_back(screenPos);
            sketchPoints.push_back(point);

            if(sketchPoints.size() > 10 &&
               glm::distance(glm::vec2(screenPos),glm::vec2(sketchPoints2d[0])) < 10) //The user has closed the curve
            {
                sketchPoints.push_back(sketchPoints[0]);
                curveClosed = true;
            }

            glBindBuffer(GL_ARRAY_BUFFER, previewVBO);
            glBufferData(GL_ARRAY_BUFFER, sketchPoints.size()*sizeof(glm::vec3),
                         sketchPoints.data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);

            return true;
        }
    }

    return false;
}

bool SliceSketchingOperator::mouseRelease(QMouseEvent *event)
{
    if(preview && event->button()==Qt::LeftButton)
    {
        switch(curAxis)
        {
        case NewVolume::X:
        {
            auto slice = volumeRenderer.getCurrentXSlice();
            xSketches[slice] = std::move(sketchPoints);
            glBindBuffer(GL_ARRAY_BUFFER, xVBOs[slice]);
            glBufferData(GL_ARRAY_BUFFER, xSketches[slice].size()*sizeof(glm::vec3),
                         xSketches[slice].data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);
            break;
        }
        case NewVolume::Y:
        {
            auto slice = volumeRenderer.getCurrentYSlice();
            ySketches[slice] = std::move(sketchPoints);
            glBindBuffer(GL_ARRAY_BUFFER, yVBOs[slice]);
            glBufferData(GL_ARRAY_BUFFER, ySketches[slice].size()*sizeof(glm::vec3),
                         ySketches[slice].data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);
            break;
        }

        case NewVolume::Z:
        {
            auto slice = volumeRenderer.getCurrentZSlice();
            zSketches[slice] = std::move(sketchPoints);
            glBindBuffer(GL_ARRAY_BUFFER, zVBOs[slice]);
            glBufferData(GL_ARRAY_BUFFER, zSketches[slice].size()*sizeof(glm::vec3),
                         zSketches[slice].data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER,0);
            break;
        }

        default:
            break;
        }

        preview = false;
        emit sketchFinished();
        return true;
    }

    return false;
}

void SliceSketchingOperator::showOnlyVisible()
{
    onlyVisible = true;
}

void SliceSketchingOperator::showAllSketches()
{
    onlyVisible = false;
}

void SliceSketchingOperator::showLines()
{
    drawMode = GL_LINE_STRIP;
}

void SliceSketchingOperator::showPointCloud()
{
    drawMode = GL_POINTS;
}
