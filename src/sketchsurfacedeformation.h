#ifndef SKETCHSURFACEDEFORMATION_H
#define SKETCHSURFACEDEFORMATION_H

#include "newvolume.h"
#include "mesh.h"
#include "surfaceslicing.h"
#include "graph.h"

#include <Eigen/Sparse>

//For debug
void writeXYZ( const std::string& path, const std::vector< glm::vec3 >& points );


class SketchSurfaceDeformation
{
public:
    using Sketch = std::vector<glm::vec3>;
    using InterceptionCurve = std::vector<glm::vec3>;
    using Surface = opengl::UntexturedMesh;

    SketchSurfaceDeformation(const NewVolume* volume, Surface& surface);

    bool addSketch(NewVolume::Axis axis, unsigned int slice, Sketch &sketch, unsigned int index = 0);
    void removeSketch(NewVolume::Axis axis, unsigned int slice, unsigned int index = 0);

    void setMinimizeDisplacements(bool minimizeDisplacements);

    std::vector<Sketch>& getSketches(NewVolume::Axis axis, unsigned int slice);
    const std::map<unsigned int, std::vector<Sketch> > &getAllSketches(NewVolume::Axis axis);
    std::vector<InterceptionCurve> getInterceptionCurves(NewVolume::Axis axis, unsigned int slice);

    void run();

    const Surface& getDeformingSurface();

private:
    using ScalarType = float;
    using T = Eigen::Triplet<ScalarType>;
    using Matrix = Eigen::SparseMatrix<ScalarType>;
    using Vector = Eigen::Matrix<ScalarType,Eigen::Dynamic,1>;



    void buildMeshGraph();

    struct EigenMesh
    {
        Eigen::MatrixXf V;
        Eigen::MatrixXi F;
        Eigen::MatrixXf N;
    };

    void buildEigenMesh();

    void alignSketch(const InterceptionCurve& originalCurve, Sketch& sketch, NewVolume::Axis axis);

    void addLaplacianConstraints(std::vector<T>& Atriplets,
                                 Vector &bx, Vector &by, Vector &bz, unsigned int& rowCount);
    void addCotLaplacianConstraints(std::vector<T>& Atriplets,
                                    Vector &bx, Vector &by, Vector &bz, unsigned int& rowCount);
    void addSketchConstraints(std::vector<T>& Atriplets, Vector& bx, Vector& by, Vector& bz, unsigned int& rowCount);

    SurfaceSlicing* slicer;
    Surface& surface;
    std::map< unsigned int, std::vector<Sketch> > sketches[3];

    bool minimizeDisplacements;

    Graph G; //mesh graph
    EigenMesh eMesh;


};

#endif // SKETCHSURFACEDEFORMATION_H
