#include "roiselectionoperator.h"

#include "raycasting.h"
#include "graph.h"

#include <QMessageBox>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <set>
#include <queue>
#include <map>
#include <iostream>
#include "meshalgorithms.h"


ROISelectionOperator::ROISelectionOperator(Scene& scene)
    : Operator(scene)
    , mesh(nullptr)
    , refinement(nullptr)
    , closed(false)
{
    initializeOpenGLFunctions();
    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/solid-color-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/solid-color-frag");
    program.link();
    if( !program.isLinked() )
        qCritical() << "[ROISelecitionOperator] Shader program not linked";

    sketchBuffer.create();
    sketchPoints.reserve(100);

    edgesBuffer.create();

    glMeshToCornerTable(scene.objects[0], &this->mesh);

    // refinement = new CornerTableAdaptiveRefinement(mesh);
}


ROISelectionOperator::~ROISelectionOperator()
{
    delete mesh;
    delete refinement;
}


void ROISelectionOperator::draw()
{
    if( !sketchPoints.empty() && sketchBuffer.isCreated() )
    {
        program.bind();

//        glBindFramebuffer(GL_FRAMEBUFFER, defaultFbo);

        glDisable(GL_DEPTH_TEST);
        glLineWidth(3.0f);
        glPointSize(6.0f);
        glEnable(GL_POINT_SMOOTH);

        QMatrix4x4 m(glm::value_ptr(scene.objects[0].modelMatrix*glm::transpose(scene.camera.modelMatrix())));
        QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
        QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

        auto mv(v*m);
        auto mvp(p*mv);
        auto vp(p*v);
        if(closed)
            program.setUniformValue("MVP", mvp);
        else
            program.setUniformValue("MVP", vp);

        program.setUniformValue("color", QVector3D(0,0,1));

        if( !edgePoints.empty() )
        {
            edgesBuffer.bind();
            edgesBuffer.allocate( &edgePoints[0],
                    (int)edgePoints.size()*sizeof(glm::vec3) );
            program.enableAttributeArray(0);
            program.setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(glm::vec3));

            glDrawArrays(GL_LINE_STRIP, 0, (int)edgePoints.size());
        }

        program.setUniformValue("color", QVector3D(1,0,0));

        sketchBuffer.bind();
        sketchBuffer.allocate( &sketchPoints[0],
                (int)sketchPoints.size()*sizeof(glm::vec3) );
        program.enableAttributeArray(0);
        program.setAttributeBuffer(0,GL_FLOAT,0,3,sizeof(glm::vec3));

        glDrawArrays(GL_LINE_STRIP, 0, (int)sketchPoints.size());

        glEnable(GL_DEPTH_TEST);
    }
}


bool ROISelectionOperator::mousePress(QMouseEvent* event)
{
    if( event->buttons() & Qt::LeftButton )
    {
        sketchPoints.clear();
        sketchPoints2d.clear();
        edgePoints.clear();
        highlightedTriangles.clear();
        closed = false;
        scene.objects[0].modelMatrix = scene.camera.modelMatrix();
        return true;
    }
}


void bresenham(glm::vec2 start, glm::vec2 end, std::vector< glm::vec2 >& line)
{
    const bool steep = (glm::abs(end.y - start.y) > glm::abs(end.x - start.x));
    if(steep)
    {
        std::swap(start.x, start.y);
        std::swap(end.x, end.y);
    }

    if(start.x > end.x)
    {
        std::swap(start, end);
    }

    const glm::vec2 delta(end.x - start.x,glm::abs(end.y - start.y));

    float error = delta.x / 2.0f;
    const int ystep = (start.y < end.y) ? 1 : -1;
    int y = (int)start.y;

    for(int x=(int)start.x; x<(int)end.x; x++)
    {
        line.push_back(steep ? glm::vec2(y,x) : glm::vec2(x,y) );

        error -= delta.y;
        if(error < 0)
        {
            y += ystep;
            error += delta.x;
        }
    }
}


bool ROISelectionOperator::mouseMove(QMouseEvent* event)
{
    if( event->buttons() & Qt::LeftButton && !closed )
    {
        glm::vec2 point(event->x(), event->y());

        if( sketchPoints2d.size() > 10 &&
            glm::distance2(sketchPoints2d[0],glm::vec2(point.x,scene.camera.height-point.y)) < 20 )
        {
            closed = true;
            sketchPoints2d.push_back(sketchPoints2d[0]);
            sketchPoints.push_back(sketchPoints[0]);
            return true;
        }

        int triangle = -1;
        float depth = 0.f;
        if( intercepts(point.x, point.y, triangle, depth) )
        {
            glm::vec3 orig = scene.camera.eye;
            glm::vec3 dir = rayDirection(glm::vec2(event->x(), event->y()),scene.camera);

            glm::vec3 sketchPoint = orig + (depth/*-0.005f*/) * dir;
            sketchPoints.push_back(sketchPoint);

            point.y = scene.camera.height-point.y; // The original implementaion need this step
            sketchPoints2d.push_back(point);
            // subdivide(triangle,orig + depth * dir);
            // scene.objects[0].update();
            scene.changed = true;
            return true;
        }
    }


    return false;
}


bool ROISelectionOperator::mouseRelease(QMouseEvent* event)
{
    if( event->button() == Qt::LeftButton )
    {
        adjust();
        // scene.objects[0].update();
        scene.changed = true;

        return true;
    }


    return false;
}


bool ROISelectionOperator::mouseDoubleClick(QMouseEvent * event)
{
    if( event->button() == Qt::LeftButton )
    {
        sketchPoints.clear();
        closed = false;
        return true;
    }

    return false;
}



bool ROISelectionOperator::intercepts( int x, int y, int& triangleId, float& depth )
{
    triangleId = -1;
    depth = -1;
    glm::ivec2 pos(x,y);
    int object;

    scene.pick(pos,object,triangleId);

    GLenum error;

//    if ((error = glGetError()) != GL_NO_ERROR)
//        qDebug() << "GL Error: "<< error;

    // qDebug() << "triangle: " << triangleId << " e objeto: " <<  object;

    if( triangleId < 0 )
        return false;

    scene.getDepth(pos,depth);

    // qDebug() << "depth: " << depth << " " << pos.x << " " << pos.y;

    glm::vec3 orig = scene.camera.eye;
    glm::vec3 dir = rayDirection(glm::vec2(x, y),scene.camera);

    opengl::UntexturedMesh& obj = scene.objects[object];
    auto i = triangleId*3;
    auto v0 = glm::vec3(scene.camera.modelMatrix() * glm::vec4(obj.vertexBuffer[obj.indices[i+0]].position,1));
    auto v1 = glm::vec3(scene.camera.modelMatrix() * glm::vec4(obj.vertexBuffer[obj.indices[i+1]].position,1));
    auto v2 = glm::vec3(scene.camera.modelMatrix() * glm::vec4(obj.vertexBuffer[obj.indices[i+2]].position,1));

    bool reponse = interceptsTriangle( orig, dir, v0, v1, v2, depth );
    return reponse;
}


// void ROISelectionOperator::subdivide(int triangleID, const glm::vec3& point)
// {
//    unsigned int numAttributes = mesh->getNumberAttributesByVertex();
//    if( numAttributes < 3 )
//    {
//        std::cerr << "[ROISelectionOperator::subdivide] Error: number of attributes less than 3." << std::endl;
//        return;
//    }

//    double* attributes = mesh->getAttributes();
//    const unsigned int* indices = mesh->getTriangleList();
//    unsigned int numTriangles = mesh->getNumTriangles();

//    unsigned int i0 = indices[3*triangleID+0]*numAttributes;
//    unsigned int i1 = indices[3*triangleID+1]*numAttributes;
//    unsigned int i2 = indices[3*triangleID+2]*numAttributes;

//    glm::vec3 v0( attributes[i0+0], attributes[i0+1], attributes[i0+2] );
//    glm::vec3 v1( attributes[i1+0], attributes[i1+1], attributes[i1+2] );
//    glm::vec3 v2( attributes[i2+0], attributes[i2+1], attributes[i2+2] );

//    auto isInsideTriangle = [](const glm::vec3& a, const glm::vec3& b,
//            const glm::vec3& c, const glm::vec3& p)
//    {
// //        //Assuming normal is normalized
// //        float cosAng = glm::dot(n, glm::normalize(p-a));

// //        if( cosAng-cosAng <= 0.001f ) //Not on triangle plane
// //            return false;

//        // Compute vectors
//        auto v0 = c - a;
//        auto v1 = b - a;
//        auto v2 = p - a;

//        // Compute dot products
//        auto dot00 = dot(v0, v0);
//        auto dot01 = dot(v0, v1);
//        auto dot02 = dot(v0, v2);
//        auto dot11 = dot(v1, v1);
//        auto dot12 = dot(v1, v2);

//        // Compute barycentric coordinates
//        float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
//        float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
//        float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

//        // Check if point is in triangle
//        return (u >= 0.f) && (v >= 0.f) && (u + v < 1.f);
//    };

//    float threshold = 0.00001f;
//    while( glm::distance2(v0,point) > threshold &&
//           glm::distance2(v1,point) > threshold &&
//           glm::distance2(v2,point) > threshold )
//    {
//        if( triangleID < 0 )
//        {
//            std::cerr << "[ROISelectionOperator::subdivide] Error: Point lies in no new triangle." << std::endl;
//            return;
//        }

//        std::set<CornerType> selection;
//        selection.insert(triangleID);
//        refinement->meshAdaptiveRefinement(selection, false, CornerTableAdaptiveRefinement::REDGREEN );

//        attributes = mesh->getAttributes();
//        indices = mesh->getTriangleList();

//        i0 = indices[3*triangleID+0]*numAttributes;
//        i1 = indices[3*triangleID+1]*numAttributes;
//        i2 = indices[3*triangleID+2]*numAttributes;

//        v0 = glm::vec3( attributes[i0+0], attributes[i0+1], attributes[i0+2] );
//        v1 = glm::vec3( attributes[i1+0], attributes[i1+1], attributes[i1+2] );
//        v2 = glm::vec3( attributes[i2+0], attributes[i2+1], attributes[i2+2] );

//        if( !isInsideTriangle(v0,v1,v2,point) )
//        {
//            triangleID = -1;
//            for( auto i = numTriangles; i < mesh->getNumTriangles(); i++ )
//            {
//                i0 = indices[3*i+0]*numAttributes;
//                i1 = indices[3*i+1]*numAttributes;
//                i2 = indices[3*i+2]*numAttributes;

//                v0 = glm::vec3( attributes[i0+0], attributes[i0+1], attributes[i0+2] );
//                v1 = glm::vec3( attributes[i1+0], attributes[i1+1], attributes[i1+2] );
//                v2 = glm::vec3( attributes[i2+0], attributes[i2+1], attributes[i2+2] );

//                if( isInsideTriangle(v0,v1,v2,point) )
//                {
//                    triangleID = i;
//                    break;
//                }
//            }
//        }

//        numTriangles = mesh->getNumTriangles();
//    }

      // Function trasfered for meshalgorithm.h
//    cornerTableToGlMesh(mesh, scene.objects[0]);
// }



void ROISelectionOperator::adjust()
{
    if( sketchPoints2d.size() < 2 )
        return;

    std::vector<glm::vec2> sketch/* = sketchPoints2d*/;
    sketch.push_back(sketchPoints2d[0]);
    for(unsigned int i = 0; i < sketchPoints2d.size()-1; ++i)
    {
        std::vector<glm::vec2> points;
        bresenham(sketchPoints2d[i],sketchPoints2d[i+1],points);

        sketch.insert(sketch.end(),points.begin()+1,points.end());
    }

    // highlightedTriangles.clear();

    auto firstHalf  = std::vector<glm::vec2>(sketch.begin(),sketch.begin()+sketch.size()/2);
    auto secondHalf = std::vector<glm::vec2>(sketch.begin()+sketch.size()/2,sketch.end());

    std::set<unsigned int> firstHalfTriangles, secondHalfTriangles;

    for(const auto& point : firstHalf)
    {
        int triangleId = -1;
        glm::ivec2 pos(point.x,scene.camera.height-point.y);
        int object;

        scene.pick(pos,object,triangleId);

        if(triangleId >= 0) firstHalfTriangles.insert(triangleId);
       // std::cout << "(" << point.x << ", " << point.y << ") ";
    }
    std::cout << std::endl;

    for(const auto& point : secondHalf)
    {
        int triangleId = -1;
        glm::ivec2 pos(point.x,scene.camera.height-point.y);
        int object;

        scene.pick(pos,object,triangleId);

        if(triangleId >= 0) secondHalfTriangles.insert(triangleId);

       // std::cout << "(" << point.x << ", " << point.y << ") ";
    }
    std::cout << std::endl;

    if( !firstHalfTriangles.empty() )
    {
      std::vector< unsigned int> firstPath, secondPath;
      std::vector< glm::vec3 > firstPathSPoints, secondPathSPoints;
      auto viewport = glm::vec4(0,0,scene.camera.width,scene.camera.height);
      findClosestEdgePath(scene.objects[0], firstHalf, scene.camera.modelMatrix(),
              scene.camera.viewMatrix(), scene.camera.projMatrix(),
              viewport, firstPath, firstPathSPoints, firstHalfTriangles);

      findClosestEdgePath(scene.objects[0], secondHalf,scene.camera.modelMatrix(),
              scene.camera.viewMatrix(), scene.camera.projMatrix(),
              viewport, secondPath, secondPathSPoints, secondHalfTriangles);

      if(secondPath[0]==firstPath.back())
      {
          firstPath.pop_back();
          firstPathSPoints.pop_back();
      }

      if(closed && secondPath.back()!=firstPath[0])
      {
          secondPath.push_back(firstPath[0]);
          secondPathSPoints.push_back(firstPathSPoints[0]);
      }


      // for( const auto& idx : firstPath )
      // {
      //     std::cout << idx << " ";
      // }
      // std::cout << std::endl;

      // for( const auto& idx : secondPath )
      // {
      //     std::cout << idx << " ";
      // }
      // std::cout << std::endl;

      firstPath.insert(firstPath.end(),secondPath.begin(),secondPath.end());
      firstPathSPoints.insert(firstPathSPoints.end(),secondPathSPoints.begin(),secondPathSPoints.end());

      float area = 0.0f;
      for(int i = 0; i < (int)firstPathSPoints.size()-1; ++i)
      {
          auto v0 = firstPathSPoints[i];
          auto v1 = firstPathSPoints[i+1];

          area += v0.x*v1.y - v1.x*v0.y;
      }

      if(area<0)
      {
          std::reverse(firstPath.begin(),firstPath.end());
      }

      for( const auto& idx : firstPath )
          edgePoints.push_back(glm::vec3(scene.camera.modelMatrix() * glm::vec4(scene.objects[0].vertexBuffer[idx].position,1)));

      if(!closed &&
         std::find(firstPath.begin()+1,firstPath.end(),firstPath[0])!=firstPath.end()) closed = true;

      if(closed)
      {
          // std::vector< std::vector< unsigned int > > bordersVector;
          // bordersInsidePolygon((*mesh),firstPath,bordersVector);
          // std::cerr <<
          //            "# borders: " << bordersVector.size() << std::endl <<
          //            "# vertex in [0]: " << bordersVector[0].size()
          //         << std::endl;
          trianglesInsidePolygon((*mesh),firstPath,highlightedTriangles);
          createHole(scene.objects[0],firstPath,highlightedTriangles);
          // if(!bordersVector.empty())
          // {
          //     triangulateHoles(scene.objects[0],bordersVector,scene.camera.modelMatrix(), scene.camera.viewMatrix(), scene.camera.projMatrix(), viewport);
              scene.objects[0].update();
          // }
      }
    }
    else
    {
        std::cerr << "Deu ruim no picking!" << std::endl;
    }
}

