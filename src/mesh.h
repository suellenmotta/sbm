#ifndef MESH_H
#define MESH_H

#include <vector>
#include <string>
#include <set>
#include <glm/glm.hpp>
#include <QOpenGLExtraFunctions>

#include <iomanip>

namespace opengl
{

struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
};

struct TexturedVertex : public Vertex
{
    glm::vec2 texCoord;
};

struct Material
{
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    glm::vec3 shininess;
};

template< typename VertexType = Vertex, int PrimitiveType = GL_TRIANGLES >
class Mesh
        : protected QOpenGLExtraFunctions
{
public:
    Mesh();
    virtual ~Mesh();

    Mesh(Mesh&& rhs) noexcept;
    Mesh(const Mesh&) = delete;

    std::vector< VertexType > vertexBuffer;
    std::vector< unsigned int > indices;

    std::string name;

    Material material;

    glm::mat4 modelMatrix;

    void create();
    void update();
    void draw();

    void bindVBO();

    void createCopy(Mesh &copy, const std::string& name);

private:
    GLuint VBO;
    GLuint EBO;

    bool created = false;
};

using UntexturedMesh = Mesh<Vertex>;
using TexturedMesh = Mesh<TexturedVertex>;

}

//Should not be here:
std::ostream& operator << (std::ostream& output, const glm::vec3& point);


#endif // MESH_H
