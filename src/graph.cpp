#include "graph.h"

#include <list>
#include <queue>
#include <iostream>
#include <map>
#include <algorithm>


Graph::Graph(unsigned int N )
    : G(std::vector< std::vector<Edge> >(N))
    , numEdges(0)
{

}


void Graph::insertEdge( int from, int to, int weight, bool undirected )
{
//    auto it = G[from].begin();
//    for(; it != G[from].end(); ++it)
//    {
//        if((*it).v == to) break;
//    }

//    if(it==G[from].end())
        G[from].push_back({to, weight});
        numEdges++;

    if( undirected )
    {
        insertEdge(to,from,weight,false);
    }
}

const std::vector<Graph::Edge> &Graph::getNeighbors(int node)
{
    return G[node];
}

const unsigned int Graph::getNumEdges()
{
    return numEdges;
}


void Graph::print()
{
    for( unsigned int u = 0; u < G.size(); u++ )
    {
        std::cout << u << ": ";

        if( !G[u].empty() )
        {
            std::cout << G[u][0].v << '(' << G[u][0].w << ')';
        }

        for( unsigned int e = 1; e < G[u].size(); e++ )
        {
            std::cout << ", " << G[u][e].v << '(' << G[u][e].w << ')';
        }

        std::cout << std::endl;
    }
}


void Graph::bfs( int s )
{
    distance = std::vector<int>( G.size(), 1000000 );
    std::vector<Color> color( G.size(), WHITE );

    std::queue<int> next;
    next.push( s );
    color[s] = GRAY;
    distance[s] = 0;

    while( !next.empty() )
    {
        int u = next.front();
        next.pop();

        std::cout << u << " ";

        for( const auto& edge : G[u] )
        {
            if( color[edge.v] == WHITE )
            {
                color[edge.v] = GRAY;
                next.push(edge.v);
            }

            if( distance[edge.v] > distance[u] + 1)
                distance[edge.v] = distance[u] + 1;
        }

        color[u] = BLACK;
    }

    std::cout << std::endl;
}


void Graph::dfs(int s)
{
    std::vector<Color> color( G.size(), WHITE );
    color[s] = GRAY;
    dfs_visit( s, color );
    std::cout << std::endl;
}


void Graph::dfs_visit( int u, std::vector<Color>& color )
{
    std::cout << u << " ";
    for( const auto& edge : G[u] )
    {
        if( color[edge.v] == WHITE )
        {
            color[edge.v] = GRAY;
            dfs_visit( edge.v, color );
        }
    }
    color[u] = BLACK;
}


void Graph::dijkstra( int s )
{
    struct comparator {
     bool operator()(std::pair< int, int > a, std::pair< int, int > b)
     {
        return a.second > b.second;
     }
    };

    std::priority_queue< std::pair< int, int >,
        std::vector< std::pair< int, int > >, comparator > heap;

    distance = std::vector<int>( G.size(), 1000000 );
    parent = std::vector<int>( G.size(), -1 );

    heap.push( std::make_pair(s, 0) );
    distance[s] = 0;

    while( !heap.empty() )
    {
        int u = heap.top().first;
        heap.pop();

        for( const auto& edge : G[u] )
        {
            if (distance[edge.v] > distance[u] + edge.w)
            {
                distance[edge.v] = distance[u] + edge.w;
                parent[edge.v] = u;
                heap.push(std::make_pair(edge.v, distance[edge.v]));
            }
        }
    }
}

