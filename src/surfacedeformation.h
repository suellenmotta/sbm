#ifndef SURFACEDEFORMATION_H
#define SURFACEDEFORMATION_H

#include "mesh.h"
#include <Eigen/Core>

struct EigenMesh
{
    Eigen::MatrixXd V;
    Eigen::MatrixXi F;
    Eigen::MatrixXd N;
};

class SurfaceDeformation
{
public:
    SurfaceDeformation(opengl::UntexturedMesh& surface);

    virtual void run() = 0;

protected:
    void meshToEigen(const opengl::UntexturedMesh& mesh, EigenMesh& eMesh);
    void eigenToMesh(const EigenMesh& eMesh, opengl::UntexturedMesh& mesh, bool computeNormals = false);

    opengl::UntexturedMesh& surface;
};

#endif // SURFACEDEFORMATION_H
