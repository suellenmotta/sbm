#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "canvas3dwidget.h"
#include "colorslist.h"

#include "surfaceeditingoperator.h"
#include "surfacereader.h"
#include "sketchsurfacegenerator.h"

#include <QFileDialog>
#include <QColorDialog>
#include <QClipboard>
#include <QDebug>

//For tests
#include <iostream>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , modelingWidget(nullptr)
{
    ui->setupUi(this);

    connect(ui->openGLWidget,
            SIGNAL(updateMousePositionText(const QString&)),
            statusBar(),
            SLOT(showMessage(const QString&)));

    ui->rightSideWidget->hide();
}

MainWindow::~MainWindow()
{
    delete modelingWidget;
    delete ui;
}

void MainWindow::enableInlineSliceOptions(bool enable)
{
    ui->inlineLabel->setEnabled(enable);
    ui->inlineSpinBox->setEnabled(enable);
    ui->inlineStepLabel->setEnabled(enable);
    ui->inlineStepSpinBox->setEnabled(enable);
}

void MainWindow::enableCrosslineSliceOptions(bool enable)
{
    ui->crosslineLabel->setEnabled(enable);
    ui->crosslineSpinBox->setEnabled(enable);
    ui->crosslineStepLabel->setEnabled(enable);
    ui->crosslineStepSpinBox->setEnabled(enable);
}

void MainWindow::enableDepthSliceOptions(bool enable)
{
    ui->depthLabel->setEnabled(enable);
    ui->depthSpinBox->setEnabled(enable);
    ui->depthStepLabel->setEnabled(enable);
    ui->depthStepSpinBox->setEnabled(enable);
}

void MainWindow::addSurface(const std::string &name)
{
    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText(QString::fromStdString(name));
    item->setCheckState(Qt::CheckState::Checked);
}

void MainWindow::on_actionLoadVolume_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        "Load volume", "", "Todos os arquivos (*.*);;DAT Files (*.dat)");

    QFileInfo check(fileName);

    if( check.exists() && check.isFile() )
    {
        auto widget = findChild<Canvas3DWidget*>("openGLWidget");
        widget->loadVolume(fileName);
        widget->update();
    }

    setupSpinBoxes();
}

void MainWindow::on_actionLoadModel_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load mesh"), "", "Todos os arquivos (*.*);;OBJ Files (*.obj);;OFF Files (*.off)");

    QFileInfo check(fileName);

    if( check.exists() && check.isFile() )
    {
        auto widget = findChild<Canvas3DWidget*>("openGLWidget");
        widget->loadObject(fileName);
        widget->update();

        QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
        item->setText(check.fileName());
        item->setCheckState(Qt::CheckState::Checked);
    }
}

void MainWindow::on_actionSeismicScene_triggered()
{
    ui->actionDeformSurface->setChecked(false);

    QString labelPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/f3-onesalt-benchmark-label.dat");
    QString seismicPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/f3-onesalt-benchmark-seismic.dat");

    auto widget = findChild<Canvas3DWidget*>("openGLWidget");
    widget->setBackgroundColor({0,0,0});
    widget->clearScene();

    auto& renderer = widget->getScene().getVolumeRenderer();

    bool labels = true;

    if(labels)
    {
        widget->loadVolume(seismicPath,"subbenchmark");

        std::vector<float> colorTable = {
            0.270588f,0.458824f,0.705882f, //dark-blue
            0.568627f,0.749020f,0.858824f, //blue
            0.878431f,0.952941f,0.972549f, //light-blue
            0.996078f,0.878431f,0.564706f, //yellow
            0.988235f,0.552941f,0.349020f, //orange
            0.843137f,0.188235f,0.152941f  //red
        };
        std::vector<float> markers = {0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f};
        renderer.setColorTable(colorTable,markers,true);
        renderer.setMinPixelValue(-1.0f);
        renderer.setMaxPixelValue(+1.0f);


        NewVolume* mixVolume = seismic::readSeismicVolume(labelPath.toStdString(),
                                                          widget->getSeismicInfo());
        renderer.setMixVolume(mixVolume);
    }
    else
    {
        widget->loadVolume(seismicPath,"subbenchmark");

        renderer.removeColorTable();
        renderer.setMinPixelValue(-0.6f);
        renderer.setMaxPixelValue(+0.6f);
    }

    ui->horizonsListWidget->clear();

    auto color = glm::vec3(0.6, 0, 0);

//    QString sketchedModel = tr("C:/Users/suellen/Projects/sbm/build-sbm-Desktop_Qt_5_9_0_MSVC2015_64bit-Release/salt-0.off");
//    widget->loadObject(sketchedModel,color, "edited");
//    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
//    item->setText("edited");
//    item->setCheckState(Qt::CheckState::Checked);

    QString dir = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/F3Salts/");

    for(int i = 0; i <= 1; ++i)
    {
        QString name = QString("salt-%1").arg(0);
        widget->loadObject(dir + name + ".off",
                           color, name);

        QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
        item->setText(name);
        item->setCheckState(Qt::CheckState::Checked);
    }

    setupSpinBoxes();
    widget->update();

//#ifdef __linux__
//    QString dir = "/local/suellen/seismicdata/SEAMhorizons/";
//#elif _WIN32

//#if VOLUME == SUBF3
//    QString dir = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/F3Salts/");
//#elif VOLUME == SUBSEAM
//    QString dir = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMSalts/");
//#elif VOLUME == SEAM
//    QString dir = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SEAMHorizons/");
//#endif

//#endif


////#if VOLUME == SUBF3
////    QString gtPath("C:/Users/suellen/Google Drive/Doutorado/Tese/Dados/F3/F3-salt-mozart-ballpivot.off");
////    widget->loadObject(gtPath,colors[19],QString("ground-truth"));
////    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
////    item->setText(QString("ground-truth"));
////    item->setCheckState(Qt::CheckState::Checked);
////#endif

//#elif VOLUME == SEAM
//    ui->horizonsListWidget->clear();
//    for(int i = 1; i < 10; ++i)
//    {
//        QString name = QString("mapa%1").arg(i);
//        widget->loadObject(dir + name + ".off",
//                           colors[(i-1)%NUM_COLORS], name);

//        QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
//        item->setText(name);
//        item->setCheckState(Qt::CheckState::Checked);
//    }
//#endif



//    ui->openGLWidget->makeCurrent();
//    auto context = ui->openGLWidget->context();
//    auto functions = context->functions();
//    auto vendor = reinterpret_cast<const char*>(functions->glGetString(GL_VENDOR));
//    auto renderer = reinterpret_cast<const char*>(functions->glGetString(GL_RENDERER));
//    auto version = reinterpret_cast<const char*>(functions->glGetString(GL_VERSION));
//    qDebug() << "OpenGL vendor: " << vendor << " "
//             << "renderer: " << renderer << " "
//             << "version: " << version;

}


void MainWindow::on_actionModelTestScene_triggered()
{
//    using Sketch = SketchInterpolationGenerator::Sketch;
//    std::vector<Sketch> origSketches(3);

//    SketchInterpolationGenerator::readXYZ("sketch-2-0-0.xyz",origSketches[0]);
//    SketchInterpolationGenerator::readXYZ("sketch-2-539-0.xyz",origSketches[1]);
//    SketchInterpolationGenerator::readXYZ("sketch-2-889-0.xyz",origSketches[2]);

//    SketchInterpolationGenerator gen(origSketches);

//    opengl::UntexturedMesh mesh;
//    gen.run(mesh);

//    std::vector<glm::vec3> vertices;
//    vertices.reserve(mesh.vertexBuffer.size());
//    for(auto v : mesh.vertexBuffer)
//        vertices.push_back(v.position);
//    saveOffFile("output-mesh.off",vertices,mesh.indices);





//    return;


    ui->actionDeformSurface->setChecked(false);

    QString labelPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/seam-br-label.dat");
    QString seismicPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/seam-br-seismic.dat");

    auto widget = ui->openGLWidget;
    widget->setBackgroundColor({0,0,0});
    widget->clearScene();

    bool labels = true;
    auto& renderer = widget->getScene().getVolumeRenderer();

    if(labels)
    {
        widget->loadVolume(seismicPath,"seam-br");

        std::vector<float> colorTable = {
            0.878431f,0.952941f,0.972549f, //light-blue
            0.270588f,0.458824f,0.705882f, //dark-blue
            0.843137f,0.188235f,0.152941f, //red
            0.568627f,0.749020f,0.858824f, //blue
            0.996078f,0.878431f,0.564706f, //yellow
            0.988235f,0.552941f,0.349020f, //orange
        };
        std::vector<float> markers = {0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f};
        renderer.setColorTable(colorTable,markers,true);
        renderer.setMinPixelValue(-93.0f);
        renderer.setMaxPixelValue(+88.0f);


        NewVolume* mixVolume = seismic::readSeismicVolume(labelPath.toStdString(),
                                                          widget->getSeismicInfo());
        renderer.setMixVolume(mixVolume);
    }

    else
    {   
        QString fileName = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/seam-br-label.dat");
        widget->loadVolume(fileName,"seam-br");

        renderer.removeColorTable();
        renderer.setMinPixelValue(0.0f);
        renderer.setMaxPixelValue(1.0f);
    }

    ui->horizonsListWidget->clear();

    QString name = QString("sphere");
    widget->createAndLoadSphere(30,{0.170588f,0.358824f,0.605882f},name,0.3f);

    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("sphere");
    item->setCheckState(Qt::CheckState::Checked);

    //Uncomment to generate a sphere copy
//    auto& objs = widget->getScene().objects;
//    opengl::UntexturedMesh copy;
//    objs[objs.size()-1].createCopy(copy,"sphere_copy");
//    widget->getScene().addObject(std::move(copy));
//    QListWidgetItem* cpItem = new QListWidgetItem(ui->horizonsListWidget);
//    cpItem->setText("sphere_copy");
//    cpItem->setCheckState(Qt::CheckState::Checked);

    QString fileName = tr("initial-surface-1.off");
    widget->loadObject(fileName, widget->nextColor(), "salt-initial");
    item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("salt-initial");
    item->setCheckState(Qt::CheckState::Checked);

    widget->update();
    setupSpinBoxes();
}

void MainWindow::on_actionBabyGEScene_triggered()
{
#ifdef __linux__
    QString fileName = "C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.nrrd";
#elif _WIN32
    QString fileName = "C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.nrrd";
#endif

    auto widget = findChild<Canvas3DWidget*>("openGLWidget");
    widget->setBackgroundColor({0,0,0});
    widget->clearScene();
    widget->loadVolume(fileName);

#ifdef __linux__
    widget->loadObject("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.off", {0.45f,0.45f,0.45f});
#elif _WIN32
    widget->loadObject("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/BabyGE/Baby.off", {0.45f,0.45f,0.45f});
#endif

    widget->update();

    ui->horizonsListWidget->clear();
    QListWidgetItem* item = new QListWidgetItem(ui->horizonsListWidget);
    item->setText("Baby GE");
    item->setCheckState(Qt::CheckState::Checked);

    setupSpinBoxes();
}

void MainWindow::on_actionWireframe_toggled(bool wireframeOn)
{
    findChild<Canvas3DWidget*>("openGLWidget")->setWireframeOn(wireframeOn);
}

void MainWindow::on_actionSnapshot_triggered()
{
    auto image = findChild<Canvas3DWidget*>("openGLWidget")->grabFramebuffer();

    QApplication::clipboard()->setImage(image);
}

void MainWindow::on_actionSelectRegion_toggled(bool operatorOn)
{
    if( operatorOn )
        findChild<Canvas3DWidget*>("openGLWidget")->setROISelectionOperator();
    else
        findChild<Canvas3DWidget*>("openGLWidget")->setOperator(nullptr);
}

void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::on_showInlineCheckbox_clicked(bool checked)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->showZSlice(checked);
        widget->update();
    }

    //enableInlineSliceOptions(checked);
}

void MainWindow::on_showCrosslineCheckbox_clicked(bool checked)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->showYSlice(checked);
        widget->update();
    }

    //enableCrosslineSliceOptions(checked);
}

void MainWindow::on_showDepthSliceCheckbox_clicked(bool checked)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->showXSlice(checked);
        widget->update();
    }

    //enableDepthSliceOptions(checked);
}

void MainWindow::on_inlineSpinBox_valueChanged(int value)
{
   auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->updateZSlice(value);
        widget->update();
    }
}

void MainWindow::on_crosslineSpinBox_valueChanged(int value)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->updateYSlice(value);
        widget->update();
    }
}

void MainWindow::on_depthSpinBox_valueChanged(int value)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        widget->updateXSlice(value);
        widget->update();
    }
}

void MainWindow::on_inlineStepSpinBox_valueChanged(int value)
{
    ui->inlineSpinBox->setSingleStep(value);
}

void MainWindow::on_crosslineStepSpinBox_valueChanged(int value)
{
    ui->crosslineSpinBox->setSingleStep(value);
}

void MainWindow::on_depthStepSpinBox_valueChanged(int value)
{
    ui->depthSpinBox->setSingleStep(value);
}

void MainWindow::on_horizonsListWidget_itemChanged(QListWidgetItem *item)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(widget->isValid())
    {
        auto index = ui->horizonsListWidget->row(item);
//        qDebug() << index;

        widget->showObject(index,item->checkState() == Qt::Checked);

        widget->update();
    }
}

void MainWindow::setupSpinBoxes()
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    unsigned int xSliceFirst;
    unsigned int xSliceLast;
    unsigned int xSliceStep;
    unsigned int ySliceFirst;
    unsigned int ySliceLast;
    unsigned int ySliceStep;
    unsigned int zSliceFirst;
    unsigned int zSliceLast;
    unsigned int zSliceStep;
    widget->getVolumeBounds(
                xSliceFirst,
                xSliceLast,
                xSliceStep,
                ySliceFirst,
                ySliceLast,
                ySliceStep,
                zSliceFirst,
                zSliceLast,
                zSliceStep);

    ui->inlineSpinBox->setRange(zSliceFirst,zSliceLast);
    ui->inlineSpinBox->setSingleStep(zSliceStep);
    ui->crosslineSpinBox->setRange(ySliceFirst,ySliceLast);
    ui->crosslineSpinBox->setSingleStep(ySliceStep);
    ui->depthSpinBox->setRange(xSliceFirst,xSliceLast);
    ui->depthSpinBox->setSingleStep(xSliceStep);
    ui->inlineStepSpinBox->setRange(zSliceStep,zSliceLast);
    ui->inlineStepSpinBox->setSingleStep(zSliceStep);
    ui->crosslineStepSpinBox->setRange(ySliceStep,zSliceLast);
    ui->crosslineStepSpinBox->setSingleStep(ySliceStep);
    ui->depthStepSpinBox->setRange(xSliceStep,xSliceLast);
    ui->depthStepSpinBox->setSingleStep(xSliceStep);

    ui->inlineSpinBox->setValue(zSliceFirst);
    ui->crosslineSpinBox->setValue(ySliceFirst);
    ui->depthSpinBox->setValue(xSliceLast);
    ui->inlineStepSpinBox->setValue(zSliceStep);
    ui->crosslineStepSpinBox->setValue(ySliceStep);
    ui->depthStepSpinBox->setValue(xSliceStep);

    ui->rightSideWidget->show();
}





void MainWindow::on_actionBackgroundColor_triggered()
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");
    auto color = widget->getBackgroundColor();
    auto newColor = QColorDialog::getColor(color,this,"Select background color");
    widget->setBackgroundColor(newColor);
    widget->update();
}

void MainWindow::on_actionBorderPoints_toggled(bool viewBorderPoints)
{
    auto widget = findChild<Canvas3DWidget*>("openGLWidget");

    if(viewBorderPoints)
    {
        auto borderOperator = widget->createBorderSelectionOperator();
        auto sketchingOperator = widget->createSliceSketchingOperator();

        surfaceModelingWidget->setBorderOperator(borderOperator);
        surfaceModelingWidget->setSliceSketchingOperator(sketchingOperator);
        surfaceModelingWidget->show();
    }

    else
    {
        surfaceModelingWidget->hide();

        widget->setOperator(nullptr);
        surfaceModelingWidget->setBorderOperator(nullptr);
        surfaceModelingWidget->setSliceSketchingOperator(nullptr);
    }

    widget->update();
}


void MainWindow::on_actionDeformSurface_toggled(bool deform)
{
    if(modelingWidget)
    {
//        modelingWidget->setSliceSketchingOperator(nullptr);
        ui->openGLWidget->setOperator(nullptr);

        removeDockWidget(modelingWidget);
        delete modelingWidget;
        modelingWidget = nullptr;
    }

    if(deform)
    {
        modelingWidget = new SketchBasedModelingWidget(this,ui->openGLWidget);
//        auto sketchingOperator = ui->openGLWidget->createSurfaceEditingOperator();
//        modelingWidget->setSliceSketchingOperator(sketchingOperator);

        addDockWidget(Qt::LeftDockWidgetArea, modelingWidget);
        modelingWidget->setFloating(false);
        modelingWidget->show();
    }
}

void MainWindow::on_actionResetCamera_triggered()
{
    ui->openGLWidget->resetCamera();
}

void MainWindow::on_actionSaveModel_triggered()
{
    auto index = ui->horizonsListWidget->currentRow();

    if(index<0)
    {
        QMessageBox msgBox;
        msgBox.setText("No model selected!");
        msgBox.exec();
        return;
    }

    auto name = ui->horizonsListWidget->currentItem()->text();

    QString path = QFileDialog::getSaveFileName(this,
        "Save model", name.append(".off"), "OFF Files (*.off)");

    if(!path.isEmpty())
    {
        QMessageBox msgBox;
        if(ui->openGLWidget->saveObject(path,index))
            msgBox.setText("Model file successfully saved!");
        else
            msgBox.setText("Error saving model file!");
        msgBox.exec();
    }
}

void MainWindow::on_actionANPScene_triggered()
{
    ui->actionDeformSurface->setChecked(false);

    QString seismicPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/ANP.dat");
    QString horribleMeshPath = tr("C:/Users/suellen/Google Drive/Doutorado/Tese/SBM/SuperficiesANP/surface_horrivel_1.off");

    auto widget = ui->openGLWidget;
    widget->setBackgroundColor({0,0,0});
    widget->clearScene();

    widget->loadVolume(seismicPath,"anp");

    auto& renderer = widget->getScene().getVolumeRenderer();
    renderer.removeColorTable();

    //Temporary
    renderer.setMinPixelValue(-54234.02734375f);
    renderer.setMaxPixelValue(56352.41796875f);

    QString name = QString("sphere");
    widget->createAndLoadSphere(256,{0.170588f,0.358824f,0.605882f},name,0.3f,{250.f,0.f,0.f});
    widget->createAndLoadSphere(256,{0.170588f,0.358824f,0.605882f},name,0.3f,{250.f,0.f,0.f});
    widget->createAndLoadSphere(60,{0.170588f,0.358824f,0.605882f},name,0.3f,{250.f,0.f,0.f});

    widget->loadObject(horribleMeshPath);

    QListWidgetItem* sphereItem = new QListWidgetItem(ui->horizonsListWidget);
    sphereItem->setText("sphere");
    sphereItem->setCheckState(Qt::CheckState::Checked);

    sphereItem = new QListWidgetItem(ui->horizonsListWidget);
        sphereItem->setText("sphere");
        sphereItem->setCheckState(Qt::CheckState::Checked);

        sphereItem = new QListWidgetItem(ui->horizonsListWidget);
            sphereItem->setText("sphere");
            sphereItem->setCheckState(Qt::CheckState::Checked);

    QListWidgetItem* surfaceItem = new QListWidgetItem(ui->horizonsListWidget);
    surfaceItem->setText("superficie-horrivel");
    surfaceItem->setCheckState(Qt::CheckState::Checked);

    widget->update();
    setupSpinBoxes();

}
