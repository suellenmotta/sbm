#include "surfacedeformation.h"

SurfaceDeformation::SurfaceDeformation(opengl::UntexturedMesh &surface)
    : surface(surface)
{

}

void SurfaceDeformation::meshToEigen(const opengl::UntexturedMesh &mesh, EigenMesh &eMesh)
{
    auto numVertices = mesh.vertexBuffer.size();
    auto numTriangles = mesh.indices.size()/3;

    eMesh.V.resize(numVertices,3);
    eMesh.N.resize(numVertices,3);
    eMesh.F.resize(numTriangles,3);

    for(unsigned int i = 0; i < numVertices; ++i)
    {
        eMesh.V.row(i) = Eigen::RowVector3d(
            mesh.vertexBuffer[i].position.x,
            mesh.vertexBuffer[i].position.y,
            mesh.vertexBuffer[i].position.z
        );

        eMesh.N.row(i) = Eigen::RowVector3d(
            mesh.vertexBuffer[i].normal.x,
            mesh.vertexBuffer[i].normal.y,
            mesh.vertexBuffer[i].normal.z
        );
    }

    for(unsigned int i = 0; i < numTriangles; ++i)
    {
        eMesh.F.row(i) = Eigen::RowVector3i(
            mesh.indices[3*i + 0],
            mesh.indices[3*i + 1],
            mesh.indices[3*i + 2]
        );
    }
}

void SurfaceDeformation::eigenToMesh(const EigenMesh &eMesh, opengl::UntexturedMesh &mesh, bool computeNormals)
{
    mesh.vertexBuffer.clear();
    mesh.indices.clear();

    auto numVertices = eMesh.V.rows();
    auto numTriangles = eMesh.F.rows();

    mesh.vertexBuffer.resize(numVertices);
    mesh.indices.resize(numTriangles*3);

    for(unsigned int i = 0; i < numVertices; ++i)
    {
        mesh.vertexBuffer[i].position = {
            eMesh.V(i,0),
            eMesh.V(i,1),
            eMesh.V(i,2)
        };
    };

    if(!eMesh.N.rows()>0)
    {
        for(unsigned int i = 0; i < numVertices; ++i)
        {
            mesh.vertexBuffer[i].normal = {
                eMesh.N(i,0),
                eMesh.N(i,1),
                eMesh.N(i,2)
            };
        };
    }
    else if(computeNormals)
    {
        //TODO: compute normals.
    }

    for(unsigned int i = 0; i < numTriangles; ++i)
    {
        mesh.indices[3*i + 0] = eMesh.F(i,0);
        mesh.indices[3*i + 1] = eMesh.F(i,1);
        mesh.indices[3*i + 2] = eMesh.F(i,2);
    }
}
