#include "harmonicsurfacedeformation.h"


#include <igl/harmonic.h>
#include <igl/per_vertex_normals.h>


HarmonicSurfaceDeformation::HarmonicSurfaceDeformation(opengl::UntexturedMesh &surface)
    : SurfaceDeformation(surface)
    , k(2)
{

}

void HarmonicSurfaceDeformation::setK(unsigned int k)
{
    this->k = k;
}

void HarmonicSurfaceDeformation::setNewPositions(std::map<unsigned int, glm::vec3> &positions)
{
    const auto& vertices = surface.vertexBuffer;

    for(const auto& pos : positions)
    {
        auto idx = pos.first;
        displacements[idx] = pos.second-vertices[idx].position;
    }
}

void HarmonicSurfaceDeformation::setFixedVertices(std::list<unsigned int> &fixedList)
{
    fixedVertices = fixedList;
}

void HarmonicSurfaceDeformation::run()
{
    EigenMesh eMesh;
    meshToEigen(surface,eMesh);

    //b contains all vertices in the 'handle'
    Eigen::VectorXi b;
    b.resize(displacements.size());
    int bi = 0;
    for(const auto& d : displacements)
    {
        b[bi++] = d.first;
    }

    Eigen::MatrixXd V_bc, D_bc;
    V_bc.resize(b.size(),eMesh.V.cols());
    D_bc.resize(b.size(),eMesh.V.cols());
    bi = 0;
    for(auto& disp : displacements)
    {
        V_bc.row(bi) = eMesh.V.row(b(bi));

        auto& d = disp.second;
        D_bc.row(bi) = Eigen::RowVector3d(d.x,d.y,d.z);
        bi++;
    }

    Eigen::MatrixXd D; //result
    igl::harmonic(eMesh.V,eMesh.F,b,D_bc,k,D);

    eMesh.V = eMesh.V+D;

    igl::per_vertex_normals(eMesh.V,eMesh.F,eMesh.N);

    eigenToMesh(eMesh,surface,true);
}
