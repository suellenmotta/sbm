#ifndef CANVAS3DWIDGET_H
#define CANVAS3DWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLExtraFunctions>
#include <QVector2D>
#include <QMessageBox>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)
QT_FORWARD_DECLARE_CLASS(QOpenGLTexture)

#include "mesh.h"
#include "operator.h"
#include "seismicvolume.h"

#include "borderselectionoperator.h"
#include "slicesketchingoperator.h"
#include "surfaceeditingoperator.h"

#include <memory>

class Canvas3DWidget
    : public QOpenGLWidget
    , protected QOpenGLExtraFunctions
{
Q_OBJECT

public:
    explicit Canvas3DWidget(QWidget *parent = 0);
    ~Canvas3DWidget();

    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
    void setWireframeOn(bool on);

    void setBackgroundColor(const QColor& color);
    QColor getBackgroundColor();

    void resetCamera();
    void clearScene();
    void loadCube();
    void loadSphere();
    void createAndLoadSphere(int resolution, const glm::vec3 &color,
                             const QString &name, float rescale = 1.0f, const glm::vec3 &translation = {0.0f,0.0f,0.0f});
    void loadBunny();
    void loadVolume(const QString& path, const std::string &volume = "f3"); //f3 = gambiarra
    void loadObject(const QString &path,
                    const glm::vec3& color = glm::vec3(0.4,0.4,0.4),
                    const QString &name = "");
    bool saveObject(const QString& path, unsigned int objIndex);
    const NewVolume* getVolume();
    void setVolume(NewVolume* volume);

    Scene& getScene();
    void getVolumeBounds(unsigned int& xSliceFirst,
                         unsigned int& xSliceLast,
                         unsigned int& xSliceStep,
                         unsigned int& ySliceFirst,
                         unsigned int& ySliceLast,
                         unsigned int& ySliceStep,
                         unsigned int& zSliceFirst,
                         unsigned int& zSliceLast,
                         unsigned int& zSliceStep);

    seismic::Info getSeismicInfo();

    void updateXSlice(unsigned int sliceNum);
    void updateYSlice(unsigned int sliceNum);
    void updateZSlice(unsigned int sliceNum);

    void showXSlice(bool show = true);
    void showYSlice(bool show = true);
    void showZSlice(bool show = true);

    void showObject(unsigned int objIndex, bool show = true);

    glm::vec3 nextColor();

    void setROISelectionOperator();

    void setOperator(std::shared_ptr<Operator> op );
    std::shared_ptr<Operator> getOperator();

    std::shared_ptr<BorderSelectionOperator> createBorderSelectionOperator();
    std::shared_ptr<SliceSketchingOperator> createSliceSketchingOperator();
    std::shared_ptr<SurfaceEditingOperator> createSurfaceEditingOperator();

signals:
    void clicked();
    void updateMousePositionText(const QString& message);
    void viewChanged();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private:
    glm::ivec2 oldPos;

    Scene scene;
    seismic::Info info;

    std::shared_ptr<Operator> currentOperator;

    unsigned int currentColor = {0u};

};

#endif // CANVAS3DWIDGET_H
