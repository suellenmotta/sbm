#include "surfaceslicing.h"

#include <unordered_map>
#include <glm/gtc/epsilon.hpp>
#include <iostream>
#include <fstream>
#include <iomanip>

void writeOFF2( const std::string& path, const std::vector<glm::vec3>& vertices,
                          const std::vector<unsigned int>& indices)
{
    std::ofstream out( path );
    if (!out)
    {
        printf( "Error writing the file %s\n", path.c_str( ) );
        return;
    }

    out << "OFF\n";

    auto numElements = indices.size( ) / 3;
    out << vertices.size( ) << " " << numElements << " 0\n";

    for (const auto& p : vertices)
    {
        out << p.x << " " << p.y << " " << p.z << "\n";
    }

    for (unsigned int i = 0, pos = 0; i < numElements; i++, pos += 3)
    {
        out << "3 ";
        out << indices[pos + 0] << " "
                                << indices[pos + 1] << " "
                                << indices[pos + 2] << "\n";
    }

    out.close( );
}

SurfaceSlicing::SurfaceSlicing(const opengl::UntexturedMesh &surface,
                               NewVolume::Info volumeInfo)
    : surface(surface)
    , info(volumeInfo)
{
    spacing[0] = glm::distance(info.p2,info.p1)/(info.numXSlices-1.0f);
    spacing[1] = glm::distance(info.p1,info.p5)/(info.numYSlices-1.0f);
    spacing[2] = glm::distance(info.p4,info.p1)/(info.numZSlices-1.0f);

    sliceNormals[0] = glm::normalize(glm::cross(info.p5-info.p1,info.p4-info.p1));
    sliceNormals[1] = glm::normalize(glm::cross(info.p2-info.p1,info.p4-info.p1));
    sliceNormals[2] = glm::normalize(glm::cross(info.p1-info.p5,info.p6-info.p5));

    buildIntersectionMap();
}

SurfaceSlicing::SliceCurves
    SurfaceSlicing::getSliceCurves(NewVolume::Axis axis, unsigned int slice)
{
    SliceCurves curves;
    curves.reserve(edges[axis][slice].size());

    for(auto& curveEdges : edges[axis][slice])
    {
        CurvePoints curve;
        curve.reserve(curveEdges.size());

        for(auto& edge : curveEdges)
        {
            curve.push_back(edge.p);
        }

        curves.push_back(curve);
    }

    return curves;
}

SurfaceSlicing::SliceCrossingEdges &SurfaceSlicing::getSliceCrossingEdges(NewVolume::Axis axis, unsigned int slice)
{
    return edges[axis][slice];
}

std::vector<SurfaceSlicing::TriangleList>&
    SurfaceSlicing::getInterceptedTriangles(NewVolume::Axis axis, unsigned int slice)
{
    return interceptedTriangles[axis][slice];
}

glm::vec3 SurfaceSlicing::getSliceNormal(NewVolume::Axis axis)
{
    return sliceNormals[axis];
}

float SurfaceSlicing::getVertexZCoord(unsigned int vertex, NewVolume::Axis axis)
{
    return zCoords[axis][vertex];
}

float SurfaceSlicing::getSliceZCoord(unsigned int slice, NewVolume::Axis axis)
{
    return slice*spacing[axis];
}

void SurfaceSlicing::sliceX()
{
    sliceAxis(NewVolume::X);
}

void SurfaceSlicing::sliceY()
{
    sliceAxis(NewVolume::Y);
}

void SurfaceSlicing::sliceZ()
{
    sliceAxis(NewVolume::Z);
}

void SurfaceSlicing::sliceAxis(NewVolume::Axis axis)
{
    computeZCoords(axis);

    const auto& triangles = surface.indices;

    auto n = triangles.size() / 3;

    auto k = axis == NewVolume::X ? info.numXSlices :
            (axis == NewVolume::Y ? info.numYSlices : info.numZSlices);

    //Compute z-min and z-max for each triangle of the mesh
    std::vector<float> zMin(n);
    std::vector<float> zMax(n);
    for(unsigned int t = 0; t < n; ++t)
    {
        auto idx = 3*t;
        auto id0 = triangles[idx+0];
        auto id1 = triangles[idx+1];
        auto id2 = triangles[idx+2];

        zMin[t] = std::min(zCoords[axis][id0],std::min(zCoords[axis][id1],zCoords[axis][id2]));
        zMax[t] = std::max(zCoords[axis][id0],std::max(zCoords[axis][id1],zCoords[axis][id2]));
    }

    //Build triangle array L: an array of size k + 1, where L[i] is a list that
    //contains all triangles whose z-min is less than the z-coordinate of the slice i.
    std::vector< std::list< unsigned int > > L(k+1);
    auto firstZ = 0.0f;
    auto lastZ = (k-1)*spacing[axis];
    for(unsigned int t = 0; t < n; ++t)
    {
        if(zMin[t] < firstZ)
            L[0].push_back(t);
        else if(zMin[t] > lastZ)
            L[k].push_back(t);
        else
            L[zMin[t]/spacing[axis]+1].push_back(t);
    }

    std::string axisStr;
    switch (axis)
    {
        case NewVolume::X: axisStr = "X"; break;
        case NewVolume::Y: axisStr = "Y"; break;
        case NewVolume::Z: axisStr = "Z"; break;
    }



    //Compute all intersections using the lists in L to speed up the process
    //The result is a vector S of size k where S[i] is a list of segments that intersect the slice i.
    std::vector< std::list< NewSegment > > S(k);
    std::list< unsigned int > A;
    float spc = spacing[axis];
    float currentZ = 0.0f;
    for(unsigned int i = 0; i < k; ++i)
    {
        A.insert(A.end(),L[i].begin(),L[i].end());

        for(auto tIt = A.begin(); tIt != A.end();)
        {
            auto t = *tIt;

            if(zMax[t] < currentZ)
            {
                tIt = A.erase(tIt);
            }
            else
            {
                auto intersection = computeIntersection(t,axis,i);

                if(intersection.size()==2)
                {
                    S[i].push_back({intersection[0],intersection[1],t});
                }
                else if(intersection.size()==3)
                {
                    std::cerr <<
                         "  [SurfaceSlicing::sliceAxis] Warning: Case not treated: full triangle interception on axis "
                         << axisStr << " slice " << k << ". Discontinuous curves may appear.\n";
                }

                ++tIt;
            }
        }
        currentZ += spc;
    }

    struct EdgeCompare {
        bool operator () (const Edge& e, const Edge& f) const {
            return (e.u == f.u && e.v == f.v) ||
                   (e.u == f.v && e.v == f.u);
        }
    };

    struct SimpleHash {
        size_t operator()(const Edge& e) const {
            return e.u ^ e.v;
        }
    };



    using Hash = std::unordered_map< Edge,std::vector<Edge>,SimpleHash,EdgeCompare >;


    //Build the hash of segments for sorting
    auto buildHash = [&](const std::list< NewSegment >& segments, Hash& H)
    {
        H.clear();

        auto processSegment = [&](Edge e, Edge f)
        {
            auto it = H.find(e);
            if(it==H.end())
            {
                H[e] = {f};
            }
            else
            {
                if(it->second[0] != f)
                {
                    if(it->second.size()<2)
                    {
                        it->second.push_back(f);
                    }
                    else
                    {
                        if(it->second[1] != f)
                            std::cerr <<
                                 "  [SurfaceSlicing::sliceAxis] Warning: Hash table collision on axis "
                                 << axisStr << " slice " << k << ". Discontinuous curves may appear.\n";
                    }
                }
            }
        };

        for(auto& segment : segments)
        {
            auto& e0 = segment.e0;
            auto& e1 = segment.e1;

            processSegment(e0,e1);
            processSegment(e1,e0);
        }
    };

    //Sort the segments according to the hash built with buildHash()
    auto sortSegments = [](Hash& H, SliceCrossingEdges& crossingEdges)
    {
        auto getNextIt = [&]()
        {
            for(auto it = H.begin(); it != H.end(); ++it)
            {
                if(it->second.size() < 2)
                    return it;
            }

            return H.begin();
        };

        crossingEdges.clear();
//        interceptedTriangles.clear();

        while(!H.empty())
        {
            std::vector< Edge > edges;
//            std::vector< unsigned int > curveTriangles;

            auto first = getNextIt()->first;
            edges.push_back(first);

            Edge cur = H[first][0];
            Edge prev = first;

            H.erase(first);

            bool endOfCurve = false;
            do
            {
                edges.push_back(cur);

                if(H[cur].size() < 2)
                {
                    H.erase(cur);
                    endOfCurve = true;
                }

                else
                {
                    auto nextIdx = (H[cur][0] != prev) ? 0 : 1;
                    auto next = H[cur][nextIdx];

                    H.erase(cur);

                    prev = cur;
                    cur = next;
                }

            } while(!endOfCurve && cur != first && H.find(cur) != H.end());

            if(cur == first)
                edges.push_back(first); //close the curve

            crossingEdges.emplace_back(edges);
//            interceptedTriangles.emplace_back(curveTriangles);
        }
    };

    curves[axis].resize(k);
    edges[axis].resize(k);
    interceptedTriangles[axis].resize(k);
    for(unsigned int i = 0; i < S.size(); ++i)
    {
        Hash H;
        buildHash(S[i],H);

        sortSegments(H,edges[axis][i]);

//        if(edges[axis][i].size()>1)
//        {
//            std::cerr << "Discontinuity in axis " << axis << " slice " << i << std::endl;
//        }
    }
}

void SurfaceSlicing::sliceAll()
{
    sliceX();
    sliceY();
    sliceZ();
}

std::map<unsigned int,glm::vec3> SurfaceSlicing::computeNewTrianglePosition(NewVolume::Axis axis, unsigned int slice,
        const SurfaceSlicing::Segment &oldSegment, const Segment &newSegment, std::map<unsigned int, glm::vec3>& newPositions)
{
    //v0 is the isolated vertex
    //oldSegment.u and newSegment.u must be in the segment v0-v1
    //oldSegment.v and newSegment.v must be in the segment v0-v2
    auto computeNewPositions = [&](const glm::vec3& v0, const glm::vec3& v1,
            const glm::vec3& v2, const Segment& oldSegment, const Segment& newSegment)
    {
        auto m0 = (oldSegment.u + oldSegment.v) * 0.5f;
        auto m1 = (newSegment.u + newSegment.v) * 0.5f;

        auto T = m1-m0;

        auto newV0 = v0 + T;

        auto a01 = glm::distance(v0,oldSegment.u) / glm::distance(v0,v1);
        auto a02 = glm::distance(v0,oldSegment.v) / glm::distance(v0,v2);

        auto dir01 = newSegment.u-newV0;
        auto dir02 = newSegment.v-newV0;

        auto length01 = glm::length(dir01);
        auto length02 = glm::length(dir02);

        dir01 /= length01;
        dir02 /= length02;

        auto newV1 = newV0 + dir01 * (length01/a01);
        auto newV2 = newV0 + dir02 * (length02/a02);

        std::vector<glm::vec3> newPositions = {newV0,newV1,newV2};
        return newPositions;
    };



    const auto& triangles = surface.indices;
    const auto& vertices = surface.vertexBuffer;
    auto t = oldSegment.t;
    std::vector<unsigned int> idx = {
        triangles[3*t+0],
        triangles[3*t+1],
        triangles[3*t+2]
    };
    std::vector<short> config;
    auto interPoints = computeIntersection(idx[0],idx[1],idx[2],axis,slice,config);


    std::cout << t << "(" << idx[0] << " " << idx[1] << " " << idx[2] << "): " <<
         oldSegment.u << "-" << oldSegment.v << " -> " <<
         newSegment.u << "-" << newSegment.v;

    if(config[0]==2)
    {
        auto origin = oldSegment;
        auto end = newSegment;
        if(interPoints[0]==origin.v)
            std::swap(end.u,end.v);

        if(config[1]==1 && config[4]==1) //no segment point is a vertex of the triangle
        {
            unsigned int v0,v1,v2;
            auto e0 = config[2];
            auto e1 = config[3];
            auto f0 = config[5];
            auto f1 = config[6];

            if(e0==f0 || e0==f1)
            {
                v0 = idx[e0];
                v1 = idx[e1];
                v2 = (e0==f0) ? idx[f1] : idx[f0];

            }
            else if(e1==f0 || e1==f1)
            {
                v0 = idx[e1];
                v1 = idx[e0];
                v2 = (e1==f0) ? idx[f1] : idx[f0];
            }

            auto newPos = computeNewPositions(vertices[v0].position,
                                              vertices[v1].position,
                                              vertices[v2].position,
                                              origin,end);
            if(newPositions.find(v0)==newPositions.end()) newPositions[v0] = newPos[0];
            if(newPositions.find(v1)==newPositions.end()) newPositions[v1] = newPos[1];
            if(newPositions.find(v2)==newPositions.end()) newPositions[v2] = newPos[2];

            std::cout << " V" << v0 << ": " << newPositions[v0];
            std::cout << " V" << v1 << ": " << newPositions[v1];
            std::cout << " V" << v2 << ": " << newPositions[v2] << std::endl;
        }
        else if(config[1]==0 && config[3]==0) //both segment points are vertices of the triangle
        {
            if(newPositions.find(idx[config[2]])==newPositions.end()) newPositions[idx[config[2]]] = end.u;
            if(newPositions.find(idx[config[4]])==newPositions.end()) newPositions[idx[config[4]]] = end.v;

            std::cout << " V" << idx[config[2]] << ": " << newPositions[idx[config[2]]];
            std::cout << " V" << idx[config[4]] << ": " << newPositions[idx[config[4]]] << std::endl;
        }
        else //one segment point is a vertex of the triangle
        {
            unsigned int v0,v1,v2;
            if(config[1]==0)
            {
                v0 = idx[config[4]];
                v1 = idx[config[2]];
                v2 = idx[config[5]];
            }
            else
            {
                v0 = idx[config[2]];
                v1 = idx[config[5]];
                v2 = idx[config[3]];
            }

            auto newPos = computeNewPositions(vertices[v0].position,
                                              vertices[v1].position,
                                              vertices[v2].position,
                                              origin,end);

            if(newPositions.find(v0)==newPositions.end()) newPositions[v0] = newPos[0];
            if(newPositions.find(v1)==newPositions.end()) newPositions[v1] = newPos[1];
            if(newPositions.find(v2)==newPositions.end()) newPositions[v2] = newPos[2];

            std::cout << " V" << v0 << ": " << newPositions[v0];
            std::cout << " V" << v1 << ": " << newPositions[v1];
            std::cout << " V" << v2 << ": " << newPositions[v2] << std::endl;
        }
    }

    return newPositions;



//    auto z0 = zCoords[axis][i0];
//    auto z1 = zCoords[axis][i1];
//    auto z2 = zCoords[axis][i2];

//    auto zS = slice*spacing[axis];

//    auto s0 = z0 < zS ? 0 : (z0 > zS ? 2 : 1);
//    auto s1 = z1 < zS ? 0 : (z1 > zS ? 2 : 1);
//    auto s2 = z2 < zS ? 0 : (z2 > zS ? 2 : 1);

//    std::vector<short> intersectionCode;
//    Segment segment;
//    if(computeIntersection(oldSegment.t,axis,slice,segment,intersectionCode))
//    {
//        if(intersectionCode[1]==0 || intersectionCode[3]==0)
//        {

//        }
//        else
//        {

//        }
    //    }
}

std::vector<short> SurfaceSlicing::getIntersectionConfig(unsigned int i0, unsigned int i1, unsigned int i2,
                                                         NewVolume::Axis axis, unsigned int slice)
{
    auto z0 = zCoords[axis][i0];
    auto z1 = zCoords[axis][i1];
    auto z2 = zCoords[axis][i2];

    auto zS = slice*spacing[axis];

    auto s0 = z0 < zS ? 0 : (z0 > zS ? 2 : 1);
    auto s1 = z1 < zS ? 0 : (z1 > zS ? 2 : 1);
    auto s2 = z2 < zS ? 0 : (z2 > zS ? 2 : 1);

    return M[s0][s1][s2];
}

std::vector<glm::vec3> SurfaceSlicing::computeIntersection(unsigned int i0, unsigned int i1,
    unsigned int i2, NewVolume::Axis axis, unsigned int slice, std::vector<short> &intersectionCode)
{
    const auto& vertices = surface.vertexBuffer;

    auto v0 = vertices[i0].position;
    auto v1 = vertices[i1].position;
    auto v2 = vertices[i2].position;

    auto z0 = zCoords[axis][i0];
    auto z1 = zCoords[axis][i1];
    auto z2 = zCoords[axis][i2];

    auto zS = slice*spacing[axis];

    auto s0 = z0 < zS ? 0 : (z0 > zS ? 2 : 1);
    auto s1 = z1 < zS ? 0 : (z1 > zS ? 2 : 1);
    auto s2 = z2 < zS ? 0 : (z2 > zS ? 2 : 1);

    glm::vec3 v[3] = {v0,v1,v2};
    float z[3] = {z0,z1,z2};

    intersectionCode = M[s0][s1][s2];

    std::vector< glm::vec3 > intersection;

    int pos = 1;
    for(int i = 0; i < intersectionCode[0]; ++i)
    {
        if(intersectionCode[pos]==0)
        {
            intersection.push_back(v[intersectionCode[pos+1]]);
            pos+=2;
        }
        else
        {
            auto p1 = intersectionCode[pos+1];
            auto p2 = intersectionCode[pos+2];

            auto a = std::abs(zS-z[p1])/std::abs(z[p1]-z[p2]);
            auto p = glm::mix(v[p1],v[p2],a);

            intersection.push_back(p);
            pos+=3;
        }
    }

    return intersection;
}

std::vector<SurfaceSlicing::Edge> SurfaceSlicing::computeIntersection(unsigned int t,
                                                                      NewVolume::Axis axis, unsigned int slice)
{
    const auto& vertices = surface.vertexBuffer;
    const auto& triangles = surface.indices;
    auto i0 = triangles[3*t+0];
    auto i1 = triangles[3*t+1];
    auto i2 = triangles[3*t+2];

    auto v0 = vertices[i0].position;
    auto v1 = vertices[i1].position;
    auto v2 = vertices[i2].position;

    auto z0 = zCoords[axis][i0];
    auto z1 = zCoords[axis][i1];
    auto z2 = zCoords[axis][i2];

    auto zS = slice*spacing[axis];

    auto s0 = z0 < zS ? 0 : (z0 > zS ? 2 : 1);
    auto s1 = z1 < zS ? 0 : (z1 > zS ? 2 : 1);
    auto s2 = z2 < zS ? 0 : (z2 > zS ? 2 : 1);

    glm::vec3 v[3] = {v0,v1,v2};
    unsigned int index[3] = {i0,i1,i2};
    float z[3] = {z0,z1,z2};

    auto code = M[s0][s1][s2];

    std::vector< Edge > intersection;

    int pos = 1;
    for(int i = 0; i < code[0]; ++i)
    {
        if(code[pos]==0)
        {
            auto idx = code[pos+1];
            intersection.push_back({index[idx],index[idx],v[idx]});
            pos+=2;
        }
        else
        {
            auto p1 = code[pos+1];
            auto p2 = code[pos+2];
            auto a = std::abs(zS-z[p1])/std::abs(z[p1]-z[p2]);
            auto p = glm::mix(v[p1],v[p2],a);
            intersection.push_back({index[p1],index[p2],p});
            pos+=3;
        }
    }

    return intersection;
}

bool SurfaceSlicing::computeIntersection(unsigned int t, NewVolume::Axis axis,
                                         unsigned int slice, Segment &segment,
                                         std::vector<short>& intersectionCode)
{
    const auto& triangles = surface.indices;
    auto id0 = triangles[3*t+0];
    auto id1 = triangles[3*t+1];
    auto id2 = triangles[3*t+2];

    auto intersection = computeIntersection(id0, id1, id2, axis, slice, intersectionCode);

    if(intersection.size() == 2)
    {
        segment = {intersection[0],intersection[1],t};
        return true;
    }

    return false;
}

void SurfaceSlicing::buildIntersectionMap()
{
    M[0][0][0] = {0};
    M[2][2][2] = {0};

    M[1][1][1] = {3,0,0,0,1,0,2};

    M[1][0][0] = {1,0,0};
    M[1][2][2] = {1,0,0};
    M[0][1][0] = {1,0,1};
    M[2][1][2] = {1,0,1};
    M[0][0][1] = {1,0,2};
    M[2][2][1] = {1,0,2};

    M[1][0][2] = {2,0,0,1,1,2};
    M[1][2][0] = {2,0,0,1,1,2};
    M[0][1][2] = {2,0,1,1,0,2};
    M[2][1][0] = {2,0,1,1,0,2};
    M[0][2][1] = {2,0,2,1,0,1};
    M[2][0][1] = {2,0,2,1,0,1};

    M[1][1][0] = {2,0,0,0,1};
    M[1][1][2] = {2,0,0,0,1};
    M[1][0][1] = {2,0,0,0,2};
    M[1][2][1] = {2,0,0,0,2};
    M[0][1][1] = {2,0,1,0,2};
    M[2][1][1] = {2,0,1,0,2};

    M[0][2][2] = {2,1,0,1,1,0,2};
    M[2][0][0] = {2,1,0,1,1,0,2};
    M[0][2][0] = {2,1,0,1,1,1,2};
    M[2][0][2] = {2,1,0,1,1,1,2};
    M[0][0][2] = {2,1,0,2,1,1,2};
    M[2][2][0] = {2,1,0,2,1,1,2};
}

void SurfaceSlicing::computeZCoords(NewVolume::Axis axis)
{
    auto& vertices = surface.vertexBuffer;
    auto n = vertices.size();
    auto point = info.p5; //A common point to all the axis

    zCoords[axis].resize(n);

    for(unsigned int i = 0; i < n; ++i)
    {
        auto v = vertices[i].position-point;
        zCoords[axis][i] = glm::dot(v,sliceNormals[axis]);
    }
}
