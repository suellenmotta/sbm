#ifndef BOUNDARYHIGHLIGHTOPERATOR_H
#define BOUNDARYHIGHLIGHTOPERATOR_H

#include "operator.h"

#include <QOpenGLShaderProgram>

class BoundaryHighlightOperator : public Operator
{
public:
    BoundaryHighlightOperator(Scene& scene);

    void draw() override;

private:
    void findBorderVertices(const Scene& scene);

    std::vector< opengl::Mesh<glm::vec3,GL_LINE_STRIP> > meshes;

    QOpenGLShaderProgram program;

    GLuint VAO;
};

#endif // BOUNDARYHIGHLIGHTOPERATOR_H
