#ifndef HARMONICSURFACEDEFORMATION_H
#define HARMONICSURFACEDEFORMATION_H

#include "surfacedeformation.h"

#include <map>
#include <list>

class HarmonicSurfaceDeformation : public SurfaceDeformation
{
public:
    HarmonicSurfaceDeformation(opengl::UntexturedMesh& surface);

    void setK(unsigned int k);
    void setNewPositions(std::map< unsigned int, glm::vec3 >& positions);
    void setFixedVertices(std::list<unsigned int>& fixedList);

    void run() override;

private:
    std::map<unsigned int, glm::vec3> displacements;
    std::list<unsigned int> fixedVertices;
    unsigned int k;
};

#endif // HARMONICSURFACEDEFORMATION_H
