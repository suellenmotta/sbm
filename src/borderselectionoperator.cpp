#include "borderselectionoperator.h"

#include "meshalgorithms.h"
#include <sstream>
#include <algorithm>

#include <QApplication> //for cursor change

BorderSelectionOperator::BorderSelectionOperator(Scene &scene, GLuint defaultFbo)
    : Operator(scene)
    , defaultFbo(defaultFbo)
    , drawMode(GL_LINE_LOOP)
    , state(NONE)
{
    initializeOpenGLFunctions();

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/solid-color-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/solid-color-border-frag");
    program.link();

    if( !program.isLinked() )
        qCritical() << "[BorderSelectionOperator] Shader program not linked";

    for(const auto& obj : scene.objects)
    {
        std::vector<Border> objBorders;
        borders.push_back(objBorders);
        findBorders(obj,borders.back());

        std::vector<bool> objSelectedBorders(borders.back().size(),false);
        isBorderSelected.push_back(objSelectedBorders);

        for(const auto& border : borders.back())
        {

        }
    }

    createBorderTexture();
    fillBorderTexture();
}

BorderSelectionOperator::~BorderSelectionOperator()
{
    glDeleteTextures(1,&sceneTex);
    glDeleteTextures(1,&pickingTex);
    glDeleteTextures(1,&depthTex);
    glDeleteFramebuffers(1,&fbo);
    glDeleteBuffers(ibos.size(),ibos.data());
}

void BorderSelectionOperator::draw()
{
    program.bind();

    glLineWidth(3.0f);
    glEnable(GL_LINE_SMOOTH);

    glPointSize(3.0f);
    glEnable(GL_POINT_SMOOTH);

    QMatrix4x4 m(glm::value_ptr(glm::transpose(scene.camera.modelMatrix())));
    QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

    auto mvp = p*v*m;
    program.setUniformValue("MVP", mvp);

    glBindFramebuffer(GL_FRAMEBUFFER,defaultFbo);

    scene.bindVAO();

    int ibo = 0;
    auto bordersIt = borders.cbegin();
    for(unsigned int obj = 0; obj < scene.objects.size(); ++obj)
    {
        if(scene.isObjectShown(obj))
        {
            scene.objects[obj].bindVBO();

            for(unsigned int border = 0; border < (*bordersIt).size(); ++border)
            {
                if(isBorderSelected[obj][border])
                {
                    program.setUniformValue("color", QVector3D(255.0f,255.0f,255.0f) );
                }
                else
                {
                    program.setUniformValue("color", QVector3D(0.0f,0.0f,0.0f) );
                }


                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibos[ibo]);
                glDrawElements(drawMode, (*bordersIt)[border].size(), GL_UNSIGNED_INT, 0);
                ++ibo;
            }
        }

        else
            ibo += (*bordersIt).size();


        ++bordersIt;
    }
}

void BorderSelectionOperator::showPointCloud()
{
    drawMode = GL_POINTS;
}

void BorderSelectionOperator::showLineLoops()
{
    drawMode = GL_LINE_LOOP;
}

void BorderSelectionOperator::unselectBorder(unsigned int object, unsigned int border)
{
    if(object < borders.size() && border < borders[object].size())
    {
        isBorderSelected[object][border] = false;
    }
}

void BorderSelectionOperator::unselectBorders()
{
    for(auto& objBorders : isBorderSelected)
        std::fill(objBorders.begin(),objBorders.end(),false);
}

std::vector< std::pair<int,int> > BorderSelectionOperator::getSelectedBorders()
{
    std::vector< std::pair<int,int> > borders;

    for(unsigned int i = 0; i < this->borders.size(); ++i)
    {
        for(unsigned int j = 0; j < this->borders[i].size(); ++j)
        {
            if(isBorderSelected[i][j])
                borders.push_back({i,j});
        }
    }

    return borders;
}

std::vector< std::string > BorderSelectionOperator::getSelectedBorderNames()
{
    std::vector< std::string > borderNames;

    for(unsigned int i = 0; i < this->borders.size(); ++i)
    {
        std::string prefix = "border_" + scene.objects[i].name + "_";

        for(unsigned int j = 0; j < this->borders[i].size(); ++j)
        {
            if(isBorderSelected[i][j])
            {
                std::stringstream str;
                str << prefix << j;
                borderNames.push_back(str.str());
            }
        }
    }

    return borderNames;
}

std::vector<glm::vec3> BorderSelectionOperator::getSelectedBordersPointCloud()
{
    auto selected = getSelectedBorders();

    std::vector<glm::vec3> pointCloud;
    for(const auto& border : selected)
    {
        auto indices = getBorderIndices(border.first,border.second);

        for(auto index : indices)
        {
            pointCloud.push_back(scene.objects[border.first].vertexBuffer[index].position);
        }
    }

    return pointCloud;
}

std::vector<unsigned int> BorderSelectionOperator::getBorderIndices(unsigned int object, unsigned int border)
{
    std::vector<unsigned int> borderIndices;

    if(object < borders.size() && border < borders[object].size())
    {
        borderIndices = borders[object][border];
    }

    return borderIndices;
}

bool BorderSelectionOperator::mousePress(QMouseEvent*)
{
    return false;
}

bool BorderSelectionOperator::mouseMove(QMouseEvent* event)
{
    glm::ivec2 pos(event->x(),event->y());

    float depth=-1;
    int object, borderId;
    pickBorder(pos,object,borderId,depth);

    if(state != NONE)
    {
        if(object!=highlighted.first || borderId!=highlighted.second)
        {
            resetHighlighting();
        }
        else
        {
            return false;
        }
    }

    if(object>-1 && borderId>-1)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFbo);
        glReadBuffer(GL_DEPTH_ATTACHMENT);
        float screenDepth;
        glReadPixels(pos.x, scene.camera.height-pos.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &screenDepth);

        if(depth<=screenDepth)
        {
            state = isBorderSelected[object][borderId] ? SELECTED : UNSELECTED;
            isBorderSelected[object][borderId] = !isBorderSelected[object][borderId];

            highlighted = {object,borderId};

            QApplication::setOverrideCursor(QCursor(Qt::PointingHandCursor));
        }
        else
        {
            QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
        }
    }
    else
    {
        QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor));
    }

    return true;
}

bool BorderSelectionOperator::mouseRelease(QMouseEvent* event)
{
    if(event->button()==Qt::LeftButton && state != NONE)
    {
        if(state == SELECTED)
        {
            isBorderSelected[highlighted.first][highlighted.second] = false;
            state = UNSELECTED;
        }

        else
        {
            isBorderSelected[highlighted.first][highlighted.second] = true;
            state = SELECTED;
        }

        emit borderSelectionChanged();
        return true;
    }
}

void BorderSelectionOperator::onViewChanged()
{
    fillBorderTexture();
}

void BorderSelectionOperator::createBorderTexture()
{
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer( GL_FRAMEBUFFER, fbo );

    glGenTextures(1, &sceneTex );
    glBindTexture(GL_TEXTURE_2D,sceneTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, scene.camera.width, scene.camera.height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneTex, 0);

    glGenTextures(1, &pickingTex );
    glBindTexture(GL_TEXTURE_2D,pickingTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32I, scene.camera.width, scene.camera.height, 0, GL_RG_INTEGER, GL_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, pickingTex, 0);

    glGenTextures(1, &depthTex );
    glBindTexture(GL_TEXTURE_2D,depthTex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, scene.camera.width, scene.camera.height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTex, 0);

    GLenum drawBuffers[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers( 2, drawBuffers );

    if ( glCheckFramebufferStatus( GL_FRAMEBUFFER ) != GL_FRAMEBUFFER_COMPLETE )
    {
        qCritical() << "[BorderSelectionOperator] Framebuffer not created";
    }
}

void BorderSelectionOperator::fillBorderTexture()
{
    program.bind();

    glLineWidth(8.0f);
    glDisable(GL_LINE_SMOOTH);

    QMatrix4x4 m(glm::value_ptr(glm::transpose(scene.camera.modelMatrix())));
    QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
    QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

    auto mvp = p*v*m;
    program.setUniformValue("MVP", mvp);

    glBindFramebuffer(GL_FRAMEBUFFER,fbo);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    GLint invalidIDs[2] = {-1,-1};
    glClearBufferiv(GL_COLOR,1,invalidIDs);

    scene.bindVAO();

    auto ibosIt = ibos.cbegin();

    for(unsigned int obj = 0; obj < scene.objects.size(); ++obj)
    {
        if(scene.isObjectShown(obj))
        {
            scene.objects[obj].bindVBO();

            program.setUniformValue("object",obj);

            for(unsigned int border = 0; border < borders[obj].size(); ++border)
            {
                program.setUniformValue("border",border);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*ibosIt));
                glDrawElements(GL_LINE_LOOP, borders[obj][border].size(), GL_UNSIGNED_INT, 0);
                ++ibosIt;
            }
        }

        else
            ibosIt += borders[obj].size();
    }
}

void BorderSelectionOperator::pickBorder(const glm::ivec2 &pos, int &obj, int &border, float& depth)
{
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glReadBuffer(GL_COLOR_ATTACHMENT1);

    int value[2] = {-1,-1};
    glReadPixels(pos.x, scene.camera.height-pos.y, 1, 1, GL_RG_INTEGER, GL_INT, &value[0] );

    obj = value[0];
    border = value[1];

    depth = -1;
    glReadBuffer(GL_DEPTH_ATTACHMENT);
    glReadPixels(pos.x, scene.camera.height-pos.y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &depth );
}

void BorderSelectionOperator::resetHighlighting()
{
    if(state == SELECTED)
    {
        isBorderSelected[highlighted.first][highlighted.second] = true;
    }
    else if(state == UNSELECTED)
    {
        isBorderSelected[highlighted.first][highlighted.second] = false;
    }

    state = NONE;
    highlighted = {-1,-1};
}
