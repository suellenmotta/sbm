#ifndef IMAGE_H
#define IMAGE_H

#include <array>

template<class Type, size_t Dimension>
class Image
{
public:
    Image(const std::array<size_t, Dimension>& sizes) : sizes(sizes)
    {
        totalSize = 1;
        for (const auto& size : sizes)
            totalSize *= size;

        buffer = totalSize > 0 ? new Type[totalSize] : nullptr;
    }

    Image(const Image<Type, Dimension>& other) : Image(other.sizes)
    {
        for (size_t i = 0; i < totalSize; ++i)
            buffer[i] = other.buffer[i];
    }

    ~Image()
    {
        delete[] buffer;
        buffer = nullptr;
    }

    class iterator
    {
    public:
        friend class Image;

        Type& operator* ()
        {
            return data[pos];
        }

        iterator& operator++ ()
        {
            ++pos;
            return *this;
        }

        iterator& operator-- ()
        {
            --pos;
            return *this;
        }

        iterator operator++ (int)
        {
            iterator result(data, pos);
            ++(*this);
            return result;
        }

        iterator operator-- (int)
        {
            iterator result(data, pos);
            --(*this);
            return result;
        }

        bool operator == (const iterator& other)
        {
            return pos == other.pos && data == other.data;
        }

        bool operator != (const iterator& other)
        {
            return pos != other.pos || data != other.data;
        }

    private:
        iterator(Type*data, int pos) : data(data), pos(pos) {}
        Type* data;
        int pos;
    };

    iterator begin()
    {
        return iterator(buffer, 0);
    }

    iterator end()
    {
        return iterator(buffer, totalSize);
    }

private:
    Type* buffer;
    std::array<size_t,Dimension> sizes;
    size_t totalSize;
    int pos;
};


using FloatImage2D = Image<float, 2>;
using FloatImage3D = Image<float, 3>;


#endif
