#include "volumerenderer.h"

#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <vector>
#include <iostream>
#include <QMatrix>

#include "raycasting.h"

struct vertex
{
    glm::vec3 pos;
    glm::vec2 texCoord;
};

#if VOLUME == SEAM || VOLUME == SUBSEAM
    #define MIN_VAL -93.0f
    #define MAX_VAL 88.0f

#elif VOLUME == F3 || VOLUME == SUBF3
    #define MIN_VAL -4500.0f
    #define MAX_VAL +4500.0f
#endif

#define VOLUME_TEXT_UNIT 0
#define COLOR_TABLE_TEXT_UNIT 1
#define VOLUME_MIX_TEXT_UNIT 2

void errorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
//    std::cout << message << std::endl;
}

VolumeRenderer::VolumeRenderer(ArcballCamera& camera)
    : VAO(0)
    , xSliceVBO(0)      , ySliceVBO(0)      , zSliceVBO(0)
    , xSliceTexture(0)  , ySliceTexture(0)  , zSliceTexture(0)
    , xSliceIndex(-1)   , ySliceIndex(-1)   , zSliceIndex(-1)
    , xSliceShow(false) , ySliceShow(false) , zSliceShow(false)
    , xSliceMinVal(MIN_VAL), ySliceMinVal(MIN_VAL), zSliceMinVal(MIN_VAL)
    , xSliceMaxVal(MAX_VAL), ySliceMaxVal(MAX_VAL), zSliceMaxVal(MAX_VAL)
    , volume(nullptr)
    , mixVolume(nullptr)
    , cam(camera)
    , lutTexture(0)
    , useColorTable(false)
    , isColorTableDiscrete(false)
    , isInitialized(false)
    , correctColor(false)
{

}

VolumeRenderer::~VolumeRenderer()
{
    if(volume)
        delete volume;

    if(mixVolume)
        delete mixVolume;

    if(isInitialized)
    {
        glDeleteVertexArrays(1, &VAO);

        glDeleteBuffers(1, &xSliceVBO);
        glDeleteBuffers(1, &ySliceVBO);
        glDeleteBuffers(1, &zSliceVBO);

        glDeleteTextures(1, &xSliceTexture);
        glDeleteTextures(1, &ySliceTexture);
        glDeleteTextures(1, &zSliceTexture);

        glDeleteTextures(1, &xSliceMixTexture);
        glDeleteTextures(1, &ySliceMixTexture);
        glDeleteTextures(1, &zSliceMixTexture);
    }
}

void VolumeRenderer::init()
{
    initializeOpenGLFunctions();

    isInitialized = true;

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/textured-quad-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/textured-quad-frag");
    program.link();

    mixProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/textured-quad-vert");
    mixProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/textured-quad-mix-frag");
    mixProgram.link();

    if(program.isLinked())
    {
        glGenBuffers(1, &zSliceVBO);
        glGenBuffers(1, &ySliceVBO);
        glGenBuffers(1, &xSliceVBO);

        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);
        glEnableVertexAttribArray( 0 );
        glEnableVertexAttribArray( 1 );
        glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
        glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3));
        glVertexAttribBinding(0,0);
        glVertexAttribBinding(1,0);

        glGenTextures(1, &xSliceTexture);
        glGenTextures(1, &ySliceTexture);
        glGenTextures(1, &zSliceTexture);

        glGenTextures(1, &xSliceMixTexture);
        glGenTextures(1, &ySliceMixTexture);
        glGenTextures(1, &zSliceMixTexture);

//        float colorTable[] =
//        {
//            170/255.0f,0.0f,0.0f,
//            1.0f,28/255.0f,0.0f,
//            1.0f,200/255.0f,0.0f,
//            243/255.0f,243/255.0f,243/255.0f,
//            56/255.0f,70/255.0f,127/255.0f,
//            0.0f,0.0f,0.0f
//        };
//        lutMarkers = {0.0f,0.070352f,0.25f,0.5f,0.883249f,1.0f};

        glGenTextures(1, &lutTexture);
//        updateColorTableTexture(colorTable,6);
    }

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(errorCallback,nullptr);
}

void VolumeRenderer::resetCamera()
{
    if(!volume)
        return;

    auto startModel = volume->getStartModelMatrix();
    auto center = glm::vec3(startModel*glm::vec4(volume->getCenter(),1));
    cam.focus(center,volume->getCircumscribedSphereRadius(),startModel);
}

const NewVolume *VolumeRenderer::getVolume()
{
    return volume;
}

void VolumeRenderer::setVolume(NewVolume* volume)
{
    if(this->volume)
        delete this->volume;

    this->volume = volume;

    if(volume)
    {
        auto startModel = volume->getStartModelMatrix();
        auto center = glm::vec3(startModel*glm::vec4(volume->getCenter(),1));
        cam.focus(center,volume->getCircumscribedSphereRadius(),startModel);

        updateXSlice(0);
        updateYSlice(0);
        updateZSlice(0);

        xSliceShow = ySliceShow = zSliceShow = true;
    }
    else
    {
        xSliceIndex = -1;
        ySliceIndex = -1;
        zSliceIndex = -1;

        xSliceShow = ySliceShow = zSliceShow = true;
    }
}

void VolumeRenderer::setMixVolume(NewVolume *volume)
{
    if(mixVolume)
        delete mixVolume;

    mixVolume = volume;

    if(mixVolume)
    {
        updateXSlice(getCurrentXSlice());
        updateYSlice(getCurrentYSlice());
        updateZSlice(getCurrentZSlice());
    }
}

void VolumeRenderer::updateXSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getXSlicePoints(index, vertices);
    updateVBO(vertices,xSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getXSliceBuffer(index,width,height);
    updateTexture(slice,width,height,GL_TEXTURE0 + VOLUME_TEXT_UNIT,xSliceTexture);


    if(correctColor)
    {
        xSliceMinVal = *(std::min_element(slice,slice+width*height));
        xSliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    if(mixVolume)
    {
        auto slice2 = mixVolume->getXSliceBuffer(index,width,height);
        updateTexture(slice2,width,height,GL_TEXTURE0 + VOLUME_MIX_TEXT_UNIT,xSliceMixTexture);
        delete[] slice2;
    }

    xSliceIndex = index;
    delete[] slice;
}

void VolumeRenderer::updateYSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getYSlicePoints(index, vertices);
    updateVBO(vertices,ySliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getYSliceBuffer(index,width,height);
    updateTexture(slice,width,height,GL_TEXTURE0 +VOLUME_TEXT_UNIT,ySliceTexture);

    if(correctColor)
    {
        ySliceMinVal = *(std::min_element(slice,slice+width*height));
        ySliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    if(mixVolume)
    {
        auto slice2 = mixVolume->getYSliceBuffer(index,width,height);
        updateTexture(slice2,width,height,GL_TEXTURE0 +VOLUME_MIX_TEXT_UNIT,ySliceMixTexture);
        delete[] slice2;
    }

    ySliceIndex = index;
    delete[] slice;
}

void VolumeRenderer::updateZSlice(unsigned int index)
{
    if(!volume)
        return;

    std::vector<glm::vec3> vertices;
    volume->getZSlicePoints(index, vertices);
    updateVBO(vertices,zSliceVBO);

    unsigned int width = 0;
    unsigned int height = 0;
    auto slice = volume->getZSliceBuffer(index,width,height);
    updateTexture(slice,width,height,GL_TEXTURE0 +VOLUME_TEXT_UNIT,zSliceTexture);

    if(correctColor)
    {
        zSliceMinVal = *(std::min_element(slice,slice+width*height));
        zSliceMaxVal = *(std::max_element(slice,slice+width*height));
    }

    if(mixVolume)
    {
        auto slice2 = mixVolume->getZSliceBuffer(index,width,height);
        updateTexture(slice2,width,height,GL_TEXTURE0 +VOLUME_MIX_TEXT_UNIT,zSliceMixTexture);
        delete[] slice2;
    }

    zSliceIndex = index;
    delete[] slice;
}

unsigned int VolumeRenderer::getCurrentXSlice() const
{
    return xSliceIndex;
}

unsigned int VolumeRenderer::getCurrentYSlice() const
{
    return ySliceIndex;
}

unsigned int VolumeRenderer::getCurrentZSlice() const
{
    return zSliceIndex;
}

void VolumeRenderer::showXSlice(bool show)
{
    xSliceShow = show;
}

void VolumeRenderer::showYSlice(bool show)
{
    ySliceShow = show;
}

void VolumeRenderer::showZSlice(bool show)
{
    zSliceShow = show;
}

bool VolumeRenderer::isXSliceShown() const
{
    return xSliceShow;
}

bool VolumeRenderer::isYSliceShown() const
{
    return ySliceShow;
}

bool VolumeRenderer::isZSliceShown() const
{
    return zSliceShow;
}

void VolumeRenderer::setMinPixelValue(float minValue)
{
    xSliceMinVal = ySliceMinVal = zSliceMinVal = minValue;
}

void VolumeRenderer::setMaxPixelValue(float maxValue)
{
    xSliceMaxVal = ySliceMaxVal = zSliceMaxVal = maxValue;
}

void VolumeRenderer::setColorTable(std::vector<float> table, std::vector<float> markers, bool isDiscrete)
{
    lutMarkers = markers;
    useColorTable = true;
    isColorTableDiscrete = isDiscrete;
    updateColorTableTexture(table.data(),table.size()/3);
}

void VolumeRenderer::removeColorTable()
{
    lutMarkers.clear();
    useColorTable = false;

}

void VolumeRenderer::draw()
{
    if(!volume)
        return;

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    auto& prog = mixVolume ? mixProgram : program;

    prog.bind();

    auto mvp = cam.projMatrix() * cam.viewMatrix() * cam.modelMatrix();

    prog.setUniformValue("mvp", QMatrix4x4(glm::value_ptr(glm::transpose(mvp))));
    prog.setUniformValue("valueSampler",VOLUME_TEXT_UNIT);
    prog.setUniformValue("lutSampler", COLOR_TABLE_TEXT_UNIT);
    prog.setUniformValue("useColorTable",useColorTable);
    prog.setUniformValue("numMarkers",(unsigned int)lutMarkers.size());
    prog.setUniformValueArray("colorMarkers",lutMarkers.data(),lutMarkers.size(),1);

    if(useColorTable)
    {
        glActiveTexture(GL_TEXTURE0 + COLOR_TABLE_TEXT_UNIT);
        glBindTexture(GL_TEXTURE_1D, lutTexture);
    }

    if(mixVolume)
    {
        prog.setUniformValue("mixTextureSampler",2);
    }

    glBindVertexArray(VAO);

    if(xSliceShow)
    {
        if(mixVolume)
        {
            glActiveTexture(GL_TEXTURE0+VOLUME_MIX_TEXT_UNIT);
            glBindTexture(GL_TEXTURE_2D, xSliceMixTexture);
        }

        glActiveTexture(GL_TEXTURE0+VOLUME_TEXT_UNIT);
        glBindTexture(GL_TEXTURE_2D, xSliceTexture);

        glBindVertexBuffer(0,xSliceVBO,0,sizeof(vertex));
        prog.setUniformValue("minValue",xSliceMinVal);
        prog.setUniformValue("maxValue",xSliceMaxVal);

        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(ySliceShow)
    {
        if(mixVolume)
        {
            glActiveTexture(GL_TEXTURE0+VOLUME_MIX_TEXT_UNIT);
            glBindTexture(GL_TEXTURE_2D, ySliceMixTexture);
        }

        glActiveTexture(GL_TEXTURE0+VOLUME_TEXT_UNIT);
        glBindTexture(GL_TEXTURE_2D, ySliceTexture);

        glBindVertexBuffer(0,ySliceVBO,0,sizeof(vertex));
        prog.setUniformValue("minValue",ySliceMinVal);
        prog.setUniformValue("maxValue",ySliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }

    if(zSliceShow)
    {
        if(mixVolume)
        {
            glActiveTexture(GL_TEXTURE0+VOLUME_MIX_TEXT_UNIT);
            glBindTexture(GL_TEXTURE_2D, zSliceMixTexture);
        }

        glActiveTexture(GL_TEXTURE0+VOLUME_TEXT_UNIT);
        glBindTexture(GL_TEXTURE_2D, zSliceTexture);

        glBindVertexBuffer(0,zSliceVBO,0,sizeof(vertex));
        prog.setUniformValue("minValue",zSliceMinVal);
        prog.setUniformValue("maxValue",zSliceMaxVal);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }


    glBindVertexArray(0);
    glActiveTexture(GL_TEXTURE0);
}

const ArcballCamera &VolumeRenderer::getCamera()
{
    return cam;
}

bool VolumeRenderer::intercepts(const glm::ivec2 &screenPos, NewVolume::Axis &axis, glm::vec3 &point) const
{
    float xDist=FLT_MAX,yDist=FLT_MAX,zDist=FLT_MAX;
    auto dir = rayDirection(screenPos,cam);

    bool interceptsX = interceptsXSlice(cam.eye,dir,xDist);
    bool interceptsY = interceptsYSlice(cam.eye,dir,yDist);
    bool interceptsZ = interceptsZSlice(cam.eye,dir,zDist);

    float dist = FLT_MAX;
    if(interceptsX)
    {
        axis = NewVolume::X;
        dist = xDist;
    }
    if(interceptsY && yDist<dist)
    {
        axis = NewVolume::Y;
        dist = yDist;
    }
    if(interceptsZ && zDist<dist)
    {
        axis = NewVolume::Z;
        dist = zDist;
    }

    if(interceptsX || interceptsY || interceptsZ)
    {
        point = glm::vec3(glm::inverse(cam.modelMatrix())*glm::vec4(cam.eye + dist * dir,1));
        return true;
    }

    return false;
}

bool VolumeRenderer::interceptsXSlice(const glm::vec3& orig, const glm::vec3& dir, float &dist) const
{
    if(xSliceShow && xSliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getXSlicePoints(xSliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

bool VolumeRenderer::interceptsYSlice(const glm::vec3 &orig, const glm::vec3& dir, float &dist) const
{
    if(ySliceShow && ySliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getYSlicePoints(ySliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

bool VolumeRenderer::interceptsZSlice(const glm::vec3& orig, const glm::vec3& dir, float &dist) const
{
    if(zSliceShow && zSliceIndex>=0)
    {
        std::vector<glm::vec3> points;
        volume->getZSlicePoints(zSliceIndex,points);

        auto m = cam.modelMatrix();

        //Transforma para o espaço de mundo
        for(auto& p : points)
            p = glm::vec3(m * glm::vec4(p,1));

        if(interceptsTriangle(orig,dir,points[0],points[1],points[2],dist))
            return true;
        else return interceptsTriangle(orig,dir,points[1],points[3],points[2],dist);
    }

    return false;
}

void VolumeRenderer::updateVBO(const std::vector<glm::vec3>& vertices, GLuint vboID)
{
    std::vector< vertex > vbo;
    vbo.reserve( 4 );

    vbo.push_back({vertices[0], {0.0f,0.0f}});
    vbo.push_back({vertices[1], {1.0f,0.0f}});
    vbo.push_back({vertices[2], {0.0f,1.0f}});
    vbo.push_back({vertices[3], {1.0f,1.0f}});

    glBindBuffer(GL_ARRAY_BUFFER, vboID);
    glBufferData(GL_ARRAY_BUFFER, vbo.size()*sizeof(vertex), &vbo[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void VolumeRenderer::updateTexture(float* slice, unsigned int width,
    unsigned int height, GLuint activeTexture, GLuint textureID)
{
    glActiveTexture(activeTexture);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D( GL_TEXTURE_2D, 0, GL_R32F, width, height,
                  0, GL_RED, GL_FLOAT, slice );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glGenerateMipmap(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE0);
}

void VolumeRenderer::updateColorTableTexture(float* colorTable, unsigned int numColors)
{
    glActiveTexture(GL_TEXTURE0+COLOR_TABLE_TEXT_UNIT);

    glEnable(GL_TEXTURE_1D);

    glBindTexture(GL_TEXTURE_1D, lutTexture);

    glTexImage1D( GL_TEXTURE_1D, 0, GL_RGB, numColors,
                  0, GL_RGB, GL_FLOAT, colorTable );

    if(isColorTableDiscrete)
    {
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);

        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

        glGenerateMipmap(GL_TEXTURE_1D);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

        glGenerateMipmap(GL_TEXTURE_1D);
    }
}

