#ifndef GRAPH_H
#define GRAPH_H

#include <vector>

class Graph
{
public:
    struct Edge
    {
        int v; //Vertice destino
        int w; //Peso da aresta
    };

    //Construtor. N = numero de vertices.
    Graph( unsigned int N );

    //Insere uma aresta no grafo, do no 'from' ao no 'to', com peso 'weight'.
    //Caso undirected esteja marcado como true, insere a aresta tambem no sentido to->from
    void insertEdge( int from, int to, int weight = 1, bool undirected = true );

    const std::vector<Edge>& getNeighbors(int node);

    const unsigned int getNumEdges();

    //Imprime os vertices e seus vizinhos
    void print();

    //Imprime os nos do grafo na ordem de uma busca em largura
    void bfs( int s );

    //Imprime os nos do grafo na ordem de uma busca em profundidade
    void dfs(int s);

    //Calcula a menor distancia de s ate cada vertice
    void dijkstra( int s );

    //Menor distancia de cada no ate um vertice especifico
    std::vector<int> distance;

    std::vector<int> parent;

private:


    enum Color
    {
        WHITE,
        GRAY,
        BLACK
    };

    void dfs_visit(int u , std::vector<Color> &color);

    //Lista de adjacencias
    std::vector< std::vector< Edge > > G;

    unsigned int numEdges;
};

#endif // GRAPH_H
