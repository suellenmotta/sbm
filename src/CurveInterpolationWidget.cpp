#include "CurveInterpolationWidget.h"

#include "ui_CurveInterpolationWidget.h"
#include "slicesketchingoperator.h"
#include "sketchsurfacegenerator.h"
#include "newvolume.h"
#include "canvas3dwidget.h"

#include <unordered_map>

typedef QPair<NewVolume::Axis,unsigned int> SketchIDType;
Q_DECLARE_METATYPE(SketchIDType)

static std::unordered_map<std::string, NewVolume::Axis> toAxis =
{
  {"Inline",    NewVolume::Z},
  {"Crossline", NewVolume::Y},
  {"Depth",     NewVolume::X}
};

CurveInterpolationWidget::CurveInterpolationWidget(QWidget *parent, Canvas3DWidget *canvas)
    : QWidget(parent)
    , ui(new Ui::CurveInterpolationWidget)
    , canvas(canvas)
    , sketchingOperator(canvas->createSliceSketchingOperator())
{
    ui->setupUi(this);

    ui->numPointsSpinBox->setRange(3,1000);
    ui->numPointsSpinBox->setValue(200);
    ui->numPointsSpinBox->setSingleStep(50);
}

void CurveInterpolationWidget::computeSurface(opengl::UntexturedMesh& mesh)
{
    auto curAxis = toAxis[ui->creationDirectionComboBox->currentText().toStdString()];
    auto sketchMap = sketchingOperator->getSketches(curAxis);
    using Sketch = std::vector<glm::vec3>;
    std::vector<Sketch> sketches;
    for(auto s : sketchMap)
    {
        sketches.push_back(s.second);
    }

    SketchInterpolationGenerator surfaceCreator(sketches);
    surfaceCreator.setSketchNumPoints(static_cast<uint>(ui->numPointsSpinBox->value()));
    surfaceCreator.run(mesh);
}

void CurveInterpolationWidget::setSketchingOn()
{
    if(sketchingOperator)
    {
        ui->addCreationSketch->setChecked(false);
        canvas->setOperator(std::static_pointer_cast<Operator>(sketchingOperator));
        connect(sketchingOperator.get(),SIGNAL(sketchFinished()),
                this,SLOT(onSketchFinished()));
    }

    ui->addCreationSketch->setChecked(true);
    canvas->update();
}

void CurveInterpolationWidget::setSketchingOff()
{
    disconnect(sketchingOperator.get(),SIGNAL(sketchFinished()),
               this,SLOT(onSketchFinished()));
    canvas->setOperator(nullptr);
    ui->addCreationSketch->setChecked(false);
    canvas->update();
}

void CurveInterpolationWidget::onSketchFinished()
{
    fillSketchesList();
}

void CurveInterpolationWidget::on_creationDirectionComboBox_currentIndexChanged(const QString &)
{
    fillSketchesList();
}

void CurveInterpolationWidget::on_addCreationSketch_clicked(bool checked)
{
    if(checked)
    {
        setSketchingOn();
    }
    else
    {
        setSketchingOff();
    }
}

void CurveInterpolationWidget::on_deleteCreationSketch_clicked()
{
    if(sketchingOperator)
    {
        auto selected = ui->creationSketchesListWidget->selectedItems();

        for(auto item : selected)
        {
            auto sketch = item->data(Qt::UserRole).value<SketchIDType>();

            sketchingOperator->deleteSketch(sketch.first,sketch.second);
        }

        qDeleteAll(selected);
        canvas->update();
    }
}

void CurveInterpolationWidget::fillSketchesList()
{
    if(sketchingOperator)
    {
        ui->creationSketchesListWidget->clear();

        auto sketchedSlices = sketchingOperator->getSketchedSlices();
        auto sketchNames = sketchingOperator->getSketchNames();

        auto curAxis = toAxis[ui->creationDirectionComboBox->currentText().toStdString()];
        QColor gray = {230,230,230};
        for(unsigned int i = 0; i < sketchedSlices.size(); ++i)
        {
            auto& axis = sketchedSlices[i].first;
            auto& idx  = sketchedSlices[i].second;
            auto& name = sketchNames[i];
            SketchIDType sketch(axis,idx);

            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(name));
            item->setData(Qt::UserRole, QVariant::fromValue(sketch));

            if(axis != curAxis)
            {
                item->setForeground(gray);
            }

            ui->creationSketchesListWidget->addItem(item);
        }
    }
}
