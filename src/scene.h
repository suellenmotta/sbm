#ifndef SCENE_H
#define SCENE_H

#include "mesh.h"
#include "camera.h"
#include "volumerenderer.h"

#include <vector>

class Scene : protected QOpenGLExtraFunctions
{
public:
    Scene();

    virtual ~Scene();

    Scene(const Scene&) = delete;

    void init();

    void reset();
    void resetCamera();

    const NewVolume* getVolume();
    void setVolume(NewVolume *volume);

    VolumeRenderer& getVolumeRenderer();

    void addObject(opengl::UntexturedMesh &&mesh);
    void addObject(const std::vector<glm::vec3>& vertices,
                   const std::vector<glm::vec3>& normals,
                   const std::vector<unsigned int> &indices,
                   const glm::vec3& diffuseColor,
                   const std::string &name = "");
    void addObject(const std::vector<opengl::Vertex>& vertexBuffer,
                   const std::vector<unsigned int> &indices,
                   const glm::vec3& diffuseColor,
                   const std::string &name = "");

    void bindVAO();
    void draw(GLuint defaultFBO = 0);

    void resize(int width, int height);
    void setWireframeOn(bool wireframeOn);
    void setClearColor(const glm::vec3& color);
    glm::vec3 getClearColor();

    void showXSlice(bool show = true);
    void showYSlice(bool show = true);
    void showZSlice(bool show = true);

    bool isXSliceShown();
    bool isYSliceShown();
    bool isZSliceShown();

    void showObject(unsigned int objIndex, bool show = true);
    bool isObjectShown(unsigned int objIndex);

    void updateXSlice(unsigned int index);
    void updateYSlice(unsigned int index);
    void updateZSlice(unsigned int index);

    unsigned int getCurrentSlice(NewVolume::Axis axis);
    unsigned int getCurrentXSlice();
    unsigned int getCurrentYSlice();
    unsigned int getCurrentZSlice();


    void pick(glm::ivec2 screenPos, int& object, int& triangle);
    void getDepth(glm::ivec2 screenPos, float &depth);



    ArcballCamera camera;

    bool changed;

    std::vector< opengl::UntexturedMesh > objects;

private:
    void createFramebuffer();
    void createWireframeTexture(int thickness);

    std::vector< bool > showObjects;

    VolumeRenderer volumeRenderer;

    QOpenGLShaderProgram objectsProgram;
    GLuint objectsVAO;
    GLuint wireframeTex;

    glm::vec3 clearColor;

    GLuint fbo;
    GLuint sceneTex;
    GLuint pickingTex;
    GLuint depthTex;

    bool wireframeOn;

};

#endif // SCENE_H
