#ifndef SKETCHSURFACEGENERATOR_H
#define SKETCHSURFACEGENERATOR_H

#include <glm/glm.hpp>
#include <vector>

#include "mesh.h"
#include <fstream>

class SketchSurfaceGenerator
{
public:
    using Sketch = std::vector<glm::vec3>;
    SketchSurfaceGenerator(const std::vector<Sketch>& sketches);
    virtual ~SketchSurfaceGenerator() {}

    virtual void run(opengl::UntexturedMesh& outputMesh) = 0;

    //Temporary functions used for test
    static void readXYZ(const std::string &path, std::vector<glm::vec3> &points)
    {
        std::ifstream input;
        input.open( path );

        if (input.is_open( ))
        {
            unsigned int numPoints;
            input >> numPoints;

            points.reserve(numPoints);

            for(unsigned int i = 0; i < numPoints; ++i)
            {
                glm::vec3 p;
                input >> p.x >> p.y >> p.z;

                points.push_back(p);
            }

            input.close( );
        }
    }
    static void writeXYZ( const std::string& path, const std::vector< glm::vec3 >& points )
    {
        std::ofstream output;
        output.open( path );

        if (output.is_open( ))
        {
            output << points.size( ) << "\n";

            for (const auto& p : points)
            {
                output << p.x << " " << p.y << " " << p.z << "\n";
            }

            output.close( );
        }
    }

protected:
    std::vector<Sketch> sketches;
};

class SketchInterpolationGenerator : public SketchSurfaceGenerator
{
public:
//    using Sketch = std::vector<glm::vec3>;
    SketchInterpolationGenerator(const std::vector<Sketch>& sketches);
    virtual ~SketchInterpolationGenerator() override {}

    virtual void run(opengl::UntexturedMesh& outputMesh) override;

    void setSketchNumPoints(unsigned int value);

private:
    void resampleCurve(Sketch &curve, unsigned int numPoints);
    void alignCurve(const Sketch &refCurve, Sketch &editCurve);

    const std::vector<Sketch> surfaceGrid;

    unsigned int sketchNumPoints;
};

#endif // SKETCHSURFACEGENERATOR_H
