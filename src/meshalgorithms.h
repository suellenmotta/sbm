#ifndef MESHALGORITHMS_H
#define MESHALGORITHMS_H

#include "mesh.h"
#include "artme/CornerTable.h"
#include "graph.h"
#include <queue>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>

float distancePointSegment(const glm::vec2& point,
                           const glm::vec2& lPoint1,
                           const glm::vec2& lPoint2);

float distancePointSegments(const glm::vec2& point,
                            const std::vector< glm::vec2 >& segments);


void findClosestEdgePath(const opengl::UntexturedMesh& mesh,
                         const std::vector< glm::vec2 >& curve, const glm::mat4& model,
                         const glm::mat4& view, const glm::mat4& proj, const glm::vec4 viewport,
                         std::vector< unsigned int >& edgePath,
                         std::vector< glm::vec3 >& edgePathScreenPoints,
                         const std::set< unsigned int >& triangles = std::set< unsigned int >() );


void trianglesInsidePolygon(const CornerTable& cornerTable,
                            std::vector< unsigned int >& polygon,
                            std::set< unsigned int >& triangles);


/**
 * @brief      Create hole in mesh deleting given triangles inside a ROI
 *
 * @param      mesh             The mesh
 * @param[in]  selectionBorder  ROI polygon represented by mesh edges
 * @param[in]  triangles        The triangles inside polygon
 */
void createHole(opengl::UntexturedMesh& mesh,
                const std::vector< unsigned int >& selectionBorder,
                const std::set< unsigned int >& triangles);


/**
 * @brief      Find borders inside a ROI polygon.
 * Steps:
 * - Get the first layer vertexex inside the polygon and put in queue. The
 *   1-star of each vertex of queue is inside polygon.
 * - To each vertex in queue get the 1-star and put vertexes not visited and not in polygon in queue.
 * - If find border's vertex, follow the triangle neighborhood to track the
 *   hole's border
 *
 * @param[in]  cornerTable  The corner table
 * @param      polygon      The polygon
 * @param      borders      The triangles
 */
void bordersInsidePolygon(const CornerTable& cornerTable,
                            std::vector< unsigned int >& polygon,
                            std::vector< std::vector< unsigned int > >& borders);


void glMeshToCornerTable(const opengl::UntexturedMesh &glMesh, CornerTable** ctMesh);

void cornerTableToGlMesh(const CornerTable* ctMesh, opengl::UntexturedMesh &glMesh);

void findBorders(const opengl::UntexturedMesh& mesh, std::vector< std::vector< unsigned int > >& borders);

/**
 * @brief      Computes the score obtained when generating a triangle
 * in the ear cutting algorithm triangulate_hole_ear_cutting().
 *
 * @param[in]  screenPos1  The screen position 1
 * @param[in]  screenPos2  The screen position 2
 * @param[in]  screenPos3  The screen position 3
 *
 * @return     the score obtained when generating an ear from
 *     screenPos2-screenPos1 to screenPos3-screenPos2
 */
double earScore(
    const glm::vec3& screenPos1,
    const glm::vec3& screenPos2,
    const glm::vec3& screenPos3
);

/**
 * @brief      Triangulates holes using the ear cutting algorithm projecting
 *     hole poitns in screen space and get transfering this connection to 3D.
 * @param[in]  holes_in  The vector of holes, each hole is represented by a
 *                       vector of vertex indexes.
 *
 * @param[in]  mesh      The mesh structre that contains the vertex list
 * @param[in]  model     The model matrix
 * @param[in]  view      The view matrix
 * @param[in]  proj      The project matrix
 * @param[in]  viewport  The viewport
 */
void triangulateHoles(
    opengl::UntexturedMesh& mesh,
    const std::vector< std::vector<unsigned int> >& holes_in,
    const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj, const glm::vec4 viewport
);

/**
 * @brief removeRepeatedVertices
 * @param mesh
 */
void removeRepeatedVertices(std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<unsigned int> &indices);

#endif // MESHALGORITHMS_H
