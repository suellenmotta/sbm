#ifndef SURFACESLICING_H
#define SURFACESLICING_H

#include "mesh.h"
#include "newvolume.h"


class SurfaceSlicing
{
public:
    struct Edge
    {
        Edge(unsigned int u = 0, unsigned int v = 0, glm::vec3 p = glm::vec3()) : u(u),v(v),p(p){}
        unsigned int u;
        unsigned int v;
        glm::vec3 p;

        bool operator == (const Edge& other) const
        {
            return (u == other.u && v == other.v) ||
                   (u == other.v && v == other.u);
        }

        bool operator != (const Edge& other) const
        {
            return !this->operator ==(other);
        }
    };

    SurfaceSlicing(const opengl::UntexturedMesh& surface, NewVolume::Info volumeInfo);

    using CurvePoints = std::vector< glm::vec3 >;
    using SliceCurves = std::vector< CurvePoints >;
    using VolumeCurves = std::vector< SliceCurves >;
    using TriangleList = std::vector< unsigned int >;
    using SliceTriangles = std::vector< TriangleList >;

    using CrossingEdges = std::vector< Edge >;
    using SliceCrossingEdges = std::vector< CrossingEdges >;
    using VolumeCrossingEdges = std::vector< SliceCrossingEdges >;

    struct Segment
    {
        glm::vec3 u;
        glm::vec3 v;
        unsigned int t;
    };

    struct NewSegment
    {
        Edge e0;
        Edge e1;
        unsigned int t;
    };

    SliceCurves getSliceCurves(NewVolume::Axis axis, unsigned int slice);
    SliceCrossingEdges& getSliceCrossingEdges(NewVolume::Axis axis, unsigned int slice);

    std::vector<TriangleList> &getInterceptedTriangles(NewVolume::Axis axis, unsigned int slice);
    glm::vec3 getSliceNormal(NewVolume::Axis axis);
    float getVertexZCoord(unsigned int vertex, NewVolume::Axis axis);
    float getSliceZCoord(unsigned int slice, NewVolume::Axis axis);


    void sliceX();
    void sliceY();
    void sliceZ();
    void sliceAxis(NewVolume::Axis axis);
    void sliceAll();

    std::map<unsigned int, glm::vec3> computeNewTrianglePosition(NewVolume::Axis axis, unsigned int slice,
                                                        const Segment& oldSegment, const Segment& newSegment, std::map<unsigned int, glm::vec3> &newPositions);

    std::vector< short > getIntersectionConfig(unsigned int i0, unsigned int i1, unsigned int i2,
                                               NewVolume::Axis axis, unsigned int slice);
    std::vector< glm::vec3 > computeIntersection(unsigned int i0, unsigned int i1,
                                                 unsigned int i2, NewVolume::Axis axis, unsigned int slice,
                                                 std::vector<short> &intersectionCode = std::vector<short>());

    std::vector<Edge> computeIntersection(unsigned int t,
                             NewVolume::Axis axis, unsigned int slice);
    bool computeIntersection(unsigned int t, NewVolume::Axis axis, unsigned int slice,
                             Segment& segment, std::vector<short> &intersectionCode);

private:
    std::vector<short> M[3][3][3];
    void buildIntersectionMap();

    //Compute "z-coordinates": The distance of each vertex to the first slice of each axis
    void computeZCoords(NewVolume::Axis axis);

    const opengl::UntexturedMesh& surface;
    NewVolume::Info info;

    //Distances between the slices of each axis
    float spacing[3];

    //Volume slices normals
    glm::vec3 sliceNormals[3];

    std::vector< float > zCoords[3];
    VolumeCurves curves[3];
    VolumeCrossingEdges edges[3];
    std::vector< SliceTriangles > interceptedTriangles[3];
};

#endif // SURFACESLICING_H
