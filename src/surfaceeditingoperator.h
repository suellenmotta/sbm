#ifndef SURFACEEDITINGOPERATOR_H
#define SURFACEEDITINGOPERATOR_H

#include "operator.h"
#include "sketchsurfacedeformation.h"

#include <QOpenGLShaderProgram>

class SurfaceEditingOperator
        : public Operator
{
    Q_OBJECT

public:
    SurfaceEditingOperator(Scene& scene);
    ~SurfaceEditingOperator() override;

    void draw() override;

    void setSketchSurfaceDeformation(SketchSurfaceDeformation* deformer);
    void updateInterceptionCurves();
    void updateSketches();

    void showInterceptionCurves(bool show = true);
    void showSketches(bool show = true);

signals:
    void sketchFinished();

private:
    void drawInterceptionCurves();
    void drawSketches();

    bool checkSliceIntersection(const glm::ivec2 &screenPoint, glm::vec3& point);
    /**
     * @brief checkCurveIntersection - Checks the intersection of the point
     * with some curve in the curAxis and curSlice
     * @param[in] point - The point to be checked
     * @param[out] curveIdx - The curve intercepted if any, -1 if no one.
     * @return - True if finds an intersection
     */
    bool checkCurveIntersection(const glm::vec3& point, int& curveIdx);

    bool mousePress(QMouseEvent* event) override;
    bool mouseMove(QMouseEvent* event) override;
    bool mouseRelease(QMouseEvent* event) override;

    const VolumeRenderer& volumeRenderer;

    SketchSurfaceDeformation* deforming;

    QOpenGLShaderProgram program;

    GLuint VAO;
    GLuint sketchVBO;

    std::map< unsigned int, std::vector< GLuint > > curvesVBOs[3];
    std::map< unsigned int, std::vector< GLuint > > sketchesVBOs[3];

    NewVolume::Axis curAxis;
    unsigned int curSlice;
    bool drawing;
    bool pickingCurve;

    int curveIndex;
    bool curveClosed;
    glm::ivec2 firstPoint;
    std::vector<glm::vec3> sketchPoints;
    std::vector<glm::vec3> sketchPoints2;

    //Test
    GLuint sketchTestVBO;

    bool curvesShown;
    bool sketchesShown;

    std::vector< glm::vec3 > correspondence;
};

#endif // SURFACEEDITINGOPERATOR_H
