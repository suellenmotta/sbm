#ifndef SKETCHBASEDSURFACEDEFORMATIONWIDGET_H
#define SKETCHBASEDSURFACEDEFORMATIONWIDGET_H

#include "canvas3dwidget.h"
#include "sketchsurfacedeformation.h"
#include "surfaceeditingoperator.h"
#include "sketchsurfacegenerator.h"

#include <QWidget>

namespace Ui {
class SketchBasedSurfaceDeformationWidget;
}

class SketchBasedSurfaceDeformationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SketchBasedSurfaceDeformationWidget(QWidget *parent, Canvas3DWidget *canvas);

    void setSliceSketchingOperator(std::shared_ptr<SurfaceEditingOperator> sketchingOperator);

    virtual ~SketchBasedSurfaceDeformationWidget();

    void setSketchingOn();
    void setSketchingOff();

    void updateSurfaceList();

    void saveSketches(const std::string& dir);

signals:

public slots:
    void onSketchFinished();

private slots:
    void on_surfaceComboBox_currentIndexChanged(int);

    void on_showInterceptionCheckBox_toggled(bool checked);

    void on_snapToVolumeCheckBox_toggled(bool checked);

    void on_volumeComboBox_currentIndexChanged(int index);

    void on_viewVolumeToolButton_toggled(bool checked);

    void on_loadVolumeToolButton_clicked();

    void on_addSketchButton_clicked(bool checked);

    void on_deformSurfaceButton_clicked();

    void on_deleteSketchButton_clicked();

    void on_showAllSketches_toggled(bool checked);


    void on_curvatureRadioButton_toggled(bool checked);

    void on_autoDeformCheckBox_toggled(bool checked);

private:
    void fillSketchesList();
    void updateDeformer();

    Ui::SketchBasedSurfaceDeformationWidget *ui;

    Canvas3DWidget* canvas;
    SketchSurfaceDeformation* deformer;

    std::shared_ptr<SurfaceEditingOperator> sketchingOperator;
};

#endif // SKETCHBASEDSURFACEDEFORMATIONWIDGET_H
