#include "SketchBasedModelingWidget.h"

#include "ui_SketchBasedModelingWidget.h"
#include "CurveInterpolationWidget.h"
#include "sketchbasedsurfacedeformationwidget.h"
#include "canvas3dwidget.h"
#include "scene.h"
#include "mainwindow.h"

SketchBasedModelingWidget::SketchBasedModelingWidget(QWidget *parent, Canvas3DWidget* canvas)
    : QDockWidget(parent)
    , ui(createAndSetupUi())
    , canvas(canvas)
    , interpolationWidget(createAndSetupInterpolationWidget(canvas))
    , deformationWidget(createAndSetupDeformationWidget(canvas))
{
    if(autoName)
    {
        QString name = "surface_" + QString::number(canvas->getScene().objects.size());
        ui->surfaceNameLineEdit->setText(name);
    }
}

SketchBasedModelingWidget::~SketchBasedModelingWidget()
{

}

Ui::SketchBasedModelingWidget *SketchBasedModelingWidget::createAndSetupUi()
{
    Ui::SketchBasedModelingWidget* ui = new Ui::SketchBasedModelingWidget;
    ui->setupUi(this);
    return ui;
}

CurveInterpolationWidget *SketchBasedModelingWidget::createAndSetupInterpolationWidget(Canvas3DWidget* canvas)
{
    CurveInterpolationWidget* widget = new CurveInterpolationWidget(ui->surfaceCreationWidget,canvas);

    auto layout = dynamic_cast<QBoxLayout*>(ui->surfaceCreationWidget->layout());
    if(layout)
    {
        layout->insertWidget(1,widget,1);
    }

    return widget;
}

SketchBasedSurfaceDeformationWidget *SketchBasedModelingWidget::createAndSetupDeformationWidget(Canvas3DWidget *canvas)
{
    SketchBasedSurfaceDeformationWidget* widget = new SketchBasedSurfaceDeformationWidget(ui->tabWidget,canvas);

    ui->tabWidget->addTab(widget,"Deform surface");

    return widget;
}

void SketchBasedModelingWidget::on_createSurfaceButton_clicked()
{
    opengl::UntexturedMesh mesh;
    interpolationWidget->computeSurface(mesh);
    mesh.material.diffuse = canvas->nextColor();
    mesh.name = ui->surfaceNameLineEdit->text().toStdString();

    //TODO: Improve this relation:
    MainWindow* window = dynamic_cast<MainWindow*>(this->parent());
    if(window)
        window->addSurface(mesh.name);

    auto& scene = canvas->getScene();
    scene.addObject(std::move(mesh));
    canvas->update();

    if(autoName)
    {
        QString name = "surface_" + QString::number(scene.objects.size());
        ui->surfaceNameLineEdit->setText(name);
    }

    deformationWidget->updateSurfaceList();
}

void SketchBasedModelingWidget::on_tabWidget_currentChanged(int)
{
    interpolationWidget->setSketchingOff();
    deformationWidget->setSketchingOff();
}
