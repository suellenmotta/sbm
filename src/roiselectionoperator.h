#ifndef ROISELECTIONOPERATOR_H
#define ROISELECTIONOPERATOR_H

#include "operator.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QVector3D>

#include <glm/glm.hpp>
#include <artme/CornerTable.h>
#include <artme/CornerTableAdaptiveRefinement.h>

#include <set>

class ROISelectionOperator : public Operator
{
public:
   ROISelectionOperator(Scene& scene);
   virtual ~ROISelectionOperator();

   void draw() override;

   bool mousePress(QMouseEvent* event) override;
   bool mouseMove(QMouseEvent* event) override;
   bool mouseRelease(QMouseEvent* event) override;
   bool mouseDoubleClick(QMouseEvent * event) override;

private:
   bool intercepts( int x, int y, int& triangleId, float& depth );
   QOpenGLShaderProgram program;
   QOpenGLBuffer sketchBuffer;
   QOpenGLBuffer edgesBuffer;

   // void subdivide(int triangleID, const glm::vec3 &point);
   void adjust();

   std::vector< glm::vec3 > sketchPoints;
   std::vector< glm::vec2 > sketchPoints2d;
   std::vector< glm::vec3 > edgePoints;

   CornerTable* mesh;
   CornerTableAdaptiveRefinement* refinement;

   bool closed;
};

#endif // ROISELECTIONOPERATOR_H
