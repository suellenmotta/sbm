#include "surfaceeditingoperator.h"

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/norm.hpp>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <list>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>
#include <Eigen/SparseQR>

#include "raycasting.h"
#include "surfaceslicing.h"
#include "harmonicsurfacedeformation.h"

#include <QDirIterator>
#include <QTextStream>

void writeIntList( const std::string& path, const std::vector< unsigned int >& values )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << values.size( ) << "\n";

        for (const auto& v : values)
        {
            output << v << "\n";
        }

        output.close( );
    }
}



void readXYZ(const std::string &path, std::vector<glm::vec3> &points)
{
    std::ifstream input;
    input.open( path );

    if (input.is_open( ))
    {
        unsigned int numPoints;
        input >> numPoints;

        points.reserve(numPoints);

        for(unsigned int i = 0; i < numPoints; ++i)
        {
            glm::vec3 p;
            input >> p.x >> p.y >> p.z;

            points.push_back(p);
        }

        input.close( );
    }
}

void writeOFF( const std::string& path, const std::vector<glm::vec3>& vertices,
                          const std::vector<unsigned int>& indices)
{
    std::ofstream out( path );
    if (!out)
    {
        printf( "Error writing the file %s\n", path.c_str( ) );
        return;
    }

    out << "OFF\n";

    auto numElements = indices.size( ) / 3;
    out << vertices.size( ) << " " << numElements << " 0\n";

    for (const auto& p : vertices)
    {
        out << p.x << " " << p.y << " " << p.z << "\n";
    }

    for (unsigned int i = 0, pos = 0; i < numElements; i++, pos += 3)
    {
        out << "3 ";
        out << indices[pos + 0] << " "
                                << indices[pos + 1] << " "
                                << indices[pos + 2] << "\n";
    }

    out.close( );
}

SurfaceEditingOperator::SurfaceEditingOperator(Scene &scene)
    : Operator(scene)
    , volumeRenderer(scene.getVolumeRenderer())
    , deforming(nullptr)
    , curAxis(NewVolume::X)
    , curSlice(0)
    , drawing(false)
    , pickingCurve(false)
    , curveClosed(false)
    , curvesShown(true)
    , sketchesShown(false)
{
    initializeOpenGLFunctions();

    program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/solid-color-vert");
    program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/solid-color-frag");
    program.link();

    if( !program.isLinked() )
        qCritical() << "[SurfaceEditingOperator] Shader program not linked";

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glEnableVertexAttribArray( 0 );
    glVertexAttribFormat(0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0,0);

    glGenBuffers(1, &sketchVBO);
    glGenBuffers(1, &sketchTestVBO);
}

SurfaceEditingOperator::~SurfaceEditingOperator()
{
}

void SurfaceEditingOperator::draw()
{
    if(!scene.objects.empty() && deforming)
    {
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_POINT_SMOOTH);

        program.bind();

        QMatrix4x4 m(glm::value_ptr(glm::transpose(scene.camera.modelMatrix())));
        QMatrix4x4 v(glm::value_ptr(glm::transpose(scene.camera.viewMatrix())));
        QMatrix4x4 p(glm::value_ptr(glm::transpose(scene.camera.projMatrix())));

        auto mvp = p*v*m;
        program.setUniformValue("MVP", mvp);

        glBindVertexArray(VAO);

        glLineWidth(5.0f);

        if(curvesShown)
        {
            drawInterceptionCurves();
        }

        if(sketchesShown)
        {
            drawSketches();
        }

        if(drawing || pickingCurve)
        {
            glDisable(GL_DEPTH_TEST);
            program.setUniformValue("color",0.0f,1.0f,1.0f);
            glBindVertexBuffer(0,sketchVBO,0,sizeof(glm::vec3));
            glDrawArrays(GL_LINE_STRIP,0,static_cast<GLsizei>(sketchPoints.size()));
        }

        glBindVertexArray(0);
        program.release();
    }

}

void SurfaceEditingOperator::setSketchSurfaceDeformation(SketchSurfaceDeformation *deformer)
{
    deforming = deformer;
    updateInterceptionCurves();
    updateSketches();
}


void SurfaceEditingOperator::updateInterceptionCurves()
{
    const auto& volume = scene.getVolume();
    const auto& info = volume->getInfo();
    unsigned int numSlices[3] = { info.numXSlices, info.numYSlices, info.numZSlices};

    for(int axis = 0; axis < 3; ++axis)
    {
        //Delete all buffers
        for(auto it : curvesVBOs[axis])
        {
            glDeleteBuffers(it.second.size(),it.second.data());
        }
        curvesVBOs[axis].clear();

        for(unsigned int slice = 0; slice < numSlices[axis]; ++slice)
        {
            auto curves = deforming->getInterceptionCurves((NewVolume::Axis)axis,slice);

            if(!curves.empty())
            {
                curvesVBOs[axis][slice] = std::vector<GLuint>();

                for(unsigned int i = 0; i < curves.size(); ++i)
                {
                    GLuint vbo;
                    glGenBuffers(1, &vbo);

                    glBindBuffer(GL_ARRAY_BUFFER, vbo);
                    glBufferData(GL_ARRAY_BUFFER, curves[i].size()*sizeof(glm::vec3),
                                 curves[i].data(), GL_DYNAMIC_DRAW);

                    curvesVBOs[axis][slice].push_back(vbo);
                }
            }
        }
    }


    /////// PARA GERAR FIGURA ///////
//    for(unsigned int slice = 0; slice < volume->numZSlices(); slice+=50)
//    {
//        auto curves = deforming->getInterceptionCurves(NewVolume::Z,slice);

//        if(!curves.empty())
//        {
//            auto intCurve = curves[0];

//            std::stringstream path;
//            path << "int-curve-depois-" << slice << ".xyz";
//            writeXYZ(path.str(),intCurve);
//        }
//    }

//    auto& surface = deforming->getDeformingSurface();
//    std::vector<glm::vec3> vertices;
//    vertices.reserve(surface.vertexBuffer.size());
//    for(auto v : surface.vertexBuffer)
//    {
//        vertices.push_back(v.position);
//    }

//    writeOFF("mesh-depois.off",vertices,surface.indices);

    /////////////////////////////////

    ////TESTE DE CORRESPONDENCIA SKETCH-INTERSEÇÃO////
//    correspondence.clear();
//    for(int axis = 0; axis < 3; ++axis)
//    {
//        auto allSketches = deforming->getAllSketches((NewVolume::Axis)axis);

//        for(auto entry : allSketches)
//        {
//            auto slice = entry.first;
//            auto sketch = entry.second[0];
//            auto curve = deforming->getInterceptionCurves((NewVolume::Axis)axis,slice)[0];

//            if(curve.size()==sketch.size())
//            {
//                for(unsigned int j = 0; j < curve.size(); ++j)
//                {
//                    correspondence.push_back(curve[j]);
//                    correspondence.push_back(sketch[j]);
//                }
//            }
//        }
//    }
//    glBindBuffer(GL_ARRAY_BUFFER, sketchTestVBO);
//    glBufferData(GL_ARRAY_BUFFER, correspondence.size()*sizeof(glm::vec3),
//                 correspondence.data(), GL_DYNAMIC_DRAW);
//    glBindBuffer(GL_ARRAY_BUFFER,0);
    ////////////////////////////////////////////////

    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void SurfaceEditingOperator::updateSketches()
{
    for(int axis = 0; axis < 3; ++axis)
    {
        for(auto it : sketchesVBOs[axis])
        {
            glDeleteBuffers(it.second.size(),it.second.data());
        }
        sketchesVBOs[axis].clear();

        auto sketches = deforming->getAllSketches((NewVolume::Axis)axis);

        for(const auto entry : sketches)
        {
            auto slice = entry.first;
            auto sliceSketches = entry.second;

            sketchesVBOs[axis][slice] = std::vector< GLuint >();

            for(unsigned int i = 0; i < sliceSketches.size(); ++i)
            {
                GLuint vbo;
                glGenBuffers(1, &vbo);

                glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glBufferData(GL_ARRAY_BUFFER, sliceSketches[i].size()*sizeof(glm::vec3),
                             sliceSketches[i].data(), GL_DYNAMIC_DRAW);

                sketchesVBOs[axis][slice].push_back(vbo);
            }
        }
    }


    glBindBuffer(GL_ARRAY_BUFFER,0);
}

void SurfaceEditingOperator::showInterceptionCurves(bool show)
{
    curvesShown = show;
}

void SurfaceEditingOperator::showSketches(bool show)
{
    sketchesShown = show;
}

void SurfaceEditingOperator::drawInterceptionCurves()
{
    auto color = deforming->getDeformingSurface().material.diffuse;
    auto hColor = color * 1.5f;

    bool sliceShown[3] = {scene.isXSliceShown(), scene.isYSliceShown(), scene.isZSliceShown()};

    for(int ax = 0; ax < 3; ++ax)
    {
        NewVolume::Axis axis = static_cast<NewVolume::Axis>(ax);

        if(sliceShown[axis])
        {
            auto slice = scene.getCurrentSlice(axis);

            auto it = curvesVBOs[axis].find(slice);

            if(it!=curvesVBOs[axis].end())
            {
                auto& vbos = it->second;

                for(unsigned int i = 0; i < vbos.size(); ++i)
                {
                    if(pickingCurve && curAxis == axis && static_cast<int>(i) == curveIndex)
                    {
                        program.setUniformValue("color", hColor.r, hColor.g, hColor.b);
                    }
                    else
                    {
                        program.setUniformValue("color", color.r, color.g, color.b);
                    }

                    glBindVertexBuffer(0,vbos[i],0,sizeof(glm::vec3));
                    auto count = static_cast<GLsizei>(deforming->getInterceptionCurves(axis,slice)[i].size());
                    glDrawArrays(GL_LINE_STRIP,0,count);
                }
            }
        }
    }
}

void SurfaceEditingOperator::drawSketches()
{
    program.setUniformValue("color",0.0f,0.0f,1.0f);
    for(int ax = 0; ax < 3; ++ax)
    {
        NewVolume::Axis axis = static_cast<NewVolume::Axis>(ax);

        for(auto& vbos : sketchesVBOs[axis])
        {
            auto& sketches = deforming->getSketches(axis,vbos.first);

            for(unsigned int i = 0; i < vbos.second.size(); ++i)
            {
                auto count = static_cast<GLsizei>(sketches[i].size());
                auto vbo = vbos.second[i];

                glBindVertexBuffer(0,vbo,0,sizeof(glm::vec3));
                glDrawArrays(GL_LINE_STRIP,0,count);
            }
        }
    }

    //Teste de correspondencia do sketch
//            glLineWidth(1.0f);
//            glPointSize(8.0f);
//            program.setUniformValue("color",0.3f,0.3f,0.3f);
//            glBindVertexBuffer(0,sketchTestVBO,0,sizeof(glm::vec3));
//            glDrawArrays(GL_LINES,0,correspondence.size());
//            program.setUniformValue("color",1.0f,0.0f,0.0f);
//            glDrawArrays(GL_POINTS,0,correspondence.size());
}

bool SurfaceEditingOperator::checkSliceIntersection(const glm::ivec2 &screenPoint, glm::vec3 &point)
{
    auto dir = rayDirection(screenPoint,scene.camera);

    bool intercepts = false;

    float dist = 0.0f;
    switch(curAxis)
    {
        case NewVolume::X:
            intercepts = volumeRenderer.interceptsXSlice(scene.camera.eye,dir,dist);
            break;
        case NewVolume::Y:
            intercepts = volumeRenderer.interceptsYSlice(scene.camera.eye,dir,dist);
            break;
        case NewVolume::Z:
            intercepts = volumeRenderer.interceptsZSlice(scene.camera.eye,dir,dist);
            break;
    }

    if(intercepts)
    {
        point = glm::vec3(
                glm::inverse(scene.camera.modelMatrix())
                *glm::vec4(scene.camera.eye + dist * dir,1));

        return true;
    }

    return false;
}

bool SurfaceEditingOperator::checkCurveIntersection(const glm::vec3 &point, int& curveIdx)
{
    curveIdx = -1;
    const auto& curves = deforming->getInterceptionCurves(curAxis,curSlice);

    float epsilon = 100.0f;
    auto checkIntersection = [&point,&epsilon](const std::vector<glm::vec3>& curve)
    {
        for(auto p : curve)
        {
            if(glm::distance2(point,p) < epsilon)
            {
                return true;
            }
        }

        return false;
    };

    for(unsigned int i = 0; i < curves.size(); ++i)
    {
        if(checkIntersection(curves[i]))
        {
            curveIdx = static_cast<int>(i);
            return true;
        }
    }

    return false;
}

bool SurfaceEditingOperator::mousePress(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton)
    {
        glm::ivec2 screenPos(event->x(), event->y());

        if(pickingCurve) //Updates the picked curve index
        {
            glm::vec3 point;
            if(checkSliceIntersection(screenPos,point))
            {
                if(checkCurveIntersection(point,curveIndex))
                {
                    std::cout << "UHUL! " << curveIndex << std::endl;
                }
            }
        }
        else //Start drawing other sketch
        {
            sketchPoints.clear();

            glm::vec3 point;
            if(volumeRenderer.intercepts(screenPos,curAxis,point))
            {
                glBindBuffer(GL_ARRAY_BUFFER, sketchVBO);
                glBufferData(GL_ARRAY_BUFFER, sketchPoints.size()*sizeof(glm::vec3),
                             sketchPoints.data(), GL_DYNAMIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                firstPoint = screenPos;
                drawing = true;
                curveClosed = false;
                pickingCurve = false;
                curveIndex = -1;

                curSlice = scene.getCurrentSlice(curAxis);
            }
        }

        return true;
    }

    return false;
}

bool SurfaceEditingOperator::mouseMove(QMouseEvent *event)
{
    const glm::ivec2 screenPos(event->x(), event->y());

    if(drawing)
    {
        if(!curveClosed && event->buttons() & Qt::LeftButton)
        {
            glm::vec3 point;
            if(checkSliceIntersection(screenPos,point))
            {
                sketchPoints.push_back(point);

                if(sketchPoints.size() > 10 &&
                   glm::distance(glm::vec2(screenPos),glm::vec2(firstPoint)) < 10) //The user has closed the curve
                {
                    sketchPoints.push_back(sketchPoints[0]);
                    curveClosed = true;
                }

                glBindBuffer(GL_ARRAY_BUFFER, sketchVBO);
                glBufferData(GL_ARRAY_BUFFER, sketchPoints.size()*sizeof(glm::vec3),
                             sketchPoints.data(), GL_DYNAMIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                return true;
            }
        }
    }

    else if(pickingCurve)
    {
        glm::vec3 point;
        if(checkSliceIntersection(screenPos,point))
        {
            auto oldIdx = curveIndex;
            if(checkCurveIntersection(point,curveIndex))
            {
                std::cout << "UHUL! " << curveIndex << std::endl;
            }

            return oldIdx != curveIndex;
        }
    }

    return false;
}

bool SurfaceEditingOperator::mouseRelease(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        if(drawing)
        {
            drawing = false;
            curveClosed = false;

            if(sketchPoints.size()<2)
            {
                sketchPoints.clear();
                return true;
            }

            curveIndex = -1;

            if(deforming->getInterceptionCurves(curAxis,curSlice).size()>1)
            {
                pickingCurve = true;
            }
            else
            {
                curveIndex = 0;
            }
        }

        if(curveIndex != -1)
        {
            pickingCurve = false;

            auto index = static_cast<uint>(curveIndex);
            if(deforming->addSketch(curAxis,curSlice,sketchPoints,index))
            {
                auto& sketch = deforming->getSketches(curAxis,curSlice)[index];

                if(sketchesVBOs[curAxis].find(curSlice)==sketchesVBOs[curAxis].end())
                {
                    sketchesVBOs[curAxis][curSlice] = std::vector< GLuint >();

                    GLuint vbo;
                    glGenBuffers(1, &vbo);

                    sketchesVBOs[curAxis][curSlice].push_back(vbo);
                }

                auto vbo = sketchesVBOs[curAxis][curSlice][0];

                glBindBuffer(GL_ARRAY_BUFFER, vbo);
                glBufferData(GL_ARRAY_BUFFER, sketch.size()*sizeof(glm::vec3),
                             sketch.data(), GL_DYNAMIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER,0);

                scene.changed = true;
                emit sketchFinished();

                sketchPoints.clear();
                return true;
            }
        }
    }
//    else if(pickingCurve)
//    {
//        pickingCurve = false;
//        sketchPoints.clear();
//        return true;
//    }

    return false;
}
