#ifndef SLICESKETCHINGOPERATOR_H
#define SLICESKETCHINGOPERATOR_H

#include "operator.h"

#include <QOpenGLShaderProgram>

class SliceSketchingOperator
        : public Operator
{
    Q_OBJECT

public:
    SliceSketchingOperator(Scene& scene);

    std::vector< std::pair< NewVolume::Axis, unsigned int > > getSketchedSlices();
    const std::map< unsigned int, std::vector< glm::vec3 > >& getSketches(NewVolume::Axis axis);
    std::vector< std::string > getSketchNames();
    std::vector< glm::vec3 > getSketchesPointCloud();
    void deleteSketch(NewVolume::Axis axis, unsigned int index);
    void deleteSketches();

    void draw() override;

    bool mousePress(QMouseEvent*) override;
    bool mouseMove(QMouseEvent* event) override;
    bool mouseRelease(QMouseEvent* event) override;

    void showOnlyVisible();
    void showAllSketches();
    void showLines();
    void showPointCloud();

signals:
    void sketchFinished();

private:
    const VolumeRenderer& volumeRenderer;

    bool onlyVisible;
    GLenum drawMode;
    NewVolume::Axis curAxis;
    bool preview;
    bool curveClosed;

    QOpenGLShaderProgram program;

    GLuint VAO;
    GLuint previewVBO;
    GLuint inlineVBO;
    GLuint crosslineVBO;
    GLuint depthVBO;

    std::vector< glm::vec3 > sketchPoints;
    std::vector< glm::ivec2 > sketchPoints2d;

    std::map< unsigned int, GLuint > xVBOs;
    std::map< unsigned int, GLuint > yVBOs;
    std::map< unsigned int, GLuint > zVBOs;

    std::map< unsigned int, std::vector< glm::vec3 > > xSketches;
    std::map< unsigned int, std::vector< glm::vec3 > > ySketches;
    std::map< unsigned int, std::vector< glm::vec3 > > zSketches;
};

#endif // SLICESKETCHINGOPERATOR_H
