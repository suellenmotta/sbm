#ifndef SEISMICVOLUME_H
#define SEISMICVOLUME_H

#include <string>
#include <glm/glm.hpp>

#include "newvolume.h"

namespace seismic
{

struct Info
{
    unsigned int
        firstInline,
        firstCrossline,
        firstDepth,
        stepInline,
        stepCrossline,
        stepDepth,
        lastInline,
        lastCrossline,
        lastDepth;

    /*
     * Volume bounding box global coordinates
     *
     *   p4------------p3
     *   |             |    ↑
     *   |             |  Inlines
     *   |             |    ↑
     *   p1------------p2
     *    → Crosslines →
     *
     */

    glm::vec2 p1, p2, p3, p4;
};


class Volume : public NewVolume
{
public:
    Volume();
    Volume(const seismic::Info& seismicInfo);
    Volume(const seismic::Info& seismicInfo, float* buffer);
    Volume(const seismic::Volume& other);
    virtual ~Volume();

    virtual NewVolume *getCopy() const;

//To be implemented:
//    float* inlineSlice(unsigned int inlineNum);
//    float* crosslineSlice(unsigned int crosslineNum);
//    float* depthSlice(unsigned int depthSliceNum);

    seismic::Info seismicInfo;

    virtual glm::mat4 getStartModelMatrix() const override;

    static NewVolume::Info toVolumeInfo(const seismic::Info& seismicInfo);

private:
    unsigned int inlineIdx(unsigned int inlineNum);
    unsigned int crosslineIdx(unsigned int crosslineNum);
    unsigned int depthIdx(unsigned int depthSliceNum);
};

/**
 * @brief Reads seismic volume written in OpendTect simple (flat) 3d file format.
 * @param filePath
 * @param volume Seismic volume read from file
 * @return true if successfully read
 */
NewVolume* readSeismicVolume(const std::string& filePath, const seismic::Info &info);

}
#endif // SEISMICVOLUME_H
