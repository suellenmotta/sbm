#version 400 core

layout( location = 0 ) in vec3 m_vertexPos;
layout( location = 1 ) in vec3 m_vertexNormal;

uniform mat4 MVP;
uniform mat4 MV;
uniform mat4 MV_TI; //inversa transposta da MV

out vec3 v_vertexPos;
out vec3 v_vertexNormal;

void main()
{
    gl_Position = MVP * vec4( m_vertexPos, 1 );

    // Posição do vértice no espaço da câmera
    v_vertexPos = ( MV * vec4( m_vertexPos, 1 ) ).xyz;

    // Normal do vértice, no espaço da câmera
    v_vertexNormal = ( MV_TI * vec4( m_vertexNormal, 0 ) ).xyz;
}
