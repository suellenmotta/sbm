#version 400 core

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 v_vertexPos[];
in vec3 v_vertexNormal[];

out vec3 S;
out vec3 fPos;
out vec3 fNormal;

void main()
{
    gl_PrimitiveID = gl_PrimitiveIDIn;

    S.x = 1.0;
    S.y = 1.0;
    S.z = 0.0;

    fPos = v_vertexPos[0];
    fNormal = v_vertexNormal[0];
    gl_Position = gl_in[0].gl_Position;
    EmitVertex();

    S.x = 0.0;
    S.y = 1.0;
    S.z = 1.0;

    fPos = v_vertexPos[1];
    fNormal = v_vertexNormal[1];
    gl_Position = gl_in[1].gl_Position;
    EmitVertex();

    S.x = 1.0;
    S.y = 0.0;
    S.z = 1.0;

    fPos = v_vertexPos[2];
    fNormal = v_vertexNormal[2];
    gl_Position = gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
