#version 400 core

in vec2 uv;
uniform sampler2D valueSampler;
uniform sampler2D mixTextureSampler;
uniform sampler1D lutSampler;
out vec3 color;

uniform uint numMarkers;
uniform float colorMarkers[10];

uniform float maxValue;
uniform float minValue;

uniform bool useColorTable;

void main()
{
    float value = texture( valueSampler, uv ).r;
    float u = clamp((value-minValue)*(1/(maxValue-minValue)),0.0f ,1.0f);
    vec3 color1 = vec3(u,u,u);

    float value2 = texture( mixTextureSampler, uv ).r;
    float s = clamp((value2-0.0f)*(1/(5.0f-0.0f)),0.0f ,1.0f);
    vec3 color2 = texture(lutSampler,s).rgb;

    color = mix(color1,color2,0.6f);
//    color = vec3(0.7f,0.7f,0.8f);
}
