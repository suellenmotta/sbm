#version 400 core

struct Light
{
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct Material
{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

uniform int objectID;
uniform Light light;
uniform Material material;
uniform int numPrimitives;
uniform sampler1D wireframeTexture;
uniform vec3 wireframeColor;

uniform bool wireframe;

in vec3 S;
in vec3 fPos;
in vec3 fNormal;

layout( location = 0 ) out vec3 finalColor;
layout( location = 1 ) out ivec2 pickingInfo;

void main()
{
    vec3 ambient = material.ambient * material.diffuse; // * light.ambient;
    vec3 diffuse = vec3(0.0,0.0,0.0);
    vec3 specular = vec3(0.0,0.0,0.0);

    vec3 N = normalize(fNormal);
    vec3 L = normalize(light.position - fPos);

    float iDif = abs(dot(L,N));

    diffuse = iDif * material.diffuse; // * light.diffuse;

    vec3 V = normalize(-fPos);
    vec3 H = normalize(L + V);

    float iSpec = pow(abs(dot(N,H)),material.shininess);
    specular = iSpec * material.specular; // * light.specular;

    finalColor = ambient + diffuse + specular;

    pickingInfo = ivec2(objectID,gl_PrimitiveID);

    if(wireframe)
    {
        float s1 = texture(wireframeTexture, S.x).r;
        float s2 = texture(wireframeTexture, S.y).r;
        float s3 = texture(wireframeTexture, S.z).r;

        finalColor = mix( finalColor, wireframeColor, s1 );
        finalColor = mix( finalColor, wireframeColor, s2 );
        finalColor = mix( finalColor, wireframeColor, s3 );
    }

}
