#version 400 core

in vec2 uv;
uniform sampler2D valueSampler;
uniform sampler1D lutSampler;
out vec3 color;

uniform uint numMarkers;
uniform float colorMarkers[10];

uniform float minValue;
uniform float maxValue;

uniform bool useColorTable;

void main()
{
    float value = texture( valueSampler, uv ).r;
    float u = clamp((value-minValue)*(1/(maxValue-minValue)),0.0f ,1.0f);

//    vec3 darkblue = vec3(0.270588f,0.458824f,0.705882f);
//    vec3 blue     = vec3(0.568627f,0.749020f,0.858824f);
//    vec3 lightblue = vec3(0.878431f,0.952941f,0.972549f);
//    vec3 yellow = vec3(0.996078f,0.878431f,0.564706f);
//    vec3 orange = vec3(0.988235f,0.552941f,0.349020f);
//    vec3 red = vec3(0.843137f,0.188235f,0.152941f);

//    //gambiarra
//    if(value>0)
//        color = darkblue;
//    else
//        color = yellow;
//    return;

    if(useColorTable)
    {


        //    if( u > colorMarkers[5] ) u = colorMarkers[5];

        //    uint maxIdx=0;
        //    while(u>colorMarkers[maxIdx]) maxIdx++;

        //    float p = 1.0f / 5.0f;
        //    if(maxIdx < 1) u = 0.0f;
        //    else
        //    {
        //        uint minIdx = maxIdx-1;

        //        float f = p / (colorMarkers[maxIdx]-colorMarkers[minIdx]);

        ////        u = ((u-colorMarkers[minIdx])*f) + (minIdx*p);
        //        u = colorMarkers[maxIdx];
        //    }

            color = texture(lutSampler,u).rgb;
    }
    else
    {
        color = vec3(u,u,u);
    }
}
