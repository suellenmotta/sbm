#version 400 core

layout( location = 0 ) in vec3 m_vertexPos;

uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4( m_vertexPos, 1 );
}
