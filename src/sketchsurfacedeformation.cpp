#include "sketchsurfacedeformation.h"

#include <iostream>
#include <iomanip>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <fstream>

#include <igl/cotmatrix.h>

#include "graph.h"

void writeXYZ( const std::string& path, const std::vector< glm::vec3 >& points )
{
    std::ofstream output;
    output.open( path );

    if (output.is_open( ))
    {
        output << points.size( ) << "\n";

        for (const auto& p : points)
        {
            output << p.x << " " << p.y << " " << p.z << "\n";
        }

        output.close( );
    }
}

SketchSurfaceDeformation::SketchSurfaceDeformation(const NewVolume *volume, SketchSurfaceDeformation::Surface &surface)
    : slicer(new SurfaceSlicing(surface,volume->getInfo()))
    , surface(surface)
    , minimizeDisplacements(true)
    , G(surface.vertexBuffer.size())
{
    slicer->sliceAll();
    std::cout << "Slice all called" << std::endl;


//    buildMeshGraph();
    buildEigenMesh();
}

bool SketchSurfaceDeformation::addSketch(NewVolume::Axis axis, unsigned int slice, SketchSurfaceDeformation::Sketch& sketch, unsigned int index)
{
//    std::stringstream file;
//    file << "sketch-" << axis << "-" << slice << "-" << index << ".xyz";
//    writeXYZ(file.str(),sketch);

    const auto& intCurves = slicer->getSliceCurves(axis,slice);

    if(index>=intCurves.size())
    {
        return false;
    }

    auto it = sketches[axis].find(slice);
    if(it==sketches[axis].end())
    {
        sketches[axis][slice] = std::vector<Sketch>();
    }

    if(sketches[axis][slice].size()<=index)
    {
        sketches[axis][slice].resize(index+1);
    }

//    writeXYZ("int-curve-exemplo.xyz",intCurves[index]);
//    writeXYZ("sketch-antes.xyz",sketch);
    alignSketch(intCurves[index],sketch,axis);
    sketches[axis][slice][index] = sketch;


    return true;
}

void SketchSurfaceDeformation::removeSketch(NewVolume::Axis axis, unsigned int slice, unsigned int index)
{
    auto it = sketches[axis].find(slice);
    if(it!=sketches[axis].end())
    {
        if(index<it->second.size())
        {
            if(index==it->second.size()-1)
            {
                it->second.pop_back();

                if(it->second.empty())
                {
                    sketches[axis].erase(it);
                }
            }
            else
            {
                it->second[index] = Sketch();
            }
        }
    }
}

void SketchSurfaceDeformation::setMinimizeDisplacements(bool minimizeDisplacements)
{
    this->minimizeDisplacements = minimizeDisplacements;
}

std::vector<SketchSurfaceDeformation::Sketch>& SketchSurfaceDeformation::getSketches(NewVolume::Axis axis, unsigned int slice)
{
    return sketches[axis][slice];
}

const std::map<unsigned int, std::vector<SketchSurfaceDeformation::Sketch> > &SketchSurfaceDeformation::getAllSketches(NewVolume::Axis axis)
{
    return sketches[axis];
}

std::vector<SketchSurfaceDeformation::InterceptionCurve> SketchSurfaceDeformation::getInterceptionCurves(NewVolume::Axis axis, unsigned int slice)
{
    return slicer->getSliceCurves(axis,slice);
}

void SketchSurfaceDeformation::run()
{
    std::cout << "\n\nSetting up deformation problem..." << std::endl;

    Vector bx, by, bz;

    unsigned int rowCount = 0;

    std::vector<Eigen::Triplet<ScalarType> > triplets;
    addCotLaplacianConstraints(triplets,bx,by,bz,rowCount);

    for(auto& t : triplets)
    {
        auto r = (float) t.row();
        if(isnan(r) || isnan(-r))
        {
            std::cout << "Laplacian row: NAN!!!" << std::endl;
            break;
        }

        auto c = (float) t.col();
        if(isnan(c) || isnan(-c))
        {
            std::cout << "Laplacian col: NAN!!!" << std::endl;
            break;
        }

        auto v = (float) t.value();
        if(isnan(v) || isnan(-v))
        {
            std::cout << "Laplacian val: NAN!!!" << std::endl;
            break;
        }
    }


    addSketchConstraints(triplets,bx,by,bz,rowCount);

    for(auto& t : triplets)
    {
        auto r = (float) t.row();
        if(isnan(r) || isnan(-r))
        {
            std::cout << "Sketch row: NAN!!!" << std::endl;
            break;
        }

        auto c = (float) t.col();
        if(isnan(c) || isnan(-c))
        {
            std::cout << "Sketch col: NAN!!!" << std::endl;
            break;
        }

        auto v = (float) t.value();
        if(isnan(v) || isnan(-v))
        {
            std::cout << "Sketch val: NAN!!!" << std::endl;
            break;
        }
    }



    std::cout << "  Updating matrix A... ";
    Matrix A(rowCount,surface.vertexBuffer.size());
    A.setFromTriplets(triplets.begin(),triplets.end());
    std::cout << "Done." << std::endl;

//    std::cout << "\n\nA:\n" << A;



    auto At = A.transpose();
    auto M = At*A;
    bx = At*bx;
    by = At*by;
    bz = At*bz;

    for (int k=0; k<A.outerSize(); ++k)
      for (Matrix::InnerIterator it(A,k); it; ++it)
      {
        auto v = it.value();
        if(isnan(v) || isnan(-v))
        {
            std::cout << "A: NAN!!! " << it.row() << " " << it.col() << std::endl;
            //break;
        }
      }

//    auto AtEval = At.eval();
//    for (int k=0; k<AtEval.outerSize(); ++k)
//      for (Matrix::InnerIterator it(AtEval,k); it; ++it)
//      {
//        auto v = it.value();
//        if(isnan(v) || isnan(-v))
//        {
//            std::cout << "At: NAN!!!" << std::endl;
//            break;
//        }
//      }

//    for(unsigned int i = 0; i < bx.rows(); ++i)
//    {
//        for(unsigned int j = 0; j < bx.cols(); ++j)
//        {
//            auto v = bx.coeff(i,j);

//            if(isnan(v) || isnan(-v))
//            {
//                std::cout << "bx: NAN!!!" << std::endl;
//                break;
//            }
//        }
//    }

//    for(unsigned int i = 0; i < by.rows(); ++i)
//    {
//        for(unsigned int j = 0; j < by.cols(); ++j)
//        {
//            auto v = by.coeff(i,j);

//            if(isnan(v) || isnan(-v))
//            {
//                std::cout << "by: NAN!!!" << std::endl;
//                break;
//            }
//        }
//    }

//    for(unsigned int i = 0; i < bz.rows(); ++i)
//    {
//        for(unsigned int j = 0; j < bz.cols(); ++j)
//        {
//            auto v = bz.coeff(i,j);

//            if(isnan(v) || isnan(-v))
//            {
//                std::cout << "bz: NAN!!!" << std::endl;
//                break;
//            }
//        }
//    }

//    std::cout << "\n\nbx:\n" << bx;
//    std::cout << "\n\nby:\n" << by;
//    std::cout << "\n\nbz:\n" << bz;

    std::cout << "Optimizing... ";

    auto t0 = std::chrono::high_resolution_clock::now();
    Eigen::SimplicialLDLT<Matrix> solver(M);
    auto t1 = std::chrono::high_resolution_clock::now();

    auto time = std::chrono::duration_cast<std::chrono::seconds>(t1-t0).count();
    std::string units = time<180 ? " seconds" : " minutes";
    std::cout << "Done. [Time spent: " << (time<180 ? time : (time/60.0f)) << units << "]" << std::endl;

    Vector xx, xy, xz;

    std::cout << "  Solving for X... ";
    xx = solver.solve(bx);
    std::cout << "Done." << std::endl;

    std::cout << "  Solving for Y... ";
    xy = solver.solve(by);
    std::cout << "Done." << std::endl;

    std::cout << "  Solving for Z... ";
    xz = solver.solve(bz);
    std::cout << "Done." << std::endl;

//    std::cout << "\n\nxx:\n" << xx;
//    std::cout << "\n\nxy:\n" << xy;
//    std::cout << "\n\nxz:\n" << xz;

    if(solver.info()!=Eigen::Success)
    {
        std::cerr << "Optimization failed!\n";
    }

//    return;

    std::cout << "Updating results... ";
    if(minimizeDisplacements)
    {
        for(int i = 0; i < surface.vertexBuffer.size(); ++i)
        {
            surface.vertexBuffer[i].position =
            {
                surface.vertexBuffer[i].position.x+xx[i],
                surface.vertexBuffer[i].position.y+xy[i],
                surface.vertexBuffer[i].position.z+xz[i]
            };
        }
    }
    else
    {
        for(int i = 0; i < surface.vertexBuffer.size(); ++i)
        {
            surface.vertexBuffer[i].position =
            {
                xx[i], xy[i], xz[i]
            };
        }
    }

    surface.update();
    std::cout << "Done." << std::endl;

    std::cout << "Recomputing interceptions... ";
    slicer->sliceAll();
    std::cout << "Done." << std::endl;

    std::cout << "\nRealigning sketches... ";
    for(int axis = 0; axis < 3; ++axis)
    {
        for(auto& entry : sketches[axis])
        {
            auto slice = entry.first;
            auto& sliceSketches = entry.second;
            const auto& sliceCurves = slicer->getSliceCurves((NewVolume::Axis)axis,slice);

            if(sliceSketches.size()>sliceCurves.size())
            {
                //TODO: write better error message
                std::cerr << "Error!" << std::endl;
            }
            else
            {
                for(unsigned int i = 0; i < sliceSketches.size(); ++i)
                {
                    if(!sliceSketches[i].empty())
                        alignSketch(sliceCurves[i],sliceSketches[i],(NewVolume::Axis)axis);
                }
            }
        }
    }
    std::cout << "\n\nDeformation finished." << std::endl;
}

const SketchSurfaceDeformation::Surface &SketchSurfaceDeformation::getDeformingSurface()
{
    return surface;
}

void SketchSurfaceDeformation::buildMeshGraph()
{
    std::cout << "Building mesh graph... ";

    auto t0 = std::chrono::high_resolution_clock::now();

    struct Edge
    {
        Edge(unsigned int u, unsigned int v)
            : u(u),v(v)
        {
            if(v<u)
                std::swap(this->u,this->v);
        }
        unsigned int u;
        unsigned int v;

        bool operator == (const Edge& other) const
        {
            return (u == other.u && v == other.v) ||
                   (u == other.v && v == other.u);
        }
    };

    struct SimpleHash {
        size_t operator()(const Edge& e) const {
            return e.u ^ e.v;
        }
    };

    std::unordered_set<Edge,SimpleHash> edges;

    auto numEdges = surface.indices.size();
    for(unsigned int i = 0; i < surface.indices.size(); i+=3)
    {
        auto i0 = surface.indices[i+0];
        auto i1 = surface.indices[i+1];
        auto i2 = surface.indices[i+2];
        edges.insert({i0,i1});
        edges.insert({i1,i2});
        edges.insert({i2,i0});
    }

    for(auto edge : edges)
    {
        G.insertEdge(edge.u,edge.v,1,false);
        G.insertEdge(edge.v,edge.u,1,false);
    }

    auto t1 = std::chrono::high_resolution_clock::now();

    auto time = std::chrono::duration_cast<std::chrono::seconds>(t1-t0).count();
    std::string units = time<180 ? " seconds" : " minutes";
    std::cout << "Done. [Time spent: " << (time<180 ? time : (time/60.0f)) << units << "]" <<
                 " [Number of vertices: " << surface.vertexBuffer.size() << " / Number of edges: " << numEdges << "]" << std::endl;
}

void SketchSurfaceDeformation::buildEigenMesh()
{
    auto numVertices = surface.vertexBuffer.size();
    auto numTriangles = surface.indices.size()/3;

    eMesh.V.resize(numVertices,3);
    eMesh.N.resize(numVertices,3);
    eMesh.F.resize(numTriangles,3);

    for(unsigned int i = 0; i < numVertices; ++i)
    {
        eMesh.V.row(i) = Eigen::RowVector3f(
            surface.vertexBuffer[i].position.x,
            surface.vertexBuffer[i].position.y,
            surface.vertexBuffer[i].position.z
        );

        eMesh.N.row(i) = Eigen::RowVector3f(
            surface.vertexBuffer[i].normal.x,
            surface.vertexBuffer[i].normal.y,
            surface.vertexBuffer[i].normal.z
        );
    }

    for(unsigned int i = 0; i < numTriangles; ++i)
    {
        eMesh.F.row(i) = Eigen::RowVector3i(
            surface.indices[3*i + 0],
            surface.indices[3*i + 1],
            surface.indices[3*i + 2]
        );
    }
}

void SketchSurfaceDeformation::alignSketch(const SketchSurfaceDeformation::InterceptionCurve &originalCurve, SketchSurfaceDeformation::Sketch &sketch, NewVolume::Axis axis)
{
    if(originalCurve.size() < 2 || sketch.size() < 2)
    {
        std::cerr << "[SurfaceEditingOperator::alignSketch] Both original "
                     "intersection curve and sketch must have at least two points." << std::endl;
        return;
    }

    auto n = originalCurve.size();
    std::cout << "\n\nAligning sketch with interception curve... [Curve size: " << n << " points / Sketch size: " << sketch.size() << " points]" << std::endl;

    auto parameterize = [](const std::vector<glm::vec3>& curve,
            std::vector<float>& coeffs)
    {
        coeffs.resize(curve.size(),0.0f);

        for(unsigned int i = 1; i < curve.size(); ++i)
        {
            auto d = glm::distance(curve[i-1],curve[i]);
            coeffs[i] = coeffs[i-1] + d;
        }

        for(unsigned int i = 1; i < curve.size(); ++i)
        {
            coeffs[i] /= coeffs.back();
        }
    };

    auto getPoint = [](const std::vector<glm::vec3>& curve,
            const std::vector<float>& coeffs, int& start, float t)
    {
        auto size = curve.size();

        while(t > 1.0f)
            t -= 1.0f;

        int end = start+1;

        while(end<size && t>coeffs[end])
        {
            end++;
        }

        start = end-1;

        auto a = (t-coeffs[start]) / (coeffs[end]-coeffs[start]);
        return glm::mix(curve[start],curve[end],a);
    };

    auto closed = glm::distance(originalCurve[0],originalCurve[originalCurve.size()-1]) < 0.001f;

    if(closed) //If the curves are closed, we have to deal with rotation and orientation problems
    {
        std::cout << "  Closed curve." << std::endl;

        //Close the sketch curve
        sketch.push_back(sketch[0]);

        std::cout << "  Checking orientation... ";
        auto pn = slicer->getSliceNormal(axis);

        glm::vec3 total;
        for(int i = 0; i < originalCurve.size()-1; ++i)
        {
            auto v0 = originalCurve[i];
            auto v1 = originalCurve[i+1];
            auto prod = glm::cross(v0,v1);
            total += prod;
        }

        float areaCurve = glm::dot(total,pn);

        total = glm::vec3();
        for(int i = 0; i < sketch.size()-1; ++i)
        {
            auto v0 = sketch[i];
            auto v1 = sketch[i+1];
            auto prod = glm::cross(v0,v1);
            total += prod;
        }

        float areaSketch = glm::dot(total,pn);

        if((areaCurve > 0 && areaSketch < 0) || (areaCurve < 0 && areaSketch > 0))
        {
            std::reverse(sketch.begin(),sketch.end());
            std::cout << "Curve reversed." << std::endl;
        }
        else
        {
            std::cout << "Same." << std::endl;
        }

        std::cout << "  Rotating and resampling sketch... ";

        auto refAabbMinP = originalCurve[0];
        auto refAabbMaxP = originalCurve[0];
        for(const auto& p : originalCurve)
        {
            refAabbMinP.x = std::min(refAabbMinP.x,p.x);
            refAabbMinP.y = std::min(refAabbMinP.y,p.y);
            refAabbMinP.z = std::min(refAabbMinP.z,p.z);

            refAabbMaxP.x = std::max(refAabbMaxP.x,p.x);
            refAabbMaxP.y = std::max(refAabbMaxP.y,p.y);
            refAabbMaxP.z = std::max(refAabbMaxP.z,p.z);

            std::cout << "p: " << p << std::endl;
        }

        std::cout << "refAabbMinP: " << refAabbMinP << std::endl;
        std::cout << "refAabbMaxP: " << refAabbMaxP << std::endl;

        auto editAabbMinP = sketch[0];
        auto editAabbMaxP = sketch[0];
        for(const auto& p : sketch)
        {
            editAabbMinP.x = std::min(editAabbMinP.x,p.x);
            editAabbMinP.y = std::min(editAabbMinP.y,p.y);
            editAabbMinP.z = std::min(editAabbMinP.z,p.z);

            editAabbMaxP.x = std::max(editAabbMaxP.x,p.x);
            editAabbMaxP.y = std::max(editAabbMaxP.y,p.y);
            editAabbMaxP.z = std::max(editAabbMaxP.z,p.z);
        }

        std::cout << "editAabbMinP: " << editAabbMinP << std::endl;
        std::cout << "editAabbMaxP: " << editAabbMaxP << std::endl;

        auto curveCenter = (refAabbMinP + refAabbMaxP) * 0.5f;
        auto sketchCenter = (editAabbMinP + editAabbMaxP) * 0.5f;

//        float nPoints = n-1.0f;
//        glm::vec3 acc;
//        for(auto i = 0; i < (int)nPoints; ++i)
//        {
//            acc += originalCurve[i];
//            std::cout << i << ": " << originalCurve[i] << std::endl;
//        }

//        std::cout << "acc: " << acc << std::endl;
//        std::cout << "nPoints: " << nPoints << std::endl;

//        glm::vec3 curveCenter(acc.x / nPoints, acc.y / nPoints, acc.z / nPoints);

//        nPoints = sketch.size()-1.0f;
//        acc = glm::vec3(0,0,0);
//        for(auto i = 0; i < (int)nPoints; ++i)
//        {
//            acc += sketch[i];
//        }

//        std::cout << "acc: " << acc << std::endl;
//        std::cout << "nPoints: " << nPoints << std::endl;

//        glm::vec3 sketchCenter(acc.x / nPoints, acc.y / nPoints, acc.z / nPoints);

        std::cout << "curveCenter: " << curveCenter << std::endl;
        std::cout << "sketchCenter: " << sketchCenter << std::endl;

        auto intercepts = [](const glm::vec3& o, const glm::vec3& d,
                const glm::vec3& a, const glm::vec3& b, glm::vec3 pn, float& t)
        {
            auto v1 = o - a;
            auto v2 = b - a;

            auto v1xv2 = glm::cross(v1,v2);

            if(glm::dot(pn,v1xv2)>0)
                pn = -pn;

            auto v3 = glm::cross(pn,d);

            auto dot = glm::dot(v2,v3);

            if(dot == 0)
            {
                //TODO: Treat this case.
            }

            else if(dot > 0)
            {
                auto t1 = glm::length(glm::cross(v2,v1)) / dot;
                auto t2 = glm::dot(v1,v3) / dot;

                if(t2 >=0 && t2 <= 1)
                {
                    t = t1;
                    return true;
                }
            }

            return false;
        };


        auto findInterception = [&](const glm::vec3& o, const glm::vec3&d,
                const std::vector<glm::vec3>& curve, unsigned int& index, float& t)
        {
            float tempT = -1.0f;
            unsigned int tempIdx = 0;
            for(unsigned int i = 0; i < curve.size()-2; ++i)
            {
                auto a = curve[i+0];
                auto b = curve[i+1];
                float curT = -1.0f;

                if(intercepts(o,d,a,b,pn,curT))
                {
                    if(curT>tempT)
                    {
                        tempT = curT;
                        tempIdx = i;
                    }
                }
            }

            if(tempT>-1.0f)
            {
                t = tempT;
                index = tempIdx;
                return true;
            }

            return false;
        };

        for(unsigned int i =0; i < originalCurve.size(); ++i)
        {
            auto d = glm::normalize(originalCurve[i]-curveCenter);

            unsigned int i0,i1;
            float t0, t1;

            if(findInterception(curveCenter,d,originalCurve,i0,t0) &&
               findInterception(sketchCenter,d,sketch,i1,t1))
            {
                std::vector<float> curveCoeffs,sketchCoeffs;
                parameterize(originalCurve,curveCoeffs);
                parameterize(sketch,sketchCoeffs);

                auto p0 = curveCenter + d*t0;
                auto p1 = sketchCenter + d*t1;

                auto prop0 = glm::distance(p0,originalCurve[i0])/
                        glm::distance(originalCurve[i0],originalCurve[i0+1]);
                auto prop1 = glm::distance(p1,sketch[i1])/
                        glm::distance(sketch[i1],sketch[i1+1]);

                auto newT0 = curveCoeffs[i0]+prop0*(curveCoeffs[i0+1]-curveCoeffs[i0]);
                auto newT1 = sketchCoeffs[i1]+prop1*(sketchCoeffs[i1+1]-sketchCoeffs[i1]);

                std::vector<glm::vec3> newSketch;
                newSketch.reserve(n);

                auto diff = newT1-newT0;
                if(diff<0)
                    diff += 1.0f;

                for(unsigned int j = 0; j < n; ++j)
                {
                    auto t = curveCoeffs[j]+diff;
                    int start = 0;
                    newSketch.push_back(getPoint(sketch,sketchCoeffs,start,t));
                }

                sketch.swap(newSketch);
                std::cout << "Done." << std::endl;

                break;
            }
        }
    }

    else
    {
        std::cout << "  Open curve." << std::endl;

        //This solution does not cover all cases:
        if( glm::distance(originalCurve[0],sketch[0]) >
            glm::distance(originalCurve[0],sketch[sketch.size()-1]) )
                std::reverse(sketch.begin(),sketch.end());

        std::cout << "  Resampling... ";
        std::vector<float> curveCoeffs,sketchCoeffs;
        parameterize(originalCurve,curveCoeffs);
        parameterize(sketch,sketchCoeffs);

        std::vector<glm::vec3> newSketch;
        newSketch.reserve(n);

        int start = 0;
        for(unsigned int j = 0; j < n; ++j)
        {
            newSketch.push_back(getPoint(sketch,sketchCoeffs,start,curveCoeffs[j]));
        }

        sketch.swap(newSketch);
        std::cout << "Done." << std::endl;
    }

    std::cout << "Sketch aligned. [Curve size: " << originalCurve.size() << " points / Sketch size: " << sketch.size() << " points]" << std::endl;

}

void SketchSurfaceDeformation::addLaplacianConstraints(std::vector<T> &Atriplets,
        Vector &bx, Vector &by, Vector &bz, unsigned int& rowCount)
{
    auto& vertices = surface.vertexBuffer;
    auto n = vertices.size();

    bx.conservativeResize(n+rowCount);
    by.conservativeResize(n+rowCount);
    bz.conservativeResize(n+rowCount);

    auto numEdges = G.getNumEdges();
    Atriplets.reserve(Atriplets.size()+numEdges+n);

    std::cout << "  Computing Laplacian... ";
    auto t0 = std::chrono::high_resolution_clock::now();

    if(minimizeDisplacements)
    {
        for(unsigned int u = 0; u < n; ++u)
        {
            auto& neighbors = G.getNeighbors(u);
            auto Nu = neighbors.size();
//            glm::vec3 laplacian;

            for(const auto& v : neighbors)
            {
//                A.insert(eqCount+u,v.v) = -1.f/Nu;
                Atriplets.push_back(T(rowCount+u,v.v,-1.f/Nu));
//                laplacian -= vertices[v.v].position/glm::vec3(Nu,Nu,Nu);
            }

//            A.insert(eqCount+u,u) = 1.0f;
            Atriplets.push_back(T(rowCount+u,u,(float)1.0f));

//            laplacian += glm::vec3(vertices[u].position.x/**Nu*/,
//                          vertices[u].position.y/**Nu*/,
//                          vertices[u].position.z/**Nu*/);

            bx[rowCount+u] = 0;//laplacian.x; //or should it be zero?
            by[rowCount+u] = 0;//laplacian.y;
            bz[rowCount+u] = 0;//laplacian.z;
        }
    }

    else
    {
        for(unsigned int u = 0; u < n; ++u)
        {
            auto& neighbors = G.getNeighbors(u);
            auto Nu = neighbors.size();
            glm::vec3 laplacian;

            for(const auto& v : neighbors)
            {
//                A.insert(eqCount+u,v.v) = -1.f/Nu;
                Atriplets.push_back(T(rowCount+u,v.v,-1.f));
                laplacian -= vertices[v.v].position/*/glm::vec3(Nu,Nu,Nu)*/;
            }

//            A.insert(eqCount+u,u) = 1.0f;
            Atriplets.push_back(T(rowCount+u,u,Nu));

            laplacian += glm::vec3(vertices[u].position.x*Nu,
                          vertices[u].position.y*Nu,
                          vertices[u].position.z*Nu);

            bx[rowCount+u] = laplacian.x;
            by[rowCount+u] = laplacian.y;
            bz[rowCount+u] = laplacian.z;
        }
    }

    rowCount += n;

    auto t1 = std::chrono::high_resolution_clock::now();

    auto time = std::chrono::duration_cast<std::chrono::seconds>(t1-t0).count();
    auto units = time<180 ? " seconds" : " minutes";
    std::cout << "Done. [Time spent: " << (time<180 ? time : (time/60.0f)) << units << "]" << std::endl;
}

void SketchSurfaceDeformation::addCotLaplacianConstraints(std::vector<SketchSurfaceDeformation::T> &Atriplets, SketchSurfaceDeformation::Vector &bx, SketchSurfaceDeformation::Vector &by, SketchSurfaceDeformation::Vector &bz, unsigned int &rowCount)
{
    Eigen::Matrix<int,Eigen::Dynamic,2> edges;
    edges.resize(3,2);
    edges <<
        1,2,
        2,0,
        0,1;

    // Gather cotangents
    Eigen::Matrix<ScalarType,Eigen::Dynamic,Eigen::Dynamic> C;
    igl::cotmatrix_entries(eMesh.V,eMesh.F,C);

    Atriplets.reserve(eMesh.F.rows()*edges.rows()*4);
    // Loop over triangles
    for(int i = 0; i < eMesh.F.rows(); i++)
    {
      // loop over edges of element
      for(int e = 0;e<edges.rows();e++)
      {
        int source = eMesh.F(i,edges(e,0));
        int dest = eMesh.F(i,edges(e,1));

//        assert(!isnan(C(i,e)));
//        std::cout << C(i,e) << " ";

        //Signs are changed: suellen
        Atriplets.push_back(T(rowCount+source,   dest, -C(i,e)));
        Atriplets.push_back(T(rowCount+dest,   source, -C(i,e)));
        Atriplets.push_back(T(rowCount+source, source,  C(i,e)));
        Atriplets.push_back(T(rowCount+dest,     dest,  C(i,e)));
      }
    }

    auto& vertices = surface.vertexBuffer;
    auto n = vertices.size();

    bx.conservativeResize(n+rowCount);
    by.conservativeResize(n+rowCount);
    bz.conservativeResize(n+rowCount);

    if(minimizeDisplacements)
    {

        for(unsigned int u = 0; u < n; ++u)
        {
            bx[rowCount+u] = 0;
            by[rowCount+u] = 0;
            bz[rowCount+u] = 0;
        }
    }
    else
    {
//        Vector bxx, bxy, bxz;
//        Vector xx, xy, xz;
//        xx.conservativeResize(n);
//        xy.conservativeResize(n);
//        xz.conservativeResize(n);
    }

    rowCount += n;
}

void SketchSurfaceDeformation::addSketchConstraints(std::vector<Eigen::Triplet<ScalarType> > &Atriplets,
                                                    Vector &bx, Vector &by, Vector &bz, unsigned int &rowCount)
{
    std::cout << "  Adding sketch restrictions... ";

    auto& vertices = surface.vertexBuffer;
    auto n = vertices.size();

    struct Equation
    {
        float alpha;
        unsigned int v0;
        unsigned int v1;
        glm::vec3 value;
    };

    std::vector< Equation > eqs;

    for(int axis = 0; axis < 3; ++axis)
    {
        for(auto& entry : sketches[axis])
        {
            auto slice = entry.first;
            auto& sliceSketches = entry.second;
            auto& sliceEdges = slicer->getSliceCrossingEdges((NewVolume::Axis)axis,slice);

            for(unsigned int i = 0; i < sliceSketches.size(); ++i)
            {
                auto& sketch = sliceSketches[i];
                auto& edges = sliceEdges[i];

                if(!sketch.empty() && sketch.size()==edges.size())
                {
                    for(unsigned int j = 0; j < sketch.size(); ++j)
                    {
                        auto edge = edges[j];

                        auto uPos = vertices[edge.u].position;
                        auto vPos = vertices[edge.v].position;
                        auto sPos = edge.p;

                        auto alpha = glm::distance(uPos,vPos) > 0 ?
                                    glm::distance(uPos,sPos) / glm::distance(uPos,vPos) : 0.0f;

                        glm::vec3 value;
                        if(minimizeDisplacements)
                            value = sketch[j]-edge.p;
                        else
                            value = sketch[j];

                        eqs.push_back({alpha,edge.u,edge.v,value});
                    }
                }
            }
        }
    }

    std::cout << "Done." << std::endl;

    Atriplets.reserve(Atriplets.size()+eqs.size()*2);

    bx.conservativeResize(rowCount+eqs.size());
    by.conservativeResize(rowCount+eqs.size());
    bz.conservativeResize(rowCount+eqs.size());

    for(unsigned int i = 0; i < eqs.size(); ++i)
    {
        auto& eq = eqs[i];

        assert(!isnan(eq.alpha));
        assert(!isnan(eq.value.x));
        assert(!isnan(eq.value.y));
        assert(!isnan(eq.value.z));

        assert(!isnan(-eq.alpha));
        assert(!isnan(-eq.value.x));
        assert(!isnan(-eq.value.y));
        assert(!isnan(-eq.value.z));

        Atriplets.push_back(T(rowCount+i,eq.v0,eq.alpha));
        Atriplets.push_back(T(rowCount+i,eq.v1,1.0f-eq.alpha));

        bx[rowCount+i] = eq.value.x;
        by[rowCount+i] = eq.value.y;
        bz[rowCount+i] = eq.value.z;
    }

    rowCount += eqs.size();


//    std::cout << std::fixed << std::setprecision(5) << "\n\nA = \n" << A;

}

