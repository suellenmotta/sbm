#include "surfacemodelingdockwidget.h"
#include "ui_surfacemodelingdockwidget.h"
#include "canvas3dwidget.h"
#include "mainwindow.h"
#include "volumealgorithms.h"

#include <QPair>
#include <QDebug>

#include <fstream>
#include <algorithm>

typedef QPair<int,int> BorderIDType;
Q_DECLARE_METATYPE(BorderIDType)

typedef QPair<NewVolume::Axis,unsigned int> SketchIDType;
Q_DECLARE_METATYPE(SketchIDType)

SurfaceModelingDockWidget::SurfaceModelingDockWidget(QWidget *parent, Canvas3DWidget *canvas)
    : QDockWidget(parent)
    , canvas(canvas)
    , ui(new Ui::SurfaceModelingDockWidget)
{
    ui->setupUi(this);
}

void SurfaceModelingDockWidget::setBorderOperator(std::shared_ptr<BorderSelectionOperator> borderOperator)
{
    if(this->borderOperator)
    {
        disconnect(this->borderOperator.get(),SIGNAL(borderSelectionChanged()),
                   this,SLOT(onBorderSelectionChanged()));
        disconnect(canvas,SIGNAL(viewChanged()),
                   this->borderOperator.get(),SLOT(onViewChanged()));
    }

    this->borderOperator = borderOperator;

    connect(this->borderOperator.get(),SIGNAL(borderSelectionChanged()),
            this,SLOT(onBorderSelectionChanged()));

    connect(canvas,SIGNAL(viewChanged()),
            this->borderOperator.get(),SLOT(onViewChanged()));
}

void SurfaceModelingDockWidget::setSliceSketchingOperator(std::shared_ptr<SliceSketchingOperator> sketchingOperator)
{
    if(this->sketchingOperator)
    {
        disconnect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
                   this,SLOT(onSketchFinished()));
    }

    this->sketchingOperator = sketchingOperator;

    connect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
            this,SLOT(onSketchFinished()));
}

SurfaceModelingDockWidget::~SurfaceModelingDockWidget()
{
    delete ui;
}

void SurfaceModelingDockWidget::onBorderSelectionChanged()
{
    fillSelectedBordersList();
}

void SurfaceModelingDockWidget::onSketchFinished()
{
    fillSketchesList();
}

void SurfaceModelingDockWidget::on_addBorderButton_toggled(bool checked)
{
    if(checked)
    {
        if(borderOperator)
        {
            ui->addSketchButton->setChecked(false);
            canvas->setOperator(std::static_pointer_cast<Operator>(borderOperator));

            canvas->update();
        }
    }
    else
    {
        canvas->setOperator(nullptr);
        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_deleteBorderButton_clicked()
{
    if(borderOperator)
    {
        auto selected = ui->bordersListWidget->selectedItems();

        for(auto item : selected)
        {
            auto border = item->data(Qt::UserRole).value<BorderIDType>();

            borderOperator->unselectBorder(border.first,border.second);
        }

        qDeleteAll(selected);
        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_addSketchButton_toggled(bool checked)
{
    if(checked)
    {
        if(sketchingOperator)
        {
            ui->addBorderButton->setChecked(false);
            canvas->setOperator(std::static_pointer_cast<Operator>(sketchingOperator));
            canvas->update();
        }
    }
    else
    {
        canvas->setOperator(nullptr);
        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_deleteSketchButton_clicked()
{
    if(sketchingOperator)
    {
        auto selected = ui->sketchesListWidget->selectedItems();

        for(auto item : selected)
        {
            auto sketch = item->data(Qt::UserRole).value<SketchIDType>();

            sketchingOperator->deleteSketch(sketch.first,sketch.second);
        }

        qDeleteAll(selected);
        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_showAllSketches_toggled(bool checked)
{
    if(sketchingOperator)
    {
        if(checked)
            sketchingOperator->showAllSketches();
        else
            sketchingOperator->showOnlyVisible();

        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_sketchesListWidget_itemSelectionChanged()
{
}

void SurfaceModelingDockWidget::on_pointCloudButton_clicked(bool checked)
{
    if(borderOperator)
    {
        if(checked)
            borderOperator->showPointCloud();
        else
            borderOperator->showLineLoops();

        canvas->update();

    }
    if(sketchingOperator)
    {
        if(checked)
            sketchingOperator->showPointCloud();
        else
            sketchingOperator->showLines();

        canvas->update();
    }
}

void SurfaceModelingDockWidget::on_viewModeButton_clicked(bool checked)
{

}

void SurfaceModelingDockWidget::on_resetButton_clicked()
{
    ui->addBorderButton->setChecked(false);
    ui->addSketchButton->setChecked(false);

    borderOperator->unselectBorders();
    sketchingOperator->deleteSketches();

    ui->bordersListWidget->clear();
    ui->sketchesListWidget->clear();

    canvas->update();
}

void SurfaceModelingDockWidget::on_generateButton_clicked()
{
    std::vector< glm::vec3 > pointCloud;

    if(borderOperator)
    {
        pointCloud = borderOperator->getSelectedBordersPointCloud();
    }
    if(sketchingOperator)
    {
        auto sketchPoints = sketchingOperator->getSketchesPointCloud();
        pointCloud.insert(pointCloud.end(),sketchPoints.begin(),sketchPoints.end());
    }

    auto inputVolume = canvas->getVolume();
    auto indexes = pointCoordsToVolumeIndexes(inputVolume,pointCloud);

    std::ofstream output;
    output.open("pointcloud.txt",std::ios::out);

    output << pointCloud.size() << "\n";
    for(unsigned int i = 0; i < pointCloud.size(); ++i)
    {
        output << pointCloud[i].x << " " << pointCloud[i].y << " " << pointCloud[i].z << " ";
        output << indexes[i].x << " " << indexes[i].y << " " << indexes[i].z << "\n";
    }

    output.close();

    auto info = inputVolume->getInfo();
    auto size = info.numXSlices*info.numYSlices*info.numZSlices;

    auto binaryVolume = inputVolume->getCopy();
    float* buffer = binaryVolume->getBuffer();
    std::fill(buffer, buffer+size, 0.0f);

    for(auto pointCoord : indexes)
    {
        auto idx = inputVolume->getIndex(pointCoord.x,pointCoord.y,pointCoord.z);
        buffer[idx] = 1.0f;
    }

    auto distanceImage = distanceTransform(binaryVolume);
    delete binaryVolume;

    auto& renderer = canvas->getScene().getVolumeRenderer();
    renderer.setVolume(distanceImage);

    buffer = distanceImage->getBuffer();
    auto min = *(std::min_element(buffer,buffer+size));
    auto max = *(std::max_element(buffer,buffer+size));
    renderer.setMinPixelValue(min);
    renderer.setMaxPixelValue(max);

    canvas->update();
}

void SurfaceModelingDockWidget::on_SurfaceModelingDockWidget_visibilityChanged(bool visible)
{
    if(!visible)
    {
        ui->bordersListWidget->clear();
        ui->sketchesListWidget->clear();

        ui->addBorderButton->setChecked(false);
        ui->addSketchButton->setChecked(false);
    }
}

void SurfaceModelingDockWidget::fillSelectedBordersList()
{
    if(borderOperator)
    {
        ui->bordersListWidget->clear();

        auto selectedData = borderOperator->getSelectedBorders();
        auto selectedNames = borderOperator->getSelectedBorderNames();

        for(unsigned int i = 0; i < selectedData.size(); ++i)
        {
            BorderIDType border(selectedData[i].first,selectedData[i].second);

            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(selectedNames[i]));
            item->setData(Qt::UserRole, QVariant::fromValue(border));

            ui->bordersListWidget->addItem(item);
        }
    }
}

void SurfaceModelingDockWidget::fillSketchesList()
{
    if(sketchingOperator)
    {
        ui->sketchesListWidget->clear();

        auto sketchedSlices = sketchingOperator->getSketchedSlices();
        auto sketchNames = sketchingOperator->getSketchNames();

        for(unsigned int i = 0; i < sketchedSlices.size(); ++i)
        {
            SketchIDType sketch(sketchedSlices[i].first,sketchedSlices[i].second);

            QListWidgetItem* item = new QListWidgetItem();
            item->setText(QString::fromStdString(sketchNames[i]));
            item->setData(Qt::UserRole, QVariant::fromValue(sketch));

            ui->sketchesListWidget->addItem(item);
        }
    }
}


