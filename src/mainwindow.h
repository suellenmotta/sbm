#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>

#include "surfacemodelingdockwidget.h"
#include "sketchbasedsurfacedeformationwidget.h"
#include "SketchBasedModelingWidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void enableInlineSliceOptions(bool enable=true);
    void enableCrosslineSliceOptions(bool enable=true);
    void enableDepthSliceOptions(bool enable=true);

    void addSurface(const std::string& name);

private slots:
    void on_actionLoadVolume_triggered();

    void on_actionLoadModel_triggered();

    void on_actionSeismicScene_triggered();

    void on_actionModelTestScene_triggered();

    void on_actionBabyGEScene_triggered();

    void on_actionWireframe_toggled(bool wireframeOn);

    void on_actionSnapshot_triggered();

    void on_actionSelectRegion_toggled(bool operatorOn);

    void on_actionExit_triggered();

    void on_showInlineCheckbox_clicked(bool checked);
    void on_showCrosslineCheckbox_clicked(bool checked);
    void on_showDepthSliceCheckbox_clicked(bool checked);
    void on_inlineSpinBox_valueChanged(int value);
    void on_crosslineSpinBox_valueChanged(int value);
    void on_depthSpinBox_valueChanged(int value);
    void on_inlineStepSpinBox_valueChanged(int value);
    void on_crosslineStepSpinBox_valueChanged(int value);
    void on_depthStepSpinBox_valueChanged(int value);
    void on_horizonsListWidget_itemChanged(QListWidgetItem *item);

    void on_actionBackgroundColor_triggered();
    
    void on_actionBorderPoints_toggled(bool viewBorderPoints);


    void on_actionDeformSurface_toggled(bool deform);

    void on_actionResetCamera_triggered();

    void on_actionSaveModel_triggered();

    void on_actionANPScene_triggered();

private:
    Ui::MainWindow *ui;

    SurfaceModelingDockWidget* surfaceModelingWidget;

    SketchBasedModelingWidget* modelingWidget;


    void setupSpinBoxes();
};

#endif // MAINWINDOW_H
