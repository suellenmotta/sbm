#ifndef VOLUMEALGORITHMS_H
#define VOLUMEALGORITHMS_H

#include "newvolume.h"



std::vector<glm::uvec3> pointCoordsToVolumeIndexes(const NewVolume* volume, const std::vector<glm::vec3>& points);


/**
 * @brief Computes the unsigned euclidian distance transform over the input volume.
 * @param inputVolume Zero or negative values in the input volume will be considered as the non-object region.
 * @return The distance field volume image
 */
NewVolume* distanceTransform(NewVolume* inputVolume);


#endif // VOLUMEALGORITHMS_H
