#-------------------------------------------------
#
# Project created by QtCreator 2017-07-17T17:37:21
#
#-------------------------------------------------

QT       += core opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sbm
TEMPLATE = app
CONFIG += c++14
CONFIG += console

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += _USE_MATH_DEFINES

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    canvas3dwidget.cpp \
    camera.cpp \
    roiselectionoperator.cpp \
    mesh.cpp \
    graph.cpp \
    meshalgorithms.cpp \
    volumerenderer.cpp \
    scene.cpp \
    newvolume.cpp \
    seismicvolume.cpp \
    surfacereader.cpp \
    boundaryhighlightoperator.cpp \
    borderselectionoperator.cpp \
    surfacemodelingdockwidget.cpp \
    slicesketchingoperator.cpp \
    raycasting.cpp \
    volumealgorithms.cpp \
    surfaceeditingoperator.cpp \
    harmonicsurfacedeformation.cpp \
    surfacedeformation.cpp \
    surfaceslicing.cpp \
    sketchbasedsurfacedeformationwidget.cpp \
    sketchsurfacedeformation.cpp \
    sketchsurfacegenerator.cpp \
    SurfaceCreationWidget.cpp \
    SketchBasedModelingWidget.cpp \
    CurveInterpolationWidget.cpp

HEADERS += \
        mainwindow.h \
    canvas3dwidget.h \
    image.h \
    camera.h \
    operator.h \
    raycasting.h \
    roiselectionoperator.h \
    scene.h \
    mesh.h \
    graph.h \
    meshalgorithms.h \
    volumerenderer.h \
    newvolume.h \
    seismicvolume.h \
    surfacereader.h \
    colorslist.h \
    boundaryhighlightoperator.h \
    borderselectionoperator.h \
    surfacemodelingdockwidget.h \
    slicesketchingoperator.h \
    volumealgorithms.h \
    surfaceeditingoperator.h \
    harmonicsurfacedeformation.h \
    surfacedeformation.h \
    surfaceslicing.h \
    sketchbasedsurfacedeformationwidget.h \
    sketchsurfacedeformation.h \
    sketchsurfacegenerator.h \
    SurfaceCreationWidget.h \
    SketchBasedModelingWidget.h \
    CurveInterpolationWidget.h

FORMS += \
        mainwindow.ui \
    surfacemodelingdockwidget.ui \
    sketchbasedsurfacedeformationwidget.ui \
    SketchBasedModelingWidget.ui \
    CurveInterpolationWidget.ui

RESOURCES += \
    data.qrc \

# Dependencies

win32 {
    INCLUDEPATH += $$PWD/../dependencies/win32/include
    INCLUDEPATH += $$PWD/../dependencies/win32/include/libigl
    DEPENDPATH += $$PWD/../dependencies/win32/include

    CONFIG( release, debug|release ) {
        LIBS += -L$$PWD/../dependencies/win32/lib/openmesh/ -lOpenMeshCore
        LIBS += -L$$PWD/../dependencies/win32/lib/artme/release/ -lARTMe
    }
    CONFIG( debug, debug|release ) {
        LIBS += -L$$PWD/../dependencies/win32/lib/openmesh/ -lOpenMeshCored
        LIBS += -L$$PWD/../dependencies/win32/lib/artme/debug/ -lARTMe
    }

    LIBS += opengl32.lib
}


linux {
    INCLUDEPATH += $$PWD/../dependencies/linux/include
    DEPENDPATH += $$PWD/../dependencies/linux/include

    LIBS += -L$$PWD/../dependencies/linux/lib/

    LIBS += -lOpenMeshCore -lartme
}
