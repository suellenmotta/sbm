
#define GLM_ENABLE_EXPERIMENTAL
#include "meshalgorithms.h"

#include <algorithm>
#include <iostream>
#include <cmath>
#include <limits>
#include <unordered_map>
#include <glm/gtx/hash.hpp>

float distancePointSegment(const glm::vec2& point,
                           const glm::vec2& lPoint1,
                           const glm::vec2& lPoint2)
{
    auto l2 = glm::distance2(lPoint1, lPoint2);

    if (l2 <= 0.000001f) return glm::distance2(point, lPoint1);

    auto t = ((point.x - lPoint1.x) * (lPoint2.x - lPoint1.x) + (point.y - lPoint1.y) * (lPoint2.y - lPoint1.y)) / l2;
    t = glm::max(0.0f, glm::min(1.0f, t));

    glm::vec2 p(lPoint1.x + t * (lPoint2.x - lPoint1.x),lPoint1.y + t * (lPoint2.y - lPoint1.y));
    return glm::distance2(point,p);
}


float distancePointSegments(const glm::vec2& point,
                            const std::vector< glm::vec2 >& segments)
{
    float minDistance = 0.0f;

    if( !segments.empty() )
    {
        if(segments.size() < 2)
            minDistance = glm::distance2(point,segments[0]);
        else
        {
            minDistance = distancePointSegment(point,segments[0],segments[1]);
            for(unsigned int i = 1; i < segments.size()-1; ++i)
            {
                auto distance = distancePointSegment(point,segments[i],segments[i+1]);
                minDistance = glm::min(distance,minDistance);
            }
        }
    }

    return minDistance;
}


void findClosestEdgePath(const opengl::UntexturedMesh& mesh,
                         const std::vector< glm::vec2 >& curve, const glm::mat4& model,
                         const glm::mat4& view, const glm::mat4& proj, const glm::vec4 viewport,
                         std::vector< unsigned int >& edgePath,
                         std::vector< glm::vec3 >& edgePathScreenPoints,
                         const std::set< unsigned int >& triangles )
{
    if(curve.empty()) return;

    if(triangles.empty())
    {
        //TODO: Preencher triangulos interceptados pela curva
    }

    std::set<unsigned int> vertices;
    for(const auto& triangle : triangles)
    {
        vertices.insert(mesh.indices[triangle*3+0]);
        vertices.insert(mesh.indices[triangle*3+1]);
        vertices.insert(mesh.indices[triangle*3+2]);
    }

    std::map< int, int > newIdxs;
    std::vector< int > oldIdxs;
    std::vector< glm::vec3 > screenPos;

    oldIdxs.reserve(vertices.size());
    screenPos.reserve(vertices.size());

    auto mv = view*model;
    for(const auto& vertex : vertices)
    {
        screenPos.push_back( glm::project(mesh.vertexBuffer[vertex].position,
                             mv, proj, viewport) );
        newIdxs[vertex] = oldIdxs.size();
        oldIdxs.push_back(vertex);
    }

    Graph G(vertices.size());
    for(const auto& triangle : triangles)
    {
        int id0 = newIdxs[mesh.indices[triangle*3+0]];
        int id1 = newIdxs[mesh.indices[triangle*3+1]];
        int id2 = newIdxs[mesh.indices[triangle*3+2]];

        float w0 = distancePointSegments(glm::vec2(screenPos[id0]),curve);
        float w1 = distancePointSegments(glm::vec2(screenPos[id1]),curve);
        float w2 = distancePointSegments(glm::vec2(screenPos[id2]),curve);

        G.insertEdge( id0, id1, w0+w1 );
        G.insertEdge( id1, id2, w1+w2 );
        G.insertEdge( id2, id0, w2+w0 );
    }

    int start=0,end=0;
    float distance = glm::distance2(curve[0],glm::vec2(screenPos[0]));
    for(unsigned int i = 1; i < screenPos.size(); ++i)
    {
        auto d = glm::distance2(curve[0],glm::vec2(screenPos[i]));
        if(d < distance)
        {
            distance = d;
            start = i;
        }
    }

    unsigned int last = curve.size()-1;
    distance = glm::distance2(curve[last],glm::vec2(screenPos[0]));
    for(unsigned int i = 1; i < screenPos.size(); ++i)
    {
        auto d = glm::distance2(curve[last],glm::vec2(screenPos[i]));
        if(d < distance)
        {
            distance = d;
            end = i;
        }
    }

    G.dijkstra(start);

    std::vector<unsigned int> invertedPath;
    std::vector< glm::vec3 > invertedPoints;
    int node = end;
    while(node != -1)
    {
        invertedPath.push_back(oldIdxs[node]);
        invertedPoints.push_back(screenPos[node]);
        node = G.parent[node];
    }
//    for( const auto& idx : invertedPath )
//    {
//        std::cout << idx << " ";
//    }

    edgePath.insert(edgePath.end(),invertedPath.rbegin(),invertedPath.rend());
    edgePathScreenPoints.insert(edgePathScreenPoints.end(),invertedPoints.rbegin(),invertedPoints.rend());
}


void trianglesInsidePolygon(const CornerTable& cornerTable,
                            std::vector< unsigned int >& polygon,
                            std::set< unsigned int >& triangles)
{
    enum
    {
        VISITED,
        FINISHED,
        FIRSTLAYER
    };

    std::map<unsigned int, int> visited;
    std::queue<int> queue;

    for(unsigned int i = 0; i < polygon.size()-1; ++i)
    {
        auto vertex = polygon[i];
        auto next = polygon[i+1];
        auto cornerStart = cornerTable.vertexToCornerIndex(vertex);
        auto corner = cornerStart;
        do
        {
            if(cornerTable.cornerToVertexIndex(
                cornerTable.cornerNext(corner)) == next )
            {
                break;
            }
            corner = cornerTable.cornerSwing(corner);
        } while(corner!=cornerStart);

        auto triangle = cornerTable.cornerTriangle(corner);
        triangles.insert(triangle);
//        auto t1 = cornerTable.cornerTriangle(cornerTable.cornerLeft(corner));
        //TODO: Handle the cases that have 1, 2 or 3 points inside the border
        auto inside = cornerTable.cornerToVertexIndex(
            cornerTable.cornerPrevious(corner));

        visited[vertex] = FINISHED;

        if(visited.find(inside)==visited.end())
        {
            visited[inside] = FIRSTLAYER;
            queue.push(inside);
        }
    }

    while(!queue.empty())
    {
        auto vertex = queue.front();
        queue.pop();

        if(visited[vertex]==FINISHED) continue;

        auto cornerStart = cornerTable.vertexToCornerIndex(vertex);
        auto corner = cornerStart;
        do
        {
            auto v = cornerTable.cornerToVertexIndex(cornerTable.cornerNext(corner));

            if(visited[cornerTable.cornerToVertexIndex(corner)]==FIRSTLAYER)
            {
                std::cerr <<
                     "Vertex in first layer"
                  << std::endl;
                // Find corner that next isn't in polygon
                // TODO: Handle the cases that have 1, 2 or 3 points inside the
                // border
                do
                {
                    // check if vertex of next is in polygon
                    auto inPolygon(std::find(polygon.begin(),polygon.end(),v));
                    if(inPolygon==polygon.end())
                    {
                        std::cerr <<
                           "Find vertex not in ppolygon"
                        << std::endl;
                        break;
                    }
                    corner = cornerTable.cornerSwing(corner);
                    v = cornerTable.cornerToVertexIndex(cornerTable.cornerNext(corner));
                } while(corner!=cornerStart);
            }
            else
            {
              std::cerr <<
                     "Vertex NOT in first layer"
                  << std::endl;
            }

            if(visited.find(v)==visited.end())
            {
                visited[v]=VISITED;
                queue.push(v);
            }

            auto triangle = cornerTable.cornerTriangle(corner);
            std::cerr << "Inserindo vertice: " <<triangle << "     de:" << cornerTable.getNumTriangles() << std::endl;
            triangles.insert(triangle);

            if(corner!=cornerStart)
                corner = cornerTable.cornerSwing(corner);
        } while(corner!=cornerStart);

        visited[vertex] = FINISHED;
    }
    std::cerr <<
                     "ACHEI OS TRIANGULOS INTERIORES AO POLIGONO"
                  << std::endl;
}


void bordersInsidePolygon(const CornerTable& cornerTable,
                            std::vector< unsigned int >& polygon,
                            std::vector< std::vector< unsigned int > >& borders)
{
    enum
    {
        VISITED,
        FINISHED,
        FIRSTLAYER
    };

    std::map<unsigned int, int> visited;
    std::queue<int> queue;
    // Put the vertexes of internal border triangles as inside points
    for(unsigned int i = 0; i < polygon.size()-1; ++i)
    {
        auto vertex = polygon[i];
        auto next = polygon[i+1];
        auto cornerStart = cornerTable.vertexToCornerIndex(vertex);
        auto corner = cornerStart;
        // Find triangle that contains the vertex and next as edge
        do
        {
            if(cornerTable.cornerToVertexIndex(
                cornerTable.cornerNext(corner)) == next )
            {
                break;
            }
            corner = cornerTable.cornerSwing(corner);
        } while(corner!=cornerStart);

        //TODO: Handle the cases that have 1, 2 or 3 points inside the border
        auto inside = cornerTable.cornerToVertexIndex(
            cornerTable.cornerPrevious(corner));

        visited[vertex] = FINISHED;

        if(visited.find(inside)==visited.end())
        {
            visited[inside] = FIRSTLAYER;
            queue.push(inside);
        }
    }

    while(!queue.empty())
    {
        auto vertex = queue.front();
        queue.pop();

        if(visited[vertex]==FINISHED) continue;

        auto cornerStart = cornerTable.vertexToCornerIndex(vertex);
        auto corner = cornerStart;
        do
        {
            // In the case of we get a corner one vertex of layer first layer
            // inside the border we can get a corner that the vertex of next
            // corner is in polygon
            auto v = cornerTable.cornerToVertexIndex(cornerTable.cornerNext(corner));
            if(visited[cornerTable.cornerToVertexIndex(corner)]==FIRSTLAYER)
            {
                // Find corner that next isn't in polygon
                // TODO: Handle the cases that have 1, 2 or 3 points inside the
                // border
                do
                {
                    // check if vertex of next is in polygon
                    auto inPolygon(std::find(polygon.begin(),polygon.end(),v));
                    if(inPolygon==polygon.end())
                    {
                        break;
                    }
                    corner = cornerTable.cornerSwing(corner);
                    v = cornerTable.cornerToVertexIndex(cornerTable.cornerNext(corner));
                } while(corner!=cornerStart);
            }

            std::vector<unsigned int> border;
            // Handle cases that hole are in border's 1-star, that can happen
            // in first layer of insider points
            if(cornerTable.cornerOpposite(corner) == CornerTable::BORDER_CORNER
               || (visited[cornerTable.cornerToVertexIndex(corner)]==FIRSTLAYER
               && cornerTable.cornerLeft(corner) == CornerTable::BORDER_CORNER) ) //found a border
            {
                auto c = cornerTable.cornerNext(corner);
                auto vStart = cornerTable.cornerToVertexIndex(c);

                if( visited[cornerTable.cornerToVertexIndex(corner)]==FIRSTLAYER && cornerTable.cornerLeft(corner) == CornerTable::BORDER_CORNER)
                {
                  c = corner;
                  vStart = vertex;
                }


                int limit = 0;

                if(visited.find(vStart)==visited.end())
                {
                    auto cornerVertex = vStart;

                    do
                    {
                        border.push_back(cornerVertex);
                        if(visited.find(cornerVertex)==visited.end())
                        {
                            visited[cornerVertex]=VISITED;
                            queue.push(cornerVertex);
                        }
                        // This cornertable implementation have some
                        // optimizations tricks, like when you call
                        // @vertexToCornerIndex and the vertex is in border, it
                        // return a corner that vertex of next corner is in
                        // border.
                        c = cornerTable.vertexToCornerIndex(cornerVertex);
                        cornerVertex = cornerTable.cornerToVertexIndex(cornerTable.cornerNext(c));
                    } while(cornerVertex != vStart && limit++ < 100000); //100000 to avoid infinite loop in non-manifold meshes
                }

                if(!border.empty())
                {
                    borders.push_back(border);
                }
            }

            if(visited.find(v)==visited.end())
            {
                visited[v]=VISITED;
                queue.push(v);
            }

             if(corner!=cornerStart)
            corner = cornerTable.cornerSwing(corner);
        } while(corner!=cornerStart);

        visited[vertex] = FINISHED;
    }
}


void glMeshToCornerTable(const opengl::UntexturedMesh &glMesh, CornerTable** ctMesh)
{
    const auto& vertexBuffer = glMesh.vertexBuffer;
    const auto& indices = glMesh.indices;
    int numAttr = 6;
    double* dVertices = new double[vertexBuffer.size()*numAttr];
    for( unsigned int i = 0; i < vertexBuffer.size(); i++ )
    {
        int index = numAttr*i;

        auto pos = vertexBuffer[i].position;
        dVertices[index+0] = pos.x;
        dVertices[index+1] = pos.y;
        dVertices[index+2] = pos.z;

        auto normal = vertexBuffer[i].normal;
        dVertices[index+3] = normal.x;
        dVertices[index+4] = normal.y;
        dVertices[index+5] = normal.z;
    }

    *ctMesh = new CornerTable( &indices[0], dVertices,
             (CornerType)indices.size()/3, (CornerType)vertexBuffer.size(), numAttr );

    delete[] dVertices;
}


void cornerTableToGlMesh(const CornerTable* ctMesh, opengl::UntexturedMesh &glMesh)
{
    auto numAttributes = ctMesh->getNumberAttributesByVertex();

    if(numAttributes < 6)
    {
        std::cerr <<
                     "[ROISelectionOperator::cornerTableToGlMesh] Error: number of attributes less than 6."
                  << std::endl;
    }

    auto nVertices = ctMesh->getNumberVertices();
    double* attributes = ctMesh->getAttributes();

    auto& vertexBuffer = glMesh.vertexBuffer;
    auto& indices = glMesh.indices;
    vertexBuffer.resize(nVertices);
    for( unsigned int i = 0; i < nVertices; i++ )
    {
        int index = i*numAttributes;
        vertexBuffer[i].position = glm::vec3(attributes[index+0],
                                    attributes[index+1],
                                    attributes[index+2]);
        vertexBuffer[i].normal = glm::vec3(attributes[index+3],
                                    attributes[index+4],
                                    attributes[index+5]);
    }

    auto triangles = ctMesh->getTriangleList();
    indices = std::vector<unsigned int>(triangles, triangles + (ctMesh->getNumTriangles()*3));
}


void findBorders(const opengl::UntexturedMesh& mesh, std::vector< std::vector< unsigned int > >& borders)
{
    CornerTable* cornerTable;
    glMeshToCornerTable(mesh,&cornerTable);

    std::vector<bool> visited(cornerTable->getNumberVertices(),false);

    int numCorners = cornerTable->getNumTriangles()*3;
    for(int c = 0; c < numCorners; ++c)
    {
        std::vector<unsigned int> border;
        if(cornerTable->cornerOpposite(c) == CornerTable::BORDER_CORNER) //found a border
        {
            auto corner = cornerTable->cornerNext(c);
            auto vStart = cornerTable->cornerToVertexIndex(corner);

            int limit = 0;

            if(!visited[vStart])
            {
                auto v = vStart;

                do
                {
                    border.push_back(v);
                    visited[v] = true;
                    // This cornertable implementation have some optimizations
                    // tricks, like when you call @vertexToCornerIndex and the
                    // vertex is in border, it return a corner that vertex of
                    // next corner is in border.
                    corner = cornerTable->vertexToCornerIndex(v);
                    v = cornerTable->cornerToVertexIndex(cornerTable->cornerNext(corner));
                } while( v != vStart && limit++ < 100000); //100000 to avoid infinite loop in non-manifold meshes
            }

            if(!border.empty())
            {
                borders.push_back(border);
            }
        }
    }

    delete cornerTable;
}


double earScore(
    const glm::vec3& screenPos1,
    const glm::vec3& screenPos2,
    const glm::vec3& screenPos3
) {
    glm::vec3 normal(0,0,-1);
    glm::vec3 a = glm::normalize(screenPos2 - screenPos1);
    glm::vec3 b = glm::normalize(screenPos3 - screenPos2);
    return atan2(glm::dot(normal, glm::cross(a, b)), glm::dot(a, b));
}


void triangulateHoles(
    opengl::UntexturedMesh& mesh,
    const std::vector< std::vector<unsigned int> >& holes_in,
    const glm::mat4& model, const glm::mat4& view, const glm::mat4& proj, const glm::vec4 viewport
) {
    std::vector< std::vector<unsigned int> >holes(holes_in);
    for(auto hole : holes)
    {
        if(hole.size() <= 3) {
            if(hole.size() == 3) {
                mesh.indices.push_back(hole[0]);
                mesh.indices.push_back(hole[1]);
                mesh.indices.push_back(hole[2]);
            }
        } else {
            // Step 1: Project hole points into screen plane
            std::vector< glm::vec3 > screenPos;
            std::unordered_map< glm::vec3, unsigned int > newIdxs;
            auto mv = view*model;
            for(const auto& vertexIdx : hole)
            {
                screenPos.push_back( glm::project(mesh.vertexBuffer[vertexIdx].position, mv, proj, viewport) );
                newIdxs[screenPos.back()]=vertexIdx;
            }

            // Step 2: ear cutting
            while(screenPos.size() > 3) {
                int best_i1 = -1;
                double best_score = std::numeric_limits<float>::min();
                for(unsigned int i1 = 0; i1 < screenPos.size(); i1++) {
                    unsigned int i2 = i1 + 1;
                    if(i2 == screenPos.size()) {
                        i2 = 0;
                    }
                    unsigned int i3 = i2 + 1;
                    if(i3 == screenPos.size()) {
                        i3 = 0;
                    }
                    double score = earScore(screenPos[i1], screenPos[i2], screenPos[i3]);
                    if(score > best_score) {
                        best_i1 = int(i1);
                        best_score = score;
                    }
                }

                unsigned int best_i2 = best_i1 + 1;
                if(best_i2 == screenPos.size()) {
                    best_i2 = 0;
                }
                unsigned int best_i3 = best_i2 + 1;
                if(best_i3 == screenPos.size()) {
                    best_i3 = 0;
                }
                mesh.indices.push_back(newIdxs[screenPos[best_i1]]);
                mesh.indices.push_back(newIdxs[screenPos[best_i2]]);
                mesh.indices.push_back(newIdxs[screenPos[best_i3]]);

                screenPos.erase(screenPos.begin() + best_i2);
            }

            // Step 3: last triangle(Last 3 triangles in trianglesHole)
            mesh.indices.push_back(newIdxs[screenPos[0]]);
            mesh.indices.push_back(newIdxs[screenPos[1]]);
            mesh.indices.push_back(newIdxs[screenPos[2]]);
        }
    }
}

void createHole(opengl::UntexturedMesh& mesh,
                const std::vector< unsigned int >& selectionBorder,
                const std::set< unsigned int >& triangles)
{
    // step 1: get the vertex list of triangles without repeat
    std::set<unsigned int> insideVertexesID;
    for(unsigned int vertexID : triangles)
    {
      std::cerr << "Inserindo verticeID: " <<vertexID*3 << "     de:" << mesh.indices.size() << std::endl;
      std::cerr << "Com vertice: " <<mesh.indices[vertexID*3] << "     de:" << mesh.vertexBuffer.size() << std::endl;

      insideVertexesID.insert(mesh.indices[vertexID*3]);
    }
    std::cerr << "END OF STEP 1" << std::endl;
    // step 2: Remove vertexes from border of this vector
    for(unsigned int vertexID : selectionBorder)
    {
      insideVertexesID.erase(mesh.indices[vertexID]);
    }
    std::cerr << "END OF STEP 2" << std::endl;
    // step 3: remove triangles from mesh indexes vector. Set is sorted, so
    // only remove triangle starting from back
    for(std::set< unsigned int >::reverse_iterator triangleID(triangles.rbegin()); triangleID!=triangles.rend(); ++triangleID)
    {
      mesh.indices.erase(std::next(mesh.indices.begin(),(*triangleID)*3));
      mesh.indices.erase(std::next(mesh.indices.begin(),(*triangleID)*3));
      mesh.indices.erase(std::next(mesh.indices.begin(),(*triangleID)*3));
    }
    std::cerr << "END OF STEP 3" << std::endl;
    // step 4: Remove vertexes from mesh vertex buffer and fix triangle indexes
    std::vector< int > newIdxs(mesh.vertexBuffer.size());
    // Fill with 0, 1, ..., mesh.vertexBuffer.size().
    std::iota( std::begin(newIdxs), std::end(newIdxs), 0);
    // for(const auto& vertexID : insideVertexesID)
    // {
    //     mesh.vertexBuffer.erase(mesh.vertexBuffer.begin()+vertexID);
    //     newIdxs[vertexID] = -1;
    //     std::transform(newIdxs.begin()+vertexID,newIdxs.end(),newIdxs.begin()+vertexID,[](int x){return x-1;});
    // }
    std::cerr << "Preparando operacoes" << std::endl;
    for(std::set< unsigned int >::reverse_iterator vertexID(insideVertexesID.rbegin()); vertexID!=insideVertexesID.rend(); ++vertexID)
    {
        std::cerr << "Apagando vertice: " <<*vertexID << "     de:" << mesh.vertexBuffer.size() << std::endl;
        mesh.vertexBuffer.erase(mesh.vertexBuffer.begin()+(*vertexID));
        newIdxs[*vertexID] = -1;
        std::transform(newIdxs.begin()+(*vertexID),newIdxs.end(),newIdxs.begin()+(*vertexID),[](int x){return x-1;});
    }
    for(unsigned int i=0; i < mesh.indices.size(); i++)
    {
        // std::cerr << "Colocando valor na posicao: " << mesh.indices[i] << "   com size: " << mesh.indices.size() << std::endl;
        mesh.indices[i]=newIdxs[mesh.indices[i]];
        // std::cerr << "ok!" << std::endl;
    }
    std::cerr << "END OF STEP 4" << std::endl;
}

void removeRepeatedVertices(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<unsigned int>& indices)
{
    std::vector<unsigned int> reindex(vertices.size());
    std::list<unsigned int> processingList;
    for(unsigned int i = 0; i < vertices.size(); ++i)
    {
        processingList.push_back(i);
        reindex[i] = i;
    }

    std::vector<unsigned int> toBeDeleted(vertices.size(),false);

    while(!processingList.empty())
    {
        auto cur = *processingList.begin();

        auto removeCount = 0;

        auto it = processingList.begin();
        for(++it; it != processingList.end();)
        {
            if(vertices[cur] == vertices[(*it)])
            {
                toBeDeleted[(*it)] = true;

                reindex[(*it)] = reindex[cur];

                std::cerr << "[MeshAlgorithms] Repeated vertex found and removed ("
                          << (*it) << " -> " << cur << ")" << std::endl;

                it = processingList.erase(it);
                removeCount++;
            }
            else
            {
                reindex[(*it)] -= removeCount;
                ++it;
            }
        }

        processingList.pop_front();
    }

    for(int i = toBeDeleted.size()-1; i >= 0; --i)
    {
        if(toBeDeleted[i])
        {
            vertices.erase(vertices.begin()+i);
            normals.erase(normals.begin()+i);
        }
    }

    for(auto& index : indices)
    {
        index = reindex[index];
    }

    for(int t = indices.size()-3; t >= 0; t -= 3)
    {
        auto id0 = indices[t+0];
        auto id1 = indices[t+1];
        auto id2 = indices[t+2];

        if(id0 == id1 || id0 == id2 || id1 == id2)
        {
            indices.erase(indices.begin()+t,indices.begin()+t+3);
        }
    }
}
