#include "sketchsurfacegenerator.h"

#include <iostream>
#include <sstream>

SketchSurfaceGenerator::SketchSurfaceGenerator(const std::vector<Sketch>& sketches)
    : sketches(sketches)
{

}

SketchInterpolationGenerator::SketchInterpolationGenerator(const std::vector<Sketch> &sketches)
    : SketchSurfaceGenerator(sketches)
{

}

void SketchInterpolationGenerator::run(opengl::UntexturedMesh &outputMesh)
{
    if(sketches.size()<2)
        return;

    resampleCurve(sketches.front(),sketchNumPoints);

    // Alinhar e amostrar igualmente todos os sketches
    auto lastIt = std::prev(sketches.end());
    for(auto it = sketches.begin(); it != lastIt; ++it)
    {
        alignCurve(*it,*std::next(it));
    }

    // Teste de alinhamento
//    int count = 0;
//    for(auto sketch : sketches)
//    {
//        std::stringstream file;
//        file << "sketch-aligned-" << count++ << ".xyz";
//        writeXYZ(file.str(),sketch);
//    }

    auto computeLength = [](const Sketch& sketch)
    {
        float length = 0.0f;
        for(unsigned int i = 0; i < sketch.size()-1; ++i)
        {
            length += glm::distance(sketch[i],sketch[i+1]);
        }
        return length;
    };


    // Definir a resolução da malha na outra direção
    std::vector<Sketch> finalCurves;

    for(auto it = sketches.begin(); it != lastIt; ++it)
    {
        auto& s0 = *it;
        auto& s1 = *std::next(it);

        if(s0.size() != s1.size())
        {
            //Should not happen after alignment
            break;
        }

        float unitLength = computeLength(s0) / (s0.size()-1);

        //Poor approximation; TODO: Compute the real distance plane
        float planeDist = glm::distance(s0[0],s1[0]);

        auto numCurves = static_cast<unsigned int>(planeDist / unitLength - 1);

        finalCurves.push_back(s0);

        auto first = finalCurves.size();
        auto last = first + numCurves - 1;

        for(unsigned int i = 0; i < numCurves; ++i)
        {
            finalCurves.push_back(Sketch(s0.size()));
        }

        auto step = 1.0f / (numCurves+1);
        for(unsigned int i = 0; i < s0.size(); ++i)
        {
            auto p0 = s0[i];
            auto p1 = s1[i];

            auto alpha = step;
            for(auto c = first; c <= last; ++c, alpha+=step)
            {
                finalCurves[c][i] = glm::mix(p0,p1,alpha);
            }
        }
    }
    finalCurves.push_back(*lastIt);

//    // Alinhar e amostrar igualmente todos os sketches
//    lastIt = std::prev(finalCurves.end());
//    for(auto it = finalCurves.begin(); it != lastIt; ++it)
//    {
//        alignCurve(*it,*std::next(it));
//    }

    //Write some of the final curves
//    auto step = finalCurves.size()/20;
//    count = 0;
//    for(unsigned int i = 0; i < finalCurves.size(); i+=step)
//    {
//        std::stringstream file;
//        file << "finalcurve-" << count++ << ".xyz";
//        writeXYZ(file.str(),finalCurves[i]);
//    }

    unsigned int ny = static_cast<unsigned int>(finalCurves.size());
    unsigned int nx = static_cast<unsigned int>(finalCurves[0].size()-1);
    auto index = [&nx](unsigned int x, unsigned int y)
    {
        return nx*y + x;
    };

    outputMesh.indices.reserve((ny-1)*(nx-1)*2);
    for(auto y = 0u; y < ny-1; ++y)
    {
        auto x = 0u;
        for(; x < nx-1; ++x)
        {
             // Creates this triangle:
             //         x
             //        /|
             //       / |
             //      /  |
             //     /   |
             //    x----x
             //
            outputMesh.indices.push_back(index(x+0,y+0));
            outputMesh.indices.push_back(index(x+1,y+0));
            outputMesh.indices.push_back(index(x+1,y+1));

            // Creates this triangle:
            //     x----x
            //     |   /
            //     |  /
            //     | /
            //     |/
            //     x
            //
           outputMesh.indices.push_back(index(x+0,y+0));
           outputMesh.indices.push_back(index(x+1,y+1));
           outputMesh.indices.push_back(index(x+0,y+1));
        }

        //Connects the last triangles to the first ones
        outputMesh.indices.push_back(index(x,y+0));
        outputMesh.indices.push_back(index(0,y+0));
        outputMesh.indices.push_back(index(0,y+1));

        outputMesh.indices.push_back(index(x,y+0));
        outputMesh.indices.push_back(index(0,y+1));
        outputMesh.indices.push_back(index(x,y+1));
    }

    outputMesh.vertexBuffer.reserve(nx*ny);
    for(auto curve : finalCurves)
    {
        auto last = std::prev(curve.end());
        for(auto it = curve.begin(); it != last; ++it)
        {
            outputMesh.vertexBuffer.push_back({*it,{0,0,0}});
        }
    }

    //Compute normals
    for(unsigned int i = 0; i < outputMesh.indices.size()-2; i+=3)
    {
        auto i0 = outputMesh.indices[i+0];
        auto i1 = outputMesh.indices[i+1];
        auto i2 = outputMesh.indices[i+2];

        auto u = outputMesh.vertexBuffer[i1].position-outputMesh.vertexBuffer[i0].position;
        auto v = outputMesh.vertexBuffer[i2].position-outputMesh.vertexBuffer[i0].position;

        auto n = glm::cross(u,v);

        outputMesh.vertexBuffer[i0].normal += n;
        outputMesh.vertexBuffer[i1].normal += n;
        outputMesh.vertexBuffer[i2].normal += n;
    }
    for(auto& v : outputMesh.vertexBuffer)
    {
        glm::normalize(v.normal);
    }

}

void SketchInterpolationGenerator::resampleCurve(SketchSurfaceGenerator::Sketch &curve, unsigned int numPoints)
{
    //Compute the arc lengths
    auto parameterize = [](const std::vector<glm::vec3>& curve,
            std::vector<float>& coeffs)
    {
        coeffs.resize(curve.size(),0.0f);

        for(unsigned int i = 1; i < curve.size(); ++i)
        {
            auto d = glm::distance(curve[i-1],curve[i]);
            coeffs[i] = coeffs[i-1] + d;
        }

        auto totalDist = coeffs.back();
        for(auto& c : coeffs)
            c /= totalDist;
    };

    //Find a point in the curve, given its parameter t in [0,1]
    auto getPoint = [](const std::vector<glm::vec3>& curve,
            const std::vector<float>& coeffs, unsigned int& start, float t)
    {
        auto size = curve.size();

        while(t > 1.0f)
            t -= 1.0f;

        unsigned int end = start+1;

        while(end<size && t>coeffs[end])
        {
            end++;
        }

        start = end-1;

        auto a = (t-coeffs[start]) / (coeffs[end]-coeffs[start]);
        return glm::mix(curve[start],curve[end],a);
    };

    std::vector<float> coeffs;
    parameterize(curve,coeffs);

    float step = 1.0f/numPoints;
    float t = 0.0f;
    unsigned int start = 0;
    Sketch newCurve(numPoints);
    for(unsigned int i = 0; i < numPoints; ++i,t+=step)
    {
        newCurve[i] = getPoint(curve,coeffs,start,t);
    }
    newCurve.push_back(newCurve[0]);

    curve.swap(newCurve);
}



void SketchInterpolationGenerator::alignCurve(const Sketch &refCurve, Sketch &editCurve)
{
    if(refCurve.size() < 2 || editCurve.size() < 2)
    {
        std::cerr << "[SketchInterpolationGenerator::alignSketch] Both reference "
                     "curve and editable must have at least two points." << std::endl;
        return;
    }

    //Print curve sizes
    auto n = refCurve.size();
    std::cout << "\n\nAligning curve... [Reference size: " << n << " points / "
                                         "Old size: " << editCurve.size() << " points]" << std::endl;

    //Compute the arc lengths
    auto parameterize = [](const std::vector<glm::vec3>& curve,
            std::vector<float>& coeffs)
    {
        coeffs.resize(curve.size(),0.0f);

        for(unsigned int i = 1; i < curve.size(); ++i)
        {
            auto d = glm::distance(curve[i-1],curve[i]);
            coeffs[i] = coeffs[i-1] + d;
        }

        auto totalDist = coeffs.back();
        for(auto& c : coeffs)
            c /= totalDist;
    };

    //Find a point in the curve, given its parameter t in [0,1]
    auto getPoint = [](const std::vector<glm::vec3>& curve,
            const std::vector<float>& coeffs, unsigned int& start, float t)
    {
        auto size = curve.size();

        while(t > 1.0f)
            t -= 1.0f;

        unsigned int end = start+1;

        while(end<size && t>coeffs[end])
        {
            end++;
        }

        start = end-1;

        auto a = (t-coeffs[start]) / (coeffs[end]-coeffs[start]);
        return glm::mix(curve[start],curve[end],a);
    };

    //Check whether the ref curve is closed
    auto closed = glm::distance(refCurve[0],refCurve[refCurve.size()-1]) < 0.001f;

    if(closed) //If the curves are closed, we have to deal with rotation and orientation problems
    {
        std::cout << "  Closed curve." << std::endl;

        //Close the sketch curve
        editCurve.push_back(editCurve[0]);

        std::cout << "  Checking orientation... ";

        glm::vec3 refTotal(0,0,0);
        for(unsigned int i = 0; i < refCurve.size()-1; ++i)
        {
            auto v0 = refCurve[i];
            auto v1 = refCurve[i+1];
            auto prod = glm::cross(v0,v1);
            refTotal += prod;
        }

        glm::vec3 editTotal(0,0,0);
        for(unsigned int i = 0; i < editCurve.size()-1; ++i)
        {
            auto v0 = editCurve[i];
            auto v1 = editCurve[i+1];
            auto prod = glm::cross(v0,v1);
            editTotal += prod;
        }

        if(glm::dot(refTotal,editTotal) < 0)
        {
            std::reverse(editCurve.begin(),editCurve.end());
            std::cout << "Curve reversed." << std::endl;
        }
        else
        {
            std::cout << "Same." << std::endl;
        }

        auto pn = glm::normalize(refTotal);
        std::cout << "  Rotating and resampling curve... ";

        glm::vec3 refAabbMin = refCurve[0];
        glm::vec3 refAabbMax = refCurve[0];
        for(const auto& p : refCurve)
        {
            refAabbMin.x = std::min(refAabbMin.x, p.x);
            refAabbMin.y = std::min(refAabbMin.y, p.y);
            refAabbMin.z = std::min(refAabbMin.z, p.z);

            refAabbMax.x = std::max(refAabbMax.x, p.x);
            refAabbMax.y = std::max(refAabbMax.y, p.y);
            refAabbMax.z = std::max(refAabbMax.z, p.z);
        }

        glm::vec3 editAabbMin = editCurve[0];
        glm::vec3 editAabbMax = editCurve[0];
        for(const auto& p : editCurve)
        {
            editAabbMin.x = std::min(editAabbMin.x, p.x);
            editAabbMin.y = std::min(editAabbMin.y, p.y);
            editAabbMin.z = std::min(editAabbMin.z, p.z);

            editAabbMax.x = std::max(editAabbMax.x, p.x);
            editAabbMax.y = std::max(editAabbMax.y, p.y);
            editAabbMax.z = std::max(editAabbMax.z, p.z);
        }

        glm::vec3 refCurveCenter = (refAabbMin + refAabbMax) * 0.5f;
        glm::vec3 editCurveCenter = (editAabbMin + editAabbMax) * 0.5f;

        std::cout << "refCurveCenter: " << refCurveCenter << std::endl;
        std::cout << "editCurveCenter: " << editCurveCenter << std::endl;

        auto intercepts = [](const glm::vec3& o, const glm::vec3& d,
                const glm::vec3& a, const glm::vec3& b, glm::vec3 pn, float& t)
        {
            auto v1 = o - a;
            auto v2 = b - a;

            auto v1xv2 = glm::cross(v1,v2);

            if(glm::dot(pn,v1xv2)>0)
                pn = -pn;

            auto v3 = glm::cross(pn,d);

            auto dot = glm::dot(v2,v3);

            if(dot == 0)
            {
                //TODO: Treat this case.
            }

            else if(dot > 0)
            {
                auto t1 = glm::length(glm::cross(v2,v1)) / dot;
                auto t2 = glm::dot(v1,v3) / dot;

                if(t2 >=0 && t2 <= 1)
                {
                    t = t1;
                    return true;
                }
            }

            return false;
        };


        auto findInterception = [&](const glm::vec3& o, const glm::vec3&d,
                const std::vector<glm::vec3>& curve, unsigned int& index, float& t)
        {
            float tempT = -1.0f;
            unsigned int tempIdx = 0;
            for(unsigned int i = 0; i < curve.size()-2; ++i)
            {
                auto a = curve[i+0];
                auto b = curve[i+1];
                float curT = -1.0f;

                if(intercepts(o,d,a,b,pn,curT))
                {
                    if(curT>tempT)
                    {
                        tempT = curT;
                        tempIdx = i;
                    }
                }
            }

            if(tempT>-1.0f)
            {
                t = tempT;
                index = tempIdx;
                return true;
            }

            return false;
        };

        for(unsigned int i =0; i < refCurve.size(); ++i)
        {
            auto d = glm::normalize(refCurve[i]-refCurveCenter);

            unsigned int i0,i1;
            float t0, t1;

            if(findInterception(refCurveCenter,d,refCurve,i0,t0) &&
               findInterception(editCurveCenter,d,editCurve,i1,t1))
            {
                std::vector<float> curveCoeffs,sketchCoeffs;
                parameterize(refCurve,curveCoeffs);
                parameterize(editCurve,sketchCoeffs);

                auto p0 = refCurveCenter + d*t0;
                auto p1 = editCurveCenter + d*t1;

                auto prop0 = glm::distance(p0,refCurve[i0])/
                        glm::distance(refCurve[i0],refCurve[i0+1]);
                auto prop1 = glm::distance(p1,editCurve[i1])/
                        glm::distance(editCurve[i1],editCurve[i1+1]);

                auto newT0 = curveCoeffs[i0]+prop0*(curveCoeffs[i0+1]-curveCoeffs[i0]);
                auto newT1 = sketchCoeffs[i1]+prop1*(sketchCoeffs[i1+1]-sketchCoeffs[i1]);

                std::vector<glm::vec3> newSketch;
                newSketch.reserve(n);

                auto diff = newT1-newT0;
                if(diff<0)
                    diff += 1.0f;

                for(unsigned int j = 0; j < n; ++j)
                {
                    auto t = curveCoeffs[j]+diff;
                    unsigned int start = 0;
                    newSketch.push_back(getPoint(editCurve,sketchCoeffs,start,t));
                }

                editCurve = newSketch;
                std::cout << "Done." << std::endl;

                break;
            }
        }
    }

    else
    {
        std::cout << "  Open curve." << std::endl;

        //This solution does not cover all cases:
        if( glm::distance(refCurve[0],editCurve[0]) >
            glm::distance(refCurve[0],editCurve[editCurve.size()-1]) )
                std::reverse(editCurve.begin(),editCurve.end());

        std::cout << "  Resampling... ";
        std::vector<float> curveCoeffs,sketchCoeffs;
        parameterize(refCurve,curveCoeffs);
        parameterize(editCurve,sketchCoeffs);

        std::vector<glm::vec3> newSketch;
        newSketch.reserve(n);

        unsigned int start = 0;
        for(unsigned int j = 0; j < n; ++j)
        {
            newSketch.push_back(getPoint(editCurve,sketchCoeffs,start,curveCoeffs[j]));
        }

        editCurve = newSketch;
        std::cout << "Done." << std::endl;
    }

    std::cout << "Curve aligned. [Reference size: " << refCurve.size() << " points / "
                                  "New size: " << editCurve.size() << " points]" << std::endl;

}

void SketchInterpolationGenerator::setSketchNumPoints(unsigned int value)
{
    sketchNumPoints = value;
}
