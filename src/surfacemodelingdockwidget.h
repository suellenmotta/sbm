#ifndef SURFACEMODELINGDOCKWIDGET_H
#define SURFACEMODELINGDOCKWIDGET_H

#include <QDockWidget>
#include <memory>

namespace Ui {
class SurfaceModelingDockWidget;
}

class BorderSelectionOperator;
class SliceSketchingOperator;
class Canvas3DWidget;

class SurfaceModelingDockWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit SurfaceModelingDockWidget(QWidget* parent, Canvas3DWidget* canvas);

    void setBorderOperator(std::shared_ptr<BorderSelectionOperator> borderOperator);

    void setSliceSketchingOperator(std::shared_ptr<SliceSketchingOperator> sketchingOperator);

    ~SurfaceModelingDockWidget();

public slots:
    void onBorderSelectionChanged();
    void onSketchFinished();

private slots:
    void on_addBorderButton_toggled(bool checked);
    void on_deleteBorderButton_clicked();

    void on_addSketchButton_toggled(bool checked);
    void on_deleteSketchButton_clicked();
    void on_showAllSketches_toggled(bool checked);
    void on_sketchesListWidget_itemSelectionChanged();

    void on_pointCloudButton_clicked(bool checked);
    void on_viewModeButton_clicked(bool checked);
    void on_resetButton_clicked();
    void on_generateButton_clicked();

    void on_SurfaceModelingDockWidget_visibilityChanged(bool visible);

private:
    void fillSelectedBordersList();
    void fillSketchesList();

    Ui::SurfaceModelingDockWidget *ui;
    Canvas3DWidget* canvas;

    std::shared_ptr<BorderSelectionOperator> borderOperator;
    std::shared_ptr<SliceSketchingOperator> sketchingOperator;
};

#endif // SURFACEMODELINGDOCKWIDGET_H
