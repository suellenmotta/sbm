#ifndef CURVEINTERPOLATIONWIDGET_H
#define CURVEINTERPOLATIONWIDGET_H

#include <QWidget>
#include <memory>
#include "mesh.h"

namespace Ui {
class CurveInterpolationWidget;
}

class SliceSketchingOperator;
class Canvas3DWidget;

class CurveInterpolationWidget : public QWidget
{
    Q_OBJECT

public:

    explicit CurveInterpolationWidget(QWidget *parent, Canvas3DWidget* canvas);

    void computeSurface(opengl::UntexturedMesh &mesh);

    void setSketchingOn();
    void setSketchingOff();

signals:

public slots:
    void onSketchFinished();

private slots:
    void on_creationDirectionComboBox_currentIndexChanged(const QString &);

    void on_addCreationSketch_clicked(bool checked);

    void on_deleteCreationSketch_clicked();

private:
    void fillSketchesList();

    Ui::CurveInterpolationWidget *ui;
    Canvas3DWidget* canvas;
    std::shared_ptr<SliceSketchingOperator> sketchingOperator;
};

#endif // CURVEINTERPOLATIONWIDGET_H
