#ifndef BORDERSELECTIONOPERATOR_H
#define BORDERSELECTIONOPERATOR_H

#include "operator.h"

#include <QOpenGLShaderProgram>

#include <vector>

class BorderSelectionOperator
        : public Operator
{
    Q_OBJECT

public:
    BorderSelectionOperator(Scene& scene, GLuint defaultFbo);
    virtual ~BorderSelectionOperator();

    void draw() override;

    void showPointCloud();
    void showLineLoops();

    void unselectBorder(unsigned int object, unsigned int border);
    void unselectBorders();
    std::vector< std::pair<int,int> > getSelectedBorders();
    std::vector< std::string > getSelectedBorderNames();
    std::vector< glm::vec3 > getSelectedBordersPointCloud();
    std::vector<unsigned int> getBorderIndices(unsigned int object, unsigned int border);

    bool mousePress(QMouseEvent*) override;
    bool mouseMove(QMouseEvent* event) override;
    bool mouseRelease(QMouseEvent* event) override;

public slots:
    void onViewChanged() override;

signals:
    void borderSelectionChanged();

private:
    BorderSelectionOperator(const BorderSelectionOperator&) = delete;

    GLuint defaultFbo;
    GLuint fbo;
    GLuint sceneTex;
    GLuint pickingTex;
    GLuint depthTex;

    std::vector< GLuint > ibos;

    void createBorderTexture();
    void fillBorderTexture();
    void pickBorder(const glm::ivec2& pos,int& obj, int& border, float& depth);

    using Border = std::vector<unsigned int>;
    std::vector< std::vector< Border > > borders;
    std::vector< std::vector< bool > > isBorderSelected;

    QOpenGLShaderProgram program;

    std::pair<int,int> highlighted;

    enum HighlightState
    {
        NONE,
        SELECTED,
        UNSELECTED
    };

    GLenum drawMode;
    HighlightState state;
    void resetHighlighting();
};

#endif // BORDERSELECTIONOPERATOR_H
