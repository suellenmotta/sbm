#include "raycasting.h"

#include <glm/gtc/matrix_transform.hpp>

glm::vec3 rayDirection( const glm::vec2& screenPoint, const ArcballCamera& camera )
{
   //Normalised device space
   glm::vec3 ray_nds;
   ray_nds.x = (2.0f * screenPoint.x) / camera.width - 1.0f;
   ray_nds.y = 1.0f - (2.0f * screenPoint.y) / camera.height;
   ray_nds.z = 1.0f;

   //Clip space
   glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);

   //Eye space
   glm::vec4 ray_eye = glm::inverse(camera.projMatrix()) * ray_clip;
   ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

   //World space
   glm::vec3 ray_wor = glm::vec3(inverse(camera.viewMatrix()) * ray_eye);

   return glm::normalize(ray_wor);
}

/**
 * Implemented follow this site: http://geomalgorithms.com/
 * a06-_intersect-2.html
 */
bool interceptsTriangle( const glm::vec3& orig, const glm::vec3& dir,
                        const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2,
                        float& dist )
{
   auto e1 = v1 - v0;
   auto e2 = v2 - v0;

   auto pvec = glm::cross(dir, e2);
   float det = glm::dot(e1, pvec);

   if( det < 1e-8 && det > -1e-8)
   {
       return false;
   }

   float inv_det = 1 / det;
   auto tvec = orig - v0;
   float u = glm::dot(tvec, pvec) * inv_det;

   if( u < 0 || u > 1)
   {
       return false;
   }

   auto qvec = glm::cross(tvec, e1);
   float v = glm::dot(dir, qvec) * inv_det;
   if( v < 0 || u + v > 1)
   {
       return false;
   }


   dist = glm::dot(e2,qvec)*inv_det;
   return true;
}

// Verifica se o raio intercepta algum triângulo de algum objeto da cena
// Algoritmo de Moller-Trumbore.
// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
// Se sim, retorna true e preenche o índice "object" do objeto interceptado
// e o ponto de interseção "point"
bool intercepts( const glm::vec2& screenPoint, const Scene& scene, int& object, glm::vec3& point )
{
   glm::vec3 orig = scene.camera.eye;
   glm::vec3 dir = rayDirection(screenPoint,scene.camera);

   object = -1;
   int curObject = -1;
   point = glm::vec3();

   float minDist = FLT_MAX;

   for( const auto& obj : scene.objects )
   {
       curObject++;

       for( unsigned int i = 0; i < obj.indices.size(); i+=3 )
       {
           //Os três vértices do triângulo
           auto v0 = glm::vec3(obj.modelMatrix * glm::vec4(obj.vertexBuffer[obj.indices[i+0]].position,1));
           auto v1 = glm::vec3(obj.modelMatrix * glm::vec4(obj.vertexBuffer[obj.indices[i+1]].position,1));
           auto v2 = glm::vec3(obj.modelMatrix * glm::vec4(obj.vertexBuffer[obj.indices[i+2]].position,1));

           float dist = 0.0f;
           if( interceptsTriangle( orig, dir, v0, v1, v2, dist ) )
           {
               if( dist < minDist )
               {
                   point = orig + dist * dir;
                   minDist = dist;
                   object = curObject;
               }
           }
       }
   }

   return object != -1;
}
