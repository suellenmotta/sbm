#include "sketchbasedsurfacedeformationwidget.h"

#include "ui_sketchbasedsurfacedeformationwidget.h"

#include <iostream>

struct SketchInfo
{
    unsigned int axis;
    unsigned int slice;
    unsigned int index;
};
Q_DECLARE_METATYPE(SketchInfo)

SketchBasedSurfaceDeformationWidget::SketchBasedSurfaceDeformationWidget(QWidget *parent,Canvas3DWidget* canvas)
    : QWidget(parent)
    , ui(new Ui::SketchBasedSurfaceDeformationWidget)
    , canvas(canvas)
    , deformer(nullptr)
    , sketchingOperator(canvas->createSurfaceEditingOperator())
{
    ui->setupUi(this);

    updateSurfaceList();

    ui->volumeComboBox->addItem("Seismic volume [default]");

    updateDeformer();

    ui->deformSurfaceButton->setEnabled(false);
}

void SketchBasedSurfaceDeformationWidget::setSliceSketchingOperator(std::shared_ptr<SurfaceEditingOperator> sketchingOperator)
{
    if(this->sketchingOperator)
    {
        disconnect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
                   this,SLOT(onSketchFinished()));
    }

    this->sketchingOperator = sketchingOperator;

    if(this->sketchingOperator)
    {
        this->sketchingOperator->setSketchSurfaceDeformation(deformer);

        connect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
                this,SLOT(onSketchFinished()));
    }
}

SketchBasedSurfaceDeformationWidget::~SketchBasedSurfaceDeformationWidget()
{
    if(this->sketchingOperator)
    {
        disconnect(this->sketchingOperator.get(),SIGNAL(sketchFinished()),
                   this,SLOT(onSketchFinished()));
    }

    delete deformer;
    deformer = nullptr;
}

void SketchBasedSurfaceDeformationWidget::setSketchingOn()
{
    if(sketchingOperator)
    {
        canvas->setOperator(std::static_pointer_cast<Operator>(sketchingOperator));
        sketchingOperator->showInterceptionCurves(true);
        connect(sketchingOperator.get(),SIGNAL(sketchFinished()),
                this,SLOT(onSketchFinished()));
    }

    ui->addSketchButton->setChecked(true);
    canvas->update();
}

void SketchBasedSurfaceDeformationWidget::setSketchingOff()
{
    canvas->setOperator(nullptr);

    if(sketchingOperator)
    {
        sketchingOperator->showInterceptionCurves(false);
        disconnect(sketchingOperator.get(),SIGNAL(sketchFinished()),
                   this,SLOT(onSketchFinished()));
    }

    ui->addSketchButton->setChecked(false);
    canvas->update();
}

void SketchBasedSurfaceDeformationWidget::updateSurfaceList()
{
    auto& scene = canvas->getScene();

    ui->surfaceComboBox->blockSignals(true);
    ui->surfaceComboBox->clear();
    for(const auto& surface : scene.objects)
    {
        ui->surfaceComboBox->addItem(QString::fromStdString(surface.name));
    }
    ui->surfaceComboBox->blockSignals(false);
}

void SketchBasedSurfaceDeformationWidget::saveSketches(const std::string &dir)
{
    if(deformer)
    {
        for(int axis = 0; axis < 3; ++axis)
        {
            auto sketches = deformer->getAllSketches((NewVolume::Axis)axis);

            for(auto& entry : sketches)
            {
                auto slice = entry.first;
                auto& sliceSketches = entry.second;

                for(unsigned int i = 0; i < sliceSketches.size(); ++i)
                {
                    std::stringstream path;
                    path << dir << "s_" << axis << "_" << slice << "_" << i << ".xyz";

                    auto& sketch = sliceSketches[i];

                    writeXYZ(path.str(), sketch);
                }
            }
        }

    }
}

void SketchBasedSurfaceDeformationWidget::onSketchFinished()
{
    fillSketchesList();

    if(ui->autoDeformCheckBox->isChecked() && deformer)
    {
        deformer->run();

        if(sketchingOperator)
        {
            sketchingOperator->updateInterceptionCurves();
            sketchingOperator->updateSketches();
        }
    }
}

void SketchBasedSurfaceDeformationWidget::on_surfaceComboBox_currentIndexChanged(int)
{
    updateDeformer();
}

void SketchBasedSurfaceDeformationWidget::on_showInterceptionCheckBox_toggled(bool checked)
{
    sketchingOperator->showInterceptionCurves(checked);
    canvas->update();
}

void SketchBasedSurfaceDeformationWidget::on_snapToVolumeCheckBox_toggled(bool checked)
{
    ui->volumeOptionsWidget->setEnabled(checked);
}

void SketchBasedSurfaceDeformationWidget::on_volumeComboBox_currentIndexChanged(int index)
{

}

void SketchBasedSurfaceDeformationWidget::on_viewVolumeToolButton_toggled(bool checked)
{

}

void SketchBasedSurfaceDeformationWidget::on_loadVolumeToolButton_clicked()
{

}

void SketchBasedSurfaceDeformationWidget::on_addSketchButton_clicked(bool checked)
{
    if(checked)
    {
        setSketchingOn();
    }
    else
    {
        setSketchingOff();
    }
}

void SketchBasedSurfaceDeformationWidget::on_deformSurfaceButton_clicked()
{
    if(deformer)
        deformer->run();

    if(sketchingOperator)
    {
        sketchingOperator->updateInterceptionCurves();
        sketchingOperator->updateSketches();
    }
}


void SketchBasedSurfaceDeformationWidget::on_deleteSketchButton_clicked()
{
    if(deformer)
    {
        auto selected = ui->sketchesListWidget->currentItem();

        if(selected)
        {
            auto sketchInfo = selected->data(Qt::UserRole).value<SketchInfo>();

            deformer->removeSketch((NewVolume::Axis)sketchInfo.axis,
                                   sketchInfo.slice,sketchInfo.index);

            ui->sketchesListWidget->removeItemWidget(selected);
            delete selected;

            if(sketchingOperator)
                sketchingOperator->updateSketches();

            if(ui->autoDeformCheckBox->isChecked())
            {
                deformer->run();

                if(sketchingOperator)
                    sketchingOperator->updateInterceptionCurves();
            }

            canvas->update();
        }
    }
}


void SketchBasedSurfaceDeformationWidget::on_showAllSketches_toggled(bool checked)
{
    sketchingOperator->showSketches(checked);
    canvas->update();
}

void SketchBasedSurfaceDeformationWidget::fillSketchesList()
{
    if(deformer)
    {
        ui->sketchesListWidget->clear();

        std::string prefixes[3] = {"sketch_X_","sketch_Y_","sketch_Z_"};

        for(int ax = 0; ax < 3; ++ax)
        {
            auto axis = static_cast<NewVolume::Axis>(ax);
            auto sketches = deformer->getAllSketches(axis);

            for(const auto& entry : sketches)
            {
                auto slice = entry.first;
                auto sliceSketches = entry.second;

                for(unsigned int idx = 0; idx < sliceSketches.size(); ++idx)
                {
                    if(!sliceSketches[idx].empty())
                    {
                        std::stringstream str;
                        str << prefixes[axis] << slice << "_[" << idx << "]";

                        SketchInfo info = {static_cast<unsigned int>(axis),slice,idx};

                        QListWidgetItem* item = new QListWidgetItem();
                        item->setText(QString::fromStdString(str.str()));
                        item->setData(Qt::UserRole, QVariant::fromValue(info));

                        ui->sketchesListWidget->addItem(item);
                    }
                }
            }
        }
    }
}

void SketchBasedSurfaceDeformationWidget::updateDeformer()
{
    ui->addSketchButton->setChecked(false);

    if(deformer)
        delete deformer;
    deformer = nullptr;

    auto index = ui->surfaceComboBox->currentIndex();
    if(index<canvas->getScene().objects.size())
    {
        auto& surface = canvas->getScene().objects[index];

        deformer = new SketchSurfaceDeformation(canvas->getVolume(),surface);

        if(sketchingOperator)
        {
            sketchingOperator->setSketchSurfaceDeformation(deformer);
        }
    }

    fillSketchesList();
}

void SketchBasedSurfaceDeformationWidget::on_curvatureRadioButton_toggled(bool checked)
{
    if(deformer)
        deformer->setMinimizeDisplacements(!checked);
}

void SketchBasedSurfaceDeformationWidget::on_autoDeformCheckBox_toggled(bool checked)
{
    ui->deformSurfaceButton->setEnabled(!checked);
}
