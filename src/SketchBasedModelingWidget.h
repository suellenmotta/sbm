#ifndef SKETCHBASEDMODELINGWIDGET_H
#define SKETCHBASEDMODELINGWIDGET_H

#include <QDockWidget>

namespace Ui {
class SketchBasedModelingWidget;
}

class Canvas3DWidget;
class CurveInterpolationWidget;
class SketchBasedSurfaceDeformationWidget;

class SketchBasedModelingWidget : public QDockWidget
{
    Q_OBJECT

public:
    explicit SketchBasedModelingWidget(QWidget* parent, Canvas3DWidget* canvas);
    ~SketchBasedModelingWidget();

private slots:
    void on_createSurfaceButton_clicked();

    void on_tabWidget_currentChanged(int);

private:
    Ui::SketchBasedModelingWidget* createAndSetupUi();
    CurveInterpolationWidget* createAndSetupInterpolationWidget(Canvas3DWidget* canvas);
    SketchBasedSurfaceDeformationWidget* createAndSetupDeformationWidget(Canvas3DWidget* canvas);

    Ui::SketchBasedModelingWidget *ui = {nullptr};
    Canvas3DWidget* canvas = {nullptr};
    CurveInterpolationWidget *interpolationWidget = {nullptr};
    SketchBasedSurfaceDeformationWidget *deformationWidget = {nullptr};

    bool autoName = {true};
};

#endif // SKETCHBASEDMODELINGWIDGET_H
